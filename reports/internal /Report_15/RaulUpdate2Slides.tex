% Modelo de slides para projetos de disciplinas do Abel
\documentclass[10pt]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}
\usepackage[numbers,sort&compress]{natbib}
\bibliographystyle{plainnat}
\usepackage{adjustbox}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{multirow}
\usepackage{xspace}
\usepackage{mathtools}

\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\magenta}[1]{{\color{magenta} #1}}
\title{Rare events and filtering}
 \subtitle{numerical results update}
 \date{\today}
\date{November, 2022}
\author{Gaukhar Shaimerdenova}
\institute{AMCS, KAUST}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\V}{\mathbb{V}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\bu}{\boldsymbol{u}}
\newcommand{\bv}{\boldsymbol{v}}
\newcommand{\bx}{\boldsymbol{x}}
\newcommand{\ba}{\boldsymbol{a}}
\newcommand{\bb}{\boldsymbol{b}}
\newcommand{\bX}{\boldsymbol{X}}
\newcommand{\bK}{\boldsymbol{K}}
\newcommand{\brho}{\boldsymbol{\rho}}
\newcommand{\bmu}{\boldsymbol{\mu}}
\newcommand{\bSigma}{\boldsymbol{\Sigma}}
\newcommand{\bW}{\boldsymbol{W}}

\begin{document}

\maketitle

\begin{frame}[fragile]{Problem setup and Overview}
	We consider the general SDE of the form
	\begin{equation}\label{sde}
		\begin{cases} 
			dX_t= a(X_t)dt+b(X_t)dW_t^{\mathbb{P}}, \quad 0<t<T, \\
			X_{0}\sim \rho_0.
		\end{cases}\,
	\end{equation}
	\red{\textbf{Objective:}} For a given threshold $K>0$, to track the probability:
	\[
	\alpha:=P(\max_{0\leq t\leq T} X_t\geq K).
	\] 
	\red{\textbf{Numerical studies done so far:}}\\
	$\bullet$ Tested a change of measure with respect to \textbf{only initial condition}: good robust results in variance reduction for 1D - OU, DW, 2D - Langevin, and in certain model parameter settings for 3D - noisy Lorenz' 63 and multiD -noisy Lorenz' 96. \\
	$\bullet$ Studied the time distribution when running maximum achieves the threshold. In the case of Lorenz examples, we observe that in the settings when dynamics are steady or periodic, the maximum is achieved in the beginning of the considered time interval; when dynamics are more chaotic, the maximum is achieved mostly in the middle/end of time. \\
\end{frame}


\begin{frame}[fragile]{Overview}
	\red{\textbf{Numerical studies done so far (cont.):}}\\
	$\bullet$ Therefore, we decided to perform a change of measure with respect to $W_t$ too. Tested on 1D - DW problem, 2D - Langevin dynamics and multiD - nosiy Lorenz' 96. \\
	$\bullet$ We faced several numerical issues in computing optimal control and used some ideas/techniques to solve these issues. \\
	$\bullet$  Optimal control is computed via solving KBE and verified with the one obtained via solving HJB equation. Tested different boundary conditions. Tested different smoothening of the final condition.\\
	$\bullet$ Tested initial conditions with different variances. Since the results depend on this parameter a lot. \\
	$\bullet$Tested the combination of change of measures with respect to both $\rho_0$ and $W_t$. 
\end{frame}

\begin{frame}[fragile]{Change of Measure with respect to $W_t$}
$\bullet$ The optimal control is defined by
\[
\xi^*(t,x) = b(t,x) \frac{\partial \log v_\tau(t,x)}{\partial x}.
\]
where $v_\tau(t,x)$ satisfies the KBE:
\[
\left\{
\begin{array}{ll}
	\partial_t v_\tau (t,x) = -a(t,x) \partial_x v_\tau (t,x) -\frac{1}{2} b^2 (t,x) \partial_{xx} v_{\tau} (t,x),\\
	v_\tau (T,\cdot) =0, \quad x<K,\\
	v_\tau (t,K) =1.
\end{array}
\right.
\]

$\red{\textbf{Note:}}$ The issue with numerical differentiation.  \[
f'(x) \approx \frac{f(x+h)-f(x)}{h}
\]
- Note that this approximation has two sources of errors: truncation error and roundoff error.\\
- Truncation error can be reduced by decreasing the step size $h$, however, if \textbf{$h$ becomes too small, loss of significance can become a factor}.\\
- The usual approach to get an adequate result is to try a step size $h$ large enough that the loss of significance in the differences begins dominate.
\end{frame}


\begin{frame}{DW example: PDE discretization with the finer mesh}
	\begin{equation}
		\begin{split}
			\mbox{\textbf{PDE domain:}}& \quad (t,x)\in [0, 1]\times (-3,0.5],\\
			\xi(t_n,x_i) &\approx \sigma \frac{\log u_{i+1}^n - \log u_{i-1}^n}{2 \Delta x}
		\end{split}
	\end{equation}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolDWsig0_05_pdeNx5000Nt1000.png}
		\includegraphics[height=4.5cm, width=5.7cm]{OptContDWsig0_05_pdeNx5000Nt1000.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: PDE discretization with the coarser mesh}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolDWsig0_05_pdeNx500Nt100.png}
		\includegraphics[height=4.5cm, width=5.7cm]{OptContDWsig0_05_pdeNx500Nt100.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: PDE discretization with the coarsest mesh}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolDWsig0_05_pdeNx50Nt10.png}
		\includegraphics[height=4.5cm, width=5.7cm]{OptContDWsig0_05_pdeNx50Nt10.png}
	\end{figure}

$\bullet$ We observe that depending on the pde discretization, the scale of the optimal control varies due to the loss of significance. Therefore, we decided to try alternative solution via HJB equation. 
\end{frame}

\begin{frame}
  $\bullet$ Alternatively, the optimal control is defined by
  \[
  \xi^*(t,x) = - b(t,x) \frac{\partial u_\tau(t,x)}{\partial x}.
  \]
  where $u_\tau(t,x)$ satisfies the HJB:
  \[
  \left\{
  \begin{array}{ll}
  	\partial_t u_\tau (t,x) = -a(t,x) \partial_x u_\tau (t,x) -\frac{1}{2} b^2 (t,x) \partial_{xx} u_{\tau} (t,x)+\frac{1}{2} b^2 (t,x) \partial_x u_\tau^2 (t,x),\\
  	u_\tau (T,\cdot) =-\log 0 = +\infty, \quad x<K,\\
  	u_\tau (t,K) = -\log 1 = 0.
  \end{array}
  \right.
  \]
  $\bullet$ Relation between KBE and HJB solutions is $v_\tau(t,x)=e^{-u_\tau(t,x)}$
  \begin{equation}
  	\begin{split}
  		\mbox{\textbf{Original condition:}}& \quad v_{\tau}(T,x)=\textbf{1}_{\{x\geq K\}},\\
  		\mbox{\textbf{Exp. smoothing:}}& \quad v_{\tau}(T,x)=\exp(\mathbf{c}(x-K)).
  	\end{split}
  \end{equation}
\end{frame}


\begin{frame}{DW example: Exponen. smoothing with $c=40$ }
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{PDEsolTendExpc40dx0005.png}
		\includegraphics[height=4.5cm, width=5.7cm]{PDEsolT0Expc40dx0005.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: Exponen. smoothing with $c=40$ }
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{controlKBEc40dx0005.png}
		\includegraphics[height=4.5cm, width=5.7cm]{controlHJBc40dx0005.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: Exponen. smoothing with $c=20$ }
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{PDEsolTendExpc20dx0005.png}
		\includegraphics[height=4.5cm, width=5.7cm]{PDEsolT0Expc20dx0005.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: Exponen. smoothing with $c=20$ }
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{controlKBEc20dx0005.png}
		\includegraphics[height=4.5cm, width=5.7cm]{controlHJBc20dx0005.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: Exponen. smoothing with $c=10$ }
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{PDEsolTendExpc10dx0005.png}
		\includegraphics[height=4.5cm, width=5.7cm]{PDEsolT0Expc10dx0005.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: Exponen. smoothing with $c=10$ }
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{controlKBEc10dx0005.png}
		\includegraphics[height=4.5cm, width=5.7cm]{controlHJBc10dx0005.png}
	\end{figure}
\footnotesize{$\bullet$ We verified that both approaches produce similar optimal control and exponential smoothing of final condition gives stable results for different pde disctezations. \\
$\bullet$ However, the optimal control depends on smoothing factor and we propose one idea how to define the appropriate smoothing factor later.\\
$\bullet$ We also tested final condition for HJB giving different finite values and compared with KBE with indicator function at the final. However, the optimal control produced by HJB doesn't match with corresponding KBE one. }
\end{frame}

\begin{frame}{Smoothing factor}
\footnotesize{$\bullet$ Erik's suggestion: to compute the optimal control for the last time step, at time $T-dt_{EM}$, and,  by numerical integration, compute the corresponding value of the KBE solution
	 at time $T-dt_{EM}$. So we can use this solution as a final condition for KBE starting from time $T-dt_{EM}$.  \\
	 $\bullet$ However, Erik's solution depends on E-M timestep and if it gets smaller, its solution gets down to zero very quickly.}
	 \begin{figure}[h!]

	 	\hspace*{-1cm}
	 	\includegraphics[height=4.5cm, width=5.7cm]{KBEsolUsedIndFun.png}
	 	\includegraphics[height=4.5cm, width=5.7cm]{KBEsolUsedErikSoldt01.png}
	 	\caption{Left: using Ind.function at time $T$; Right: using Erik's solution at time $T-dt_{EM}$ with $dt_{EM}=0.1$.}
	 \end{figure}
\end{frame}

\begin{frame}{DW: KBE solution with $dx=0.005, dt=dx^2$}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolUsedIndFunsemilog.png}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolUsedErikSoldt01semilog.png}
		\caption{Left: using Ind.function at time $T$; Right: using Erik's solution at time $T-dt$ with $dt_{EM}=0.1$.}
	\end{figure}
\end{frame}

\begin{frame}{DW: Optimal Control with $dx=0.005, dt=dx^2$}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{ControlUsedIndFun.png}
		\includegraphics[height=4.5cm, width=5.7cm]{ControlUsedErikSoldt01.png}
		\caption{Left: using Ind.function at time $T$; Right: using Erik's solution at time $T-dt$ with $dt_{EM}=0.1$.}
	\end{figure}
\end{frame}

\begin{frame}{DW: Optimal Control $dx=0.005, dt=dx^2, dt_{EM}=0.1$}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolExpSm15ErikSoldt01.png}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolExpSm15ErikSoldt01semilog.png}
		\includegraphics[height=4cm, width=5.7cm]{ControlExpSm15dt01.png}
	\end{figure}
\end{frame}

\begin{frame}{DW: Optimal Control $dx=0.005, dt=dx^2, dt_{EM}=0.01$}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolExpSm40ErikSoldt001.png}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolExpSm40ErikSoldt001semilog.png}
		\includegraphics[height=4cm, width=5.7cm]{ControlExpSm40dt001.png}
	\end{figure}
\end{frame}

\begin{frame}{DW: Optimal Control $dx=0.005, dt=dx^2, dt_{EM}=0.001$}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolExpSm100ErikSoldt0001.png}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolExpSm100ErikSoldt0001semilog.png}
		\includegraphics[height=4cm, width=5.7cm]{ControlExpSm100dt0001.png}
	\end{figure}
\end{frame}


\begin{frame}{Double Well: IS with respect to $W_t$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,0.2)}$}}\\
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				0 & 0.04056& 0.04061& 7.17\% & 0.18\\ 
				\hline
				0.5 & 0.0041 & 0.0043 & 4.70\% & 4.05 \\ 
				\hline
				1 & 1.3e-04 & 1.45e-04 & 3.69\% & 194.9 \\ 
				\hline
				1.2 & 1.00e-05 & 2.37e-05 & 6.3\% & 403.8 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, Euler timestep $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Double Well: IS with respect to both $W_t$ and $\rho_0$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,0.2)}$}}\\
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				0 & 0.04102& 0.04076& 3.70\% & 0.66\\ 
				\hline
				0.5 & 0.00393& 0.00416& 1.85\% & 26.7 \\ 
				\hline
				1 & 1.40e-04 & 1.55e-04 & 2.85\% & 304.6 \\ 
				\hline
				1.2 & 1.00e-05 & 2.36e-05 & 2.4\% & 2884.2 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, Euler timestep $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Double Well: IS with respect to $W_t$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,0.5)}$}}\\
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				0.5 & 0.03215& 0.03451& 9.93\% & 0.11\\ 
				\hline
				1 & 0.0048 & 0.0053 &32.9\% & 0.07\\ 
				\hline
				1.5 & 1.8e-04 & 1.39e-04 & 18.08\% & 8.45 \\ 
				\hline
				1.8 & 1.00e-05 & 1.03e-05 & 43.35\% & 19.94 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, Euler timestep $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}


\begin{frame}{Double Well: IS with respect to both $W_t$ and $\rho_0$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,0.5)}$}}\\
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				0.5 & 0.03262& 0.03206& 1.88\% & 3.29\\ 
				\hline
				1 & 0.0051& 0.0048& 3.00\% & 8.85\\ 
				\hline
				1.5 & 1.10e-04 & 1.49e-04 & 5.04\% & 100.9 \\ 
				\hline
				1.8 & 2.00e-05 & 9.11e-06 & 9.44\% & 472.7\\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, Euler timestep $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Double Well: IS with respect to $W_t$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,1)}$}}\\
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				1.5 & 0.01331& 0.01190& 33.99\% & 0.03\\ 
				\hline
				1 & 0.0023 & 0.0016 &100.9\% & 0.02\\ 
				\hline
				2.5 & 3.9e-04 & 2.4e-05 & 63.95\% & 4.1 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, Euler timestep $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Double Well: IS with respect to both $W_t$ and $\rho_0$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,1)}$}}\\
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				1.5 & 0.01286& 0.01316& 13.94\% & 0.15 \\ 
				\hline
				2 & 0.0022& 0.0016& 16.99\% & 0.85\\ 
				\hline
				2.5 & 2.8e-04 & 8.1e-05 & 21.15\% & 10.56\\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, Euler timestep $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Double Well: IS with respect to both $W_t$ and $\rho_0$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,1)}$}}\\
		\large{ \textbf{Optimal control is upper bounded by 1 for all time}}\\
		
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				1.5 & 0.01336& 0.01334& 1.17\% & 20.6 \\ 
				\hline
				2 & 0.00226& 0.00234& 1.84\% & 48.32\\ 
				\hline
				2.5 & 3.0e-04 & 3.5e-04 & 1.59\% & 431.9\\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, Euler timestep $\Delta t= 0.01$, $M= 10^5$, $smf=40$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Double Well: IS with respect to both $W_t$ and $\rho_0$}
	\large{ \textbf{Initial condition is fixed:  $\red{x_0=0}$}}\\
	\large{ \textbf{Optimal control is upper bounded by 1 for all time}}\\
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				1 & 0.0547& 0.0542& 1.16\% & 4.97 \\ 
				\hline
				1.5 & 0.00121& 0.00116& 4.66\% & 15.2\\ 
				\hline
		        1.7 & 1.2e-04 & 1.4e-04 & 10.30\% & 25.28\\ 
				\hline
			    1.8 & 6.0e-05 & 5.0e-05 & 15.93\% & 30.2\\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, Euler timestep $\Delta t= 0.01$, $M= 10^5$, $smf=40$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Double Well: IS with respect to $\rho_0$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,1)}$}}\\
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				1.5 & 0.01307& 0.01341& 2.37\% & 5.05 \\ 
				\hline
				2 & 0.0025& 0.0023& 1.75\% & 54.04\\ 
				\hline
				2.5 & 4.6e-04 &  3.5e-04 & 1.34\% & 610.2\\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, Euler timestep $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\footnotesize{\red{\textbf{Remarks:}}\\
$\bullet$	If $\sigma_0$ is relatively small, it is better to perform IS with respect to both $W_t$ and $\rho_0$.\\
$\bullet$	If $\sigma_0$ is relatively large, we obtain variance reduction via IS only with respect to $\rho_0$. \\}
\end{frame}



\begin{frame}{Constrained Hamiltonian}
	$\bullet$ If we put constraints on the optimal control:
	\begin{equation}
   \hat{\xi}(t,x)	=\begin{cases} 
			L, \qquad \mbox{ if } \xi^*(t,x) <L, \\
			\xi^* , \qquad \mbox{if } \xi^*(t,x) \in [L,U],\\
			U,  \qquad \mbox{ if } \xi^*(t,x) >U.
		\end{cases}\,.
	\end{equation}

Then we have to solve the HJB equation:
 \[
 \partial_t \gamma + H(x, \partial_x \gamma, \partial_{xx} \gamma) = 0
 \]
 where 
	\[ H=
	\left\{
	\begin{array}{ll}
		(a-bL) \partial_x \gamma+\frac{1}{2} b^2 \partial_{xx} \gamma-b^2 (\partial_x \gamma)^2 -\frac{L^2}{2}, \quad  \xi^*(t,x) <L\\
		a \partial_x \gamma+\frac{1}{2} b^2 \partial_{xx} \gamma-\frac{1}{2} b^2 (\partial_x \gamma)^2, \quad \xi^*(t,x) \in [L,U], \\
		(a-bU) \partial_x \gamma+\frac{1}{2} b^2 \partial_{xx} \gamma-b^2 (\partial_x \gamma)^2 -\frac{U^2}{2}, \quad  \xi^*(t,x) >U.
	\end{array}
	\right.
	\]
\end{frame}

\begin{frame}{Langevin dynamics}
		Two dimensional Langevin dynamics
		\begin{equation}\label{Langevin}
			\begin{cases} 
				dX_t= V_t dt, \\
				dV_t = -U'(X_t)dt-\kappa V_t dt + (2\kappa T)^{1/2}dW_t, \;
			\end{cases}\,
		\end{equation}
		where the double well potential $U(x)=x^2/4+1/(4x^2+2)$.\\
		$\bullet$ \textbf{Setting:} $P_1=[0 \; 1]$.
\end{frame}

\begin{frame}{Langevin: IS with respect to $W_t$}
	\large{ \textbf{Initial condition is random: $\red{\textbf{x}_0 \sim N([0\;0], [0.5\; 0; 0\; 0.5])}$}}\\
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				2.5 & 9.38e-04 & 7.71e-04 & 9.30\% & 5.76 \\ 
				\hline
				3 & 4.74e-05 & 5.22e-05 & 11.49\% & 55.67 \\ 
				\hline
				3.5 & 1.04e-06 & 2.76e-06 & 13.71\% & 741\\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\kappa=2^{-5}\pi^2, T=1$. Simulation parameters: Final time $N=1$, $\Delta t= 0.01$, $M=10^5$. Exp. smoothing factor $smf=25$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Langevin: IS with respect to  $\rho_0$}
	\large{ \textbf{Initial condition is random: $\red{\textbf{x}_0 \sim N([0\;0], [0.5\; 0; 0\; 0.5])}$}}\\
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				2.5 & 9.38e-04 & 0.0042 & 4.88\% & 3.86 \\ 
				\hline
				3 & 4.74e-05 & 4.41e-04 & 10.11\% & 8.52\\ 
				\hline
				3.5 & 1.04e-06 & 3.09e-05 & 30.93\% & 12.98 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\kappa=2^{-5}\pi^2, T=1$. Simulation parameters: Final time $N=1$, $\Delta t= 0.01$, $M=10^5$. Exp. smoothing factor $smf=25$.}
		\label{table:DWerror}
	\end{table}
\end{frame}


\begin{frame}{Langevin: IS with respect to both $W_t$ and $\rho_0$}
	\large{ \textbf{Initial condition is random: $\red{\textbf{x}_0 \sim N([0\;0], [0.5\; 0; 0\; 0.5])}$}}\\
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				2.5 & 9.38e-04 & 0.0044 & 89.47\% & 0.01 \\ 
				\hline
				3 & 4.74e-05 & 2.45e-04 & 21.52\% & 3.38 \\ 
				\hline
				3.5 & 1.04e-06 & 2.32e-05 & 40.41\% & 10.2 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\kappa=2^{-5}\pi^2, T=1$. Simulation parameters: Final time $N=1$, $\Delta t= 0.01$, $M=10^5$. Exp. smoothing factor $smf=25$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Langevin: IS with respect to $W_t$}
	\large{ \textbf{Initial condition is random: $\red{\textbf{x}_0 \sim N([0\;0], [0.2\; 0; 0\; 0.2])}$}}\\
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				2&  0.0041  & 0.0045 & 3.27\% & 7.99 \\ 
				\hline
				2.5 & 3.00e-04  & 3.58e-04 & 4.03\% & 66.1\\ 
				\hline
				3 &  2.09e-05 & 1.47e-05 & 2.42\% & 4469\\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\kappa=2^{-5}\pi^2, T=1$. Simulation parameters: Final time $N=1$, $\red{\Delta t= 0.1}$, $M=10^5$. Exp. smoothing factor $smf=10$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Langevin: IS with respect to  $\rho_0$}
	\large{ \textbf{Initial condition is random: $\red{\textbf{x}_0 \sim N([0\;0], [0.2\; 0; 0\; 0.2])}$}}\\
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{\rho_0}}$ & $\red{\epsilon_{st}^{PDE,\rho_0}}$ &  \\ 
				\hline
				2 & 0.0041 & 0.0065 & 5.96\% & 1.67 \\ 
				\hline
				2.5 & 3.00e-04 & 5.51e-04 & 18.55\% & 2.02\\ 
				\hline
				3 & 2.09e-05 & 3.39e-05 & 69.44\% & 2.35 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\kappa=2^{-5}\pi^2, T=1$. Simulation parameters: Final time $N=1$, $\red{\Delta t= 0.1}$, $M=10^5$. Exp. smoothing factor $smf=10$.}
		\label{table:DWerror}
	\end{table}
\end{frame}


\begin{frame}{Langevin: IS with respect to both $W_t$ and $\rho_0$}
	\large{ \textbf{Initial condition is random: $\red{\textbf{x}_0 \sim N([0\;0], [0.2\; 0; 0\; 0.2])}$}}\\
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{both}}$ & $\red{\epsilon_{st}^{PDE,both}}$ &  \\ 
				\hline
				2 &  0.0041  & 0.0071 & 3.77\% & 3.8 \\ 
				\hline
				2.5 & 3.00e-04  & 6.48e-04 & 3.26\% & 55.8 \\ 
				\hline
				3 &  2.09e-05 & 3.56e-05 & 3.17\% & 1075 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\kappa=2^{-5}\pi^2, T=1$. Simulation parameters: Final time $N=1$, $\red{\Delta t= 0.1}$, $M=10^5$. Exp. smoothing factor $smf=10$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Lorenz 96: IS with respect to both $W_t$ and $\rho_0$}
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{both}}$ & $\red{\epsilon_{st}^{PDE,both}}$ &  \\ 
				\hline
				2 &  0.0480 & 0.0472 & 2.27\% & 1.5 \\ 
				\hline
				2.5 & 0.0018 & 0.0017 & 5.36\% & 8.98 \\ 
				\hline
				2.7 & 3.4e-04 & 3.02e-04 & 10.02\% & 12.66 \\ 
				\hline
				3 &  2.0e-05 & 1.75e-05 & 36.2\% & 16.75 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\sigma=0.5, F=2, d=10$. Simulation parameters: Final time $N=1$, $\red{\Delta t= 0.01}$, $M=10^5$, $\red{\textbf{x}_0 \sim N([0\;...\;0], 0.1\textbf{I})}$. Exp. smoothing factor $smf=15$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Lorenz 96: IS with respect to $W_t$}
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{both}}$ & $\red{\epsilon_{st}^{PDE,both}}$ &  \\ 
				\hline
				2.5 &  0.00177 & 0.00184& 19.99\% & 0.52\\ 
				\hline
				2.7 & 3.1e-04 & 3.06e-04 & 10.82\% & 10.7 \\ 
				\hline
				3 &  3.0e-05 & 1.35e-05 & 17.6\% & 91.5 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\sigma=0.5, F=2, d=10$. Simulation parameters: Final time $N=1$, $\red{\Delta t= 0.01}$, $M=10^5$, $\red{\textbf{x}_0 \sim N([0\;...\;0], 0.1\textbf{I})}$. Exp. smoothing factor $smf=15$.}
		\label{table:DWerror}
	\end{table}
\red{\textbf{Remarks:}} When $F$ gets larger, Lorenz' 96 produces very chaotic dynamics. A slight change in the initial condition or in the drift, produce significantly different samples compared to crude Monte Carlo samples. So we conclude change of measures with respect to both $\rho_0$ abd $W_t$ do not work to reduce the variance.
\end{frame}


\end{document}