% Modelo de slides para projetos de disciplinas do Abel
\documentclass[10pt]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}
\usepackage[numbers,sort&compress]{natbib}
\bibliographystyle{plainnat}
\usepackage{adjustbox}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{multirow}
\usepackage{xspace}
\usepackage{mathtools}
\usepackage{wrapfig}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\green}[1]{{\color{green} #1}}
\newcommand{\magenta}[1]{{\color{magenta} #1}}
\title{Rare events and filtering}
 \subtitle{A semi-analytical solution of KBE}
 \date{\today}
\date{Sept, 2023}
\author{Gaukhar Shaimerdenova}
\institute{AMCS, KAUST}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\V}{\mathbb{V}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\bu}{\boldsymbol{u}}
\newcommand{\bv}{\boldsymbol{v}}
\newcommand{\bx}{\boldsymbol{x}}
\newcommand{\ba}{\boldsymbol{a}}
\newcommand{\bb}{\boldsymbol{b}}
\newcommand{\bX}{\boldsymbol{X}}
\newcommand{\bK}{\boldsymbol{K}}
\newcommand{\brho}{\boldsymbol{\rho}}
\newcommand{\bmu}{\boldsymbol{\mu}}
\newcommand{\bSigma}{\boldsymbol{\Sigma}}
\newcommand{\bW}{\boldsymbol{W}}

%%%%%%%%   Macros we will need for this paper %%%%%%%%%%%%%%%

\newcommand{\norm}[1]{\left\| #1 \right\|}
\newcommand{\abs}[1]{\left| #1 \right|}
\newcommand{\ceil}[1]{\left \lceil #1 \right \rceil}
\newcommand{\prt}[1]{\left( #1 \right)}
\newcommand{\cost}[1]{\mathrm{Cost}}
\newcommand{\Tol}{\mathrm{TOL}}

% mathbb font letters 
\newcommand{\bF}{\mathbb{F}}
\newcommand{\bN}{\mathbb{N}}
\newcommand{\bP}{\mathbb{P}}
\newcommand{\bR}{\mathbb{R}}
\newcommand{\bZ}{\mathbb{Z}}
\newcommand{\bT}{\mathbf{T}}

% mathcal capital letters
\newcommand{\cB}{\mathcal{B}}
\newcommand{\cF}{\mathcal{F}}
\newcommand{\cO}{\mathcal{O}}
\newcommand{\cI}{\mathcal{I}}
\newcommand{\cJ}{\mathcal{J}}
\newcommand{\cK}{\mathcal{K}}
\newcommand{\cL}{\mathcal{L}}
\newcommand{\cN}{\mathcal{N}}
\newcommand{\cT}{\mathcal{T}}

%bold letters
\newcommand{\bw}{\boldsymbol{w}}

% ensemble notation 
\newcommand{\vBar}{\bar v}
\newcommand{\vHat}{\hat{v}}
\newcommand{\vBarHat}{\hat{\bar{v}}}
\newcommand{\meanHatMC}[1]{\hat{m}^{\rm{MC}}_{#1}}
\newcommand{\covHatMC}[1]{\widehat{C}^{\rm{MC}}_{#1}}
\newcommand{\yTilde}[1]{\tilde{y}_{#1}}
\newcommand{\meanHatML}[1]{\hat{m}^{\rm{ML}}_{#1}}
\newcommand{\covHatML}[1]{\widehat{C}^{\rm{ML}}_{#1}}
\newcommand{\kMC}[1]{K^{\rm{MC}}_{#1}}


% statistics notation
\newcommand{\Prob}[1]{\bP_{#1}}
\newcommand{\Ex}[1]{\E \left[ #1 \right]}
\newcommand{\Var}[1]{\V \left[ #1 \right]}
\newcommand{\Cov}{\overline{\mathrm{Cov}}}
\newcommand{\D}{\mathrm{D}}
\newcommand{\Ind}[2]{\textbf{1}_{\{#2\}}}

% colors
\newcommand{\blue}[1]{{\color{blue} #1}}


\begin{document}

\maketitle

\begin{frame}{}
The KBE with a Dirichlet boundary condition on the domain $(-\infty, \cK] \times [0,T]$:
\begin{equation}\label{eq:BKEpde}
	\left\{
	\begin{array}{ll}
		\frac{\partial \gamma}{\partial t}  = -a	\frac{\partial \gamma}{\partial x}   -\frac{1}{2} b^2 	\frac{\partial^2 \gamma}{\partial x},\\
		\gamma(x, T) =0, \quad x<\cK,\\
		\gamma(\cK, t) =1, \quad t\leq T,\\
		\gamma(-\infty, t) =0, \quad t \leq T.
	\end{array}
	\right.
\end{equation}
By a change of variable
\[
t'=T-t, \quad x'=\cK-x,
\]
we get 
\begin{equation}\label{eq:FKEpde}
	\left\{
	\begin{array}{ll}
		-\frac{\partial \gamma}{\partial t'}  = a	\frac{\partial \gamma}{\partial x'}   -\frac{1}{2} b^2 	\frac{\partial^2 \gamma}{\partial x'} \iff \frac{\partial \gamma}{\partial t'} + a	\frac{\partial \gamma}{\partial x'}  = \frac{1}{2} b^2 	\frac{\partial^2 \gamma}{\partial x'}  ,\\
		\gamma(x', 0) =0, \quad x'>0,\\
		\gamma(0, t') =1, \quad t'\leq 0,\\
		\gamma(\infty, t') =0, \quad t' \leq 0.
	\end{array}
	\right.
\end{equation}
\end{frame}

\begin{frame}{}
By F.Harleman, R.Rumer (1963), we have a closed-form solution for~\eqref{eq:FKEpde} given by
\[
\gamma(x',t')=\frac{1}{2}erfc\Bigg(\frac{x'-a t'}{2\sqrt{\frac{b^2}{2}t'}}\Bigg)+\frac{1}{2}e^{\frac{ax'}{b^2/2}}erfc\Bigg(\frac{x'+a t'}{2\sqrt{\frac{b^2}{2}t'}}\Bigg).
\]
Then, back to $(x,t)$, we have the exact solution in the form
\begin{equation}\label{eq:analsol}
	\begin{split}
\gamma(x,t)=&\frac{1}{2}erfc\Bigg(\frac{\cK-x-a (T-t)}{2\sqrt{\frac{b^2}{2}(T-t)}}\Bigg)\\
&+\frac{1}{2}e^{\frac{a(\cK-x)}{b^2/2}}erfc\Bigg(\frac{\cK-x+a (T-t)}{2\sqrt{\frac{b^2}{2}(T-t)}}\Bigg).
	\end{split}
\end{equation}

\end{frame}


\begin{frame}{Optimal Control based on the analytical solution with the freezed coefficients at the corner}
	The PDE solution with frozen coeff. at $(\cK,T)$ is
	\begin{equation}\label{eq:freezcoeffsol}
		\footnotesize
		\begin{split}
			\gamma(x,t)=\frac{1}{2}erfc\Bigg(\frac{\cK-x-a(\cK,T) (T-t)}{\abs{b(\cK,T)}\sqrt{2(T-t)}}\Bigg)+\frac{1}{2}e^{\frac{a(\cK,T)(\cK-x)}{b^2(\cK,T)/2}}erfc\Bigg(\frac{\cK-x+a(\cK,T) (T-t)}{\abs{b(\cK, T)}\sqrt{2(T-t)}}\Bigg).
		\end{split}
	\end{equation}	
   The optimal control based on~\eqref{eq:freezcoeffsol} is
	\begin{equation}\label{eq:optcontfrozencoeff}
	\footnotesize
	\begin{split}
		&\xi(x,t)=b\frac{\partial (\log(\gamma(x,t)))}{\partial x}= \frac{b}{\gamma(x,t)}\frac{\partial (\gamma(x,t))}{\partial x}\\
		&=\frac{b}{\gamma(x,t)} \Bigg[\frac{1}{\abs{b}\sqrt{2\pi (T-t)}}\Big(e^{-(R^{-}(x))^2}+e^{-(R^{+}(x))^2+\frac{2a(K-x)}{b^2}}\Big)-\frac{a}{b^2}e^{\frac{2a(K-x)}{b^2}}erfc(R^{+}(x))\Bigg],
	\end{split}
\end{equation}	
where $R^{\pm}(x):=\frac{K-x\pm a(T-t)}{\abs{b}\sqrt{2(T-t)}}$. 
\end{frame}

\begin{frame}{3D plots for $\gamma(x,t)$ in~\eqref{eq:freezcoeffsol} and $\xi(x,t)$ in~\eqref{eq:optcontfrozencoeff} [$\Delta x=0.01, \Delta t=0.01$]}
	\begin{figure}[h!]
		\centering
		\includegraphics[height=4.1cm, width=5.3cm]{Gamma_analytical.png}
		\includegraphics[height=4.1cm, width=5.3cm]{Gamma_analytical_semilogz.png}
		\includegraphics[height=4.1cm, width=5.3cm]{xi_analytical.png}
	\end{figure}
\end{frame}

\begin{frame}{\small DW: $T=1$, $\cK=0.5$, $b=0.5$}
	\begin{figure}[h!]
		\centering
		\includegraphics[height=4.1cm, width=5.3cm]{AnalOptControl_dt_conv_xmin1.png}
		\includegraphics[height=4.1cm, width=5.3cm]{AnalOptControl_dt_conv_x0.png}
		\includegraphics[height=4.1cm, width=5.3cm]{AnalOptControl_dt_conv_x04.png}
		\includegraphics[height=4.1cm, width=5.3cm]{AnalOptControl_dt_conv_xK.png}
	\end{figure}
\end{frame}

\begin{frame}{\small DW: $T=1$, $\cK=0.5$, $b=0.5$}
	\begin{figure}[h!]
		\centering
		\includegraphics[height=4.3cm, width=5.3cm]{AnalOptControl_dtscaled.png}
		\includegraphics[height=4.3cm, width=5.3cm]{AnalOptControl_dtscaled_semilogy.png}
	\end{figure}
\end{frame}

%\begin{frame}{\small DW: $\cK=0.5$ }
%	\begin{figure}[h!]
%		\centering
%		\includegraphics[height=4.3cm, width=5.3cm]{AnalOptControl_vary_x_dt001_semilogy.png}
%		\includegraphics[height=4.3cm, width=5.3cm]{AnalOptControl_vary_x_dt0001_semilogy.png}
%	\end{figure}
%\end{frame}

\begin{frame}{Optimal control problem of the prelast step}
	\begin{equation}
		\left\{
		\begin{array}{ll}
			X_T=X_0+(a+b\xi)\Delta t+b\Delta W_T,\\
			Y_T=2\xi\Delta W_T + \xi^2 \Delta t,
		\end{array}
		\right.
	\end{equation}
	where $a:=a(X_0)$, $b:=b(X_0)$, $\Delta W_T = \sqrt{\Delta t} \epsilon_T \sim N(0,\Delta t)$.
	
	We want to minimize the 2nd moment
	$$\min_{\xi} \E [\Ind{}{X_T\geq K}e^{-Y_T}|X_0=x].$$
	Note that $X_T\geq K \iff \epsilon_T \geq \frac{K-x-(a+b\xi)\Delta t}{b\sqrt{dt}}$. Then,
	\begin{equation}
		\begin{split}
			&\E [\Ind{}{X_T\geq K}e^{-Y_T}|X_0=x]=\frac{1}{\sqrt{2\pi}}\int_{\frac{K-x-(a+b\xi)\Delta t}{b\sqrt{dt}}}^{\infty} e^{-2\xi\sqrt{\Delta t}z-\xi^2\Delta t-\frac{z^2}{2}}dz\\
			&=\frac{e^{\xi^2\Delta t}}{\sqrt{2\pi}}\int_{\frac{K-x-(a+b\xi)\Delta t}{b\sqrt{dt}}}^{\infty} e^{-(\frac{z}{\sqrt{2}}+\xi\sqrt{2\Delta t})^2}dz=\frac{e^{\xi^2\Delta t}}{\sqrt{\pi}}\int_{\frac{K-x-(a-b\xi)\Delta t}{b\sqrt{dt}}}^{\infty} e^{-s^2}ds\\
			&=\frac{e^{\xi^2\Delta t}}{2}erfc(\frac{K-x-(a-b\xi)\Delta t}{b\sqrt{2\Delta t}}).
		\end{split}
	\end{equation}
\end{frame}

\begin{frame}{Optimal control problem of the prelast step}
	The target function to minimise is 
	$$F(\xi) = \frac{e^{\xi^2\Delta t}}{2}erfc\Big(\frac{K-x-(a-b\xi)\Delta t}{b\sqrt{2\Delta t}}\Big).$$
	The derivative of $F$ is
	\begin{equation}
		\begin{split}
			F'(\xi)&= \xi \Delta t e^{\xi^2 \Delta t} erfc \Big(\frac{K-x-(a-b\xi)\Delta t}{b\sqrt{2\Delta t}}\Big)\\
			&-\sqrt{\frac{\Delta t}{2\pi}} e^{-\frac{(K-x-a\Delta t)^2}{2b^2 \Delta t}-\frac{(K-x-a\Delta t)\xi}{b}+0.5\xi^2\Delta t}.
		\end{split}
	\end{equation}
	The second derivative of $F$ needed for Newton's method is
	\begin{equation}
		\begin{split}
			F''(\xi)&= (\Delta t e^{\xi^2 \Delta t}+2\xi^2\Delta t^2e^{\xi^2\Delta t} ) erfc \Big(\frac{K-x-(a-b\xi)\Delta t}{b\sqrt{2\Delta t}}\Big)\\
			&-\frac{\sqrt{2\Delta t}}{\sqrt{\pi}}\xi\Delta t e^{\xi^2 \Delta t-\frac{(K-x-(a-b\xi)\Delta t)^2}{2b^2\Delta t}}\\
			&-\sqrt{\frac{\Delta t}{2\pi}}\Big(\xi\Delta t -\frac{(K-x-a\Delta t)}{b}\Big)e^{-\frac{(K-x-a\Delta t)^2}{2b^2 \Delta t}-\frac{(K-x-a\Delta t)\xi}{b}+0.5\xi^2\Delta t}.
		\end{split}
	\end{equation}
\end{frame}

\begin{frame}{\small DW: $T=1$, $\cK=0.5$ }
	\begin{figure}[h!]
		\centering
		\includegraphics[height=4.1cm, width=5.3cm]{EMoptimalcontrol.png}
		\includegraphics[height=4.1cm, width=5.3cm]{EM_KBEsolution.png}
		\includegraphics[height=4.1cm, width=5.3cm]{EM_KBEsolutionsemilogy.png}
	\end{figure}
\end{frame}

\begin{frame}{\small DW: $T=1$, $\cK=0.5$ }
	\begin{figure}[h!]
		\centering
		\includegraphics[height=4.1cm, width=5.3cm]{comparisonEM_analPDEcontrol_dt001.png}
		\includegraphics[height=4.1cm, width=5.3cm]{REcomparisonEM_analPDEcontrol_dt001.png}
		\centering
		\includegraphics[height=4.1cm, width=5.3cm]{comparisonEM_analPDEcontrol_dt0001.png}
		\includegraphics[height=4.1cm, width=5.3cm]{REcomparisonEM_analPDEcontrol_dt0001.png}
	\end{figure}
\end{frame}

\end{document}