% Modelo de slides para projetos de disciplinas do Abel
\documentclass[10pt]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}
\usepackage[numbers,sort&compress]{natbib}
\bibliographystyle{plainnat}
\usepackage{adjustbox}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{multirow}
\usepackage{xspace}
\usepackage{mathtools}

\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\magenta}[1]{{\color{magenta} #1}}
\title{Rare events and filtering}
 \subtitle{importance sampling with respect to $W$\\ and with both $W$ and $\rho_0$}
 \date{\today}
\date{October, 2022}
\author{Gaukhar Shaimerdenova}
\institute{AMCS, KAUST}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\V}{\mathbb{V}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\bu}{\boldsymbol{u}}
\newcommand{\bv}{\boldsymbol{v}}
\newcommand{\bx}{\boldsymbol{x}}
\newcommand{\ba}{\boldsymbol{a}}
\newcommand{\bb}{\boldsymbol{b}}
\newcommand{\bX}{\boldsymbol{X}}
\newcommand{\bK}{\boldsymbol{K}}
\newcommand{\brho}{\boldsymbol{\rho}}
\newcommand{\bmu}{\boldsymbol{\mu}}
\newcommand{\bSigma}{\boldsymbol{\Sigma}}
\newcommand{\bW}{\boldsymbol{W}}

\begin{document}

\maketitle

\begin{frame}[fragile]{Problem setup in 1D}
	We consider the general SDE of the form
	\begin{equation}\label{sde}
		\begin{cases} 
			dX_t= a(t, X_t)dt+b(t, X_t)dW_t^{\mathbb{P}}, \quad 0<t<T, \\
			X_{0}\sim \rho_0.
		\end{cases}\,
	\end{equation}
	\red{\textbf{Objective:}} For a given threshold $K>0$, to track the probability:
	\[
	\alpha:=P(\max_{0\leq t\leq T} X_t\geq K).
	\] 
	\textbf{Notation:} $M^T:=\max_{0\leq t\leq T} X_t$.
	
	Let us define the process $W_t^{\mathbb{Q}}=W_t^{\mathbb{P}}-\int_{0}^{t} \xi(s,X_s)ds$ and substitute in~\eqref{sde}:
	\begin{equation*}
		\begin{cases}
			dX_t=  (a(t, X_t)dt+b(t, X_t)\xi(t,X_t))dt+b(t, X_t)dW_t^{\mathbb{Q}},\\
			X_{0} \sim \rho_0.
		\end{cases}\,
	\end{equation*}
   $\bullet$ Girsanov's theorem states that $(W_t^\mathbb{Q})_{t\geq}0$  is a standard Brownian motion under the probabilty measure $Q$ with the likelihood ratio
	\[
	\frac{d\mathbb{P}}{d\mathbb{Q}}=e^{-\int_{0}^{t} \xi(s,X_s)dW_s^{\mathbb{Q}}-\frac{1}{2}\int_{0}^{t}\xi^2(s,X_s)ds}.
	\]
\end{frame}

\begin{frame}{Importance sampling}
	 $\bullet$  Since the distribution of $W^{\mathbb{Q}}$ under $\mathbb{Q}$ is the same as the distribution of $W^{\mathbb{P}}$ under $\mathbb{P}$, we can work with the following SDE under $\mathbb{P}$:
	\begin{equation*}
		\begin{cases}
			dX_t^{\xi}=  (a(t, X_t^{\xi})dt+b(t, X_t^{\xi})\xi(t,X_t^{\xi}))dt+b(t, X_t^{\xi})dW_t^{\mathbb{P}}, \quad 0<t<T\\
			X_{0}^{\xi} \sim \rho_0.
		\end{cases}\,
	\end{equation*}
$\bullet$ 	Note we have two sources of randomness in the quantity of interest:
\[
\alpha= \mathbb{E}_{\rho_0 \times \mathbb{P}}[\textbf{1}_{\{M^T\geq K\}}]
\]
$\bullet$ We apply an \textbf{importance sampling with respect to $W_t^{\mathbb{P}}$:}
 \begin{equation}
 	\begin{split}
 		\mathbb{E}_{\rho_0 \times \mathbb{P}}[\textbf{1}_{\{M^T\geq K\}}] =\E_{\rho_0 \times \mathbb{Q}} \Big[ \textbf{1}_{\{M^T\geq K\}} \frac{d\mathbb{P}}{d\mathbb{Q}} \Big]
 	\end{split}
 \end{equation}
   	
\end{frame}

\begin{frame}{Connection with exit time}
 $\bullet$ Let us define the exit time (a first passage time) for a given domain $D$ by
\[
\tau(x,t) = \inf\{s>t: X_s\notin D \mbox{ and } X_t=x\}.
\]
 Note the exit probability $P(\tau(x,t) \leq T):=u_{\tau}(t,x)$ solves the Kolmogorov Backward equation (KBE) 
with Dirichlet boundary condition:
\[
\left\{
\begin{array}{ll}
	\partial_t u_\tau (t,x) = -a(t,x) \partial_x u_\tau (t,x) -\frac{1}{2} b^2 (t,x) \partial_{xx} u_{\tau} (t,x),\\
	u_\tau (T,\cdot) =0, \quad x\in D,\\
	u_\tau (\cdot,x) =1, \quad x\in  (0,T)\times \partial D 
\end{array}
\right.
\]
$\bullet$  If we consider the domain $D:=\{X_t: X_t< K\}$, observe the connection between the events
\[
\{M^T\geq K\}=\{\tau(X_0,0)\leq T\}.
\]
Then we can represent the QoI as
\begin{equation*}
	\begin{split}
		\mathbb{E}_{\rho_0 \times \mathbb{P}}[\textbf{1}_{\{M^T\geq K\}}] &=	\mathbb{E}[\textbf{1}_{\{\tau(X_0,0) \leq T\}}]=	\mathbb{E}_{\rho_0 \times \mathbb{P}}[\textbf{1}_{\{X_{\tau \wedge T}=K\}}]\\
		&=\mathbb{E}_{\rho_0 \times \mathbb{Q}} \Big[ \textbf{1}_{\{X_{\tau \wedge T}^{\xi}=K\}} \frac{d\mathbb{P}}{d\mathbb{Q}} \Big].
	\end{split}
\end{equation*}

   


\end{frame}

\begin{frame}{Optimal control}
$\bullet$ Our aim now is to find a control $\xi$ that minimizes the variance of the QoI (i.e. sufficient to minimize the second moment): 
\[
\min_{\xi} \mathbb{E}_{\rho_0 \times \mathbb{Q}} \Big[\textbf{1}_{\{X_{\tau(x_0,0) \wedge T}^{\xi}=K\}}  e^{-2\int_{0}^{\tau(x_0,0) \wedge T} \xi(s,X_s^{\xi})dW_s^{\mathbb{Q}}-\int_{0}^{\tau(x_0,0) \wedge T}\xi^2(s,X_s^{\xi})ds} \Big | X_0^{\xi}=x_0] 
\]

$\bullet$ Let us define the value function that minimizes the second moment  of the MC estimator:
\begin{equation*}
	\begin{split}
&u_\tau(t,x)=\\
&\min_{\xi} \mathbb{E}_{\rho_0 \times \mathbb{Q}} \Big[\textbf{1}_{\{X_{\tau(x,t) \wedge T}^{\xi}=K\}}  e^{-2\int_{t}^{\tau(x,t) \wedge T} \xi(s,X_s^{\xi})dW_s^{\mathbb{Q}}-\int_{t}^{\tau(x,t) \wedge T}\xi^2(s,X_s^{\xi})ds} \Big | X_t^{\xi}=x] 
	\end{split}
\end{equation*}

$\bullet$ $v^2_\tau(t,x)=u_\tau(t,x)$ satisfies the KBE with the optimal control (Theorem 2, C.Hartmann et.al.)
\[
\xi^*(t,x) = b(t,x) \frac{\partial \log v_\tau(t,x)}{\partial x}.
\]
\end{frame}


\begin{frame}{Numerical differentiation}
\[
f'(x) \approx \frac{f(x+h)-f(x)}{h}
\]
Note that this approximation has two sources of errors: truncation error and roundoff error.\\
Truncation error can be reduced by decreasing the step size $h$, however, if \textbf{$h$ becomes too small, loss of significance can become a factor}.\\
The usual approach to get an adequate result is to try a step size $h$ large enough that the loss of significance in the differences begins dominate.
\end{frame}


\begin{frame}{DW example: PDE discretization with the finer mesh}
	\begin{equation}
		\begin{split}
			\mbox{\textbf{PDE domain:}}& \quad (t,x)\in [0, 1]\times (-3,0.5],\\
			\xi(t_n,x_i) &\approx \sigma \frac{\log u_{i+1}^n - \log u_{i-1}^n}{2 \Delta x}
		\end{split}
	\end{equation}
	\begin{figure}[h!]
			\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolDWsig0_05_pdeNx5000Nt1000.png}
		\includegraphics[height=4.5cm, width=5.7cm]{OptContDWsig0_05_pdeNx5000Nt1000.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: PDE discretization with the coarser mesh}
	\begin{equation}
		\begin{split}
			\mbox{\textbf{PDE domain:}}& \quad (t,x)\in [0, 1]\times (-3,0.5] .
		\end{split}
	\end{equation}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolDWsig0_05_pdeNx500Nt100.png}
		\includegraphics[height=4.5cm, width=5.7cm]{OptContDWsig0_05_pdeNx500Nt100.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: PDE discretization with the coarsest mesh}
	\begin{equation}
		\begin{split}
			\mbox{\textbf{PDE domain:}}& \quad (t,x)\in [0, 1]\times (-3,0.5] .
		\end{split}
	\end{equation}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolDWsig0_05_pdeNx50Nt10.png}
		\includegraphics[height=4.5cm, width=5.7cm]{OptContDWsig0_05_pdeNx50Nt10.png}
	\end{figure}
\end{frame}



\begin{frame}{Double Well: IS with respect to $W_t$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,0.2)}$}}\\
	\large{ \textbf{Final condition: } Indicator function}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				0 & 0.04056& 0.04061& 7.17\% & 0.18\\ 
				\hline
				0.5 & 0.0041 & 0.0043 & 4.70\% & 4.05 \\ 
				\hline
				1 & 1.3e-04 & 1.45e-04 & 3.69\% & 194.9 \\ 
				\hline
				1.2 & 1.00e-05 & 2.37e-05 & 6.3\% & 403.8 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, Euler timestep $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Double Well: IS with respect to both $W_t$ and $\rho_0$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,0.2)}$}}\\
	\large{ \textbf{Final condition: } Indicator function}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				0 & 0.04102& 0.04076& 3.70\% & 0.66\\ 
				\hline
				0.5 & 0.00393& 0.00416& 1.85\% & 26.7 \\ 
				\hline
				1 & 1.40e-04 & 1.55e-04 & 2.85\% & 304.6 \\ 
				\hline
				1.2 & 1.00e-05 & 2.36e-05 & 2.4\% & 2884.2 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, Euler timestep $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Double Well: IS with respect to $W_t$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,0.5)}$}}\\
	\large{ \textbf{Final condition: } Indicator function}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				0.5 & 0.03215& 0.03451& 9.93\% & 0.11\\ 
				\hline
				1 & 0.0048 & 0.0053 &32.9\% & 0.07\\ 
				\hline
				1.5 & 1.8e-04 & 1.39e-04 & 18.08\% & 8.45 \\ 
				\hline
				1.8 & 1.00e-05 & 1.03e-05 & 43.35\% & 19.94 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, Euler timestep $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}


\begin{frame}{Double Well: IS with respect to both $W_t$ and $\rho_0$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,0.5)}$}}\\
	\large{ \textbf{Final condition: } Indicator function}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				0.5 & 0.03262& 0.03206& 1.88\% & 3.29\\ 
				\hline
				1 & 0.0051& 0.0048& 3.00\% & 8.85\\ 
				\hline
				1.5 & 1.10e-04 & 1.49e-04 & 5.04\% & 100.9 \\ 
				\hline
				1.8 & 2.00e-05 & 9.11e-06 & 9.44\% & 472.7\\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, Euler timestep $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Double Well: IS with respect to $W_t$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,1)}$}}\\
	\large{ \textbf{Final condition: } Indicator function}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				1.5 & 0.01331& 0.01190& 33.99\% & 0.03\\ 
				\hline
				1 & 0.0023 & 0.0016 &100.9\% & 0.02\\ 
				\hline
				2.5 & 3.9e-04 & 2.4e-05 & 63.95\% & 4.1 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, Euler timestep $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Double Well: IS with respect to both $W_t$ and $\rho_0$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,1)}$}}\\
	\large{ \textbf{Final condition: } Indicator function}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				1.5 & 0.01286& 0.01316& 13.94\% & 0.15 \\ 
				\hline
				2 & 0.0022& 0.0016& 16.99\% & 0.85\\ 
				\hline
				2.5 & 2.8e-04 & 8.1e-05 & 21.15\% & 10.56\\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, Euler timestep $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Double Well: IS with respect to $\rho_0$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,1)}$}}\\
	\large{ \textbf{Final condition: } Indicator function}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				1.5 & 0.01307& 0.01341& 2.37\% & 5.05 \\ 
				\hline
				2 & 0.0025& 0.0023& 1.75\% & 54.04\\ 
				\hline
				2.5 & 4.6e-04 &  3.5e-04 & 1.34\% & 610.2\\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, Euler timestep $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Remarks}
$\bullet$	If $\sigma_0$ is relatively small, it is better to perform IS with respect to both $W_t$ and $\rho_0$.\\
$\bullet$	If $\sigma_0$ is relatively large, we obtain variance reduction via IS only with respect to $\rho_0$.\\
$\bullet$   If $\sigma_0$ is large, the mean estimate produced by IS with respect to $W_t$ is not converging to the estimate by MC. If the initial value is fixed, then we don't have such an issue. \\
 $\bullet$  
\end{frame}

\begin{frame}{Final Condition Regularization: $dx=0.0012$ } %dt=0.001
	\begin{equation}
		\begin{split}
			\mbox{\textbf{Original condition:}}& \quad u_{\tau}(T,x)=\textbf{1}_{\{x\geq K\}},\\
			\mbox{\textbf{Hyperbolic tangent:}}& \quad u_{\tau}(T,x)=0.5(\tanh(\mathbf{sm_f}*(x-K))+1),\\
			\mbox{\textbf{Exp. smoothing:}}& \quad u_{\tau}(T,x)=\exp(\mathbf{c}(x-K)).
		\end{split}
	\end{equation}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{FinalCondition.png}
		\includegraphics[height=4.5cm, width=5.7cm]{FinalConditionSemoLogScale.png}
	\end{figure}
\end{frame}




\begin{frame}{DW example: Exponen. smoothing with $c=40$ }
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{PDEsolTendExpc40dx0005.png}
		\includegraphics[height=4.5cm, width=5.7cm]{PDEsolT0Expc40dx0005.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: Exponen. smoothing with $c=40$ }
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{controlKBEc40dx0005.png}
		\includegraphics[height=4.5cm, width=5.7cm]{controlHJBc40dx0005.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: Exponen. smoothing with $c=20$ }
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{PDEsolTendExpc20dx0005.png}
		\includegraphics[height=4.5cm, width=5.7cm]{PDEsolT0Expc20dx0005.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: Exponen. smoothing with $c=20$ }
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{controlKBEc20dx0005.png}
		\includegraphics[height=4.5cm, width=5.7cm]{controlHJBc20dx0005.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: Exponen. smoothing with $c=10$ }
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{PDEsolTendExpc10dx0005.png}
		\includegraphics[height=4.5cm, width=5.7cm]{PDEsolT0Expc10dx0005.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: Exponen. smoothing with $c=10$ }
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{controlKBEc10dx0005.png}
		\includegraphics[height=4.5cm, width=5.7cm]{controlHJBc10dx0005.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: No smoothing of Indicator function}
	
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4cm, width=10cm]{HJBfinalcond.png}
		\includegraphics[height=4cm, width=6cm]{PDEsolNoSmoothdx001.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: No smoothing of Indicator function }
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{controlKBENoSmoothdx001.png}
		\includegraphics[height=4.5cm, width=5.7cm]{controlHJBNoSmooth10dx001.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: No smoothing of Indicator function }
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{HJBsolNoSmooth20dx001.png}
		\includegraphics[height=4.5cm, width=5.7cm]{HJBcontrolNoSmooth20dx001.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: No smoothing of Indicator function }
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{HJBsolNoSmooth10dx001.png}
		\includegraphics[height=4.5cm, width=5.7cm]{HJBcontrolNoSmooth10dx001.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: No smoothing of Indicator function }
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{HJBsolNoSmooth5dx001.png}
		\includegraphics[height=4.5cm, width=5.7cm]{HJBcontrolNoSmooth5dx001.png}
	\end{figure}
\end{frame}


\begin{frame}{DW example: No smoothing of Indicator function }
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{HJBsolNoSmooth1dx001.png}
		\includegraphics[height=4.5cm, width=5.7cm]{HJBcontrolNoSmooth1dx001.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: No smoothing of Indicator function }
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolNoSmoothdx001.png}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEcontrolNoSmoothdx001.png}
	\end{figure}
\end{frame}

\begin{frame}{Conclusion}
	$\bullet$ The optimal control provided by HJB with exponential smoothing shows similar numerical results in variance reduction as we got in the case of KBE. Exp. smoothing factor matters. 
	
	$\bullet$ In the case of no smoothing, a finite approximation of the final condition matters. We observe some variance reduction in large $\sigma_0$, however, there is a divergence in the mean estimate of the rare event between MC and IS. \\
	
\end{frame}

\begin{frame}{DW: KBE solution with $dx=0.005, dt=dx^2$}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolUsedIndFun.png}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolUsedErikSoldt01.png}
		\caption{Left: using Ind.function at time $T$; Right: using Erik's solution at time $T-dt$ with $dt_{EM}=0.1$.}
	\end{figure}
\end{frame}

\begin{frame}{DW: KBE solution with $dx=0.005, dt=dx^2$}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolUsedIndFunsemilog.png}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolUsedErikSoldt01semilog.png}
		\caption{Left: using Ind.function at time $T$; Right: using Erik's solution at time $T-dt$ with $dt_{EM}=0.1$.}
	\end{figure}
\end{frame}

\begin{frame}{DW: Optimal Control with $dx=0.005, dt=dx^2$}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{ControlUsedIndFun.png}
		\includegraphics[height=4.5cm, width=5.7cm]{ControlUsedErikSoldt01.png}
		\caption{Left: using Ind.function at time $T$; Right: using Erik's solution at time $T-dt$ with $dt_{EM}=0.1$.}
	\end{figure}
\end{frame}

\begin{frame}{DW: Optimal Control $dx=0.005, dt=dx^2, dt_{EM}=0.1$}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolExpSm15ErikSoldt01.png}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolExpSm15ErikSoldt01semilog.png}
		\includegraphics[height=4cm, width=5.7cm]{ControlExpSm15dt01.png}
	\end{figure}
\end{frame}

\begin{frame}{DW: Optimal Control $dx=0.005, dt=dx^2, dt_{EM}=0.01$}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolExpSm40ErikSoldt001.png}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolExpSm40ErikSoldt001semilog.png}
		\includegraphics[height=4cm, width=5.7cm]{ControlExpSm40dt001.png}
	\end{figure}
\end{frame}

\begin{frame}{DW: Optimal Control $dx=0.005, dt=dx^2, dt_{EM}=0.001$}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolExpSm100ErikSoldt0001.png}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolExpSm100ErikSoldt0001semilog.png}
		\includegraphics[height=4cm, width=5.7cm]{ControlExpSm100dt0001.png}
	\end{figure}
\end{frame}

\begin{frame}{Langevin dynamics}
	Two dimensional Langevin dynamics
	\begin{equation}\label{Langevin}
		\begin{cases} 
			dX_t= V_t dt, \\
			dV_t = -U'(X_t)dt-\kappa V_t dt + (2\kappa T)^{1/2}dW_t, \;
		\end{cases}\,
	\end{equation}
	where the double well potential $U(x)=x^2/4+1/(4x^2+2)$.\\
	$\bullet$ \textbf{Setting:} $P_1=[0 \; 1]$.
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|ccl|ccl|l|l|}
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{3}{c|}{\textbf{Estimator} }            & \multicolumn{3}{c|}{\textbf{Relative stat. error}}   &\multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{CE,\tilde{\mu}}}$} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ \cline{2-7} 
				& \multicolumn{1}{c|}{$\hat{\alpha}_{MC}$} & \multicolumn{1}{c|}{$\hat{\alpha}_{CE}^{\tilde{\mu}}$} & \multicolumn{1}{c|}{$\red{\hat{\alpha}_{PDE}^{\tilde{\mu},\tilde{\sigma}}}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{MC}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{CE,\tilde{\mu}}$} & \multicolumn{1}{c|}{$\red{\epsilon_{st}^{PDE,\tilde{\mu},\tilde{\sigma}}}$} & & \\ \hline
				2.5               & \multicolumn{1}{c|}{0.00486}     &  \multicolumn{1}{c|}{0.00461} & \multicolumn{1}{c|}{0.00472}& \multicolumn{1}{c|}{8.87\%}     &  \multicolumn{1}{c|}{5.91\%} & \multicolumn{1}{c|}{4.73\%}  &\multicolumn{1}{c|}{2.38} & \multicolumn{1}{c|}{3.63} \\ \hline
				3                & \multicolumn{1}{c|}{4.80e-04}     &  \multicolumn{1}{c|}{5.37e-04} & \multicolumn{1}{c|}{5.62e-04}& \multicolumn{1}{c|}{28.28\%}     &  \multicolumn{1}{c|}{13.98\%} & \multicolumn{1}{c|}{10.59\%}  &\multicolumn{1}{c|}{3.66} & \multicolumn{1}{c|}{6.10} \\ \hline
				3.5                 & \multicolumn{1}{c|}{5.0e-05}     &  \multicolumn{1}{c|}{4.79e-05}  &  \multicolumn{1}{c|}{3.85e-05} &\multicolumn{1}{c|}{87.65\%}     &  \multicolumn{1}{c|}{29.58\% }&\multicolumn{1}{c|}{21.89\% } &\multicolumn{1}{c|}{9.16} &\multicolumn{1}{c|}{20.84}\\ \hline
				4               & \multicolumn{1}{c|}{0}     &  \multicolumn{1}{c|}{2.30e-06} &  \multicolumn{1}{c|}{1.06e-06}& \multicolumn{1}{c|}{601.37\%}     & \multicolumn{1}{c|}{76.63\%} & \multicolumn{1}{c|}{39.48\%} &\multicolumn{1}{c|}{28.42} &\multicolumn{1}{c|}{232.06}\\ \hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\kappa=2^{-5}\pi^2, T=1$. Simulation parameters: Final time $N=1$, $\Delta t= 0.001$, $\red{\textbf{x}_0 \sim N([0\;0], [0.5\; 0; 0\; 0.5])}$, $M=10^5$. In $\hat{\alpha}_{CE}^{\tilde{\mu}}$: $\tilde{\rho}_{x_0} \sim N(\tilde{\mu}^{CE},\sigma_0 )$ and in $\hat{\alpha}_{PDE}^{\tilde{\mu},\tilde{\sigma}}: \tilde{\rho}_{x_0}\sim N(\tilde{\mu}^{PDE},\tilde{\sigma}^{PDE} )$ .}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Langevin: IS with respect to $W_t$}
	\large{ \textbf{Initial condition is random: $\red{\textbf{x}_0 \sim N([0\;0], [0.1\; 0; 0\; 0.1])}$}}\\
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				2 & 0.0050 & 0.0091 & 79.9\% & 0.007 \\ 
				\hline
				2.5 & 3.51e-04 & 3.25e-04 & 20.3\% & 2.9 \\ 
				\hline
				3 & 1.75e-05 & 1.22e-05 & 14.86\% & 143 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\kappa=2^{-5}\pi^2, T=1$. Simulation parameters: Final time $N=1$, $\Delta t= 0.01$, $M=10^5$. Exp. smoothing factor $smf=25$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Langevin: IS with respect to  $\rho_0$}
	\large{ \textbf{Initial condition is random: $\red{\textbf{x}_0 \sim N([0\;0], [0.1\; 0; 0\; 0.1])}$}}\\
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				2 & 0.0050 & 0.0068 & 6.53\% & 1.32 \\ 
				\hline
				2.5 & 3.51e-04 & 4.79e-04 & 22.72\% & 1.6 \\ 
				\hline
				3 & 1.75e-05 & 1.12e-05 & 98.79\% & 3.5 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\kappa=2^{-5}\pi^2, T=1$. Simulation parameters: Final time $N=1$, $\Delta t= 0.01$, $M=10^5$. Exp. smoothing factor $smf=25$.}
		\label{table:DWerror}
	\end{table}
\end{frame}


\begin{frame}{Langevin: IS with respect to both $W_t$ and $\rho_0$}
	\large{ \textbf{Initial condition is random: $\red{\textbf{x}_0 \sim N([0\;0], [0.1\; 0; 0\; 0.1])}$}}\\

	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				2 & 0.0050& 0.0053 & 12.45\% & 0.47 \\ 
				\hline
				2.5 & 3.51e-04 & 4.61e-04 & 24.59\% & 1.37 \\ 
				\hline
				3 & 1.75e-05 & 1.74e-05 & 8.74\% & 289 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\kappa=2^{-5}\pi^2, T=1$. Simulation parameters: Final time $N=1$, $\Delta t= 0.01$, $M=10^5$. Exp. smoothing factor $smf=25$.}
		\label{table:DWerror}
	\end{table}
\end{frame}


\begin{frame}{Langevin: IS with respect to $W_t$}
	\large{ \textbf{Initial condition is random: $\red{\textbf{x}_0 \sim N([0\;0], [0.5\; 0; 0\; 0.5])}$}}\\
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				2.5 & 9.38e-04 & 7.71e-04 & 9.30\% & 5.76 \\ 
				\hline
				3 & 4.74e-05 & 5.22e-05 & 11.49\% & 55.67 \\ 
				\hline
				3.5 & 1.04e-06 & 2.76e-06 & 13.71\% & 741\\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\kappa=2^{-5}\pi^2, T=1$. Simulation parameters: Final time $N=1$, $\Delta t= 0.01$, $M=10^5$. Exp. smoothing factor $smf=25$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Langevin: IS with respect to  $\rho_0$}
	\large{ \textbf{Initial condition is random: $\red{\textbf{x}_0 \sim N([0\;0], [0.5\; 0; 0\; 0.5])}$}}\\
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				2.5 & 9.38e-04 & 0.0042 & 4.88\% & 3.86 \\ 
				\hline
				3 & 4.74e-05 & 4.41e-04 & 10.11\% & 8.52\\ 
				\hline
				3.5 & 1.04e-06 & 3.09e-05 & 30.93\% & 12.98 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\kappa=2^{-5}\pi^2, T=1$. Simulation parameters: Final time $N=1$, $\Delta t= 0.01$, $M=10^5$. Exp. smoothing factor $smf=25$.}
		\label{table:DWerror}
	\end{table}
\end{frame}


\begin{frame}{Langevin: IS with respect to both $W_t$ and $\rho_0$}
	\large{ \textbf{Initial condition is random: $\red{\textbf{x}_0 \sim N([0\;0], [0.5\; 0; 0\; 0.5])}$}}\\
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				2.5 & 9.38e-04 & 0.0044 & 89.47\% & 0.01 \\ 
				\hline
				3 & 4.74e-05 & 2.45e-04 & 21.52\% & 3.38 \\ 
				\hline
				3.5 & 1.04e-06 & 2.32e-05 & 40.41\% & 10.2 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\kappa=2^{-5}\pi^2, T=1$. Simulation parameters: Final time $N=1$, $\Delta t= 0.01$, $M=10^5$. Exp. smoothing factor $smf=25$.}
		\label{table:DWerror}
	\end{table}
\end{frame}


\begin{frame}{Langevin: IS with respect to $W_t$}
	\large{ \textbf{Initial condition is random: $\red{\textbf{x}_0 \sim N([0\;0], [0.2\; 0; 0\; 0.2])}$}}\\
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				2&  0.0041  & 0.0045 & 3.27\% & 7.99 \\ 
				\hline
				2.5 & 3.00e-04  & 3.58e-04 & 4.03\% & 66.1\\ 
				\hline
				3 &  2.09e-05 & 1.47e-05 & 2.42\% & 4469\\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\kappa=2^{-5}\pi^2, T=1$. Simulation parameters: Final time $N=1$, $\red{\Delta t= 0.1}$, $M=10^5$. Exp. smoothing factor $smf=10$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Langevin: IS with respect to  $\rho_0$}
	\large{ \textbf{Initial condition is random: $\red{\textbf{x}_0 \sim N([0\;0], [0.2\; 0; 0\; 0.2])}$}}\\
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{\rho_0}}$ & $\red{\epsilon_{st}^{PDE,\rho_0}}$ &  \\ 
				\hline
				2 & 0.0041 & 0.0065 & 5.96\% & 1.67 \\ 
				\hline
				2.5 & 3.00e-04 & 5.51e-04 & 18.55\% & 2.02\\ 
				\hline
				3 & 2.09e-05 & 3.39e-05 & 69.44\% & 2.35 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\kappa=2^{-5}\pi^2, T=1$. Simulation parameters: Final time $N=1$, $\red{\Delta t= 0.1}$, $M=10^5$. Exp. smoothing factor $smf=10$.}
		\label{table:DWerror}
	\end{table}
\end{frame}


\begin{frame}{Langevin: IS with respect to both $W_t$ and $\rho_0$}
	\large{ \textbf{Initial condition is random: $\red{\textbf{x}_0 \sim N([0\;0], [0.2\; 0; 0\; 0.2])}$}}\\
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{both}}$ & $\red{\epsilon_{st}^{PDE,both}}$ &  \\ 
				\hline
				2 &  0.0041  & 0.0071 & 3.77\% & 3.8 \\ 
				\hline
				2.5 & 3.00e-04  & 6.48e-04 & 3.26\% & 55.8 \\ 
				\hline
				3 &  2.09e-05 & 3.56e-05 & 3.17\% & 1075 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\kappa=2^{-5}\pi^2, T=1$. Simulation parameters: Final time $N=1$, $\red{\Delta t= 0.1}$, $M=10^5$. Exp. smoothing factor $smf=10$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Lorenz 96: IS with respect to both $W_t$ and $\rho_0$}
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{both}}$ & $\red{\epsilon_{st}^{PDE,both}}$ &  \\ 
				\hline
				2 &  0.0480 & 0.0472 & 2.27\% & 1.5 \\ 
				\hline
				2.5 & 0.0018 & 0.0017 & 5.36\% & 8.98 \\ 
				\hline
			    2.7 & 3.4e-04 & 3.02e-04 & 10.02\% & 12.66 \\ 
				\hline
				3 &  2.0e-05 & 1.75e-05 & 36.2\% & 16.75 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\sigma=0.5, F=2, d=10$. Simulation parameters: Final time $N=1$, $\red{\Delta t= 0.01}$, $M=10^5$, $\red{\textbf{x}_0 \sim N([0\;...\;0], 0.1\textbf{I})}$. Exp. smoothing factor $smf=15$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Lorenz 96: IS with respect to both $W_t$}
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{both}}$ & $\red{\epsilon_{st}^{PDE,both}}$ &  \\ 
				\hline
				2.5 &  0.00177 & 0.00184& 19.99\% & 0.52\\ 
				\hline
				2.7 & 3.1e-04 & 3.06e-04 & 10.82\% & 10.7 \\ 
				\hline
				3 &  3.0e-05 & 1.35e-05 & 17.6\% & 91.5 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\sigma=0.5, F=2, d=10$. Simulation parameters: Final time $N=1$, $\red{\Delta t= 0.01}$, $M=10^5$, $\red{\textbf{x}_0 \sim N([0\;...\;0], 0.1\textbf{I})}$. Exp. smoothing factor $smf=15$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\end{document}