% Modelo de slides para projetos de disciplinas do Abel
\documentclass[10pt]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}
\usepackage[numbers,sort&compress]{natbib}
\bibliographystyle{plainnat}
\usepackage{adjustbox}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{multirow}
\usepackage{xspace}
\usepackage{mathtools}
\usepackage{wrapfig}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\green}[1]{{\color{green} #1}}
\newcommand{\magenta}[1]{{\color{magenta} #1}}
\title{Rare events and filtering}
 \subtitle{A semi-analytical solution of KBE}
 \date{\today}
\date{Sept, 2023}
\author{Gaukhar Shaimerdenova}
\institute{AMCS, KAUST}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\V}{\mathbb{V}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\bu}{\boldsymbol{u}}
\newcommand{\bv}{\boldsymbol{v}}
\newcommand{\bx}{\boldsymbol{x}}
\newcommand{\ba}{\boldsymbol{a}}
\newcommand{\bb}{\boldsymbol{b}}
\newcommand{\bX}{\boldsymbol{X}}
\newcommand{\bK}{\boldsymbol{K}}
\newcommand{\brho}{\boldsymbol{\rho}}
\newcommand{\bmu}{\boldsymbol{\mu}}
\newcommand{\bSigma}{\boldsymbol{\Sigma}}
\newcommand{\bW}{\boldsymbol{W}}

%%%%%%%%   Macros we will need for this paper %%%%%%%%%%%%%%%

\newcommand{\norm}[1]{\left\| #1 \right\|}
\newcommand{\abs}[1]{\left| #1 \right|}
\newcommand{\ceil}[1]{\left \lceil #1 \right \rceil}
\newcommand{\prt}[1]{\left( #1 \right)}
\newcommand{\cost}[1]{\mathrm{Cost}}
\newcommand{\Tol}{\mathrm{TOL}}

% mathbb font letters 
\newcommand{\bF}{\mathbb{F}}
\newcommand{\bN}{\mathbb{N}}
\newcommand{\bP}{\mathbb{P}}
\newcommand{\bR}{\mathbb{R}}
\newcommand{\bZ}{\mathbb{Z}}
\newcommand{\bT}{\mathbf{T}}

% mathcal capital letters
\newcommand{\cB}{\mathcal{B}}
\newcommand{\cF}{\mathcal{F}}
\newcommand{\cO}{\mathcal{O}}
\newcommand{\cI}{\mathcal{I}}
\newcommand{\cJ}{\mathcal{J}}
\newcommand{\cK}{\mathcal{K}}
\newcommand{\cL}{\mathcal{L}}
\newcommand{\cN}{\mathcal{N}}
\newcommand{\cT}{\mathcal{T}}

%bold letters
\newcommand{\bw}{\boldsymbol{w}}

% ensemble notation 
\newcommand{\vBar}{\bar v}
\newcommand{\vHat}{\hat{v}}
\newcommand{\vBarHat}{\hat{\bar{v}}}
\newcommand{\meanHatMC}[1]{\hat{m}^{\rm{MC}}_{#1}}
\newcommand{\covHatMC}[1]{\widehat{C}^{\rm{MC}}_{#1}}
\newcommand{\yTilde}[1]{\tilde{y}_{#1}}
\newcommand{\meanHatML}[1]{\hat{m}^{\rm{ML}}_{#1}}
\newcommand{\covHatML}[1]{\widehat{C}^{\rm{ML}}_{#1}}
\newcommand{\kMC}[1]{K^{\rm{MC}}_{#1}}


% statistics notation
\newcommand{\Prob}[1]{\bP_{#1}}
\newcommand{\Ex}[1]{\E \left[ #1 \right]}
\newcommand{\Var}[1]{\V \left[ #1 \right]}
\newcommand{\Cov}{\overline{\mathrm{Cov}}}
\newcommand{\D}{\mathrm{D}}
\newcommand{\Ind}[2]{\textbf{1}_{\{#2\}}}

% colors
\newcommand{\blue}[1]{{\color{blue} #1}}


\begin{document}

\maketitle

\begin{frame}{}
The KBE with a Dirichlet boundary condition on the domain $(-\infty, \cK] \times [0,T]$:
\begin{equation}\label{eq:BKEpde}
	\left\{
	\begin{array}{ll}
		\frac{\partial \gamma}{\partial t}  = -a	\frac{\partial \gamma}{\partial x}   -\frac{1}{2} b^2 	\frac{\partial^2 \gamma}{\partial x},\\
		\gamma(x, T) =0, \quad x<\cK,\\
		\gamma(\cK, t) =1, \quad t\leq T,\\
		\gamma(-\infty, t) =0, \quad t \leq T.
	\end{array}
	\right.
\end{equation}
By a change of variable
\[
t'=T-t, \quad x'=\cK-x,
\]
we get 
\begin{equation}\label{eq:FKEpde}
	\left\{
	\begin{array}{ll}
		-\frac{\partial \gamma}{\partial t'}  = a	\frac{\partial \gamma}{\partial x'}   -\frac{1}{2} b^2 	\frac{\partial^2 \gamma}{\partial x'} \iff \frac{\partial \gamma}{\partial t'} + a	\frac{\partial \gamma}{\partial x'}  = \frac{1}{2} b^2 	\frac{\partial^2 \gamma}{\partial x'}  ,\\
		\gamma(x', 0) =0, \quad x'>0,\\
		\gamma(0, t') =1, \quad t'\leq 0,\\
		\gamma(\infty, t') =0, \quad t' \leq 0.
	\end{array}
	\right.
\end{equation}
\end{frame}

\begin{frame}{}
By F.Harleman, R.Rumer, 1963, we have a closed-form solution for~\eqref{eq:FKEpde} given by
\[
\gamma(x',t')=\frac{1}{2}erfc\Bigg(\frac{x'-a t'}{2\sqrt{\frac{b^2}{2}t'}}\Bigg)+\frac{1}{2}e^{\frac{ax'}{b^2/2}}erfc\Bigg(\frac{x'+a t'}{2\sqrt{\frac{b^2}{2}t'}}\Bigg).
\]
Then, back to $(x,t)$, we have the exact solution in the form
\begin{equation}\label{eq:analsol}
	\begin{split}
\gamma(x,t)=&\frac{1}{2}erfc\Bigg(\frac{\cK-x-a(x,t) (T-t)}{2\sqrt{\frac{b^2(x,t)}{2}(T-t)}}\Bigg)\\
&+\frac{1}{2}e^{\frac{a(x,t)(\cK-x)}{b^2(x,t)/2}}erfc\Bigg(\frac{\cK-x+a(x,t) (T-t)}{2\sqrt{\frac{b^2(x,t)}{2}(T-t)}}\Bigg).
	\end{split}
\end{equation}

\end{frame}

\begin{frame}{Analytical solution with freezed coeff. at the corner}
	\begin{equation}\label{eq:freezcoeffsol}
		\footnotesize
		\begin{split}
			\gamma(x,t)=\frac{1}{2}erfc\Bigg(\frac{\cK-x-a(\cK,T) (T-t)}{2\sqrt{\frac{b^2(\cK,T)}{2}(T-t)}}\Bigg)+\frac{1}{2}e^{\frac{a(\cK,T)(\cK-x)}{b^2(\cK,T)/2}}erfc\Bigg(\frac{\cK-x+a(\cK,T) (T-t)}{2\sqrt{\frac{b^2(\cK,T)}{2}(T-t)}}\Bigg).
		\end{split}
	\end{equation}
\begin{figure}[h!]
	\hspace*{-1cm}
	\includegraphics[height=4.5cm, width=5.7cm]{KBEanalsol_fixedcoeff.png}
	\includegraphics[height=4.5cm, width=5.7cm]{KBEanalsol_fixedcoeff_semilogy.png}
\end{figure}	
\end{frame}

\begin{frame}{A comparison of the exact solution (solid lines) with frozen coeff. solution (dotted lines ) }
		Here, we compare the exact solution~\eqref{eq:analsol} with the frozen coeff. solution~\eqref{eq:freezcoeffsol} (freezed on whole domain).
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{Exact_FreezedCoeff_KBEsol_comp.png}
		\includegraphics[height=4.5cm, width=5.7cm]{Exact_FreezedCoeff_KBEsol_comp_semilogy.png}
	\end{figure}	
\end{frame}

\begin{frame}{An absolute difference of the exact solution from the frozen coeff. solution over $x$ with fixed time and vice versa}
	\begin{figure}[h!]
		\includegraphics[height=3.7cm, width=5.2cm]{exactfrozendiffsolK05.png}
		\includegraphics[height=3.7cm, width=5.2cm]{exactfrozendiffsolK05_semilogy.png}
		\includegraphics[height=3.7cm, width=5.2cm]{Exact_FreezedCoeff_KBEsol_comp_over_t.png}
		\includegraphics[height=3.7cm, width=5.2cm]{Exact_FreezedCoeff_KBEsol_comp_over_t_semilogy.png}
	\end{figure}	
\end{frame}

\begin{frame}{Analytical solution with freezed coeff. at the corner}
The case $\cK=0.5$.
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=3.7cm, width=5.2cm]{KBEanalsol_fixedcoeff_K05_view.png}\\	\includegraphics[height=3.7cm, width=5.2cm]{KBEanalsol_fixedcoeff_K05.png}
		\includegraphics[height=3.7cm, width=5.2cm]{KBEanalsol_fixedcoeff_K05_semilogy.png}
	\end{figure}	
\end{frame}

\begin{frame}{  The corresponding optimal control based on the frozen coef. analyt. solution}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\centering
		\includegraphics[height=4.5cm, width=5.7cm]{analsol_optimcontrol_K05.png}	\includegraphics[height=4.5cm, width=5.7cm]{analsol_optimcontrol_K05_atdifftime_semilogy.png}
	\end{figure}	
\end{frame}

\begin{frame}{PDE solver}
We propose to solve~\eqref{eq:BKEpde} in the following way:\\
	\begin{figure}[h!]
	\includegraphics[width=0.5\linewidth]{PDEsolveridea.png} 
	\label{fig:wrapfig}
\end{figure}
\red{(I)} at $(t,x)\in [T-\Delta t, T] \times [\cK-\Delta x, \cK]$
	\begin{equation}
	\begin{split}
		v(t,x)=&\frac{1}{2}erfc\Bigg(\frac{\cK-x-a(\cK,T) (T-t)}{2\sqrt{\frac{b^2(\cK,T)}{2}(T-t)}}\Bigg)\\
		&+\frac{1}{2}e^{\frac{a(\cK,T)(\cK-x)}{b^2(\cK,T)/2}}erfc\Bigg(\frac{\cK-x+a(\cK,T) (T-t)}{2\sqrt{\frac{b^2(\cK,T)}{2}(T-t)}}\Bigg).
	\end{split}
\end{equation}
\end{frame}

\begin{frame}
\blue{(II)} at $(t,x)\in [T-\Delta t, T] \times [\infty, \cK-\Delta x)$
\begin{equation}
	\left\{
	\begin{array}{ll}
	\gamma_1(T,x)=0,\\
	\gamma_1(t,\cK-\Delta x)=v(t,\cK-\Delta x).
	\end{array}
	\right.
\end{equation}
We use the Crank-Nicolson scheme to approximate $\gamma_1$ in the given region.

\green{(III)} at $(t,x)\in [0, T-\Delta t] \times [\infty, \cK)$
\begin{equation}
	\begin{split}
		&\gamma_2(T-\Delta t,x) = \left\{
	\begin{array}{ll}
	  \gamma_1(T-\Delta t,x), \; x<\cK-\Delta x,\\
		v(T-\Delta t,x), \; x\geq \cK-\Delta x,
	\end{array}
	\right.\\
	&\gamma_2(t, \cK) =1.
\end{split}
\end{equation}
We use the Crank-Nicolson scheme to approximate $\gamma_2$ in the given region.
\end{frame}



\begin{frame}{\small Double Well example }
	Using the strategy for PDE solver described above with the PDE discretization $\Delta x^{PDE}=0.005$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$ and $\Delta x =0.2$, $\Delta t = 0.01$ for the red region, we provide PDE solutions for 3 different cases: $\cK=0.5$, $\cK= 1$ and $\cK= 2$ and its applications to the importance sampling.
	
	\textbf{I.}  $\cK=0.5$. 
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx02_dt001.png}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx02_dt001_semilogy.png}
	\end{figure}
\end{frame}

\begin{frame}{An absolute difference of the exact solution~\eqref{eq:analsol} from the frozen coeff. solution of the PDE solver above}
	\begin{figure}[h!]
		\includegraphics[height=3.7cm, width=5.2cm]{ExactFrozenInRedregioncomp.png}
		\includegraphics[height=3.7cm, width=5.2cm]{ExactFrozenInRedregioncomp_semilogy.png}
		\includegraphics[height=3.7cm, width=5.2cm]{ExactFrozenInRedregioncomp_over_t.png}
		\includegraphics[height=3.7cm, width=5.2cm]{ExactFrozenInRedregioncomp_over_t_semilogy.png}
	\end{figure}	
\end{frame}


\begin{frame}{An absolute difference of the analyt. solution~\eqref{eq:analsol} from the frozen coeff. solution of the PDE solver above }
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=3.7cm, width=5.2cm]{absdiffAnalPdesolver_K05_over_t.png}
		\includegraphics[height=3.7cm, width=5.2cm]{absdiffAnalPdesolver_K05_semilogy_over_t.png}
		\hspace*{-1cm}
		\includegraphics[height=3.7cm, width=5.2cm]{absdiffAnalPdesolver_K05.png}
		\includegraphics[height=3.7cm, width=5.2cm]{absdiffAnalPdesolver_K05_semilogy.png}
	\end{figure}
\end{frame}


\begin{frame}{\small DW: $\cK=0.5$, $\Delta x =0.2$, $\Delta t = 0.01$, $\Delta x^{PDE}=0.005$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$ }
	The PDE solutions in 2D format at certain fixed times.
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx02_dt001_2D.png}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx02_dt001_semilogy_2D.png}
	\end{figure}
$\bullet$ In the next slide, we provide each stage of approximating optimal initial density: KBE solution at time $0$; the product of KBE solution with original initial density; the normalised product, the Gaussian approximation of the normalised product. Normalisation is done over the whole domain for $x$ (not only $x<\cK$ but also $x\geq \cK$).
\end{frame}

\begin{frame}{\small DW: $\cK=0.5$, $\Delta x =0.2$, $\Delta t = 0.01$, $\Delta x^{PDE}=0.005$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$ }
	\begin{figure}[h!]
		\centering
		\includegraphics[height=4.3cm, width=5.3cm]{optimdensity_DW_K05_sig02_PDEsol.png}
		\includegraphics[height=4.3cm, width=5.3cm]{optimdensity_DW_K05_sig02_product.png}
		\includegraphics[height=4.3cm, width=5.3cm]{optimdensity_DW_K05_sig02_normprod.png}
		\includegraphics[height=4.3cm, width=5.3cm]{optimdensity_DW_K05_sig02_GaussApprox.png}
	\end{figure}
\end{frame}

\begin{frame}{\small DW: the same above figures in log scale}
	\begin{figure}[h!]
		\centering
		\includegraphics[height=4.3cm, width=5.3cm]{optimdensity_DW_K05_sig02_PDEsol_semilogy.png}
		\includegraphics[height=4.3cm, width=5.3cm]{optimdensity_DW_K05_sig02_product_semilogy.png}
		\includegraphics[height=4.3cm, width=5.3cm]{optimdensity_DW_K05_sig02_normprod_semilogy.png}
		\includegraphics[height=4.3cm, width=5.3cm]{optimdensity_DW_K05_sig02_GaussApprox_semilogy.png}
	\end{figure}
\end{frame}

\begin{frame}{\small Double Well example: similar plots for the case $\cK=1$ }
	\textbf{II.}  $\cK=1$. 
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx02_dt001_K1.png}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx02_dt001_semilogy_K1.png}
	\end{figure}
\end{frame}

\begin{frame}{\small DW: $\cK=1$, $\Delta x =0.2$, $\Delta t = 0.01$, $\Delta x^{PDE}=0.005$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$ }
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx02_dt001_2D_K1.png}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx02_dt001_semilogy_2D_K1.png}
	\end{figure}
\end{frame}

\begin{frame}{\small DW: $\cK=1$, $\Delta x =0.2$, $\Delta t = 0.01$, $\Delta x^{PDE}=0.005$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$ }
	\begin{figure}[h!]
		\centering
		\includegraphics[height=4.3cm, width=5.3cm]{optimdensity_DW_K05_sig05_PDEsol.png}
		\includegraphics[height=4.3cm, width=5.3cm]{optimdensity_DW_K05_sig05_product.png}
		\includegraphics[height=4.3cm, width=5.3cm]{optimdensity_DW_K05_sig05_normprod.png}
		\includegraphics[height=4.3cm, width=5.3cm]{optimdensity_DW_K05_sig05_GaussApprox.png}
	\end{figure}
\end{frame}

\begin{frame}{\small Double Well example: similar plots for the case $\cK=2$ }
	\textbf{II.}  $\cK=2$. 
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx02_dt001_K2.png}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx02_dt001_semilogy_K2.png}
	\end{figure}
\end{frame}

\begin{frame}{\small DW: $\cK=2$, $\Delta x =0.2$, $\Delta t = 0.01$, $\Delta x^{PDE}=0.005$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$ }
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx02_dt001_2D_K2.png}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx02_dt001_semilogy_2D_K2.png}
	\end{figure}
\end{frame}

\begin{frame}{\small DW: $\cK=2$, $\Delta x =0.2$, $\Delta t = 0.01$, $\Delta x^{PDE}=0.005$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$ }
	\begin{figure}[h!]
		\centering
		\includegraphics[height=4.3cm, width=5.3cm]{optimdensity_DW_K05_sig1_PDEsol.png}
		\includegraphics[height=4.3cm, width=5.3cm]{optimdensity_DW_K05_sig1_product.png}
		\includegraphics[height=4.3cm, width=5.3cm]{optimdensity_DW_K05_sig1_normprod.png}
		\includegraphics[height=4.3cm, width=5.3cm]{optimdensity_DW_K05_sig1_GaussApprox.png}
	\end{figure}
\end{frame}

\begin{frame}{Optimal control problem of the prelast step}
\begin{equation}
	\left\{
	\begin{array}{ll}
		X_T=X_0+(a+b\xi)\Delta t+b\Delta W_T,\\
		Y_T=2\xi\Delta W_T + \xi^2 \Delta t,
	\end{array}
	\right.
\end{equation}
where $a:=a(X_0)$, $b:=b(X_0)$, $\Delta W_T = \sqrt{\Delta t} \epsilon_T \sim N(0,\Delta t)$.

We want to minimize the 2nd moment
$$\min_{\xi} \E [\Ind{}{X_T\geq K}e^{-Y_T}|X_0=x].$$
Note that $X_T\geq K \iff \epsilon_T \geq \frac{K-x-(a+b\xi)\Delta t}{b\sqrt{dt}}$. Then,
\begin{equation}
\begin{split}
&\E [\Ind{}{X_T\geq K}e^{-Y_T}|X_0=x]=\frac{1}{\sqrt{2\pi}}\int_{\frac{K-x-(a+b\xi)\Delta t}{b\sqrt{dt}}}^{\infty} e^{-2\xi\sqrt{\Delta t}z-\xi^2\Delta t-\frac{z^2}{2}}dz\\
&=\frac{e^{\xi^2\Delta t}}{\sqrt{2\pi}}\int_{\frac{K-x-(a+b\xi)\Delta t}{b\sqrt{dt}}}^{\infty} e^{-(\frac{z}{\sqrt{2}}+\xi\sqrt{2\Delta t})^2}dz=\frac{e^{\xi^2\Delta t}}{\sqrt{\pi}}\int_{\frac{K-x-(a-b\xi)\Delta t}{b\sqrt{dt}}}^{\infty} e^{-s^2}ds\\
&=\frac{e^{\xi^2\Delta t}}{2}erfc(\frac{K-x-(a-b\xi)\Delta t}{b\sqrt{2\Delta t}}).
\end{split}
\end{equation}
\end{frame}

\begin{frame}{Optimal control problem of the prelast step}
The target function to minimise is 
$$F(\xi) = \frac{e^{\xi^2\Delta t}}{2}erfc\Big(\frac{K-x-(a-b\xi)\Delta t}{b\sqrt{2\Delta t}}\Big).$$
The derivative of $F$ is
\begin{equation}
\begin{split}
F'(\xi)&= \xi \Delta t e^{\xi^2 \Delta t} erfc \Big(\frac{K-x-(a-b\xi)\Delta t}{b\sqrt{2\Delta t}}\Big)\\
&-\sqrt{\frac{\Delta t}{2\pi}} e^{-\frac{(K-x-a\Delta t)^2}{2b^2 \Delta t}-\frac{(K-x-a\Delta t)\xi}{b}+0.5\xi^2\Delta t}.
\end{split}
\end{equation}
The second derivative of $F$ needed for Newton's method is
\begin{equation}
	\begin{split}
		F''(\xi)&= (\Delta t e^{\xi^2 \Delta t}+2\xi^2\Delta t^2e^{\xi^2\Delta t} ) erfc \Big(\frac{K-x-(a-b\xi)\Delta t}{b\sqrt{2\Delta t}}\Big)\\
		&-\frac{\sqrt{2\Delta t}}{\sqrt{\pi}}\xi\Delta t e^{\xi^2 \Delta t-\frac{(K-x-(a-b\xi)\Delta t)^2}{2b^2\Delta t}}\\
		&-\sqrt{\frac{\Delta t}{2\pi}}\Big(\xi\Delta t -\frac{(K-x-a\Delta t)}{b}\Big)e^{-\frac{(K-x-a\Delta t)^2}{2b^2 \Delta t}-\frac{(K-x-a\Delta t)\xi}{b}+0.5\xi^2\Delta t}.
	\end{split}
\end{equation}
\end{frame}

\begin{frame}{DW example: $K=0.5$, $b=0.5$}
	\begin{figure}[h!]
	\centering
	\includegraphics[height=4.1cm, width=5.3cm]{OptimControlNewton.png}\\
	\includegraphics[height=4.1cm, width=5.3cm]{KBEsolNewton.png}
	\includegraphics[height=4.1cm, width=5.3cm]{KBEsolNewtonsemilogy.png}
\end{figure}
\end{frame}

\begin{frame}{DW example: $K=0.5$, $b=0.5$}
	\begin{figure}[h!]
		\centering
		\includegraphics[height=4.5cm, width=5.3cm]{OptimControlNewton_dtscaled.png}
		\includegraphics[height=4.5cm, width=5.3cm]{KBEsolNewton_varyingdt.png}
	\end{figure}
\end{frame}
\end{document}