% Modelo de slides para projetos de disciplinas do Abel
\documentclass[10pt]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}
\usepackage[numbers,sort&compress]{natbib}
\bibliographystyle{plainnat}
\usepackage{adjustbox}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{multirow}
\usepackage{xspace}
\usepackage{mathtools}

\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\green}[1]{{\color{green} #1}}
\newcommand{\magenta}[1]{{\color{magenta} #1}}
\title{Rare events and filtering}
 \subtitle{tempering results}
 \date{\today}
\date{April, 2023}
\author{Gaukhar Shaimerdenova}
\institute{AMCS, KAUST}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\V}{\mathbb{V}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\bu}{\boldsymbol{u}}
\newcommand{\bv}{\boldsymbol{v}}
\newcommand{\bx}{\boldsymbol{x}}
\newcommand{\ba}{\boldsymbol{a}}
\newcommand{\bb}{\boldsymbol{b}}
\newcommand{\bX}{\boldsymbol{X}}
\newcommand{\bK}{\boldsymbol{K}}
\newcommand{\brho}{\boldsymbol{\rho}}
\newcommand{\bmu}{\boldsymbol{\mu}}
\newcommand{\bSigma}{\boldsymbol{\Sigma}}
\newcommand{\bW}{\boldsymbol{W}}

\begin{document}

\maketitle

\begin{frame}{Noisy Lorenz 63}
	A partially  noisy Lorenz '63  model 
	\begin{equation}\label{Langevin}
		\begin{cases} 
			du_1=r(u_2-u_1)dt+\sigma dW_1, \\
			du_2=(su_1-u_2-u_1u_3)dt+\sigma dW_2,\\
			du_3=(u_1u_2-qu_3)dt+\sigma dW_3,\;
		\end{cases}\,
	\end{equation}
	where $W_j, j=1,2,3$ is assumed to be independent Brownian motions. \\
	Chaotic classic setting: $r=10, s=28, q=8/3.$\\
	Initial condition: $(u_1, u_2, u_3)=(5,5,25)+N(\mathbf{0}, 0.5\mathbf{I});$\\
    Monte Carlo sample size in all runs: $M=10^4$.\\
    We aim to track the first component, so $P_1=[1 \; 0 \; 0]$ over time $[0,1]$
\end{frame}

\begin{frame}{First component trajectory samples over $[0,1]$ and $[0,10]$}
	\begin{figure}[h!]
		\includegraphics[height=5cm, width=5cm]{L63u1trajectoryM1e4.png}
		\includegraphics[height=5cm, width=5cm]{L63u1trajectoryM1e4T10.png}
	\end{figure}
\end{frame}

\begin{frame}{Initial run: fitted drift function}
	\small{This is results of initial run. I generated samples of Lorenz 63 (L63) without importance sampling (IS) and define the initial threshold $\mathcal{K}=13.5$ corresponding to the probability $\hat{\alpha}\approx 3\times10^{-2}$ and I aim to do tempering until $\mathcal{K}=14.1$ corresponding to $\hat{\alpha}\approx 4\times10^{-4}$.}
	\begin{figure}[h!]
	\includegraphics[height=5.5cm, width=6cm]{L63initialrun.png}
	\caption{\tiny Fitted drift function using hyperbolic polynomial space of order 4  and actual samples of the first component of L63}
\end{figure}
\end{frame}

\begin{frame}{Initial run: pde solution and optimal control}
	\small{This is corresponding pde solution and optimal control using $\bar{a}(x,t)$ from Figure 1.}
	\begin{figure}[h!]
		\includegraphics[height=5cm, width=5cm]{L63pdesolInitialRun.png}
		\includegraphics[height=5cm, width=5cm]{L63optcontrolinitialRun.png}
		\caption{A solution of Kolmogorov backward equation and corresponding optimal control}
	\end{figure}
\end{frame}

\begin{frame}{First iteration of IS: fitted drift function}
	\small{I perform IS with respect to both initial condition and Wiener paths. I shift only mean in the initial condition and keep the variance the same since it converges to zero quickly. New shifted mean after first iteration $\tilde{\mu}_0(1)\approx 5.82$}
	\begin{figure}[h!]
		\includegraphics[height=6cm, width=7cm]{L63runafterIS_1.png}
		\caption{}
	\end{figure}
\end{frame}

\begin{frame}{First iteration of IS:  pde solution and optimal control}
	\small{I set the threshold $K_{\ell+1}=K_{\ell}+0.2$ ($\ell$ is the iteration of tempering) and solve the pde again with the new constructed $\bar{a}(x,t)$ in Figure 3.}
\begin{figure}[h!]
	\includegraphics[height=5cm, width=5cm]{L63pdesolafterIS_1.png}
	\includegraphics[height=5cm, width=5cm]{L63optconafterIS_1.png}
	\caption{A solution of Kolmogorov backward equation and corresponding optimal control, $\mathcal{K}_1=13.7$ (recall $\mathcal{K}_0=13.5$).}
\end{figure}
\end{frame}

\begin{frame}{Second iteration of IS: fitted drift function}
	\small{I perform again IS with respect to both initial condition and Wiener paths. New shifted mean  $\tilde{\mu}_0(1)\approx 6.02$}
	\begin{figure}[h!]
		\includegraphics[height=6cm, width=7cm]{L63runafterIS_2.png}
		\caption{}
	\end{figure}
\end{frame}

\begin{frame}{Second iteration of IS:  pde solution and optimal control}
	\small{I set the threshold $K_{\ell+1}=K_{\ell}+0.2$ ($\ell$ is the iteration of tempering) and solve the pde again with the new constructed $\bar{a}(x,t)$ in Figure 5.}
	\begin{figure}[h!]
		\includegraphics[height=5cm, width=5cm]{L63pdesolafterIS_2.png}
		\includegraphics[height=5cm, width=5cm]{L63optconafterIS_2.png}
		\caption{A solution of Kolmogorov backward equation and corresponding optimal control, $\mathcal{K}_2=13.9$ (recall $\mathcal{K}_0=13.5$).}
	\end{figure}
\end{frame}

\begin{frame}{Third iteration of IS: fitted drift function}
	\small{I perform again IS with respect to both initial condition and Wiener paths. New shifted mean  $\tilde{\mu}_0(1)\approx 6.08$}
	\begin{figure}[h!]
		\includegraphics[height=6cm, width=7cm]{L63runafterIS_3.png}
		\caption{}
	\end{figure}
\end{frame}

\begin{frame}{Third iteration of IS:  pde solution and optimal control}
	\small{I set the threshold $K_{\ell+1}=K_{\ell}+0.2$ ($\ell$ is the iteration of tempering) and solve the pde again with the new constructed $\bar{a}(x,t)$ in Figure 7.}
	\begin{figure}[h!]
		\includegraphics[height=5cm, width=5cm]{L63pdesolafterIS_3.png}
		\includegraphics[height=5cm, width=5cm]{L63optconafterIS_3.png}
		\caption{A solution of Kolmogorov backward equation and corresponding optimal control, $\mathcal{K}_3=14.1$ (recall $\mathcal{K}_0=13.5$).}
	\end{figure}

\end{frame}
\begin{frame}{fitted drift function}
		\small{I perform again IS with respect to both initial condition and Wiener paths. New shifted mean  $\tilde{\mu}_0(1)\approx 6.23$}
	\begin{figure}[h!]
		\includegraphics[height=6cm, width=7cm]{L63runafterIS_4.png}
		\caption{}
	\end{figure}
\end{frame}

%\begin{frame}{Remark}
%As far as I understood, when we do this tempering the new computed estimator should match with the initial one, right? (since I am computing it with the likelihood)
%When I compute the probability $\hat{\alpha}$ after each IS, I am using samples and likelihoods of all timesteps, I am not stopping when it reaches thereshold. So I use full time samples for L2 regession to construct $\bar{a}(x,t)$. That's why maybe it is getting smaller and smaller. I was a bit confused  on this point. Anyway, the new constructed $\bar{a}(x,t)$ is not helping to reduce the variance later when I perform IS.\\
%
%\end{frame}

\begin{frame}{Numerical Tests}
   	\begin{figure}[h!]
   	\includegraphics[height=6cm, width=7cm]{L63abarK14.png}
   	\caption{}
   \end{figure}
\end{frame}

\begin{frame}{KBE solution}
	\begin{figure}[h!]
		\includegraphics[height=5cm, width=5cm]{KBEsolK14.png}
		\includegraphics[height=5cm, width=5cm]{KBEsolK14top.png}
		\caption{}
	\end{figure}
\end{frame}

\begin{frame}{Optimal Control}
	\begin{figure}[h!]
		\includegraphics[height=5cm, width=5cm]{optimalcontrolK14top.png} 
        \includegraphics[height=5cm, width=5cm]{optimalcontrolK14.png}
		\caption{}
	\end{figure}
\end{frame}

\begin{frame}{Likelihood and exponents}
	$\hat{\alpha}^{IS}=2.97\times10^{-4}$   $\qquad Var^{IS}=6.3\times10^{-4}$\\
    $\hat{\alpha}^{MC}= 0.0121$    $\qquad \qquad Var^{MC}=0.0119$ \\
    
	\begin{figure}[h!]
		\includegraphics[height=5cm, width=5cm]{L63K14sumofexponents.png} 
		\includegraphics[height=5cm, width=5cm]{L63K14likelihood.png}
		\caption{}
	\end{figure}
\end{frame}

\end{document}