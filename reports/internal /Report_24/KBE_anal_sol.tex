% Modelo de slides para projetos de disciplinas do Abel
\documentclass[10pt]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}
\usepackage[numbers,sort&compress]{natbib}
\bibliographystyle{plainnat}
\usepackage{adjustbox}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{multirow}
\usepackage{xspace}
\usepackage{mathtools}
\usepackage{wrapfig}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\green}[1]{{\color{green} #1}}
\newcommand{\magenta}[1]{{\color{magenta} #1}}
\title{Rare events and filtering}
 \subtitle{A semi-analytical solution of KBE}
 \date{\today}
\date{Sept, 2023}
\author{Gaukhar Shaimerdenova}
\institute{AMCS, KAUST}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\V}{\mathbb{V}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\bu}{\boldsymbol{u}}
\newcommand{\bv}{\boldsymbol{v}}
\newcommand{\bx}{\boldsymbol{x}}
\newcommand{\ba}{\boldsymbol{a}}
\newcommand{\bb}{\boldsymbol{b}}
\newcommand{\bX}{\boldsymbol{X}}
\newcommand{\bK}{\boldsymbol{K}}
\newcommand{\brho}{\boldsymbol{\rho}}
\newcommand{\bmu}{\boldsymbol{\mu}}
\newcommand{\bSigma}{\boldsymbol{\Sigma}}
\newcommand{\bW}{\boldsymbol{W}}

%%%%%%%%   Macros we will need for this paper %%%%%%%%%%%%%%%

\newcommand{\norm}[1]{\left\| #1 \right\|}
\newcommand{\abs}[1]{\left| #1 \right|}
\newcommand{\ceil}[1]{\left \lceil #1 \right \rceil}
\newcommand{\prt}[1]{\left( #1 \right)}
\newcommand{\cost}[1]{\mathrm{Cost}}
\newcommand{\Tol}{\mathrm{TOL}}

% mathbb font letters 
\newcommand{\bF}{\mathbb{F}}
\newcommand{\bN}{\mathbb{N}}
\newcommand{\bP}{\mathbb{P}}
\newcommand{\bR}{\mathbb{R}}
\newcommand{\bZ}{\mathbb{Z}}
\newcommand{\bT}{\mathbf{T}}

% mathcal capital letters
\newcommand{\cB}{\mathcal{B}}
\newcommand{\cF}{\mathcal{F}}
\newcommand{\cO}{\mathcal{O}}
\newcommand{\cI}{\mathcal{I}}
\newcommand{\cJ}{\mathcal{J}}
\newcommand{\cK}{\mathcal{K}}
\newcommand{\cL}{\mathcal{L}}
\newcommand{\cN}{\mathcal{N}}
\newcommand{\cT}{\mathcal{T}}

%bold letters
\newcommand{\bw}{\boldsymbol{w}}

% ensemble notation 
\newcommand{\vBar}{\bar v}
\newcommand{\vHat}{\hat{v}}
\newcommand{\vBarHat}{\hat{\bar{v}}}
\newcommand{\meanHatMC}[1]{\hat{m}^{\rm{MC}}_{#1}}
\newcommand{\covHatMC}[1]{\widehat{C}^{\rm{MC}}_{#1}}
\newcommand{\yTilde}[1]{\tilde{y}_{#1}}
\newcommand{\meanHatML}[1]{\hat{m}^{\rm{ML}}_{#1}}
\newcommand{\covHatML}[1]{\widehat{C}^{\rm{ML}}_{#1}}
\newcommand{\kMC}[1]{K^{\rm{MC}}_{#1}}


% statistics notation
\newcommand{\Prob}[1]{\bP_{#1}}
\newcommand{\Ex}[1]{\E \left[ #1 \right]}
\newcommand{\Var}[1]{\V \left[ #1 \right]}
\newcommand{\Cov}{\overline{\mathrm{Cov}}}
\newcommand{\D}{\mathrm{D}}
\newcommand{\Ind}[2]{\textbf{1}_{\{#2\}}}

% colors
\newcommand{\blue}[1]{{\color{blue} #1}}


\begin{document}

\maketitle

\begin{frame}{}
The KBE with a Dirichlet boundary condition on the domain $(-\infty, \cK] \times [0,T]$:
\begin{equation}\label{eq:BKEpde}
	\left\{
	\begin{array}{ll}
		\frac{\partial \gamma}{\partial t}  = -a	\frac{\partial \gamma}{\partial x}   -\frac{1}{2} b^2 	\frac{\partial^2 \gamma}{\partial x},\\
		\gamma(x, T) =0, \quad x<\cK,\\
		\gamma(\cK, t) =1, \quad t\leq T,\\
		\gamma(-\infty, t) =0, \quad t \leq T.
	\end{array}
	\right.
\end{equation}
By a change of variable
\[
t'=T-t, \quad x'=\cK-x,
\]
we get 
\begin{equation}\label{eq:FKEpde}
	\left\{
	\begin{array}{ll}
		-\frac{\partial \gamma}{\partial t'}  = a	\frac{\partial \gamma}{\partial x'}   -\frac{1}{2} b^2 	\frac{\partial^2 \gamma}{\partial x'} \iff \frac{\partial \gamma}{\partial t'} + a	\frac{\partial \gamma}{\partial x'}  = \frac{1}{2} b^2 	\frac{\partial^2 \gamma}{\partial x'}  ,\\
		\gamma(x', 0) =0, \quad x'>0,\\
		\gamma(0, t') =1, \quad t'\leq 0,\\
		\gamma(\infty, t') =0, \quad t' \leq 0.
	\end{array}
	\right.
\end{equation}
\end{frame}

\begin{frame}{}
By F.Harleman, R.Rumer, 1963, we have a closed-form solution for~\eqref{eq:FKEpde} given by
\[
\gamma(x',t')=\frac{1}{2}erfc\Bigg(\frac{x'-a t'}{2\sqrt{\frac{b^2}{2}t'}}\Bigg)+\frac{1}{2}e^{\frac{ax'}{b^2/2}}erfc\Bigg(\frac{x'+a t'}{2\sqrt{\frac{b^2}{2}t'}}\Bigg).
\]
Then, back to $(x,t)$, we have the analytical solution in the form
\begin{equation}\label{eq:analsol}
	\begin{split}
\gamma(x,t)=&\frac{1}{2}erfc\Bigg(\frac{\cK-x-a(x,t) (T-t)}{2\sqrt{\frac{b^2(x,t)}{2}(T-t)}}\Bigg)\\
&+\frac{1}{2}e^{\frac{a(x,t)(\cK-x)}{b^2(x,t)/2}}erfc\Bigg(\frac{\cK-x+a(x,t) (T-t)}{2\sqrt{\frac{b^2(x,t)}{2}(T-t)}}\Bigg).
	\end{split}
\end{equation}

\end{frame}

\begin{frame}{DW example: analytical solution for $\sigma=0.5$, $\cK=2$}
		\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEanalsol.png}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEanalsol_semilogy.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: $\sigma=0.5$, $\cK=2$}
	The same figures as the previous slide from a different angle
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEanalsolRotated.png}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEanalsolsemilogy.png}
	\end{figure}
\end{frame}

\begin{frame}{Analytical solution with freezed coeff. at the corner}
	\begin{equation*}	
		\footnotesize
		\begin{split}
			\gamma(x,t)=\frac{1}{2}erfc\Bigg(\frac{\cK-x-a(\cK,T) (T-t)}{2\sqrt{\frac{b^2(\cK,T)}{2}(T-t)}}\Bigg)+\frac{1}{2}e^{\frac{a(\cK,T)(\cK-x)}{b^2(\cK,T)/2}}erfc\Bigg(\frac{\cK-x+a(\cK,T) (T-t)}{2\sqrt{\frac{b^2(\cK,T)}{2}(T-t)}}\Bigg).
		\end{split}
	\end{equation*}
\begin{figure}[h!]
	\hspace*{-1cm}
	\includegraphics[height=4.5cm, width=5.7cm]{KBEanalsol_fixedcoeff.png}
	\includegraphics[height=4.5cm, width=5.7cm]{KBEanalsol_fixedcoeff_semilogy.png}
\end{figure}	
\end{frame}


\begin{frame}{Analytical solution with freezed coeff. at the corner}
	DW example: $\cK=2$, $\sigma=0.5$.
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEanalsolatdifftime.png}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEanalsolatdifftime_semilogy.png}
	\end{figure}	
\end{frame}


\begin{frame}{DW: using the analytical solution (in the slide 5) at time 0 }
	\begin{figure}[h!]
		\includegraphics[height=4cm, width=5cm]{optiminitdens_DW_sig02_analsol.png}
		\hspace*{-0.63cm}
		\includegraphics[height=4cm, width=5cm]{optiminitdens_DW_sig05_analsol.png}
		\hspace*{-0.63cm}
		\includegraphics[height=4cm, width=5cm]{optiminitdens_DW_sig1_analsol.png}
		\caption{\textbf{Double Well example:} the original initial density ($\rho_{u_0}$ in red solid line) sampled from one well whereas the threshold $\cK$ is given in another well. A comparision of the optimal importance initial densities based on the IS wrt $\rho_0$ ($\tilde\rho_{u_0}^{PDE,1}$ in cyan dash-dotted line) and the IS wrt both $\rho_0$ and $W(t)$ ($\tilde\rho_{u_0}^{PDE,2}$ in blue dash-dotted line) with the CE-based IS initial density ($\tilde\rho_{u_0}^{CE}$ in green dashed line) for the cases of different initial standard deviation $\sigma_0$ to estimate around $10^{-3}$ probability. }
		\label{fig:DWchangeofrho0}
	\end{figure}
\end{frame}

\begin{frame}{DW: The similar plots in the manuscript draft for comparison}
	\begin{figure}[h!]
		\includegraphics[height=4cm, width=5cm]{DWrho0K05sigma0_02_4.png}
		\hspace*{-0.63cm}
		\includegraphics[height=4cm, width=5cm]{DWrho0K1sigma0_5_4.png}
		\hspace*{-0.63cm}
		\includegraphics[height=4cm, width=5cm]{DWrho0K2sigma1_4.png}
		\caption{\textbf{Double Well example:} the original initial density ($\rho_{u_0}$ in red solid line) sampled from one well whereas the threshold $\cK$ is given in another well. A comparision of the optimal importance initial densities based on the IS wrt $\rho_0$ ($\tilde\rho_{u_0}^{PDE,1}$ in cyan dash-dotted line) and the IS wrt both $\rho_0$ and $W(t)$ ($\tilde\rho_{u_0}^{PDE,2}$ in blue dash-dotted line) with the CE-based IS initial density ($\tilde\rho_{u_0}^{CE}$ in green dashed line) for the cases of different initial standard deviation $\sigma_0$ to estimate around $10^{-3}$ probability. }
		\label{fig:DWchangeofrho0}
	\end{figure}
\end{frame}

\begin{frame}{PDE solver}
We propose to solve~\eqref{eq:BKEpde} in the following way:\\
	\begin{figure}[h!]
	\includegraphics[width=0.5\linewidth]{PDEsolveridea.png} 
	\label{fig:wrapfig}
\end{figure}
\red{(I)} at $(t,x)\in [T-\Delta t, T] \times [\cK-\Delta x, \cK]$
	\begin{equation}\label{eq:analsol}
	\begin{split}
		v(t,x)=&\frac{1}{2}erfc\Bigg(\frac{\cK-x-a(\cK,T) (T-t)}{2\sqrt{\frac{b^2(\cK,T)}{2}(T-t)}}\Bigg)\\
		&+\frac{1}{2}e^{\frac{a(\cK,T)(\cK-x)}{b^2(\cK,T)/2}}erfc\Bigg(\frac{\cK-x+a(\cK,T) (T-t)}{2\sqrt{\frac{b^2(\cK,T)}{2}(T-t)}}\Bigg).
	\end{split}
\end{equation}
\end{frame}

\begin{frame}
\blue{(II)} at $(t,x)\in [T-\Delta t, T] \times [\infty, \cK-\Delta x)$
\begin{equation}
	\left\{
	\begin{array}{ll}
	\gamma_1(T,x)=0,\\
	\gamma_1(t,\cK-\Delta x)=v(t,\cK-\Delta x).
	\end{array}
	\right.
\end{equation}
We use the Crank-Nicolson scheme to approximate $\gamma_1$ in the given region.

\green{(III)} at $(t,x)\in [0, T-\Delta t] \times [\infty, \cK)$
\begin{equation}
	\begin{split}
		&\gamma_2(T-\Delta t,x) = \left\{
	\begin{array}{ll}
	  \gamma_1(T-\Delta t,x), \; x<\cK-\Delta x,\\
		v(T-\Delta t,x), \; x\geq \cK-\Delta x,
	\end{array}
	\right.\\
	&\gamma_2(t, \cK) =1.
\end{split}
\end{equation}
We use the Crank-Nicolson scheme to approximate $\gamma_2$ in the given region.
\end{frame}

\begin{frame}{\small DW: $\Delta x =\Delta x^{PDE} $, $\Delta t = \Delta t^{PDE}$, $\Delta x^{PDE}=0.005$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$ }
		\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.3cm, width=5.5cm]{KBEfindiffsol.png}
		\includegraphics[height=4.3cm, width=5.5cm]{KBEfindiffsol_semilogy.png}
			\includegraphics[height=4cm, width=5.5cm]{KBEfindiffsol_semilogy_rotated.png}
	\end{figure}
\end{frame}

\begin{frame}{\small DW: $\Delta x =\Delta x^{PDE} $, $\Delta t = \Delta t^{PDE}$, $\Delta x^{PDE}=0.005$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$ }
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.3cm, width=5.5cm]{KBEfindiffsol_2D.png}
		\includegraphics[height=4.3cm, width=5.5cm]{KBEfindiffsol_semilogy_2D.png}
	\end{figure}
\end{frame}


\begin{frame}{\small DW: $\Delta x =0.5$, $\Delta t = 0.1$, $\Delta x^{PDE}=0.005$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$ }
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx05_dt01.png}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx05_dt01semilogy.png}
	\end{figure}
\end{frame}

\begin{frame}{\small DW: $\Delta x =0.5$, $\Delta t = 0.1$, $\Delta x^{PDE}=0.005$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$ }
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx05_dt01_2D.png}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx05_dt01_2D_semilogy.png}
	\end{figure}
\end{frame}

\begin{frame}{\small DW: $\Delta x =0.1$, $\Delta t = 0.001$, $\Delta x^{PDE}=0.005$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$ }
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx01_dt001.png}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx01_dt001_semilogy.png}
	\end{figure}
\end{frame}


\begin{frame}{\small DW: $\Delta x =0.1$, $\Delta t = 0.001$, $\Delta x^{PDE}=0.005$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$ }
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx01_dt001_2D.png}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx01_dt001_semilogy_2D.png}
	\end{figure}
\end{frame}

\begin{frame}{\small DW: $\Delta x =0.1$, $\Delta t = 0.01$, $\Delta x^{PDE}=0.005$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$ }
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx01_dt01.png}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx01_dt01_semilogy.png}
	\end{figure}
\end{frame}


\begin{frame}{\small DW: $\Delta x =0.1$, $\Delta t = 0.01$, $\Delta x^{PDE}=0.005$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$ }
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx01_dt012D.png}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx01_dt01_semilogy_2D.png}
	\end{figure}
\end{frame}


\begin{frame}{\small DW: $\Delta x =0.2$, $\Delta t = 0.01$, $\Delta x^{PDE}=0.005$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$ }
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx02_dt001.png}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx02_dt001_semilogy.png}
	\end{figure}
\end{frame}

\begin{frame}{\small DW: $\Delta x =0.2$, $\Delta t = 0.01$, $\Delta x^{PDE}=0.005$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$ }
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx02_dt001_2D.png}
		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_dx02_dt001_semilogy_2D.png}
	\end{figure}
\end{frame}

\begin{frame}{\small DW: $\Delta x =0.2$, $\Delta t = 0.01$, $\Delta x^{PDE}=0.005$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$ }
	We plot optimal initial densities computed by using the analytical solution in the corner $[T-0.01]\times [\cK-0.2, \cK]$
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=4.7cm, width=5.5cm]{optiminitdens_DW_sig1_findiffdx02dt001.png}
	\end{figure}

Similar results as in the manuscript.
\end{frame}

\begin{frame}{Comparison of exact solutions with freezed and no freezed coeff.}
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=4.7cm, width=5.5cm]{exactanalcomp.png}
		\includegraphics[height=4.7cm, width=5.5cm]{exactanalcomp_semilog.png}
	\end{figure}
\end{frame}

%\begin{frame}{\small DW: $\Delta x =\Delta x^{PDE}$, $\Delta t = \Delta t^{PDE} $, $\Delta x^{PDE}=0.1$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$ }
%	\begin{figure}[h!]
%		\centering
%		\hspace*{-1cm}
%		\includegraphics[height=4.7cm, width=5.5cm]{KBEanalsol_semilogy_rotated_dxpde01.png}
%		\includegraphics[height=4.7cm, width=5.5cm]{KBEfindiffsol_semilogy_rotated_dxpde01.png}
%	\end{figure}
%\end{frame}


%\begin{frame}{DW example: $\sigma=0.5$, $\cK=2$}
%	Below, we plot the analytical solution~\eqref{eq:analsol} for DW example at timestep $T-dt_{EM}$ with fixed coefficients at the singularity $(\cK, T)$, where $dt_{EM}$ is the Euler-Maruyama timestep and compare with the  Erik's version obtained via computing the optimal control at the same pre-last time step and by numerical integration, computing the corresponding KBE solution.
%	\begin{figure}[h!]
%	\hspace*{-1cm}
%	\includegraphics[height=4.5cm, width=5.7cm]{comparisonKBEsol.png}
%	\includegraphics[height=4.5cm, width=5.7cm]{comparisonKBEsolsemilogy.png}
%   \end{figure}
%
%\end{frame}
%
%\begin{frame}
%	As you see, both analytical and semi-analytical solutions goes to zero very fast on the left boundary side. To avoid this, we used an exponential smoothing before. The speed of going to 0 depends on the Euler-Maruyama timestep, if it is smaller and smaller, the solutions goes to zero very quickly. 
%   
%   I've tried a Gaussian bound of $erfc$ function in the analytical solution, it doesn't solve the issue. 
%   
%   %In my opinion, our current pde solver with exponential smoothing works well for our 2 problems, a similar smoothing technique is used in Shyam's paper too. 
%\end{frame}
\end{document}