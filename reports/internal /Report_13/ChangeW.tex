% Modelo de slides para projetos de disciplinas do Abel
\documentclass[10pt]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}
\usepackage[numbers,sort&compress]{natbib}
\bibliographystyle{plainnat}
\usepackage{adjustbox}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{multirow}
\usepackage{xspace}
\usepackage{mathtools}

\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\magenta}[1]{{\color{magenta} #1}}
\title{Rare events and filtering}
 \subtitle{importance sampling with respect to $W$}
 \date{\today}
\date{August, 2022}
\author{Gaukhar Shaimerdenova}
\institute{AMCS, KAUST}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\V}{\mathbb{V}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\bu}{\boldsymbol{u}}
\newcommand{\bv}{\boldsymbol{v}}
\newcommand{\bx}{\boldsymbol{x}}
\newcommand{\ba}{\boldsymbol{a}}
\newcommand{\bb}{\boldsymbol{b}}
\newcommand{\bX}{\boldsymbol{X}}
\newcommand{\bK}{\boldsymbol{K}}
\newcommand{\brho}{\boldsymbol{\rho}}
\newcommand{\bmu}{\boldsymbol{\mu}}
\newcommand{\bSigma}{\boldsymbol{\Sigma}}
\newcommand{\bW}{\boldsymbol{W}}

\begin{document}

\maketitle

\begin{frame}[fragile]{Problem setup in 1D}
	We consider the general SDE of the form
	\begin{equation}\label{sde}
		\begin{cases} 
			dX_t= a(t, X_t)dt+b(t, X_t)dW_t^{\mathbb{P}}, \quad 0<t<T, \\
			X_{0}\sim \rho_0.
		\end{cases}\,
	\end{equation}
	\red{\textbf{Objective:}} For a given threshold $K>0$, to track the probability:
	\[
	\alpha:=P(\max_{0\leq t\leq T} X_t\geq K).
	\] 
	\textbf{Notation:} $M^T:=\max_{0\leq t\leq T} X_t$.
	
	Let us define the process $W_t^{\mathbb{Q}}=W_t^{\mathbb{P}}-\int_{0}^{t} \xi(s,X_s)ds$ and substitute in~\eqref{sde}:
	\begin{equation*}
		\begin{cases}
			dX_t=  (a(t, X_t)dt+b(t, X_t)\xi(t,X_t))dt+b(t, X_t)dW_t^{\mathbb{Q}},\\
			X_{0} \sim \rho_0.
		\end{cases}\,
	\end{equation*}
   $\bullet$ Girsanov's theorem states that $(W_t^\mathbb{Q})_{t\geq}0$  is a standard Brownian motion under the probabilty measure $Q$ with the likelihood ratio
	\[
	\frac{d\mathbb{P}}{d\mathbb{Q}}=e^{-\int_{0}^{t} \xi(s,X_s)dW_s^{\mathbb{Q}}-\frac{1}{2}\int_{0}^{t}\xi^2(s,X_s)ds}.
	\]
\end{frame}

\begin{frame}{Importance sampling}
	 $\bullet$  Since the distribution of $W^{\mathbb{Q}}$ under $\mathbb{Q}$ is the same as the distribution of $W^{\mathbb{P}}$ under $\mathbb{P}$, we can work with the following SDE under $\mathbb{P}$:
	\begin{equation*}
		\begin{cases}
			dX_t^{\xi}=  (a(t, X_t^{\xi})dt+b(t, X_t^{\xi})\xi(t,X_t^{\xi}))dt+b(t, X_t^{\xi})dW_t^{\mathbb{P}}, \quad 0<t<T\\
			X_{0}^{\xi} \sim \rho_0.
		\end{cases}\,
	\end{equation*}
$\bullet$ 	Note we have two sources of randomness in the quantity of interest:
\[
\alpha= \mathbb{E}_{\rho_0 \times \mathbb{P}}[\textbf{1}_{\{M^T\geq K\}}]
\]
$\bullet$ We apply an \textbf{importance sampling with respect to $W_t^{\mathbb{P}}$:}
 \begin{equation}
 	\begin{split}
 		\mathbb{E}_{\rho_0 \times \mathbb{P}}[\textbf{1}_{\{M^T\geq K\}}] =\E_{\rho_0 \times \mathbb{Q}} \Big[ \textbf{1}_{\{M^T\geq K\}} \frac{d\mathbb{P}}{d\mathbb{Q}} \Big]
 	\end{split}
 \end{equation}
   	
\end{frame}

\begin{frame}{Connection with exit time}
 $\bullet$ Let us define the exit time (a first passage time) for a given domain $D$ by
\[
\tau(x,t) = \inf\{s>t: X_s\notin D \mbox{ and } X_t=x\}.
\]
 Note the exit probability $P(\tau(x,t) \leq T):=u_{\tau}(t,x)$ solves the Kolmogorov Backward equation (KBE) 
with Dirichlet boundary condition:
\[
\left\{
\begin{array}{ll}
	\partial_t u_\tau (t,x) = -a(t,x) \partial_x u_\tau (t,x) -\frac{1}{2} b^2 (t,x) \partial_{xx} u_{\tau} (t,x),\\
	u_\tau (T,\cdot) =0, \quad x\in D,\\
	u_\tau (\cdot,x) =1, \quad x\in  (0,T)\times \partial D 
\end{array}
\right.
\]
$\bullet$  If we consider the domain $D:=\{X_t: X_t< K\}$, observe the connection between the events
\[
\{M^T\geq K\}=\{\tau(X_0,0)\leq T\}.
\]
Then we can represent the QoI as
\begin{equation*}
	\begin{split}
		\mathbb{E}_{\rho_0 \times \mathbb{P}}[\textbf{1}_{\{M^T\geq K\}}] &=	\mathbb{E}[\textbf{1}_{\{\tau(X_0,0) \leq T\}}]=	\mathbb{E}_{\rho_0 \times \mathbb{P}}[\textbf{1}_{\{X_{\tau \wedge T}=K\}}]\\
		&=\mathbb{E}_{\rho_0 \times \mathbb{Q}} \Big[ \textbf{1}_{\{X_{\tau \wedge T}^{\xi}=K\}} \frac{d\mathbb{P}}{d\mathbb{Q}} \Big].
	\end{split}
\end{equation*}

   


\end{frame}

\begin{frame}{Optimal control}
$\bullet$ Our aim now is to find a control $\xi$ that minimizes the variance of the QoI (i.e. sufficient to minimize the second moment): 
\[
\min_{\xi} \mathbb{E}_{\rho_0 \times \mathbb{Q}} \Big[\textbf{1}_{\{X_{\tau(x_0,0) \wedge T}^{\xi}=K\}}  e^{-2\int_{0}^{\tau(x_0,0) \wedge T} \xi(s,X_s^{\xi})dW_s^{\mathbb{Q}}-\int_{0}^{\tau(x_0,0) \wedge T}\xi^2(s,X_s^{\xi})ds} \Big | X_0^{\xi}=x_0] 
\]

$\bullet$ Let us define the value function that minimizes the second moment  of the MC estimator:
\begin{equation*}
	\begin{split}
&u_\tau(t,x)=\\
&\min_{\xi} \mathbb{E}_{\rho_0 \times \mathbb{Q}} \Big[\textbf{1}_{\{X_{\tau(x,t) \wedge T}^{\xi}=K\}}  e^{-2\int_{t}^{\tau(x,t) \wedge T} \xi(s,X_s^{\xi})dW_s^{\mathbb{Q}}-\int_{t}^{\tau(x,t) \wedge T}\xi^2(s,X_s^{\xi})ds} \Big | X_t^{\xi}=x] 
	\end{split}
\end{equation*}

$\bullet$ $v^2_\tau(t,x)=u_\tau(t,x)$ satisfies the KBE with the optimal control (Theorem 2, C.Hartmann et.al.)
\[
\xi^*(t,x) = b(t,x) \frac{\partial \log v_\tau(t,x)}{\partial x}.
\]
\end{frame}




\begin{frame}{Problem setup in multi-D}
 $\red{\bullet}$ Consider a stochastic process $\bX\in \R^d $ with $d>1$ following the SDE for  $0<t<T$, 
 \begin{equation}\label{sdemultiD}
 	\begin{cases} 
 		d\bX_i (t)= \ba_i(t, \bX(t))dt+\bb_{ij}(t,\bX(t))d\bW_j (t), \\
 		\bX(0)\sim \boldsymbol{\rho}_0, \mbox{ where }\boldsymbol{\rho}_0 \sim N(\bmu_0, \bSigma_0),
 	\end{cases}\,
 \end{equation}
 where $\bW$ is a $J-$dimensional Wiener process with independent components and $i=1,...,d$, $j=1,...,J$. 
 
  $\red{\bullet}$ Similarly to 1D, we perform a change of measure with respect to $\bW$. 
   \begin{equation}\label{sdechangeofmeasure}
  	\begin{cases} 
  		d\bX_i (t)= (\ba_i(t, \bX(t))+\bb_{ij}(t,\bX(t))\boldsymbol{\xi}_j(t,\bX(t)))dt+\bb_{ij}(t,\bX(t))d\bW_j (t), \\
  		\bX(0)\sim \boldsymbol{\rho}_0, \mbox{ where }\boldsymbol{\rho}_0 \sim N(\bmu_0, \bSigma_0).
  	\end{cases}\,
  \end{equation}
 \red{\textbf{Objective:}} Given a threshold $K$ and the projection $P_1 \in \R^{1\times d}$, consider the one-dimensional non-Markovian process $S_1(t)=P_1 \bX(t)$ and track the probability
 \[
 \boldsymbol{\alpha}:=P(\max_{0\leq t\leq T} S_1(t) \geq K).
 \]
\end{frame}

\begin{frame}{Markovian projection}
	$\bullet$ \textbf{Importance sampling with respect to $\bW$:}
	\begin{equation}
		\begin{split}
			\mathbb{E}_{\boldsymbol{\rho}_0 \times \mathbb{P}}[\textbf{1}_{\{\max_{0\leq t\leq T} S_1(t) \geq K\}}] =\E_{\boldsymbol{\rho}_0 \times \mathbb{Q}} \Big[ \textbf{1}_{\{\max_{0\leq t\leq T} S_1(t) \geq K\}} \frac{\boldsymbol{d\mathbb{P}}}{\boldsymbol{d\mathbb{Q}}} \Big]
		\end{split}
	\end{equation}
    where
    \[
    \frac{\boldsymbol{d\mathbb{P}}}{\boldsymbol{d\mathbb{Q}}} = \mbox{exp}\Big\{-\int_{t}^{\tau\wedge T} ||\boldsymbol{\xi}(s, \bX_s)||^2ds -\frac{1}{2} \int_{t}^{\tau \wedge T} <\boldsymbol{\xi}(s,\bX(s)), d\bW>\Big\}.
    \]
    
   	$\bullet$  The value function that minimizes the second moment of the QoI is
   	\begin{equation*}
   		\begin{split}
   			&\boldsymbol{u}_\tau(t,\bx)=\min_{\boldsymbol{\xi}} \mathbb{E}_{\boldsymbol{\rho}_0 \times \mathbb{Q}} \Big[\textbf{1}_{\{S_1(\tau \wedge T)=K\}} \Big(\frac{\boldsymbol{d\mathbb{P}}}{\boldsymbol{d\mathbb{Q}}} \Big)^2\Big | \bX_t=\bx \Big].
   		\end{split}
   	\end{equation*}
     $\bullet$ Note $\boldsymbol{v}_\tau(t,\bx):=\boldsymbol{u}^2_\tau(t,\bx)$ satisfies the high-dimensional KBE and
    \[
    \boldsymbol{\xi}^*(t,\bx) = \boldsymbol{b}(t,\bx)^T \nabla \log \boldsymbol{v}_\tau(t,\bx).
    \]
    $\bullet$ Instead of solving KBE in multi D, we can solve the KBE of the corresponding projected Markovian process in 1D:
    \[
    \boldsymbol{\xi}^*(t,\bx) = \boldsymbol{b}(t,\bx)^T \nabla \log \boldsymbol{v}_\tau(t,\bx) \approx \boldsymbol{b}(t,\bx)^T P_1^T\frac{\partial\log \bar{v}_\tau(t,P_1 \bx)}{\partial x}.
    \]
\end{frame}

\begin{frame}{Original Indicator function at final condition: DW example}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=5cm, width=5.5cm]{KBEsol_at_diff_t.png}
		\includegraphics[height=5cm, width=5.5cm]{OptControl_at_diff_time.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: Linear extrapolation BC}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=5cm, width=5.5cm]{loguLinearBC.png}
		\includegraphics[height=5cm, width=5.5cm]{logksiLinearBC.png}
	\end{figure}
\end{frame}


\begin{frame}{DW example: Expon. extrap BC}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=5cm, width=5.5cm]{loguExpBC.png}
		\includegraphics[height=5cm, width=5.5cm]{logksiExpBC.png}
	\end{figure}
\end{frame}


\begin{frame}{DW example: Dirichlet BC}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=5cm, width=5.5cm]{loguDirichletBC.png}
		\includegraphics[height=5cm, width=5.5cm]{logksiDirichletBC.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: Neumann BC}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=5cm, width=5.5cm]{loguNeumannBC.png}
		\includegraphics[height=5cm, width=5.5cm]{logksiNeumannBC.png}
	\end{figure}
\end{frame}

\begin{frame}{DW example: Neumann BC coarser mesh}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=5cm, width=5.5cm]{loguNeumannBCcoarse.png}
		\includegraphics[height=5cm, width=5.5cm]{logksiNeumannBCcoarse.png}
	\end{figure}
\end{frame}

\begin{frame}{Smoothened Indicator function at final condition: DW example}
	\large{ \textbf{Smoothing factor:  $\kappa = 3$}}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=5cm, width=5.5cm]{KBEsol_at_diff_t_Smooth.png}
		\includegraphics[height=5cm, width=5.5cm]{OptControl_at_diff_time_Smooth1.png}
	\end{figure}
\end{frame}

\begin{frame}{Double Well dynamics with $\sigma=0.5$}
	\large{ \textbf{Initial condition is fixed:  $\red{x_0=-1}$,}}\\
	\large{ \textbf{Original Indicator function, no smoothing}}\\
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				0 & 0.03178 & 0.02508 & 5.35\% & 0.53 \\ 
				\hline
				0.5 & 0.00232 & 0.00222 & 4.67\% & 7.93 \\ 
				\hline
				0.8 & 3.30e-04 & 2.88e-04 & 11.23\% & 10.55 \\ 
				\hline
				1 & 2.00e-05 & 4.77e-05 & 4.25\% & 446 \\ 
				\hline
				1.2 & 1.00e-05 & 6.16e-06 & 6.17\% & 1637 \\
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}


\begin{frame}{Double Well dynamics with $\sigma=0.5$}
	\large{ \textbf{Initial condition is fixed:  $\red{x_0=-1}$,}}\\
	\large{ \textbf{Smoothing factor:  $\kappa = 3$}}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
\begin{tabular}{|c|c|c|c|c|} 
	\hline
	\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
	\cline{2-4}
	& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
	\hline
	0 & 0.03108 & 0.03145 & 1.02\% & 11.81 \\ 
	\hline
	0.5 & 0.00228 & 0.00243 & 1.48\% & 72.67 \\ 
	\hline
	0.8 & 3.20e-04 & 3.09e-04 & 2.28\% & 239 \\ 
	\hline
	1 & 6.00e-05 & 5.33e-05 & 3.15\% & 728 \\ 
	\hline
	1.2 & 0 & 6.68e-06 & 3.42\% & 4924 \\
	\hline
\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Double Well dynamics with $\sigma=0.5$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,1)}$}}\\
	\large{ \textbf{Smoothing factor:  $\kappa = 15$}}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				0.5 & 0.1580 & 0.1733 & 30.56\% & 0.00 \\ 
				\hline
				1 & 0.0631 & 0.0537 & 33.11\% & 0.01 \\ 
				\hline
				1.5 & 0.0136 & 0.0067 & 34.53\% & 0.05 \\ 
				\hline
				2 & 0.0024 & 6.97e-04 & 84.78\% & 0.08 \\ 
				\hline
				2.5 & 3.5e-04 & 5.18e-05 & 138.7\% & 0.39 \\
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Double Well dynamics with $\sigma=0.5$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,0.5)}$}}\\
	\large{ \textbf{Smoothing factor:  $\kappa = 15$}}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				0 & 0.1085 & 0.0852 & 5.87\% & 0.13 \\ 
				\hline
				0.5 & 0.0327 & 0.0300 & 12.68\% & 0.08 \\ 
				\hline
				1 & 0.0049 & 0.0045 & 17.02\% & 0.30 \\ 
				\hline
				1.5 & 2.0e-04 & 1.28e-04 & 18.36\% & 8.91 \\ 
				\hline
		    	1.8 & 1.0e-05 & 6.30e-06 & 17.98\% & 188.6 \\ 
				\hline
				2 &  0 & 8.38e-07 & 21.78\% & 966.5 \\
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Double Well dynamics with $\sigma=0.5$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,0.1)}$}}\\
	\large{ \textbf{Smoothing factor:  $\kappa = 15$}}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				0 & 0.0346 & 0.0303& 5.86\% & 0.37 \\ 
				\hline
				0.5 & 0.00274 & 0.00270 & 4.29\% & 7.73 \\ 
				\hline
		    	0.8 & 3.90e-04 & 3.74e-04 & 5.50\% & 33.99\\ 
				\hline
				1 & 4.00e-05 & 6.74e-05 & 4.10\% & 339.5 \\ 
				\hline
				1.2 & 2.00e-05 & 8.85e-06 & 4.09\% & 2597 \\ 
				\hline
				1.5 &  0 & 2.54e-07 & 6.23\% & 39056 \\
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}



\begin{frame}{Double Well dynamics with $\sigma=0.5$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,1)}$}}\\
	\large{ \textbf{Smoothing factor:  $\kappa = 3$}}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				0 & 0.2461& 0.2413 & 3.26\% & 0.15\\ 
				\hline
				1 & 0.0632 & 0.0620 & 4.09\% & 0.37 \\ 
				\hline
				2 & 0.00271 & 0.00282 & 46.14\% & 0.06 \\ 
				\hline
				3 & 8.0e-05 & 1.13e-05 & 178.3\% & 1.07 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Double Well dynamics with $\sigma=0.5$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,0.5)}$}}\\
	\large{ \textbf{Smoothing factor:  $\kappa = 3$}}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				0 & 0.1104& 0.1100 & 6.26\% & 0.09\\ 
				\hline
				1 & 0.0049 & 0.0051 & 6.23\% & 1.95 \\ 
				\hline
		    	1.5 & 1.2e-04 & 1.66e-04 & 32.84\% & 2.15 \\ 
				\hline
				2 & 0 & 2.02e-06 & 100\% & 18.99 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}


\begin{frame}{Double Well dynamics with $\sigma=0.5$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,0.1)}$}}\\
	\large{ \textbf{Smoothing factor:  $\kappa = 3$}}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				0 & 0.0341& 0.0333 & 1.30\% & 6.85\\ 
				\hline
		    	0.5 & 0.0029 & 0.0028 & 2.92\% & 15.9 \\ 
				\hline
				0.8 & 4.60e-04 & 3.82e-04 & 1.61\% & 389\\ 
				\hline
				1 & 9.0e-05 & 7.03e-05 & 2.67\% & 769 \\ 
				\hline
				1.2 & 2.00e-05 & 9.46e-06 & 3.78\% & 2845 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}


\begin{frame}{Double Well dynamics with $\sigma=0.5$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,0.1)}$}}\\
	\large{ \textbf{Indicator function}}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				0 & 0.0334& 0.0258& 3.66\% & 1.11\\ 
				\hline
				0.5 & 0.0027 & 0.0026 & 7.55\% & 2.59 \\ 
				\hline
				0.8 & 3.00e-04 & 3.60e-04 & 9.25\% & 12.47\\ 
				\hline
				1 & 4.0e-05 & 6.47e-05 & 10.86\% & 50.41 \\ 
				\hline
				1.2 & 1.00e-05 & 1.11e-05 & 53.36\% & 12.22 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}


\begin{frame}{Remarks for DW tests}
	$\bullet$ Mean estimates produced by MC and IS are not close in non rare event: in the case of Indicator function at the final condition (no matter for $\sigma_0$ and cropping the left boundary and extrapolation). PDE discretization: $[-5, 5]$, $N_x=1e3$ and $N_t=1e3$. \\
	- Better results in mean convergence in the case of PDE discretization: $[-5, 5]$, $N_x=1e2$ and $N_t=1e2$. 
	
   $\bullet$ Smoothing factor $\kappa=15$ gives good results only in small $\sigma_0$, in larger $\sigma_0$, we observe mean convergence problem. 
   
   $\bullet$ Smoothing factor $\kappa=3$ gives good results only in small $\sigma_0$, in larger $\sigma_0$, no variance reduction but also no mean convergence issue. 
   
    $\bullet$
   
\end{frame}

\begin{frame}{Ornstein-Uhlenbeck dynamics with $\sigma=1$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(1,0.1)}$}}\\
	\large{ \textbf{Indicator function}}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				2.5 & 0.0432& 0.0283& 3.30\% & 0 \\ 
				\hline
				3 & 0.0053 & 0.0036 & 37.25\% & 0 \\ 
				\hline
				3.5 & 3.7e-04 & 1.70e-04 & 9.24\% & 26\\ 
				\hline
				4 & 1.0e-05 & 6.67e-06 & 11.62\% & 426 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=1$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Ornstein-Uhlenbeck dynamics with $\sigma=1$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(1,0.5)}$}}\\
	\large{ \textbf{Indicator function}}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				2.5 & 0.0648& 0.0253& 5.89\% & 0.4 \\ 
				\hline
				3 & 0.0093 & 0.0034 & 6.56\% & 2.6 \\ 
				\hline
				3.5 & 6.9e-04 & 3.4e-04 & 37.1\% & 0.8\\ 
				\hline
				4 & 3.0e-05 & 1.28e-05 & 7.19\% & 586 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=1$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}


\begin{frame}{Ornstein-Uhlenbeck dynamics with $\sigma=1$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(1,1)}$}}\\
	\large{ \textbf{Indicator function}}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				2.5 & 0.1562& 0.1743& 135.2\% & 0 \\ 
				\hline
				3 & 0.0528 & 0.0202 & 112.7\% & 0 \\ 
				\hline
				3.5 & 0.0140& 9.7e-04 & 21.4\% & 0.9\\ 
				\hline
				4 & 0.00295 & 6.6e-05 & 13.5\% & 32 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=1$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
checked the fixed initial condition, the mean converges well.
\end{frame}

\begin{frame}{Ornstein-Uhlenbeck dynamics with $\sigma=1$}
	\large{ \textbf{Initial condition is fixed: $x_0=1$}}\\
	\large{ \textbf{Indicator function}}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				2.5 & 0.04203& 0.02138& 4.99\% & 0.7 \\ 
				\hline
				3 & 0.00486 & 0.00234 & 6.51\% & 3.86 \\ 
				\hline
				3.5 & 3.6e-04& 1.8e-04 & 9.01\% & 28\\ 
				\hline
				4 & 1.0e-05& 6.4e-06 & 11.3\% & 468 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=1$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}

\end{frame}


\begin{frame}{Ornstein-Uhlenbeck dynamics with $\sigma=1$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(1,0.1)}$}}\\
	\large{ \textbf{Smoothing factor} $\kappa=3$}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				2 & 0.2098& 0.1959& 5.30\% & 0 \\ 
				\hline
				2.5 & 0.0425 & 0.0437 & 8.46\% & 0.1\\ 
				\hline
				3 & 0.00471& 0.00479 & 5.47\% & 2.7\\ 
				\hline
				3.5 & 3.0e-04 & 3.12e-04 & 11.63\% & 9.1 \\ 
				\hline
		    	4 & 1.0e-05 & 1.1e-05 & 6.99\% & 713 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=1$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}



\begin{frame}{Ornstein-Uhlenbeck dynamics with $\sigma=1$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(1,0.1)}$}}\\
	\large{ \textbf{Smoothing factor} $\kappa=15$}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				2 & 0.2114& 0.1222& 8.12\% & 0 \\ 
				\hline
				3 & 0.00529& 0.00343 & 33.63\% & 0\\ 
				\hline
				3.5 & 3.0e-04 & 1.89e-04 & 11.69\% & 14.9 \\ 
				\hline
				4 & 2.0e-05 & 7.58e-06 & 20.9\% & 116 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=1$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}{Original Indicator function at final condition: OU example}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=5cm, width=5.5cm]{KBEsol_at_diff_t_IndFun_OU.png}
		\includegraphics[height=5cm, width=5.5cm]{OptControl_at_diff_time_IndFun_OU.png}
	\end{figure}
\end{frame}

\begin{frame}{Smoothened Indicator function at final condition: OU example}
	\large{ \textbf{Smoothing factor:  $\kappa = 3$}}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=5cm, width=5.5cm]{KBEsol_at_diff_t_Smooth3_OU.png}
		\includegraphics[height=5cm, width=5.5cm]{OptControl_at_diff_time_Smooth3_OU.png}
	\end{figure}
\end{frame}

\begin{frame}{IS with both sources of randomness}
	Let $\bar{X}_{0:N}:=\{\bar{X}_i\}_{i=0}^N$ denote Euler-Maruyama approximation of the SDEa at time $0=t_0<t_1<...<t_N=T$ with $\bar{X}_0=\bar{X}_0^{\xi}=X_0$ and consider the joint density $ \rho_{\bar{X}_{0:N}} (\bar{x}_{0:N})$ of random vector $(x_0,\bar{X}_{1:N})$ defined in a sample space $\Omega$. Then, we can write
	\begin{equation}
		\begin{split}
			&\mathbb{E}_{\rho \times \mathbb{P}}[\textbf{1}_{\{\max_{0\leq n\leq N-1} \bar{X}_n \geq K\}}] =\int_{\Omega} \textbf{1}_{\{\max_{0\leq n\leq N-1} \bar{X}_n \geq K\}} \rho_{\bar{X}_{0:N}} (\bar{x}_{0:N}) d\bar{x}_{0:N}\\
			&= \int_{\Omega} \textbf{1}_{\{\max_{0\leq n\leq N-1} \bar{X}_n \geq K\}} \frac{\rho_{\bar{X}_{0:N}}(\bar{x}_{0:N})}{\tilde{\rho}_{\bar{X}_{0:N}^{\xi}} (\bar{x}_{0:N})}\tilde{\rho}_{\bar{X}_{0:N}^{\xi}}(\bar{x}_{0:N})d\bar{x}_{0:N}\\
			&=\mathbb{E}_{\tilde{\rho} \times \mathbb{Q}} \Big[  \textbf{1}_{\{\max_{0\leq n\leq N-1} \bar{X}_n\geq K\}} \frac{\rho_{\bar{X}_{0:N}}(\bar{x}_{0:N})}{\tilde{\rho}_{\bar{X}_{0:N}^{\xi}} (\bar{x}_{0:N})} \Big]
		\end{split}
	\end{equation}
By the method of Lagrangian multipliers, we can deduce that the optimal joint importance density which can reduce the variance of the estimator is
	\begin{equation}
	\begin{split}
	\tilde{\rho}_{\bar{X}_{0:N}^{\xi}} (\bar{x}_{0:N}) \propto \rho_{\bar{X}_{0:N}}(\bar{x}_{0:N}) \textbf{1}_{\{\max_{0\leq n\leq N-1} \bar{X}_n \geq K\}}
	\end{split}
\end{equation}


\end{frame}

\begin{frame}{IS with both sources of randomness}

\begin{equation}
	\begin{split}
\tilde{\rho}_{\bar{X}_{0}^{\xi}} (\bar{x}_{0}) &= \int \tilde{\rho}_{\bar{X}_{0:N}^{\xi}} (\bar{x}_{0:N}) d\bar{x}_{1:N} \\
& \propto \int \rho_{\bar{X}_{0:N}}(\bar{x}_{0:N}) \textbf{1}_{\{\max_{0\leq n\leq N-1} \bar{X}_n \geq K\}}d\bar{x}_{1:N}\\
& = \int \textbf{1}_{\{\max_{0\leq n\leq N-1} \bar{X}_n \geq K\}} \rho_{\bar{X}_{0}}(\bar{x}_{0}) \rho_{\bar{X}_{1:N}|X_0}(\bar{x}_{1:N}|\bar{x}_0)  d\bar{x}_{1:N}\\
&= \rho_{\bar{X}_{0}}(\bar{x}_{0})  \E[ \textbf{1}_{\{\max_{0\leq n\leq N-1} \bar{X}_n \geq K\}}|\bar{X}_0=\bar{x}_0] \\
&=\rho_{\bar{X}_{0}}(\bar{x}_{0}) P(\max_{0\leq n\leq N-1} \bar{X}_n \geq K|\bar{X}_0=\bar{x}_0)
	\end{split}
\end{equation}
\end{frame}


\begin{frame}{DW dynamics with $\sigma=0.5$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,0.1)}$}}\\
	\large{ \textbf{Smoothing factor} $\kappa=3$}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				0 & 0.03395& 0.03312 & 1.29\% & 6.94\\ 
				\hline
				0.5 & 0.00280 & 0.00278  & 1.08\% & 118 \\ 
				\hline
				1 & 1.20e-04 & 6.99e-05 & 2.02\% & 1348\\ 
				\hline
				1.2 & 2.00e-05 & 9.2e-06 & 3.33\% & 3745 \\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

%\begin{frame}{DW dynamics with $\sigma=0.5$}
%	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,0.1)}$}}\\
%	\large{ \textbf{Smoothing factor} $\kappa=3$}
%	\begin{table}[h!]
%		\begin{adjustbox}{width=\columnwidth,center}
%			\begin{tabular}{|c|c|c|c|c|} 
%				\hline
%				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
%				\cline{2-4}
%				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
%				\hline
%				-0.5 & 0.3882& 0.3860& 1.32\% & 0.6 \\ 
%				\hline
%				0 & 0.03338& 0.03547 & 1.48\% & 5.24\\ 
%				\hline
%				0.5 & 0.00260 & 0.00278  & 1.21\% & 94.55 \\ 
%				\hline
%				1 & 6.00e-05 & 6.87e-05 & 2.13\% & 1233 \\ 
%				\hline
%				1.2 & 1.00e-05 & 9.6e-06 & 3.70\% & 2937 \\ 
%				\hline
%			\end{tabular}
%		\end{adjustbox}
%		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
%		\label{table:DWerror}
%	\end{table}

%Here, var.reduction comes more from dW control rather than initial condition.
%\end{frame}

\begin{frame}{DW dynamics with $\sigma=0.5$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,0.5)}$}}\\
	\large{ \textbf{Smoothing factor} $\kappa=3$}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
				\hline
				0.5 & 0.0323& 0.0329& 1.96\% & 3.03\\ 
				\hline
				1 & 0.0049 & 0.0050  & 1.36\% & 42.1\\ 
				\hline
				1.5 & 1.4e-04 & 1.6e-04 & 2.59\% & 370\\ 
				\hline
				2 & 0 & 1.2e-06 & 9.93\% & 3191\\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

%\begin{frame}{DW dynamics with $\sigma=0.5$}
%	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,0.5)}$}}\\
%	\large{ \textbf{Smoothing factor} $\kappa=3$}
%	\begin{table}[h!]
%		\begin{adjustbox}{width=\columnwidth,center}
%			\begin{tabular}{|c|c|c|c|c|} 
%				\hline
%				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
%				\cline{2-4}
%				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\ 
%				\hline
%				0 & 0.1095& 0.1084& 2.78\% & 0.5 \\ 
%				\hline
%				0.5 & 0.0327& 0.0320 & 1.32\% & 6.88\\ 
%				\hline
%				1 & 0.0052 & 0.0048  & 1.95\% & 20.9\\ 
%				\hline
%				1.5 & 1.2e-04 & 1.5e-04 & 5.47\% & 84.33 \\ 
%				\hline
%				2 & 0 & 1.3e-06 & 24.19\% & 518.7 \\ 
%				\hline
%			\end{tabular}
%		\end{adjustbox}
%		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
%		\label{table:DWerror}
%	\end{table}
%\end{frame}

\begin{frame}{DW dynamics with $\sigma=0.5$}
	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,1)}$}}\\
	\large{ \textbf{Smoothing factor} $\kappa=3$}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|} 
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
				\cline{2-4}
				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\
				\hline 
		    	1.5 & 0.0134 &  0.0133  & 4.17\% & 1.7\\ 
				\hline
				2 & 0.0024 &  0.0030  & 3.49\% & 0.09\\ 
				\hline
				2.5 & 3.1e-04 & 2.9e-04  & 9.93\% & 13.3\\ 
				\hline
				3  & 6.00e-05 & 4.03e-05  & 21.7\% & 20.2\\ 
				\hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
		\label{table:DWerror}
	\end{table}
\end{frame}

%\begin{frame}{DW dynamics with $\sigma=0.5$}
%	\large{ \textbf{Initial condition is random:  $\red{x_0\sim N(-1,1)}$}}\\
%	\large{ \textbf{Smoothing factor} $\kappa=3$}
%	\begin{table}[h!]
%		\begin{adjustbox}{width=\columnwidth,center}
%			\begin{tabular}{|c|c|c|c|c|} 
%				\hline
%				\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator}} & \textbf{Relative stat. error} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ 
%				\cline{2-4}
%				& $\hat{\alpha}_{MC}$ & $\red{\hat{\alpha}_{PDE}^{dW}}$ & $\red{\epsilon_{st}^{PDE,dW}}$ &  \\
%				\hline 
%				1 & 0.0628 &  0.0627  & 3.13\% & 0.6\\ 
%				\hline
%				1.5 & 0.0132 &  0.0135  & 7.06\% & 0.6\\ 
%				\hline
%				2 & 0.0027 &  0.0023  & 21.53\% & 0.4\\ 
%				\hline
%				2.5 & 3.8e-04 & 2.8e-04  & 26.05\% & 2\\ 
%				\hline
%				3  & 3.00e-05 & 3.08e-05  & 36.22\% & 9.5\\ 
%				\hline
%			\end{tabular}
%		\end{adjustbox}
%		\caption{ Model parameter: $\sigma=0.5$. Simulation parameters: $T=1$, $\Delta t= 0.01$, $M= 10^5$.}
%		\label{table:DWerror}
%	\end{table}
%\end{frame}

\end{document}