% Modelo de slides para projetos de disciplinas do Abel
\documentclass[10pt]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}
\usepackage[numbers,sort&compress]{natbib}
\bibliographystyle{plainnat}
\usepackage{adjustbox}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{multirow}
\usepackage{xspace}
\usepackage{mathtools}
\usepackage{wrapfig}
\usepackage[linesnumbered,ruled,vlined]{algorithm2e}
\SetKwInput{KwInput}{Input}                % Set the Input
\SetKwInput{KwOutput}{Output}              % set the Output
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\green}[1]{{\color{green} #1}}
\newcommand{\magenta}[1]{{\color{magenta} #1}}
\title{Rare events and filtering}
 \subtitle{A semi-analytical solution of KBE}
 \date{\today}
\date{Nov, 2023}
\author{Gaukhar Shaimerdenova}
\institute{AMCS, KAUST}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\V}{\mathbb{V}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\bu}{\boldsymbol{u}}
\newcommand{\bv}{\boldsymbol{v}}
\newcommand{\bx}{\boldsymbol{x}}
\newcommand{\ba}{\boldsymbol{a}}
\newcommand{\bb}{\boldsymbol{b}}
\newcommand{\bX}{\boldsymbol{X}}
\newcommand{\bK}{\boldsymbol{K}}
\newcommand{\brho}{\boldsymbol{\rho}}
\newcommand{\bmu}{\boldsymbol{\mu}}
\newcommand{\bSigma}{\boldsymbol{\Sigma}}
\newcommand{\bW}{\boldsymbol{W}}


%%%%%%%%   Macros we will need for this paper %%%%%%%%%%%%%%%

\newcommand{\norm}[1]{\left\| #1 \right\|}
\newcommand{\abs}[1]{\left| #1 \right|}
\newcommand{\ceil}[1]{\left \lceil #1 \right \rceil}
\newcommand{\prt}[1]{\left( #1 \right)}
\newcommand{\cost}[1]{\mathrm{Cost}}
\newcommand{\Tol}{\mathrm{TOL}}

% mathbb font letters 
\newcommand{\bF}{\mathbb{F}}
\newcommand{\bN}{\mathbb{N}}
\newcommand{\bP}{\mathbb{P}}
\newcommand{\bR}{\mathbb{R}}
\newcommand{\bZ}{\mathbb{Z}}
\newcommand{\bT}{\mathbf{T}}

% mathcal capital letters
\newcommand{\cB}{\mathcal{B}}
\newcommand{\cF}{\mathcal{F}}
\newcommand{\cO}{\mathcal{O}}
\newcommand{\cI}{\mathcal{I}}
\newcommand{\cJ}{\mathcal{J}}
\newcommand{\cK}{\mathcal{K}}
\newcommand{\cL}{\mathcal{L}}
\newcommand{\cN}{\mathcal{N}}
\newcommand{\cT}{\mathcal{T}}

%bold letters
\newcommand{\bw}{\boldsymbol{w}}

% ensemble notation 
\newcommand{\vBar}{\bar v}
\newcommand{\vHat}{\hat{v}}
\newcommand{\vBarHat}{\hat{\bar{v}}}
\newcommand{\meanHatMC}[1]{\hat{m}^{\rm{MC}}_{#1}}
\newcommand{\covHatMC}[1]{\widehat{C}^{\rm{MC}}_{#1}}
\newcommand{\yTilde}[1]{\tilde{y}_{#1}}
\newcommand{\meanHatML}[1]{\hat{m}^{\rm{ML}}_{#1}}
\newcommand{\covHatML}[1]{\widehat{C}^{\rm{ML}}_{#1}}
\newcommand{\kMC}[1]{K^{\rm{MC}}_{#1}}


% statistics notation
\newcommand{\Prob}[1]{\bP_{#1}}
\newcommand{\Ex}[1]{\E \left[ #1 \right]}
\newcommand{\Var}[1]{\V \left[ #1 \right]}
\newcommand{\Cov}{\overline{\mathrm{Cov}}}
\newcommand{\D}{\mathrm{D}}
\newcommand{\Ind}[2]{\textbf{1}_{\{#2\}}}

% colors
\newcommand{\blue}[1]{{\color{blue} #1}}


\begin{document}

\maketitle

\begin{frame}{}
The KBE with a Dirichlet boundary condition on the domain $(-\infty, \cK] \times [0,T]$:
\begin{equation}\label{eq:BKEpde}
	\left\{
	\begin{array}{ll}
		\frac{\partial \gamma}{\partial t}  = -a(x,t)	\frac{\partial \gamma}{\partial x}   -\frac{1}{2} b^2(x,t) 	\frac{\partial^2 \gamma}{\partial x},\\
		\gamma(x, T) =0, \quad x<\cK,\\
		\gamma(\cK, t) =1, \quad t\leq T.\\
	\end{array}
	\right.
\end{equation}

By F.Harleman, R.Rumer (1963), if $a(x,t):=a$ and $b(x,t):=b$ are constant coefficients, we have a closed-form solution in the form
\begin{equation}\label{eq:analsol}
	\begin{split}
\gamma_{\textbf{const}}(x,t)=&\frac{1}{2}erfc\Bigg(\frac{\cK-x-a (T-t)}{2\sqrt{\frac{b^2}{2}(T-t)}}\Bigg)\\
&+\frac{1}{2}e^{\frac{a(\cK-x)}{b^2/2}}erfc\Bigg(\frac{\cK-x+a (T-t)}{2\sqrt{\frac{b^2}{2}(T-t)}}\Bigg).
	\end{split}
\end{equation}
\end{frame}


\begin{frame}{Analytical solution with the freezed coefficients at the corner}
	If we set $a(x,t):=a(\cK, T)$ and $b(x,t):=b(\cK, T)$, the PDE solution is
	\begin{equation}\label{eq:freezcoeffsol}
		\footnotesize
		\begin{split}
			\gamma_{\textbf{fr}}(x,t)=&\frac{1}{2}erfc\Bigg(\frac{\cK-x-a(\cK,T) (T-t)}{\abs{b(\cK,T)}\sqrt{2(T-t)}}\Bigg)\\
			&+\frac{1}{2}e^{\frac{a(\cK,T)(\cK-x)}{b^2(\cK,T)/2}}erfc\Bigg(\frac{\cK-x+a(\cK,T) (T-t)}{\abs{b(\cK, T)}\sqrt{2(T-t)}}\Bigg).
		\end{split}
	\end{equation}	
   The optimal control based on~\eqref{eq:freezcoeffsol} is
	\begin{equation}\label{eq:optcontfrozencoeff}
	\footnotesize
	\begin{split}
		\xi_{\textbf{fr}}(x,t)=& b(\cK,T) \frac{\partial (\log(\gamma_{\textbf{fr}}(x,t)))}{\partial x}= \frac{b(\cK,T) }{\gamma_{\textbf{fr}}(x,t)}\frac{\partial (\gamma_{\textbf{fr}}(x,t))}{\partial x}\\
		&=\frac{b(\cK,T) }{\gamma_{\textbf{fr}}(x,t)} \Bigg[\frac{1}{\abs{b(\cK,T) }\sqrt{2\pi (T-t)}}\Big(e^{-(R^{-}(x))^2}+e^{-(R^{+}(x))^2+\frac{2a(\cK,T) (K-x)}{b(\cK,T) ^2}}\Big)\\
		&-\frac{a(\cK,T) }{b(\cK,T) ^2}e^{\frac{2a(\cK,T) (K-x)}{b(\cK,T) ^2}}erfc(R^{+}(x))\Bigg],
	\end{split}
\end{equation}	
where $R^{\pm}(x):=\frac{K-x\pm a(\cK,T) (T-t)}{\abs{b(\cK,T) }\sqrt{2(T-t)}}$. 

\red{Asymtotically},  as $t\rightarrow T$, the optimal control~\eqref{eq:optcontfrozencoeff} behaves as
\[
	\xi_{\textbf{fr}}^{\infty}(x,t)=\frac{1}{b(\cK, T)} \Big(\frac{K-x}{T-t}\Big)-a(\cK, T).
\]
\end{frame}

%
%\begin{frame}{Optimal control problem of the prelast step}
%	\begin{equation}
%		\left\{
%		\begin{array}{ll}
%			X_T=X_0+(a+b\xi)\Delta t+b\Delta W_T,\\
%			Y_T=2\xi\Delta W_T + \xi^2 \Delta t,
%		\end{array}
%		\right.
%	\end{equation}
%	where $a:=a(X_0)$, $b:=b(X_0)$, $\Delta W_T = \sqrt{\Delta t} \epsilon_T \sim N(0,\Delta t)$.
%	
%	We want to minimize the 2nd moment
%	$$\min_{\xi} \E [\Ind{}{X_T\geq K}e^{-Y_T}|X_0=x].$$
%	Note that $X_T\geq K \iff \epsilon_T \geq \frac{K-x-(a+b\xi)\Delta t}{b\sqrt{dt}}$. Then,
%	\begin{equation}
%		\begin{split}
%			&\E [\Ind{}{X_T\geq K}e^{-Y_T}|X_0=x]=\frac{1}{\sqrt{2\pi}}\int_{\frac{K-x-(a+b\xi)\Delta t}{b\sqrt{dt}}}^{\infty} e^{-2\xi\sqrt{\Delta t}z-\xi^2\Delta t-\frac{z^2}{2}}dz\\
%			&=\frac{e^{\xi^2\Delta t}}{\sqrt{2\pi}}\int_{\frac{K-x-(a+b\xi)\Delta t}{b\sqrt{dt}}}^{\infty} e^{-(\frac{z}{\sqrt{2}}+\xi\sqrt{2\Delta t})^2}dz=\frac{e^{\xi^2\Delta t}}{\sqrt{\pi}}\int_{\frac{K-x-(a-b\xi)\Delta t}{b\sqrt{dt}}}^{\infty} e^{-s^2}ds\\
%			&=\frac{e^{\xi^2\Delta t}}{2}erfc(\frac{K-x-(a-b\xi)\Delta t}{b\sqrt{2\Delta t}}).
%		\end{split}
%	\end{equation}
%\end{frame}
%
%\begin{frame}{Optimal control problem of the prelast step}
%	The target function to minimise is 
%	$$F(\xi) = \frac{e^{\xi^2\Delta t}}{2}erfc\Big(\frac{K-x-(a-b\xi)\Delta t}{b\sqrt{2\Delta t}}\Big).$$
%	The derivative of $F$ is
%	\begin{equation}
%		\begin{split}
%			F'(\xi)&= \xi \Delta t e^{\xi^2 \Delta t} erfc \Big(\frac{K-x-(a-b\xi)\Delta t}{b\sqrt{2\Delta t}}\Big)\\
%			&-\sqrt{\frac{\Delta t}{2\pi}} e^{-\frac{(K-x-a\Delta t)^2}{2b^2 \Delta t}-\frac{(K-x-a\Delta t)\xi}{b}+0.5\xi^2\Delta t}.
%		\end{split}
%	\end{equation}
%	The second derivative of $F$ needed for Newton's method is
%	\begin{equation}
%		\begin{split}
%			F''(\xi)&= (\Delta t e^{\xi^2 \Delta t}+2\xi^2\Delta t^2e^{\xi^2\Delta t} ) erfc \Big(\frac{K-x-(a-b\xi)\Delta t}{b\sqrt{2\Delta t}}\Big)\\
%			&-\frac{\sqrt{2\Delta t}}{\sqrt{\pi}}\xi\Delta t e^{\xi^2 \Delta t-\frac{(K-x-(a-b\xi)\Delta t)^2}{2b^2\Delta t}}\\
%			&-\sqrt{\frac{\Delta t}{2\pi}}\Big(\xi\Delta t -\frac{(K-x-a\Delta t)}{b}\Big)e^{-\frac{(K-x-a\Delta t)^2}{2b^2 \Delta t}-\frac{(K-x-a\Delta t)\xi}{b}+0.5\xi^2\Delta t}.
%		\end{split}
%	\end{equation}
%\end{frame}

\begin{frame}{PDE solver with frozen coeff. in the corner}
	We propose to solve~\eqref{eq:BKEpde} in the following way:\\
	\begin{figure}[h!]
		\includegraphics[width=0.5\linewidth]{PDEsolveridea1.png} 
		\label{fig:wrapfig}
	\end{figure}
	\red{(I)} at $(x,t)\in [\cK-\Delta x, \cK]\times [T-\Delta t, T] $
	\begin{equation}
		\begin{split}
			\gamma_{\textbf{fr}}(x,t)=&\frac{1}{2}erfc\Bigg(\frac{\cK-x-a(\cK,T) (T-t)}{2\sqrt{\frac{b^2(\cK,T)}{2}(T-t)}}\Bigg)\\
			&+\frac{1}{2}e^{\frac{a(\cK,T)(\cK-x)}{b^2(\cK,T)/2}}erfc\Bigg(\frac{\cK-x+a(\cK,T) (T-t)}{2\sqrt{\frac{b^2(\cK,T)}{2}(T-t)}}\Bigg).
		\end{split}
	\end{equation}
\end{frame}

\begin{frame}
	\blue{(II)} at $(x,t)\in [-\infty, \cK-\Delta x) \times [T-\Delta t, T]$
	\begin{equation}
		\left\{
		\begin{array}{ll}
			\gamma_1(x,T)=0,\\
			\gamma_1(\cK-\Delta x, t)=\gamma_{\textbf{fr}}(\cK-\Delta x, t).
		\end{array}
		\right.
	\end{equation}
	We use the Crank-Nicolson scheme to approximate $\gamma_1$ in the given region.
	
	\green{(III)} at $(x,t)\in [-\infty, \cK) \times [0, T-\Delta t]$
	\begin{equation}
		\begin{split}
			&\gamma_2(x,T-\Delta t) = \left\{
			\begin{array}{ll}
				\gamma_1(x,T-\Delta t), \; x<\cK-\Delta x,\\
				\gamma_{\textbf{fr}}(x,T-\Delta t), \; x\geq \cK-\Delta x,
			\end{array}
			\right.\\
			&\gamma_2(\cK, t) =1.
		\end{split}
	\end{equation}
	We use the Crank-Nicolson scheme to approximate $\gamma_2$ in the given region.
	
	We denote the PDE solution obtained via this strategy by $\gamma^{\textbf{PDE}}_{\textbf{fr}}(x,t)$ and the optimal control correspondingly $\xi^{\textbf{PDE}}_{\textbf{fr}}(x,t)$. In num.tests, we set $\Delta x^{PDE} = 0.005$, $\Delta t^{PDE} =(\Delta x^{PDE})^2$, $\Delta t = 0.001$ and $\Delta x =0.02$.
	
	We also define the asymptotic optimal control for non-constant coefficients by
	\[
	\xi^{\infty}(x,t)=\frac{1}{b(x, t)} \Big(\frac{K-x}{T-t}\Big)-a(x, t).
	\]
\end{frame}

\begin{frame}{Optimal Controls}
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=4.2cm, width=5.5cm]{OptControl_Asymptotic.png}
		\includegraphics[height=4.2cm, width=5.5cm]{OptControlAnalPDEsol.png}
		\includegraphics[height=4.2cm, width=5.5cm]{OptControlExpSmooting.png}
	\end{figure}
\end{frame}

\begin{frame}{IS simulation strategy}
	
\begin{algorithm}[H]
	\DontPrintSemicolon
	
	\KwInput{ \small $T$, $N_t$, $\cK$, model parameter $b$.}
	%\KwOutput{}
	$\Delta t_{EM}=\frac{T}{N_t}$; 
	
	Set
	\begin{equation}
		\small
	\xi(x,t)= 	\left\{
		\begin{array}{ll}
		\xi_{\textbf{fr}}^{\textbf{PDE}}(x,t),  \; \mbox{   if  } T-t\geq 2 \Delta t_{EM},\\
		\xi^{\infty}(x,t), \quad \mbox{  if  }  T-t\leq 2 \Delta t_{EM};
		\end{array}
		\right.
	\end{equation}

	$n=0$, $t_n=0$, $\Delta t_n = \Delta t_{EM} $, $X(0)=x_0$, $Y(0)=0$;
	
	\While{$(t_n\leq T)$, $(X(n)<\cK)$ and $(\Delta t_n >0.0001)$}
	{  $\Delta t_n = \min (\Delta t_{EM}, \frac{T-t_n}{2})$;
		
		Simulate
       	\begin{equation}
       	\left\{
       	\begin{array}{ll}
       		X(n+1) = X(n) + (a(X(n))+b\xi(X(n),t_n)) \Delta t_n +b \Delta W_n \\
            Y(n+1) = Y(n) + \xi^2(X(n), t_n)\Delta t_n + 2\xi(X(n), t_n) \Delta W_n;
       	\end{array}
       	\right.
       \end{equation}
   
		$n=n+1$;
		
		$t_n=t_n+\Delta t_n$
	}
	\caption{Sketch of the adaptive time-stepping}
\end{algorithm}
\end{frame}

\begin{frame}{Double Well example}
		\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|ccccl|l|l|l|l|}
				\hline
				\multirow{2}{*}{$\cK$} & \multicolumn{1}{c|}{\textbf{Estimator} }            & \multicolumn{5}{c|}{\textbf{Relative statistical error}}   &\multicolumn{4}{c|}{\textbf{Variance reduction}}\\\cline{2-11}  
				&  \multicolumn{1}{c|}{\multirow{2}{*}{$\hat{\alpha}$}} & \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{MC}$} }& \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{CE,\tilde{\rho}_{u_0}}$}} & \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{PDE,\tilde{\rho}_{u_0}}$} }&\multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{PDE,dW}$} }& \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{PDE,\text{both}}$} }& \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{CE,\tilde{\rho}_{u_0}}}$}& \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE,\tilde{\rho}_{u_0}}}$} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE,dW}}$}&\multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \text{both}}}$}\\  [2ex]\hline
				0                 & \multicolumn{1}{c|}{$4.0 \times 10^{-2}$}& \multicolumn{1}{c|}{3.1\%}     &  \multicolumn{1}{c|}{3.0\%} & \multicolumn{1}{c|}{2.9\%} & \multicolumn{1}{c|}{0.8\%}&\multicolumn{1}{c|}{0.5\%} &\multicolumn{1}{c|}{1.0} & \multicolumn{1}{c|}{1.2} &\multicolumn{1}{c|}{15} &\multicolumn{1}{c|}{37} \\ \hline
				0.5                & \multicolumn{1}{c|}{$4.3\times 10^{-3}$}& \multicolumn{1}{c|}{9.8\%}     &  \multicolumn{1}{c|}{8.8\%} & \multicolumn{1}{c|}{8.4\%} & \multicolumn{1}{c|}{1.2\%}&\multicolumn{1}{c|}{0.5\%} &\multicolumn{1}{c|}{1.25} & \multicolumn{1}{c|}{1.3}&\multicolumn{1}{c|}{63}&\multicolumn{1}{c|}{364}\\ \hline
				1              &  \multicolumn{1}{c|}{$1.6\times 10^{-4}$} &\multicolumn{1}{c|}{46\%}     &  \multicolumn{1}{c|}{-\% }&\multicolumn{1}{c|}{41\% }& \multicolumn{1}{c|}{2.0\%}&\multicolumn{1}{c|}{0.5\%} &\multicolumn{1}{c|}{-} &\multicolumn{1}{c|}{1.7} &\multicolumn{1}{c|}{736} &\multicolumn{1}{c|}{9956}\\ \hline
				%				1.2              &  \multicolumn{1}{c|}{$2.4 \times 10^{-5}$}& \multicolumn{1}{c|}{43.8\%}     & \multicolumn{1}{c|}{-\%} & \multicolumn{1}{c|}{27.9\%} & \multicolumn{1}{c|}{0.7\%}&\multicolumn{1}{c|}{0.09\%}&\multicolumn{1}{c|}{-} &\multicolumn{1}{c|}{1.84}&\multicolumn{1}{c|}{2678}&\multicolumn{1}{c|}{165435} \\ \hline
			\end{tabular}
		\end{adjustbox}
		\caption{\textbf{Based on fully $\xi^{\infty}(x,t)$. } Model parameter: $b=0.5$. Simulation parameters: $T=1$, $\Delta t_{EM}= 0.01$,  $S = 10^5$. }
		\label{table:DWsmallsigma}
	\end{table}

	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|ccccl|l|l|l|l|}
				\hline
				\multirow{2}{*}{$\cK$} & \multicolumn{1}{c|}{\textbf{Estimator} }            & \multicolumn{5}{c|}{\textbf{Relative statistical error}}   &\multicolumn{4}{c|}{\textbf{Variance reduction}}\\\cline{2-11}  
				&  \multicolumn{1}{c|}{\multirow{2}{*}{$\hat{\alpha}$}} & \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{MC}$} }& \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{CE,\tilde{\rho}_{u_0}}$}} & \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{PDE,\tilde{\rho}_{u_0}}$} }&\multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{PDE,dW}$} }& \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{PDE,\text{both}}$} }& \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{CE,\tilde{\rho}_{u_0}}}$}& \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE,\tilde{\rho}_{u_0}}}$} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE,dW}}$}&\multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \text{both}}}$}\\  [2ex]\hline
				0                 & \multicolumn{1}{c|}{$4.0 \times 10^{-2}$}& \multicolumn{1}{c|}{3.1\%}     &  \multicolumn{1}{c|}{3.0\%} & \multicolumn{1}{c|}{2.9\%} & \multicolumn{1}{c|}{0.5\%}&\multicolumn{1}{c|}{0.2\%} &\multicolumn{1}{c|}{1.0} & \multicolumn{1}{c|}{1.2} &\multicolumn{1}{c|}{31} &\multicolumn{1}{c|}{376} \\ \hline
				0.5                & \multicolumn{1}{c|}{$4.3\times 10^{-3}$}& \multicolumn{1}{c|}{9.6\%}     &  \multicolumn{1}{c|}{8.8\%} & \multicolumn{1}{c|}{8.4\%} & \multicolumn{1}{c|}{0.9\%}&\multicolumn{1}{c|}{0.2\%} &\multicolumn{1}{c|}{1.25} & \multicolumn{1}{c|}{1.3}&\multicolumn{1}{c|}{108}&\multicolumn{1}{c|}{2253}\\ \hline
				1              &  \multicolumn{1}{c|}{$1.6\times 10^{-4}$} &\multicolumn{1}{c|}{50\%}     &  \multicolumn{1}{c|}{-\% }&\multicolumn{1}{c|}{41\% }& \multicolumn{1}{c|}{0.2\%}&\multicolumn{1}{c|}{0.03\%} &\multicolumn{1}{c|}{-} &\multicolumn{1}{c|}{1.7} &\multicolumn{1}{c|}{699} &\multicolumn{1}{c|}{41504}\\ \hline
%				1.2              &  \multicolumn{1}{c|}{$2.4 \times 10^{-5}$}& \multicolumn{1}{c|}{43.8\%}     & \multicolumn{1}{c|}{-\%} & \multicolumn{1}{c|}{27.9\%} & \multicolumn{1}{c|}{0.7\%}&\multicolumn{1}{c|}{0.09\%}&\multicolumn{1}{c|}{-} &\multicolumn{1}{c|}{1.84}&\multicolumn{1}{c|}{2678}&\multicolumn{1}{c|}{165435} \\ \hline
			\end{tabular}
		\end{adjustbox}
		\caption{\textbf{ Based on the combination of $\xi^{\textbf{PDE}}_{\textbf{fr}}(x,t)$ and $\xi^{\infty}(x,t)$.} Model parameter: $b=0.5$. Simulation parameters: $T=1$, $\Delta t_{EM}= 0.01$,  $S = 10^5$. }
		\label{table:DWsmallsigma}
	\end{table}
\end{frame}

\begin{frame}{IS trajectories $M=100$}
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=4.2cm, width=5.5cm]{Traj_X_fullyAsymApprx.png}
		\includegraphics[height=4.2cm, width=5.5cm]{Traj_Y_fullyAsymApprx.png}
		\hspace*{-1cm}
		\includegraphics[height=4.2cm, width=5.5cm]{Traj_X_fullyComb.png}
		\includegraphics[height=4.2cm, width=5.5cm]{Traj_Y_fullyComb.png}
	\end{figure}
\end{frame}

\begin{frame}{Stopping time distribution}
	\begin{figure}[h!]
		\centering
		\hspace*{-1cm}
		\includegraphics[height=4.2cm, width=5.5cm]{StopTimeDist_AsymApprx.png}
		\includegraphics[height=4.2cm, width=5.5cm]{StopTimeDist_CombPDE_Asymp.png}
		\includegraphics[height=4.2cm, width=5.5cm]{StopTimeDist_ExpSmooth.png}
	\end{figure}
\end{frame}





\begin{frame}{IS simulation strategy: Gobet correction}
	
	\begin{algorithm}[H]
		\DontPrintSemicolon
		
		\KwInput{ \small $T$, $N_t$, $\cK$, model parameter $b$.}
		%\KwOutput{}
		$\Delta t_{EM}=\frac{T}{N_t}$; 
		
		Set
		\begin{equation}
			\small
			\xi(x,t)= 	\left\{
			\begin{array}{ll}
				\xi_{\textbf{fr}}^{\textbf{PDE}}(x,t),  \; \mbox{   if  } T-t\geq 2 \Delta t_{EM},\\
				\xi^{\infty}(x,t), \quad \mbox{  if  }  T-t\leq 2 \Delta t_{EM};
			\end{array}
			\right.
		\end{equation}
		
		$n=0$, $t_n=0$, $\Delta t = \Delta t_{EM} $, $X(0)=x_0$, $Y(0)=0$, $Q=1$;
		
		\While{$(t_n\leq T)$, $(\bar{X}(n)<\cK)$ and $(\Delta t >0.0001)$}
		{  $\Delta t = \min (\Delta t_{EM}, \frac{T-t_n}{2})$;
			
			Simulate $(\bar{X}(n+1), \bar{Y}(n+1))$

          Compute exiting probability: $q=e^{-\frac{2(\cK-\bar{X}(n))_{+}(\cK-\bar{X}(n+1))_{+}}{b^2 \Delta t}}$;
		 
		  Q=Q*(1-q); Sample $r \sim U(0,1)$
		 
	    	If $r<q$, then 
	    	$t_n=t_n+0.5\Delta t$, $\bar{X}(n+1)=\cK$,
	    	else
			$t_n=t_n+\Delta t$
            
	     	$n=n+1$;			
		}
	Compute $Q*e^{Y_{t_n}}$
		\caption{Sketch of the adaptive time-stepping with Gobet-type correction}
	\end{algorithm}
\end{frame}
\end{document}