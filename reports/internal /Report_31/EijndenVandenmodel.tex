% Modelo de slides para projetos de disciplinas do Abel
\documentclass[10pt]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}
\usepackage[numbers,sort&compress]{natbib}
\bibliographystyle{plainnat}
\usepackage{adjustbox}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{multirow}
\usepackage{xspace}
\usepackage{mathtools}

\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\green}[1]{{\color{green} #1}}
\newcommand{\magenta}[1]{{\color{magenta} #1}}
\title{Rare events and filtering}
 \subtitle{Charney-deVore model}
 \date{\today}
\date{Apr, 2024}
\author{Gaukhar Shaimerdenova}
\institute{AMCS, KAUST}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\V}{\mathbb{V}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\bu}{\boldsymbol{u}}
\newcommand{\bv}{\boldsymbol{v}}
\newcommand{\bx}{\boldsymbol{x}}
\newcommand{\ba}{\boldsymbol{a}}
\newcommand{\bb}{\boldsymbol{b}}
\newcommand{\bX}{\boldsymbol{X}}
\newcommand{\bK}{\boldsymbol{K}}
\newcommand{\brho}{\boldsymbol{\rho}}
\newcommand{\bmu}{\boldsymbol{\mu}}
\newcommand{\bSigma}{\boldsymbol{\Sigma}}
\newcommand{\bW}{\boldsymbol{W}}

%%%%%%%%   Macros we will need for this paper %%%%%%%%%%%%%%%

\newcommand{\norm}[1]{\left\| #1 \right\|}
\newcommand{\abs}[1]{\left| #1 \right|}
\newcommand{\ceil}[1]{\left \lceil #1 \right \rceil}
\newcommand{\prt}[1]{\left( #1 \right)}
\newcommand{\cost}[1]{\mathrm{Cost}}
\newcommand{\Tol}{\mathrm{TOL}}

% mathbb font letters 
\newcommand{\bF}{\mathbb{F}}
\newcommand{\bN}{\mathbb{N}}
\newcommand{\bP}{\mathbb{P}}
\newcommand{\bR}{\mathbb{R}}
\newcommand{\bZ}{\mathbb{Z}}
\newcommand{\bT}{\mathbf{T}}

% mathcal capital letters
\newcommand{\cB}{\mathcal{B}}
\newcommand{\cF}{\mathcal{F}}
\newcommand{\cO}{\mathcal{O}}
\newcommand{\cI}{\mathcal{I}}
\newcommand{\cJ}{\mathcal{J}}
\newcommand{\cK}{\mathcal{K}}
\newcommand{\cL}{\mathcal{L}}
\newcommand{\cN}{\mathcal{N}}
\newcommand{\cT}{\mathcal{T}}

%bold letters
\newcommand{\bw}{\boldsymbol{w}}

% ensemble notation 
\newcommand{\vBar}{\bar v}
\newcommand{\vHat}{\hat{v}}
\newcommand{\vBarHat}{\hat{\bar{v}}}
\newcommand{\meanHatMC}[1]{\hat{m}^{\rm{MC}}_{#1}}
\newcommand{\covHatMC}[1]{\widehat{C}^{\rm{MC}}_{#1}}
\newcommand{\yTilde}[1]{\tilde{y}_{#1}}
\newcommand{\meanHatML}[1]{\hat{m}^{\rm{ML}}_{#1}}
\newcommand{\covHatML}[1]{\widehat{C}^{\rm{ML}}_{#1}}
\newcommand{\kMC}[1]{K^{\rm{MC}}_{#1}}


% statistics notation
\newcommand{\Prob}[1]{\bP_{#1}}
\newcommand{\Ex}[1]{\E \left[ #1 \right]}
\newcommand{\Var}[1]{\V \left[ #1 \right]}
\newcommand{\Cov}{\overline{\mathrm{Cov}}}
\newcommand{\D}{\mathrm{D}}
\newcommand{\Ind}[2]{\textbf{1}_{\{#2\}}}

% colors
\newcommand{\blue}[1]{{\color{blue} #1}}


\begin{document}

\maketitle

\begin{frame}{A simple climate model with metastability}
	A  noisy Charney-deVore model 
	\begin{equation}\label{Langevin}
		\begin{cases} 
			du_1=(\tilde{\gamma}_1u_3-C(u_1-u_1^*))dt+ bdW_1, \\
			du_2=(-(\alpha_1u_1-\beta_1)u_3-Cu_2-\delta_1u_4u_6)dt+bdW_2,\\
			du_3=((\alpha_1u_1-\beta_1)u_2 - \gamma_1u_1-Cu_3+\delta_1u_4u_5)dt+bdW_3,\;\\
			du_4=(\tilde{\gamma}_2u_6-C(u_4-u_4^*)+\eta(u_2u_6-u_3u_5))dt+b dW_4,\;\\
			du_5=(-(\alpha_2u_1-\beta_2)u_6-Cu_5-\delta_2u_3u_4)dt+bdW_5,\;			\\	
			du_6=((\alpha_2u_1-\beta_2)u_5-\gamma_2u_4-Cu_6+\delta_2u_2u_4)dt+bdW_6,\;
		\end{cases}\,
	\end{equation}
	where for $m\in{1,2}$,
	\begin{equation*}
		\begin{split}
		&\alpha_m=\frac{8\sqrt{2}}{\pi}\frac{m^2}{4m^2-1}\frac{q^2+m^2-1}{b^2+m^2}, \quad \beta_m=\frac{\beta q^2}{q^2+m^2},\\
		&\gamma_m=\gamma\frac{ \sqrt{2}q}{\pi}\frac{4m^3}{(4m^2-1)(q^2+m^2)}, \quad \tilde{\gamma}_m=\gamma \frac{\sqrt{2}q}{\pi}\frac{4m}{4m^2-1},\\
		&\delta_m =\frac{64\sqrt{2}}{15\pi}\frac{q^2-m^2+1}{q^2+m^2}, \quad \eta=\frac{16\sqrt{2}}{5\pi}.
		\end{split}
	\end{equation*}

\end{frame}

\begin{frame}{Simulation details:}
 We use the following model parameters
 \begin{itemize}
 	\item  a zonally symmetric forcing profile by setting $\boldsymbol{u_1^*=0.95}$ and $\boldsymbol{u_4^*=-0.76095}$;
 	
 	\item $C$ is the thermal relaxation timescale, we set to $\boldsymbol{C=0.1}$ describing a damping time of $\sim 10d$;
 	
 	\item $\gamma$ is the orographic amplitude, we set to $\boldsymbol{\gamma=0.2}$, corresponding to a $200m$ amplitude;
 	
 	\item $\beta$ is the Coriolis parameter, we set to $\boldsymbol{\beta=1.25}$, defining a central latitude of $45^o$;
 	
 	\item $q$ defines the channel half-width, we set to $\boldsymbol{q=0.5}$, which gives a channel of $6300km\times 1600km$.
 
 \end{itemize}
and the diffusion parameter $\boldsymbol{b=\sqrt{2\epsilon}}$ and the initial condition is $\boldsymbol{u_0\sim N([0.7650, 0.2288, -0.2990, -0.3657, -0.1636, 0.3108], \sigma_0^2I)}$
 \end{frame}

\begin{frame}{Charney-deVore model }
		The model exibits a quasi-periodic nature: blocking and zonal regime
	\begin{figure}
		\vspace*{-0.2cm}
		\centering
		\includegraphics[height=3.8cm, width=4.5cm]{devore_u1_b0.png}
		\includegraphics[height=3.8cm, width=4.5cm]{devore_u1_b00141.png}
		\includegraphics[height=3.8cm, width=4.5cm]{devore_u1_b014.png}
		\includegraphics[height=3.8cm, width=4.5cm]{devore_u1_b04472.png}
	\end{figure}

\end{frame}


\begin{frame}{A noisy Charney-deVore model: $\epsilon=0.01$}
	\bigskip
	\begin{table}[h!]
	\begin{adjustbox}{width=\columnwidth,center}
		\begin{tabular}{|c|c|c|c|c|l|}
			\hline
			\multirow{2}{*}{$\cK$} & \multicolumn{5}{c|}{\textbf{Estimator with 95\% CI} } \\ \cline{2-6}  
			&  \multicolumn{1}{c|}{\multirow{2}{*}{$\hat{\alpha}^{MC}$}} & \multicolumn{1}{c|}{\multirow{2}{*}{$\hat{\alpha}^{CE,\tilde{\rho}_{0}}$} }& \multicolumn{1}{c|}{\multirow{2}{*}{$\hat{\alpha}^{PDE,\tilde{\rho}_{0}}$}} & \multicolumn{1}{c|}{\multirow{2}{*}{$\hat{\alpha}^{PDE,W_t}$} }&\multicolumn{1}{c|}{\multirow{2}{*}{$\hat{\alpha}^{PDE,\text{both}}$} }\\  [2ex]\hline
			\multirow{2}{*}{$\mathbf{1.1}$}  & \multicolumn{1}{c|}{$\mathbf{3.2 \times 10^{-2}}$}& \multicolumn{1}{c|}{$\mathbf{-}$}     &  \multicolumn{1}{c|}{$\mathbf{3.0 \times 10^{-2}}$} & \multicolumn{1}{c|}{$\mathbf{3.1\times 10^{-2}}$} & \multicolumn{1}{c|}{$\mathbf{3.1 \times 10^{-2}}$} \\
			& $[0.0308, 0.0330]$& &$[0.0294, 0.0313]$ &$[0.0306, 0.0311]$&$[0.0308, 0.0312]$\\  \hline
			
			\multirow{2}{*}{$\mathbf{1.2}$}    & \multicolumn{1}{c|}{$\mathbf{4.1\times 10^{-3}}$}& \multicolumn{1}{c|}{$\mathbf{-}$}     &  \multicolumn{1}{c|}{$\mathbf{4.2 \times 10^{-3}}$} & \multicolumn{1}{c|}{$\mathbf{4.1 \times 10^{-3}}$} & \multicolumn{1}{c|}{$\mathbf{4.1 \times 10^{-3}}$} \\
			& $[0.00401, 0.00426]$& &$[0.00405, 0.00427]$ &$[0.004091, 0.00412]$&$[0.004099, 0.00412]$\\  \hline
			\multirow{2}{*}{$\mathbf{1.3}$}    & \multicolumn{1}{c|}{$\mathbf{3.5 \times 10^{-4}}$}& \multicolumn{1}{c|}{$\mathbf{-}$}     &  \multicolumn{1}{c|}{$\mathbf{4.1 \times 10^{-4}}$} & \multicolumn{1}{c|}{$\mathbf{3.5 \times 10^{-4}}$} & \multicolumn{1}{c|}{$\mathbf{3.44 \times 10^{-4}}$} \\
			& $[2.3\text{e-}04, 4.7\text{e-}04]$& &$[3.1\text{e-}04, 5.2\text{e-}04]$ &$[3.42\text{e-}04, 3.54\text{e-}04]$&$[3.41\text{e-}04, 3.46\text{e-}04]$\\  \hline
			\multirow{2}{*}{$\mathbf{1.4}$}    & \multicolumn{1}{c|}{$\mathbf{2.6\times 10^{-5}}$}& \multicolumn{1}{c|}{$-$}     &  \multicolumn{1}{c|}{$\mathbf{2.1 \times 10^{-5}}$} & \multicolumn{1}{c|}{$\mathbf{1.8\times 10^{-5}}$} & \multicolumn{1}{c|}{$\mathbf{1.8\times 10^{-5}}$}\\
			& $[1.6\text{e-}05, 3.6\text{e-}05]$& &$[1.4\text{e-}05, 2.7\text{e-}05]$ &$[1.797\text{e-}05, 1.83\text{e-}05]$&$[1.797\text{e-}05, 1.82\text{e-}05]$\\  \hline
		\end{tabular}
	\end{adjustbox}
	\newline
	\vspace*{0.2 cm}
	\begin{adjustbox}{width=\columnwidth,center}
		\begin{tabular}{|c|ccccl|l|l|l|l|}
			\hline
			\multirow{2}{*}{$\cK$}     & \multicolumn{5}{c|}{\textbf{Relative statistical error}}   &\multicolumn{4}{c|}{\textbf{Variance reduction}}\\\cline{2-10}  
			& \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{MC}$} }& \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{CE,\tilde{\rho}_{0}}$}} & \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{PDE,\tilde{\rho}_{0}}$} }&\multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{PDE,W_t}$} }& \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{PDE,\text{both}}$} }& \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{CE,\tilde{\rho}_{0}}}$}& \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE,\tilde{\rho}_{0}}}$} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE,W_t}}$}&\multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \text{both}}}$}\\  [2ex]\hline
			$\mathbf{1.1}$          & \multicolumn{1}{c|}{$\mathbf{3.4\%}$}     &  \multicolumn{1}{c|}{$\mathbf{-\%}$} & \multicolumn{1}{c|}{$\mathbf{3.2\%}$} & \multicolumn{1}{c|}{$\mathbf{0.8\%}$}&\multicolumn{1}{c|}{$\mathbf{0.7\%}$} &\multicolumn{1}{c|}{$\mathbf{-} $} & \multicolumn{1}{c|}{$\mathbf{1.3} $} &\multicolumn{1}{c|}{$\mathbf{21} $} &\multicolumn{1}{c|}{$\mathbf{26} $} \\ \hline
			$\mathbf{1.2}$          & \multicolumn{1}{c|}{$\mathbf{3.0\%}$}     &  \multicolumn{1}{c|}{$\mathbf{-\%}$} & \multicolumn{1}{c|}{$\mathbf{2.6\%}$} & \multicolumn{1}{c|}{$\mathbf{0.4\%}$}&\multicolumn{1}{c|}{$\mathbf{0.3\%}$} &\multicolumn{1}{c|}{$\mathbf{-} $} & \multicolumn{1}{c|}{$\mathbf{1.3} $} &\multicolumn{1}{c|}{$\mathbf{68} $} &\multicolumn{1}{c|}{$\mathbf{139} $} \\ \hline
			$\mathbf{1.3}$          & \multicolumn{1}{c|}{$\mathbf{33\%}$}     &  \multicolumn{1}{c|}{$\mathbf{-\%}$} & \multicolumn{1}{c|}{$\mathbf{25\%}$} & \multicolumn{1}{c|}{$\mathbf{1.8\%}$}&\multicolumn{1}{c|}{$\mathbf{0.7\%}$} &\multicolumn{1}{c|}{$\mathbf{-} $} & \multicolumn{1}{c|}{$\mathbf{1.2} $} &\multicolumn{1}{c|}{$\mathbf{342} $} &\multicolumn{1}{c|}{$\mathbf{2142} $} \\ \hline
			$\mathbf{1.4}$          & \multicolumn{1}{c|}{$\mathbf{38\%}$}     &  \multicolumn{1}{c|}{$\mathbf{-\%}$} & \multicolumn{1}{c|}{$\mathbf{32\%}$} & \multicolumn{1}{c|}{$\mathbf{0.8\%}$}&\multicolumn{1}{c|}{$\mathbf{0.2\%}$} &\multicolumn{1}{c|}{$\mathbf{-} $} & \multicolumn{1}{c|}{$\mathbf{2.4} $} &\multicolumn{1}{c|}{$\mathbf{4709} $} &\multicolumn{1}{c|}{$\mathbf{11611} $}  \\ \hline
		\end{tabular}
	\end{adjustbox}
	\caption{Model parameter: $b=0.1414$. Simulation parameters: $T=1$, $\Delta t= 0.01$,  $J = 10^5$, $\sigma_0^2=0.0025$, ( $J = 10^6$ for the last row)
	}
	\label{table:DWlargesigma}
\end{table}
\end{frame}

\begin{frame}{A noisy Charney-deVore model: $\epsilon=0.1$}
	\bigskip
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|l|}
				\hline
				\multirow{2}{*}{$\cK$} & \multicolumn{5}{c|}{\textbf{Estimator with 95\% CI} } \\ \cline{2-6}  
				&  \multicolumn{1}{c|}{\multirow{2}{*}{$\hat{\alpha}^{MC}$}} & \multicolumn{1}{c|}{\multirow{2}{*}{$\hat{\alpha}^{CE,\tilde{\rho}_{0}}$} }& \multicolumn{1}{c|}{\multirow{2}{*}{$\hat{\alpha}^{PDE,\tilde{\rho}_{0}}$}} & \multicolumn{1}{c|}{\multirow{2}{*}{$\hat{\alpha}^{PDE,W_t}$} }&\multicolumn{1}{c|}{\multirow{2}{*}{$\hat{\alpha}^{PDE,\text{both}}$} }\\  [2ex]\hline
				\multirow{2}{*}{$\mathbf{1.9}$}  & \multicolumn{1}{c|}{$\mathbf{1.0 \times 10^{-2}}$}& \multicolumn{1}{c|}{$\mathbf{-}$}     &  \multicolumn{1}{c|}{$\mathbf{1.0\times 10^{-2}}$} & \multicolumn{1}{c|}{$\mathbf{1.0\times 10^{-2}}$} & \multicolumn{1}{c|}{$\mathbf{1.0\times 10^{-2}}$} \\
				& $[0.0097, 0.0110]$& &$[0.0096, 0.0108]$&$[0.00996, 0.01003]$ &$[0.00998, 0.01003]$\\  \hline
				
				\multirow{2}{*}{$\mathbf{2}$}    & \multicolumn{1}{c|}{$\mathbf{5.3\times 10^{-3}}$}& \multicolumn{1}{c|}{$\mathbf{-}$}     &  \multicolumn{1}{c|}{$\mathbf{5.2 \times 10^{-3}}$} & \multicolumn{1}{c|}{$\mathbf{5.0 \times 10^{-3}}$} & \multicolumn{1}{c|}{$\mathbf{5.0 \times 10^{-3}}$} \\
				& $[0.0049, 0.0058]$& &$[0.0048, 0.0057]$ &$[0.00494, 0.00497]$&$[0.00494, 0.00496]$\\  \hline
				\multirow{2}{*}{$\mathbf{2.4}$}    & \multicolumn{1}{c|}{$\mathbf{1.7 \times 10^{-4}}$}& \multicolumn{1}{c|}{$\mathbf{-}$}     &  \multicolumn{1}{c|}{$\mathbf{1.1 \times 10^{-4}}$} & \multicolumn{1}{c|}{$\mathbf{1.8 \times 10^{-4}}$} & \multicolumn{1}{c|}{$\mathbf{1.77 \times 10^{-4}}$} \\
				& $[8.9\text{e-}05, 2.5\text{e-}04]$& &$[4.5\text{e-}05, 1.7\text{e-}04]$ &$[1.76\text{e-}04, 1.78\text{e-}04]$&$[1.764\text{e-}04, 1.776\text{e-}04]$\\  \hline
				\multirow{2}{*}{$\mathbf{-}$}    & \multicolumn{1}{c|}{$\mathbf{- \times 10^{-5}}$}& \multicolumn{1}{c|}{$\mathbf{- \times 10^{-5}}$}     &  \multicolumn{1}{c|}{$\mathbf{- \times 10^{-5}}$} & \multicolumn{1}{c|}{$\mathbf{- \times 10^{-5}}$} & \multicolumn{1}{c|}{$\mathbf{- \times 10^{-5}}$}\\
				& $[-\text{e-}05, -\text{e-}05]$& $[-\text{e-}05, -\text{e-}05]$&$[-\text{e-}05, -\text{e-}05]$ &$[-\text{e-}05, -\text{e-}05]$&$[-\text{e-}05, -\text{e-}05]$\\  \hline
			\end{tabular}
		\end{adjustbox}
		\newline
		\vspace*{0.2 cm}
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|ccccl|l|l|l|l|}
				\hline
				\multirow{2}{*}{$\cK$}     & \multicolumn{5}{c|}{\textbf{Relative statistical error}}   &\multicolumn{4}{c|}{\textbf{Variance reduction}}\\\cline{2-10}  
				& \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{MC}$} }& \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{CE,\tilde{\rho}_{0}}$}} & \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{PDE,\tilde{\rho}_{0}}$} }&\multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{PDE,W_t}$} }& \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{PDE,\text{both}}$} }& \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{CE,\tilde{\rho}_{0}}}$}& \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE,\tilde{\rho}_{0}}}$} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE,W_t}}$}&\multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \text{both}}}$}\\  [2ex]\hline
				$\mathbf{1.9}$          & \multicolumn{1}{c|}{$\mathbf{6.1\%}$}     &  \multicolumn{1}{c|}{$\mathbf{-\%}$} & \multicolumn{1}{c|}{$\mathbf{6.1\%}$} & \multicolumn{1}{c|}{$\mathbf{0.3\%}$}&\multicolumn{1}{c|}{$\mathbf{0.3\%}$} &\multicolumn{1}{c|}{$\mathbf{-} $} & \multicolumn{1}{c|}{$\mathbf{1.0} $} &\multicolumn{1}{c|}{$\mathbf{358} $} &\multicolumn{1}{c|}{$\mathbf{593} $} \\ \hline
				$\mathbf{2}$          & \multicolumn{1}{c|}{$\mathbf{8.5\%}$}     &  \multicolumn{1}{c|}{$\mathbf{-\%}$} & \multicolumn{1}{c|}{$\mathbf{8.4\%}$} & \multicolumn{1}{c|}{$\mathbf{0.4\%}$}&\multicolumn{1}{c|}{$\mathbf{0.3\%}$} &\multicolumn{1}{c|}{$\mathbf{-} $} & \multicolumn{1}{c|}{$\mathbf{1.1} $} &\multicolumn{1}{c|}{$\mathbf{633} $} &\multicolumn{1}{c|}{$\mathbf{1140} $} \\ \hline
				$\mathbf{2.4}$          & \multicolumn{1}{c|}{$\mathbf{47.5\%}$}     &  \multicolumn{1}{c|}{$\mathbf{-\%}$} & \multicolumn{1}{c|}{$\mathbf{57.9\%}$} & \multicolumn{1}{c|}{$\mathbf{0.5\%}$}&\multicolumn{1}{c|}{$\mathbf{0.3\%}$} &\multicolumn{1}{c|}{$\mathbf{-} $} & \multicolumn{1}{c|}{$\mathbf{1.7} $} &\multicolumn{1}{c|}{$\mathbf{10010} $} &\multicolumn{1}{c|}{$\mathbf{18391} $} \\ \hline
				$\mathbf{-}$          & \multicolumn{1}{c|}{$\mathbf{-\%}$}     &  \multicolumn{1}{c|}{$\mathbf{-\%}$} & \multicolumn{1}{c|}{$\mathbf{-\%}$} & \multicolumn{1}{c|}{$\mathbf{-\%}$}&\multicolumn{1}{c|}{$\mathbf{-\%}$} &\multicolumn{1}{c|}{$\mathbf{-} $} & \multicolumn{1}{c|}{$\mathbf{-} $} &\multicolumn{1}{c|}{$\mathbf{-} $} &\multicolumn{1}{c|}{$\mathbf{-} $}  \\ \hline
			\end{tabular}
		\end{adjustbox}
		\caption{Model parameter: $b=0.4472$. Simulation parameters: $T=1$, $\Delta t= 0.01$,  $J = 10^5$, $\sigma_0^2=0.0025$.
		}
		\label{table:DWlargesigma}
	\end{table}
\end{frame}



\begin{frame}{A noisy Charney-deVore model: $\epsilon=0.0001$}
	\bigskip
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|c|c|c|c|l|}
				\hline
				\multirow{2}{*}{$\cK$} & \multicolumn{5}{c|}{\textbf{Estimator with 95\% CI} } \\ \cline{2-6}  
				&  \multicolumn{1}{c|}{\multirow{2}{*}{$\hat{\alpha}^{MC}$}} & \multicolumn{1}{c|}{\multirow{2}{*}{$\hat{\alpha}^{CE,\tilde{\rho}_{0}}$} }& \multicolumn{1}{c|}{\multirow{2}{*}{$\hat{\alpha}^{PDE,\tilde{\rho}_{0}}$}} & \multicolumn{1}{c|}{\multirow{2}{*}{$\hat{\alpha}^{PDE,W_t}$} }&\multicolumn{1}{c|}{\multirow{2}{*}{$\hat{\alpha}^{PDE,\text{both}}$} }\\  [2ex]\hline
				\multirow{2}{*}{$\mathbf{0.81}$}  & \multicolumn{1}{c|}{$\mathbf{3.0 \times 10^{-2}}$}& \multicolumn{1}{c|}{$\mathbf{-}$}     &  \multicolumn{1}{c|}{$\mathbf{2.8\times 10^{-2}}$} & \multicolumn{1}{c|}{$\mathbf{2.5\times 10^{-2}}$} & \multicolumn{1}{c|}{$\mathbf{2.4\times 10^{-2}}$} \\
				& $[0.0287, 0.0308]$& &$[0.0273, 0.0292]$&$[0.0233, 0.0261]$ &$[0.0235, 0.0247]$\\  \hline
				
				\multirow{2}{*}{$\mathbf{0.82}$}    & \multicolumn{1}{c|}{$\mathbf{3.6\times 10^{-3}}$}& \multicolumn{1}{c|}{$\mathbf{-}$}     &  \multicolumn{1}{c|}{$\mathbf{3.5 \times 10^{-3}}$} & \multicolumn{1}{c|}{$\mathbf{5.5 \times 10^{-3}}$} & \multicolumn{1}{c|}{$\mathbf{3.2 \times 10^{-3}}$} \\
				& $[0.0033, 0.0040]$& &$[0.0032, 0.0038]$ &$[0.0012, 0.010]$&$[0.0030, 0.0034]$\\  \hline
				\multirow{2}{*}{$\mathbf{0.83}$}    & \multicolumn{1}{c|}{$\mathbf{3.5 \times 10^{-4}}$}& \multicolumn{1}{c|}{$\mathbf{-}$}     &  \multicolumn{1}{c|}{$\mathbf{3.4\times 10^{-4}}$} & \multicolumn{1}{c|}{$\mathbf{2.6 \times 10^{-4}}$} & \multicolumn{1}{c|}{$\mathbf{2.5\times 10^{-4}}$} \\
				& $[2.4\text{e-}04, 4.7\text{e-}04]$& &$[2.5\text{e-}04, 4.4\text{e-}04]$ &$[2.1\text{e-}04, 3.0\text{e-}04]$&$[2.0\text{e-}04, 3.1\text{e-}04]$\\  \hline
				\multirow{2}{*}{$\mathbf{-}$}    & \multicolumn{1}{c|}{$\mathbf{- \times 10^{-5}}$}& \multicolumn{1}{c|}{$\mathbf{- \times 10^{-5}}$}     &  \multicolumn{1}{c|}{$\mathbf{- \times 10^{-5}}$} & \multicolumn{1}{c|}{$\mathbf{- \times 10^{-5}}$} & \multicolumn{1}{c|}{$\mathbf{- \times 10^{-5}}$}\\
				& $[-\text{e-}05, -\text{e-}05]$& $[-\text{e-}05, -\text{e-}05]$&$[-\text{e-}05, -\text{e-}05]$ &$[-\text{e-}05, -\text{e-}05]$&$[-\text{e-}05, -\text{e-}05]$\\  \hline
			\end{tabular}
		\end{adjustbox}
		\newline
		\vspace*{0.2 cm}
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|ccccl|l|l|l|l|}
				\hline
				\multirow{2}{*}{$\cK$}     & \multicolumn{5}{c|}{\textbf{Relative statistical error}}   &\multicolumn{4}{c|}{\textbf{Variance reduction}}\\\cline{2-10}  
				& \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{MC}$} }& \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{CE,\tilde{\rho}_{0}}$}} & \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{PDE,\tilde{\rho}_{0}}$} }&\multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{PDE,W_t}$} }& \multicolumn{1}{c|}{\multirow{2}{*}{$\epsilon_{st}^{PDE,\text{both}}$} }& \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{CE,\tilde{\rho}_{0}}}$}& \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE,\tilde{\rho}_{0}}}$} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE,W_t}}$}&\multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \text{both}}}$}\\  [2ex]\hline
				$\mathbf{0.81}$          & \multicolumn{1}{c|}{$\mathbf{3.5\%}$}     &  \multicolumn{1}{c|}{$\mathbf{-\%}$} & \multicolumn{1}{c|}{$\mathbf{3.3\%}$} & \multicolumn{1}{c|}{$\mathbf{5.7\%}$}&\multicolumn{1}{c|}{$\mathbf{2.5\%}$} &\multicolumn{1}{c|}{$\mathbf{-} $} & \multicolumn{1}{c|}{$\mathbf{1.3} $} &\multicolumn{1}{c|}{$\mathbf{0.01} $} &\multicolumn{1}{c|}{$\mathbf{2.7} $} \\ \hline
				$\mathbf{0.82}$          & \multicolumn{1}{c|}{$\mathbf{10.3\%}$}     &  \multicolumn{1}{c|}{$\mathbf{-\%}$} & \multicolumn{1}{c|}{$\mathbf{8.6\%}$} & \multicolumn{1}{c|}{$\mathbf{78.9\%}$}&\multicolumn{1}{c|}{$\mathbf{7.1\%}$} &\multicolumn{1}{c|}{$\mathbf{-} $} & \multicolumn{1}{c|}{$\mathbf{1.5} $} &\multicolumn{1}{c|}{$\mathbf{0.01} $} &\multicolumn{1}{c|}{$\mathbf{2.7} $} \\ \hline
				$\mathbf{0.83}$          & \multicolumn{1}{c|}{$\mathbf{33\%}$}     &  \multicolumn{1}{c|}{$\mathbf{-\%}$} & \multicolumn{1}{c|}{$\mathbf{27.6\%}$} & \multicolumn{1}{c|}{$\mathbf{18.6\%}$}&\multicolumn{1}{c|}{$\mathbf{21.6\%}$} &\multicolumn{1}{c|}{$\mathbf{-} $} & \multicolumn{1}{c|}{$\mathbf{1.5} $} &\multicolumn{1}{c|}{$\mathbf{5.9} $} &\multicolumn{1}{c|}{$\mathbf{4.5} $} \\ \hline
				$\mathbf{-}$          & \multicolumn{1}{c|}{$\mathbf{-\%}$}     &  \multicolumn{1}{c|}{$\mathbf{-\%}$} & \multicolumn{1}{c|}{$\mathbf{-\%}$} & \multicolumn{1}{c|}{$\mathbf{-\%}$}&\multicolumn{1}{c|}{$\mathbf{-\%}$} &\multicolumn{1}{c|}{$\mathbf{-} $} & \multicolumn{1}{c|}{$\mathbf{-} $} &\multicolumn{1}{c|}{$\mathbf{-} $} &\multicolumn{1}{c|}{$\mathbf{-} $}  \\ \hline
			\end{tabular}
		\end{adjustbox}
		\caption{Model parameter: $b=0.0141$. Simulation parameters: $T=1$, $\Delta t= 0.01$,  $J = 10^5$, $\boldsymbol{\sigma_0^2=0.000025}$.
		}
		\label{table:DWlargesigma}
	\end{table}
\end{frame}

\begin{frame}{Charney-deVore model }
	\begin{figure}
		\vspace*{-0.2cm}
		\centering
		\includegraphics[height=5cm, width=5cm]{eps001u1traj.png}
		\includegraphics[height=5cm, width=5cm]{eps00001u1traj.png}
	\end{figure}
\end{frame}
\end{document}