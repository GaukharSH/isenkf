% Modelo de slides para projetos de disciplinas do Abel
\documentclass[10pt]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}
\usepackage[numbers,sort&compress]{natbib}
\bibliographystyle{plainnat}
\usepackage{adjustbox}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{multirow}
\usepackage{xspace}
\usepackage{mathtools}

\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\green}[1]{{\color{green} #1}}
\newcommand{\magenta}[1]{{\color{magenta} #1}}
\title{Rare events and filtering}
 \subtitle{Large Deviation Theory}
 \date{\today}
\date{July, 2023}
\author{Gaukhar Shaimerdenova}
\institute{AMCS, KAUST}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\V}{\mathbb{V}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\bu}{\boldsymbol{u}}
\newcommand{\bv}{\boldsymbol{v}}
\newcommand{\bx}{\boldsymbol{x}}
\newcommand{\ba}{\boldsymbol{a}}
\newcommand{\bb}{\boldsymbol{b}}
\newcommand{\bX}{\boldsymbol{X}}
\newcommand{\bK}{\boldsymbol{K}}
\newcommand{\brho}{\boldsymbol{\rho}}
\newcommand{\bmu}{\boldsymbol{\mu}}
\newcommand{\bSigma}{\boldsymbol{\Sigma}}
\newcommand{\bW}{\boldsymbol{W}}

%%%%%%%%   Macros we will need for this paper %%%%%%%%%%%%%%%

\newcommand{\norm}[1]{\left\| #1 \right\|}
\newcommand{\abs}[1]{\left| #1 \right|}
\newcommand{\ceil}[1]{\left \lceil #1 \right \rceil}
\newcommand{\prt}[1]{\left( #1 \right)}
\newcommand{\cost}[1]{\mathrm{Cost}}
\newcommand{\Tol}{\mathrm{TOL}}

% mathbb font letters 
\newcommand{\bF}{\mathbb{F}}
\newcommand{\bN}{\mathbb{N}}
\newcommand{\bP}{\mathbb{P}}
\newcommand{\bR}{\mathbb{R}}
\newcommand{\bZ}{\mathbb{Z}}
\newcommand{\bT}{\mathbf{T}}

% mathcal capital letters
\newcommand{\cB}{\mathcal{B}}
\newcommand{\cF}{\mathcal{F}}
\newcommand{\cO}{\mathcal{O}}
\newcommand{\cI}{\mathcal{I}}
\newcommand{\cJ}{\mathcal{J}}
\newcommand{\cK}{\mathcal{K}}
\newcommand{\cL}{\mathcal{L}}
\newcommand{\cN}{\mathcal{N}}
\newcommand{\cT}{\mathcal{T}}

%bold letters
\newcommand{\bw}{\boldsymbol{w}}

% ensemble notation 
\newcommand{\vBar}{\bar v}
\newcommand{\vHat}{\hat{v}}
\newcommand{\vBarHat}{\hat{\bar{v}}}
\newcommand{\meanHatMC}[1]{\hat{m}^{\rm{MC}}_{#1}}
\newcommand{\covHatMC}[1]{\widehat{C}^{\rm{MC}}_{#1}}
\newcommand{\yTilde}[1]{\tilde{y}_{#1}}
\newcommand{\meanHatML}[1]{\hat{m}^{\rm{ML}}_{#1}}
\newcommand{\covHatML}[1]{\widehat{C}^{\rm{ML}}_{#1}}
\newcommand{\kMC}[1]{K^{\rm{MC}}_{#1}}


% statistics notation
\newcommand{\Prob}[1]{\bP_{#1}}
\newcommand{\Ex}[1]{\E \left[ #1 \right]}
\newcommand{\Var}[1]{\V \left[ #1 \right]}
\newcommand{\Cov}{\overline{\mathrm{Cov}}}
\newcommand{\D}{\mathrm{D}}
\newcommand{\Ind}[2]{\textbf{1}_{\{#2\}}}

% colors
\newcommand{\blue}[1]{{\color{blue} #1}}


\begin{document}

\maketitle

\begin{frame}{Double Well example: value function comparison with $s_f=60$}
	{\footnotesize \textbf{Deterministic} means a solution of HJB based on the corresponding ODE;\\
		\textbf{Stochastic} means a solution of HJB based on the corresponding SDE;}
	\begin{figure}
		\centering
		\includegraphics[height=4cm, width=4cm]{figures/valfunsig005_smf60.png}
		\includegraphics[height=4cm, width=4cm]{figures/valfunsig05_smf60.png}
		\includegraphics[height=4cm, width=4cm]{figures/valfunsig1_smf60.png}
	\end{figure}
\end{frame}

\begin{frame}{Double Well example: value function comparison with $s_f=1$}
	{\footnotesize \textbf{Deterministic} means a solution of HJB based on the corresponding ODE;\\
		\textbf{Stochastic} means a solution of HJB based on the corresponding SDE;}
	\begin{figure}
		\centering
		\includegraphics[height=4cm, width=4cm]{figures/valfun_sig005_smf1.png}
		\includegraphics[height=4cm, width=4cm]{figures/valfun_sig05_smf1.png}
		\includegraphics[height=4cm, width=4cm]{figures/valfun_sig1_smf1.png}
	\end{figure}
\end{frame}

\begin{frame}{Double Well example: optimal control comparison in $\sigma=0.05$}
	{\footnotesize \textbf{Deterministic} means a solution of HJB based on the corresponding ODE;\\
		\textbf{Stochastic} means a solution of HJB based on the corresponding SDE;}
	\begin{figure}
		\centering
		\includegraphics[height=4cm, width=4cm]{figures/Controlatmin1_DW_sig005.png}
		\includegraphics[height=4cm, width=4cm]{figures/Controlat0_DW_sig005.png}
		\includegraphics[height=4cm, width=4cm]{figures/Controlat1_DW_sig005.png}
	\end{figure}
\end{frame}

\begin{frame}{Double Well example: optimal control comparison in $\sigma=0.5$}
	{\footnotesize \textbf{Deterministic} means a solution of HJB based on the corresponding ODE;\\
		\textbf{Stochastic} means a solution of HJB based on the corresponding SDE;}
	\begin{figure}
		\centering
		\includegraphics[height=4cm, width=4cm]{figures/Controlatmin1_DW_sig05.png}
		\includegraphics[height=4cm, width=4cm]{figures/Controlat0_DW_sig05.png}
		\includegraphics[height=4cm, width=4cm]{figures/Controlat1_DW_sig05.png}
	\end{figure}
\end{frame}

\begin{frame}{Double Well example: optimal control comparison in $\sigma=1$}
	{\footnotesize \textbf{Deterministic} means a solution of HJB based on the corresponding ODE;\\
		\textbf{Stochastic} means a solution of HJB based on the corresponding SDE;}
	\begin{figure}
		\centering
		\includegraphics[height=4cm, width=4cm]{figures/Controlatmin1_DW_sig1.png}
		\includegraphics[height=4cm, width=4cm]{figures/Controlat0_DW_sig1.png}
		\includegraphics[height=4cm, width=4cm]{figures/Controlat1_DW_sig1.png}
	\end{figure}
\end{frame}

\begin{frame}{DW: comparison of trajectories in diff. $\sigma$ (fixed $K=2.5$)}
	\begin{figure}
		\centering
		\includegraphics[height=4cm, width=4cm]{figures/Trajectory_sig005_DW.png}
		\includegraphics[height=4cm, width=4cm]{figures/Trajectory_sig05_DW.png}
		\includegraphics[height=4cm, width=4cm]{figures/Trajectory_sig1_DW.png}
	\end{figure}
	
\end{frame}

\begin{frame}{Noisy Lorenz 63}
	A  noisy Lorenz '63  model 
	\begin{equation}\label{Langevin}
		\begin{cases} 
			dx=r(y-x)dt+\sigma dW_1, \\
			dy=(sx-y-xz)dt+\sigma dW_2,\\
			dz=(xy-qz)dt+\sigma dW_3,\;
		\end{cases}\,
	\end{equation}
	where $W_j, j=1,2,3$ is assumed to be independent Brownian motions. \\
	Chaotic classic setting: $r=10, s=28, q=8/3.$\\
	Initial condition: $(x, y, z)=(5,5,25)+N(\mathbf{0}, 0.0025\mathbf{I});$\\
	We aim to track the first component, so $P_1=[1 \; 0 \; 0]$ over time $[0,1]$
\end{frame}

\begin{frame}{Lorenz 63: at the final time based on $M=10^4$ samples, $\sigma=0.05$}
	$x, y, z$ are the 1st, 2nd, 3rd components, respectively.
	\begin{figure}
		\vspace*{-0.2cm}
		\centering
		\includegraphics[height=4cm, width=4cm]{figures/L63margdens_xyT1sig005.png}
		\includegraphics[height=4cm, width=4cm]{figures/L63margdens_xzT1sig005.png}
		\includegraphics[height=4cm, width=4cm]{figures/L63margdens_yzT1sig005.png}
	\end{figure}
\end{frame}

\begin{frame}{L0renz 63: value function with fixed $K=11.95$, $s_f=60$}
	$\small (x, y ,z)=[5\,5\, 25; 6\,5\, 25; 7\, 5\,25; 8\,5\,25; 9\,5\,25; 10\,5\,25; 11\,5\,25]'$ 
	\begin{figure}
		\vspace*{-0.2cm}
		\centering
		\includegraphics[height=4cm, width=4cm]{figures/L63_valfun_sig0005.png}
		\includegraphics[height=4cm, width=4cm]{figures/L63_valfun_sig005.png}
		\includegraphics[height=4cm, width=4cm]{figures/L63_valfun_sig05.png}
		\includegraphics[height=4cm, width=4cm]{figures/L63_valfun_sig1.png}
	\end{figure}
\end{frame}

\begin{frame}{L0renz 63: value function with fixed $K=11.95$, $s_f=60$}
	$\small (x, y ,z)=[11.65\;12.8\; 30.2; 11.7\;12.8\; 30.2;  ... 11.95\;12.8\;30.2]'$ 
	\begin{figure}
		\vspace*{-0.2cm}
		\centering
		\includegraphics[height=4cm, width=4cm]{figures/L63_valfun_sig0005_ic116.png}
		\includegraphics[height=4cm, width=4cm]{figures/L63_valfun_sig005_ic116.png}
		\includegraphics[height=4cm, width=4cm]{figures/L63_valfun_sig05_ic116.png}
		\includegraphics[height=4cm, width=4cm]{figures/L63_valfun_sig1_ic116.png}
	\end{figure}
\end{frame}

\begin{frame}{L63: comparison of trajectories in diff. $\sigma$, $s_f=50$}
	$K$ is chosen such that the QoI is around $10^{-4}$ probability in these plots.
	\begin{figure}
		\vspace*{-0.2cm}
		\centering
		\includegraphics[height=4cm, width=4cm]{figures/Trajectory_sig005_L63.png}
		\includegraphics[height=4cm, width=4cm]{figures/Trajectory_sig05_L63.png}
		\includegraphics[height=4cm, width=4cm]{figures/Trajectory_sig1_L63.png}
		\includegraphics[height=4cm, width=4cm]{figures/Trajectory_sig10_L63.png}
	\end{figure}
	
\end{frame}



\begin{frame}{L63: corresponding optimal controls in diff. $\sigma$, $s_f=50$}
	{\footnotesize \textbf{Deterministic} means a solution of HJB based on the corresponding ODE;\\
		\textbf{Stochastic} means a solution of HJB based on the corresponding SDE;}
	\begin{figure}
		\centering
		\includegraphics[height=4cm, width=4cm]{figures/Controlat5_L63_sig005.png}
		\includegraphics[height=4cm, width=4cm]{figures/Controlat5_L63_sig05.png}
		\includegraphics[height=4cm, width=4cm]{figures/Controlat5_L63_sig1.png}
		\includegraphics[height=4cm, width=4cm]{figures/Controlat5_L63_sig10.png}
	\end{figure}
\end{frame}

\begin{frame}{L63: KBE solution at different time with $s_f=50$}
	\begin{figure}
		\centering
		\includegraphics[height=6cm, width=6cm]{figures/KBEsolatdifftime_smf50_sig005L63.png}
	\end{figure}
\end{frame}

\begin{frame}{L63: KBE solution at different time with $s_f=5$}
	\begin{figure}
		\centering
		\includegraphics[height=4cm, width=4cm]{figures/KBEsolatdifftime_smf5_sig005L63.png}
		\includegraphics[height=4cm, width=4cm]{figures/KBEsolatdifftime_smf5_sig05L63.png}
		\includegraphics[height=4cm, width=4cm]{figures/KBEsolatdifftime_smf5_sig1L63.png}
		\includegraphics[height=4cm, width=4cm]{figures/KBEsolatdifftime_smf5_sig10L63.png}
	\end{figure}
\end{frame}

\begin{frame}{L63: corresponding optimal controls in diff. $\sigma$, $s_f=5$}
	\begin{figure}
		\centering
		\includegraphics[height=4cm, width=4cm]{figures/OptControlCompPontKBE_sig005_L63.png}
		\includegraphics[height=4cm, width=4cm]{figures/OptControlCompPontKBE_sig05_L63smf5.png}
		\includegraphics[height=4cm, width=4cm]{figures/OptControlCompPontKBE_sig1_L63smf5.png}
		\includegraphics[height=4cm, width=4cm]{figures/Controlat5_L63_sig10_2version.png}
	\end{figure}
\end{frame}

\begin{frame}{L63: Optimal control with $\sigma=10$, $s_f=5$}
	\begin{figure}
		\centering
		\includegraphics[height=6cm, width=6cm]{figures/OptControlCompPontKBE_sig10_L63smf5_diffx.png}
	\end{figure}
	
	In this case, for a rare event around $10^{-4}$, we have 4.4 times variance reduction via KBE, 2.1 times via Pontryagin.
\end{frame}

\begin{frame}{Conclusion on Lorenz 63 example}
	$\bullet$ A solution for optimal control obtained by Pontryagin's Principle is independent of states, it depends only on time.\\
	$\bullet$ The optimal control obtained by Pontryagin (interpolated in time in small $\sigma$ regime) is used for IS simulation and gives around $5\%$ decrease in relative error and around $1.5-2$ times variance reduction for about $10^{-4}$ probaility degree.\\
	$\bullet$ In relatively small $\sigma$, there is a numerical issue in pde solver, even we solve the issue and verify with Pontryagin solution, it gives anyway a relatively little variance reduction.\\
	$\bullet$ In relatively large $\sigma$, there is no numerical issue, both KBE and Pontryagin gives relatively smaller variance reduction (maybe it should be as it is for chaotic dynamics).\\
	%$\bullet$ Value function for Lorenz I didn't compare with KBE/HJB, since we have to generate for each initial pont x a new Markovian projection samples \\
	$\bullet$  A smoothing factor matters!\\
	$\bullet$ I suggest to include Lorenz 63 example with the case $\sigma=10$ to the manuscript.
\end{frame}

\begin{frame}{Appendix I: $\sigma=0.05$, $\sigma=0.5$, $\sigma=1$, $\sigma=10$}
	\begin{figure}
		\centering
		\includegraphics[height=4cm, width=4cm]{figures/abarMP_sig005.png}
		\includegraphics[height=4cm, width=4cm]{figures/abarMP_sig05.png}
		\includegraphics[height=4cm, width=4cm]{figures/abarMP_sig1.png}
		\includegraphics[height=4cm, width=4cm]{figures/abarMP_sig10.png}
	\end{figure}
\end{frame}

\begin{frame}{Appendix I: $\sigma=0.05$}
	\begin{figure}
		\centering
		\includegraphics[height=4cm, width=4cm]{figures/abarMP_sig005.png}
		\includegraphics[height=4cm, width=4cm]{figures/abarMP_sig005_uniform.png}

	\end{figure}
\end{frame}
%\begin{frame}{Conclusion on the project}
%    \red{\textbf{Numerical studies done so far:}}\\
%	$\bullet$ Tested a change of measure with respect to \textbf{only initial condition}: good robust results in variance reduction for 1D - OU, DW, 2D - Langevin, and in weakly chaotic regime for 3D - noisy Lorenz' 63 and multiD -noisy Lorenz' 96. \\
%	$\bullet$ Studied the time distribution when running maximum achieves the threshold. In the case of Lorenz examples, we observe that in the settings when dynamics are steady or periodic, the maximum is achieved in the beginning of the considered time interval; when dynamics are more chaotic, the maximum is achieved mostly in the middle/end of time. \\
%	$\bullet$ Therefore, we decided to perform a change of measure with respect to $W_t$ too. Tested on 1D - DW problem, 2D - Langevin dynamics, 3D - Lorenz' 63, multiD - nosiy Lorenz' 96. \\
%	$\bullet$ We faced several numerical issues in computing optimal control and used some ideas/techniques to solve these issues. \\
%	$\bullet$  Optimal control is computed via solving KBE and verified with the one obtained via solving HJB equation. Tested different boundary conditions. Tested different smoothening of the final condition.\\
%	$\bullet$ Tested initial conditions with different variances. Since the results depend on this parameter a lot. \\
%	$\bullet$ Tested the combination of change of measures with respect to both $\rho_0$ and $W_t$. \\
%	$\bullet$ For Lorenz examples, 
%\end{frame}

%\begin{frame}{Double Well example: optimal control comparison}
%	\begin{figure}
%		\centering
%		\includegraphics[height=4cm, width=4cm]{Control_t0_sigma01.png}
%		\includegraphics[height=4cm, width=4cm]{Control_t0_sigma05.png}
%		\includegraphics[height=4cm, width=4cm]{Control_t0_sigma1.png}
%	\end{figure}
%\end{frame}
%\begin{frame}{Conclusion on the project}

%\end{frame}

%\begin{frame}{Double Well example: optimal control comparison}
%	\begin{figure}
%		\centering
%		\includegraphics[height=4cm, width=4cm]{Control_t0_sigma01.png}
%		\includegraphics[height=4cm, width=4cm]{Control_t0_sigma05.png}
%		\includegraphics[height=4cm, width=4cm]{Control_t0_sigma1.png}
%	\end{figure}
%\end{frame}

\end{document}