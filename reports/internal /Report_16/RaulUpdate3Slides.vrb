\frametitle{Change of Measure with respect to $W_t$}
$\bullet$ The optimal control is defined by
\[
\xi^*(t,x) = b(t,x) \frac{\partial \log v_\tau(t,x)}{\partial x}.
\]
where $v_\tau(t,x)$ satisfies the KBE:
\[
\left\{
\begin{array}{ll}
	\partial_t v_\tau (t,x) = -a(t,x) \partial_x v_\tau (t,x) -\frac{1}{2} b^2 (t,x) \partial_{xx} v_{\tau} (t,x),\\
	v_\tau (T,\cdot) =0, \quad x<K,\\
	v_\tau (t,K) =1.
\end{array}
\right.
\]

$\red{\textbf{Note:}}$ The issue with numerical differentiation.  \[
f'(x) \approx \frac{f(x+h)-f(x)}{h}
\]
- Note that this approximation has two sources of errors: truncation error and roundoff error.\\
- Truncation error can be reduced by decreasing the step size $h$, however, if \textbf{$h$ becomes too small, loss of significance can become a factor}.\\
- The usual approach to get an adequate result is to try a step size $h$ large enough that the loss of significance in the differences begins dominate.
