% Modelo de slides para projetos de disciplinas do Abel
\documentclass[10pt]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}
\usepackage[numbers,sort&compress]{natbib}
\bibliographystyle{plainnat}
\usepackage{adjustbox}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{multirow}
\usepackage{xspace}
\usepackage{mathtools}

\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\green}[1]{{\color{green} #1}}
\newcommand{\magenta}[1]{{\color{magenta} #1}}
\title{Rare events and filtering}
 \subtitle{Chronology of numerical tests}
 \date{\today}
\date{July, 2023}
\author{Gaukhar Shaimerdenova}
\institute{AMCS, KAUST}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\V}{\mathbb{V}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\bu}{\boldsymbol{u}}
\newcommand{\bv}{\boldsymbol{v}}
\newcommand{\bx}{\boldsymbol{x}}
\newcommand{\ba}{\boldsymbol{a}}
\newcommand{\bb}{\boldsymbol{b}}
\newcommand{\bX}{\boldsymbol{X}}
\newcommand{\bK}{\boldsymbol{K}}
\newcommand{\brho}{\boldsymbol{\rho}}
\newcommand{\bmu}{\boldsymbol{\mu}}
\newcommand{\bSigma}{\boldsymbol{\Sigma}}
\newcommand{\bW}{\boldsymbol{W}}

%%%%%%%%   Macros we will need for this paper %%%%%%%%%%%%%%%

\newcommand{\norm}[1]{\left\| #1 \right\|}
\newcommand{\abs}[1]{\left| #1 \right|}
\newcommand{\ceil}[1]{\left \lceil #1 \right \rceil}
\newcommand{\prt}[1]{\left( #1 \right)}
\newcommand{\cost}[1]{\mathrm{Cost}}
\newcommand{\Tol}{\mathrm{TOL}}

% mathbb font letters 
\newcommand{\bF}{\mathbb{F}}
\newcommand{\bN}{\mathbb{N}}
\newcommand{\bP}{\mathbb{P}}
\newcommand{\bR}{\mathbb{R}}
\newcommand{\bZ}{\mathbb{Z}}
\newcommand{\bT}{\mathbf{T}}

% mathcal capital letters
\newcommand{\cB}{\mathcal{B}}
\newcommand{\cF}{\mathcal{F}}
\newcommand{\cO}{\mathcal{O}}
\newcommand{\cI}{\mathcal{I}}
\newcommand{\cJ}{\mathcal{J}}
\newcommand{\cK}{\mathcal{K}}
\newcommand{\cL}{\mathcal{L}}
\newcommand{\cN}{\mathcal{N}}
\newcommand{\cT}{\mathcal{T}}

%bold letters
\newcommand{\bw}{\boldsymbol{w}}

% ensemble notation 
\newcommand{\vBar}{\bar v}
\newcommand{\vHat}{\hat{v}}
\newcommand{\vBarHat}{\hat{\bar{v}}}
\newcommand{\meanHatMC}[1]{\hat{m}^{\rm{MC}}_{#1}}
\newcommand{\covHatMC}[1]{\widehat{C}^{\rm{MC}}_{#1}}
\newcommand{\yTilde}[1]{\tilde{y}_{#1}}
\newcommand{\meanHatML}[1]{\hat{m}^{\rm{ML}}_{#1}}
\newcommand{\covHatML}[1]{\widehat{C}^{\rm{ML}}_{#1}}
\newcommand{\kMC}[1]{K^{\rm{MC}}_{#1}}


% statistics notation
\newcommand{\Prob}[1]{\bP_{#1}}
\newcommand{\Ex}[1]{\E \left[ #1 \right]}
\newcommand{\Var}[1]{\V \left[ #1 \right]}
\newcommand{\Cov}{\overline{\mathrm{Cov}}}
\newcommand{\D}{\mathrm{D}}
\newcommand{\Ind}[2]{\textbf{1}_{\{#2\}}}

% colors
\newcommand{\blue}[1]{{\color{blue} #1}}


\begin{document}

\maketitle

\begin{frame}{}
	\red{\textbf{Numerical studies done in this project:}}\\
	$\bullet$ Tested \textbf{a change of measure with respect to only initial condition}: good robust results in variance reduction for \\
	1D - Ornstein-Uhlenbeck, Double Well problems, \\
	2D - Langevin dynamics,\\
	3D - noisy Lorenz 63 in steady/periodic/weakly chaotic regime,\\
	multiD - noisy Lorenz 96 in steady/periodic/weakly chaotic regime.\\
	
	
	$\bullet$ Studied the time distribution when running maximum achieves the threshold. In the case of Lorenz examples, we observe that in the settings when dynamics are steady or periodic, the maximum is achieved in the beginning of the considered time interval; when dynamics are more chaotic, the maximum is achieved mostly in the middle/end of time. \\
	$\bullet$ Therefore, we decided to perform \textbf{a change of measure with respect to Wiener processes} $W_t$ too. Tested on 1D - DW problem, 2D - Langevin dynamics, 3D - Lorenz' 63, multiD -  Lorenz' 96. \\
	$\bullet$ We faced several numerical issues in stability of pde solver for KBE and in computing optimal control (concerning numerical differentiation of $\log$) \\
\end{frame}

\begin{frame}{}
	$\bullet$ Tested different boundary conditions for PDEs. Tested different smoothening of the indicator function in the final condition for PDEs. \\
	$\bullet$ Exponential smoothening of indicator function helped to solve the issue with instability of pde solver.\\
	$\bullet$  Due to num.diff. issue of $\log$, we decided also to try solving the HJB PDE and obtain optimal control which doesn't depend on $\log$ function. We verified that optimal controls produced by KBE match with HJB ones (See below an illustraton for DW example). \\
    	\begin{figure}[h!]
    		\hspace*{-1cm}
    		\includegraphics[height=4.5cm, width=5.7cm]{controlKBEc40dx0005.png}
    		\includegraphics[height=4.5cm, width=5.7cm]{controlHJBc40dx0005.png}
    	\end{figure}
\end{frame}

\begin{frame}{}
	$\bullet$ However, we observed that the smoothening factor matters, it affects on the level of optimal control. There was a question how to choose an appropriate smoothening factor. \\
	$\bullet$ Instead, Erik suggested to compute the optimal control for the last time step $T-dt_{EM}$ and, by numerical integration, compute the corresponding value of the KBE solution at time $T-dt_{EM}$. So we can use this solution as a final condition for KBE starting from time $T-dt_{EM}$. However, Erik's solution depends on Euler-Maruyama timestep $dt_{EM}$ and if it gets smaller, its solution gets down to zero very quickly.\\
	$\bullet$ Therefore, I decided to use exponential smoothening anyway but choose the smoothening factor such that it is close to Erik's solution on the right boundary, but in the left boundary it doesn't get to zero quickly as Erik's solution does (See below 3 slides as the illustarion for this in different $dt_{EM}$). Attached in the email a report presented 17 Nov, 22 if you are interested in more details on this.
\end{frame}
\begin{frame}{DW: $dx_{PDE}=0.005, dt_{PDE}=dx^2, \red{dt_{EM}=0.1}$}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolExpSm15ErikSoldt01.png}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolExpSm15ErikSoldt01semilog.png}
		\includegraphics[height=4cm, width=5.7cm]{ControlExpSm15dt01.png}
	\end{figure}
\end{frame}

\begin{frame}{DW: $dx_{PDE}=0.005, dt_{PDE}=dx^2, \red{dt_{EM}=0.01}$}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolExpSm40ErikSoldt001.png}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolExpSm40ErikSoldt001semilog.png}
		\includegraphics[height=4cm, width=5.7cm]{ControlExpSm40dt001.png}
	\end{figure}
\end{frame}

\begin{frame}{DW:$dx_{PDE}=0.005, dt_{PDE}=dx^2, \red{dt_{EM}=0.001}$}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolExpSm100ErikSoldt0001.png}
		\includegraphics[height=4.5cm, width=5.7cm]{KBEsolExpSm100ErikSoldt0001semilog.png}
		\includegraphics[height=4cm, width=5.7cm]{ControlExpSm100dt0001.png}
	\end{figure}
\end{frame}
\begin{frame}{}
	$\bullet$ Tested also initial conditions with different variances. Since the results depend on this parameter a lot. \\
	$\bullet$ Tested the combination of \textbf{change of measures with respect to both $\rho_0$ and $W_t$}. The combination gives superior results compared to its sole variants for examples Double Well and Langevin.\\
	$\bullet$ In chaotic dynamics, I was working mostly with multi-D Lorenz 96 example and observed if we add noise only in one compenent which we track and the noise level is similar to forcing term in dynamics (i.e. $\sigma$ is close to $F$) then we have some variance reduction. Raul advised to test more 3D Lorenz 63. In Lorenz 63, I faced numerical issue in pde solver since it was strongy advection dominated with $\sigma=0.5$. Erik looked at the code and observed the issue with $\bar{a}(t,x)$ on the corners produced by Markovian Projection samples (where a high degree polynomial was used). \\
	$\bullet$ Then, Raul suggested to try tempering, I implemeted it but we didn't get any benefit from that.
\end{frame}
\begin{frame}{}
	$\bullet$  Then, Raul suggested to see the Large Deviation Theory approach. I implemented it and verified the theory for Double Well in small noise regime. For Lorenz 63, Pontryagin's solution depends only on time and it gives a variance reduction but not so much. When $\sigma$ is large, there is no issue in pde solver, and solutions produced by Pontryagin and KBE approximately match.  \\
	$\bullet$ In Lorenz 63, Erik's solution works for large $\sigma$ and I can choose smoothing factor based on that. However, I observed that slightly smaller smoothening factor is better to avoid round-off error in computing likelihood so we can get unbiased estimator where the QoI value is close to the Monte Carlo counterpart value.
\end{frame}

\begin{frame}{}
	
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=7cm, width=7cm]{L63_expsm_Eriksol_sig10.png}
	\end{figure}
	
\end{frame}
\end{document}