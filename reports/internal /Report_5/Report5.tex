\documentclass[]{amsart}
%\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb,amsthm}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{relsize}
\usepackage{tikz}
\usetikzlibrary{scopes}
\usetikzlibrary{backgrounds}
\usepackage{mdframed}
\newtheorem{theorem}{Theorem}
\newtheorem{defintion}{Definition}
\newtheorem{assumption}{Assumption}
\newtheorem{remark}{Remark}
\newtheorem{notation}{Notation}
\newtheorem{lemma}{Lemma}
\newtheorem{corollary}{Corollary}
\usepackage{multirow}
\usepackage[width=1.0\textwidth, font=footnotesize]{caption}
%\usepackage{xcolor}
\usepackage{tcolorbox}
\usepackage{physics}
\usepackage{amsmath}
\usepackage{tikz}
\usepackage{mathdots}
\usepackage{yhmath}
\usepackage{cancel}
\usepackage{color}
\usepackage{siunitx}
\usepackage{array}
\usepackage{multirow}
\usepackage{amssymb}
\usepackage{gensymb}
\usepackage{tabularx}
\usepackage{booktabs}
\usetikzlibrary{fadings}
\usetikzlibrary{patterns}
\usetikzlibrary{shadows.blur}
\usetikzlibrary{shapes}
\usepackage[linesnumbered,ruled,vlined]{algorithm2e}
\SetKwInput{KwInput}{Input}                % Set the Input
\SetKwInput{KwOutput}{Output}   
\newcommand{\red}[1]{\textcolor{red}{#1}}
\newcommand{\green}[1]{\textcolor{green}{#1}}
\newcommand{\T}{\mathrm{T}}
\DeclareMathOperator*{\argmax}{argmax} 
%%%%%%%%   Macros we will need for this paper %%%%%%%%%%%%%%%

% statistics notation
\newcommand{\Prob}[1]{\bP\left(#1\right)}
\newcommand{\Ex}[1]{\E \left[ #1 \right]}
\newcommand{\Cov}{\overline{\mathrm{Cov}}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\D}{\mathrm{D}}

\title{Report 5}
\author{Gaukhar Shaimerdenova}

\begin{document}
\maketitle
\begin{tcolorbox}
The report contains the idea of importance sampling for rare events. 
\end{tcolorbox}

We consider the general SDE of the form
\begin{equation}\label{sde}
	\begin{cases} 
		dX_t= a(X_t)dt+b(X_t)dW_t, \quad 0<t<T, \\
		X_{0}=x_0\sim \rho_0.
	\end{cases}\,
\end{equation}
\textbf{Objective:} We are interested in computing the quantity of interest (QoI)
\[
P(M^T>K):=P(\max_{0\leq t\leq T} X_t>K).
\]
for a given threshold $K>0$.

We denote a numerical approximation of $M^T$ by $\bar{M}_{\Delta t}^T:=\max_{0\leq t\leq T} \bar{X}_t$, where $\bar{X}_t$ is the numerical solution of SDE~\eqref{sde} with time step $\Delta t$. Then we can write
\[
P(M^T>K)=P(\bar{M}_{\Delta t}^T>K)+\mathcal{O}(\Delta t)=1-P(\bar{M}_{\Delta t}^T\leq K )+\mathcal{O}(\Delta t).
\]

Now we try to compute $P(\bar{M}_{\Delta t}^T\leq K )$.


Let $0=t_0<t_1<...<t_N=T$, then the Euler-Maruyama approximation of the SDE~\eqref{sde} is defined by 
\[
\bar{X}_{n+1}=\bar{X}_n+a(\bar{X}_n)\Delta t_n+b(\bar{X}_n)\Delta W_{n},
\]
with $\bar{X}_0=x_0$, $\Delta W_n=W_{t_{n+1}}-W_{t_n}\sim N(0, \Delta t_n)$ for $n=0,...,N-1.$

We can extend the solution $\bar{X}$ from $0=t_0<t_1<...<t_N=T$ to all $t\in [0,T]$ by a continuous version of Forward Euler:
\[
\bar{X}_t=\bar{X}_n+a(\bar{X}_n)(t-t_n)+b(\bar{X}_n)(W_t-W_{t_n}), \quad t_n<t<t_{n+1},
\]
that is 
\[
\bar{X}_t=\bar{X}_n+\int_{t_n}^{t} a(\bar{X}_n)ds +\int_{t_n}^{t} b(\bar{X}_n)dW_s,
\]
or we can rewrite it in the following form
\[
\bar{X}_t=\bar{X}_n+\int_{t_n}^{t} \bar{a}(\bar{X}_s)ds +\int_{t_n}^{t} \bar{b}(\bar{X}_s)dW_s
\]
where functions
\[
\bar{a}(\bar{X}_s)=a(\bar{X}_n), \quad \bar{b}(\bar{X}_s)=b(\bar{X}_n) \quad \mbox{for all   } t_n\leq s<t_{n+1}.
\]

Then $\bar{M}_{\Delta t}^T$ corresponds to the continuous Forward Euler approximation $\bar{X}_t$ on the whole interval $0<t<T$:
\[
\bar{X}_t=x_0+\int_{0}^{t} \bar{a}(\bar{X}_s)ds +\int_{0}^{t} \bar{b}(\bar{X}_s)dW_s,
\]
and similarly,  $\bar{M}_{n, \Delta t}^T$ relates to the continuous Forward Euler approximation $\bar{X}_t$ on the interval $t_n\leq t\leq t_{n+1}$.

Then, by the property of maximum and by the independency of solutions at each interval given the $\bar{X}_n$ for $n=0,1,..,N$, we can write

\[
P(\bar{M}_{\Delta t}^T\leq K|\bar{X}_n, n=0,1,..,N)=\prod_{n=0}^{N-1} P(\bar{M}_{n,\Delta t}^T\leq K|\bar{X}_{n}, W_{t_{n}}, W_{t_{n+1}}),
\]
where $\bar{X}_{n}$ is the Forward Euler approximation solution at time step $t_n$ with $\bar{X}_{0}=x_0$. 

For a fixed $n$, let us consider the probability 
\begin{equation*}
	\begin{split}
 P(\bar{M}_{n,\Delta t }^T&\leq K|\bar{X}_{n}, W_{t_{n}}, W_{t_{n+1}})=P(\max_{t_n\leq t\leq t_{n+1}} \bar{X}_t \leq K |\bar{X}_{n},W_{t_{n}}, W_{t_{n+1}})\\
 &=P(\max_{t_n\leq t\leq t_{n+1}} \big[a(\bar{X}_n)(t-t_n)+b(\bar{X}_n)(W_t-W_{t_n})\big]\leq K-\bar{X}_n|\bar{X}_{n},W_{t_{n}}, W_{t_{n+1}})\\
 &=P(\max_{0\leq t\leq t_{n+1}-t_n} \big[a(\bar{X}_n)t+b(\bar{X}_n)(W_{t+t_n}-W_{t_n})\big]\leq K-\bar{X}_n|\bar{X}_{n}, W_{t_{n}}, W_{t_{n+1}})\\
 &=P(\max_{0\leq t\leq b^2(\bar{X}_n)(t_{n+1}-t_n)} \big[\frac{a(\bar{X}_n)}{b^2(\bar{X}_n)}t+b(\bar{X}_n)(W_{t/b^2(\bar{X}_n)+t_n}-W_{t_n})\big]\leq K-\bar{X}_n|\bar{X}_{n}, W_{t_{n}}, W_{t_{n+1}}).
	\end{split}
\end{equation*}
The latter probabilty can be considered as a drifted Brownian bridge and we have a closed form [see Lemma 1 in~\cite{pedersentowards} and (3.3) in~\cite{beghin1999maximum}]

\begin{equation*}
	\begin{split}
P&(\max_{0\leq t\leq b^2(\bar{X}_n)(t_{n+1}-t_n)} \big[\frac{a(\bar{X}_n)}{b^2(\bar{X}_n)}t+b(\bar{X}_n)(W_{t/b^2(\bar{X}_n)+t_n}-W_{t_n})\big]\leq K-\bar{X}_n|\bar{X}_{n}, W_{t_{n}}, W_{t_{n+1}})\\
&=(1-e^{-2(K-\bar{X}_{n})^2/t^*+2(K-\bar{X}_{n})(b(\bar{X}_{n})\Delta W_n+a(\bar{X}_{n})\Delta t_n)/t^*})\mathbf{1}_{\{\max (\bar{X}_n, \bar{X}_{n+1})<K\}},
	\end{split}
\end{equation*}
where $t^*=b^2(\bar{X}_n)(t_{n+1}-t_n)$.

Finally, we get
\begin{equation*}
	\begin{split}
	P(\bar{M}_{\Delta t}^T>K) = \E[ 1-\prod_{n=0}^{N-1} \underbrace{(1-e^{-2(K-\bar{X}_{n})^2/t^*+2(K-\bar{X}_{n})(b(\bar{X}_{n})\Delta W_n+a(\bar{X}_{n})\Delta t_n)/t^*})}_{I_K}\mathbf{1}_{\{\max (\bar{X}_n, \bar{X}_{n+1})<K\}}].
	\end{split}
\end{equation*}

\red{\textbf{!Notice.}} In the regime $K>>1$, we have $I_K\approx 1$ and the problem behaves like a rare event.  
\begin{equation}
	\begin{split}
\alpha:=P(\bar{M}_{\Delta t}^T>K)  &= \E[h(\bar{X}_0, \bar{X}_1, ..., \bar{X}_N)]\\
		&\approx \E[ 1-\prod_{n=0}^{N-1} \mathbf{1}_{\{\max (\bar{X}_n, \bar{X}_{n+1})<K\}}]=\E[1- \mathbf{1}_{\{\max_{0\leq n \leq N} \bar{X}_n<K\}}]\\
		&=\E[\mathbf{1}_{\{\max_{0\leq n \leq N} \bar{X}_n\geq K\}}]=P(\max_{0\leq n \leq N} \bar{X}_n\geq K).
	\end{split}
\end{equation}
Notice that the probability of maximum of continuous forward Euler solution between $0$ and $T$ in the rare event regime can be approximated by the probability of maximum of forward Euler solutions at time $t_n$ for $n=1,...,N$. 

\red{\textbf{Idea 1.}} Apply an importance sampling only with respect to the initial condition $x_0$ originally sampled from $\rho_0$. 

For ease of notation, we denote by $\bar{X}_{0:N}:=\{\bar{X}_i\}_{i=0}^N$ and consider the joint density $ \rho_{\bar{X}_{0:N}} (\bar{x}_{0:N})$ of random vector $(x_0,\bar{X}_{1:N})$ defined in a sample space $\Omega$. Note that we assume $\bar{X}_0=X_0$. Then, we introduce an alternate density function $\tilde{\rho}_{x_0}(x)$ and 

\begin{equation}
	\begin{split}
		 \E[h(X_0, \bar{X}_{1:N})]&=\int_{\Omega} h(x, \bar{x}_{1:N} ) \rho_{X_0, \bar{X}_{1:N}} (x, \bar{x}_{1:N})dx d\bar{x}_{1:N}\\
		 & =\int_{\Omega} h(x, \bar{x}_{1:N} ) \rho_{X_0} (x) \rho_{ \bar{X}_{1:N}|X_0} ( \bar{x}_{1:N}|x)dx d\bar{x}_{1:N}\\
		 &= \int_{\Omega} h(x, \bar{x}_{1:N} ) \frac{\rho_{X_0} (x)}{\tilde{\rho}_{X_0}(x)} \rho_{ \bar{X}_{1:N}|X_0} ( \bar{x}_{1:N}|x)\tilde{\rho}_{X_0}(x)dx d\bar{x}_{1:N}\\
		 &=\E \Big[ h(x, \bar{x}_{1:N} ) \frac{\rho_{X_0} (x)}{\tilde{\rho}_{X_0}(x)} \Big]:=\E \Big[ h(x, \bar{x}_{1:N} ) L(x)\Big]
	\end{split}
\end{equation}

In order to select a proper density that results to smaller variance, let us consider the second moment of the estimator:
\begin{equation}
	\begin{split}
		\E[ h^2(x, \bar{x}_{1:N} ) L^2(x)]&=\int_\Omega h^2(x, \bar{x}_{1:N} ) \frac{\rho^2_{X_0} (x)}{\tilde{\rho}^2_{X_0}(x)} \rho_{ \bar{X}_{1:N}|X_0} ( \bar{x}_{1:N}|x)\tilde{\rho}_{x_0}(x)dx d\bar{x}_{1:N}\\
		&=\int_\Omega h^2(x, \bar{x}_{1:N}) \frac{\rho^2_{X_0} (x)}{\tilde{\rho}_{X_0}(x)} \rho_{ \bar{X}_{1:N}|X_0} ( \bar{x}_{1:N}|x)  dx d\bar{x}_{1:N}\\
		&=\int_\Omega  \E[h^2(x, \bar{x}_{1:N})| X_0=x] \frac{\rho^2_{X_0} (x)}{\tilde{\rho}_{X_0}(x)}  dx
	\end{split}
\end{equation}
where we use the tower property of the conditional expectation ($\E[X]=\E[\E[X|Y]]$). 

By the method of Lagrangian multipliers, we can deduce that the optimal importance density which can reduce the variance of the estimator is
\begin{equation}\label{optimdensity}
\tilde{\rho}_{X_0}(x) \propto \rho_{X_0}(x) \sqrt{ \E[h^2(x, \bar{x}_{1:N})| X_0=x]}.
\end{equation}
As we have seen before, $h$ is approximately an indicator function when $K$ is large. 
Hence, the optimal importance density approximately satisfies 
\begin{equation}\label{optimdensity1}
	\tilde{\rho}_{X_0}(x) \propto \rho_{X_0}(x) \sqrt{P(\max_{0\leq n \leq N} \bar{X}_n\geq K|X_0=x)}.
\end{equation}
\red{\textbf{Idea 2.}} Approximate $P(\max_{0\leq n \leq N} \bar{X}_n\geq K |X_0=x)$ by $P(\max_{0\leq n \leq N} \bar{\tilde{X}}_n\geq K|X_0=x)$  where $\tilde{X}_t$ is a Gaussian process with mean $\mu_t$ and covariance $\sigma_{t,s}$. 

If we assume that $P(\max_{0\leq n \leq N} \bar{\tilde{X}}_n\geq K|X_0=x )$ is known, then we can approximate the optimal density in \eqref{optimdensity} as follows
\begin{equation}\label{ApproxOptimdensity}
	\tilde{\rho}_{X_0} (x)\propto \rho_{X_0} (x)\sqrt{P(\max_{0\leq n \leq N} \bar{\tilde{X}}_n\geq K |X_0=x)}.
\end{equation}
\red{\textbf{Remark.}} If $\tilde{X}_t$ is a centered continuous Gaussian process, a.s. bounded on $[0,T]$, then we have an universal bound referred as the Borell-TIS inequality for the excursion probability~[see (2.1.4) in \cite{adler2007random}, pp.50.]
\begin{equation}\label{B-TISineq}
	P(\sup_{t\in[0,T]} \tilde{X}_t > K ) \lesssim e^{-K^2/2\sigma^2_T},
\end{equation}
where $\sigma^2_T:=\sup_{t\in [0,T]} \E[\tilde{X}_t^2 ]$. 

\red{\textbf{Example.}} Let us consider the Ornstein-Uhlenbeck process in the general form
\begin{equation}\label{OU}
	\begin{cases} 
		dX_t= \theta(a-X_t)dt+bdW_t, \quad 0<t<T, \\
		X_{0}=x_0\sim \rho_0.
	\end{cases}\,
\end{equation}
with mean
\[
\mu_t:=\E[X_t]=x_0e^{-\theta t}+a(1-e^{-\theta t})
\]
and variance
\[
\sigma_t^2:=\mathbb{V} [X_t] = \frac{b^2}{2\theta} (1-e^{-2\theta t})
\]
where $\theta, a$ and $b$ are constants. 

Note that in order to apply the Borell-TIS inequality, we need a centered continuous Gaussian process. We introduce a Gaussian process $\tilde{X}_t$ with the mean $\tilde{\mu}_t$ defined as an average of $\mu_t$ over time
\[
\tilde{\mu}_t=\frac{1}{T} \int_{0}^{T} \mu_t dt = a+(x_0-a)\frac{1-e^{-\theta T}}{\theta T}
\]
and with the variance $\tilde{\sigma}_t^2=\sigma_t^2$. Then we can define a centered Gaussian process as $\hat{X}_t:=\tilde{X}_t-\tilde{\mu}_t$ and consider the probability to apply the Borell-TIS inequality in~\eqref{B-TISineq}:
\[
P(\max_{0\leq t \leq T}\tilde{X}_t >K |x_0)=P(\max_{0\leq t \leq T}\hat{X}_t >K-\tilde{\mu}_t|x_0)\leq e^{-(K-\tilde{\mu}_t)^2/2\tilde{\sigma}^2_T}
\]
where $\tilde{\sigma}^2_T=\sup_{0\leq t \leq T} \E[\hat{X}_t^2]=\frac{b^2}{2 \theta}(1-e^{-2\theta T})$.

If we assume $\rho_{x_0} \sim N(\mu_0, \sigma_0^2)$,  then the  importance density in~\eqref{ApproxOptimdensity} becomes Gaussian as follows

\begin{equation}
	\begin{split}
\tilde{\rho}_{x_0} (x) &\propto \rho_{x_0}(x) \sqrt{P(\max_{0\leq t \leq T}\tilde{X}_t >K |x_0=x)}\\
& \propto e^{-\frac{(x-\mu_0)^2}{2\sigma_0^2}}e^{-(K-\tilde{\mu}_t)^2/4\tilde{\sigma}^2_T}\\
& \propto e^{-\frac{(x-\mu_*)^2}{2\sigma_*^2}}
	\end{split}
\end{equation}
where $\sigma_*^2= \frac{2\tilde{\sigma}^2_T\sigma_0^2 \theta^2 T^2}{2\tilde{\sigma}^2_T \theta^2 T^2+\sigma_0^2(1-e^{-\theta T})^2}$ and $\mu_*=\sigma_*^2(\frac{\mu_0}{\sigma_0^2}+\frac{(K-a)(1-e^{-\theta T})}{2\sigma^2_T\theta T}+\frac{a(1-e^{-\theta T})^2}{2 \sigma^2_T \theta^2 T^2 })$.

As we mentioned, the function $h$ behaves like an indicator function when $K>>1$. Therefore, we can approximate the variance of $h$ as
\begin{equation}
\begin{split}
\mathbb{V}[h(\bar{X}_0, \bar{X}_1,..., \bar{X}_N)]&=\E[h^2(\bar{X}_0, \bar{X}_1,..., \bar{X}_N)]-(\E[h(\bar{X}_0, \bar{X}_1,..., \bar{X}_N)])^2\\
&\approx\alpha - \alpha^2 \approx \alpha.
\end{split}
\end{equation}
The estimator of $\alpha$ by crude Monte Carlo is defined by
\[
\hat{\alpha}_{MC}:= \frac{1}{M} \sum_{i=1}^{M} h(\bar{X}_0^{(i)}, \bar{X}_1^{(i)},..., \bar{X}_N^{(i)})
\]
where $\bar{X}_{0:N}^{(i)}$ are i.i.d. samples of $\bar{X}_{0:N}$ with the relative statistical error derived by the CLT
\begin{equation}
\epsilon_{st}\approx \frac{C_\alpha \sqrt{\mathbb{V}[h(\bar{X}_{0:N})]}}{\alpha \sqrt{M}}\approx \frac{C_\alpha}{\sqrt{\alpha M}}.
\label{errorMC}
\end{equation}
Similarly, we define the estimator of $\alpha$ with the importance sampling as
\[
\hat{\alpha}_{IS}:= \frac{1}{M} \sum_{i=1}^{M} h(\bar{X}_{0:N}^{(i)})L(\bar{X}_{0}^{(i)}) 
\]
and the corresponding relative statistical error is
\begin{equation}
\epsilon_{st}^{IS} \approx \frac{C_\alpha \sqrt{\mathbb{V}[h(\bar{X}_{0:N})L(\bar{X}_0)]}}{\alpha \sqrt{M}}
\label{errorIS}
\end{equation}
In simulations, we set $C_\alpha \approx 1.96$ and use the corresponding estimators for $\alpha$ and the sample variance for the estimation of the variance. We note that in the case of crude Monte Carlo for large values of $K>>1$, $\alpha_{MC}\approx 0$ which blows up $\epsilon_{st}$. To remedy this, we use $\alpha_{IS}$ in~\eqref{errorMC} for $K>>1$.

Table~\ref{table:OUerror} provides the simulation results of the Ornstein-Uhlenbeck example for the estimators and relative statistical errors with and without importance sampling technique given the range of thresholds $K=[3 ,\; 4,\; 4.5,\; 5, \; 5.5,\; 6]$. We observe, as it is expected, the estimator with importance sampling produces accurate approximations with much more reduced variance. 


\begin{table}[h!]
	\begin{tabular}{|c|ccl|ccl|l|l|}
		\hline
		\multirow{2}{*}{$K$} & \multicolumn{3}{c|}{\textbf{Estimator} }            & \multicolumn{3}{c|}{\textbf{Relative stat. error}}   &\multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{IS}}$} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{CE}}$} \\ \cline{2-7} 
		& \multicolumn{1}{c|}{$\hat{\alpha}_{MC}$} & \multicolumn{1}{c|}{$\hat{\alpha}_{IS}$} & \multicolumn{1}{c|}{$\hat{\alpha}_{CE}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{MC}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{IS}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{CE}$} & & \\ \hline
		3                  & \multicolumn{1}{c|}{0.0597}     &  \multicolumn{1}{c|}{0.0609} & \multicolumn{1}{c|}{0.0611}& \multicolumn{1}{c|}{2.51\%}     &  \multicolumn{1}{c|}{1.40\%} & \multicolumn{1}{c|}{2.84\%}  &\multicolumn{1}{c|}{3.23} & \multicolumn{1}{c|}{0.16} \\ \hline
		4                  & \multicolumn{1}{c|}{0.0035}     &  \multicolumn{1}{c|}{0.0034}  &  \multicolumn{1}{c|}{0.0033} &\multicolumn{1}{c|}{10.68\%}     &  \multicolumn{1}{c|}{2.77\% }&\multicolumn{1}{c|}{3.02\% } &\multicolumn{1}{c|}{14.89} &\multicolumn{1}{c|}{2.56}\\ \hline
		4.5                 & \multicolumn{1}{c|}{5.93e-04}     &  \multicolumn{1}{c|}{5.47e-04} &  \multicolumn{1}{c|}{5.37e-04}& \multicolumn{1}{c|}{26.50\%}     & \multicolumn{1}{c|}{4.09\%} & \multicolumn{1}{c|}{3.19\%} &\multicolumn{1}{c|}{41.99} &\multicolumn{1}{c|}{14.09}\\ \hline
		5                  & \multicolumn{1}{c|}{9.04e-05}      &  \multicolumn{1}{c|}{7.06e-05}  & \multicolumn{1}{c|}{6.92e-05} & \multicolumn{1}{c|}{73.29\%}     &  \multicolumn{1}{c|}{5.82\%} &  \multicolumn{1}{c|}{1.62\%} &\multicolumn{1}{c|}{158.34} &\multicolumn{1}{c|}{422.7}  \\ \hline
		5.5                  & \multicolumn{1}{c|}{4.88e-07}      &  \multicolumn{1}{c|}{6.92e-06}  &\multicolumn{1}{c|}{7.36e-06}  &\multicolumn{1}{c|}{235.52\%}     &  \multicolumn{1}{c|}{8.84\%} & \multicolumn{1}{c|}{2.29\%}&\multicolumn{1}{c|}{710.23}  &\multicolumn{1}{c|}{1986.9}  \\ \hline
		6                 & \multicolumn{1}{c|}{0}      &  \multicolumn{1}{c|}{6.85e-07}  &\multicolumn{1}{c|}{5.99e-07}  & \multicolumn{1}{c|}{748.81\%}     & \multicolumn{1}{c|}{13.98\%} & \multicolumn{1}{c|}{1.47\%} &\multicolumn{1}{c|}{2868.75} & \multicolumn{1}{c|}{58974} \\ \hline
	\end{tabular}
\caption{ Model parameters: $\theta=1, a=1, b=1$. Simulation parameters: $T=1$, $\Delta t= 0.001$, $x_0 \sim N(1,1)$, $M=10^5$. }
\label{table:OUerror}
\end{table}

\begin{table}[h!]
	\begin{tabular}{|c|ccl|ccl|l|l|}
		\hline
		\multirow{2}{*}{$K$} & \multicolumn{3}{c|}{\textbf{Estimator} }            & \multicolumn{3}{c|}{\textbf{Relative stat. error}}   &\multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{IS}}$} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{CE}}$} \\ \cline{2-7} 
		& \multicolumn{1}{c|}{$\hat{\alpha}_{MC}$} & \multicolumn{1}{c|}{$\hat{\alpha}_{IS}$} & \multicolumn{1}{c|}{$\hat{\alpha}_{CE}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{MC}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{IS}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{CE}$} & & \\ \hline
		3                  & \multicolumn{1}{c|}{0.0597}     &  \multicolumn{1}{c|}{0.0609} & \multicolumn{1}{c|}{0.0601}& \multicolumn{1}{c|}{2.51\%}     &  \multicolumn{1}{c|}{1.40\%} & \multicolumn{1}{c|}{2.72\%}  &\multicolumn{1}{c|}{3.23} & \multicolumn{1}{c|}{0.87} \\ \hline
		4                  & \multicolumn{1}{c|}{0.0035}     &  \multicolumn{1}{c|}{0.0034}  &  \multicolumn{1}{c|}{0.0033} &\multicolumn{1}{c|}{10.68\%}     &  \multicolumn{1}{c|}{2.77\% }&\multicolumn{1}{c|}{8.77\% } &\multicolumn{1}{c|}{14.89} &\multicolumn{1}{c|}{1.51}\\ \hline
		4.5                 & \multicolumn{1}{c|}{5.93e-04}     &  \multicolumn{1}{c|}{5.47e-04} &  \multicolumn{1}{c|}{5.69e-04}& \multicolumn{1}{c|}{26.50\%}     & \multicolumn{1}{c|}{4.09\%} & \multicolumn{1}{c|}{5.22\%} &\multicolumn{1}{c|}{41.99} &\multicolumn{1}{c|}{24.81}\\ \hline
		5                  & \multicolumn{1}{c|}{9.04e-05}      &  \multicolumn{1}{c|}{7.06e-05}  & \multicolumn{1}{c|}{7.34e-05} & \multicolumn{1}{c|}{73.29\%}     &  \multicolumn{1}{c|}{5.82\%} &  \multicolumn{1}{c|}{5.94\%} &\multicolumn{1}{c|}{158.34} &\multicolumn{1}{c|}{148.2}  \\ \hline
		5.5                  & \multicolumn{1}{c|}{4.88e-07}      &  \multicolumn{1}{c|}{6.92e-06}  &\multicolumn{1}{c|}{7.39e-06}  &\multicolumn{1}{c|}{235.52\%}     &  \multicolumn{1}{c|}{8.84\%} & \multicolumn{1}{c|}{3.11\%}&\multicolumn{1}{c|}{710.23}  &\multicolumn{1}{c|}{5371.6}  \\ \hline
		6                 & \multicolumn{1}{c|}{0}      &  \multicolumn{1}{c|}{6.85e-07}  &\multicolumn{1}{c|}{5.91e-07}  & \multicolumn{1}{c|}{748.81\%}     & \multicolumn{1}{c|}{13.98\%} & \multicolumn{1}{c|}{2.22\%} &\multicolumn{1}{c|}{2868.75} & \multicolumn{1}{c|}{1.3193e+05} \\ \hline
	\end{tabular}
	\caption{ Model parameters: $\theta=1, a=1, b=1$. Simulation parameters: $T=1$, $\Delta t= 0.001$, $x_0 \sim N(1,1)$, $M=10^5$. }
	\label{table:OUerror}
\end{table}
Notice that the total work depends on the number of Monte Carlo samples $M$ and the cost per sample. In both cases, the cost per sample is the same. Therefore, we study the values of $M$ to compare the computational costs of both cases. Given a certain tolerance $TOL$, we can derive the corresponding formulas for $M$ from~\eqref{errorMC} and~\eqref{errorIS}. Figure~\ref{fig:WorkvsK} illustrates the computational cost versus the values of $K$ of the respective two approximation methods for the tolerance $TOL=5\%$. We observe as $K$ increases the approximation with importance sampling requires much less computational cost than the crude Monte Carlo. 

\begin{figure}[h!]
\includegraphics[height=7cm, width=10cm]{WorkvsK.png}
\caption{Computational cost vs $K$ for a given $TOL=5\%$.}
\label{fig:WorkvsK}
\end{figure}

\textbf{Further directions for discussion:}\\
$\bullet$ Try different bounds instead of~\eqref{B-TISineq} and try different examples instead of Ornstein-Uhlenbeck process.\\
$\bullet$ Choose the importance sampling density $\tilde{\rho}$ as the Gaussian with a shifted mean $\tilde{\mu}$ and also with the scaled variance $\tilde{\sigma}^2$, then solve optimization problem with respect to $\tilde{\mu}$ and $\tilde{\sigma}$. We can employ the Cross-Entropy method.\\
$\bullet$  A change of measure with respect to the randomness $W_t$ (up to now it is not clear whether we need it).\\
$\bullet$ If we consider the general SDE with a constant diffusion part, we can approximate it with Ornstein-Uhlenbeck (OU) process. \\
\textbf{1st way.} Solve moment equations of the general SDE to get $m_t=\E[X_t]$ and $v_t=\E[X_t^2]$ and set the paramters of OU process as
$a=\lim_{t\rightarrow \infty} m_t$, the diffusion parameter is the same and $\theta=\frac{b^2}{2\lim_{t\rightarrow \infty} v_t}$.\\
\textbf{2nd way.} Linearise the drift term as \\
\[
a(X_t)=a(\bar{m}_t)+a'(\bar{m}_t)(x_t-\bar{m}_t)
\]
where $\bar{m}_t=\frac{1}{T}\int_0^T m_t dt$\\
$\bullet$ If we consider the general SDE (with a non-constant diffusion part), then we can approximate it with OU process and proceed with importance sampling.\\
%$\bullet$  Another idea for the general SDE, to use a so called IGBM process to approximate the main SDE\\
%\[
%dX_t=\theta (a-X_t)dt+bX_t dW_t
%\]
%which has a closed forms for its moments,
%and proceed with importance sampling. 
\bibliography{report5}
\bibliographystyle{plain}
\end{document}


