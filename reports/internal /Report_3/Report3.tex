\documentclass[]{amsart}
%\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb,amsthm}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{relsize}
\usepackage{tikz}
\usetikzlibrary{scopes}
\usetikzlibrary{backgrounds}
\usepackage{mdframed}
\newtheorem{theorem}{Theorem}
\newtheorem{assumption}{Assumption}
\newtheorem{remark}{Remark}
\newtheorem{notation}{Notation}
\newtheorem{lemma}{Lemma}
\newtheorem{corollary}{Corollary}
\usepackage{multirow}
\usepackage[width=1.0\textwidth, font=footnotesize]{caption}
%\usepackage{xcolor}
\usepackage{tcolorbox}
\usepackage[linesnumbered,ruled,vlined]{algorithm2e}
\SetKwInput{KwInput}{Input}                % Set the Input
\SetKwInput{KwOutput}{Output}   
\usepackage{physics}
\usepackage{amsmath}
\usepackage{tikz}
\usepackage{mathdots}
\usepackage{yhmath}
\usepackage{cancel}
\usepackage{color}
\usepackage{siunitx}
\usepackage{array}
\usepackage{multirow}
\usepackage{amssymb}
\usepackage{gensymb}
\usepackage{tabularx}
\usepackage{booktabs}
\usetikzlibrary{fadings}
\usetikzlibrary{patterns}
\usetikzlibrary{shadows.blur}
\usetikzlibrary{shapes}

\newcommand{\red}[1]{\textcolor{red}{#1}}

%%%%%%%%   Macros we will need for this paper %%%%%%%%%%%%%%%

% statistics notation
\newcommand{\Prob}[1]{\bP\left(#1\right)}
\newcommand{\Ex}[1]{\E \left[ #1 \right]}
\newcommand{\Cov}{\overline{\mathrm{Cov}}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\D}{\mathrm{D}}
\newcommand{\T}{\mathrm{T}}
\DeclareMathOperator*{\argmax}{argmax} 
\title{Report 3}
\author{Gaukhar Shaimerdenova}

\begin{document}
\maketitle
\begin{tcolorbox}
The report contains the preliminary algorithm of ISEnKF for tracking rare events.
\end{tcolorbox}

Let us consider the dynamics 
\begin{equation}\label{sde}
	\begin{cases}
		dX_t= a(X_t)dt+b(X_t)dW_t, \qquad t_n < t<t_{n+1}\\
		X_{t_n} \sim \mu_{t_n}.
	\end{cases}\,.
\end{equation}
Note that we have two sources of randomness in~\eqref{sde}, coming from the initial condition and  Wiener process $W_t$. 

Given the accumulated data $Y_n$ up to time $t_n$, our objective is to track the probability
\begin{equation}\label{qoi}
 P( \max_{t_n\leq t\leq t_{n+1}} X_t >K |Y_n)
\end{equation}
for a given threshold $K$ until the next measurement time $t_{n+1}$.

By ensemble Kalman filtering (EnKF), we approximate the probability~\eqref{qoi} at observation times by Gaussian computing the corresponding mean and variance, i.e. say at time $t_n$:
\[
P(X_{t_n}>K|Y_n)=1-P(X_{t_n}\leq K|Y_n)\approx 1-\Phi\Big(\frac{K-m_n}{\sigma_n}\Big),
\]
where $m_n$ and $\sigma_n^2$ are mean and variance at time $t_n$, and  $\Phi(x)=\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{x}e^{-t^2/2}dt$.

Suppose we want to estimate $P(X_{s}>K|Y_n)$ at the fixed time $s\in (t_n, t_{n+1})$. Let $P$ be the probabilty measure on the space $\Omega$ of continuous trajectories that is induced by the Brownian motion $(W_t)_{t\geq 0}$, i.e., a path space measure that drives the SDE:
\begin{equation}\label{sdeUnderP}
	\begin{cases}
	    dX_t= a(X_t)dt+b(X_t)dW_t^{\mathbb{P}}, \quad t<s\\
	    X_{t_n} \sim \mu_n.
	\end{cases}\,
\end{equation}
Let us define the process $W_t^{\mathbb{Q}}=W_t^{\mathbb{P}}-\int_{0}^{t} \theta(s,X_s)ds$ and substitute it in~\eqref{sdeUnderP}.
\begin{equation*}
	\begin{cases}
		dX_t=  (a(X_t)dt+\theta(t,X_t)b(X_t))dt+b(X_t)dW_t^{\mathbb{Q}}, \quad t<s\\
		X_{t_n} \sim \nu_n.
	\end{cases}\,
\end{equation*}
Note, by construction, that $(W_t^\mathbb{Q})_{t\geq}0$ is not a Brownian motion under $P$ since its expectation with respect to $P$ is not zero in general. We have to find a measure $Q\ll P$ under which $(W_t^\mathbb{Q})_{t\geq}0$ is a Brownian motion. Girsanov's theorem states that $(W_t^\mathbb{Q})_{t\geq}0$  is a standard Brownian motion under the probabilty measure $Q$ with the likelihood ratio
\[
\frac{d\mathbb{P}}{d\mathbb{Q}}=e^{-\int_{t_n}^{s} \theta(t,X_t)dW_t^{\mathbb{Q}}-\frac{1}{2}\int_{t_n}^{s}\theta^2(t,X_t)dt}.
\]
with respect to $\mathbb{P}$. Since the distribution of $W^{\mathbb{Q}}$ under $\mathbb{Q}$ is the same as the distribution of $W^{\mathbb{P}}$ under $\mathbb{P}$, we can work with the following SDE under $\mathbb{P}$:
	\begin{equation*}
	\begin{cases}
		dX_t=  (a(X_t)dt+\theta(t,X_t)b(X_t))dt+b(X_t)dW_t^{\mathbb{P}}, \quad t<s\\
		X_{t_n} \sim \nu_n.
	\end{cases}\,
\end{equation*}
Also, we assume $\mu_n \sim N(m_n,C_n)$ and $\nu_n \sim N(\bar{m}_n,C_n)$, where $\bar{m}_n=\argmax\limits_w\mathbf{1}_{\{X_{t_n}(w)>K\}}\rho_n(w)$ with $\rho_n\sim N({m}_{n},{C}_{n})$, and the corresponfing Radon-Nikodym derivative is 
\[
\frac{d\mu_n}{d\nu_n}=\frac{\frac{1}{\sqrt{2\pi \sigma_n^2}}e^{-\frac{(x-m_n)^2}{2\sigma_n^2}}}{\frac{1}{\sqrt{2\pi \sigma_n^2}}e^{-\frac{(x-\bar{m}_n)^2}{2\sigma_n^2}}}.
\]

Now we can apply change of measures as follows
\begin{equation*}
	\begin{split}
		P(X_s>K|Y_n) &= E_{\mu_n \otimes \mathbb{P}}[\boldsymbol{1}_{\{X_s>K\}}]=E_{\nu_n\otimes \mathbb{Q}}[\boldsymbol{1}_{\{X_s>K\}}\frac{d\mu_n}{d\nu_n}\frac{d\mathbb{P}}{d\mathbb{Q}}]\\
		&=e^{-\frac{(m_n^2-\bar{m}_n^2)}{2C_n}}E_{\nu_n\otimes \mathbb{Q}}[\boldsymbol{1}_{\{X_s>K\}} e^{-\frac{(m_n-\bar{m}_n)}{C_n}X_{t_n}}e^{-\int_{t_n}^{s} \theta(t,X_t)dW_t^{\mathbb{Q}}-\frac{1}{2}\int_{t_n}^{s}\theta^2(t,X_t)dt}].
	\end{split}
\end{equation*}



For ease of notation, let us denote $M_{t_n}^{t_{n+1}}:=\max_{t_n\leq t \leq t_{n+1}} X_t$. We define the first time the process $\{M_{t_n}^{t_{n+1}}\}$ attains the threshold $K>0$ after time $t_n$ by
\[
\tau_K=\inf \{t>t_{n}: X_t=K\}.
\]
 In the literature, the random variable $\tau_K$ is referred to \textit{a first passage time}.  Then observe the connection between the events:
\[
\{M_{t_n}^{t_{n+1}}\geq K\}=\{\tau_K\leq t_{n+1}\}
\]
For any $t\in (t_n, t_{n+1})$ (not fixed), we can write


\begin{equation}
	\begin{split}
		P( M_{t_n}^{t_{n+1}}\geq K|Y_n) &= E_{\mu_n \otimes \mathbb{P}}[\boldsymbol{1}_{\{ M_{t_n}^{t_{n+1}}\geq K \}}|Y_n]=E_{\mu_n \otimes\mathbb{P}}[\boldsymbol{1}_{\{ X_{\tau_K\wedge t_{n+1}}=K \}}|Y_n]
	\end{split}
\end{equation}

Now we can apply the optimal importance sampling strategies from the Donsker-Varadhan principle. We consider $\tau:=\tau_K\wedge t_{n+1}$ is a random stopping time. 
	\begin{equation}
		\begin{split}
		E_{\mu_n \otimes\mathbb{P}}[\boldsymbol{1}_{\{ X_{\tau}=K \}}|Y_n]=e^{-\frac{(m_n^2-\bar{m}_n^2)}{2C_n}}E_{\nu_n\otimes \mathbb{Q}}[\boldsymbol{1}_{\{X_\tau>K\}} e^{-\frac{(m_n-\bar{m}_n)}{C_n}X_{t_n}}e^{-\int_{t_n}^{\tau} \theta(t,X_t)dW_t^{\mathbb{Q}}-\frac{1}{2}\int_{t_n}^{\tau}\theta^2(t,X_t)dt}].
		\end{split}
	\end{equation}
The problem turns out into an optimal control problem finding optimal $\theta^*$. In paper by C.Hartmann et.al, by Theorem 2, we have a solution for this problem:
\[
\theta_t^*=\sigma(X_s^{\theta^*})^T \grad_x \log \Psi(X_s^{\theta^*}, s)
\]
where 
\[
\Psi(x,t)=	E_{\mu_n \otimes\mathbb{P}}[\boldsymbol{1}_{\{ X_{\tau}=K \}}|Y_n, X_t=x]
\]
is considered as a function of the initial condition $X_t=x$ with $0\leq t \leq \tau \leq T$.
\begin{tcolorbox}
Let  $D=\{X_t: X_t<K\}$ be a domain in real space and  the boundary $\partial D= \{ X_t: X_t=K\}$. We define
\[
\tau=\inf \{s>0: X_s\notin D\}=\inf \{s>0: X_s \geq K \},
\]
then we want to prove 
\[
E[\boldsymbol{1}_{\{ X_{\tau \wedge T}=K\}}]=P(\tau\leq T).
\]
By the law of total probability we have
\begin{equation}
	\begin{split}
E[\boldsymbol{1}_{\{ X_{\tau \wedge T}=K\}}]&=E[\boldsymbol{1}_{\{ X_{\tau \wedge T}=K\}}|\tau\leq T]P(\tau \leq T)+E[\boldsymbol{1}_{\{ X_{\tau \wedge T}=K\}}|\tau> T]P(\tau>T)\\
&=E[\boldsymbol{1}_{\{ X_{\tau}=K\}}|\tau\leq T]P(\tau \leq T)+E[\boldsymbol{1}_{\{ X_{T}=K\}}|\tau> T]P(\tau>T)\\
&=P(\tau \leq T)
	\end{split}
\end{equation}
since $E[\boldsymbol{1}_{\{ X_{\tau}=K\}}|\tau\leq T]=1$ and $E[\boldsymbol{1}_{\{ X_{T}=K\}}|\tau> T]=0.$

\textit{Comment: In this setting, $t_n=0$ and $t_{n+1}=T$. The case which Nadhir considered what about at initial time $t_n$, $X_{t_n}\geq K$, note that by definition $\tau$ cannot be equal to $t_n$, so there is no contradiction in the statement which we want to prove I think. $\tau$ is the first time when we hit the level $K$ strictly after time $t_n$ (or after time zero).}
\end{tcolorbox}
\begin{algorithm}[H]
	\DontPrintSemicolon
	
	\KwInput{Initial ensemble $\{x_{0,i}\}_{i=1}^M \sim N(u_0,\Gamma)$}
	\KwOutput{}

	
	\For{n=1 : $\mathcal{N}$}
	{  \textbf{Prediction step:} $\hat{x}_{n,i}=\Psi_n^N(x_{n-1,i})$ for $i=1,...,M.$
		%$\hat{m}_{n}=\frac{1}{M}\sum_{i=1}^M \hat{x}_{n,i},$
		$\hat{C}_{n}=\frac{1}{M}\sum_{i=1}^{M}\hat{x}_{n,i}(\hat{x}_{n,i})^\T-\sum_{i=1}^{M}\frac{\hat{x}_{n,i}}{M}\bigg(\sum_{i=1}^{M}\frac{\hat{x}_{n,i}}{M}\bigg)^\T.$
		
		\textbf{Update step:} $K_n=\hat{C}_{n} H^\T (H\hat{C}_{n}H^T+\Gamma)^{-1},$ 
		$x_{n,i}=(I-K_nH)\hat{x}_{n,i}+K_n(y_n+\eta_{n,i})$ for $i=1,...,M,\qquad\qquad\qquad$
		where $\{\eta_{n,i}\}_{i=1}^M$ are iid with $\eta_{n,1}\sim N(0,\Gamma)$.
		
	    \textbf{Compute sample mean and cov:} ${m}_{n}=\frac{1}{M}\sum_{i=1}^M x_{n,i},$
	     ${C}_{n}=\frac{1}{M}\sum_{i=1}^{M}x_{n,i}(x_{n,i})^\T-\sum_{i=1}^{M}\frac{x_{n,i}}{M}\bigg(\sum_{i=1}^{M}\frac{x_{n,i}}{M}\bigg)^\T.$
		
		
		\textbf{Optimal IS:} 
		$\bar{m}_n=\argmax\limits_w\mathbf{1}_{\{X_n(w)>K\}}\rho_n(w), \mbox{  where  } \rho_n\sim N({m}_{n},{C}_{n})$.
		
		\textbf{Generate auxilliary ensemble:} $\{\tilde{x}_{n,i}\}_{i=1}^M \sim N(\bar{m}_n,\hat{C}_n).$
		
		\textbf{Simulate the auxilliary ensemble:}
		$\hat{\tilde{x}}_{n+1,i}=\tilde{\Psi}_n^N(\tilde{x}_{n,i})\qquad\qquad$ where the dynamics $\tilde{\Psi}_n$ corresponds to the following SDE:
		$d\tilde{X}_t=a(\tilde{X}_t)dt+b(\tilde{X}_t)\mu(t,\tilde{X}_t)dt+b(\tilde{X}_t)dW_t,  \qquad t_{n}\leq t\leq t_{n+1}.\quad\qquad$
		Find $\mu$ optimally by solving 
		\begin{equation*}
		\begin{cases}
		  \partial_t\gamma+a(t,x)\partial_x \gamma +\frac{b^2}{2}\partial_x^2\gamma+\frac{b^2}{4}(\partial_x \gamma)^2=0,\\
		 \gamma(t_{n+1},x)=2 \log(\abs{\mathbf{1}_{\{x>K\}}}),
		\end{cases}
     	\end{equation*}
     then define the optimal $\mu^*(t,x)=\frac{b(t,x)\partial_x\gamma(t,x)}{2}.$
     
     
	}


	\caption{ISEnKF}
\end{algorithm}
 
\end{document}


