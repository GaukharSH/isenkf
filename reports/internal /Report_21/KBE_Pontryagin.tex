% Modelo de slides para projetos de disciplinas do Abel
\documentclass[10pt]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}
\usepackage[numbers,sort&compress]{natbib}
\bibliographystyle{plainnat}
\usepackage{adjustbox}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{multirow}
\usepackage{xspace}
\usepackage{mathtools}

\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\green}[1]{{\color{green} #1}}
\newcommand{\magenta}[1]{{\color{magenta} #1}}
\title{Rare events and filtering}
 \subtitle{Large Deviation Principle}
 \date{\today}
\date{July, 2023}
\author{Gaukhar Shaimerdenova}
\institute{AMCS, KAUST}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\V}{\mathbb{V}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\bu}{\boldsymbol{u}}
\newcommand{\bv}{\boldsymbol{v}}
\newcommand{\bx}{\boldsymbol{x}}
\newcommand{\ba}{\boldsymbol{a}}
\newcommand{\bb}{\boldsymbol{b}}
\newcommand{\bX}{\boldsymbol{X}}
\newcommand{\bK}{\boldsymbol{K}}
\newcommand{\brho}{\boldsymbol{\rho}}
\newcommand{\bmu}{\boldsymbol{\mu}}
\newcommand{\bSigma}{\boldsymbol{\Sigma}}
\newcommand{\bW}{\boldsymbol{W}}

%%%%%%%%   Macros we will need for this paper %%%%%%%%%%%%%%%

\newcommand{\norm}[1]{\left\| #1 \right\|}
\newcommand{\abs}[1]{\left| #1 \right|}
\newcommand{\ceil}[1]{\left \lceil #1 \right \rceil}
\newcommand{\prt}[1]{\left( #1 \right)}
\newcommand{\cost}[1]{\mathrm{Cost}}
\newcommand{\Tol}{\mathrm{TOL}}

% mathbb font letters 
\newcommand{\bF}{\mathbb{F}}
\newcommand{\bN}{\mathbb{N}}
\newcommand{\bP}{\mathbb{P}}
\newcommand{\bR}{\mathbb{R}}
\newcommand{\bZ}{\mathbb{Z}}
\newcommand{\bT}{\mathbf{T}}

% mathcal capital letters
\newcommand{\cB}{\mathcal{B}}
\newcommand{\cF}{\mathcal{F}}
\newcommand{\cO}{\mathcal{O}}
\newcommand{\cI}{\mathcal{I}}
\newcommand{\cJ}{\mathcal{J}}
\newcommand{\cK}{\mathcal{K}}
\newcommand{\cL}{\mathcal{L}}
\newcommand{\cN}{\mathcal{N}}
\newcommand{\cT}{\mathcal{T}}

%bold letters
\newcommand{\bw}{\boldsymbol{w}}

% ensemble notation 
\newcommand{\vBar}{\bar v}
\newcommand{\vHat}{\hat{v}}
\newcommand{\vBarHat}{\hat{\bar{v}}}
\newcommand{\meanHatMC}[1]{\hat{m}^{\rm{MC}}_{#1}}
\newcommand{\covHatMC}[1]{\widehat{C}^{\rm{MC}}_{#1}}
\newcommand{\yTilde}[1]{\tilde{y}_{#1}}
\newcommand{\meanHatML}[1]{\hat{m}^{\rm{ML}}_{#1}}
\newcommand{\covHatML}[1]{\widehat{C}^{\rm{ML}}_{#1}}
\newcommand{\kMC}[1]{K^{\rm{MC}}_{#1}}


% statistics notation
\newcommand{\Prob}[1]{\bP_{#1}}
\newcommand{\Ex}[1]{\E \left[ #1 \right]}
\newcommand{\Var}[1]{\V \left[ #1 \right]}
\newcommand{\Cov}{\overline{\mathrm{Cov}}}
\newcommand{\D}{\mathrm{D}}
\newcommand{\Ind}[2]{\textbf{1}_{\{#2\}}}

% colors
\newcommand{\blue}[1]{{\color{blue} #1}}


\begin{document}

\maketitle

\begin{frame}{Double Well example: value function comparison}
	{\footnotesize \textbf{Deterministic} means a solution of HJB based on the corresponding ODE;\\
		\textbf{Stochastic} means a solution of HJB based on the corresponding SDE;}
	\begin{figure}
		\centering
		\includegraphics[height=6cm, width=7cm]{ValueFunsigma005.png}
	\end{figure}
\end{frame}


\begin{frame}{Double Well example: value function comparison}
	{\footnotesize \textbf{Deterministic} means a solution of HJB based on the corresponding ODE;\\
		\textbf{Stochastic} means a solution of HJB based on the corresponding SDE;}
\begin{figure}
	\centering
	\includegraphics[height=4cm, width=4cm]{ValueFun_t0_sigma01_2.png}
	\includegraphics[height=4cm, width=4cm]{ValueFun_t0_sigma03_2.png}
	\includegraphics[height=4cm, width=4cm]{ValueFun_t0_sigma05_2.png}
    \includegraphics[height=4cm, width=4cm]{ValueFun_t0_sigma1_2.png}
\end{figure}
\end{frame}

\begin{frame}{Noisy Lorenz 63}
	A  noisy Lorenz '63  model 
	\begin{equation}\label{Langevin}
		\begin{cases} 
			du_1=r(u_2-u_1)dt+\sigma dW_1, \\
			du_2=(su_1-u_2-u_1u_3)dt+\sigma dW_2,\\
			du_3=(u_1u_2-qu_3)dt+\sigma dW_3,\;
		\end{cases}\,
	\end{equation}
	where $W_j, j=1,2,3$ is assumed to be independent Brownian motions, $\sigma=10$. \\
	Chaotic classic setting: $r=10, s=28, q=8/3.$\\
	Initial condition: $(u_1, u_2, u_3)=(0,0,1)+N(\mathbf{0}, 0.0025\mathbf{I});$\\
	We aim to track the first component, so $P_1=[1 \; 0 \; 0]$ over time $[0,1]$
\end{frame}

\begin{frame}{Lorenz 63: the same setting from the paper, M=4000}
	$x, y, z$ are the 1st, 2nd, 3rd components, respectively.
\begin{figure}
	\vspace*{-0.2cm}
	\centering
	\includegraphics[height=4cm, width=4cm]{L63scatterdistXY.png}
	\includegraphics[height=4cm, width=4cm]{L63scatterdistXZ.png}
		\includegraphics[height=4cm, width=4cm]{L63scatterdistYZ.png}
\end{figure}
\end{frame}

\begin{frame}{Lorenz 63: comparison of marginal densities for the 1st comp. and its projected version based on M=10000 samples}
	\begin{figure}
		\centering
		\includegraphics[height=8cm, width=8cm]{L63_margdensitycomp_u1.png}
	\end{figure}
\end{frame}

%\begin{frame}{Double Well example: optimal control comparison}
%	\begin{figure}
%		\centering
%		\includegraphics[height=4cm, width=4cm]{Control_t0_sigma01.png}
%		\includegraphics[height=4cm, width=4cm]{Control_t0_sigma05.png}
%		\includegraphics[height=4cm, width=4cm]{Control_t0_sigma1.png}
%	\end{figure}
%\end{frame}
\end{document}