\frametitle{Project goal}
We consider the general SDE of the form
\begin{equation}\label{sde}
	\begin{cases}
		dX_t= a(X_t)dt+b(X_t)dW_t^{\mathbb{P}}, \quad 0<t<T, \\
		X_{0}\sim \rho_0.
	\end{cases}\,
\end{equation}
\red{\textbf{Objective:}} For a given threshold $K>0$, to track the probability:
\[
\alpha:=P(\max_{0\leq t\leq T} X_t\geq K).
\]
\textbf{Notation:} $M^T:=\max_{0\leq t\leq T} X_t$.

Note we have two sources of randomness in the quantity of interest (QoI):
\[
\alpha= \mathbb{E}_{\rho_0 \times \mathbb{P}}[\textbf{1}_{\{M^T\geq K\}}]
\]
\red{\textbf{Focus:}}  We apply an importance sampling only with respect to the initial condition $X_0$ originally sampled from $\rho_0$.
