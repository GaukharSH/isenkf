% Modelo de slides para projetos de disciplinas do Abel
\documentclass[10pt]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}
\usepackage[numbers,sort&compress]{natbib}
\bibliographystyle{plainnat}
\usepackage{adjustbox}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{multirow}
\usepackage{xspace}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\magenta}[1]{{\color{magenta} #1}}
\title{Rare events and filtering}
 \subtitle{Preliminary results on the project}
 \date{\today}
\date{Jan, 2022}
\author{Gaukhar Shaimerdenova}
\institute{AMCS, KAUST}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\bu}{\boldsymbol{u}}
\newcommand{\bv}{\boldsymbol{v}}
\newcommand{\bx}{\boldsymbol{x}}
\begin{document}

\maketitle


\begin{frame}[fragile]{Project goal}
We consider the general SDE of the form
\begin{equation}\label{sde}
	\begin{cases} 
		dX_t= a(X_t)dt+b(X_t)dW_t^{\mathbb{P}}, \quad 0<t<T, \\
		X_{0}\sim \rho_0.
	\end{cases}\,
\end{equation}
\red{\textbf{Objective:}} For a given threshold $K>0$, to track the probability:
\[
\alpha:=P(\max_{0\leq t\leq T} X_t\geq K).
\] 
\textbf{Notation:} $M^T:=\max_{0\leq t\leq T} X_t$.

Note we have two sources of randomness in the quantity of interest (QoI):
\[
\alpha= \mathbb{E}_{\rho_0 \times \mathbb{P}}[\textbf{1}_{\{M^T\geq K\}}]
\]
\red{\textbf{Focus:}}  We apply an importance sampling only with respect to the initial condition $X_0$ originally sampled from $\rho_0$. 
\end{frame}

\begin{frame}{Importance Sampling}
\textbf{Importance sampling with respect to the initial condition:}
\begin{equation}
	\begin{split}
		 \mathbb{E}_{\rho_0 \times \mathbb{P}}[\textbf{1}_{\{M^T\geq K\}}] =\E_{\tilde{\rho}_0 \times \mathbb{P}} \Big[ \textbf{1}_{\{M^T\geq K\}} \frac{\rho_{0}(x)}{\tilde{\rho}_{0}(x)} \Big]
	\end{split}
\end{equation}
By the method of Lagrangian multipliers, we can deduce that \\
\textbf{the optimal importance density} which can reduce the variance of the estimator is
\begin{equation}\label{optimdensity}
	\tilde{\rho}_{0}(x) \propto \rho_{0}(x) \sqrt{ \E[\textbf{1}_{\{M^T\geq K\}}| X_0=x]}=\rho_{0}(x) \sqrt{P(M^T\geq K| X_0=x)}.
\end{equation}

\red{\textbf{!}} Now we have to approximate $P(M^T\geq K| X_0=x)$.\\

\textbf{1st approach:} Approximate $X_t$ by the Gaussian process and apply the Borell-TIS bound to estimate the probability. 

\textbf{2nd approach:} Apply multilevel Cross-Entropy method to compute the rare-event probability. 

\textbf{3rd approach:} Solve the backward Kolmogorov equation corresponding to the target probability. 

\end{frame}

\begin{frame}{Approach \#1}
	
	\red{\textbf{Idea:}} Approximate $X_t$ by a Gaussian process $\tilde{X}_t$ with mean $\mu_t$ and variance $\sigma_{t}^2$, so that $P(M^T \geq K |X_0=x) \approx P( \tilde{M}^T \geq K|X_0=x)$, where $\tilde{M}^T = \max_{0\leq t\leq T} \tilde{X}_t$.
	
	\small{\red{\textbf{Remark.}} If $\tilde{X}_t$ is a centered continuous Gaussian process, a.s. bounded on $[0,T]$, then we have an universal bound referred as the Borell-TIS inequality for target probability where $\sigma^2_T:=\sup_{t\in [0,T]} \E[\tilde{X}_t^2 ]$:
	\begin{equation}\label{B-TISineq}
		P(\sup_{t\in[0,T]} \tilde{X}_t > K ) \lesssim e^{-K^2/2\sigma^2_T},
	\end{equation}}

	\begin{table}[h!]
		\vspace*{-0.4cm}
			\begin{adjustbox}{width=\columnwidth,center}
		\begin{tabular}{|c|cc|cc|c|}
			\hline
			\multirow{2}{*}{$K$} & \multicolumn{2}{c|}{\textbf{Estimator} }            & \multicolumn{2}{c|}{\textbf{Relative stat. error}}   &\multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{IS}}$}    \\ \cline{2-5} 
			& \multicolumn{1}{c|}{\textit{without} IS: $\hat{\alpha}_{MC}$} & \textit{with} IS: $\red{\hat{\alpha}_{IS}}$ & \multicolumn{1}{c|}{\textit{without} IS: $\epsilon_{st}^{MC}$} & \textit{with} IS: $\red{\epsilon_{st}^{IS}}$ & \\ \hline
					3                  & \multicolumn{1}{c|}{0.0597}     & 0.0609  & \multicolumn{1}{c|}{2.51\%}     & 1.40\% &3.23\\ \hline
					4                  & \multicolumn{1}{c|}{0.0035}     & 0.0034  & \multicolumn{1}{c|}{10.68\%}     & 2.77\% &14.89\\ \hline
					4.5                 & \multicolumn{1}{c|}{5.93e-04}     & 5.47e-04 & \multicolumn{1}{c|}{26.50\%}     &4.09\% &41.99\\ \hline
					5                  & \multicolumn{1}{c|}{9.04e-05}      & 7.15e-05   & \multicolumn{1}{c|}{73.29\%}     & 5.82\% & 158.34 \\ \hline
					5.5                  & \multicolumn{1}{c|}{4.88e-07}      & 6.92e-06   & \multicolumn{1}{c|}{235.52\%}     & 8.84\% & 710.23  \\ \hline
					6                 & \multicolumn{1}{c|}{0}      & 6.85e-07   & \multicolumn{1}{c|}{748.81\%}     & 13.98\%  &2868.75 \\ \hline
		\end{tabular}
		\end{adjustbox}
\caption{ \tiny{ \textbf{OU process.} Model parameters: $\theta=1, a=1, b=1$. Simulation parameters: $T=1$, $\Delta t= 0.001$, $x_0 \sim N(1,1)$, $M=10^5$.} }
\label{table:OUerror}
\end{table}
\end{frame}

\begin{frame}{Approach \#2}
    \textbf{Cross-Entropy method.}\\
	$\bullet$ Assume the optimal importance sampling density $\tilde{\rho}$ to be  from the family of densities $\{\rho (.; \bv)\}$\\
	$\bullet$ Choose  the parameter vector $\bv$ such that the distance between $\tilde{\rho}$ and $\{\rho (.; \bv)\}$ is minimal. 
    We consider the minimisation problem of the \textit{Kullback-Leibler distance} (which is also termed the \textit{cross-entropy} between given densities). \\
    \red{\textbf{Advantage:}} The solution of this problem can often be calculated analytically. In particular, this happens if the distributions of the random variables belong to a natural exponential family. \\
     \red{\textbf{!}} However, for rare event probabilities, when the probability of the "target event" is too small, it is difficult to carry out since $h$ takes the value zero with a high probability.\\
    \textbf{Multilevel Cross-Entropy method.}\\
    $\bullet$ The main idea is to construct a sequence of reference parameters $\{\bv_\ell, \ell\geq 0\}$ and a sequence of threshold levels $\{K_\ell, \ell\geq 1\}$ to overcome the difficulty with rare-event probability. 
\end{frame}

\begin{frame}{Multilevel Cross-Entropy method: shift $\tilde{\mu}$ and scale $\tilde{\sigma}$}
\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
	\begin{tabular}{|c|ccl|ccl|l|l|}
		\hline
		\multirow{2}{*}{$K$} & \multicolumn{3}{c|}{\textbf{Estimator} }            & \multicolumn{3}{c|}{\textbf{Relative stat. error}}   &\multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{IS}}$} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{CE}}$} \\ \cline{2-7} 
		& \multicolumn{1}{c|}{$\hat{\alpha}_{MC}$} & \multicolumn{1}{c|}{$\hat{\alpha}_{IS}$} & \multicolumn{1}{c|}{$\red{\hat{\alpha}_{CE}}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{MC}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{IS}$} & \multicolumn{1}{c|}{$\red{\epsilon_{st}^{CE}}$} & & \\ \hline
		3                  & \multicolumn{1}{c|}{0.0597}     &  \multicolumn{1}{c|}{0.0609} & \multicolumn{1}{c|}{0.0601}& \multicolumn{1}{c|}{2.51\%}     &  \multicolumn{1}{c|}{1.40\%} & \multicolumn{1}{c|}{2.72\%}  &\multicolumn{1}{c|}{3.23} & \multicolumn{1}{c|}{0.87} \\ \hline
		4                  & \multicolumn{1}{c|}{0.0035}     &  \multicolumn{1}{c|}{0.0034}  &  \multicolumn{1}{c|}{0.0033} &\multicolumn{1}{c|}{10.68\%}     &  \multicolumn{1}{c|}{2.77\% }&\multicolumn{1}{c|}{8.77\% } &\multicolumn{1}{c|}{14.89} &\multicolumn{1}{c|}{1.51}\\ \hline
		4.5                 & \multicolumn{1}{c|}{5.93e-04}     &  \multicolumn{1}{c|}{5.47e-04} &  \multicolumn{1}{c|}{5.69e-04}& \multicolumn{1}{c|}{26.50\%}     & \multicolumn{1}{c|}{4.09\%} & \multicolumn{1}{c|}{5.22\%} &\multicolumn{1}{c|}{41.99} &\multicolumn{1}{c|}{24.81}\\ \hline
		5                  & \multicolumn{1}{c|}{9.04e-05}      &  \multicolumn{1}{c|}{7.06e-05}  & \multicolumn{1}{c|}{7.34e-05} & \multicolumn{1}{c|}{73.29\%}     &  \multicolumn{1}{c|}{5.82\%} &  \multicolumn{1}{c|}{5.94\%} &\multicolumn{1}{c|}{158.34} &\multicolumn{1}{c|}{148.2}  \\ \hline
		5.5                  & \multicolumn{1}{c|}{4.88e-07}      &  \multicolumn{1}{c|}{6.92e-06}  &\multicolumn{1}{c|}{7.39e-06}  &\multicolumn{1}{c|}{235.52\%}     &  \multicolumn{1}{c|}{8.84\%} & \multicolumn{1}{c|}{3.11\%}&\multicolumn{1}{c|}{710.23}  &\multicolumn{1}{c|}{5371.6}  \\ \hline
		6                 & \multicolumn{1}{c|}{0}      &  \multicolumn{1}{c|}{6.85e-07}  &\multicolumn{1}{c|}{5.91e-07}  & \multicolumn{1}{c|}{748.81\%}     & \multicolumn{1}{c|}{13.98\%} & \multicolumn{1}{c|}{2.22\%} &\multicolumn{1}{c|}{2868.75} & \multicolumn{1}{c|}{1.3193e+05} \\ \hline
	\end{tabular}
\end{adjustbox}
	\caption{ \textbf{OU process.} Model parameters: $\theta=1, a=1, b=1$. Simulation parameters: $T=1$, $\Delta t= 0.001$, $x_0 \sim N(1,1)$, $M=10^5$. }
	\label{table:OUerror}
\end{table}
\red{\textbf{Remark.}} Note that the CE method with a Gaussian assumption for the initial condition may lead to infinite variance of the estimator. Therefore, we observe certain non-robustness of the estimator variance produced by CE method 
\end{frame}

\begin{frame}{Multilevel Cross-Entropy method: fix $\tilde{\mu}/\tilde{\sigma}$ - OU process}
	\red{\textbf{!}} Below we test two cases: \\
	$\bullet$ when we fix $\tilde{\sigma}=\sigma_0$ and shift $\tilde{\mu}$; \\
	$\bullet$ when we fix $\tilde{\mu}=\mu_0$ and scale $\tilde{\sigma}$. 
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
		\begin{tabular}{|c|ccl|ccl|l|l|}
			\hline
			\multirow{2}{*}{$K$} & \multicolumn{3}{c|}{\textbf{Estimator} }            & \multicolumn{3}{c|}{\textbf{Relative stat. error}}   &\multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{CE,\tilde{\mu}}}$} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{CE, \tilde{\sigma}}}$} \\ \cline{2-7} 
			& \multicolumn{1}{c|}{$\hat{\alpha}_{MC}$} & \multicolumn{1}{c|}{$\red{\hat{\alpha}_{CE}^{\tilde{\mu}}}$} & \multicolumn{1}{c|}{$\red{\hat{\alpha}_{CE}^{\tilde{\sigma}}}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{MC}$} & \multicolumn{1}{c|}{$\red{\epsilon_{st}^{CE,\tilde{\mu}}}$} & \multicolumn{1}{c|}{$\red{\epsilon_{st}^{CE,\tilde{\sigma}}}$} & & \\ \hline
			3                  & \multicolumn{1}{c|}{0.0597}     &  \multicolumn{1}{c|}{0.0609} & \multicolumn{1}{c|}{0.0620}& \multicolumn{1}{c|}{2.51\%}     &  \multicolumn{1}{c|}{1.84\%} & \multicolumn{1}{c|}{1.97\%}  &\multicolumn{1}{c|}{1.87} & \multicolumn{1}{c|}{1.60} \\ \hline
			4                  & \multicolumn{1}{c|}{0.0035}     &  \multicolumn{1}{c|}{0.0033}  &  \multicolumn{1}{c|}{0.0033} &\multicolumn{1}{c|}{10.68\%}     &  \multicolumn{1}{c|}{2.90\% }&\multicolumn{1}{c|}{4.37\% } &\multicolumn{1}{c|}{13.88} &\multicolumn{1}{c|}{6.09}\\ \hline
			4.5                 & \multicolumn{1}{c|}{5.93e-04}     &  \multicolumn{1}{c|}{5.44e-04} &  \multicolumn{1}{c|}{5.49e-04}& \multicolumn{1}{c|}{26.50\%}     & \multicolumn{1}{c|}{3.39\%} & \multicolumn{1}{c|}{8.70\%} &\multicolumn{1}{c|}{61.31} &\multicolumn{1}{c|}{9.24}\\ \hline
			5                  & \multicolumn{1}{c|}{9.04e-05}      &  \multicolumn{1}{c|}{7.19e-05}  & \multicolumn{1}{c|}{7.03e-05} & \multicolumn{1}{c|}{73.29\%}     &  \multicolumn{1}{c|}{3.15\%} &  \multicolumn{1}{c|}{9.18\%} &\multicolumn{1}{c|}{537.1} &\multicolumn{1}{c|}{64.83}  \\ \hline
			5.5                  & \multicolumn{1}{c|}{4.88e-07}      &  \multicolumn{1}{c|}{7.41e-06}  &\multicolumn{1}{c|}{7.0e-06}  &\multicolumn{1}{c|}{235.52\%}     &  \multicolumn{1}{c|}{2.95\%} & \multicolumn{1}{c|}{5.77\%}&\multicolumn{1}{c|}{5976.7}  &\multicolumn{1}{c|}{1647.2}  \\ \hline
			6                 & \multicolumn{1}{c|}{0}      &  \multicolumn{1}{c|}{6.85e-07}  &\multicolumn{1}{c|}{6.06e-07}  & \multicolumn{1}{c|}{748.81\%}     & \multicolumn{1}{c|}{3.95\%} & \multicolumn{1}{c|}{6.55\%} &\multicolumn{1}{c|}{40700} & \multicolumn{1}{c|}{14865} \\ \hline
		\end{tabular}
		\end{adjustbox}
		\caption{ \textbf{Ornstein-Uhlenbeck SDE.} Model parameters: $\theta=1, a=1, b=1$. Simulation parameters: $T=1$, $\Delta t= 0.001$, $x_0 \sim N(1,1)$, $M=10^5$. }
		\label{table:OUerror}
	\end{table}
	
\end{frame}

\begin{frame}{Multilevel Cross-Entropy method: fix $\tilde{\mu}/\tilde{\sigma}$ - DW process}
	
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|ccl|ccl|l|l|}
				\hline
			\multirow{2}{*}{$K$} & \multicolumn{3}{c|}{\textbf{Estimator} }            & \multicolumn{3}{c|}{\textbf{Relative stat. error}}   &\multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{CE,\tilde{\mu}}}$} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{CE, \tilde{\sigma}}}$} \\ \cline{2-7} 
			& \multicolumn{1}{c|}{$\hat{\alpha}_{MC}$} & \multicolumn{1}{c|}{$\red{\hat{\alpha}_{CE}^{\tilde{\mu}}}$} & \multicolumn{1}{c|}{$\red{\hat{\alpha}_{CE}^{\tilde{\sigma}}}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{MC}$} & \multicolumn{1}{c|}{$\red{\epsilon_{st}^{CE,\tilde{\mu}}}$} & \multicolumn{1}{c|}{$\red{\epsilon_{st}^{CE,\tilde{\sigma}}}$} & & \\ \hline
			3                  & \multicolumn{1}{c|}{0.0691}     &  \multicolumn{1}{c|}{0.0682} & \multicolumn{1}{c|}{0.0684}& \multicolumn{1}{c|}{2.36\%}     &  \multicolumn{1}{c|}{1.92\%} & \multicolumn{1}{c|}{1.97\%}  &\multicolumn{1}{c|}{2.12} & \multicolumn{1}{c|}{1.52} \\ \hline
			4                  & \multicolumn{1}{c|}{0.0052}     &  \multicolumn{1}{c|}{0.0055}  &  \multicolumn{1}{c|}{0.0054} &\multicolumn{1}{c|}{8.61\%}     &  \multicolumn{1}{c|}{4.77\% }&\multicolumn{1}{c|}{4.37\% } &\multicolumn{1}{c|}{1.33} &\multicolumn{1}{c|}{3.15}\\ \hline
			4.5               & \multicolumn{1}{c|}{0.001}     &  \multicolumn{1}{c|}{0.001} &  \multicolumn{1}{c|}{0.001}& \multicolumn{1}{c|}{18.31\%}     & \multicolumn{1}{c|}{4.57\%} & \multicolumn{1}{c|}{7.48\%} &\multicolumn{1}{c|}{17.91} &\multicolumn{1}{c|}{6.39}\\ \hline
			5                  & \multicolumn{1}{c|}{1.81e-04}      &  \multicolumn{1}{c|}{1.63e-04}  & \multicolumn{1}{c|}{1.50e-04} & \multicolumn{1}{c|}{46.14\%}     &  \multicolumn{1}{c|}{6.98\%} &  \multicolumn{1}{c|}{10.86\%} &\multicolumn{1}{c|}{48.44} &\multicolumn{1}{c|}{21.72}  \\ \hline
			5.5               & \multicolumn{1}{c|}{2.00e-05}      &  \multicolumn{1}{c|}{2.06e-05}  &\multicolumn{1}{c|}{2.67e-05}  &\multicolumn{1}{c|}{138.59\%}     &  \multicolumn{1}{c|}{15.07\%} & \multicolumn{1}{c|}{57.7\%}&\multicolumn{1}{c|}{82.13}  &\multicolumn{1}{c|}{4.32}  \\ \hline
			6                 & \multicolumn{1}{c|}{0}      &  \multicolumn{1}{c|}{1.85e-06}  &\multicolumn{1}{c|}{3.57e-06}  & \multicolumn{1}{c|}{455.69\%}     & \multicolumn{1}{c|}{17.41\%} & \multicolumn{1}{c|}{88.53\%} &\multicolumn{1}{c|}{687.01} & \multicolumn{1}{c|}{13.74} \\ \hline
			\end{tabular}
		\end{adjustbox}
		\caption{ \textbf{Double Well SDE.} Model parameters: $\theta=1, a=1, b=1$. Simulation parameters: $T=1$, $\Delta t= 0.001$, $x_0 \sim N(1,1)$, $M=10^5$. }
		\label{table:OUerror}
	\end{table}
	\red{\textbf{Remark:}} The Cross-Entropy method with scaling $\tilde \sigma$ and fixing $\tilde \mu$ produces much more non-stable variance estimates than the vice versa case. However, in both cases we observed this the non-robustness issue. 
\end{frame}

\begin{frame}{Approach \#3: PDE method}
	\red{\textbf{Idea:}} Solve the backward Kolmogorov equation (BKE) corresponding to the target probability:
	\[
	P(\max_{t\leq s\leq T} X_s \geq K | X_t=x) = u(x,t)
	\]
	with Dirichlet boundary condition on the domain $(-\infty, K] \times [0,T]$, 
\[
		\left\{
		\begin{array}{ll}
			u_t (x,t) = -a(x,t) u_x (x,t) -\frac{1}{2} b^2 (x,t) u_{xx} (x,t),\\
			u(x, T) =0, \quad x<K,\\
			u(K, t) =1. 
		\end{array}
	\right.
\]
$\bullet$ We solve the BKE by the Crank-Nicolson scheme. 
	\begin{figure}[hbt!]
		\centering
		\vspace*{-0.2cm}
		\includegraphics[scale=0.12]{PDEsolDWK4.png}	
	\end{figure}
\end{frame}

\begin{frame}{Convergence of Crank-Nicolsen scheme}
	\begin{figure}[hbt!]
		\centering
		\vspace*{-0.1cm}
	    \includegraphics[scale=0.25]{{DWvarydxfixdt1e4.png}}
		\includegraphics[scale=0.25]{{DWvarydtfixdx2e3.png}}
		\vspace*{-1cm}
		\caption{\textbf{DW process}. \textit{Left:} Vary $N_x$ and fix $N_t=1e4$. \textit{Right:} Vary $N_t$ and fix $N_x=2e3$. Model parameters: $\mu=1$, $\sigma=1$, $\theta=1$. }
	\end{figure}
%	\begin{figure}[hbt!]
%		\centering
%		\includegraphics[scale=0.2]{{DWvarydxfixdt1e4.png}}
%			\includegraphics[scale=0.2]{{DWvarydtfixdx2e3.png}}
%		\vspace*{-4.5cm}	
%		\caption{\textbf{DW process}. Vary $N_x$ and fix $N_t=1e4$. Model parameters: $\mu=1$, $\sigma=1$, $\theta=1$. }
%	\end{figure}
\end{frame}

\begin{frame}{PDE method: OU process}
	\large{ \textbf{Ornstein-Uhlenbeck process:}}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|ccl|ccl|l|l|}
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{3}{c|}{\textbf{Estimator} }            & \multicolumn{3}{c|}{\textbf{Relative stat. error}}   &\multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{CE,\tilde{\mu}}}$} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu},\tilde{\sigma}}}$} \\ \cline{2-7} 
				& \multicolumn{1}{c|}{$\hat{\alpha}_{MC}$} & \multicolumn{1}{c|}{$\hat{\alpha}_{CE}^{\tilde{\mu}}$} & \multicolumn{1}{c|}{$\red{\hat{\alpha}_{PDE}^{\tilde{\mu},\tilde{\sigma}}}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{MC}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{CE,\tilde{\mu}}$} & \multicolumn{1}{c|}{$\red{\epsilon_{st}^{PDE,\tilde{\mu},\tilde{\sigma}}}$} & & \\ \hline
				4                  & \multicolumn{1}{c|}{0.0608}     &  \multicolumn{1}{c|}{0.0602} & \multicolumn{1}{c|}{0.0605}& \multicolumn{1}{c|}{1.12\%}     &  \multicolumn{1}{c|}{0.90\%} & \multicolumn{1}{c|}{0.62\%}  &\multicolumn{1}{c|}{1.58} & \multicolumn{1}{c|}{3.26} \\ \hline
				4.5                  & \multicolumn{1}{c|}{0.0034}     &  \multicolumn{1}{c|}{0.0035}  &  \multicolumn{1}{c|}{0.0033} &\multicolumn{1}{c|}{4.78\%}     &  \multicolumn{1}{c|}{\magenta{9.45\%} }&\multicolumn{1}{c|}{1.27\% } &\multicolumn{1}{c|}{\magenta{0.25}} &\multicolumn{1}{c|}{14.46}\\ \hline
				5                 & \multicolumn{1}{c|}{5.55e-04}     &  \multicolumn{1}{c|}{5.40e-04} &  \multicolumn{1}{c|}{5.46e-04}& \multicolumn{1}{c|}{11.77\%}     & \multicolumn{1}{c|}{1.41\%} & \multicolumn{1}{c|}{1.61\%} &\multicolumn{1}{c|}{71.95} &\multicolumn{1}{c|}{54.23}\\ \hline
				5.5                  & \multicolumn{1}{c|}{7.97e-05}      &  \multicolumn{1}{c|}{7.02e-05}  & \multicolumn{1}{c|}{7.17e-05} & \multicolumn{1}{c|}{31.05\%}     &  \multicolumn{1}{c|}{1.28\%} &  \multicolumn{1}{c|}{2.01\%} &\multicolumn{1}{c|}{666.12} &\multicolumn{1}{c|}{263.57}  \\ \hline
			\end{tabular}
		\end{adjustbox}
		\caption{  Model parameters: $\theta=1, a=1, b=1$. Simulation parameters: $T=1$, $\Delta t= 0.001$, $x_0 \sim N(1,1)$, $M=5 \times 10^5$. In $\hat{\alpha}_{CE}^{\tilde{\mu}}$: $\tilde{\rho}_{x_0} \sim N(\tilde{\mu}^{CE},1 )$ and in $\hat{\alpha}_{PDE}^{\tilde{\mu}}: \tilde{\rho}_{x_0}\sim N(\tilde{\mu}^{PDE},\tilde{\sigma}^{PDE} )$ .}
		\label{table:OUerror}
	\end{table}
\end{frame}

\begin{frame}
	
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=7cm, width=12cm]{OUK5rho.png}
	\end{figure}
$$	\tilde{\rho}_{0}(x) \propto \rho_{0}(x) \sqrt{P(M^T\geq K| X_0=x)}.$$
\end{frame}

\begin{frame}{PDE mthod: DW process (sampling in the same well)}
	\large{ \textbf{Double Well process:}}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|ccl|ccl|l|l|}
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{3}{c|}{\textbf{Estimator} }            & \multicolumn{3}{c|}{\textbf{Relative stat. error}}   &\multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{CE,\tilde{\mu}}}$} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ \cline{2-7} 
				& \multicolumn{1}{c|}{$\hat{\alpha}_{MC}$} & \multicolumn{1}{c|}{$\hat{\alpha}_{CE}^{\tilde{\mu}}$} & \multicolumn{1}{c|}{$\red{\hat{\alpha}_{PDE}^{\tilde{\mu},\tilde{\sigma}}}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{MC}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{CE,\tilde{\mu}}$} & \multicolumn{1}{c|}{$\red{\epsilon_{st}^{PDE,\tilde{\mu},\tilde{\sigma}}}$} & & \\ \hline
				4                 & \multicolumn{1}{c|}{0.00530}     &  \multicolumn{1}{c|}{0.00538} & \multicolumn{1}{c|}{0.00541}& \multicolumn{1}{c|}{3.81\%}     &  \multicolumn{1}{c|}{1.76\%} & \multicolumn{1}{c|}{1.38\%}  &\multicolumn{1}{c|}{4.61} & \multicolumn{1}{c|}{7.45} \\ \hline
				4.5                 & \multicolumn{1}{c|}{0.00108}     &  \multicolumn{1}{c|}{0.00105}  &  \multicolumn{1}{c|}{0.00110} &\multicolumn{1}{c|}{8.43\%}     &  \multicolumn{1}{c|}{2.90\% }&\multicolumn{1}{c|}{2.00\% } &\multicolumn{1}{c|}{8.70} &\multicolumn{1}{c|}{18.23}\\ \hline
				5               & \multicolumn{1}{c|}{2.06e-04}     &  \multicolumn{1}{c|}{1.65e-04} &  \multicolumn{1}{c|}{1.61e-04}& \multicolumn{1}{c|}{19.31\%}     & \multicolumn{1}{c|}{4.35\%} & \multicolumn{1}{c|}{3.45\%} &\multicolumn{1}{c|}{24.63} &\multicolumn{1}{c|}{39.99}\\ \hline
				5.5                 & \multicolumn{1}{c|}{2.55e-05}      &  \multicolumn{1}{c|}{1.91e-05}  & \multicolumn{1}{c|}{1.91e-05} & \multicolumn{1}{c|}{54.86\%}     &  \multicolumn{1}{c|}{6.59\%} &  \multicolumn{1}{c|}{6.37\%} &\multicolumn{1}{c|}{92.83} &\multicolumn{1}{c|}{99.26}  \\ \hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\theta=1, a=1, b=1$. Simulation parameters: $T=1$, $\Delta t= 0.001$, $\red{x_0 \sim N(1,1)}$, $M=5 \times 10^5$. In $\hat{\alpha}_{CE}^{\tilde{\mu}}$: $\tilde{\rho}_{x_0} \sim N(\tilde{\mu}^{CE},1 )$ and in $\hat{\alpha}_{PDE}^{\tilde{\mu},\tilde{\sigma}}: \tilde{\rho}_{x_0}\sim N(\tilde{\mu}^{PDE},\tilde{\sigma}^{PDE} )$ .}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}
	
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=8cm, width=12cm]{DWK5rho.png}
	\end{figure}
\end{frame}

\begin{frame}{PDE method: DW process (sampling in different wells)}
	\large{ \textbf{Double Well process:}}
	\begin{table}[h!]
		\begin{adjustbox}{width=\columnwidth,center}
			\begin{tabular}{|c|ccl|ccl|l|l|}
				\hline
				\multirow{2}{*}{$K$} & \multicolumn{3}{c|}{\textbf{Estimator} }            & \multicolumn{3}{c|}{\textbf{Relative stat. error}}   &\multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{CE,\tilde{\mu}}}$} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}, \tilde{\sigma}}}$} \\ \cline{2-7} 
				& \multicolumn{1}{c|}{$\hat{\alpha}_{MC}$} & \multicolumn{1}{c|}{$\hat{\alpha}_{CE}^{\tilde{\mu}}$} & \multicolumn{1}{c|}{$\red{\hat{\alpha}_{PDE}^{\tilde{\mu},\tilde{\sigma}}}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{MC}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{CE,\tilde{\mu}}$} & \multicolumn{1}{c|}{$\red{\epsilon_{st}^{PDE,\tilde{\mu},\tilde{\sigma}}}$} & & \\ \hline
				2                 & \multicolumn{1}{c|}{0.02179}     &  \multicolumn{1}{c|}{0.02177} & \multicolumn{1}{c|}{0.02160}& \multicolumn{1}{c|}{1.88\%}     &  \multicolumn{1}{c|}{1.38\%} & \multicolumn{1}{c|}{1.04\%}  &\multicolumn{1}{c|}{1.84} & \multicolumn{1}{c|}{3.29} \\ \hline
				3                 & \multicolumn{1}{c|}{9.84e-04}     &  \multicolumn{1}{c|}{9.12e-04}  &  \multicolumn{1}{c|}{9.19e-04} &\multicolumn{1}{c|}{8.83\%}     &  \multicolumn{1}{c|}{4.36\% }&\multicolumn{1}{c|}{3.48\% } &\multicolumn{1}{c|}{4.43} &\multicolumn{1}{c|}{6.90}\\ \hline
				3.5               & \multicolumn{1}{c|}{1.65e-04}     &  \multicolumn{1}{c|}{1.45e-04} &  \multicolumn{1}{c|}{1.30e-04}& \multicolumn{1}{c|}{21.56\%}     & \multicolumn{1}{c|}{\magenta{21.60\%}} & \multicolumn{1}{c|}{7.55\%} &\multicolumn{1}{c|}{\magenta{1.13}} &\multicolumn{1}{c|}{10.38}\\ \hline
				4                  & \multicolumn{1}{c|}{2.58e-05}      &  \multicolumn{1}{c|}{1.36e-05}  & \multicolumn{1}{c|}{1.62e-05} & \multicolumn{1}{c|}{54.61\%}     &  \multicolumn{1}{c|}{9.98\%} &  \multicolumn{1}{c|}{13.20\%} &\multicolumn{1}{c|}{56.90} &\multicolumn{1}{c|}{27.22}  \\ \hline
			\end{tabular}
		\end{adjustbox}
		\caption{ Model parameters: $\theta=1, a=1, b=1$. Simulation parameters: $T=1$, $\Delta t= 0.001$, $\red{x_0 \sim N(-1,1)}$, $M=5 \times 10^5$. In $\hat{\alpha}_{CE}^{\tilde{\mu}}$: $\tilde{\rho}_{x_0} \sim N(\tilde{\mu}^{CE},\sigma_0 )$ and in $\hat{\alpha}_{PDE}^{\tilde{\mu},\tilde{\sigma}}: \tilde{\rho}_{x_0}\sim N(\tilde{\mu}^{PDE},\tilde{\sigma}^{PDE} )$ .}
		\label{table:DWerror}
	\end{table}
\end{frame}

\begin{frame}
	
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=8cm, width=12cm]{DWK4rho2ndwell.png}
	\end{figure}
\end{frame}

\begin{frame}{Conclusion}
$\bullet$ We tested three approaches. \\
$\bullet$  Non-robustness of the Cross-Entropy method can lead to the considerable variability of the variance. Increasing the Monte Carlo sample size can reduce the variability slightly. \\
$\bullet$ The PDE approach is the optimal one since it gives the true estimates for the target probability. We observe the variability of the variance too. However, increasing the Monte Carlo sample size stabilizes the varaince much better compared to CE method. \\
$\bullet$  Instead of fitting the solution of the PDE to the Gaussian, we can sample directly from the obtained solution for $\tilde{\rho}_0$, for example, via Inverse Transform Sampling (ITS) method:

\textbf{Algorithm (ITS).}\\
1. Generate $U \sim \mbox{Unif} [0,1]$,\\
2. Find the inverse CDF: $\tilde{F}_{0}^{-1}(x)$,\\
3. Compute $X_0=\tilde{F}_{0}^{-1}(U)$.

\end{frame}

%\begin{frame}
%	
%	\begin{figure}[h!]
%		\hspace*{-1cm}
%		\includegraphics[height=7cm, width=12cm]{CEconfintK5OU3.png}
%		\caption{\small{OU process. 5 independent runs of CE method. $S=10^4$. $B=10^3$}}
%	\end{figure}
%\end{frame}
%
%\begin{frame}
%	
%	\begin{figure}[h!]
%		\hspace*{-1cm}
%		\includegraphics[height=7cm, width=12cm]{CEconfintK5OU2.png}
%		\caption{\small{OU process. 5 independent runs of CE method. $S=10^4$. $B=10^3$}}
%	\end{figure}
%\end{frame}
%
%\begin{frame}
%	
%	\begin{figure}[h!]
%		\hspace*{-1cm}
%		\includegraphics[height=7cm, width=12cm]{CEconfintK5OU4.png}
%		\caption{\small{OU process. 5 independent runs of PDE method. $S=10^4$. $B=10^3$}}
%	\end{figure}
%\end{frame}
%
%\begin{frame}
%
%	\begin{figure}[h!]
%		\hspace*{-1cm}
%		\includegraphics[height=7cm, width=12cm]{CEconfintK5OU4B1e4.png}
%		\caption{\small{OU process. 5 independent runs of CE method. $S=10^4$. $B=10^4$}}
%	\end{figure}
%\end{frame}
%
%\begin{frame}
%	
%	\begin{figure}[h!]
%		\hspace*{-1cm}
%		\includegraphics[height=7cm, width=12cm]{CEconfintK5OU4S1e5.png}
%		\caption{\small{OU process. 5 independent runs of CE method. $S=10^5$. $B=10^4$}}
%	\end{figure}
%\end{frame}
%
%\begin{frame}
%	
%	\begin{figure}[h!]
%		\hspace*{-1cm}
%		\includegraphics[height=7cm, width=12cm]{PDEconfintK5OU.png}
%		\caption{\small{OU process. 5 independent runs of PDE method. $S=10^5$. $B=10^3$}}
%	\end{figure}
%\end{frame}
%
%\begin{frame}
%	\begin{figure}[h!]
%		\hspace*{-1cm}
%		\includegraphics[height=7cm, width=12cm]{CEconfintK5OU4Btrunc.png}
%		\caption{\small{OU process. 5 independent runs of CE method. $S=10^4$. $B=10^4$}}
%	\end{figure}
%\end{frame}
%\begin{frame}
%	\begin{figure}[h!]
%	\hspace*{-1cm}
%	\includegraphics[height=7cm, width=12cm]{PDEconfintK5OUBtrunc.png}
%	\caption{\small{OU process. 5 independent runs of PDE method. $S=10^4$. $B=10^4$}}
%\end{figure}
%\end{frame}


%\begin{frame}
%	\begin{figure}[h!]
%		\hspace*{-1cm}
%		\includegraphics[height=7cm, width=12cm]{OUbootstrp.png}
%		\caption{\small{OU process. Confidence interval of samle standard deviation by PDE and CE methods. $S=10^5$. $B=10^4$}}
%	\end{figure}
%\end{frame}

\begin{frame}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=7cm, width=12cm]{OUbootstrp2.png}
		\caption{\small{OU process. Confidence interval of sample standard deviation by PDE and CE methods. $S=10^5$. $B=10^4$ (Another run)}}
	\end{figure}
\end{frame}


\begin{frame}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=7cm, width=12cm]{DWbootstrp.png}
		\caption{\small{OU process. Confidence interval of samle standard deviation by PDE and CE methods. $S=10^5$. $B=10^4$}}
	\end{figure}
\end{frame}

\begin{frame}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=7cm, width=12cm]{DWbootstrp2.png}
		\caption{\small{OU process. Confidence interval of sample standard deviation by PDE and CE methods. $S=10^5$. $B=10^4$ (Another run)}}
	\end{figure}
\end{frame}


\begin{frame}
	\begin{figure}[h!]
		\hspace*{-1cm}
		\includegraphics[height=7cm, width=12cm]{VarRed3methods.png}
		\caption{\small{Variance reduction of PDE method over MC method for OU, DW ($\mu=1$) and DW ($\mu=-1$) examples.}}
	\end{figure}
\end{frame}
\end{document}

%
%\begin{frame}{Comparison of Cross-Entropy and PDE methods}
%   \large{ \textbf{Ornstein-Uhlenbeck process:}}
%   \begin{table}[h!]
%   	\begin{adjustbox}{width=\columnwidth,center}
%   	\begin{tabular}{|c|ccl|ccl|l|l|}
%   		\hline
%   		\multirow{2}{*}{$K$} & \multicolumn{3}{c|}{\textbf{Estimator} }            & \multicolumn{3}{c|}{\textbf{Relative stat. error}}   &\multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{CE,\tilde{\mu}}}$} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{PDE, \tilde{\mu}}}$} \\ \cline{2-7} 
%   		& \multicolumn{1}{c|}{$\hat{\alpha}_{MC}$} & \multicolumn{1}{c|}{$\hat{\alpha}_{CE}^{\tilde{\mu}}$} & \multicolumn{1}{c|}{$\hat{\alpha}_{PDE}^{\tilde{\mu}}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{MC}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{CE,\tilde{\mu}}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{PDE,\tilde{\mu}}$} & & \\ \hline
%   		3                  & \multicolumn{1}{c|}{0.0597}     &  \multicolumn{1}{c|}{0.0609} & \multicolumn{1}{c|}{0.0615}& \multicolumn{1}{c|}{2.51\%}     &  \multicolumn{1}{c|}{1.84\%} & \multicolumn{1}{c|}{1.37\%}  &\multicolumn{1}{c|}{1.87} & \multicolumn{1}{c|}{3.32} \\ \hline
%   		4                  & \multicolumn{1}{c|}{0.0035}     &  \multicolumn{1}{c|}{0.0033}  &  \multicolumn{1}{c|}{0.0032} &\multicolumn{1}{c|}{10.68\%}     &  \multicolumn{1}{c|}{2.90\% }&\multicolumn{1}{c|}{2.33\% } &\multicolumn{1}{c|}{13.88} &\multicolumn{1}{c|}{21.86}\\ \hline
%   		4.5                 & \multicolumn{1}{c|}{5.93e-04}     &  \multicolumn{1}{c|}{5.44e-04} &  \multicolumn{1}{c|}{5.12e-04}& \multicolumn{1}{c|}{26.50\%}     & \multicolumn{1}{c|}{3.39\%} & \multicolumn{1}{c|}{2.81\%} &\multicolumn{1}{c|}{61.31} &\multicolumn{1}{c|}{94.69}\\ \hline
%   		5                  & \multicolumn{1}{c|}{9.04e-05}      &  \multicolumn{1}{c|}{7.19e-05}  & \multicolumn{1}{c|}{7.29e-05} & \multicolumn{1}{c|}{73.29\%}     &  \multicolumn{1}{c|}{4.15\%} &  \multicolumn{1}{c|}{4.97\%} &\multicolumn{1}{c|}{537.1} &\multicolumn{1}{c|}{213.61}  \\ \hline
%   		5.5                  & \multicolumn{1}{c|}{4.88e-07}      &  \multicolumn{1}{c|}{7.41e-06}  &\multicolumn{1}{c|}{7.22e-06}  &\multicolumn{1}{c|}{235.52\%}     &  \multicolumn{1}{c|}{2.95\%} & \multicolumn{1}{c|}{5.02\%}&\multicolumn{1}{c|}{5976.7}  &\multicolumn{1}{c|}{2103.77}  \\ \hline
%   		6                 & \multicolumn{1}{c|}{0}      &  \multicolumn{1}{c|}{6.85e-07}  &\multicolumn{1}{c|}{6.42e-07}  & \multicolumn{1}{c|}{748.81\%}     & \multicolumn{1}{c|}{3.95\%} & \multicolumn{1}{c|}{10.46\%} &\multicolumn{1}{c|}{40700} & \multicolumn{1}{c|}{5471.4} \\ \hline
%   	\end{tabular}
%    \end{adjustbox}
%   	\caption{  Model parameters: $\theta=1, a=1, b=1$. Simulation parameters: $T=1$, $\Delta t= 0.001$, $x_0 \sim N(1,1)$, $M=10^5$. In $\hat{\alpha}_{CE}^{\tilde{\mu}}$: $\tilde{\rho}_{x_0} \sim N(\tilde{\mu}^{CE},1 )$ and in $\hat{\alpha}_{PDE}^{\tilde{\mu}}: \tilde{\rho}_{x_0}\sim N(\tilde{\mu}^{PDE},1 )$ .}
%   	\label{table:OUerror}
%   \end{table}
%\end{frame}
%
%\begin{frame}{Comparison of Cross-Entropy and PDE methods}
%	   \large{ \textbf{Double Well process:}}
%\begin{table}[h!]
%	\begin{adjustbox}{width=\columnwidth,center}
%	\begin{tabular}{|c|ccl|ccl|l|l|}
%		\hline
%		\multirow{2}{*}{$K$} & \multicolumn{3}{c|}{\textbf{Estimator} }            & \multicolumn{3}{c|}{\textbf{Relative stat. error}}   &\multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{CE,\tilde{\mu}}}$} & \multirow{2}{*}{$\frac{\mathbb{V}_{MC}}{\mathbb{V}_{CE, \tilde{\mu}, \tilde{\sigma}}}$} \\ \cline{2-7} 
%		& \multicolumn{1}{c|}{$\hat{\alpha}_{MC}$} & \multicolumn{1}{c|}{$\hat{\alpha}_{CE}^{\tilde{\mu}}$} & \multicolumn{1}{c|}{$\hat{\alpha}_{PDE}^{\tilde{\mu},\tilde{\sigma}}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{MC}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{CE,\tilde{\mu}}$} & \multicolumn{1}{c|}{$\epsilon_{st}^{CE,\tilde{\mu},\tilde{\sigma}}$} & & \\ \hline
%		2                 & \multicolumn{1}{c|}{0.0218}     &  \multicolumn{1}{c|}{0.0217} & \multicolumn{1}{c|}{0.0216}& \multicolumn{1}{c|}{4.20\%}     &  \multicolumn{1}{c|}{2.93\%} & \multicolumn{1}{c|}{2.27\%}  &\multicolumn{1}{c|}{2.06} & \multicolumn{1}{c|}{3.35} \\ \hline
%		3                 & \multicolumn{1}{c|}{0.0012}     &  \multicolumn{1}{c|}{9.18e-04}  &  \multicolumn{1}{c|}{8.53e-04} &\multicolumn{1}{c|}{17.88\%}     &  \multicolumn{1}{c|}{9.58\% }&\multicolumn{1}{c|}{6.91\% } &\multicolumn{1}{c|}{4.56} &\multicolumn{1}{c|}{9.42}\\ \hline
%		3.5               & \multicolumn{1}{c|}{2.41e-04}     &  \multicolumn{1}{c|}{1.29e-04} &  \multicolumn{1}{c|}{1.34e-04}& \multicolumn{1}{c|}{39.95\%}     & \multicolumn{1}{c|}{17.38\%} & \multicolumn{1}{c|}{15.45\%} &\multicolumn{1}{c|}{9.94} &\multicolumn{1}{c|}{11.98}\\ \hline
%		4                  & \multicolumn{1}{c|}{2.0e-05}      &  \multicolumn{1}{c|}{1.19e-05}  & \multicolumn{1}{c|}{1.34e-05} & \multicolumn{1}{c|}{138.59\%}     &  \multicolumn{1}{c|}{31.21\%} &  \multicolumn{1}{c|}{24.15\%} &\multicolumn{1}{c|}{33.02} &\multicolumn{1}{c|}{49.22}  \\ \hline
%		4.5               & \multicolumn{1}{c|}{0}      &  \multicolumn{1}{c|}{1.10e-06}  &\multicolumn{1}{c|}{1.30e-06}  &\multicolumn{1}{c|}{589.63\%}     &  \multicolumn{1}{c|}{38.71\%} & \multicolumn{1}{c|}{74.36\%}&\multicolumn{1}{c|}{231.98}  &\multicolumn{1}{c|}{53.31}  \\ \hline
%		5                & \multicolumn{1}{c|}{0}      &  \multicolumn{1}{c|}{5.58e-08}  &\multicolumn{1}{c|}{9.02e-09}  & \multicolumn{1}{c|}{2623.99\%}     & \multicolumn{1}{c|}{57.83\%} & \multicolumn{1}{c|}{81.02\%} &\multicolumn{1}{c|}{2058.99} & \multicolumn{1}{c|}{6489.87} \\ \hline
%	\end{tabular}
%    \end{adjustbox}
%	\caption{ Model parameters: $\theta=1, a=1, b=1$. Simulation parameters: $T=1$, $\Delta t= 0.001$, $x_0 \sim N(-1,1)$, $M=10^5$. In $\hat{\alpha}_{CE}^{\tilde{\mu}}$: $\tilde{\rho}_{x_0} \sim N(\tilde{\mu}^{CE},1 )$ and in $\hat{\alpha}_{PDE}^{\tilde{\mu},\tilde{\sigma}}: \tilde{\rho}_{x_0}\sim N(\tilde{\mu}^{PDE},\tilde{\sigma}^{PDE} )$ .}
%	\label{table:DWerror}
%\end{table}
%\end{frame}
%
%\begin{frame}
%
%	\begin{figure}[h!]
%			\hspace*{-1cm}
%\includegraphics[height=8cm, width=12cm]{DWK5rho.png}
%\end{figure}
%\end{frame}