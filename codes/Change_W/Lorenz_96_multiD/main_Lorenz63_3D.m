clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for Lorenz 96 problem to test CE method and PDE method
%%% in performing IS technique: 
%-- dv_k = (v_{k-1}(v_{k+1}-v_{k-2})-v_k+F)dt+sigma_k dW_k ---------------%
%-- v_0=v_d, v_{d+1}=v_1 , v_{-1}= v_{d-1}, k=1,...,d.     ---------------%
%-------------------------------------------------------------------------%
%%% in computing QoI: P(max_{0<=t<=T} P1*v(t) >K)

%Model parameters
d=3;         %problem dimension
sigma=3;     %constant diffusion parameter
r=3;
q=1;
s=26;
% sigma=5;
% r=10;
% q=8/3;
% s=28;
R=s+r;
I=eye(d);
%a = @(u) [r*(u(2,:,:)-u(1,:,:)); -r*u(1,:,:)-u(2,:,:)-u(1,:,:).*u(3,:,:); u(1,:,:).*u(2,:,:)-s*u(3,:,:)-s*(q+r)]; %Lorenz 96 drift
a = @(u) [r*(u(2,:,:)-u(1,:,:)); s*u(1,:,:)-u(2,:,:)-u(1,:,:).*u(3,:,:); u(1,:,:).*u(2,:,:)-q*u(3,:,:)]; %Lorenz 96 drift
b = [sigma; 0; 0];   

%Simulation parameters
T=10;         %final time (simulation length)
M=10^5       %Monte Carlo sample size
K=3.5        %the threshold
N=1000;      %number of timesteps in SDE
dt=T/N;      %discretization step in SDE

%Initial original density parameters 
%mu=zeros(d,1);
mu=[5 5 25]';
%A=randn(d);
Sigma=0.5*I;
%Sigma=cov(A);
% rho=0.3;
% Sigma = 0.1*(1-rho)*I+0.1*rho*ones(d,d);
%Define the Projector which component to track
P1   = I(1,:); 
%P1   = I(end,:);

%Generate samples of Lorenz 96 dynamics to use for both CE and L2 regression
u=zeros(d,N,M);
Mbar=zeros(1,M);
h=zeros(1,M);
g=zeros(M,N+1);
for m=1:M
    u(:,1,m)=mu+chol(Sigma)'*randn(d,1); 
    %u(:,1,m)=[5 5 25]+0.5*[1 1 1];
    g(1,1)=u(1,1,m)^2/(R^2*q/r)+u(2,1,m)^2/(q*R^2)+(u(3,1,m)-R)^2/R^2;
    for n=1:N
%         u(1,n+1,m)=u(1,n,m)+r*(u(2,n,m)-u(1,n,m))*dt+sigma*sqrt(dt)*randn;
%         u(2,n+1,m)=u(2,n,m)+(q*u(1,n,m)-u(2,n,m)-u(1,n,m)*u(3,n,m))*dt;
%         u(3,n+1,m)=u(3,n,m)+(u(1,n,m)*u(2,n,m)-s*u(3,n,m))*dt;
          u(:,n+1,m)=u(:,n,m)+lorenz63(u(:,n,m),r,q,s)*dt+b.*sqrt(dt).*randn(3,1);
          g(m,n+1)=u(1,n+1,m)^2/(R^2*q/r)+u(2,n+1,m)^2/(q*R^2)+(u(3,n+1,m)-R)^2/R^2;
%           if g(m,n+1)>1
%               pause()
%           end
    end
    Mbar(m)=max(P1*u(:,:,m));
    %Mbar(m)=max(g(m,:));
    %h(m)=(max(g(m,:))>1);
%     if Mbar(m)>1
%          Mbar(m)
%         pause()
%     end
end
alpha_hat=mean(h)
        figure(1)
        plot3(u(1,:,1),u(2,:,1),u(3,:,1),'k')
        hold on
        [X,Y,Z]=ellipsoid(0,0,R,R*sqrt(q/r),sqrt(q)*R,R);
        surf(X,Y,Z,'facecolor','white','EdgeColor','black','Linestyle', ':','FaceAlpha',.33) 
        figure(2)
        plot(0:dt:T, u(1,:,1))
        
        K=R*sqrt(q/r);
%% L2 regression
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We approximate only the drift, the diffusion is constant

%Define the Polynomial space
w = 2;                                                         %Polynomial degree
p = poly_space(w,'TD');                                        %Polynomial space
p_dim = size(p, 1);                                            %Cardinality of polynomial space

t  = linspace(0,T,N+1)';                                       %Generating t_0, t_1, ..., t_{N-1}
Tt = reshape(repmat(t,1,M)', (N+1)*M,1);                       %Replicating M times each t_n and saving as a column vector [t_0 t_0 ... t_0, t_1, ..., t_1,..., t_{N-1}, ..., t_{N_1}]'
Xx = reshape(squeeze(pagemtimes(P1,u))', (N+1)*M,1);           %Projected Samples saved as a column vector [X_0^(1),..., X_0^(M), X_1^(1),..,X_1^(M), ..., X_{N-1}^(1),...,X_{N-1}^(M)]
tX = [Tt Xx];                                                  %Two column vectors of size N*M for [t_n, X_{t_n}]
f  = reshape(squeeze(pagemtimes(P1,a(u)))',(N+1)*M,1);         %Given f function, we are going to solve

D     = x2fx(tX, p);                                           %This function helps to create Psi matrix each column is non-orth. basis function psi_p for different given p
[Q,R] = modified_GS(D);                                        %Apply modified Gram-Schmidt process to get QR decomposition
a_p   = Q'*f;                                                  %Solve Normal equations based on orthonormalised basis

psy     = @(t,s) x2fx([t s], p);                               %Non-orth. basis function
psy_bar = @(t,s) x2fx([t s], p)/R;                             %Orth. basis function
a_bar   = @(t,s) psy_bar(t,s)*a_p;                             %Approximation to the drift function a(x)
b_bar   = @(s)   sqrt(P1*(b*b')*P1')*ones(size(s));                %Approximation to the diffusion function b(x)
%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sL = -3;                                                       %Lower end of discretization interval for ptojected process s=P_1x
sU = K;                                                        %Upper end of discretization interval for ptojected process s=P_1x
Ns = 10000;                                                    %Number of discretization intervals in space s
ds = (sU-sL)/Ns;                                               %Spatial step size in s
sPDE  = linspace(sL,sU,Ns+1)';                                 %Linspace for s
Ntpde=2500;                                                    %Number of timesteps


%PDE solution
ileqK  = find(sPDE<=K);                                        %Indexes of sPDE which is less or equal than K
igreK  = find(sPDE>K);                                         %Indexes of sPDE which is greater than K
NsleqK = length(ileqK)-1;                                      %Number of spatial steps in PDE

%%
[ss, uPDE] = KBEsolverL2(sPDE(ileqK),T,a_bar,b_bar,Ntpde,NsleqK); %PDE solver for the inhomogeneous underlying dynamics with first few time steps Backward Euler and the rest is Crank-Nickolson.

%Plot PDE solution at time 0
figure(1)
plot(ss, uPDE, 'Linewidth',2)
xlabel('s', 'fontsize', 14)
ylabel('u(s,0)','fontsize', 14)

% Initial density is set to d-variate Gaussian, but we consider the
% marginal denisty in v1 since we consider the projection P1=[1 0...0] and solved the KBE with respect to v1

rho_Gaussian = @(x, m, sig) exp(-(x-m).^2/(2*sig^2))/(sqrt(2*pi*sig^2)); 
rhoMarg_v1 = rho_Gaussian(ss, P1*mu, sqrt(P1*Sigma*P1'));
rhoTemp = [rhoMarg_v1.*sqrt(abs(uPDE));rho_Gaussian(sPDE(igreK), P1*mu, sqrt(P1*Sigma*P1'))];
NC = trapezoidal(rhoTemp, ds); %normalising constant
rhoTemp=rhoTemp./NC;           %optimal IS density

%plot(ss, rhoMarg_v1.*sqrt(abs(uPDE)))
%Note that the optimal denisty rhoJointTilde_0(v_1,v_2,...,v_d) \approx rho_0(v_1,v_2,...,v_d)sqrt(u_PDE(v_1,0))
% where rho_0(v_1,v_2,...,v_d) = rho_0(v_2,...,v_d|v_1)rho_0(v_1) and rho_0(v_1)*sqrt(u_PDE(v_1,0)) is fitted to 1D Gaussian
v1=[ss;sPDE(igreK)];
dv1 = v1(2)-v1(1);
mFit   = trapezoidal(v1.*rhoTemp, dv1)
sigFit = sqrt(trapezoidal(v1.^2.*rhoTemp, dv1)-mFit^2)
rhoTildeFit_0 = rho_Gaussian(v1, mFit, sigFit);
%%
for n=1:2
M=10^5;
%Conditional Gaussian rho_0(v_2,...,v_d|v_1) 
v1Smpl=mFit+sigFit*randn(1,M);
% mCond=mu(2:end)+corr*(v1Smpl-P1*mu)/sqrt(P1*Sigma*P1');
% sigCond=(1-corr^2)*Sigma(2:end,2:end);
mCond = mu(2:end)+ Sigma(2:d,1)/Sigma(1,1)*(v1Smpl-mu(1));
sigCond = Sigma(2:d, 2:d) - Sigma(2:d,1)/Sigma(1,1)*Sigma(1,2:d);

%P1=[0 .. 1]case
% mCond = mu(1:end-1)+ Sigma(1:end-1,end)/Sigma(end,end)*(v1Smpl-mu(end));
% sigCond = Sigma(1:end-1, 1:end-1) - Sigma(1:end-1,end)/Sigma(end,end)*Sigma(end,1:end-1);
%% Computing the QoI with parameters obtained by PDE approach
h=zeros(1,M);
u=zeros(d,N);
u0=[v1Smpl; mCond+chol(sigCond)'*randn(d-1,M)];

%P1=[0 .. 1]case
%u0=[mCond+chol(sigCond)'*randn(d-1,M);v1Smpl];
parfor m=1:M
    u=u0(:,m);   
    for k=1:N
%         u(1,k+1)=u(1,k)+r*(u(2,k)-u(1,k))*dt+sigma*sqrt(dt)*randn;
%         u(2,k+1)=u(2,k)+(q*u(1,k)-u(2,k)-u(1,k)*u(3,k))*dt;
%         u(3,k+1)=u(3,k)+(u(1,k)*u(2,k)-s*u(3,k));
          u(:,k+1)=u(:,k)+lorenz63(u(:,k),r,q,s)*dt+b.*sqrt(dt).*randn(3,1);
    end
    h(m)=(max(P1*u)>=K);
    
end
Weight = rho_Gaussian(P1*u0,P1*mu,sqrt(P1*Sigma*P1'))./rho_Gaussian(P1*u0, mFit, sigFit);
alpha_hat_L2=mean(h.*Weight)
Vaprx_L2=var(h.*Weight)
RelError_L2=1.96/sqrt(alpha_hat_L2*M)
RelErrorIS_L2=1.96*sqrt(Vaprx_L2)/(alpha_hat_L2*sqrt(M))
varRatio_L2=(RelError_L2/RelErrorIS_L2)^2

%% Bootstrapping for PDE
if isempty(gcp)
    parpool;
end
opt = statset('UseParallel',true);
B=10^4;
conf=95;% in percent
stats_PDE = bootstrp(B, @(x) [mean(x) std(x)], h.*Weight, 'Options', opt);
SmplStd_PDE=mean(stats_PDE(:,2))
CId_pde = prctile(stats_PDE(:,2), (100-conf)/2)
CIu_pde=  prctile(stats_PDE(:,2), (100-(100-conf)/2))
confIntlength=CIu_pde-CId_pde
end
%pause()
% %% MULTILEVEL CROSS-ENTROPY METHOD
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %  We consider only mean shifting but fixing the covariance
% beta=0.01;     %CE method parameter
% M=10^5;
% % Multilevel procedure to find the optimal mu_tilde
% Weight = ones(1,M);
% sMbar=sort(Mbar);
% K_ell=sMbar(1, ceil((1-beta)*M));
% K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
% h=(Mbar>=K_ell);
% Sigma_tilde=Sigma;
% ell=1;
% u0=mu+chol(Sigma)'*randn(d,M);
% 
% while K_ell<K
%     mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight);
%     
%     ell=ell+1;
%     
%     for m=1:M
%         u(:,1,m)=mu_tilde+chol(Sigma_tilde)'*randn(d,1);
%         u0(:,m)=u(:,1,m);
%         for n=2:N+1
% %            u(1,n+1,m)=u(1,n,m)+r*(u(2,n,m)-u(1,n,m))*dt+sigma*sqrt(dt)*randn;
% %            u(2,n+1,m)=u(2,n,m)+(q*u(1,n,m)-u(2,n,m)-u(1,n,m)*u(3,n,m))*dt;
% %            u(3,n+1,m)=u(3,n,m)+(u(1,n,m)*u(2,n,m)-s*u(3,n,m)); 
%              u(:,n,m)=u(:,n-1,m)+lorenz63(u(:,n-1,m),r,q,s)*dt+b.*sqrt(dt).*randn(3,1);
%         end
%         Mbar(m)=max(P1*u(:,:,m));
%     end
% 
%     sMbar=sort(Mbar);
%     K_ell=sMbar(1, ceil((1-beta)*M));
%     K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
%     h=(Mbar>=K_ell);
%   
%     Weight=mvnpdf(u0', mu', Sigma)'./mvnpdf(u0',mu_tilde',Sigma_tilde)';
% end
% 
% if K_ell==K
%     mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight)
% end
% % Computing the QoI with parameters obtained by multilevel CE approach
% %mu_tilde=mu       %Crude MC setting to compare
% for n=1:1
% M=10^5;
% h=zeros(1,M);
% u0=mu_tilde+chol(Sigma_tilde)'*randn(d,M);
% 
% parfor m=1:M
%     u=u0(:,m);  
%     for k=1:N
% %         u(1,k+1)=u(1,k)+r*(u(2,k)-u(1,k))*dt+sigma*sqrt(dt)*randn;
% %         u(2,k+1)=u(2,k)+(q*u(1,k)-u(2,k)-u(1,k)*u(3,k))*dt;
% %         u(3,k+1)=u(3,k)+(u(1,k)*u(2,k)-s*u(3,k));  
%         u(:,k+1)=u(:,k)+lorenz63(u(:,k),r,q,s)*dt+b.*sqrt(dt).*randn(3,1);
%     end    
%     h(m)=(max(P1*u)>=K);
% end
% Weight=mvnpdf(u0', mu', Sigma)'./mvnpdf(u0',mu_tilde',Sigma_tilde)';
% alpha_hat_CE=mean(h.*Weight)
% Vaprx_CE=var(h.*Weight)
% RelError_CE=1.96/sqrt(alpha_hat_CE*M)
% RelErrorIS_CE=1.96*sqrt(Vaprx_CE)/(alpha_hat_CE*sqrt(M))
% varRatio_CE=(RelError_CE/RelErrorIS_CE)^2
% 
% %% Bootstrapping for CE
% if isempty(gcp)
%     parpool;
% end
% opt = statset('UseParallel',true);
% B=10^4;
% conf=95;% in percent
% stats_CE = bootstrp(B, @(x) [mean(x) std(x)], h.*Weight, 'Options', opt);
% SmplStd_CE=mean(stats_CE(:,2))
% CId_ce = prctile(stats_CE(:,2), (100-conf)/2)
% CIu_ce=  prctile(stats_CE(:,2), (100-(100-conf)/2))
% confIntlength=CIu_ce-CId_ce
% end