function f=lorenz63(y,r,q,s)
f(1,1)=r*(y(2)-y(1));
f(2,1)=s*y(1)-y(2)-y(1)*y(3);
f(3,1)=y(1)*y(2)-q*y(3);
end

% function f=lorenz63(y,r,q,s)
% f(1,1)=r*(y(2)-y(1));
% f(2,1)=r*y(1)-y(2)-y(1)*y(3);
% f(3,1)=y(1)*y(2)-s*y(3)-s*(r+q);
% end