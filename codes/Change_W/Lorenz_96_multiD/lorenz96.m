function f=lorenz96(v,F,P1)
f=[v(end); v(1:end-1)].*([v(2:end); v(1)]-[v(end-1:end);v(1:end-2)])-v+(F*v.^0);
end