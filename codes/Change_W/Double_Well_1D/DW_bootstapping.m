clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for BOOTSTRAPPING of Double Well 1D problem to check 
%%% the robustness of CE method and PDE method

%Problem parameters
sigma=0.5;
b=@(x) sigma;                           %the diffusion parameter
a=@(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x); %DW drift parameter
T=1;                                    %simulation length
Krng=[1.5 2 2.5 3];                     %the threshold DW

Kstep=1000;                       %Numerical timestep number
dt = T/Kstep;                     %Timestep

%Initial density parameters in CE method
mu_0 = -1; 
sigma_0 = 1;
mu_tilde_CEv=[1.396609817574580 2.016952498922572 2.579580202262708 3.089672024089878];%DW
sigma_tilde_CEv=1*ones(size(Krng));%DW
mu_tilde_PDEv=[0.897096010848667 1.418597423720042 2.010397778073012 2.607701018240120];%DW after fixing the bug
sigma_tilde_PDEv=[0.640342812920618 0.669556789182874 0.649080752660137 0.596037400168803];%DW after fixing the bug
C = {'r','m', 'g', 'k', 'y'}; % Cell array of colros.
Cc={'g','b'};
S=10^5;
conf=95;% in percent
B=10^4;
for n=1:2
    
    
    for i=1:length(Krng)
        K=Krng(i);
        h_PDE=zeros(1,S);
        h_CE=zeros(1,S);
        mu_tilde_PDE=mu_tilde_PDEv(i);
        sigma_tilde_PDE = sigma_tilde_PDEv(i);
        mu_tilde_CE=mu_tilde_CEv(i);
        sigma_tilde_CE=sigma_tilde_CEv(i);
        u0_PDE=mu_tilde_PDE+sigma_tilde_PDE*randn(1,S);
        u0_CE=mu_tilde_CE+sigma_tilde_CE*randn(1,S);
        u_pde=zeros(1,Kstep);
        u_ce=u_pde;
        
        for s=1:S
            u_pde(1)=u0_PDE(s);
            u_ce(1)=u0_CE(s);
            for k=1:Kstep
                dW_pde = sqrt(dt)*randn;
                dW_ce  = sqrt(dt)*randn;
                u_pde(k+1)= u_pde(k)+a(u_pde(k))*dt+b(u_pde(k))*dW_pde; %Forward Euler
                u_ce(k+1)= u_ce(k)+a(u_ce(k))*dt+b(u_ce(k))*dW_ce; %Forward Euler
            end
            h_PDE(s)=(max(u_pde)>K);
            h_CE(s)=(max(u_ce)>K);
        end
        Weight_PDE=(sigma_tilde_PDE/sigma_0)*exp(-(u0_PDE-mu_0).^2./(2*sigma_0^2)+(u0_PDE-mu_tilde_PDE).^2./(2*sigma_tilde_PDE^2));
        Weight_CE=(sigma_tilde_CE/sigma_0)*exp(-(u0_CE-mu_0).^2./(2*sigma_0^2)+(u0_CE-mu_tilde_CE).^2./(2*sigma_tilde_CE^2));
        
        q_PDE=h_PDE.*Weight_PDE;
        q_CE=h_CE.*Weight_CE;
        
        if isempty(gcp)
            parpool;
        end
        opt = statset('UseParallel',true);
             
        stats_PDE = bootstrp(B, @(x) [mean(x) std(x)], q_PDE, 'Options', opt);
        stats_CE = bootstrp(B, @(x) [mean(x) std(x)], q_CE, 'Options', opt);

        SmplStd_PDE(i)=mean(stats_PDE(:,2));
        SmplStd_CE(i)=mean(stats_CE(:,2));
        
        CId_pde(i) = prctile(stats_PDE(:,2), (100-conf)/2);
        CIu_pde(i)=  prctile(stats_PDE(:,2), (100-(100-conf)/2));
        
        CId_ce(i) = prctile(stats_CE(:,2), (100-conf)/2);
        CIu_ce(i)=  prctile(stats_CE(:,2), (100-(100-conf)/2));
    end
    figure(1)
    plot_ci(Krng',[SmplStd_PDE; CId_pde; CIu_pde],'PatchColor', C{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', C{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', C{n}, 'YScale', 'log');
    hold on
    plot_ci(Krng',[SmplStd_CE; CId_ce; CIu_ce], 'PatchColor', Cc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Cc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Cc{n}, 'YScale', 'log');
    grid on
    hold on
end
    xlabel('K')
    ylabel('sample std')
    legend('95% CI of PDE 1st run','PDE mean 1st run','','','95% CI of CE 1st run','CE mean 1st run','','','95% CI of PDE 2nd run', 'PDE mean 2nd run','','', '95% CI of CE 2nd run', 'CE mean2nd run')

