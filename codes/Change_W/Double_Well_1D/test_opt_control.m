%K = 3; xmin = 0; xmax = K+1; dt = 0.01; make_plots = true;
K = 0.5; xmin = -3; xmax = K+1; dt = 0.01; make_plots = true; Nx=1001;

a = @(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x);
%a = @(x) 1-x;
%a = @(x) x.^2;
%a = @(x) ones(size(x));
%b = @(x) ones(size(x))-0.25*sin(4*pi*x); % Assumed to be of constant sign, bounded away from 0
%b = @(x) ones(size(x)); % Assumed to be of constant sign, bounded away from 0
b = @(x) 0.5*ones(size(x)); % Assumed to be of constant sign, bounded away from 0

[xv,zetav,u_tau] = tabulate_IC(a,b,dt,K,xmin,xmax,make_plots,Nx);

% Illustrate the dt-dependence of the optimal control and the KBE solution
% at time T-dt.
dtv = 0.4*2.^(0:-1:-5);
dt_strings = cell(1,length(dtv));

fig_c = figure; hold on;
xlabel('$x$','interpreter','latex','fontsize',14)
ylabel('$\Delta t\zeta(T-\Delta t,x)$','interpreter','latex','fontsize',14)
title('$\Delta t$-scaled optimal control $\zeta(T-\Delta t,x)$','interpreter','latex','fontsize',14)

fig_u = figure; hold on;
xlabel('$x$','interpreter','latex','fontsize',14)
ylabel('$u_\tau(T-\Delta t,x)$','interpreter','latex','fontsize',14)
title('KBE solution at $T-\Delta t$ for varying $\Delta t$','interpreter','latex','fontsize',14)
make_plots = false;
for k=1:length(dtv)
  dt = dtv(k);
  [xv,zetav,u_tau] = tabulate_IC(a,b,dt,K,xmin,xmax,make_plots);
  figure(fig_c)
  plot(xv,dt*zetav,'-','linewidth',2)
  figure(fig_u)
  plot(xv,u_tau,'-','linewidth',2)
  dt_strings{k} = ['$\Delta t=',num2str(dt),'$'];
end
figure(fig_c)
lh=legend(dt_strings);
set(lh,'interpreter','latex','fontsize',14,'location','NorthEast')
figure(fig_u)
lh=legend(dt_strings);
set(lh,'interpreter','latex','fontsize',14,'location','SouthEast')