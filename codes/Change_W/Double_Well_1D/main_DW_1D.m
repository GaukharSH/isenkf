clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for 1D problem to test PDE method in
%%% performing IS technique with respect to dW
%%% 
%-----x-- dX_t= a(X_t)dt+b(X_t) dW_t -----------------------------------%
%----------------------------------------------------------------------%
%%% in computing QoI: P(max_{0<=t<=T} X_t >K)
%seed=1; rng(seed); %choose random  number seed 
%Problem parameters
sigma=0.5;

% a=@(t,x) 1-x+0*t;                             %OU drift
% aa=@(x) 1-x;
a=@(t,x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x)+0*t;   %DW drift
aa=@(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x);        %DW drift
% a=@(t,x) x.^2+0*t;  
% aa=@(x) x.^2;        
b=@(x) sigma+0*x;                                 %DW diffusion
T=1;                                              %simulation length
K=0                                               %the threshold
Kstep=100;                                        %Numerical timestep number in SDE
dt = T/Kstep;                                     %Timestep in SDE

%Original density parameters
mu_0  =  -1;   %mean
sigma_0 = 0.2   %std

%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = -2.5                % Lower end of discretization interval
xU = K;                % Upper end of discretization interval
dx = 0.005;             %HJB
dtPde = dx^2;
% dx = 0.012;           %KBE
% dtPde = 0.0010; 
Nx = round((xU-xL)/dx);
Nt = round(T/dtPde);
x  = linspace(xL,xU,Nx+1)'; 

[~,~,vErik] = tabulate_IC(aa,b,dt,K,xL,xU,false,Nx+1);
semilogy(x,vErik', 'Linewidth',3)
%% PDE solution for all time
[tW, xW, v] = KBEsolverForAllt(x,T,a,b,Nt,Nx,K,vErik');
%%
%[tW, xW, u] = HJBsolverForAllt_CN(x,T,a,b,Nt,Nx,K,vErik');
%%
%Plot the PDE solution at time 0
% figure(1)
% semilogy(xW, v(:,end), 'r-',xW, exp(-u(:,end)), 'b-', 'Linewidth',3)
% xlabel('x', 'fontsize', 14)
% ylabel('v(x,T)','fontsize', 14)
% legend('KBE', 'HJB: -log0=10')
% title('PDE: dx=0.01, dt=dx^2; Ind. function')
%%
dxW=xW(2)-xW(1);
%v=[0*v(1,:); v]; %Dirichlet
%v=[2*v(1,:)-v(2,:); v];%Linear
%v=[v(1,:); v]; %Neumann
%v=[v(2,:); v]; %Exponen.


% ksiGriddedHJB = zeros(length(xW),length(tW));
% ksiGriddedHJB(2:end-1,:) = -sigma*(u(3:end,:)-u(1:end-2,:))./(2*dxW);
% ksiGriddedHJB(1,:) = -sigma*(u(2,:)-u(1,:))./dxW;
% ksiGriddedHJB(end,:) = -sigma*(u(end,:)-u(end-1,:))./dxW;

ksiGridded = zeros(length(xW),length(tW));
ksiGridded(2:end-1,:) = sigma*(log(v(3:end,:))-log(v(1:end-2,:)))./(2*dxW);
ksiGridded(1,:) = sigma*(log(v(2,:))-log(v(1,:)))./dxW;
ksiGridded(end,:) = sigma*(log(v(end,:))-log(v(end-1,:)))./dxW;

% z=zeros(length(xW),length(tW));
% z(2:end-1,:)=(v(3:end,:)-v(1:end-2,:))./v(1:end-2,:);
% z(1,:) = (v(2,:)-v(1,:))./(v(1,:));
% z(end,:) = (v(end,:)-v(end-1,:))./(v(end-1,:));
% ksiGridded = zeros(length(xW),length(tW));
% ksiGridded(2:end-1,:) = sigma*(z(2:end-1,:))./(2*dxW);
% ksiGridded(1,:) = sigma*(z(1,:))./(dxW);
% ksiGridded(end,:) = sigma*(z(end,:))./(dxW);
% 
% ksiGridded = zeros(length(xW),length(tW));
% ksiGridded(2:end-1,:) = sigma*(log1p(z(2:end-1,:)))./(2*dxW);
% ksiGridded(1,:) = sigma*(log1p(z(1,:)))./(dxW);
% ksiGridded(end,:) = sigma*(log1p(z(end,:)))./(dxW);
% 
% 
% ksiGriddedT = zeros(length(xW),length(tW));
% ksiGriddedT(2:end-1,:) = (v(3:end,:)-v(1:end-2,:))./(v(2:end-1,:));
% ksiGriddedT(1,:) = (v(2,:)-v(1,:))./(v(1,:));
% ksiGriddedT(end,:) = (v(end,:)-v(end-1,:))./(v(end,:));
% 
% ksi1 =flip(ksiGriddedHJB,2);
%
% figure(10)
% for j=1:4000:Nt
% plot(xW, ksiGridded(:,j), 'LineWidth', 2)
% hold on
% end
% hold off
% xlabel('x')
% ylabel('ksi')
% title('KBE: dx=0.01, dt=dx^2; Ind.function')
% figure()
% for j=1:55:Nt
% plot(xW, ksiGriddedHJB(:,j), 'LineWidth', 2)
% hold on
% end
% hold off
% xlabel('x')
% ylabel('ksi')
% title('HJB: dx=0.01, dt=dx^2; Ind.function: -log0=10')
%%
[Xw,Tw] = ndgrid(xW,tW);
F=griddedInterpolant(Xw(100:end,1:end),flip(Tw(100:end,1:end),2), flip(ksiGridded(100:end,1:end),2),'linear', 'linear');
%F=griddedInterpolant(Xw(100:end,1:end),flip(Tw(100:end,1:end),2), flip(ksiGridded(100:end,1:end),2),'linear', 'linear');
ksi = @(x,t) F(x,t).*(x<K)+zeros(size(t)).*(x>=K); 
% figure(2)
% % h=gca;
% p0=surf(Xw, Tw, v);
% set(p0,'LineStyle','none')
% figure(22)
% h=gca;
% p0=surf(Xw, Tw, exp(-u));
% set(p0,'LineStyle','none')
% %set(p0, 'zscale','log')
% figure(3)
% p1=surf(Xw(:,1:end), Tw(:,1:end), ksiGridded(:,1:end));
% %p1=surf(Xw(1:end,:), Tw(1:end,:), F(Xw(1:end,:),Tw(1:end,:)));
% set(p1,'LineStyle','none')
% figure(4)
% xF  = linspace(xL-2,K+2,Nx+1)'; 
% [XwF,TwF] = ndgrid(xF,tW);
% p2=surf(XwF, TwF, ksi(XwF,TwF));
% set(p2,'LineStyle','none')
%%
 %[Xw,Tw] = meshgrid(xx(2:end),tW);
 %ksi =@(x,t) interp2(Xw,Tw,ksiGridded',x,t,'linear', 0);
%  xq=x(ileqK);
%  ksi = @(x) interp1(xq(2:end),ksiGridded(:,end),x,'linear', 0);
%  figure(3)
%  plot(x,ksi(x),'-b')
%%
 %ksi = @(x,t) -10*exp(x); 

%% Computing the QoI with obtained mFit and sigFit parameters from PDE approach
S=10^5;

h=zeros(1,S);
hOrig=zeros(1,S);
u=zeros(1,Kstep+1);
mFit=mu_0; %Crude MC setting to compare
sigFit = sigma_0;
u0=mFit+sigFit*randn(1,S);
%fprintf(['1st run'])
t=linspace(0,T,Kstep+1);
meanL=0;
parfor s=1:S
        u=u0(s);
        uOrig=u0(s);
%     u=mFit;
%     uOrig=mFit;
   % L=1;
   optcontTemp=zeros(1,Kstep);
   dWTemp=zeros(1,Kstep);
   k=1;
    while k<=Kstep && u(k)<K
        dW = sqrt(dt)*randn;
        %u(k+1)= u(k)+(a(t(k),u(k))+b(u(k))*ksi(u(k),t(k)))*dt+b(u(k))*dW;
        %uOrig(k+1)=uOrig(k)+aa(uOrig(k))*dt+sigma*dW;
        optcontTemp(k)=ksi(u(k),t(k));
%        optcontTemp(k)=interp1(xW,ksi1(:,k),u(k),'linear', 'extrap').*(u(k)<K)+0.*(u(k)>=K);
%         exponent=-0.5*dt*(ksi(u(k),t(k)))^2-ksi(u(k),t(k))*dW
        dWTemp(k) = dW;
        u(k+1)= u(k)+(aa(u(k))+sigma*optcontTemp(k))*dt+sigma*dW;
        %u(k+1)= u(k)+(aa(u(k))+sigma*ksi(u(k),t(k)))*dt+sigma*dW; %Forward Euler
        %L=L*exp(-0.5*dt*(ksi(u(k),t(k)))^2-ksi(u(k),t(k))*dW);
%         if u(k)>=K
%          u(k)
%          pause();
%         end
        k=k+1;
    end
    for n=1:Kstep
        dW = sqrt(dt)*randn;
        uOrig(n+1)=uOrig(n)+aa(uOrig(n))*dt+sigma*dW;
    end 
    L=exp(-0.5*dt*norm(optcontTemp)^2-dot(optcontTemp, dWTemp));
    meanL=meanL+L;
    h(s)=(max(u)>=K)*L;
    hOrig(s)=(max(uOrig)>=K);
end
meanL=meanL/S
%Weight_PDE=(sigFit/sigma_0)*exp(-(u0-mu_0).^2./(2*sigma_0^2)+(u0-mFit).^2./(2*sigFit^2));
alpha_hat_PDE=mean(h)
alpha_hat_PDE_orig=mean(hOrig)
Vaprx_PDE=var(h)
Vaprx_PDE_orig=var(hOrig)
RelError_PDE=1.96*sqrt(1-alpha_hat_PDE)/sqrt(alpha_hat_PDE*S)
RelErrorIS_PDE=1.96*sqrt(Vaprx_PDE)/(alpha_hat_PDE*sqrt(S))
varRatio_PDE=(RelError_PDE/RelErrorIS_PDE)^2
varRatio_PDE1=Vaprx_PDE_orig/Vaprx_PDE
%%
tt=[T-dtPde 0.9 0.8 0.5 0];
C = {'r','m', 'g', 'k', 'y'}; 
for i=1:length(tt)
    figure(9)
    tfix=find(tW==tt(i));
    %semilogy(xW, exp(-u(:,tfix)), 'LineWidth', 2)
    plot(xW, v(:,tfix), 'LineWidth', 2)
    hold on
end
grid on
legend('t=T-dt', 't=0.9T', 't=0.8T', 't=0.5T', 't=0')
xlabel('x')
ylabel('u(t,x)')
%title('HJB: dx= 0.01, dt=dx^2; Ind.function: -log0=1')
%%
ksiInterp=ksi(Xw,Tw);
for i=1:length(tt)
    figure(11)
    tfix=find(tW==tt(i));
    plot(xW, ksiInterp(:,tfix), 'LineWidth', 2)
    hold on
end
grid on
legend('t=T-dt','t=0.9T', 't=0.8T', 't=0.5T', 't=0')
xlabel('x')
ylabel('ksi(t,x)')
%title('HJB: dx= 0.01, dt=dx^2; Ind.function: -log0=1')
% %%
% % %%
% % 
% %
% xx=[-4.99 -4.9 -4.5 -4.0];
% for i=1:length(xx)
%     figure(19)
%     xfix=find(xW==xx(i));
%     plot(tW, ksiInterp(xfix,:), 'LineWidth', 2)
%     hold on
% end
% grid on
% legend('x=-5', 't=-4.8', 't=-4.6', 't=-4.4')
% xlabel('t')
% ylabel('ksi(t,x)')
% title('Optimal control at different time')
% %
% for s=1:S
% 
%     plot(t,ksi(u0(s),t))
%     hold on
% end
% smfac=3;
% indicatfun = @(x) (x>=K);
% smoothfun=@(x)0.5*(tanh(smfac*(x-K))+1);
% figure(1)
% plot(x, smoothfun(x), 'b--', x, indicatfun(x), 'r:', xW, v(:,1),'b-', 'Linewidth',2)
% legend('Smoothed Indicator', 'Indicator function', 'PDE sol at final T')
%%
figure(1)
semilogy(x(2:end),vErik(2:end)', 'k--', xW, v(:,1),'b-', 'Linewidth',3)
xlabel('x')
ylabel('u(x,T-dt_{EM})')
legend('ErikSol', 'Exp.Smooth c=100')
figure(2)
plot(x(2:end),vErik(2:end)', 'k--', xW, v(:,1),'b-', 'Linewidth',3)
xlabel('x')
ylabel('u(x,T-dt_{EM})')
legend('ErikSol', 'Exp.Smooth c=100')