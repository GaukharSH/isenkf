clear;  close all;
%Convergence test of KBE
% load('refsolforConv.mat')
% uRef=u;
% xRef=x(2:end);
K=4;
tau=1;
xL = -5; % Lower end of discretization interval
xU = K;  % Upper end of discretization interval
Nti =  2*2.^(10:1:16); % Number of time steps between observations
%Nti = 20*2.^(2:1:8);
Nxi =  10*2.^(2:1:9);% Number of discretization intervals in space
%x  = linspace(xL,xU,Nx+1)';% spatial step size
%  dt = tau/Nt;
%  dx = (xU-xL)/Nx;
%%
mu=1;
theta=1;
sigma=1;
a=@(x) 1-x;            %OU drift
%a=@(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x); %DW
b=@(x) sigma+0*x;  %OU process 
%b=@(x) sigma*x;   %IGBM process
for k=1:length(Nti)
    %Nx=Nxi(k);
    Nx= 1e3;
    Nt=Nti(k);
    x  = linspace(xL,xU,Nx+1)';% spatial step size
    dt = tau/Nt;
    dx = (xU-xL)/Nx;
    %[x,u] = KBEsolver1(x,tau,a,b,Nt,Nx);%PDE Solution
    [x,u] = BKEsolver(x,tau,a,b,Nt,Nx);
    %errKBE(k)=sqrt(mean((u-interp1(xRef,uRef, x(2:end), 'linear')).^2));
     
    if k>1
     errMF(k-1)=sqrt(mean((u-interp1(xCoarse,uCoarse,x,'linear', 'extrap')).^2));
    end
    uCoarse=u;
    xCoarse=x;
end
%%
% deltaX=(xU-xL)./Nxi;
% loglog(deltaX(2:end),errMF,'-ob','Linewidth', 1.5)
% xlabel('dx', 'Fontsize', 20)
% ylabel('RMSE', 'Fontsize', 20)
% slopeMean=polyfit(log2(deltaX(2:end)),log2(errMF),1);
% title([ ' Slope = ', num2str(slopeMean(1))])

deltaT=tau./Nti;
loglog(deltaT(2:end),errMF,'-ob','Linewidth', 1.5)
xlabel('dt', 'Fontsize', 20)
ylabel('RMSE', 'Fontsize', 20)
slopeMean=polyfit(log2(deltaT(2:end)),log2(errMF),1);
title([ ' Slope = ', num2str(slopeMean(1))])
%%

% loglog(deltaX(2:end),real(errMF),'-r', 'Linewidth', 1.5)
% loglog(deltaT(1:end-2),errMF(1:1:end-1),'-r', 'Linewidth', 1.5)
% xlabel('dx')
% ylabel('RMSE')

function I = trapezoidal(fx,dx) % For fx column vecor or matrix
  I = (sum(fx)-0.5*(fx(1,:)+fx(end,:)))*dx;
end