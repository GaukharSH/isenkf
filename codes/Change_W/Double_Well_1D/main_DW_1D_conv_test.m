clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for 1D problem to test PDE method in
%%% performing IS technique with respect to dW
%%% 
%------- dX_t= a(X_t)dt+b(X_t) dW_t -----------------------------------%
%----------------------------------------------------------------------%
%%% in computing QoI: P(max_{0<=t<=T} X_t >K)

%Problem parameters
sigma=0.5;
% a=@(t,x) 1-x+0*t;                             %OU drift
% aa=@(x) 1-x;
a=@(t,x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x)+0*t;   %DW drift
aa=@(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x);        %DW drift
b=@(x) sigma+0*x;                               %DW diffusion
T=1;                                            %simulation length
K=-0.5                                          %the threshold
Kstep=10;                                      %Numerical timestep number in SDE
dt = T/Kstep;                                   %Timestep in SDE

%Original density parameters
mu_0  =  -1;   %mean
sigma_0 = 0.5  %std

%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = -5;         % Lower end of discretization interval
xU = K;          % Upper end of discretization interval
Nt = 10;        % Number of time steps 
Nx = 50;        % Number of discretization intervals in space
dx = (xU-xL)/Nx; % spatial step size
x  = linspace(xL,xU,Nx+1)'; 

%PDE Solution 
ileqK=find(x<=K); 
igreK=find(x>K);
NxleqK=length(ileqK)-1;

%% PDE solution for all time
[tW, xW, v] = KBEsolverForAllt(x(ileqK),T,a,b,Nt,NxleqK,K);

%Plot the PDE solution at time 0
figure(1)
plot(xW, v(:,end), 'Linewidth',3)
xlabel('x', 'fontsize', 14)
ylabel('v(x,T)','fontsize', 14)
%%
dxW=xW(2)-xW(1);
%v=[0*v(1,:); v]; %Dirichlet
%v=[2*v(1,:)-v(2,:); v];%Linear
%v=[v(1,:); v]; %Neumann
%v=[v(2,:); v]; %Exponen.
ksiGridded = zeros(length(xW),length(tW));
ksiGridded(2:end-1,:) = sigma*(log(v(3:end,:))-log(v(1:end-2,:)))./(2*dxW);
ksiGridded(1,:) = sigma*(log(v(2,:))-log(v(1,:)))./dxW;
ksiGridded(end,:) = sigma*(log(v(end,:))-log(v(end-1,:)))./dxW;

% ksiGridded = zeros(length(xW),length(tW));
% ksiGridded(2:end-1,:) = sigma*(v(3:end,:)-v(1:end-2,:))./(2*dxW*v(2:end-1,:));
% ksiGridded(1,:) = sigma*(v(2,:)-v(1,:))./(dxW*v(1,:));
% ksiGridded(end,:) = sigma*(v(end,:)-v(end-1,:))./(dxW*v(end,:));

% ksi1 =flip(ksiGridded,2);

%%
[Xw,Tw] = ndgrid(xW,tW);
F=griddedInterpolant(Xw(1:end,1:end),flip(Tw(1:end,1:end),2), flip(ksiGridded(1:end,1:end),2),'linear', 'linear');
%F=griddedInterpolant(Xw(1000:end,1:end),flip(Tw(1000:end,1:end),2), flip(ksiGridded(1000:end,1:end),2),'linear', 'linear');
ksi = @(x,t) F(x,t).*(x<K)+zeros(size(t)).*(x>=K); 
figure(2)
% h=gca;
p0=surf(Xw, Tw, v);
set(p0,'LineStyle','none')
%set(h, 'zscale','log')
figure(3)
p1=surf(Xw(:,1:end), Tw(:,1:end), ksiGridded(:,1:end));
%p1=surf(Xw(1:end,:), Tw(1:end,:), F(Xw(1:end,:),Tw(1:end,:)));
set(p1,'LineStyle','none')
figure(4)
xF  = linspace(xL-5,K+2,Nx+1)'; 
[XwF,TwF] = ndgrid(xF,tW);
p2=surf(XwF, TwF, ksi(XwF,TwF));
set(p2,'LineStyle','none')
%%
 %[Xw,Tw] = meshgrid(xx(2:end),tW);
 %ksi =@(x,t) interp2(Xw,Tw,ksiGridded',x,t,'linear', 0);
%  xq=x(ileqK);
%  ksi = @(x) interp1(xq(2:end),ksiGridded(:,end),x,'linear', 0);
%  figure(3)
%  plot(x,ksi(x),'-b')
%%
 %ksi = @(x,t) 5; 

%% Computing the QoI with obtained mFit and sigFit parameters from PDE approach
Srng=[1e3 5e3 1e4 5e4 1e5];
%Srng=[1e3 5e3 1e4 5e4 1e5 5e5 1e6];
%Srng=[1e5];
alpha_hat_PDE=zeros(1, length(Srng));
alpha_hat_PDE_orig=zeros(1, length(Srng));
Vaprx_PDE=zeros(1, length(Srng));
Vaprx_PDE_orig=zeros(1, length(Srng));
RelError_PDE=zeros(1, length(Srng));
RelErrorIS_PDE=zeros(1, length(Srng));
varRatio_PDE=zeros(1, length(Srng));
varRatio_PDE1=zeros(1, length(Srng));
LklhoodMean=zeros(1, length(Srng));
for i=1:length(Srng)
    S=Srng(i);
    
    h=zeros(1,S);
    hOrig=zeros(1,S);
    u=zeros(1,Kstep+1);
    mFit=mu_0; %Crude MC setting to compare
    sigFit = sigma_0;
    u0=mFit+sigFit*randn(1,S);
    %fprintf(['1st run'])
    t=linspace(0,T,Kstep+1);
    meanL=0;
    parfor s=1:S
        u=u0(s);
        uOrig=u0(s);
        % u=mFit;
        % uOrig=mFit;
        L=1;
        for k=1:Kstep
            dW = sqrt(dt)*randn;
            %u(k+1)= u(k)+(a(t(k),u(k))+b(u(k))*ksi(u(k),t(k)))*dt+b(u(k))*dW;
            uOrig(k+1)=uOrig(k)+aa(uOrig(k))*dt+sigma*dW;
            %ksi(u(k))
            u(k+1)= u(k)+(aa(u(k))+sigma*ksi(u(k),t(k)))*dt+sigma*dW; %Forward Euler
            L=L*exp(-0.5*dt*(ksi(u(k),t(k)))^2-ksi(u(k),t(k))*dW);
            %pause()
                    if u(k)<-5
                        u(k)
                    %pause()
                    end
        end
        meanL=meanL+L;
        h(s)=(max(u)>=K)*L;
        hOrig(s)=(max(uOrig)>=K);
    end
%     figure(101)
%     subplot(1,2,1)
%     histogram(h)
%     subplot(1,2,2)
%     histogram(hOrig)
%     pause()
    LklhoodMean(i)=meanL/S;
    alpha_hat_PDE(i)=mean(h);
    alpha_hat_PDE_orig(i)=mean(hOrig);
    Vaprx_PDE(i)=var(h);
    Vaprx_PDE_orig(i)=var(hOrig);
    RelError_PDE(i)=1.96*sqrt(1-alpha_hat_PDE(i))/sqrt(alpha_hat_PDE(i)*S);
    RelErrorIS_PDE(i)=1.96*sqrt(Vaprx_PDE(i))/(alpha_hat_PDE(i)*sqrt(S));
    varRatio_PDE(i)=(RelError_PDE(i)/RelErrorIS_PDE(i))^2;
    varRatio_PDE1(i)=Vaprx_PDE_orig(i)/Vaprx_PDE(i);
end
%%
statErrMC=Vaprx_PDE_orig./sqrt(Srng);
statErrIS=Vaprx_PDE./sqrt(Srng);
errorMC=abs(statErrMC(1:end-1)-statErrMC(end));
errorIS=abs(statErrIS(1:end-1)-statErrIS(end));

%%
figure(10)
loglog(Srng(1:end-1), errorMC, 'r-o', Srng(1:end-1), errorIS, 'b-*', Srng(1:end-1), 1./sqrt(Srng(1:end-1)), 'k-','LineWidth',2)
xlabel('Sample Size M')
ylabel('Absolute error')
legend('MC', 'IS', '1/M^{1/2}')

figure(11)
semilogx(Srng, alpha_hat_PDE_orig, 'r-*', Srng, alpha_hat_PDE, 'b-o', Srng, alpha_hat_PDE_orig+1.96*sqrt(Vaprx_PDE_orig./Srng), 'r--', Srng, alpha_hat_PDE_orig-1.96*sqrt(Vaprx_PDE_orig./Srng), 'r--',Srng, alpha_hat_PDE+1.96*sqrt(Vaprx_PDE./Srng), 'b--', Srng, alpha_hat_PDE-1.96*sqrt(Vaprx_PDE./Srng), 'b--', 'LineWidth',2)
xlabel('Sample Size M')
legend('MC mean', 'IS mean')
% %%
% tt=[0.9 0.8 0.5 0];
% C = {'r','m', 'g', 'k', 'y'}; 
% for i=1:length(tt)
%     figure(9)
%     tfix=find(tW==tt(i));
%     semilogy(xW, v(:,tfix), 'LineWidth', 2)
%     hold on
% end
% grid on
% legend('t=0.9T', 't=0.8T', 't=0.5T', 't=0')
% xlabel('x')
% ylabel('log u(t,x)')
% title('KBE solution at different time with Neumann BC with coarse mesh')
% %%
% ksiInterp=ksi(Xw,Tw);
% for i=1:length(tt)
%     figure(10)
%     tfix=find(tW==tt(i));
%     semilogy(xW, ksiInterp(:,tfix), 'LineWidth', 2)
%     hold on
% end
% grid on
% legend('t=0.9T', 't=0.8T', 't=0.5T', 't=0')
% xlabel('x')
% ylabel('log ksi(t,x)')
% title('Optimal control at different time with Neumann BC with coarse mesh')
% 
% 
% %%
% %
% xx=[-4.99 -4.9 -4.5 -4.0];
% for i=1:length(xx)
%     figure(19)
%     xfix=find(xW==xx(i));
%     plot(tW, ksiInterp(xfix,:), 'LineWidth', 2)
%     hold on
% end
% grid on
% legend('x=-5', 't=-4.8', 't=-4.6', 't=-4.4')
% xlabel('t')
% ylabel('ksi(t,x)')
% title('Optimal control at different time')
% %
% for s=1:S
% 
%     plot(t,ksi(u0(s),t))
%     hold on
% end
% smfac=3;
% indicatfun = @(x) (x>=K);
% smoothfun=@(x)0.5*(tanh(smfac*(x-K))+1);
% figure(1)
% plot(x, smoothfun(x), 'b--', x, indicatfun(x), 'r:', xW, v(:,1),'b-', 'Linewidth',2)
% legend('Smoothed Indicator', 'Indicator function', 'PDE sol at final T')