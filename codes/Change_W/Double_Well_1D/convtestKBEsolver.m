clear;   close all; set(0, 'defaultaxesfontsize', 15); format long

%Problem parameters
sigma=0.5;
% a=@(t,x) 1-x+0*t;                             %OU drift
% aa=@(x) 1-x;
a=@(t,x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x)+0*t;   %DW drift
aa=@(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x);        %DW drift
b=@(x) sigma+0*x;                               %DW diffusion
T=1;                                            %simulation length
K=0.5                                             %the threshold

%Original density parameters
mu_0  =  -1;   %mean
sigma_0 = 0.2  %std

%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = -3;         % Lower end of discretization interval
xU = K;          % Upper end of discretization interval

Nti =  2*2.^(4:1:8); % Number of time steps between observations
Nxi =  10*2.^(1:1:5);% Number of discretization intervals in space
Nt=1e4;
%%
for k=1:length(Nxi)
    Nx=Nxi(k);
    %Nt=Nti(k);
    x  = linspace(xL,xU,Nx+1)';% spatial step size
    dt = T/Nt;
    dx = (xU-xL)/Nx;
    %PDE Solution 
    ileqK=find(x<=K); 
    igreK=find(x>K);
    NxleqK=length(ileqK)-1;

    [tW, xW, v] = HJBsolverForAllt_CN(x(ileqK),T,a,b,Nt,NxleqK,K);
     
%     ksiGridded = zeros(length(xW),length(tW));
%     ksiGridded(2:end-1,:) = sigma*(v(3:end,:)-v(1:end-2,:))./(2*dx*v(2:end-1,:));
%     ksiGridded(1,:) = sigma*(v(2,:)-v(1,:))./(dx*v(1,:));
%     ksiGridded(end,:) = sigma*(v(end,:)-v(end-1,:))./(dx*v(end,:));
%     z=zeros(length(xW),length(tW));
%     z(2:end-1,:)=(v(3:end,:)-v(1:end-2,:))./v(1:end-2,:);
%     z(1,:) = (v(2,:)-v(1,:))./(v(1,:));
%     z(end,:) = (v(end,:)-v(end-1,:))./(v(end-1,:));
% 
%     ksiGridded = zeros(length(xW),length(tW));
%     ksiGridded(2:end-1,:) = sigma*(log1p(z(2:end-1,:)))./(2*dx);
%     ksiGridded(1,:) = sigma*(log1p(z(1,:)))./(dx);
%     ksiGridded(end,:) = sigma*(log1p(z(end,:)))./(dx);

%     ksiGridded = zeros(length(xW),length(tW));
%     ksiGridded(2:end-1,:) = (log(v(3:end,:))-log(v(1:end-2,:)))./(2*dx);
%     ksiGridded(1,:) =(log(v(2,:))-log(v(1,:)))./(dx);
%     ksiGridded(end,:) = (log(v(end,:))-log(v(end-1,:)))./(dx);

ksiGridded = zeros(length(xW),length(tW));
ksiGridded(2:end-1,:) = -sigma*(v(3:end,:)-v(1:end-2,:))./(2*dx);
ksiGridded(1,:) = -sigma*(v(2,:)-v(1,:))./dx;
ksiGridded(end,:) = -sigma*(v(end,:)-v(end-1,:))./dx;

    if k>1
     errMF(k-1)=sqrt(mean((v(:,end)-interp1(Xcoarse,vCoarse,xW,'linear', 'extrap')).^2));
     %errMF(k-1)=sqrt(mean((ksiGridded(:,end)-interp1(Xcoarse,ksiGriddedCoarse,xW,'linear', 'extrap')).^2));
    end
    %[Xcoarse,Tcoarse] = ndgrid(xW,tW);
    Xcoarse=xW;
    vCoarse=v(:,end);
    ksiGriddedCoarse=ksiGridded(:,end);
end
%%
deltaX=(xU-xL)./Nxi;
loglog(deltaX(2:end),errMF,'-ob','Linewidth', 1.5)
xlabel('dx', 'Fontsize', 20)
ylabel('RMSE', 'Fontsize', 20)
slopeMean=polyfit(log2(deltaX(2:end)),log2(errMF),1);
title([ ' Slope = ', num2str(slopeMean(1))])

% deltaT=1./Nti;
% loglog(deltaT(2:end),errMF,'-ob','Linewidth', 1.5)
% xlabel('dt', 'Fontsize', 20)
% ylabel('RMSE', 'Fontsize', 20)
% slopeMean=polyfit(log2(deltaT(2:end)),log2(errMF),1);
% title([ ' Slope = ', num2str(slopeMean(1))])
%%

% loglog(deltaX(2:end),real(errMF),'-r', 'Linewidth', 1.5)
% loglog(deltaT(1:end-2),errMF(1:1:end-1),'-r', 'Linewidth', 1.5)
% xlabel('dx')
% ylabel('RMSE')
