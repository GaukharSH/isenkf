function I = trapezoidal(fx,dx) % For fx column vector or matrix
  I = (sum(fx)-0.5*(fx(1,:)+fx(end,:)))*dx;
end