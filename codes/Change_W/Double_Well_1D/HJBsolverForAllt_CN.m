function [time,x,v] = HJBsolverForAllt_CN(x,T,a,b,Nt,Nx,K,vErik)
  time  = linspace(T,0,Nt+1);     %Timespace in descending order
  dx = x(2)-x(1);                 %spatial step size
  dt = time(1)-time(2);           %time step
  
  v=zeros(Nx-1,Nt+1);
  vIter = zeros(Nx-1,Nt+1);
  % Final conditon v(x,T)=log 0
%  v(:,2) = -log(vErik(2:end-1));
%  v(:,1) = 10*ones(Nx-1,1); 
%   c=20;
  v(:,2)=-log(exp(c*(x(2:end-1)-K)));

  vIter(:,2) = v(:,2);
  vIterOld = vIter(:,2);
  for n=2:Nt
      
  t  = repmat(time(n),1,length(x))';
  
  % Below we create A_n tridiagonal matrix
  ax_n  = a(t,x);
  bx  = b(x);

  % Sub-diagonal elements of A_n
  Am    = ax_n(3:Nx)/dx/2-bx(3:Nx).^2/dx^2/2;
  
  % Diagonal elements of A_n
  Ac    = bx(2:Nx).^2/dx^2;
  Ac(1) = ax_n(2)/dx; %Linear extraploation
  %Ac(1) = ax_n(2)/dx/2+bx(2).^2/dx^2/2;%Neumann
  
  % Super-diagonal elements of A_n
  Ap    = -ax_n(2:Nx-1)/dx/2-bx(2:Nx-1).^2/dx^2/2;
  Ap(1) = -ax_n(2)/dx; %Linear extraploation
  %Ap_n(1) = -bx(2).^2/dx^2; %Exp. extraploation  
  
 
  % Matrix multiplying known solution values, at time t+dt
  Aexpl = spdiags(repmat([0,1,0],Nx-1,1)-dt/2*[[Am;0],Ac,[0;Ap]],-1:1,Nx-1,Nx-1);
  % Matrix multiplying iterated solution solution values, at time t+dt
  Bexpl = spdiags(-dt/2*[[Am;0],Ac,[0;Ap]],-1:1,Nx-1,Nx-1);
  % ----- Time stepping scheme ------------------------------------- %
   C = bx(2:Nx).^2/dx^2/8.*(([v(3:Nx-1,n); 0; 0]-[2*v(2,n)-v(3,n);v(2:Nx-1,n)]).^2+([vIter(3:Nx-1,n); 0; 0]-[2*vIter(2,n)-vIter(3,n); vIter(2:Nx-1,n)]).^2); 
   vIter(:,n) = Aexpl*v(:,n)+Bexpl*vIter(:,n)-C*dt/2; 
    

  iter=0;
  while max(abs(vIter(:,n)-vIterOld))>1e-08*max(abs(vIterOld))
%       max(abs(vIter(:,n)-vIterOld))
%       1e-8*max(abs(vIterOld))
%       pause()
      iter=iter+1;
    vIterOld = vIter(:,n);
    %Nonlinear term
    C = bx(2:Nx).^2/dx^2/8.*(([v(3:Nx-1,n); 0; 0]-[2*v(2,n)-v(3,n);v(2:Nx-1,n)]).^2+([vIter(3:Nx-1,n); 0; 0]-[2*vIter(2,n)-vIter(3,n); vIter(2:Nx-1,n)]).^2); 
    
    vIter(:,n) = Aexpl*v(:,n)+Bexpl*vIter(:,n)-C*dt/2;
  end

  v(:,n+1) = vIter(:,n);
  end
  
  
  
  x=x(2:end);              %x_0 is excluded since u(0,t) is approximated and incorporated to u(x_1,t) 
  v=[v; zeros(1,Nt+1)];    %u(x_{Nx+1}, t)=0 since we have boundary condition u(K,t)=0
  v=v(:,2:end);
  time=time(2:end);
end

