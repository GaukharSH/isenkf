function [x,u] = BKEsolver(x,tau,a,b,Nt,Nx)
    
 % Spatial discretization
  dx = x(2)-x(1);      % spatial step size
  dt = tau/Nt;         % time step
  nstep = Nt;          % number of time steps

  % Time stepping scheme (I+dt/2*A)u_{n} = (I-dt/2*A)u_{n+1}, A tridiagonal
  ax  = a(x);
  bx  = b(x);
  % Sub-diagonal elements of A
  Am    = ax(3:Nx)/dx/2-bx(3:Nx).^2/dx^2/2;
  
  % Diagonal elements of A
  Ac    = bx(2:Nx).^2/dx^2;
  Ac(1) = ax(2)/dx;
  
  % Super-diagonal elements of A
  Ap    = -ax(2:Nx-1)/dx/2-bx(2:Nx-1).^2/dx^2/2;
  Ap(1) = -ax(2)/dx;
  bc    = (ax(Nx)/dx/2+bx(Nx).^2/dx^2/2)*dt;
  
  % Matrix multiplying known solution values, at time t
  Aexpl = spdiags(repmat([0,1,0],Nx-1,1)-dt/2*[[Am;0],Ac,[0;Ap]],-1:1,Nx-1,Nx-1);
  % Matrix multiplying unknown solution values, at time t+dt
  Aimpl = spdiags(repmat([0,1,0],Nx-1,1)+dt/2*[[Am;0],Ac,[0;Ap]],-1:1,Nx-1,Nx-1);
  % LU-factorization of tri-diagonal matrix has no fill in
  [L,U] = lu(Aimpl);

  % ----- Time stepping scheme ------------------------------------- %

  % Initializations ------------------------------------------------ %

  % Final conditon u(x,T)=0
  u = zeros(1,Nx-1)'; 
  r = [zeros(1,Nx-2)'; bc];
  for n=1:nstep
    u = U\(L\(Aexpl*u+r));  
  end
  x=x(2:end);
  u=[u;1];
end




