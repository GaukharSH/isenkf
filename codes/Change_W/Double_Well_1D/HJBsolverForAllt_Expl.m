function [time,x,v] = HJBsolverForAllt_Expl(x,T,a,b,Nt,Nx,K)
  time  = linspace(T,0,Nt+1);     %Timespace in descending order
  dx = x(2)-x(1);                 %spatial step size
  dt = time(1)-time(2);           %time step
  
  v=zeros(Nx-1,Nt+1);
  % Final conditon v(x,T)=log 0
  %v(:,1) = 1*ones(Nx-1,1); 
  c=15;
  v(:,1)=-log(exp(c*(x(2:end-1)-K)));
  
  vStar = zeros(Nx+1,Nt+1);
  vStar(:,1) = [v(1,1); v(:,1); 0];
  figure()
  plot(x,vStar(:,1), 'Linewidth',2)
  hold on
  for n=1:Nt
  t   = repmat(time(n),1,length(x))';
  
  % Time stepping scheme u_{n} = (I-dt*A)v_{n+1}-dt*Bv_{n+1},
  
  % Below we create B_{n+1} tridiagonal matrix at time n+1
  ax  = a(t,x);
  bx  = b(x);
  
  % Sub-diagonal elements of B_{n+1}
  Am  = ax(3:Nx)/dx/2-bx(3:Nx).^2/dx^2/2;
  
  % Diagonal elements of B_{n+1}
  Ac    = bx(2:Nx).^2/dx^2;
  Ac(1) = ax(2)/dx; %Linear extraploation
  %Ac(1)=ax(2)/dx/2+bx(2).^2/dx^2/2;%Neumann
  
  % Super-diagonal elements of B_{n+1}
  Ap    = -ax(2:Nx-1)/dx/2-bx(2:Nx-1).^2/dx^2/2;
  Ap(1) = -ax(2)/dx;%Linear extraploation
  %Ap(1) = -bx(2).^2/dx^2; %Exp. extraploation
  
  % Below we create B_{n+1} vector
  B = bx(2:Nx).^2/dx^2/8.*((vStar(3:Nx+1,n)-vStar(1:Nx-1,n))./dx/2).^2;
  
  
  % Matrix multiplying known solution values, at time t+dt
  Aexpl = spdiags(repmat([0,1,0],Nx-1,1)-dt*[[Am;0],Ac,[0;Ap]],-1:1,Nx-1,Nx-1);

  % ----- Time stepping scheme ------------------------------------- %
    v(:,n+1) = (Aexpl*v(:,n)-B*dt);
    plot(x,[v(1,n+1); v(:,n+1); 0], 'Linewidth',2)
    pause()
    
  end
  x=x(2:end);              %x_0 is excluded since u(0,t) is approximated and incorporated to u(x_1,t) 
  v=[v; zeros(1,Nt+1)];    %u(x_{Nx+1}, t)=0 since we have boundary condition u(K,t)=0  
  v=v(:,2:end);
  time=time(2:end);
end

