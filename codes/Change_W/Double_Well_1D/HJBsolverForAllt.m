function [time,x,v] = HJBsolverForAllt(x,T,a,b,Nt,Nx,K)
  time  = linspace(T,0,Nt+1);     %Timespace in descending order
  dx = x(2)-x(1);                 %spatial step size
  dt = time(1)-time(2);           %time step
  
  v=zeros(Nx-1,Nt+1);
  vStar=zeros(Nx+1,Nt+1);
  % Final conditon v(x,T)=log 0
  v(:,1) = 1*ones(Nx-1,1); 
  %   c=30;
  %   v(:,1)=-log(exp(c*(x(2:end-1)-K)));
  % Initial guess for v^*(x,t) set to the solution of linear pde
  % vStar = [vKBE(1,:); vKBE];
  vUpd = [v(1,:); v; zeros(1,Nt+1)];
  %Initial guess for v^*(x,T)
  epsilon=1e-5;
  vStar(:,1)=[v(1,1); v(:,1); 0]+epsilon*ones(Nx+1,1);
  iter=0;
  sqrt(mean(vUpd(:,1)-vStar(:,1)).^2)
  %while sqrt(mean(vUpd(:,1)-vStar(:,1)).^2)>1e-5
  while max(abs(vUpd-))>1e-5
     iter=iter+1
  for n=1:Nt
  t   = repmat(time(n),1,length(x))';
  tt  = repmat(time(n+1),1,length(x))';
  
  % Time stepping scheme (I+dt/2*A_n)u_{n} = (I-dt/2*B_{n+1})u_{n+1}+dt*C_{n+1},
 
  % Below we create A_n tridiagonal matrix
  ax_n  = a(tt,x);
  bx  = b(x);

  % Sub-diagonal elements of A_n
  Am_n    = ax_n(3:Nx)/dx/2-bx(3:Nx).^2/dx^2/2;
  
  % Diagonal elements of A_n
  Ac_n    = bx(2:Nx).^2/dx^2;
  %Ac_n(1) = ax_n(2)/dx; %Linear extraploation
  Ac_n(1) = ax_n(2)/dx/2+bx(2).^2/dx^2/2;%Neumann
  
  % Super-diagonal elements of A_n
  Ap_n    = -ax_n(2:Nx-1)/dx/2-bx(2:Nx-1).^2/dx^2/2;
  %Ap_n(1) = -ax_n(2)/dx; %Linear extraploation
  %Ap_n(1) = -bx(2).^2/dx^2; %Exp. extraploation
  
  % Below we create B_{n+1} tridiagonal matrix at time n+1
  ax  = a(t,x);
  
  % Sub-diagonal elements of B_{n+1}
  Am  = ax(3:Nx)/dx/2-bx(3:Nx).^2/dx^2/2+(vStar(4:Nx+1,n)-vStar(2:Nx-1,n)).*bx(3:Nx).^2/dx^2/4;
  
  % Diagonal elements of B_{n+1}
  Ac    = bx(2:Nx).^2/dx^2;
  %Ac(1) = ax(2)/dx; %Linear extraploation
  Ac(1)=ax(2)/dx/2+bx(2).^2/dx^2/2;%Neumann
  
  % Super-diagonal elements of B_{n+1}
  Ap    = -ax(2:Nx-1)/dx/2-bx(2:Nx-1).^2/dx^2/2+(vStar(3:Nx,n)-vStar(1:Nx-2,n)).*bx(2:Nx-1).^2/dx^2/4;
  %Ap(1) = -ax(2)/dx;%Linear extraploation
  %Ap(1) = -bx(2).^2/dx^2; %Exp. extraploation
  
  % Below we create C_{n+1} vector
  C = -bx(2:Nx).^2/dx^2/8.*(vStar(3:Nx+1,n)-vStar(1:Nx-1,n));
  
  
  % Matrix multiplying known solution values, at time t+dt
  Aexpl = spdiags(repmat([0,1,0],Nx-1,1)-dt/2*[[Am;0],Ac,[0;Ap]],-1:1,Nx-1,Nx-1);
  % Matrix multiplying unknown solution values, at time t
  Aimpl = spdiags(repmat([0,1,0],Nx-1,1)+dt/2*[[Am_n;0],Ac_n,[0;Ap_n]],-1:1,Nx-1,Nx-1);

  % ----- Time stepping scheme ------------------------------------- %
    v(:,n+1) = Aimpl\(Aexpl*v(:,n)+C*dt); 
    vUpd(:,n+1) =[v(1,n+1); v(:,n+1); 0];
  end
  vStar=vUpd;
  vUpd=[v(1,:); v; zeros(1,Nt+1)];

  end
  x=x(2:end);              %x_0 is excluded since u(0,t) is approximated and incorporated to u(x_1,t) 
  v=[v; zeros(1,Nt+1)];    %u(x_{Nx+1}, t)=0 since we have boundary condition u(K,t)=0
  
  v=v(:,2:end);
  time=time(2:end);
end

