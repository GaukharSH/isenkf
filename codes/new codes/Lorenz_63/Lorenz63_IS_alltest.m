clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for Lorenz 63 problem to test PDE method
%%% in performing IS technique: 
%-- dv1 = r(v2-v1)+sigma*dW ---------------%
%-- dv2 = sv1-v2-v1v3   ---------------%
%-- dv3 = v1v2-qv3      ---------------%
%-------------------------------------------------------------------------%
%%% in computing QoI: P(max_{0<=t<=T} P1*v(t) >K)
    
%Model parameters for Lorenz 63
d=3;              %problem dimension
sigma=10;        %constant diffusion parameter
r=10;                    
q=8/3;
s=28;
% sigma=3;        %constant diffusion parameter
% r=3;                    
% q=1;
% s=26;
%(s,r, q)=(r,sigma,b)=(4,16,1) (1,16,0.03), (1,-17, -1)
% r=-17;                    
% q=-1;
% s=1;
a = @(u) [r*(u(2,:,:)-u(1,:,:)); s*u(1,:,:)-u(2,:,:)-u(1,:,:).*u(3,:,:); u(1,:,:).*u(2,:,:)-q*u(3,:,:)]; %Lorenz 63 drift
%b = @(u) [sigma; sigma; sigma]; 
b = [sigma; sigma; sigma];
%b = @(u) [sigma*u(1,:,:); sigma*u(2,:,:); sigma*u(3,:,:)]; 

I=eye(d);

%Simulation parameters
T=0.1;         %final time (simulation length)
M=10^4;      %Monte Carlo sample size
N=100;      %number of timesteps in SDE
dt=T/N;      %discretization step in SDE

%% Initial original density parameters 
mu=[0 0 0]';
Sigma=0.000025*I;
% mu=[5.5 5.5 25.5]';
% Sigma=0.0025*I;
%Sigma=0.5*I;
%Define the Projector which component to track
P1    = I(1,:); %1st component
%P1   = diag(I)'/d; 
%P1   = I(end,:);
%% Generate samples of Lorenz 63 dynamics to use for L2 regression
u=zeros(d,N+1,M);
t=linspace(0,T,N+1);
Mbar=zeros(1,M);
K=100;
% aTrue=zeros(N,M);
% u_bar=zeros(N,M);
for m=1:M
    u(:,1,m)=mu+chol(Sigma)'*randn(d,1);
    %u(:,1,m)=mu+chol(Sigma)'*randl(d,1);
    %u(:,1,m)=unifrnd(2,10, d,1);
    %aTrue(1,m)=P1*lorenz63(u(:,1,m),r,q,s);
    %aMP(1,m)=a_bar(t(1),u(1,1,m));
    %u_bar(1,m)=mu(1)+chol(Sigma(1,1))'*randn;
    n=1;
    while n<=N && P1*u(:,n,m)<K  
        %Lorenz63
        u(:,n+1,m)=u(:,n,m)+lorenz63(u(:,n,m),r,q,s)*dt+b.*sqrt(dt).*randn(3,1);%EM time-stepping
        %aTrue(n+1,m)=P1*lorenz63(u(:,n,m),r,q,s);
        %u(:,n+1,m)=u(:,n,m)+rk4(u(:,n,m),dt,r,q,s)+b.*sqrt(dt).*randn(3,1); %Runge-Kutta time-stepping
        %aMP(n+1,m)=a_bar(t(n+1),u(1,n+1,m));
        %u_bar(n+1,m)=u_bar(n,m)+a_bar(t(n),u_bar(n,m))*dt+b_bar(u_bar(n,m)).*sqrt(dt).*randn;%EM time-stepping
    
        exitprob=exp(-2*max(K-P1*u(:,n,m),0)*max(K-P1*u(:,n+1,m),0)/(P1*b.*dt));
        if rand<exitprob 
            u(1,n+1,m)=K; %manually set, in the case of P1=[1 0 0], later I must make it automatic for any projection
        end
        n=n+1;
    end 
%     n
%     P1*u(:,n,m)
%     pause()
% figure(5)
% plot(t, u(1,:,m), 'LineWidth', 2)
% xlabel('t', 'fontsize', 14)
% ylabel('u_1', 'fontsize', 14)
% hold on
    Mbar(m)=max(P1*u(:,:,m));
end
%%
figure
plot3(u(1,:,1),u(2,:,1),u(3,:,1))
%% Generate samples of Lorenz 63 dynamics to use for L2 regression
u=zeros(d,N,M);
t=linspace(0,T,N+1);
Mbar=zeros(1,M);
% aTrue=zeros(N,M);
% u_bar=zeros(N,M);
for m=1:M
    u(:,1,m)=mu+chol(Sigma)'*randn(d,1);
    %u(:,1,m)=mu+chol(Sigma)'*randl(d,1);
    %u(:,1,m)=unifrnd(2,10, d,1);
    %aTrue(1,m)=P1*lorenz63(u(:,1,m),r,q,s);
    %aMP(1,m)=a_bar(t(1),u(1,1,m));
    %u_bar(1,m)=mu(1)+chol(Sigma(1,1))'*randn;
    for n=1:N       
        %Lorenz63
        u(:,n+1,m)=u(:,n,m)+lorenz63(u(:,n,m),r,q,s)*dt+b.*sqrt(dt).*randn(3,1);%EM time-stepping
        %aTrue(n+1,m)=P1*lorenz63(u(:,n,m),r,q,s);
        %u(:,n+1,m)=u(:,n,m)+rk4(u(:,n,m),dt,r,q,s)+b(u(:,n,m)).*sqrt(dt).*randn(3,1); %Runge-Kutta time-stepping
         
        %u(:,n+1,m)=u(:,n,m)+rk4(u(:,n,m),dt,r,q,s)+b.*sqrt(dt).*randn(3,1); %Runge-Kutta time-stepping
        %aMP(n+1,m)=a_bar(t(n+1),u(1,n+1,m));
        %u_bar(n+1,m)=u_bar(n,m)+a_bar(t(n),u_bar(n,m))*dt+b_bar(u_bar(n,m)).*sqrt(dt).*randn;%EM time-stepping
    end 
%     figure(5)
% plot(t, u(1,:,m), 'LineWidth', 2)
% xlabel('t', 'fontsize', 14)
% ylabel('u_1', 'fontsize', 14)
% title('\sigma=0.5')
% hold on
    Mbar(m)=max(P1*u(:,:,m));
end
%% Plotting marginal and joint densities at final time 
x=squeeze(u(1,end,:));
y=squeeze(u(2,end,:));
z=squeeze(u(3,end,:));
figure
scatterhist(x,y,'Kernel','on','Location','NorthEast',...
    'Direction','out','Color','k','LineStyle',{'-'},...
    'LineWidth',[2],'Marker','o','MarkerSize',[4]);
figure
scatterhist(x,z,'Kernel','on','Location','NorthEast',...
    'Direction','out','Color','k','LineStyle',{'-'},...
    'LineWidth',[2],'Marker','o','MarkerSize',[4]);
figure
scatterhist(y,z,'Kernel','on','Location','NorthEast',...
    'Direction','out','Color','k','LineStyle',{'-'},...
    'LineWidth',[2],'Marker','o','MarkerSize',[4]);
%% Thresholds corresponding to O(1e-3) probability
%K=11.8;    %if \sigma=0.05
K=20.2;    %if \sigma=0.5
%K=13.4;     %if \sigma=1
%K=27.5;    %if \sigma=10
K=12;
alpha_hat=mean(Mbar>=K); %QoI
%% L2 regression
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We approximate only the drift, the diffusion is constant

%Define the Polynomial space
w = 2;                                                         %Polynomial degree
p = poly_space(w,'HC');                                        %Polynomial space
p_dim = size(p, 1);                                            %Cardinality of polynomial space

t  = linspace(0,T,N+1)';                                       %Generating t_0, t_1, ..., t_{N-1}
Tt = reshape(repmat(t,1,M)', (N+1)*M,1);                       %Replicating M times each t_n and saving as a column vector [t_0 t_0 ... t_0, t_1, ..., t_1,..., t_{N-1}, ..., t_{N_1}]'
Xx = reshape(squeeze(pagemtimes(P1,u))', (N+1)*M,1);           %Projected Samples saved as a column vector [X_0^(1),..., X_0^(M), X_1^(1),..,X_1^(M), ..., X_{N-1}^(1),...,X_{N-1}^(M)]
tX = [Tt Xx];                                                  %Two column vectors of size N*M for [t_n, X_{t_n}]
f  = reshape(squeeze(pagemtimes(P1,a(u)))',(N+1)*M,1);         %Given f function, we are going to solve

D     = x2fx(tX, p);                                           %This function helps to create Psi matrix each column is non-orth. basis function psi_p for different given p
[Q,R] = modified_GS(D);                                        %Apply modified Gram-Schmidt process to get QR decomposition
a_p   = Q'*f;                                                  %Solve Normal equations based on orthonormalised basis

psy     = @(t,s) x2fx([t s], p);                               %Non-orth. basis function
psy_bar = @(t,s) x2fx([t s], p)/R;                             %Orth. basis function
a_bar   = @(t,s) psy_bar(t,s)*a_p;                             %Approximation to the drift function a(x)
%b_bar   = @(s)   sqrt(P1*(b*b')*P1')*ones(size(s));            %Approximation to the diffusion function b(x)
b_bar   = @(s)   sqrt(P1*(b*b')*P1')*ones(size(s));            %Approximation to the diffusion function b(x)
%%
f_b = reshape(squeeze(pagemtimes(P1,b(u))).^2',(N+1)*M,1);
b_p   = Q'*f_b;
b_bar_square   = @(t,s) psy_bar(t,s)*b_p;      
b_bar   = @(t,s) sqrt(psy_bar(t,s)*b_p); %Approximation to the drift function b(x)
%%
figure
fsurf(a_bar, [0 T -K K])
hold on
plot3(Tt, Xx, f, 'k*')
xlabel('t','fontsize', 18)
ylabel('x','fontsize', 18)
zlabel('$\bar{a}(x,t)$', 'interpreter', 'latex','fontsize', 18)
title(['$\mathcal{K}=$' num2str(K), ', ', '$\sigma=$', num2str(sigma), ', ', 'HC=', num2str(w)],'interpreter', 'latex','fontsize', 22)
%% residual
% figure
% plot3(Tt,Xx, f_b-b_bar(Tt,Xx), 'r')
% xlabel('t','fontsize', 18)
% ylabel('x','fontsize', 18)
% zlabel('$P_1a(x,t)-\bar{a}(x,t)$', 'interpreter', 'latex','fontsize', 18)
% title('Residual over t')
% view([0 360])
figure
plot3(Tt,Xx, f-a_bar(Tt,Xx), 'r')
xlabel('t','fontsize', 18)
ylabel('x','fontsize', 18)
zlabel('$P_1a(x,t)-\bar{a}(x,t)$', 'interpreter', 'latex','fontsize', 18)
title('Residual over x')
view([90 360])

%% Affine approximation
%Define the Polynomial space
w = 2;                                                                      %Polynomial degree
p=[1 0];
p_dim = size(p, 1);                                                         %Cardinality of polynomial space

t  = linspace(0.2,0.3,100+1)';                                              %Generating t_0, t_1, ..., t_{N-1}
Tt = reshape(repmat(t,1,M)', (100+1)*M,1);                                  %Replicating M times each t_n and saving as a column vector [t_0 t_0 ... t_0, t_1, ..., t_1,..., t_{N-1}, ..., t_{N_1}]'
Xx = reshape(squeeze(pagemtimes(P1,u(:,1:101,:)))', (100+1)*M,1);           %Projected Samples saved as a column vector [X_0^(1),..., X_0^(M), X_1^(1),..,X_1^(M), ..., X_{N-1}^(1),...,X_{N-1}^(M)]
tX = [Tt Xx];                                                               %Two column vectors of size N*M for [t_n, X_{t_n}]
f  = reshape(squeeze(pagemtimes(P1,a(u(:,1:101,:))))',(100+1)*M,1);         %Given f function, we are going to solve

D     = x2fx(tX, p);                                                        %This function helps to create Psi matrix each column is non-orth. basis function psi_p for different given p
[Q,R] = modified_GS(D);                                                     %Apply modified Gram-Schmidt process to get QR decomposition
a_p   = Q'*f;                                                               %Solve Normal equations based on orthonormalised basis

psy     = @(t,s) x2fx([t s], p);                                            %Non-orth. basis function
psy_bar = @(t,s) x2fx([t s], p)/R;                                          %Orth. basis function
a_bar   = @(t,s) psy_bar(t,s)*a_p;                                          %Approximation to the drift function a(x)
b_bar   = @(s)   sqrt(P1*(b*b')*P1')*ones(size(s));                         %Approximation to the diffusion function b(x)

figure
fsurf(a_bar, [0.2 0.3 -K K+5])
hold on
plot3(Tt, Xx, f, 'k*')
xlabel('t','fontsize', 18)
ylabel('x','fontsize', 18)
zlabel('$\bar{a}(x,t)$', 'interpreter', 'latex','fontsize', 18)
title(['$\mathcal{K}=$' num2str(K), ', ', '$\sigma=$', num2str(sigma), ', ', 'HC=', num2str(w)],'interpreter', 'latex','fontsize', 22)

%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = -K;                 % Lower end of discretization interval
%xL = -3;                     % Lower end of discretization interval
xU = K;                       % Upper end of discretization interval
dxPde = 0.05;                % Space step in PDE    
dtPde = dxPde^2;              % Time step in PDE
Nx = round((xU-xL)/dxPde);    % Number of space steps in PDE
Nt = round(T/dtPde);          % Number of time steps in PDE
x  = linspace(xL,xU,Nx+1)';   % Space grid points

%a_barTmdt=@(x) a_bar(T-dt, x);
%[~,~,vErik] = tabulate_IC(a_barTmdt,b_bar,dt,K,xL,xU,false,Nx+1); %KBE solution at T-dt obtained via numerical optimization (using Erik's code)
%% Analytical solution for frozen coeff. at the corner
v_anal=@(x,t) 0.5*erfc((K-x-a_bar(T,K).*(T-t))./sqrt(2*b_bar(K).^2.*(T-t)))...
              +0.5*exp(a_bar(T,K).*(K-x)./b_bar(K).^2./2).*erfc((K-x+a_bar(T,K).*(T-t))./sqrt(2*b_bar(K).^2.*(T-t)));
% %% Analytical solution for frozen coeff. at the corner
% v_anal=@(x,t) 0.5*erfc((K-x-a_bar(T,K).*(T-t))./sqrt(2*b_bar(T,K).^2.*(T-t)))...
%               +0.5*exp(a_bar(T,K).*(K-x)./b_bar(T,K).^2./2).*erfc((K-x+a_bar(T,K).*(T-t))./sqrt(2*b_bar(T,K).^2.*(T-t)));

          %% Finding a (red) region in the corner to use analytical solution for the pde solver
absdiff=cell(Nx+1, Nt+1);
t=linspace(T,0,Nt+1);
[Xw,Tw] = ndgrid(x,t);
[px, py]=gradient(v_anal(Xw,Tw));
gradNeeded={[dxPde dtPde]};
for i = 1:Nx+1
for j = 1:Nt+1
absdiff{i,j}= norm([px(i,j) py(i,j)]-[dxPde dtPde]);
end
end
[MinVal, ind]=min(cell2mat(absdiff),[], 'all', 'linear');
[idx{1:ndims(cell2mat(absdiff))}] = ind2sub(size(cell2mat(absdiff)), ind);
RedRegionIndx=[idx{:}];
x(RedRegionIndx(1))
t(RedRegionIndx(2))
px(idx{:}) %should be close to dxPde
py(idx{:}) %should be close to dtPde
%% KBE solution for all time using anal.sol
[tW, xW, v] = KBEsolverForAllt_AnalSol(x,T,a_bar,b_bar,Nt,Nx,K,RedRegionIndx); %PDE solver via CN backward scheme using analytical solution with frozen coeff. in the corner
%% KBE solution for all time using anal.sol
% [tW, xW, v] = KBEsolverForAllt_AnalSol_b_bar(x,T,a_bar,b_bar,Nt,Nx,K,RedRegionIndx); %PDE solver via CN backward scheme using analytical solution with frozen coeff. in the corner

%% Plot for choosing the appropriate smoothing factor
figure
plot(xW, vErik(2:end)', '--k', xW, v(:,1), '-b', 'Linewidth',2) % xW, exp(-u(:,1)), '-r',
xlabel('x', 'fontsize', 14)
ylabel('u(x,0)','fontsize', 14)
legend('Erik sol', 'Exp.smooth. 10')
%% Plot KBE solution for all t and x
[Xw,Tw] = ndgrid(xW,tW);
figure
p0=surf(Xw, Tw, v);
set(p0,'LineStyle','none')
xlabel('x','fontsize', 18)
ylabel('t','fontsize', 18)
zlabel('u(x,t)','fontsize', 20)
title(['$\mathcal{K}=$' num2str(K), ', ', '$\sigma=$', num2str(sigma)],'interpreter', 'latex','fontsize', 22)
%% Plot KBE solution at different time in 2D
tt=[T-dtPde 0.9 0.8 0.5 0];
C = {'r','m', 'g', 'k', 'y'}; 
for i=1:length(tt)
    figure(11)
    tfix=find(tW==tt(i));
    %semilogy(xW, exp(-u(:,tfix)), 'LineWidth', 2)
    semilogy(xW, v(:,tfix), 'LineWidth', 2)
    hold on
end
grid on
legend('t=T-dt', 't=0.9T', 't=0.8T', 't=0.5T', 't=0')
xlabel('x')
ylabel('u(t,x)')
title('KBE solution at different time')
%%
for j=1:Nt+1
    ind=find(v(:,j)<0);
    if isempty(ind)==0
    v(1:ind(end),j)=0;
    end
end

%% dW CONTROL
%obtained via KBE solution on PDE grids
ksiGridded = zeros(length(xW),length(tW));
ksiGridded(2:end-1,:) = sigma*(log(v(3:end,:))-log(v(1:end-2,:)))./(2*dxPde);
ksiGridded(1,:) = sigma*(log(v(2,:))-log(v(1,:)))./dxPde;
ksiGridded(end,:) = sigma*(log(v(end,:))-log(v(end-1,:)))./dxPde;

% ksiGridded = zeros(length(xW),length(tW));
% ksiGridded(2:end-1,:) = b_bar(Tw,Xw)*(log(v(3:end,:))-log(v(1:end-2,:)))./(2*dxPde);
% ksiGridded(1,:) = b_bar()*(log(v(2,:))-log(v(1,:)))./dxPde;
% ksiGridded(end,:) = b_bar()*(log(v(end,:))-log(v(end-1,:)))./dxPde;

[Xw,Tw] = ndgrid(xW,tW); %meshgrid for the interpolant

cropN=round(0.20*Nx)+1; %if we use KBE, I crop 10% of the left baoundary and linearly smooth it to avoid instability on the left handside; for HJB no need to do this and nCrop=1
cropT=round(0*Nt)+1;
Ff=griddedInterpolant(Xw(cropN:end,cropT:end),flip(Tw(cropN:end,cropT:end),2), flip(real(ksiGridded(cropN:end,cropT:end)),2),'linear','linear');
ksiStar_PDE = @(x,t) Ff(x,t).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function based on the discrete optimal control values

ksiStar_Asmp = @(x,t) ((1/sigma)*((K-x)./(T-t))-a_bar(t,x)).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function based on asymptotic apprx.
%ksiStar = @(x,t, dt_EM) ksiStar_PDE(x,t);
ksiStar = @(x,t, dt_EM) sum([ksiStar_PDE(x,t).*(T-t>=10*dt_EM) ksiStar_Asmp(x,t)*(T-t<10*dt_EM)], 'omitnan'); %the factor 4 corresponds for the particular choice of dtPde=0.005/2 where ksigridded(x,t) with t<=0.96 smooth,t>0.96 it has a kink close to the barrier.
%%
%Optimal control using HJB solution
% Ff = griddedInterpolant(Xw,flip(Tw,2), flip(ksiGriddedHJB,2),'linear','linear');
% ksi = @(x,t) Ff(x,t).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function for any x and t

% %Plot the optimal control on the PDE meshgrid
figure
p1=surf(Xw(550:end,50:end), Tw(550:end,50:end), ksiGridded(550:end,50:end));
set(p1,'LineStyle','none')
% 
% % %Plot the extrapolated optimal control
% figure(6)
% xF  = linspace(xL-5,K+3,Nx+1)'; 
% [XwF,TwF] = ndgrid(xF,tW);
% p2=surf(XwF, TwF, ksi(XwF,TwF));
% set(p2,'LineStyle','none')

%% Plot optimal control at different time in 2D
%ksiInterp=ksiStar(Xw,Tw);
for i=1:length(tt)
    figure(8)
    tfix=find(tW==tt(i));
    plot(xW, ksiGridded(:,tfix), 'LineWidth', 2)
    hold on
end
grid on
legend('t=T-dt','t=0.9T', 't=0.8T', 't=0.5T', 't=0')
xlabel('x')
ylabel('ksi(t,x)')
hold off
title('Optimal Control at different time')

%% Computing the QoI with and without change of measure wrt W 
S=10^5; %MC sample size

%Initialization
h_IS=zeros(1,S);
h_MC=zeros(1,S);
u_MC=zeros(d,N+1);
u_IS=u_MC;
u0=mu+chol(Sigma)'*randn(d,S);
%u0=mu*ones(1,S);%+chol(Sigma)'*randn(d,S);
u0_shifted=u0;
t=linspace(0,T,N+1);
meanL=0;
dt_const=dt;
parfor i=1:S
       u_MC=u0(:,i);
       u_IS=u0_shifted(:,i);
       
       L=1;
       k=1;
       t_k=0;
       dt=dt_const;
%        trng=[];
    while t_k<=T && P1*u_IS(:,k)<K && dt>0.000001
%         trng=[trng, t_k];
    dt=min(dt_const, 0.5*(T-t_k));
    dW = sqrt(dt).*randn(d,1);

    %Lorenz63 
    %u_IS(:,k+1)= u_IS(:,k)+rk4(u_IS(:,k),dt,r,q,s)+P1'.*b.*ksiStar(P1*u_IS(:,k),t(k), dt_const)*dt+b.*dW; %Runge-Kutta
    u_IS(:,k+1)= u_IS(:,k)+(lorenz63(u_IS(:,k),r,q,s)+P1'.*b.*ksiStar(P1*u_IS(:,k),t_k,dt_const))*dt+b.*dW; %Forward Euler 
    %u_IS(:,k+1)= u_IS(:,k)+(P1'*(K-u_IS(1,k))/(T-t_k))*dt+b.*dW; %Forward Euler 
    L=L*exp(-0.5*dt*(ksiStar(P1*u_IS(:,k),t_k, dt_const))^2-ksiStar(P1*u_IS(:,k),t_k,dt_const)*P1*dW);
%     t_k
%     dt
%     u_IS(1,k)
%     1/sigma*(K-u_IS(1,k))/(T-t_k)-a_bar(t_k,u_IS(1,k))
%     -0.5*dt*(1/sigma*(K-u_IS(1,k))/(T-t_k)-a_bar(t_k,u_IS(1,k)))^2-(1/sigma*(K-u_IS(1,k))/(T-t_k)-a_bar(t_k,u_IS(1,k)))*P1*dW
%    L=L*exp(-0.5*dt*(1/sigma*(K-u_IS(1,k))/(T-t_k)-a_bar(t_k,u_IS(1,k)))^2-(1/sigma*(K-u_IS(1,k))/(T-t_k)-a_bar(t_k,u_IS(1,k)))*P1*dW);
     exitprob=exp(-2*max(K-P1*u_IS(:,k),0)*max(K-P1*u_IS(:,k+1),0)/(P1*b*dt));
%     L
%     pause()
        if rand<exitprob 
            t_k=t_k+0.5*dt ;
            u_IS(1,k+1)=K; %manually set, in the case of P1=[1 0 0], later I must make it automatic for any projection
        else
            t_k=t_k+dt;
        end

    k=k+1;
    end
%     k
%     L
%     figure(5)
% plot(u_IS(1,1:k-1), 'LineWidth', 2)
% xlabel('t', 'fontsize', 14)
% ylabel('u_1', 'fontsize', 14)
% hold on
% pause()
%     dt
    %ksiStar(P1*u_IS(:,k),t_k-dt, dt_const)
%L
%      trng=[trng, t_k];
% figure(10)
% plot(trng(1:end), u_IS(1,:), 'LineWidth', 2)
% xlabel('t', 'fontsize', 14)
% ylabel('u_1', 'fontsize', 14)
% hold on
    %Monte Carlo simulation
        n=1;
        t_n=0;
    while t_n<=T && P1*u_MC(:,n)<K 
         dW = sqrt(dt_const).*randn(d,1);
         u_MC(:,n+1)= u_MC(:,n)+lorenz63(u_MC(:,n),r,q,s)*dt_const+b.*dW;
         %u_MC(:,n+1)= u_MC(:,n)+rk4(u_MC(:,n),dt,r,q,s)+b.*dW;
         
            q_MC= exp(-2*max(K-P1*u_MC(:,n),0)*max(K-P1*u_MC(:,n+1),0)/(P1*b*dt_const));
            if rand<q_MC 
            u_MC(1,n+1)=K;  %manually set, in the case of P1=[1 0 0], later I must make it automatic for any projection
            end
            t_n=t_n+dt_const;
            n=n+1;
    end

    meanL=meanL+L;
    h_IS(i)=(max(P1*u_IS)>=K)*L;
    %pause()
    h_MC(i)=(max(P1*u_MC)>=K);
end
meanL=meanL/S
%Weight = rho_x0(P1*u0_shifted,P1*mu,sqrt(P1*Sigma*P1'))./rho_x0(P1*u0_shifted, mFit, sigFit);
Weight = ones(1,S);
alpha_hat_IS_W=mean(h_IS.*Weight)
alpha_hat_MC=mean(h_MC)
Vaprx_IS_W=var(h_IS.*Weight)
Vaprx_MC=var(h_MC)
RelError_MC=1.96*sqrt(1-alpha_hat_IS_W)/sqrt(alpha_hat_IS_W*S)
RelError_IS=1.96*sqrt(Vaprx_IS_W)/(alpha_hat_IS_W*sqrt(S))
varRatio_approx=(RelError_MC/RelError_IS)^2
varRatio_exact=Vaprx_MC/Vaprx_IS_W

function dv = rk4(v_old,h,r,q,s)
% v[t+1] = rk4(v[t],step)
 k1 = lorenz63(v_old,r,q,s);
 k2 = lorenz63(v_old+1/2.*h.*k1,r,q,s);
 k3 = lorenz63(v_old+1/2.*h.*k2,r,q,s);
 k4 = lorenz63(v_old+h.*k3,r,q,s);
 dv= 1/6*h*(k1+2*k2+2*k3+k4);
end