clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for Lorenz 96 problem to test CE method and PDE method
%%% in performing IS technique: 
%-- dv_k = (v_{k-1}(v_{k+1}-v_{k-2})-v_k+F)dt+sigma_k dW_k ---------------%
%-- v_0=v_d, v_{d+1}=v_1 , v_{-1}= v_{d-1}, k=1,...,d.     ---------------%
%-------------------------------------------------------------------------%
%%% in computing QoI: P(max_{0<=t<=T} P1*v(t) >K)

%Model parameters
d=10;                                            %problem dimension
F=8;                                             %forcing term
sigma=0.5;                                       %constant diffusion parameter
I=eye(d);
%Lorenz 96
a = @(v) [v(end,:,:); v(1:end-1,:,:)].*([v(2:end,:,:); v(1,:,:)]-[v(end-1:end,:,:);v(1:end-2,:,:)])-v+F*v.^0; %Lorenz 96 drift
b = sigma*ones(d,1);

%Simulation parameters
T=0.1;         %final time (simulation length)
M=10^5;       %Monte Carlo sample size
N=100;       %number of timesteps in SDE
dt=T/N;      %discretization step in SDE
%%
%Initial original density parameters 
mu=0*ones(d,1);
Sigma=0.01*I;

%Define the Projector which component to track
P1   = I(1,:); 
%% Generate samples of Lorenz 96 dynamics to use for L2 regression
u=zeros(d,N,M);
t=linspace(0,T,N+1);
Mbar=zeros(1,M);

for m=1:M
    u(:,1,m)=mu+chol(Sigma)'*randn(d,1); 
    for n=1:N
        %Lorenz96
        %u(:,n+1,m)=u(:,n,m)+rk4(u(:,n,m),dt,F)+(b.*sqrt(dt).*randn(d,1));

        u(:,n+1,m)=u(:,n,m)+lorenz96(u(:,n,m),F)*dt+b.*sqrt(dt).*randn(d,1);       
    end
   
    Mbar(m)=max(P1*u(:,:,m));
end

%%
K=floor(max(Mbar))
%% L2 regression
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We approximate only the drift, the diffusion is constant

%Define the Polynomial space
w = 4;                                                         %Polynomial degree
p = poly_space(w,'HC');                                        %Polynomial space
p_dim = size(p, 1);                                            %Cardinality of polynomial space

t  = linspace(0,T,N+1)';                                       %Generating t_0, t_1, ..., t_{N-1}
Tt = reshape(repmat(t,1,M)', (N+1)*M,1);                       %Replicating M times each t_n and saving as a column vector [t_0 t_0 ... t_0, t_1, ..., t_1,..., t_{N-1}, ..., t_{N_1}]'
Xx = reshape(squeeze(pagemtimes(P1,u))', (N+1)*M,1);           %Projected Samples saved as a column vector [X_0^(1),..., X_0^(M), X_1^(1),..,X_1^(M), ..., X_{N-1}^(1),...,X_{N-1}^(M)]
tX = [Tt Xx];                                                  %Two column vectors of size N*M for [t_n, X_{t_n}]
f  = reshape(squeeze(pagemtimes(P1,a(u)))',(N+1)*M,1);         %Given f function, we are going to solve

D     = x2fx(tX, p);                                           %This function helps to create Psi matrix each column is non-orth. basis function psi_p for different given p
[Q,R] = modified_GS(D);                                        %Apply modified Gram-Schmidt process to get QR decomposition
a_p   = Q'*f;                                                  %Solve Normal equations based on orthonormalised basis

psy     = @(t,s) x2fx([t s], p);                               %Non-orth. basis function
psy_bar = @(t,s) x2fx([t s], p)/R;                             %Orth. basis function
a_bar   = @(t,s) psy_bar(t,s)*a_p;                             %Approximation to the drift function a(x)
b_bar   = @(s)   sqrt(P1*(b*b')*P1')*ones(size(s));            %Approximation to the diffusion function b(x)
%%
figure
plot3(Xx,Tt, f, 'k*')
hold on
xplot=linspace(-2,K+2,51);
aplot=zeros(length(t), length(xplot));
[tt,xx]=ndgrid(t,xplot);
for i=1:length(t)
    for j=1:length(xplot)
        aplot(i,j)=a_bar(t(i),xplot(j));
    end
end
surf(xx,tt, aplot)
grid on
xlabel('t', 'fontsize', 18)
ylabel('x','fontsize', 18)
zlabel('$\bar{a}(x,t)$', 'interpreter', 'latex','fontsize', 18)
title(['$\mathcal{K}=$' num2str(K), ', ', '$\hat{\alpha}=$', num2str(alpha_hat), ', ', 'HC=4'],'interpreter', 'latex','fontsize', 22)
hold off
%% to check Mark Proj performance plot a and a_bar
% t=[0:dt:T];
% for i=1:N+1
%     aL2(i)=a_bar(t(i),P1*mean(u(:,i,:),3));
% end
% plot(t, aL2, 'r', t, P1*mean(a(u),3),'b', 'LineWidth', 3)
% xlabel('t')
% ylabel('u_1')
% legend('$\bar{a}(t,mean(u_1))$', '$mean(a(u_1))$', 'interpreter', 'latex','fontsize', 18)
% title('T=1')
% pause()
%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = mu(1)-2;                      % Lower end of discretization interval
xU = K;                       % Upper end of discretization interval
dxPde = 0.01;                 % Space step in PDE    
dtPde = dxPde^2;              % Time step in PDE
%dtPde = dxPde/2;              % Time step in PDE
Nx = round((xU-xL)/dxPde);    % Number of space steps in PDE
Nt = round(T/dtPde);          % Number of time steps in PDE
x  = linspace(xL,xU,Nx+1)';   % Space grid points

 a_barTmdt=@(x) a_bar(T-dt, x);

%[~,~,vErik] = tabulate_IC(a_barTmdt,b_bar,dt,K,xL,xU,false,Nx+1); %KBE solution at T-dt obtained via numerical optimization (using Erik's code)

%%
smf=10;

%KBE solution for all time
[tW, xW, v] = KBEsolverForAllt(x,T,a_bar,b_bar,Nt,Nx,K,smf); %PDE solver via CN backward scheme using exponential smoothing of Ind.Fun in a final condition
%[tW, xW, u] = HJBsolverForAllt_CN(x,T,a_bar,b_bar,Nt,Nx,K,smf);


%%
figure(1)
plot(xW, v(:,1), '-b', xW, vErik(2:end)', '--k', 'Linewidth',2) % xW, exp(-u(:,1)), '-r',
xlabel('s', 'fontsize', 14)
ylabel('u(s,0)','fontsize', 14)

[Xw,Tw] = ndgrid(xW,tW);
figure
p0=surf(Xw, Tw, v);
set(p0,'LineStyle','none')
xlabel('x','fontsize', 18)
ylabel('t','fontsize', 18)
%zlabel('u(x,t)','fontsize', 20)
title('KBE solution: $K=13, \sigma=0.5$', 'fontsize', 22, 'interpreter', 'latex')

%pause()
%% rho_0 CONTROL
rho_x0 = @(x, m, sig) exp(-(x-m).^2/(2*sig^2))/(sqrt(2*pi*sig^2)); %1D Gaussian density
rhoTilde_x0 = [rho_x0(xW, P1*mu, sqrt(P1*Sigma*P1')).*v(:,end); rho_x0((K:dxPde:K+100*dxPde)', P1*mu, sqrt(P1*Sigma*P1'))];
NC = trapezoidal(rhoTilde_x0, dxPde); %normalising constant
rhoTilde_x0=rhoTilde_x0./NC;          %optimal IS density

%fit optimal IS denisty to Gaussian
x=[xW;(K:dxPde:K+100*dxPde)'];
mFit = trapezoidal(x.*rhoTilde_x0, dxPde)
sigFit = sqrt(trapezoidal(x.^2.*rhoTilde_x0, dxPde)-mFit^2)
rhoFit_x0 = rho_x0(x, mFit, sigFit);

%Plot the densities
figure(1)
plot(x, rho_x0(x, P1*mu, sqrt(P1*Sigma*P1')), '-r',x, rhoTilde_x0, '-b',x, rhoFit_x0, '--b', 'Linewidth',3)
hold on
y = ylim; % current y-axis limits
plot([K K],[y(1) y(2)], '-k', 'LineWidth', 2)
hold off
title('DW process',  'fontsize',22)
xlabel('x','fontsize',18)
%legend({['$\rho_{x_0} \sim N($' num2str(mu_0), ', ',num2str(sigma_0) ')'];'$ \tilde{\rho}_{x_0}^{PDE}$'; ['$\tilde{\rho}_{x_0}^{fit}\sim N$(' num2str(mFit,3) ', ' num2str(sigFit,2) ')']; ['K=' num2str(K)]},'fontsize',22,'interpreter','latex')

%%
S=10^5;
%Conditional Gaussian rho_0(v_2,...,v_d|v_1) 
v1Smpl=mFit+sigFit*randn(1,S);
% mCond=mu(2:end)+corr*(v1Smpl-P1*mu)/sqrt(P1*Sigma*P1');
% sigCond=(1-corr^2)*Sigma(2:end,2:end);
mCond = mu(2:end)+ Sigma(2:d,1)/Sigma(1,1)*(v1Smpl-mu(1));
sigCond = Sigma(2:d, 2:d) - Sigma(2:d,1)/Sigma(1,1)*Sigma(1,2:d);

%P1=[0 .. 1]case
%mCond = mu(1:end-1)+ Sigma(1:end-1,end)/Sigma(end,end)*(v1Smpl-mu(end));
%sigCond = Sigma(1:end-1, 1:end-1) - Sigma(1:end-1,end)/Sigma(end,end)*Sigma(end,1:end-1);

%% dW CONTROL
% gradlog1d=zeros(length(xW),length(tW));
% gradlog1d(2:end-1,:) = (log(v(3:end,:))-log(v(1:end-2,:)))./(2*dxPde);
% gradlog1d(1,:) = (log(v(2,:))-log(v(1,:)))./dxPde;
% gradlog1d(end,:) = (log(v(end,:))-log(v(end-1,:)))./dxPde;

%obtained via KBE solution on PDE grids
ksiGridded = zeros(length(xW),length(tW));
ksiGridded(2:end-1,:) = sigma*(log(v(3:end,:))-log(v(1:end-2,:)))./(2*dxPde);
ksiGridded(1,:) = sigma*(log(v(2,:))-log(v(1,:)))./dxPde;
ksiGridded(end,:) = sigma*(log(v(end,:))-log(v(end-1,:)))./dxPde;

% %obtained via HJB solution on PDE grids
% ksiGriddedHJB = zeros(length(xW),length(tW));
% ksiGriddedHJB(2:end-1,:) = -sigma*(u(3:end,:)-u(1:end-2,:))./(2*dxPde);
% ksiGriddedHJB(1,:) = -sigma*(u(2,:)-u(1,:))./dxPde;
% ksiGriddedHJB(end,:) = -sigma*(u(end,:)-u(end-1,:))./dxPde;

[Xw,Tw] = ndgrid(xW,tW); %meshgrid for the interpolant
% figure(3)
% p0=surf(Xw, Tw, v);
% set(p0,'LineStyle','none')

cropN=round(0.1*Nx)+1; %if we use KBE, I crop 10% of the left baoundary and linearly smooth it to avoid instability on the left handside; for HJB no need to do this and nCrop=1
cropT=round(0*Nt)+1;
Ff=griddedInterpolant(Xw(cropN:end,cropT:end),flip(Tw(cropN:end,cropT:end),2), flip(real(ksiGridded(cropN:end,cropT:end)),2),'nearest','nearest');
ksi = @(x,t) Ff(x,t).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function for any x and t

%Optimal control using HJB solution
% Ff = griddedInterpolant(Xw,flip(Tw,2), flip(ksiGriddedHJB,2),'linear','linear');
% ksi = @(x,t) Ff(x,t).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function for any x and t
%ksi = @(x,t) 0;
% %Plot the optimal control on the PDE meshgrid
figure(5)
p1=surf(Xw, Tw, ksiGridded);
set(p1,'LineStyle','none')
%
% %Plot the extrapolated optimal control
figure(6)
xF  = linspace(xL-5,K+3,Nx+1)'; 
[XwF,TwF] = ndgrid(xF,tW);
p2=surf(XwF, TwF, ksi(XwF,TwF));
set(p2,'LineStyle','none')

%%
%Plot KBE solution at different time in 2D
tt=[T-dtPde 0.9 0.8 0.5 0];
C = {'r','m', 'g', 'k', 'y'}; 
for i=1:length(tt)
    figure(7)
    tfix=find(tW==tt(i));
    %semilogy(xW, exp(-u(:,tfix)), 'LineWidth', 2)
    plot(xW, v(:,tfix), 'LineWidth', 2)
    hold on
end
grid on
legend('t=T-dt', 't=0.9T', 't=0.8T', 't=0.5T', 't=0')
xlabel('x')
ylabel('u(t,x)')
title('KBE solution at different time')

%Plot optimal control at different time in 2D
ksiInterp=ksi(Xw,Tw);
for i=1:length(tt)
    figure(8)
    tfix=find(tW==tt(i));
    plot(xW, ksiInterp(:,tfix), 'LineWidth', 2)
    hold on
end
grid on
legend('t=T-dt','t=0.9T', 't=0.8T', 't=0.5T', 't=0')
xlabel('x')
ylabel('ksi(t,x)')
hold off
title('Optimal Control at different time')
%% Computing the QoI with and without change of measure wrt W and rho_0
S=10^5; %MC sample size

%Initialization
h_IS=zeros(1,S);
h_MC=zeros(1,S);
u_MC=zeros(d,N+1);
u_IS=u_MC;
%u0=mu*ones(1,S);%+chol(Sigma)'*randn(d,S);
u0=mu+chol(Sigma)'*randn(d,S);
%u0_shifted=[v1Smpl; mCond+chol(sigCond)'*randn(d-1,S)];
u0_shifted=u0;
t=linspace(0,T,N+1);
meanL=0;
parfor s=1:S
       u_MC=u0(:,s);
       u_IS=u0_shifted(:,s);
       L=1;
       k=1;

        
        while k<=N && P1*u_IS(:,k)<K
   
        dW = sqrt(dt).*randn(d,1);
        u_IS(:,k+1)= u_IS(:,k)+(lorenz96(u_IS(:,k),F)+P1'*sigma*ksi(P1*u_IS(:,k),t(k)))*dt+sigma*dW; %Forward Euler 
       
        %u_IS(:,k+1)= u_IS(:,k)+rk4(u_IS(:,k),dt,F)+P1'*sigma*ksi(P1*u_IS(:,k),t(k))*dt+b.*dW; %Runge-Kutta 

        L=L*exp(-0.5*dt*(ksi(P1*u_IS(:,k),t(k)))^2-ksi(P1*u_IS(:,k),t(k))*P1*dW);
        k=k+1;
        end
    
    
    for n=1:N
         dW = sqrt(dt).*randn(d,1);
         u_MC(:,n+1)= u_MC(:,n)+lorenz96(u_MC(:,n),F)*dt+sigma*dW;

         %u_MC(:,n+1)= u_MC(:,n)+rk4(u_MC(:,n),dt,F)+b.*dW;

    end

    meanL=meanL+L;
    h_IS(s)=(max(P1*u_IS)>=K)*L;
    h_MC(s)=(max(P1*u_MC)>=K);
    runmax_W(s)=max(P1*u_IS);
    runmax_MC(s)=max(P1*u_MC);
end
meanL=meanL/S
%Weight = rho_x0(P1*u0_shifted,P1*mu,sqrt(P1*Sigma*P1'))./rho_x0(P1*u0_shifted, mFit, sigFit);
Weight = ones(1,S);
alpha_hat_IS_W=mean(h_IS.*Weight)
alpha_hat_MC=mean(h_MC)
Vaprx_IS_W=var(h_IS.*Weight)
Vaprx_MC=var(h_MC)

%%
%[f_both,xboth] = ksdensity(runmax_IS, 'function', 'pdf');
[f_IS_W,x_W] = ksdensity(runmax_W,'function', 'pdf'); 
%[f_IS_rho0,x_rho0] = ksdensity(runmax_IS_rho0,'function', 'pdf'); 
[f_MC,x_MC] = ksdensity(runmax_MC, 'function', 'pdf'); 
figure
plot(x_MC, f_MC, 'r-', x_W, f_IS_W, 'c--', 'Linewidth',4);
%plot(x_MC, f_MC, 'r-', x_rho0, f_IS_rho0, 'b:', x_W, f_IS_W, 'c--',xboth, f_both, 'k-.', 'Linewidth',4);
% % hold on
% % plot([K K],[y(1) y(2)], '-k', 'LineWidth', 4)
% % hold off
% xlabel('u', 'fontsize', 18)
% [~, hobj, ~, ~] =legend('MC', 'IS wrt \rho_0', 'IS wrt W(t)', 'IS wrt both','fontsize',25)
% ylabel('density function of QoI','fontsize', 18)
% title('$\sigma_0=1, \mathcal{K}=2$','interpreter', 'latex','fontsize', 20)
% pp = get(gca,'XTickLabel');
% set(gca,'XTickLabel',pp,'FontName','Times','fontsize',20)
% hl = findobj(hobj,'type','line');
% set(hl,'LineWidth',2.5);
% set(gca,'FontSize',40)
% RelError_MC=1.96*sqrt(1-alpha_hat_IS_W)/sqrt(alpha_hat_IS_W*S)
% RelError_IS=1.96*sqrt(Vaprx_IS_W)/(alpha_hat_IS_W*sqrt(S))
% varRatio_approx=(RelError_MC/RelError_IS)^2
% varRatio_exact=Vaprx_MC/Vaprx_IS_W

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %% Computing the QoI with and without change of measure wrt W and rho_0
% %S=10^4; %MC sample size
% 
% %Initialization
% h_IS=zeros(1,S);
% h_MC=zeros(1,S);
% u_MC=zeros(d,N+1);
% u_IS=u_MC;
% u0=mu+chol(Sigma)'*randn(d,S);
% u0_shifted=[v1Smpl; mCond+chol(sigCond)'*randn(d-1,S)];
% t=linspace(0,T,N+1);
% meanL=0;
% 
% for s=1:S
%        u_MC=u0(:,s);
%        u_IS=u0_shifted(:,s);
%        L=1;
%     for k=1:N
%         dW = sqrt(dt).*randn(d,1);
%         u_MC(:,k+1)= u_MC(:,k)+lorenz96(u_MC(:,k),F,P1)*dt+sigma*dW;
%         for n=1:d
%         u_IS(n,k+1)= u_IS(n,k)+(lorenz96(u_IS(n,k),F,P1)+sigma*ksi(u_IS(n,k),t(k)))*dt+sigma*dW(n); %Forward Euler
%         L=L*exp(-0.5*dt*(ksi(u_IS(n,k),t(k)))^2-ksi(u_IS(n,k),t(k))*dW(n));
%         end
%     end
%     meanL=meanL+L;
%     h_IS(s)=(max(P1*u_IS)>=K)*L;
%     h_MC(s)=(max(P1*u_MC)>=K);
% end
% meanL=meanL/S
% Weight = rho_x0(P1*u0_shifted,P1*mu,sqrt(P1*Sigma*P1'))./rho_x0(P1*u0_shifted, mFit, sigFit);
% alpha_hat_IS_W=mean(h_IS.*Weight)
% alpha_hat_MC=mean(h_MC)
% Vaprx_IS_W=var(h_IS.*Weight)
% Vaprx_MC=var(h_MC)
% RelError_MC=1.96*sqrt(1-alpha_hat_IS_W)/sqrt(alpha_hat_IS_W*S)
% RelError_IS=1.96*sqrt(Vaprx_IS_W)/(alpha_hat_IS_W*sqrt(S))
% varRatio_approx=(RelError_MC/RelError_IS)^2
% varRatio_exact=Vaprx_MC/Vaprx_IS_W


%% Bootstrapping for PDE
if isempty(gcp)
    parpool;
end
opt = statset('UseParallel',true);
B=10^4;
conf=95;% in percent
stats_PDE = bootstrp(B, @(x) [mean(x) std(x)], h_IS.*Weight, 'Options', opt);
SmplStd_PDE = mean(stats_PDE(:,2))
CId_pde = prctile(stats_PDE(:,2), (100-conf)/2)
CIu_pde=  prctile(stats_PDE(:,2), (100-(100-conf)/2))
confIntlength = CIu_pde-CId_pde

% %% MULTILEVEL CROSS-ENTROPY METHOD
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %  We consider only mean shifting but fixing the covariance
% beta=0.01;     %CE method parameter
% M=10^5;
% % Multilevel procedure to find the optimal mu_tilde
% Weight = ones(1,M);
% sMbar=sort(Mbar);
% K_ell=sMbar(1, ceil((1-beta)*M));
% K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
% h=(Mbar>=K_ell);
% Sigma_tilde=Sigma;
% ell=1;
% u0=mu+chol(Sigma)'*randn(d,M);
% 
% while K_ell<K
%     mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight);
%     
%     ell=ell+1;
%     
%     for m=1:M
%         u(:,1,m)=mu_tilde+chol(Sigma_tilde)'*randn(d,1);
%         u0(:,m)=u(:,1,m);
%         for n=2:N+1
%             u(:,n,m)=u(:,n-1,m)+lorenz96(u(:,n-1,m),F,P1)*dt+P1'*sigma.*sqrt(dt).*randn(d,1);
%         end
%         Mbar(m)=max(P1*u(:,:,m));
%     end
% 
%     sMbar=sort(Mbar);
%     K_ell=sMbar(1, ceil((1-beta)*M));
%     K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
%     h=(Mbar>=K_ell);
%   
%     Weight=mvnpdf(u0', mu', Sigma)'./mvnpdf(u0',mu_tilde',Sigma_tilde)';
% end
% 
% if K_ell==K
%     mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight)
% end
% %% Computing the QoI with parameters obtained by multilevel CE approach
% %mu_tilde=mu       %Crude MC setting to compare
% for n=1:2
% M=10^5;
% h=zeros(1,M);
% u0=mu_tilde+chol(Sigma_tilde)'*randn(d,M);
% 
% parfor s=1:M
%     u=u0(:,s);  
%     for k=1:N
%         u(:,k+1)=u(:,k)+lorenz96(u(:,k),F,P1)*dt+P1'*sigma.*sqrt(dt).*randn(d,1);
%     end    
%     h(s)=(max(P1*u(:,:))>=K);
% end
% Weight=mvnpdf(u0', mu', Sigma)'./mvnpdf(u0',mu_tilde',Sigma_tilde)';
% alpha_hat_CE=mean(h.*Weight)
% Vaprx_CE=var(h.*Weight)
% RelError_CE=1.96/sqrt(alpha_hat_CE*M)
% RelErrorIS_CE=1.96*sqrt(Vaprx_CE)/(alpha_hat_CE*sqrt(M))
% varRatio_CE=(RelError_CE/RelErrorIS_CE)^2
% 
% %% Bootstrapping for CE
% if isempty(gcp)
%     parpool;
% end
% opt = statset('UseParallel',true);
% B=10^4;
% conf=95;% in percent
% stats_CE = bootstrp(B, @(x) [mean(x) std(x)], h.*Weight, 'Options', opt);
% SmplStd_CE=mean(stats_CE(:,2))
% CId_ce = prctile(stats_CE(:,2), (100-conf)/2)
% CIu_ce=  prctile(stats_CE(:,2), (100-(100-conf)/2))
% confIntlength=CIu_ce-CId_ce
% end

function dv = rk4(v_old,h,F)
% v[t+1] = rk4(v[t],step)
 k1 = lorenz96(v_old,F);
 k2 = lorenz96(v_old+1/2.*h.*k1,F);
 k3 = lorenz96(v_old+1/2.*h.*k2,F);
 k4 = lorenz96(v_old+h.*k3,F);
 dv= 1/6*h*(k1+2*k2+2*k3+k4);
end
