clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for Lorenz 63 problem to test PDE method
%%% in performing IS technique: 
%-- dv1 = r(v2-v1)+sigma*dW ---------------%
%-- dv2 = sv1-v2-v1v3   ---------------%
%-- dv3 = v1v2-qv3      ---------------%
%-------------------------------------------------------------------------%
%%% in computing QoI: P(max_{0<=t<=T} P1*v(t) >K)

%Model parameters for Blocked Atmospheric model
d=6;                       %problem dimension
epsilon=0.0;              %constant diffusion parameter
%gamma=0; bb=2; bbeta=1/4; C=10^(-2); u_1_star=0.06; u_4_star=0.05;
gamma=0.2; bb=0.5; bbeta=1.25; C=10^(-1); u_1_star=0.95; u_4_star=-0.76095;
%gamma=1; bb=0.5; bbeta=5/4; C=10^(-1); u_1_star=9/2; u_4_star=-9/5;

tilde_gamma_1=gamma*4*sqrt(2)*bb/(pi*3);
tilde_gamma_2=gamma*8*sqrt(2)*bb/(pi*15);
alpha_1=8*sqrt(2)*bb^2/(pi*3*(bb^2+1));
alpha_2=32*sqrt(2)*(bb^2+3)/(15*pi*(bb^2+4));
beta_1=bbeta*bb^2/(bb^2+1);
beta_2=bbeta*bb^2/(bb^2+4);
gamma_1=gamma*sqrt(2)*bb*4/(pi*3*(bb^2+1));
gamma_2=gamma*sqrt(2)*bb*32/(pi*15*(bb^2+4));
delta_1=64*sqrt(2)*bb^2/(15*pi*(bb^2+1));
delta_2=64*sqrt(2)*(bb^2-3)/(15*pi*(bb^2+4));
eta=16*sqrt(2)/(5*pi);

% tilde_gamma_1=gamma*4*sqrt(2)*bb/(pi*3);
% tilde_gamma_2=gamma*8*sqrt(2)*bb/(pi*15);
% alpha_1=8*sqrt(2)*bb^2/(pi*3*(bb^2+1));
% alpha_2=32*sqrt(2)*(bb^2)/(5*pi*(bb^2+4));
% beta_1=bbeta*bb^2/(bb^2+1);
% beta_2=bbeta*bb^2/(bb^2+4);
% gamma_1=gamma*sqrt(2)*bb*4/(pi*3*(bb^2+1));
% gamma_2=gamma*sqrt(2)*bb*24/(pi*15*(bb^2+4));
% delta_1=64*sqrt(2)*bb^2/(15*pi*(bb^2+1));
% delta_2=64*sqrt(2)*(bb^2-3)/(15*pi*(bb^2+4));
% eta=16*sqrt(2)/(5*pi);

a = @(u) [tilde_gamma_1*u(3,:,:)-C*(u(1,:,:)-u_1_star); 
    -(alpha_1*u(1,:,:)-beta_1).*u(3,:,:)-C*u(2,:,:)-delta_1*u(4,:,:).*u(6,:,:);
    (alpha_1*u(1,:,:)-beta_1).*u(2,:,:)-gamma_1*u(1,:,:)-C*u(3,:,:)+delta_1*u(4,:,:).*u(5,:,:);
    tilde_gamma_2*u(6,:,:)-C*(u(4,:,:)-u_4_star)+eta*(u(2,:,:).*u(6,:,:)-u(3,:,:).*u(5,:,:));
    -(alpha_2*u(1,:,:)-beta_2).*u(6,:,:)-C*u(5,:,:)-delta_2*u(3,:,:).*u(4,:,:); 
    (alpha_2*u(1,:,:)-beta_2).*u(5,:,:)-gamma_2*u(4,:,:)-C*u(6,:,:)+delta_2*u(2,:,:).*u(4,:,:)];
    
b = sqrt(2*epsilon)*ones(6,1);
%b = 8*10^(-3)*ones(6,1);
%b=1.6*10^(-2)*ones(6,1);
I=eye(d);

%Simulation parameters
T=1000;         %final time (simulation length)
M=10^0;      %Monte Carlo sample size
N=100000;      %number of timesteps in SDE
dt=T/N;      %discretization step in SDE

%% Initial original density parameters 
%mu=[0.0496 0.0150 -0.0100 0.0225 0.0168 -0.0029]';
%mu=[0.927796,0.160574,-0.0969953,-0.672758,-0.118184,0.214505]';
%mu=[0.696420985829880, 0.093870631839434, -0.419688104567451, -0.291633128173216, 0.007263117778186, 0.355939091583886]';
mu=[0.765041151259315, 0.228824892863142, -0.299091249463907, -0.365730340465999, -0.163693969763922, 0.310840305684531]';
Sigma=0.0025*I;
%Sigma=0*I;
%Define the Projector which component to track
P1    = I(1,:); %1st component

%% Generate samples of Lorenz 63 dynamics to use for L2 regression
close 
u=zeros(d,N,M);
t=linspace(0,T,N+1);
Mbar=zeros(1,M);
for m=1:M
    u(:,1,m)=mu+chol(Sigma)'*randn(d,1);
    for n=1:N       
        u(:,n+1,m)=u(:,n,m)+a(u(:,n,m))*dt+b.*sqrt(dt).*randn(6,1);%EM time-stepping    
    end 
 figure(13)
 plot(t, u(1,:,m), 'LineWidth', 2)
 xlabel('t', 'fontsize', 28)
 ylabel('u_1', 'fontsize', 28)
 title('b=0.01','fontsize', 28)
 hold on
    Mbar(m)=max(P1*u(:,:,m));
end
% %%
% %u=zeros(d,N,M);
% v=zeros(d,1);
% t=linspace(0,T,N+1);
% Mbar=zeros(1,M);
% K=0.77;
% for m=1:M
%     u(:,1,m)=mu+chol(Sigma)'*randn(d,1);
%     
%     n=1;
%     while n<=N && P1*u(:,n,m)<K  
%         
%         u(:,n+1,m)=u(:,n,m)+a(u(:,n,m))*dt+b.*sqrt(dt).*randn(6,1);%EM time-stepping 
% 
%         exitprob=exp(-2*max(K-P1*u(:,n,m),0)*max(K-P1*u(:,n+1,m),0)/(P1*b.*dt));
%         if rand<exitprob 
%             u(1,n+1,m)=K; %manually set, in the case of P1=[1 0 0], later I must make it automatic for any projection
%         end
%         n=n+1;
%     end 
% 
%     Mbar(m)=max(P1*u(:,:,m));
% end

%% Plotting marginal and joint densities at final time 
x=squeeze(u(1,end,:));
y=squeeze(u(2,end,:));
z=squeeze(u(3,end,:));
figure
scatterhist(x,y,'Kernel','on','Location','NorthEast',...
    'Direction','out','Color','k','LineStyle',{'-'},...
    'LineWidth',[2],'Marker','o','MarkerSize',[4]);
figure
scatterhist(x,z,'Kernel','on','Location','NorthEast',...
    'Direction','out','Color','k','LineStyle',{'-'},...
    'LineWidth',[2],'Marker','o','MarkerSize',[4]);
figure
scatterhist(y,z,'Kernel','on','Location','NorthEast',...
    'Direction','out','Color','k','LineStyle',{'-'},...
    'LineWidth',[2],'Marker','o','MarkerSize',[4]);
%%
figure
plot3(u(1,:,1),u(2,:,1),u(3,:,1))
figure
plot(t, u(1,:,1))
%% Thresholds corresponding to O(1e-3) probability
K=1.2;
alpha_hat=mean(Mbar>=K); %QoI

%% L2 regression
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We approximate only the drift, the diffusion is constant

%Define the Polynomial space
w = 2;                                                         %Polynomial degree
p = poly_space(w,'HC');                                        %Polynomial space
p_dim = size(p, 1);                                            %Cardinality of polynomial space

t  = linspace(0,T,N+1)';                                       %Generating t_0, t_1, ..., t_{N-1}
Tt = reshape(repmat(t,1,M)', (N+1)*M,1);                       %Replicating M times each t_n and saving as a column vector [t_0 t_0 ... t_0, t_1, ..., t_1,..., t_{N-1}, ..., t_{N_1}]'
Xx = reshape(squeeze(pagemtimes(P1,u))', (N+1)*M,1);           %Projected Samples saved as a column vector [X_0^(1),..., X_0^(M), X_1^(1),..,X_1^(M), ..., X_{N-1}^(1),...,X_{N-1}^(M)]
tX = [Tt Xx];                                                  %Two column vectors of size N*M for [t_n, X_{t_n}]
f  = reshape(squeeze(pagemtimes(P1,a(u)))',(N+1)*M,1);         %Given f function, we are going to solve

D     = x2fx(tX, p);                                           %This function helps to create Psi matrix each column is non-orth. basis function psi_p for different given p
[Q,R] = modified_GS(D);                                        %Apply modified Gram-Schmidt process to get QR decomposition
a_p   = Q'*f;                                                  %Solve Normal equations based on orthonormalised basis

psy     = @(t,s) x2fx([t s], p);                               %Non-orth. basis function
psy_bar = @(t,s) x2fx([t s], p)/R;                             %Orth. basis function
a_bar   = @(t,s) psy_bar(t,s)*a_p;                             %Approximation to the drift function a(x)
%b_bar   = @(s)   sqrt(P1*(b*b')*P1')*ones(size(s));           %Approximation to the diffusion function b(x)
b_bar   = @(s)   sqrt(P1*(b*b')*P1')*ones(size(s));            %Approximation to the diffusion function b(x)
%% MULTILEVEL CROSS-ENTROPY METHOD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  We consider only mean shifting but fixing the covariance
beta=0.001;  %CE method parameter
M=10^6;     %Monte Carlo sample size
N=100;      %Timestep size in SDE

% Multilevel procedure to find the optimal mu_tilde
Weight = ones(1,M);
sMbar=sort(Mbar);
K_ell=sMbar(1, ceil((1-beta)*M));
K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
h=(Mbar>=K_ell);
Sigma_tilde=Sigma;
ell=1;
u0=mu+chol(Sigma)'*randn(d,M);

while K_ell<K
    mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight);
    
    ell=ell+1;
    for m=1:M
        u(:,1,m)=mu_tilde+chol(Sigma_tilde)'*randn(d,1);
        u0(:,m)=u(:,1,m);
        n=2;
        while n<=N && P1*u(:,n,m)<K
            
            u(:,n+1,m)=u(:,n,m)+a(u(:,n,m))*dt+b.*sqrt(dt).*randn(6,1);%EM time-stepping
            
            exitprob=exp(-2*max(K-P1*u(:,n,m),0)*max(K-P1*u(:,n+1,m),0)/(P1*b.*dt));
            if rand<exitprob
                u(1,n+1,m)=K; %manually set, in the case of P1=[1 0 0], later I must make it automatic for any projection
            end
            n=n+1;
        end
        Mbar(m)=max(P1*u(:,:,m));
    end
    
    sMbar=sort(Mbar);
    K_ell=sMbar(1, ceil((1-beta)*M));
    K_ell=K_ell*(K_ell<K)+K*(K_ell>=K)
    h=(Mbar>=K_ell);
    
    Weight=mvnpdf(u0', mu', Sigma)'./mvnpdf(u0',mu_tilde',Sigma_tilde)';
end

if K_ell==K
    mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight)
end


% %%
% figure
% fsurf(a_bar, [0 T 0.05 K])
% hold on
% plot3(Tt, Xx, f, 'k*')
% xlabel('t','fontsize', 18)
% ylabel('x','fontsize', 18)
% zlabel('$\bar{a}(x,t)$', 'interpreter', 'latex','fontsize', 18)
% title(['$\mathcal{K}=$' num2str(K), ', ', '$\sigma=$', num2str(sigma), ', ', 'HC=', num2str(w)],'interpreter', 'latex','fontsize', 22)
%% residual
% figure
% plot3(Tt,Xx, f_b-b_bar(Tt,Xx), 'r')
% xlabel('t','fontsize', 18)
% ylabel('x','fontsize', 18)
% zlabel('$P_1a(x,t)-\bar{a}(x,t)$', 'interpreter', 'latex','fontsize', 18)
% title('Residual over t')
% view([0 360])
% figure
% plot3(Tt,Xx,f-a_bar(Tt,Xx), 'r')
% xlabel('t','fontsize', 18)
% ylabel('x','fontsize', 18)
% zlabel('$P_1a(x,t)-\bar{a}(x,t)$', 'interpreter', 'latex','fontsize', 18)
% title('Residual over x')
% view([90 360])
%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = 0;                 % Lower end of discretization interval
%xL = -3;                     % Lower end of discretization interval
xU = K;                       % Upper end of discretization interval
dxPde = 0.001;                % Space step in PDE    
dtPde = dxPde;              % Time step in PDE
Nx = round((xU-xL)/dxPde);    % Number of space steps in PDE
Nt = round(T/dtPde);          % Number of time steps in PDE
x  = linspace(xL,xU,Nx+1)';   % Space grid points

%a_barTmdt=@(x) a_bar(T-dt, x);
%[~,~,vErik] = tabulate_IC(a_barTmdt,b_bar,dt,K,xL,xU,false,Nx+1); %KBE solution at T-dt obtained via numerical optimization (using Erik's code)
%% Analytical solution for frozen coeff. at the corner
v_anal=@(x,t) 0.5*erfc((K-x-a_bar(T,K).*(T-t))./sqrt(2*b_bar(K).^2.*(T-t)))...
              +0.5*exp(a_bar(T,K).*(K-x)./b_bar(K).^2./2).*erfc((K-x+a_bar(T,K).*(T-t))./sqrt(2*b_bar(K).^2.*(T-t)));
% %% Analytical solution for frozen coeff. at the corner
% v_anal=@(x,t) 0.5*erfc((K-x-a_bar(T,K).*(T-t))./sqrt(2*b_bar(T,K).^2.*(T-t)))...
%               +0.5*exp(a_bar(T,K).*(K-x)./b_bar(T,K).^2./2).*erfc((K-x+a_bar(T,K).*(T-t))./sqrt(2*b_bar(T,K).^2.*(T-t)));

%% Finding a (red) region in the corner to use analytical solution for the pde solver
absdiff=cell(Nx+1, Nt+1);
t=linspace(T,0,Nt+1);
[Xw,Tw] = ndgrid(x,t);
[px, py]=gradient(v_anal(Xw,Tw));
gradNeeded={[dxPde dtPde]};
for i = 1:Nx+1
for j = 1:Nt+1
absdiff{i,j}= norm([px(i,j) py(i,j)]-[dxPde dtPde]);
end
end
[MinVal, ind]=min(cell2mat(absdiff),[], 'all', 'linear');
[idx{1:ndims(cell2mat(absdiff))}] = ind2sub(size(cell2mat(absdiff)), ind);
RedRegionIndx=[idx{:}];
x(RedRegionIndx(1))
t(RedRegionIndx(2))
px(idx{:}) %should be close to dxPde
py(idx{:}) %should be close to dtPde
%% KBE solution for all time using anal.sol
[tW, xW, v] = KBEsolverForAllt_AnalSol(x,T,a_bar,b_bar,Nt,Nx,K,RedRegionIndx); %PDE solver via CN backward scheme using analytical solution with frozen coeff. in the corner
%% Plot KBE solution for all t and x
[Xw,Tw] = ndgrid(xW,tW);
figure
p0=surf(Xw, Tw, v);
set(p0,'LineStyle','none')
xlabel('x','fontsize', 18)
ylabel('t','fontsize', 18)
zlabel('u(x,t)','fontsize', 20)

%% rho_0 CONTROL
rho_x0 = @(x, m, sig) exp(-(x-m).^2/(2*sig^2))/(sqrt(2*pi*sig^2)); %1D Gaussian density
rhoTilde_x0 = [rho_x0(xW, P1*mu, sqrt(P1*Sigma*P1')).*sqrt(v(:,end)); rho_x0((K:dxPde:K+100*dxPde)', P1*mu, sqrt(P1*Sigma*P1'))];
NC = trapezoidal(rhoTilde_x0, dxPde); %normalising constant
rhoTilde_x0=rhoTilde_x0./NC;          %optimal IS density

%fit optimal IS denisty to Gaussian
x=[xW;(K:dxPde:K+100*dxPde)'];
mFit      = trapezoidal(x.*rhoTilde_x0, dxPde)
sigFit    = sqrt(trapezoidal(x.^2.*rhoTilde_x0, dxPde)-mFit^2)
rhoFit_x0 = rho_x0(x, mFit, sigFit);

%Plot the densities
figure(1)
plot(x, rho_x0(x, P1*mu, sqrt(P1*Sigma*P1')), '-r',x, rhoTilde_x0, '-b',x, rhoFit_x0, '--b', 'Linewidth',3)
hold on
y = ylim; % current y-axis limits
plot([K K],[y(1) y(2)], '-k', 'LineWidth', 2)
hold off
title('DW process',  'fontsize',22)
xlabel('x','fontsize',18)
legend({['$\rho_{x_0} \sim N($' num2str(P1*mu), ', ',num2str(sqrt(P1*Sigma*P1')) ')'];'$ \tilde{\rho}_{x_0}^{PDE}$'; ['$\tilde{\rho}_{x_0}^{fit}\sim N$(' num2str(mFit,3) ', ' num2str(sigFit,2) ')']; ['K=' num2str(K)]},'fontsize',22,'interpreter','latex')

M=10^6;
%Conditional Gaussian rho_0(v_2,...,v_d|v_1) 
v1Smpl=mFit+sigFit*randn(1,M);
% mCond=mu(2:end)+corr*(v1Smpl-P1*mu)/sqrt(P1*Sigma*P1');
% sigCond=(1-corr^2)*Sigma(2:end,2:end);
mCond = mu(2:end)+ Sigma(2:d,1)/Sigma(1,1)*(v1Smpl-mu(1));
sigCond = Sigma(2:d, 2:d) - Sigma(2:d,1)/Sigma(1,1)*Sigma(1,2:d);
u0_rho0=[v1Smpl; mCond+chol(sigCond)'*randn(d-1,M)];

%% IS wrt both
%Note that the optimal denisty rhoJointTilde_0(x,v) \approx rho_0(x,v)sqrt(u_PDE(v,0))
% where rho_0(x,v) = rho_0(x|v)rho_0(v) and rho_0(v)*u_PDE(v,0) is fitted to 1D Gaussian

%Marginal denisty in v since we consider the projection P1=[0 1] and solved the KBE with respect to v
rhoTemp_both =[rho_x0(xW, P1*mu, sqrt(P1*Sigma*P1')).*v(:,end); rho_x0((K:dxPde:K+100*dxPde)', P1*mu, sqrt(P1*Sigma*P1'))];
%after x>K we have Gaussian
NC_both = trapezoidal(rhoTemp_both, dxPde); %normalising constant
rhoTemp_both=rhoTemp_both./NC_both;           %optimal IS density

%Fitting the rho_0(v)*u_PDE to Gaussian
x=[xW;(K:dxPde:K+100*dxPde)'];
mFit_both   = trapezoidal(x.*rhoTemp_both, dxPde)
sigFit_both = sqrt(trapezoidal(x.^2.*rhoTemp_both, dxPde)-mFit_both^2)
rhoTildeFit_0_both = rho_x0(x, mFit_both, sigFit_both);

%Conditional Gaussian rho_0(x|v) 
vSmpl_both=mFit_both+sigFit_both*randn(1,M);
mCond_both=mu(2:end)+ Sigma(2:d,1)/Sigma(1,1)*(v1Smpl-mu(1));
sigCond_both = Sigma(2:d, 2:d) - Sigma(2:d,1)/Sigma(1,1)*Sigma(1,2:d);
u0_both=[vSmpl_both; mCond_both+chol(sigCond_both)'*randn(d-1,M)];

%% dW CONTROL
%obtained via KBE solution on PDE grids
ksiGridded = zeros(length(xW),length(tW));
ksiGridded(2:end-1,:) = b'*P1'*(log(v(3:end,:))-log(v(1:end-2,:)))./(2*dxPde);
ksiGridded(1,:) = b'*P1'*(log(v(2,:))-log(v(1,:)))./dxPde;
ksiGridded(end,:) = b'*P1'*(log(v(end,:))-log(v(end-1,:)))./dxPde;

[Xw,Tw] = ndgrid(xW,tW); %meshgrid for the interpolant

cropN=round(0.1*Nx)+1; %if we use KBE, I crop 10% of the left baoundary and linearly smooth it to avoid instability on the left handside; for HJB no need to do this and nCrop=1
cropT=round(0*Nt)+1;
Ff=griddedInterpolant(Xw(cropN:end,cropT:end),flip(Tw(cropN:end,cropT:end),2), flip(real(ksiGridded(cropN:end,cropT:end)),2),'linear','linear');
ksiStar_PDE= @(x,t) Ff(x,t).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function based on the discrete optimal control values

ksiStar_Asmp= @(x,t) ((1/(b'*P1'))*((K-x)./(T-t))-a_bar(t,x)).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function based on asymptotic apprx.

ksiStar = @(x,t, dt_EM) sum([ksiStar_PDE(x,t).*(T-t>=100*dt_EM) ksiStar_Asmp(x,t)*(T-t<100*dt_EM)], 'omitnan'); %the factor 4 corresponds for the particular choice of dtPde=0.005/2 where ksigridded(x,t) with t<=0.96 smooth,t>0.96 it has a kink close to the barrier.

%% Computing the QoI with and without change of measure wrt W 
S=10^6; %MC sample size

%Initialization
h_IS=zeros(1,S);
h_MC=zeros(1,S);
h_CE=zeros(1,S);
h_rho0=zeros(1,S);
h=zeros(1,S);
u_MC=zeros(d,N+1);
u0_MC=mu+chol(Sigma)'*randn(d,S);
u0_CE=mu_tilde+chol(Sigma_tilde)'*randn(d,S);
u0_W=mu+chol(Sigma)'*randn(d,S);
t=linspace(0,T,N+1);
meanL=0;
dt_const=dt;
parfor i=1:S
       u_MC=u0_MC(:,i);
       u_CE=u0_CE(:,i);
       u_IS=u0_W(:,i);
       u_rho0=u0_rho0(:,i);
       u=u0_both(:,i);
   %IS wrt both 
       L_both=1;
       k=1;
       t_k=0;
       dt=dt_const;
    while t_k<=T && P1*u(:,k)<K && dt>0.00000000001
    dt=min(dt_const, 0.5*(T-t_k));
    dW = sqrt(dt).*randn(d,1);

    % 
    u(:,k+1)= u(:,k)+(a(u(:,k))+P1'.*b.*ksiStar(P1*u(:,k),t_k,dt_const))*dt+b.*dW; %Forward Euler 

    L_both=L_both*exp(-0.5*dt*(ksiStar(P1*u(:,k),t_k, dt_const))^2-ksiStar(P1*u(:,k),t_k,dt_const)*P1*dW);
    exitprob=exp(-2*max(K-P1*u(:,k),0)*max(K-P1*u(:,k+1),0)/(P1*b*dt));

        if rand<exitprob 
            t_k=t_k+0.5*dt ;
            u(1,k+1)=K; %manually set, in the case of P1=[1 0 0], later I must make it automatic for any projection
        else
            t_k=t_k+dt;
        end
    k=k+1;
    end
%     figure(1)
%     plot(u(1,:),'b', 'Linewidth',2)
%     L_both
%        pause()

     %IS wrt W  
       L=1;
       k=1;
       t_k=0;
       dt=dt_const;
    while t_k<=T && P1*u_IS(:,k)<K && dt>0.00000000001
    dt=min(dt_const, 0.5*(T-t_k));
    dW = sqrt(dt).*randn(d,1);

    % 
    u_IS(:,k+1)= u_IS(:,k)+(a(u_IS(:,k))+P1'.*b.*ksiStar(P1*u_IS(:,k),t_k,dt_const))*dt+b.*dW; %Forward Euler 
    
    L=L*exp(-0.5*dt*(ksiStar(P1*u_IS(:,k),t_k, dt_const))^2-ksiStar(P1*u_IS(:,k),t_k,dt_const)*P1*dW);
    exitprob=exp(-2*max(K-P1*u_IS(:,k),0)*max(K-P1*u_IS(:,k+1),0)/(P1*b*dt));

        if rand<exitprob 
            t_k=t_k+0.5*dt ;
            u_IS(1,k+1)=K; %manually set, in the case of P1=[1 0 0], later I must make it automatic for any projection
        else
            t_k=t_k+dt;
        end

    k=k+1;
    end
    
        %IS wrt rho0
        n=1;
        t_n=0;
    while t_n<=T && P1*u_rho0(:,n)<K 
         dW = sqrt(dt_const).*randn(d,1);
         u_rho0(:,n+1)= u_rho0(:,n)+a(u_rho0(:,n))*dt_const+b.*dW;
         
        q_MC= exp(-2*max(K-P1*u_rho0(:,n),0)*max(K-P1*u_rho0(:,n+1),0)/(P1*b*dt_const));
        if rand<q_MC 
        u_rho0(1,n+1)=K;  %manually set, in the case of P1=[1 0 0], later I must make it automatic for any projection
        end
        t_n=t_n+dt_const;
        n=n+1;
    end
    
    %CE
        n=1;
        t_n=0;
        while t_n<=T && P1*u_CE(:,n)<K
            dW = sqrt(dt_const).*randn(d,1);
            u_CE(:,n+1)= u_CE(:,n)+a(u_CE(:,n))*dt_const+b.*dW;
         
                   
            q_CE= exp(-2*max(K-u_CE(1,n),0)*max(K-u_CE(1,n+1),0)/(P1*b*dt_const));
            if rand<q_CE
            u_CE(1,n+1)=K;
            end
            t_n=t_n+dt_const;
            n=n+1;
        end

    %Monte Carlo simulation
        n=1;
        t_n=0;
    while t_n<=T && P1*u_MC(:,n)<K 
         dW = sqrt(dt_const).*randn(d,1);
         u_MC(:,n+1)= u_MC(:,n)+a(u_MC(:,n))*dt_const+b.*dW;
         
            q_MC= exp(-2*max(K-P1*u_MC(:,n),0)*max(K-P1*u_MC(:,n+1),0)/(P1*b*dt_const));
            if rand<q_MC 
            u_MC(1,n+1)=K;  %manually set, in the case of P1=[1 0 0], later I must make it automatic for any projection
            end
            t_n=t_n+dt_const;
            n=n+1;
    end
%      plot(u_rho0(1,:),'g','Linewidth',2)
%      hold on
%     plot(u_IS(1,:),'c','Linewidth',2)
%     hold on
%     plot(u_MC(1,:),'r','Linewidth',2)
%     i
%     pause()
    meanL=meanL+L;
    h_IS(i)=(max(P1*u_IS)>=K)*L;
    h_rho0(i)=(max(P1*u_rho0)>=K);
    h_MC(i)=(max(P1*u_MC)>=K);
    h_CE(i)=(max(P1*u_CE)>=K);
    h(i)=(max(P1*u)>=K)*L_both;

end
meanL=meanL/S
Weight_rho0= rho_x0(P1*u0_rho0,P1*mu,sqrt(P1*Sigma*P1'))./rho_x0(P1*u0_rho0, mFit, sigFit);
Weight_both = rho_x0(P1*u0_both,P1*mu,sqrt(P1*Sigma*P1'))./rho_x0(P1*u0_both, mFit_both, sigFit_both);
Weight_CE=mvnpdf(u0_CE', mu', Sigma)'./mvnpdf(u0_CE',mu_tilde',Sigma)';
alpha_hat_IS_both=mean(h.*Weight_both)
alpha_hat_IS_W=mean(h_IS)
alpha_hat_MC=mean(h_MC)
alpha_hat_CE=mean(h_CE.*Weight_CE)
alpha_hat_IS_rho0=mean(h_rho0.*Weight_rho0)
Vaprx_IS_both=var(h.*Weight_both)
Vaprx_IS_W=var(h_IS)
Vaprx_IS_rho0=var(h_rho0.*Weight_rho0)
Vaprx_MC=var(h_MC)
Vaprx_CE=var(h_CE.*Weight_CE)
RelError_CE=1.96*sqrt(Vaprx_CE)/(alpha_hat_CE*sqrt(S))
RelError_MC=1.96*sqrt(Vaprx_MC)/(alpha_hat_MC*sqrt(S))
RelError_IS_both=1.96*sqrt(Vaprx_IS_both)/(alpha_hat_IS_both*sqrt(S))
RelError_IS_W=1.96*sqrt(Vaprx_IS_W)/(alpha_hat_IS_W*sqrt(S))
RelError_IS_rho0=1.96*sqrt(Vaprx_IS_rho0)/(alpha_hat_IS_rho0*sqrt(S))
%varRatio_approx=(RelError_MC/RelError_IS)^2
varRatio_both=Vaprx_MC/Vaprx_IS_both
varRatio_W=Vaprx_MC/Vaprx_IS_W
varRatio_rho0=Vaprx_MC/Vaprx_IS_rho0
varRatio_CE=Vaprx_MC/Vaprx_CE

IS_both_CIu=alpha_hat_IS_both+1.96*sqrt(Vaprx_IS_both)/sqrt(S)
IS_both_CId=alpha_hat_IS_both-1.96*sqrt(Vaprx_IS_both)/sqrt(S)
IS_W_CIu=alpha_hat_IS_W+1.96*sqrt(Vaprx_IS_W)/sqrt(S)
IS_W_CId=alpha_hat_IS_W-1.96*sqrt(Vaprx_IS_W)/sqrt(S)
IS_rho0_CIu=alpha_hat_IS_rho0+1.96*sqrt(Vaprx_IS_rho0)/sqrt(S)
IS_rho0_CId=alpha_hat_IS_rho0-1.96*sqrt(Vaprx_IS_rho0)/sqrt(S)
MC_CIu=alpha_hat_MC+1.96*sqrt(Vaprx_MC)/sqrt(S)
MC_CId=alpha_hat_MC-1.96*sqrt(Vaprx_MC)/sqrt(S)
CE_CIu=alpha_hat_CE+1.96*sqrt(Vaprx_CE)/sqrt(S)
CE_CId=alpha_hat_CE-1.96*sqrt(Vaprx_CE)/sqrt(S)
%% Bootstrapping
close all
C = {'red','magenta'}; % Cell array of colros.
Cc={'green','blue'};
Ccc = {'cyan', 'yellow'};
Cccc = {'black', 'red'};

B=10^4;
conf=95;
Krng=[2 2.5 3 3.5];                     %the threshold DW

q_both=[h_both_K2; h_both_K25; h_both_K3; h_both_K35];
q_IS_W=[h_IS_W_K2; h_IS_W_K25; h_IS_W_K3; h_IS_W_K35];
q_IS_rho0=[h_IS_rho0_K2; h_IS_rho0_K25; h_IS_rho0_K3; h_IS_rho0_K35];
q_IS_CE=[h_IS_CE_K2; h_IS_CE_K25; h_IS_CE_K3; h_IS_CE_K35];

q_both_2=[h_both_K2_2; h_both_K25_2; h_both_K3_2; h_both_K35_2];
q_IS_W_2=[h_IS_W_K2_2; h_IS_W_K25_2; h_IS_W_K3_2; h_IS_W_K35_2];
q_IS_rho0_2=[h_IS_rho0_K2_2; h_IS_rho0_K25_2; h_IS_rho0_K3_2; h_IS_rho0_K35_2];
q_IS_CE_2=[h_IS_CE_K2_2; h_IS_CE_K25_2; h_IS_CE_K3_2; h_IS_CE_K35_2];

Q_both{1}=q_both;Q_both{2}=q_both_2;
Q_IS_W{1}=q_IS_W;Q_IS_W{2}=q_IS_W_2;
Q_IS_rho0{1}=q_IS_rho0;Q_IS_rho0{2}=q_IS_rho0_2;
Q_IS_CE{1}=q_IS_CE;Q_IS_CE{2}=q_IS_CE_2;

for n=1:2
        for i=1:length(Krng)
        K=Krng(i);
        if isempty(gcp)
           parpool;
        end
        opt = statset('UseParallel',true);

        stats_IS_both = bootstrp(B, @(x) [mean(x) std(x)], Q_both{n}(i,:), 'Options', opt);
        stats_IS_W = bootstrp(B, @(x) [mean(x) std(x)], Q_IS_W{n}(i,:), 'Options', opt);
        stats_IS_rho0 = bootstrp(B, @(x) [mean(x) std(x)], Q_IS_rho0{n}(i,:), 'Options', opt);
        stats_IS_CE = bootstrp(B, @(x) [mean(x) std(x)], Q_IS_CE{n}(i,:), 'Options', opt);

        SmplStd_IS_both(i)=mean(stats_IS_both(:,2));
        SmplStd_IS_W(i) =mean(stats_IS_W(:,2));
        SmplStd_IS_rho0(i) =mean(stats_IS_rho0(:,2));
        SmplStd_IS_CE(i) =mean(stats_IS_CE(:,2));

        CId_IS_both(i) =  prctile(stats_IS_both(:,2), (100-conf)/2);
        CIu_IS_both(i) =  prctile(stats_IS_both(:,2), (100-(100-conf)/2));

        CId_IS_W(i) = prctile(stats_IS_W(:,2), (100-conf)/2);
        CIu_IS_W(i) =  prctile(stats_IS_W(:,2), (100-(100-conf)/2));

        CId_IS_rho0(i) = prctile(stats_IS_rho0(:,2), (100-conf)/2);
        CIu_IS_rho0(i) =  prctile(stats_IS_rho0(:,2), (100-(100-conf)/2));
        
        CId_IS_CE(i) = prctile(stats_IS_CE(:,2), (100-conf)/2);
        CIu_IS_CE(i) =  prctile(stats_IS_CE(:,2), (100-(100-conf)/2));
        end
    figure(1)
    plot_ci(Krng',[SmplStd_IS_both; CId_IS_both; CIu_IS_both],'PatchColor', C{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', C{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', C{n}, 'YScale', 'log');
    grid on
    hold on
    figure(2)
    plot_ci(Krng',[SmplStd_IS_W; CId_IS_W; CIu_IS_W], 'PatchColor', Cc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Cc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Cc{n}, 'YScale', 'log');
    grid on
    hold on
    title('IS with W(t)')
    figure(3)
    plot_ci(Krng',[SmplStd_IS_rho0; CId_IS_rho0; CIu_IS_rho0], 'PatchColor', Ccc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Ccc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Ccc{n}, 'YScale', 'log');
    grid on
    hold on
    title('IS with \rho_0')
    figure(4)
    plot_ci(Krng',[SmplStd_IS_CE; CId_IS_CE; CIu_IS_CE], 'PatchColor', Cccc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Cccc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Cccc{n}, 'YScale', 'log');
    grid on
    hold on
    title('IS with CE')
    
    figure(5)
    plot_ci(Krng',[SmplStd_IS_both; CId_IS_both; CIu_IS_both],'PatchColor', C{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', C{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', C{n}, 'YScale', 'log');
    grid on
    hold on
    plot_ci(Krng',[SmplStd_IS_W; CId_IS_W; CIu_IS_W], 'PatchColor', Cc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Cc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Cc{n}, 'YScale', 'log');
    grid on
    hold on
    plot_ci(Krng',[SmplStd_IS_rho0; CId_IS_rho0; CIu_IS_rho0], 'PatchColor', Ccc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Ccc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Ccc{n}, 'YScale', 'log');
    grid on
    hold on
    plot_ci(Krng',[SmplStd_IS_CE; CId_IS_CE; CIu_IS_CE], 'PatchColor', Cccc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Cccc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Cccc{n}, 'YScale', 'log');
    grid on
    hold on
end
function dv = rk4(v_old,h,r,q,s)
% v[t+1] = rk4(v[t],step)
 k1 = lorenz63(v_old,r,q,s);
 k2 = lorenz63(v_old+1/2.*h.*k1,r,q,s);
 k3 = lorenz63(v_old+1/2.*h.*k2,r,q,s);
 k4 = lorenz63(v_old+h.*k3,r,q,s);
 dv= 1/6*h*(k1+2*k2+2*k3+k4);
end