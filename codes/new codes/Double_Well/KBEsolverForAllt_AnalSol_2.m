function [time,x,u] = KBEsolverForAllt_AnalSol(x,T1,T2,a,b,Nt,Nx,K, RedRegionIndx)
  time  = linspace(T1,T2,Nt+1);     %Timespace in descending order
  dx = x(2)-x(1);                 %spatial step size
  dt = time(1)-time(2);           %time step
  u=zeros(Nx+1,Nt+1);
  
  %Space and timesteps number for analytical solution
    Dx=Nx+1-RedRegionIndx(1);
    Dt=RedRegionIndx(2);
  %I.  Solution for (t,x) \in [T-\Delta t,T]x[K-\Delta x,K]: x1 = x(end-Dx:end) and t1 = time(1:1+Dt)
  
  for i=Nx+1-Dx:Nx+1
      for j=1:1+Dt
          if i==Nx+1 && j==1
              u(i,j)=1; %solution in the corner at (t,x) \in [T,K]
          else
              %analytical solution
              u(i,j)= 0.5*erfc((K-x(i)-a(T,x(Nx+1))*(T-time(j)))/sqrt(2*b(x(Nx+1)).^2.*(T-time(j))))...
              +0.5*exp(a(T,x(Nx+1))*(K-x(i))/b(x(Nx+1)).^2/2).*erfc((K-x(i)+a(T,x(Nx+1))*(T-time(j)))./sqrt(2*b(x(Nx+1)).^2.*(T-time(j))));         
          end
      end
  end
 
  %II. Solution for (t,x) \in [T-\Delta t,T]x(-Infty, K-\Delta x)
  Nx2=Nx-Dx;
  x2=x(1:Nx2+1);
  v2=u(Nx+1-Dx,1:1+Dt); %right boundary values at K-Dx
  
  for n=1:Dt
  t   = repmat(time(n),1,length(x2))';
  tt  = repmat(time(n+1),1,length(x2))';
  
  % Time stepping scheme (I+dt/2*A_n)u_{n} = (I-dt/2*A_{n+1})u_{n+1},
 
  % Below we create A_{n+1} tridiagonal matrix at time n+1
  ax  = a(t,x2);
  bx  = b(x2);
  
  % Sub-diagonal elements of A_{n+1}
  Am  = ax(3:Nx2)/dx/2-bx(3:Nx2).^2/dx^2/2;
  
  % Diagonal elements of A_{n+1}
  Ac    = bx(2:Nx2).^2/dx^2;
  Ac(1) = ax(2)/dx; %Linear extraploation
  %Ac(1)=ax(2)/dx/2+bx(2).^2/dx^2/2;%Neumann
  
  % Super-diagonal elements of A_{n+1}
  Ap    = -ax(2:Nx2-1)/dx/2-bx(2:Nx2-1).^2/dx^2/2;
  Ap(1) = -ax(2)/dx; %Linear extraploation
  %Ap(1) = -bx(2).^2/dx^2; %Exp. extraploation
  
  % Below we create A_n tridiagonal matrix
  ax_n  = a(tt,x2);
  
  % Sub-diagonal elements of A_n
  Am_n    = ax_n(3:Nx2)/dx/2-bx(3:Nx2).^2/dx^2/2;
  
  % Diagonal elements of A_n
  Ac_n    = bx(2:Nx2).^2/dx^2;
  Ac_n(1) = ax_n(2)/dx; %Linear extraploation
  %Ac_n(1) = ax_n(2)/dx/2+bx(2).^2/dx^2/2;%Neumann
  
  % Super-diagonal elements of A_n
  Ap_n    = -ax_n(2:Nx2-1)/dx/2-bx(2:Nx2-1).^2/dx^2/2;
  Ap_n(1) = -ax_n(2)/dx; %Linear extraploation
  %Ap_n(1) = -bx(2).^2/dx^2; %Exp. extraploation
  
  %Boundary condition at Nx+1
  bc    = ((ax_n(Nx2)/dx/2+bx(Nx2).^2/dx^2/2).*v2(n+1)+(ax(Nx2)/dx/2+bx(Nx2).^2/dx^2/2).*v2(n))*dt/2;
 
  % Matrix multiplying known solution values, at time t+dt
  Aexpl = spdiags(repmat([0,1,0],Nx2-1,1)-dt/2*[[Am;0],Ac,[0;Ap]],-1:1,Nx2-1,Nx2-1);
  % Matrix multiplying unknown solution values, at time t
  Aimpl = spdiags(repmat([0,1,0],Nx2-1,1)+dt/2*[[Am_n;0],Ac_n,[0;Ap_n]],-1:1,Nx2-1,Nx2-1);
  r = [zeros(1,Nx2-2)'; bc];
  
  u(2:Nx2,n+1) = Aimpl\(Aexpl*u(2:Nx2,n)+r); 
  end
  
  %III. Solution for (t,x) \in [0, T-\Delta t]x(\Infty,K)
 
  for n=Dt+1:Nt
  t   = repmat(time(n),1,length(x))';
  tt  = repmat(time(n+1),1,length(x))';
  
  % Time stepping scheme (I+dt/2*A_n)u_{n} = (I-dt/2*A_{n+1})u_{n+1},
 
  % Below we create A_{n+1} tridiagonal matrix at time n+1
  ax  = a(t,x);
  bx  = b(x);
  
  % Sub-diagonal elements of A_{n+1}
  Am  = ax(3:Nx)/dx/2-bx(3:Nx).^2/dx^2/2;
  
  % Diagonal elements of A_{n+1}
  Ac    = bx(2:Nx).^2/dx^2;
  Ac(1) = ax(2)/dx; %Linear extraploation
  %Ac(1)=ax(2)/dx/2+bx(2).^2/dx^2/2;%Neumann
  
  % Super-diagonal elements of A_{n+1}
  Ap    = -ax(2:Nx-1)/dx/2-bx(2:Nx-1).^2/dx^2/2;
  Ap(1) = -ax(2)/dx;%Linear extraploation
  %Ap(1) = -bx(2).^2/dx^2; %Exp. extraploation
  
  % Below we create A_n tridiagonal matrix
  ax_n  = a(tt,x);
  
  % Sub-diagonal elements of A_n
  Am_n    = ax_n(3:Nx)/dx/2-bx(3:Nx).^2/dx^2/2;
  
  % Diagonal elements of A_n
  Ac_n    = bx(2:Nx).^2/dx^2;
  Ac_n(1) = ax_n(2)/dx; %Linear extraploation
  %Ac_n(1) = ax_n(2)/dx/2+bx(2).^2/dx^2/2;%Neumann
  
  % Super-diagonal elements of A_n
  Ap_n    = -ax_n(2:Nx-1)/dx/2-bx(2:Nx-1).^2/dx^2/2;
  Ap_n(1) = -ax_n(2)/dx; %Linear extraploation
  %Ap_n(1) = -bx(2).^2/dx^2; %Exp. extraploation
  
  %Boundary condition at Nx+1
  bc    = ((ax_n(Nx)+ax(Nx))/dx/2+bx(Nx).^2/dx^2)*dt/2;
  
  % Matrix multiplying known solution values, at time t+dt
  Aexpl = spdiags(repmat([0,1,0],Nx-1,1)-dt/2*[[Am;0],Ac,[0;Ap]],-1:1,Nx-1,Nx-1);
  % Matrix multiplying unknown solution values, at time t
  Aimpl = spdiags(repmat([0,1,0],Nx-1,1)+dt/2*[[Am_n;0],Ac_n,[0;Ap_n]],-1:1,Nx-1,Nx-1);
  r = [zeros(1,Nx-2)'; bc];
  
  u(2:Nx,n+1) = Aimpl\(Aexpl*u(2:Nx,n)+r);   
  end
  u(Nx+1,Dt+2:end)=1;       %u(x_{Nx+1}, t)=1 since we have boundary condition u(K,t)=1
  x=x(2:end);u=u(2:end,:);  %x_0 is excluded since u(x_0,t) is approximated and incorporated to u(x_1,t)            
end

