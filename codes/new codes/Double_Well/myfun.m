function [F,f,H] = myfun(z,K,x,ax,bx,dt)
      F = 0.5*exp(z.^2*dt).*erfcx((K-x-(ax-bx.*z)*dt)./bx/sqrt(2*dt)); % function
      f = z*dt.*exp(z.^2*dt).*erfc((K-x-(ax-bx.*z)*dt)./bx/sqrt(2*dt))-...  %gradient
      sqrt(dt/(2*pi))*exp(-(K-x-ax*dt).^2./(2*bx.^2.*dt)-(K-x-ax*dt).*z./bx+0.5*z.^2*dt);
      H=(dt*exp(z.^2*dt)+2*z.^2*dt^2*exp(z.^2*dt))*erfc((K-x-(ax-bx.*z)*dt)./bx/sqrt(2*dt))...
      -z.*dt*exp(z.^2*dt).*sqrt(2*dt/pi)*exp(-(K-x-(ax-bx.*z)*dt).^2/(2*dt*bx.^2))...
      -sqrt(dt/2/pi)*exp(-(K-x-ax*dt).^2./(2*bx.^2.*dt)-(K-x-ax*dt).*z./bx+0.5*z.^2*dt)*(-(K-x-ax*dt)./bx+z*dt);

 end