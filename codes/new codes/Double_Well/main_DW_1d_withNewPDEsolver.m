clear; close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Consider the SDE in the form
%------- dX_t= a(X_t)dt+b(X_t)dW_t -----------------------------------%
%----------------------------------------------------------------------%
%%% QoI: P(max_{0<=t<=T} X_t >K), where K is given the threshold

%seed=1; rng(seed);

%Model parameters
sigma=0.5;
a  = @(t,x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x)+0*t;    %DW drift
aa = @(x)   0.25*(8*x./(1+2*x.^2).^2 - 2*x);        %DW drift
b  = @(x)   sigma+0*x;                              %constant diffusion

%Simulation parameters
T=1;                                              %simulation time length
K=1.2                                               %the threshold
Kstep=100;                                        %numerical timestep number in SDE
dt_const = T/Kstep;                               %timestep in SDE

%Original initial density parameters
mu_0    = -1;     %mean
sigma_0 =  0.2      %std

%% MULTILEVEL CROSS-ENTROPY METHOD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We consider only mean shifting but fixing the covariance
M=10^5;
beta=0.01;                              %CE method parameter
mu_tilde = mu_0;
sigma_tilde = sigma_0;

% Multilevel procedure to find the optimal mu_tilde
u=zeros(1,Kstep);
Mbar=zeros(1,M);
Weight=ones(1,M);

%Defining first K_1
u0=mu_tilde+sigma_tilde*randn(1,M);
for m=1:M
    u(1)=u0(m);
    for k=1:Kstep
        dW = sqrt(dt_const)*randn;
        u(k+1)= u(k)+aa(u(k))*dt_const+b(u(k))*dW; %Forward Euler
    end
%     figure(1)
%     plot([0:0.01:1],u)
%     hold on
%     pause()
    Mbar(m)=max(u);
end

sMbar=sort(Mbar);
K_ell=sMbar(1, ceil((1-beta)*M));
K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
h=(Mbar>=K_ell);
ell=1;
while K_ell<K
    mu_tilde=sum(h.*Weight.*u0)/sum(h.*Weight);
    sigma_tilde=sqrt(sum(h.*Weight.*(u0-mu_tilde).^2)/sum(h.*Weight));
    
    ell=ell+1;
    u0=mu_tilde+sigma_tilde*randn(1,M);
    for m=1:M
        u(1)=u0(m);
        for k=1:Kstep
            dW = sqrt(dt_const)*randn;
            u(k+1)= u(k)+aa(u(k))*dt_const+b(u(k))*dW; %Forward Euler
        end
        Mbar(m)=max(u);
    end
    sMbar=sort(Mbar);
    K_ell=sMbar(1, ceil((1-beta)*M));
    K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
    h=Mbar>=K_ell;
    Weight=(sigma_tilde/sigma_0)*exp(-(u0-mu_0).^2./(2*sigma_0^2)+(u0-mu_tilde).^2./(2*sigma_tilde^2));
end

if K_ell==K
    mu_tilde=sum(h.*Weight.*u0)/sum(h.*Weight);
    sigma_tilde=sqrt(sum(h.*Weight.*(u0-mu_tilde).^2)/sum(h.*Weight));
end

%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = -5;                         % Lower end of discretization interval
xU = K;                          % Upper end of discretization interval
dxPde = 0.005;                   % Space step in PDE    
dtPde = dxPde/2;                 % Time step in PDE
    Nx = round((xU-xL)/dxPde);       % Number of space steps in PDE
Nt = round(T/dtPde);             % Number of time steps in PDE
x  = linspace(xL,xU,Nx+1)';      % Space grid points

%% Analytical solution for frozen coeff. at the corner
v_anal=@(x,t) 0.5*erfc((K-x-a(T,K).*(T-t))./sqrt(2*b(K).^2.*(T-t)))...
              +0.5*exp(a(T,K).*(K-x)./b(K).^2./2).*erfc((K-x+a(T,K).*(T-t))./sqrt(2*b(K).^2.*(T-t)));
%% Finding a (red) region in the corner to use analytical solution for the pde solver
absdiff=cell(Nx+1, Nt+1);
t=linspace(T,0,Nt+1);
[Xw,Tw] = ndgrid(x,t);
[px, py]=gradient(v_anal(Xw,Tw));
gradNeeded={[dxPde dtPde]};
for i = 1:Nx+1
for j = 1:Nt+1
absdiff{i,j}= norm([px(i,j) py(i,j)]-[dxPde dtPde]);
end
end
[MinVal, ind]=min(cell2mat(absdiff),[], 'all', 'linear');
[idx{1:ndims(cell2mat(absdiff))}] = ind2sub(size(cell2mat(absdiff)), ind);
RedRegionIndx=[idx{:}];
x(RedRegionIndx(1))
t(RedRegionIndx(2))
K-x(RedRegionIndx(1))
T-t(RedRegionIndx(2))
px(idx{:}) %should be close to dxPde
py(idx{:}) %should be close to dtPde
%% KBE solution for all time using anal.sol
[tW, xW, v] = KBEsolverForAllt_AnalSol(x,T,a,b,Nt,Nx,K,RedRegionIndx); %PDE solver via CN backward scheme using analytical solution with frozen coeff. in the corner
%% rho_0 CONTROL in IS wrt initial condition
rho_x0 = @(x, m, sig) exp(-(x-m).^2/(2*sig^2))/(sqrt(2*pi*sig^2)); %1D Gaussian density
rhoTilde_x0 = [rho_x0(xW, mu_0, sigma_0).*sqrt(v(:,end)); rho_x0((K:dxPde:K+500*dxPde)', mu_0, sigma_0)];
NC = trapezoidal(rhoTilde_x0, dxPde); %normalising constant
rhoTilde_x0=rhoTilde_x0./NC;          %optimal IS density

%fit optimal IS denisty to Gaussian
x=[xW;(K:dxPde:K+500*dxPde)'];
mFit = trapezoidal(x.*rhoTilde_x0, dxPde)
sigFit = sqrt(trapezoidal(x.^2.*rhoTilde_x0, dxPde)-mFit^2)
rhoFit_x0 = rho_x0(x, mFit, sigFit);

%% rho_0 CONTROL in IS wrt both
rhoTilde_x0_both = [rho_x0(xW, mu_0, sigma_0).*v(:,end); rho_x0((K:dxPde:K+500*dxPde)', mu_0, sigma_0)];
NC_both = trapezoidal(rhoTilde_x0_both, dxPde); %normalising constant
rhoTilde_x0_both=rhoTilde_x0_both./NC_both;     %optimal IS density

%fit optimal IS denisty to Gaussian
mFit_both = trapezoidal(x.*rhoTilde_x0_both, dxPde)
sigFit_both = sqrt(trapezoidal(x.^2.*rhoTilde_x0_both, dxPde)-mFit_both^2)
rhoFit_x0_both = rho_x0(x, mFit_both, sigFit_both);

%% dW CONTROL
%obtained via KBE solution on PDE grids
ksiGridded = zeros(length(xW),length(tW));
ksiGridded(2:end-1,:) = sigma*(log(v(3:end,:))-log(v(1:end-2,:)))./(2*dxPde);
ksiGridded(1,:) = sigma*(log(v(2,:))-log(v(1,:)))./dxPde;
ksiGridded(end,:) = sigma*(log(v(end,:))-log(v(end-1,:)))./dxPde;

%Optimal control using KBE solution
[Xw,Tw] = ndgrid(xW,tW); %meshgrid for the interpolant
cropN=0.1*Nx; %if we use KBE, I crop 10% of the left boundary and linearly smooth it to avoid instability on the left handside; for HJB no need to do this and nCrop=1
F=griddedInterpolant(Xw(cropN:end,:),flip(Tw(cropN:end,:),2), flip(ksiGridded(cropN:end,:),2),'linear','linear');
ksiStar_PDE= @(x,t) F(x,t).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function based on the discrete optimal control values

ksiStar_Asmp= @(x,t) ((1/sigma)*((K-x)./(T-t))-aa(x)).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function based on asymptotic apprx.

%%
ksiStar = @(x,t, dt_EM) sum([ksiStar_PDE(x,t)*(T-t>=10*dt_EM) ksiStar_Asmp(x,t)*(T-t<10*dt_EM)], 'omitnan'); %the factor 4 corresponds for the particular choice of dtPde=0.005/2 where ksigridded(x,t) with t<=0.96 smooth,t>0.96 it has a kink close to the barrier.
%% Computing the QoI with and without change of measure wrt W and rho_0
Srng=[1e6];

for ss=1:length(Srng)
    S=Srng(ss);
    
    %Initialization
    h_IS=zeros(1,S); h_IS_rho0=h_IS;h_MC=h_IS;  h_IS_W=h_IS; h_CE=h_IS; timeHit=h_IS;
    runmax_IS=zeros(1,S);runmax_IS_W=runmax_IS;runmax_IS_rho0=runmax_IS;runmax_MC=runmax_IS; runmax_CE=runmax_IS;
    u_MC=zeros(1,Kstep+1);Y=u_MC; u_IS=u_MC;u_IS_W=u_MC;u_IS_rho0=u_MC; u_CE=u_MC;
    
    u0=mu_0+sigma_0*randn(1,S);
    u0_shifted_1=mFit+sigFit*randn(1,S);
    u0_shifted_2=mFit_both+sigFit_both*randn(1,S);
    u0_CE=mu_tilde+sigma_tilde*randn(1,S);
    
    meanL=0;
    meanL_W=0;
    t0=0;
    trng1=[];
    trng2=[];
    
parfor s=1:S
       u_MC=u0(s);
       u_IS=u0_shifted_2(s); 
       u_IS_W=u0(s);
       u_IS_rho0=u0_shifted_1(s);
       u_CE=u0_CE(s);

       
       %We integrate until the state hits the threshold or until final time
       trng1=t0;
       trng2=t0;
       
       %IS wrt both
       Q=1;
       L=1;
       k=1;
       t_n=0;
       dt=dt_const;
        while t_n<=T && u_IS(k)<K && dt>0.000001
            dt=min(dt_const, 0.5*(T-t_n));
            dW1 = sqrt(dt)*randn;
            L=L*exp(-0.5*dt*(ksiStar(u_IS(k),t_n,dt_const))^2-ksiStar(u_IS(k),t_n, dt_const)*dW1);
            u_IS(k+1)= u_IS(k)+(aa(u_IS(k))+sigma*ksiStar(u_IS(k),t_n, dt_const))*dt+sigma*dW1; %Forward Euler 
            q=exp(-2*max(K-u_IS(k),0)*max(K-u_IS(k+1),0)/(sigma^2*dt));
            
            if rand<q 
                t_n=t_n+0.5*dt; u_IS(k+1)=K;
            else
                t_n=t_n+dt;
            end
            
            Q=Q*q;
            k=k+1;
            trng1=[trng1 t_n];
        end

       %IS wrt W(t)
        dt=dt_const;
        L_W=1;
        Q_W=1;
        k=1;
        t_n=0;
        while t_n<=T && u_IS_W(k)<K && dt>0.000001
            dt=min(dt_const, 0.5*(T-t_n));
            dW2 = sqrt(dt)*randn;
            L_W=L_W*exp(-0.5*dt*(ksiStar(u_IS_W(k),t_n, dt_const))^2-ksiStar(u_IS_W(k),t_n, dt_const)*dW2);
            u_IS_W(k+1)= u_IS_W(k)+(aa(u_IS_W(k))+sigma*ksiStar(u_IS_W(k),t_n, dt_const))*dt+sigma*dW2; %Forward Euler 
            q_W= exp(-2*max(K-u_IS_W(k),0)*max(K-u_IS_W(k+1),0)/(sigma^2*dt));
            
            if rand<q_W 
                t_n=t_n+0.5*dt; u_IS_W(k+1)=K;
            else
                t_n=t_n+dt;
            end
            
            Q_W=Q_W*q_W;
            k=k+1;
            trng2=[trng2 t_n];
        end

        %MC simulation and IS wrt rho_0 
        Q_MC=1;
        Q_IS_rho0=1;
        Q_CE=1;
        n=1;
        t_n=0;
        while t_n<=T && u_MC(n)<K
            u_MC(n+1)=u_MC(n)+aa(u_MC(n))*dt_const+sigma*sqrt(dt_const)*randn;
            
            q_MC= exp(-2*max(K-u_MC(n),0)*max(K-u_MC(n+1),0)/(sigma^2*dt_const));
            if rand<q_MC 
            u_MC(n+1)=K;
            end
            t_n=t_n+dt_const;
            n=n+1;
            Q_MC=Q_MC*q_MC;
        end
       
        n=1;
        t_n=0;
        while t_n<=T && u_IS_rho0(n)<K
            u_IS_rho0(n+1)= u_IS_rho0(n)+(aa(u_IS_rho0(n)))*dt_const+sigma*sqrt(dt_const)*randn; %Forward Euler 

            q_IS_rho0=exp(-2*max(K-u_IS_rho0(n),0)*max(K-u_IS_rho0(n+1),0)/(sigma^2*dt_const));
            if rand<q_IS_rho0 
            u_IS_rho0(n+1)=K;
            end
            Q_IS_rho0=Q_IS_rho0*(q_IS_rho0);
            t_n=t_n+dt_const;
            n=n+1;
        end
        
        n=1;
        t_n=0;
        while t_n<=T && u_CE(n)<K
            u_CE(n+1)=u_CE(n)+aa(u_CE(n))*dt_const+sigma*sqrt(dt_const)*randn;

            q_CE= exp(-2*max(K-u_CE(n),0)*max(K-u_CE(n+1),0)/(sigma^2*dt_const));
            if rand<q_CE 
            u_CE(n+1)=K;
            end
            t_n=t_n+dt_const;
            n=n+1;
            Q_CE=Q_CE*(q_CE);
        end

    meanL=meanL+L;
    meanL_W=meanL_W+L_W;
    h_IS(s)=(max(u_IS)>=K)*L;
    h_IS_W(s)=(max(u_IS_W)>=K)*L_W;
    h_IS_rho0(s)=(max(u_IS_rho0)>=K);
    h_MC(s)=(max(u_MC)>=K);
    h_CE(s)=(max(u_CE)>=K);

    runmax_IS(s)=max(u_IS);
    runmax_IS_W(s)=max(u_IS_W);
    runmax_IS_rho0(s)=max(u_IS_rho0);
    runmax_MC(s)=max(u_MC);
    runmax_CE(s)=max(u_CE); 
end

meanL=meanL/S
meanL_W=meanL_W/S
Weight_CE=(sigma_tilde/sigma_0)*exp(-(u0_CE-mu_0).^2./(2*sigma_0^2)+(u0_CE-mu_tilde).^2./(2*sigma_tilde^2));
Weight_PDE_1=(sigFit/sigma_0)*exp(-(u0_shifted_1-mu_0).^2./(2*sigma_0^2)+(u0_shifted_1-mFit).^2./(2*sigFit^2));
Weight_PDE_2=(sigFit_both/sigma_0)*exp(-(u0_shifted_2-mu_0).^2./(2*sigma_0^2)+(u0_shifted_2-mFit_both).^2./(2*sigFit_both^2));
alpha_hat_IS_both(ss)=mean(h_IS.*Weight_PDE_2)
alpha_hat_IS_W(ss)=mean(h_IS_W)
alpha_hat_IS_rho0(ss)=mean(h_IS_rho0.*Weight_PDE_1)
alpha_hat_MC(ss)=mean(h_MC)
alpha_hat_CE(ss)=mean(h_CE.*Weight_CE)
Vaprx_IS_both=var(h_IS.*Weight_PDE_2)
Vaprx_IS_W=var(h_IS_W)
Vaprx_IS_rho0=var(h_IS_rho0.*Weight_PDE_1)
Vaprx_MC=var(h_MC)
Vaprx_CE=var(h_CE.*Weight_CE)
RelError_MC=1.96*sqrt(Vaprx_MC)/(alpha_hat_MC(ss)*sqrt(S))
RelError_IS_both=1.96*sqrt(Vaprx_IS_both)/(alpha_hat_IS_both(ss)*sqrt(S))
RelError_IS_W=1.96*sqrt(Vaprx_IS_W)/(alpha_hat_IS_W(ss)*sqrt(S))
RelError_IS_rho0=1.96*sqrt(Vaprx_IS_rho0)/(alpha_hat_IS_rho0(ss)*sqrt(S))
RelError_CE=1.96*sqrt(Vaprx_CE)/(alpha_hat_CE(ss)*sqrt(S))
varRatio_both=Vaprx_MC/Vaprx_IS_both
varRatio_W=Vaprx_MC/Vaprx_IS_W
varRatio_rho0=Vaprx_MC/Vaprx_IS_rho0
varRatio_CE=Vaprx_MC/Vaprx_CE

IS_both_CIu(ss)=alpha_hat_IS_both(ss)+1.96*sqrt(Vaprx_IS_both)/sqrt(S)
IS_both_CId(ss)=alpha_hat_IS_both(ss)-1.96*sqrt(Vaprx_IS_both)/sqrt(S)
IS_W_CIu(ss)=alpha_hat_IS_W(ss)+1.96*sqrt(Vaprx_IS_W)/sqrt(S)
IS_W_CId(ss)=alpha_hat_IS_W(ss)-1.96*sqrt(Vaprx_IS_W)/sqrt(S)
IS_rho0_CIu(ss)=alpha_hat_IS_rho0(ss)+1.96*sqrt(Vaprx_IS_rho0)/sqrt(S)
IS_rho0_CId(ss)=alpha_hat_IS_rho0(ss)-1.96*sqrt(Vaprx_IS_rho0)/sqrt(S)
MC_CIu(ss)=alpha_hat_MC(ss)+1.96*sqrt(Vaprx_MC)/sqrt(S)
MC_CId(ss)=alpha_hat_MC(ss)-1.96*sqrt(Vaprx_MC)/sqrt(S)
CE_CIu(ss)=alpha_hat_CE(ss)+1.96*sqrt(Vaprx_CE)/sqrt(S)
CE_CId(ss)=alpha_hat_CE(ss)-1.96*sqrt(Vaprx_CE)/sqrt(S)
end

%%
figure
histogram(timeHit,'Normalization', 'pdf')
xlabel('t')
ylabel('density function')

figure
f=ksdensity(timeHit,0:0.01:1,'Function', 'pdf');
plot(0:0.01:1,f,'Linewidth',2)
ylabel('density function')
xlabel('t')
%% Confidence interval plots
figure
loglog(Srng, alpha_hat_IS_both, 'bd', Srng, IS_both_CIu , 'b-', Srng, IS_both_CId , 'b-', 'Linewidth', 2)
hold on
loglog(Srng, alpha_hat_IS_W, 'ms', Srng, IS_W_CIu , 'm-', Srng, IS_W_CId , 'm-', 'Linewidth', 2)
hold on
loglog(Srng, alpha_hat_IS_rho0, 'c^', Srng, IS_rho0_CIu , 'c-', Srng, IS_rho0_CId , 'c-', 'Linewidth', 2)
hold on
loglog(Srng, alpha_hat_CE, 'gx', Srng, CE_CIu , 'g-', Srng, CE_CId , 'g-','Linewidth', 2)
hold on
loglog(Srng, alpha_hat_MC, 'ro', Srng, MC_CIu , 'r-', Srng, MC_CId , 'r-','Linewidth', 2)
xlabel('Sample size M')
legend( 'IS wrt both','', '', 'IS wrt W(t)','', '', 'IS wrt \rho_0', '', '', 'CE', '', '', 'MC')
title('95% confidence interval')

% function f=ksiStar(x,t,T,dt_EM) 
%     if T-t<10*dt_EM 
%        f= ((1/sigma)*((K-x)./(T-t))-aa(x)).*(x<K)+zeros(size(t)).*(x>=K); 
%     else
%        f=  F(x,t).*(x<K)+zeros(size(t)).*(x>=K);
%     end
% end

%% Bootstrapping for CE
close all
C = {'red','magenta'}; % Cell array of colros.
Cc={'green','blue'};
Ccc = {'cyan', 'yellow'};
Cccc = {'black', 'red'};

B=10^4;
conf=95;
Krng=[1.5 2 2.5 3];                     %the threshold DW

q_IS_CE=[h_IS_CE_K15_sigma0scale; h_IS_CE_K2_sigma0scale; h_IS_CE_K25_sigma0scale; h_IS_CE_K3_sigma0scale];

q_IS_CE_2=[h_IS_CE_K15_sigma0scale_2; h_IS_CE_K2_sigma0scale_2; h_IS_CE_K25_sigma0scale_2; h_IS_CE_K3_sigma0scale_2];

Q_IS_CE{1}=q_IS_CE;Q_IS_CE{2}=q_IS_CE_2;

for n=1:2
        for i=1:length(Krng)
        K=Krng(i);
        if isempty(gcp)
           parpool;
        end
        opt = statset('UseParallel',true);

        stats_IS_CE = bootstrp(B, @(x) [mean(x) std(x)], Q_IS_CE{n}(i,:), 'Options', opt);

        SmplStd_IS_CE(i) =mean(stats_IS_CE(:,2));

        CId_IS_CE(i) = prctile(stats_IS_CE(:,2), (100-conf)/2);
        CIu_IS_CE(i) =  prctile(stats_IS_CE(:,2), (100-(100-conf)/2));
        end

    figure(4)
    plot_ci(Krng',[SmplStd_IS_CE; CId_IS_CE; CIu_IS_CE], 'PatchColor', Cccc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Cccc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Cccc{n}, 'YScale', 'log');
    grid on
    hold on
    title('IS with CE')
    
end
figure(4)
xlabel('$\mathcal{K}$','interpreter', 'latex', 'FontName','Times','fontsize',50)
ylabel('sample std','FontName','Times','fontsize',40)
title(['IS with CE'], 'FontName','Times', 'fontsize',30,'interpreter','latex')
legend('95% CI bootstrap', 'run 1', '', '', '95% CI bootstrap','run 2', 'FontName','Times', 'fontsize',20)

%% Bootstrapping
% close all
% C    = {'red','magenta'}; % Cell array of colros.
% Cc   = {'green','blue'};
% Ccc  = {'cyan', 'yellow'};
% Cccc = {'black', 'red'};
% 
% B=10^5;
% conf=95;
% %Krng=[0 0.5 1 1.2];                     %the threshold DW
% % q_both=[h_both_K0; h_both_K05; h_both_K1; h_both_K12];
% % q_IS_W=[h_IS_W_K0; h_IS_W_K05; h_IS_W_K1; h_IS_W_K12];
% % q_IS_rho0=[h_IS_rho0_K0; h_IS_rho0_K05; h_IS_rho0_K1; h_IS_rho0_K12];
% % q_IS_CE=[h_IS_CE_K0; h_IS_CE_K05; h_IS_CE_K1; h_IS_CE_K12];
% 
% for n=1:2
%         for i=1:length(Krng)
%         K=Krng(i);
%         if isempty(gcp)
%            parpool;
%         end
%         opt = statset('UseParallel',true);
% 
%         stats_IS_both = bootstrp(B, @(x) [mean(x) std(x)], q_both(i,:), 'Options', opt);
%         stats_IS_W = bootstrp(B, @(x) [mean(x) std(x)], q_IS_W(i,:), 'Options', opt);
%         stats_IS_rho0 = bootstrp(B, @(x) [mean(x) std(x)], q_IS_rho0(i,:), 'Options', opt);
%         stats_IS_CE = bootstrp(B, @(x) [mean(x) std(x)], q_IS_CE(i,:), 'Options', opt);
% 
%         SmplStd_IS_both(i)=mean(stats_IS_both(:,2));
%         SmplStd_IS_W(i) =mean(stats_IS_W(:,2));
%         SmplStd_IS_rho0(i) =mean(stats_IS_rho0(:,2));
%         SmplStd_IS_CE(i) =mean(stats_IS_CE(:,2));
% 
%         CId_IS_both(i) =  prctile(stats_IS_both(:,2), (100-conf)/2);
%         CIu_IS_both(i) =  prctile(stats_IS_both(:,2), (100-(100-conf)/2));
% 
%         CId_IS_W(i) = prctile(stats_IS_W(:,2), (100-conf)/2);
%         CIu_IS_W(i) =  prctile(stats_IS_W(:,2), (100-(100-conf)/2));
% 
%         CId_IS_rho0(i) = prctile(stats_IS_rho0(:,2), (100-conf)/2);
%         CIu_IS_rho0(i) =  prctile(stats_IS_rho0(:,2), (100-(100-conf)/2));
%         
%         CId_IS_CE(i) = prctile(stats_IS_CE(:,2), (100-conf)/2);
%         CIu_IS_CE(i) =  prctile(stats_IS_CE(:,2), (100-(100-conf)/2));
%         end
%     figure(1)
%     plot_ci(Krng',[SmplStd_IS_both; CId_IS_both; CIu_IS_both],'PatchColor', C{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', C{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', C{n}, 'YScale', 'log');
%     grid on
%     title('IS wrt both \rho_0 and W(t)')
%     hold on
%     figure(2)
%     plot_ci(Krng',[SmplStd_IS_W; CId_IS_W; CIu_IS_W], 'PatchColor', Cc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Cc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Cc{n}, 'YScale', 'log');
%     grid on
%     hold on
%     title('IS with W(t)')
%     figure(3)
%     plot_ci(Krng',[SmplStd_IS_rho0; CId_IS_rho0; CIu_IS_rho0], 'PatchColor', Ccc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Ccc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Ccc{n}, 'YScale', 'log');
%     grid on
%     hold on
%     title('IS with \rho_0')
%     figure(4)
%     plot_ci(Krng',[SmplStd_IS_CE; CId_IS_CE; CIu_IS_CE], 'PatchColor', Cccc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Ccc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Cccc{n}, 'YScale', 'log');
%     grid on
%     hold on
%     title('IS with CE')
%     
%     figure(5)
%     plot_ci(Krng',[SmplStd_IS_both; CId_IS_both; CIu_IS_both],'PatchColor', C{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', C{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', C{n}, 'YScale', 'log');
%     grid on
%     hold on
%     plot_ci(Krng',[SmplStd_IS_W; CId_IS_W; CIu_IS_W], 'PatchColor', Cc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Cc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Cc{n}, 'YScale', 'log');
%     grid on
%     hold on
%     plot_ci(Krng',[SmplStd_IS_rho0; CId_IS_rho0; CIu_IS_rho0], 'PatchColor', Ccc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Ccc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Ccc{n}, 'YScale', 'log');
%     grid on
%     hold on
%     plot_ci(Krng',[SmplStd_IS_CE; CId_IS_CE; CIu_IS_CE], 'PatchColor', Cccc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Ccc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Cccc{n}, 'YScale', 'log');
%     grid on
%     hold on
% end