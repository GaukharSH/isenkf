clear; close all; set(0, 'defaultaxesfontsize', 15); format long
%Krng=[1.2];
%for zzz=length(Krng)
%clear; close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for 1D problem to performe IS technique with respect
%%% to both rho_0 and W
%%% 
%------- dX_t= a(X_t)dt+b(X_t)dW_t -----------------------------------%
%----------------------------------------------------------------------%
%%% in computing QoI: P(max_{0<=t<=T} X_t >K)
%seed=1; rng(seed);

%Model parameters
sigma=0.5;
a  = @(t,x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x)+0*t;  %DW drift
aa = @(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x);        %DW drift
b  = @(x) sigma+0*x;                              %constant diffusion

%Simulation parameters
T=100;                                              %simulation time length
K=0.5                                             %the threshold
Kstep=10000;                                        %numerical timestep number in SDE
dt = T/Kstep;                                     %timestep in SDE

%Original initial density parameters
mu_0    = -1;     %mean
sigma_0 = 0.2     %std

%% MULTILEVEL CROSS-ENTROPY METHOD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We consider only mean shifting but fixing the covariance
M=10^0;
beta=0.01;                              %CE method parameter
mu_tilde = mu_0;
sigma_tilde = sigma_0;

% Multilevel procedure to find the optimal mu_tilde
u=zeros(1,Kstep);
Mbar=zeros(1,M);
Weight=ones(1,M);
t=linspace(0,T,Kstep+1);

%Defining first K_1
u0=mu_tilde+sigma_tilde*randn(1,M);
for m=1:M
    u(1)=u0(m);
    for k=1:Kstep
        dW = sqrt(dt)*randn;
        u(k+1)= u(k)+aa(u(k))*dt+b(u(k))*dW; %Forward Euler
    end
    figure(10)
 plot(t, u, 'LineWidth', 2)
 xlabel('t', 'fontsize', 28)
 ylabel('u', 'fontsize', 28)
 title('b=0.5','fontsize', 28)
 hold on
    Mbar(m)=max(u);
end

%% Plotting the trajectory with potential
close all
V = @(x) 0.25*(2./(1+2*x.^2) + x.^2); 
u0_ex=-0.7;
ObsSteps=100;
N=100;
[dwTraj,yobs]=ObsAndParticles(u0_ex(1),ObsSteps,sigma,1, 1, 0.1, aa);%(u0,ObsSteps, sigma,tau, H, Gamma, a_pi)
tt=linspace(0,ObsSteps,ObsSteps*100);
plot(30*V(-2:0.1:2)-10.5,[-2:0.1:2], 'k:','LineWidth', 3)
hold on
for i=1:ObsSteps
    plot(tt((i-1)*100+1:1:i*100), dwTraj{i},'-b','LineWidth', 1.5)
    hold on
    plot(tt(i*100), yobs(i+1), '*r', 'LineWidth', 2)
    hold on
end
hold off
xlabel('t')
legend('DW potential','SDE trajectory', 'Observations', 'LineWidth', 1.5, 'FontSize', 20)
set(gca,'FontSize',20)
%%
sMbar=sort(Mbar);
K_ell=sMbar(1, ceil((1-beta)*M));
K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
h=Mbar>=K_ell;
ell=1;
while K_ell<K
    mu_tilde=sum(h.*Weight.*u0)/sum(h.*Weight);
    %sigma_tilde=sqrt(sum(h.*Weight.*(u0-mu_tilde).^2)/sum(h.*Weight));
    
    ell=ell+1;
    u0=mu_tilde+sigma_tilde*randn(1,M);
    for m=1:M
        u(1)=u0(m);
        for k=1:Kstep
            dW = sqrt(dt)*randn;
            u(k+1)= u(k)+aa(u(k))*dt+b(u(k))*dW; %Forward Euler
        end
        Mbar(m)=max(u);
    end
    sMbar=sort(Mbar);
    K_ell=sMbar(1, ceil((1-beta)*M));
    K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
    h=Mbar>=K_ell;
    Weight=(sigma_tilde/sigma_0)*exp(-(u0-mu_0).^2./(2*sigma_0^2)+(u0-mu_tilde).^2./(2*sigma_tilde^2));
end

if K_ell==K
    mu_tilde=sum(h.*Weight.*u0)/sum(h.*Weight);
    %sigma_tilde=sqrt(sum(h.*Weight.*(u0-mu_tilde).^2)/sum(h.*Weight));
end

%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = -5;                         % Lower end of discretization interval
xU = K;                          % Upper end of discretization interval
dxPde = 0.005;                   % Space step in PDE    
dtPde = dxPde/2;                 % Time step in PDE
Nx = round((xU-xL)/dxPde);       % Number of space steps in PDE
Nt = round(T/dtPde);             % Number of time steps in PDE
x  = linspace(xL,xU,Nx+1)';      % Space grid points

%[~,~,vErik] =   tabulate_IC(aa,b,dt,K,xL,xU,true,Nx+1); %KBE solution at T-dt obtained via numerical optimization (using Erik's code)

%% KBE solution for all time using anal.sol
[tW, xW, v] = KBEsolverForAllt_AnalSol(x,T,a,b,Nt,Nx,K, RedRegionIndx); %PDE solver via CN backward scheme using exponential smoothing of Ind.Fun in a final condition
%%
figure
[Xw,Tw] = ndgrid(xW,tW);
p2=surf(Xw, Tw, v);
set(p2,'LineStyle','none')
xlabel('x','FontName','Times','fontsize',20)
ylabel('t','FontName','Times','fontsize',20)
zlabel('\gamma(x,t)','FontName','Times','fontsize',20)
title('KBE finite diff. solution: $\mathcal{K}=2$, $b=0.5$','interpreter', 'latex', 'FontName','Times','fontsize',20)
%view([-120 50])
%%
v_anal=@(x,t) 0.5*erfc((K-x-a(T,K).*(T-t))./sqrt(2*b(K).^2.*(T-t)))...
              +0.5*exp(a(T,K).*(K-x)./b(K).^2./2).*erfc((K-x+a(T,K).*(T-t))./sqrt(2*b(K).^2.*(T-t)));

figure
p1=surf(Xw, Tw, v_anal(Xw,Tw));
set(p1,'LineStyle','none')
xlabel('x','FontName','Times','fontsize',20)
ylabel('t','FontName','Times','fontsize',20)
zlabel('\gamma(x,t)','FontName','Times','fontsize',20)
title('KBE analytical solution: $\mathcal{K}=2$, $\sigma=0.5$','interpreter', 'latex', 'FontName','Times','fontsize',20)
%%
v_exact=@(x,t) 0.5*erfc((K-x-a(t,x).*(T-t))./sqrt(2*b(x).^2.*(T-t)))...
              +0.5*exp(a(t,x).*(K-x)./b(x).^2./2).*erfc((K-x+a(t,x).*(T-t))./sqrt(2*b(x).^2.*(T-t)));
vex=v_exact(Xw,Tw);
%van=v_anal(Xw,Tw);
van=v;

%%
clear absdiff
t=linspace(T,0,Nt+1);
[Xw,Tw] = ndgrid(x,t);
[px, py]=gradient(v_anal(Xw,Tw));
gradNeeded={[dxPde dtPde]};
for i = 1:Nx+1
for j = 1:Nt+1
absdiff{i,j}= norm([px(i,j) py(i,j)]-[dxPde dtPde]);
end
end
[MinVal ind]=min(cell2mat(absdiff),[], 'all', 'linear');
[idx{1:ndims(cell2mat(absdiff))}] = ind2sub(size(cell2mat(absdiff)), ind);
RedRegionIndx=[idx{:}];
px(idx{:})
py(idx{:})
%%
close all
%Plot KBE solution at different time in 2D
tt=[0.99 0.97 0.96 0.9 0.4];
xx=[x(end-1) 0 -0.5 -1 -2 -4];
%tfixn=[4001 7679 11720 19801 40001];
C = {'r','m','g','b','y','k'}; 
%vasol=v_anal(Xw,Tw);
for i=1:length(tt)
    figure(7)
    tfix=find(tW==tt(i));
    xfix=find(xW==xx(i));
    semilogy(xW, v(:,2),'-','color',C{i}, 'LineWidth', 2)
    %semilogy(xW, gradient(v(:,tfix)),'-','color',C{i}, 'LineWidth', 2)
    xlabel('t','FontName','Times','fontsize',20')
    hold on
%     figure(8)
%     %plot(xW, abs(v(:,tfix)-van(:,tfix)),'-', 'color',C{i}, 'LineWidth', 2)
%     plot(tW, abs(v(xfix,:)-van(xfix,:)),'-', 'color',C{i}, 'LineWidth', 2)
%     hold on
end
grid on
legend('t=0.9999', 't=0.99', 't=0.9', 't=0.8', 't=0.5', 't=0')
%legend('x=K-dx_{PDE}', 'x=0', 'x=-0.5', 'x=-1', 'x=-2', 'x=-5')
xlabel('t','FontName','Times','fontsize',20)
ylabel('|\gamma_{anal}-\gamma_{pde solver}|','FontName','Times','fontsize',20)
title('$\mathcal{K}=0.5$, $b=0.5$','interpreter', 'latex', 'FontName','Times','fontsize',20)
%%
%Plot optimal control at different time in 2D
%ksiInterp=ksi(Xw,Tw);
for i=1:length(tt)
    figure(8)
    tfix=find(tW==tt(i));
    semilogy(xW, ksiGridded(:,12), 'LineWidth', 2)
    hold on
end
grid on
legend('t=0.9999', 't=0.99', 't=0.9', 't=0.8', 't=0.5', 't=0')
%legend('t=T-dt','t=0.8T', 't=0.7T', 't=0.5T', 't=0')
xlabel('x')
ylabel('\xi(t,x)')
hold off
title('Optimal Control at different time')
%% Exponentianl smoothing
%Factor in exp.smoothing depends on EM timestep dt, based on numerical
%tests using Erik's solution, we observe the following settings
%dt=0.1, smf=15; dt=0.01, smf=40; dt=0.001, smf=100
smf=40;
% KBE solution for all time
[tW, xW, v] = KBEsolverForAllt(x,T,a,b,Nt,Nx,K,smf); %PDE solver via CN backward scheme using exponential smoothing of Ind.Fun in a final condition
%%
figure
semilogy(xW,v(:,2), 'b-', xW, vErik(2:end), 'k--', 'Linewidth',2)
xlabel('x')
legend('u(x,T)', 'Erik sol')
% figure
% [Xw,Tw] = ndgrid(xW,tW);
% p2=surf(Xw, Tw, v);
% set(p2,'LineStyle','none')
%% rho_0 CONTROL in IS wrt initial condition
rho_x0 = @(x, m, sig) exp(-(x-m).^2/(2*sig^2))/(sqrt(2*pi*sig^2)); %1D Gaussian density
rhoTilde_x0 = [rho_x0(xW, mu_0, sigma_0).*sqrt(v(:,end)); rho_x0((K:dxPde:K+500*dxPde)', mu_0, sigma_0)];
NC = trapezoidal(rhoTilde_x0, dxPde); %normalising constant
rhoTilde_x0=rhoTilde_x0./NC;          %optimal IS density

%fit optimal IS denisty to Gaussian
x=[xW;(K:dxPde:K+500*dxPde)'];
mFit = trapezoidal(x.*rhoTilde_x0, dxPde)
sigFit = sqrt(trapezoidal(x.^2.*rhoTilde_x0, dxPde)-mFit^2)
rhoFit_x0 = rho_x0(x, mFit, sigFit);

%% rho_0 CONTROL in IS wrt both
rhoTilde_x0_both = [rho_x0(xW, mu_0, sigma_0).*v(:,end); rho_x0((K:dxPde:K+500*dxPde)', mu_0, sigma_0)];
NC_both = trapezoidal(rhoTilde_x0_both, dxPde); %normalising constant
rhoTilde_x0_both=rhoTilde_x0_both./NC_both;          %optimal IS density

%fit optimal IS denisty to Gaussian
mFit_both = trapezoidal(x.*rhoTilde_x0_both, dxPde)
sigFit_both = sqrt(trapezoidal(x.^2.*rhoTilde_x0_both, dxPde)-mFit_both^2)
rhoFit_x0_both = rho_x0(x, mFit_both, sigFit_both);
%% rho_0 CONTROL in CE
rhoCE_x0 = rho_x0(x, mu_tilde, sigma_tilde);
%% Plot the densities
V = @(x) 0.25*(2./(1+2*x.^2) + x.^2); 
figure
semilogy(x, rho_x0(x, mu_0, sigma_0), '-r', x, rhoFit_x0, '-.c', x, rhoFit_x0_both, '-.b', x, rhoCE_x0, '--g', 'Linewidth',5)
hold on
semilogy([-4:0.01:4], V([-4:0.01:4])-0.37,':', 'color', [.5 .5 .5],'LineWidth', 5)
hold on
y = ylim; % current y-axis limits
semilogy([K K],[y(1) y(2)], '-k', 'LineWidth', 4)
hold off
ylim([0 2.2])
xlim([-4.5 5.5])
qq = get(gca,'XTickLabel');
set(gca,'XTickLabel',qq,'FontName','Times','fontsize',20)
title(['$\mathbf{\sigma_0=}$',num2str(sigma_0,2)], 'FontName','Times', 'fontsize',40,'interpreter','latex')
xlabel('x','fontsize',20)
%legend({'$\rho_{u_0}$'; ['$\tilde{\rho}_{u_0}^{PDE}$'];['$\tilde{\rho}_{u_0}^{CE}$']; ['DW potential']; ['$\mathcal{K}$=' num2str(K)]},'fontsize',30,'FontName','Times','interpreter','latex')
[~, hobj, ~, ~] =legend({'$\rho_{u_0} \sim N(-1,1)$'; ['$\tilde{\rho}_{u_0}^{PDE,1}\sim N$(' num2str(mFit,2) ', ' num2str(sigFit,4) ')'];['$\tilde{\rho}_{u_0}^{PDE,2}\sim N$(' num2str(mFit_both,3) ', ' num2str(sigFit_both,4) ')'];['$\tilde{\rho}_{u_0}^{CE}\sim N$(' num2str(mu_tilde,3) ', ' num2str(sigma_tilde,2) ')']; ['DW potential']; ['$\mathcal{K}$=' num2str(K)]},'fontsize',35,'FontName','Times','interpreter','latex');
hl = findobj(hobj,'type','line');
set(hl,'LineWidth',2.5);
set(gca,'FontSize',40)
%% Plot the densities before Gaussian approx: normilsed product
V = @(x) 0.25*(2./(1+2*x.^2) + x.^2); 
figure
semilogy(x, rho_x0(x, mu_0, sigma_0), '-r', x, rhoTilde_x0, '-.c', x, rhoTilde_x0_both, '-.b', 'Linewidth',5)
hold on
semilogy([-4:0.01:4], V([-4:0.01:4])-0.37,':', 'color', [.5 .5 .5],'LineWidth', 5)
hold on
y = ylim; % current y-axis limits
semilogy([K K],[y(1) y(2)], '-k', 'LineWidth', 4)
hold off
ylim([0 2.2])
xlim([-4.5 5.5])
qq = get(gca,'XTickLabel');
set(gca,'XTickLabel',qq,'FontName','Times','fontsize',20)
title(['$\mathbf{\sigma_0=}$',num2str(sigma_0,2)], 'FontName','Times', 'fontsize',40,'interpreter','latex')
xlabel('x','fontsize',20)
%legend({'$\rho_{u_0}$'; ['$\tilde{\rho}_{u_0}^{PDE}$'];['$\tilde{\rho}_{u_0}^{CE}$']; ['DW potential']; ['$\mathcal{K}$=' num2str(K)]},'fontsize',30,'FontName','Times','interpreter','latex')
[~, hobj, ~, ~] =legend({'$\rho_{u_0} \sim N(-1,1)$'; ['$\rho_{u_0}(x)\sqrt{\gamma(x,0)}/c_1$']; ['$\rho_{u_0}(x)\gamma(x,0)/c_2$']; ['DW potential']; ['$\mathcal{K}$=' num2str(K)]},'fontsize',35,'FontName','Times','interpreter','latex');
hl = findobj(hobj,'type','line');
set(hl,'LineWidth',2.5);
set(gca,'FontSize',40)
%% Plot the densities before Gaussian approx: product
V = @(x) 0.25*(2./(1+2*x.^2) + x.^2); 
figure
semilogy(x, rho_x0(x, mu_0, sigma_0), '-r', xW, rho_x0(xW, mu_0, sigma_0).*sqrt(v(:,end)), '-.c', xW, rho_x0(xW, mu_0, sigma_0).*v(:,end), '-.b', 'Linewidth',5)
hold on
semilogy([-4:0.01:4], V([-4:0.01:4])-0.37,':', 'color', [.5 .5 .5],'LineWidth', 5)
hold on
y = ylim; % current y-axis limits
semilogy([K K],[y(1) y(2)], '-k', 'LineWidth', 4)
hold off
ylim([0 2.2])
xlim([-4.5 5.5])
qq = get(gca,'XTickLabel');
set(gca,'XTickLabel',qq,'FontName','Times','fontsize',20)
title(['$\mathbf{\sigma_0=}$',num2str(sigma_0,2)], 'FontName','Times', 'fontsize',40,'interpreter','latex')
xlabel('x','fontsize',20)
%legend({'$\rho_{u_0}$'; ['$\tilde{\rho}_{u_0}^{PDE}$'];['$\tilde{\rho}_{u_0}^{CE}$']; ['DW potential']; ['$\mathcal{K}$=' num2str(K)]},'fontsize',30,'FontName','Times','interpreter','latex')
[~, hobj, ~, ~] =legend({'$\rho_{u_0} \sim N(-1,1)$'; ['$\tilde{\rho}_{u_0}^{PDE,1}\propto \rho_{u_0}(x)\sqrt{\gamma(x,0)}$']; ['$\tilde{\rho}_{u_0}^{PDE,2}\propto \rho_{u_0}(x)\gamma(x,0)$']; ['DW potential']; ['$\mathcal{K}$=' num2str(K)]},'fontsize',35,'FontName','Times','interpreter','latex');
hl = findobj(hobj,'type','line');
set(hl,'LineWidth',2.5);
set(gca,'FontSize',40)
%% Plot the densities before Gaussian approx: pde solutions
V = @(x) 0.25*(2./(1+2*x.^2) + x.^2); 
figure
semilogy(x, rho_x0(x, mu_0, sigma_0), '-r', xW, sqrt(v(:,end)), '-.c', xW, v(:,end), '-.b', 'Linewidth',5)
hold on
semilogy([-4:0.01:4], V([-4:0.01:4])-0.37,':', 'color', [.5 .5 .5],'LineWidth', 5)
hold on
y = ylim; % current y-axis limits
semilogy([K K],[y(1) y(2)], '-k', 'LineWidth', 4)
hold off
ylim([0 2.2])
xlim([-4.5 5.5])
qq = get(gca,'XTickLabel');
set(gca,'XTickLabel',qq,'FontName','Times','fontsize',20)
title(['$\mathbf{\sigma_0=}$',num2str(sigma_0,2)], 'FontName','Times', 'fontsize',40,'interpreter','latex')
xlabel('x','fontsize',20)
%legend({'$\rho_{u_0}$'; ['$\tilde{\rho}_{u_0}^{PDE}$'];['$\tilde{\rho}_{u_0}^{CE}$']; ['DW potential']; ['$\mathcal{K}$=' num2str(K)]},'fontsize',30,'FontName','Times','interpreter','latex')
[~, hobj, ~, ~] =legend({'$\rho_{u_0} \sim N(-1,1)$'; ['$\sqrt{\gamma(x,0)}$']; ['$\gamma(x,0)$']; ['DW potential']; ['$\mathcal{K}$=' num2str(K)]},'fontsize',35,'FontName','Times','interpreter','latex');
hl = findobj(hobj,'type','line');
set(hl,'LineWidth',2.5);
set(gca,'FontSize',40)
%% dW CONTROL

% %obtained via KBE solution on PDE grids
ksiGridded = zeros(length(xW),length(tW));
ksiGridded(2:end-1,:) = sigma*(log(v(3:end,:))-log(v(1:end-2,:)))./(2*dxPde);
ksiGridded(1,:) = sigma*(log(v(2,:))-log(v(1,:)))./dxPde;
ksiGridded(end,:) = sigma*(log(v(end,:))-log(v(end-1,:)))./dxPde;

%%
[Xw,Tw] = ndgrid(xW,tW); %meshgrid for the interpolant
%Optimal control using KBE solution
cropN=0.1*Nx; %if we use KBE, I crop 10% of the left boundary and linearly smooth it to avoid instability on the left handside; for HJB no need to do this and nCrop=1
F=griddedInterpolant(Xw(cropN:end,:),flip(Tw(cropN:end,:),2), flip(ksiGridded(cropN:end,:),2),'linear','linear');

ksiStar_PDE= @(x,t) F(x,t).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function for any x and t
%ksiStar2 = @(x,t) ksiStar1(x,t).*(0<ksiStar1(x,t)<100)+100.*(ksiStar1(x,t)>=100)+0*(ksiStar1(x,t)<=0)
%%

%%
% %Plot the extrapolated optimal control
figure
[Xw,Tw] = ndgrid(xW(1:end-4),tW(1:end-41));
p2=surf(Xw, Tw, ksiGridded);
 set(p2,'LineStyle','none')
 xlabel('x','FontName','Times','fontsize',20)
ylabel('t','FontName','Times','fontsize',20)
zlabel('\xi(x,t)','FontName','Times','fontsize',20)

figure
xF  = linspace(xL,K,Nx+1)'; 
[XwF,TwF] = ndgrid(xF,tW);
p2=surf(XwF, TwF, ksiStar_PDE(XwF,TwF));
 set(p2,'LineStyle','none')
 xlabel('x','FontName','Times','fontsize',20)
ylabel('t','FontName','Times','fontsize',20)
zlabel('\xi(x,t)','FontName','Times','fontsize',20)
%% Computing the QoI with and without change of measure wrt W and rho_0
S=10^5; %MC sample size
%Initialization
h_IS=zeros(1,S); h_IS_rho0=h_IS;h_MC=h_IS;  h_IS_W=h_IS; h_CE=h_IS;
runmax_IS=zeros(1,S);runmax_IS_W=runmax_IS;runmax_IS_rho0=runmax_IS;runmax_MC=runmax_IS; runmax_CE=runmax_IS;
u_MC=zeros(1,Kstep+1);u_IS=u_MC;u_IS_W=u_MC;u_IS_rho0=u_MC; u_CE=u_MC;
u0=mu_0+sigma_0*randn(1,S);
u0_shifted_1=mFit+sigFit*randn(1,S);
u0_shifted_2=mFit_both+sigFit_both*randn(1,S);
u0_CE=mu_tilde+sigma_tilde*randn(1,S);
meanL=0;
meanL_W=0;
parfor s=1:S
       u_MC=u0(s);
       u_IS=u0_shifted_2(s);
       u_IS_W=u0(s);
       u_IS_rho0=u0_shifted_1(s);
       u_CE=u0_CE(s);
       L=1;
       k=1;
       t_n=0;
       
       %We integrate until the state hits the threshold or until final time
       
       %IS wrt both
        while t_n<=T && u_IS(k)<K

            dW1 = sqrt(dt)*randn;
            dW2 = sqrt(dt)*randn;

            L=L*exp(-0.5*dt*(ksiStar(u_IS(k),t_n))^2-ksiStar(u_IS(k),t_n)*dW1);
            u_IS(k+1)= u_IS(k)+(aa(u_IS(k))+sigma*ksiStar(u_IS(k),t_n))*dt+sigma*dW1; %Forward Euler 

            k=k+1;
            t_n=t_n+dt;
        end
    
       %IS wrt W(t)
        L_W=1;
        k=1;
        t_n=0;
       
        while t_n<=T && u_IS_W(k)<K
        
        dW2 = sqrt(dt)*randn;
        
        L_W=L_W*exp(-0.5*dt*(ksiStar(u_IS_W(k),t_n))^2-ksiStar(u_IS_W(k),t_n)*dW2);
        u_IS_W(k+1)= u_IS_W(k)+(aa(u_IS_W(k))+sigma*ksiStar(u_IS_W(k),t_n))*dt+sigma*dW2; %Forward Euler 

        k=k+1;
        t_n=t_n+dt;
        end
    
       %MC simulation and IS wrt rho_0 
        for n=1:Kstep
            u_MC(n+1)=u_MC(n)+aa(u_MC(n))*dt+sigma*sqrt(dt)*randn;
            u_IS_rho0(n+1)= u_IS_rho0(n)+(aa(u_IS_rho0(n)))*dt+sigma*sqrt(dt)*randn; %Forward Euler 
            u_CE(n+1)=u_CE(n)+aa(u_CE(n))*dt+sigma*sqrt(dt)*randn;
        end
    meanL=meanL+L;
    meanL_W=meanL_W+L_W;

    h_IS(s)=(max(u_IS)>=K)*L;
    h_IS_W(s)=(max(u_IS_W)>=K)*L_W;
    h_IS_rho0(s)=(max(u_IS_rho0)>=K);
    h_MC(s)=(max(u_MC)>=K);
    h_CE(s)=(max(u_CE)>=K);

    runmax_IS(s)=max(u_IS);
    runmax_IS_W(s)=max(u_IS_W);
    runmax_IS_rho0(s)=max(u_IS_rho0)
    runmax_MC(s)=max(u_MC);
    runmax_CE(s)=max(u_CE);
end
% fnm = sprintf('DWsig1_all_K%d.mat',K);
% save(fnm,'-nocompression','-v7.3') 
% end
%%
meanL=meanL/S
meanL_W=meanL_W/S
Weight_CE=(sigma_tilde/sigma_0)*exp(-(u0_CE-mu_0).^2./(2*sigma_0^2)+(u0_CE-mu_tilde).^2./(2*sigma_tilde^2));
Weight_PDE_1=(sigFit/sigma_0)*exp(-(u0_shifted_1-mu_0).^2./(2*sigma_0^2)+(u0_shifted_1-mFit).^2./(2*sigFit^2));
Weight_PDE_2=(sigFit_both/sigma_0)*exp(-(u0_shifted_2-mu_0).^2./(2*sigma_0^2)+(u0_shifted_2-mFit_both).^2./(2*sigFit_both^2));
alpha_hat_IS_both=mean(h_IS.*Weight_PDE_2)
alpha_hat_IS_W=mean(h_IS_W)
alpha_hat_IS_rho0=mean(h_IS_rho0.*Weight_PDE_1)
alpha_hat_MC=mean(h_MC)
alpha_hat_CE=mean(h_CE.*Weight_CE)
Vaprx_IS_both=var(h_IS.*Weight_PDE_2)
Vaprx_IS_W=var(h_IS_W)
Vaprx_IS_rho0=var(h_IS_rho0.*Weight_PDE_1)
Vaprx_MC=var(h_MC)
Vaprx_CE=var(h_CE.*Weight_CE)
RelError_MC=1.96*sqrt(Vaprx_MC)/(alpha_hat_MC*sqrt(S))
RelError_IS_both=1.96*sqrt(Vaprx_IS_both)/(alpha_hat_IS_both*sqrt(S))
RelError_IS_W=1.96*sqrt(Vaprx_IS_W)/(alpha_hat_IS_W*sqrt(S))
RelError_IS_rho0=1.96*sqrt(Vaprx_IS_rho0)/(alpha_hat_IS_rho0*sqrt(S))
RelError_CE=1.96*sqrt(Vaprx_CE)/(alpha_hat_CE*sqrt(S))
%varRatio_approx=(RelError_MC/RelError_IS)^2
varRatio_both=Vaprx_MC/Vaprx_IS_both
varRatio_W=Vaprx_MC/Vaprx_IS_W
varRatio_rho0=Vaprx_MC/Vaprx_IS_rho0
varRatio_CE=Vaprx_MC/Vaprx_CE

% %% Bootstrapping
% C = {'red','magenta'}; % Cell array of colros.
% Cc={'green','blue'};
% Ccc = {'cyan', 'yellow'};
% B=10^4;
% conf=95;
% Krng=[1.5 2 2.5 3];                     %the threshold DW
% q_both=[h_both_K15; h_both_K2; h_both_K25; h_both_K3];
% q_IS_W=[h_IS_W_K15; h_IS_W_K2; h_IS_W_K25; h_IS_W_K3];
% q_IS_rho0=[h_IS_rho0_K15; h_IS_rho0_K2; h_IS_rho0_K25; h_IS_rho0_K3];
% 
% for n=1:2
%         for i=1:length(Krng)
%         K=Krng(i);
%         if isempty(gcp)
%            parpool;
%         end
%         opt = statset('UseParallel',true);
% 
%         stats_IS_both = bootstrp(B, @(x) [mean(x) std(x)], q_both(i,:), 'Options', opt);
%         stats_IS_W = bootstrp(B, @(x) [mean(x) std(x)], q_IS_W(i,:), 'Options', opt);
%         stats_IS_rho0 = bootstrp(B, @(x) [mean(x) std(x)], q_IS_rho0(i,:), 'Options', opt);
% 
%         SmplStd_IS_both(i)=mean(stats_IS_both(:,2));
%         SmplStd_IS_W(i) =mean(stats_IS_W(:,2));
%         SmplStd_IS_rho0(i) =mean(stats_IS_rho0(:,2));
% 
%         CId_IS_both(i) =  prctile(stats_IS_both(:,2), (100-conf)/2);
%         CIu_IS_both(i) =  prctile(stats_IS_both(:,2), (100-(100-conf)/2));
% 
%         CId_IS_W(i) = prctile(stats_IS_W(:,2), (100-conf)/2);
%         CIu_IS_W(i) =  prctile(stats_IS_W(:,2), (100-(100-conf)/2));
% 
%         CId_IS_rho0(i) = prctile(stats_IS_rho0(:,2), (100-conf)/2);
%         CIu_IS_rho0(i) =  prctile(stats_IS_rho0(:,2), (100-(100-conf)/2));
%         end
%     figure(1)
%     plot_ci(Krng',[SmplStd_IS_both; CId_IS_both; CIu_IS_both],'PatchColor', C{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', C{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', C{n}, 'YScale', 'log');
%     grid on
%     title('IS wrt both \rho_0 and W(t)')
%     hold on
%     figure(2)
%     plot_ci(Krng',[SmplStd_IS_W; CId_IS_W; CIu_IS_W], 'PatchColor', Cc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Cc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Cc{n}, 'YScale', 'log');
%     grid on
%     hold on
%     title('IS with W(t)')
%     figure(3)
%     plot_ci(Krng',[SmplStd_IS_rho0; CId_IS_rho0; CIu_IS_rho0], 'PatchColor', Ccc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Ccc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Ccc{n}, 'YScale', 'log');
%     grid on
%     hold on
%     title('IS with \rho_0')
% end
%% Plot pdf of running maximum
[f_both,xboth] = ksdensity(runmax_IS, 'function', 'pdf');
[f_IS_W,x_W] = ksdensity(runmax_IS_W,'function', 'pdf'); 
[f_IS_rho0,x_rho0] = ksdensity(runmax_IS_rho0,'function', 'pdf'); 
[f_MC,x_MC] = ksdensity(runmax_MC, 'function', 'pdf'); 
[f_CE,x_CE] = ksdensity(runmax_CE, 'function', 'pdf'); 
figure
plot(x_MC, f_MC, 'r-', x_rho0, f_IS_rho0, 'b:', x_W, f_IS_W, 'c--',xboth, f_both, 'k-.', 'Linewidth',4);
% hold on
% plot([K K],[y(1) y(2)], '-k', 'LineWidth', 4)
% hold off
xlabel('u', 'fontsize', 18)
[~, hobj, ~, ~] =legend('MC', 'IS wrt \rho_0', 'IS wrt W(t)', 'IS wrt both','fontsize',25)
ylabel('density function of QoI','fontsize', 18)
title('$\sigma_0=0.5, \mathcal{K}=1$','interpreter', 'latex','fontsize', 20)
pp = get(gca,'XTickLabel');
set(gca,'XTickLabel',pp,'FontName','Times','fontsize',20)
hl = findobj(hobj,'type','line');
set(hl,'LineWidth',2.5);
set(gca,'FontSize',40)

% %% Plot pdf of running maximum
% [f_both,xboth] = ksdensity(h_IS.*Weight_PDE_2, 'function', 'pdf');
% area_both = trapz(xboth,f_both);
% f_both = f_both/area_both/20;
% [f_IS_W,x_W] = ksdensity(h_IS_W,'function', 'pdf'); 
% area_W = trapz(x_W,f_IS_W);
% f_IS_W = f_IS_W/area_W/20;
% [f_IS_rho0,x_rho0] = ksdensity(h_IS_rho0.*Weight_PDE_1,'function', 'pdf'); 
% [f_MC,x_MC] = ksdensity(h_MC, 'function', 'pdf'); 
% [f_CE,x_CE] = ksdensity(h_CE.*Weight_CE, 'function', 'pdf'); 
% figure
% plot(x_MC, f_MC, 'r-', x_rho0, f_IS_rho0, 'b:', x_W, f_IS_W, 'c--',xboth, f_both, 'k-.', 'Linewidth',4);
% % hold on
% % plot([K K],[y(1) y(2)], '-k', 'LineWidth', 4)
% % hold off
% xlabel('u', 'fontsize', 18)
% [~, hobj, ~, ~] =legend('MC', 'IS wrt \rho_0', 'IS wrt W(t)', 'IS wrt both','fontsize',25)
% ylabel('density function of QoI','fontsize', 18)
% title('$\sigma_0=1, \mathcal{K}=2$','interpreter', 'latex','fontsize', 20)
% pp = get(gca,'XTickLabel');
% set(gca,'XTickLabel',pp,'FontName','Times','fontsize',20)
% hl = findobj(hobj,'type','line');
% set(hl,'LineWidth',2.5);
% set(gca,'FontSize',40)

 %% test conditional varianca
% %% Computing the QoI with and without change of measure wrt W and rho_0
% J=1000;
% Vaprx_IS_both=0;
% Vaprx_IS_W=0;
% Vaprx_IS_rho0=0;
% Vaprx_MC=0;
% Vaprx_CE=0;
% for j=1:J
% S=10^4; %MC sample size
% %Initialization
% h_IS=zeros(1,S); h_IS_rho0=h_IS;h_MC=h_IS;  h_IS_W=h_IS; h_CE=h_IS;
% runmax_IS=zeros(1,S);runmax_IS_W=runmax_IS;runmax_IS_rho0=runmax_IS;runmax_IS_MC=runmax_IS; runmax_CE=runmax_IS;
% u_MC=zeros(1,Kstep+1);u_IS=u_MC;u_IS_W=u_MC;u_IS_rho0=u_MC; u_CE=u_MC;
% u0=mu_0+sigma_0*randn(1,S);
% u0_shifted_1=mFit+sigFit*randn(1,S);
% u0_shifted_2=mFit_both+sigFit_both*randn(1,S);
% u0_CE=mu_tilde+sigma_tilde*randn(1,S);
% meanL=0;
% meanL_W=0;
% parfor s=1:S
%        u_MC=u0(s);
%        u_IS=u0_shifted_2(s);
%        u_IS_W=u0(s);
%        u_IS_rho0=u0_shifted_1(s);
%        u_CE=u0_CE(s);
%        L=1;
%        k=1;
%        t_n=0;
%        
%        %We integrate until the state hits the threshold or until final time
%        
%        %IS wrt both
%         while t_n<=T && u_IS(k)<K
% 
%             dW1 = sqrt(dt)*randn;
%             dW2 = sqrt(dt)*randn;
% 
%             L=L*exp(-0.5*dt*(ksiStar(u_IS(k),t_n))^2-ksiStar(u_IS(k),t_n)*dW1);
%             u_IS(k+1)= u_IS(k)+(aa(u_IS(k))+sigma*ksiStar(u_IS(k),t_n))*dt+sigma*dW1; %Forward Euler 
% 
%             k=k+1;
%             t_n=t_n+dt;
%         end
%     
%        %IS wrt W(t)
%         L_W=1;
%         k=1;
%         t_n=0;
%        
%         while t_n<=T && u_IS_W(k)<K
%         
%         dW2 = sqrt(dt)*randn;
%         
%         L_W=L_W*exp(-0.5*dt*(ksiStar(u_IS_W(k),t_n))^2-ksiStar(u_IS_W(k),t_n)*dW2);
%         u_IS_W(k+1)= u_IS_W(k)+(aa(u_IS_W(k))+sigma*ksiStar(u_IS_W(k),t_n))*dt+sigma*dW2; %Forward Euler 
% 
%         k=k+1;
%         t_n=t_n+dt;
%         end
%     
%        %MC simulation and IS wrt rho_0 
%         for n=1:Kstep
%             u_MC(n+1)=u_MC(n)+aa(u_MC(n))*dt+sigma*sqrt(dt)*randn;
%             u_IS_rho0(n+1)= u_IS_rho0(n)+(aa(u_IS_rho0(n)))*dt+sigma*sqrt(dt)*randn; %Forward Euler 
%             u_CE(n+1)=u_CE(n)+aa(u_CE(n))*dt+sigma*sqrt(dt)*randn;
%         end
%     meanL=meanL+L;
%     meanL_W=meanL_W+L_W;
% 
%     h_IS(s)=(max(u_IS)>=K)*L;
%     h_IS_W(s)=(max(u_IS_W)>=K)*L_W;
%     h_IS_rho0(s)=(max(u_IS_rho0)>=K);
%     h_MC(s)=(max(u_MC)>=K);
%     h_CE(s)=(max(u_CE)>=K);
% 
%     runmax_IS(s)=max(u_IS);
%     runmax_IS_W(s)=max(u_IS_W);
%     runmax_IS_rho0(s)=max(u_IS_rho0)
%     runmax_MC(s)=max(u_MC);
%     runmax_CE(s)=max(u_CE);
% end
% Weight_CE=(sigma_tilde/sigma_0)*exp(-(u0_CE-mu_0).^2./(2*sigma_0^2)+(u0_CE-mu_tilde).^2./(2*sigma_tilde^2));
% Weight_PDE_1=(sigFit/sigma_0)*exp(-(u0_shifted_1-mu_0).^2./(2*sigma_0^2)+(u0_shifted_1-mFit).^2./(2*sigFit^2));
% Weight_PDE_2=(sigFit_both/sigma_0)*exp(-(u0_shifted_2-mu_0).^2./(2*sigma_0^2)+(u0_shifted_2-mFit_both).^2./(2*sigFit_both^2));
% 
% alpha_hat_IS_both(j)=mean(h_IS.*Weight_PDE_2);
% alpha_hat_IS_W(j)=mean(h_IS_W);
% alpha_hat_IS_rho0(j)=mean(h_IS_rho0.*Weight_PDE_1);
% alpha_hat_MC(j)=mean(h_MC);
% alpha_hat_CE(j)=mean(h_CE.*Weight_CE);
% 
% Vaprx_IS_both=Vaprx_IS_both+var(h_IS.*Weight_PDE_2);
% Vaprx_IS_W=Vaprx_IS_W+var(h_IS_W);
% Vaprx_IS_rho0=Vaprx_IS_rho0+var(h_IS_rho0.*Weight_PDE_1);
% Vaprx_MC=Vaprx_MC+var(h_MC);
% Vaprx_CE=Vaprx_CE+var(h_CE.*Weight_CE);
% end
% %%
% total_var_W=var(h_IS_W)
% 
% var_cond_exp_both=var(alpha_hat_IS_both)
% var_cond_exp_W=var(alpha_hat_IS_W)
% var_cond_exp_rho0=var(alpha_hat_IS_rho0)
% var_cond_exp_MC=var(alpha_hat_MC)
% var_cond_exp_CE=var(alpha_hat_CE)
% %sample averages of variances
% Vaprx_IS_both=Vaprx_IS_both/j
% Vaprx_IS_W=Vaprx_IS_W/j
% Vaprx_IS_rho0=Vaprx_IS_rho0/j
% Vaprx_MC=Vaprx_MC/j
% Vaprx_CE=Vaprx_CE/j

% %% Test2 time to achieve K
% S=10^5; %MC sample size
% %Initialization
% h_IS=zeros(1,S); h_IS_rho0=h_IS;h_MC=h_IS;  h_IS_W=h_IS; h_CE=h_IS;
% runmax_IS=zeros(1,S);runmax_IS_W=runmax_IS;runmax_IS_rho0=runmax_IS;runmax_IS_MC=runmax_IS; runmax_CE=runmax_IS;
% u_MC=zeros(1,Kstep+1);u_IS=u_MC;u_IS_W=u_MC;u_IS_rho0=u_MC; u_CE=u_MC;
% u0=mu_0+sigma_0*randn(1,S);
% u0_shifted_1=mFit+sigFit*randn(1,S);
% u0_shifted_2=mFit_both+sigFit_both*randn(1,S);
% u0_CE=mu_tilde+sigma_tilde*randn(1,S);
% meanL=0;
% meanL_W=0;
% t=linspace(0,T,Kstep+1);
% timetoachieveK_MC=[];
% timetoachieveK_both=[];
% timetoachieveK_W=[];
% timetoachieveK_rho0=[];
% timetoachieveK_CE=[];
% parfor s=1:S
%        u_MC=u0(s);
%        u_IS=u0_shifted_2(s);
%        u_IS_W=u0(s);
%        u_IS_rho0=u0_shifted_1(s);
%        u_CE=u0_CE(s);
%        L=1;
%        k=1;
%        t_n=0;
%        
%        %We integrate until the state hits the threshold or until final time
%        
%        %IS wrt both
%         while t_n<=T && u_IS(k)<K
% 
%             dW1 = sqrt(dt)*randn;
%             dW2 = sqrt(dt)*randn;
% 
%             L=L*exp(-0.5*dt*(ksiStar(u_IS(k),t_n))^2-ksiStar(u_IS(k),t_n)*dW1);
%             u_IS(k+1)= u_IS(k)+(aa(u_IS(k))+sigma*ksiStar(u_IS(k),t_n))*dt+sigma*dW1; %Forward Euler 
% 
%             k=k+1;
%             t_n=t_n+dt;
%         end
%     
%        %IS wrt W(t)
%         L_W=1;
%         k=1;
%         t_n=0;
%        
%         while t_n<=T && u_IS_W(k)<K
%         
%         dW2 = sqrt(dt)*randn;
%         
%         L_W=L_W*exp(-0.5*dt*(ksiStar(u_IS_W(k),t_n))^2-ksiStar(u_IS_W(k),t_n)*dW2);
%         u_IS_W(k+1)= u_IS_W(k)+(aa(u_IS_W(k))+sigma*ksiStar(u_IS_W(k),t_n))*dt+sigma*dW2; %Forward Euler 
% 
%         k=k+1;
%         t_n=t_n+dt;
%         end
%     
%        %MC simulation and IS wrt rho_0 
%         for n=1:Kstep
%             u_MC(n+1)=u_MC(n)+aa(u_MC(n))*dt+sigma*sqrt(dt)*randn;
%             u_IS_rho0(n+1)= u_IS_rho0(n)+(aa(u_IS_rho0(n)))*dt+sigma*sqrt(dt)*randn; %Forward Euler 
%             u_CE(n+1)=u_CE(n)+aa(u_CE(n))*dt+sigma*sqrt(dt)*randn;
%         end
%     meanL=meanL+L;
%     meanL_W=meanL_W+L_W;
% 
%     h_IS(s)=(max(u_IS)>=K)*L;
%     h_IS_W(s)=(max(u_IS_W)>=K)*L_W;
%     h_IS_rho0(s)=(max(u_IS_rho0)>=K);
%     h_MC(s)=(max(u_MC)>=K);
%     h_CE(s)=(max(u_CE)>=K);
% 
%     runmax_IS(s)=max(u_IS);
%     runmax_IS_W(s)=max(u_IS_W);
%     runmax_IS_rho0(s)=max(u_IS_rho0);
%     runmax_MC(s)=max(u_MC);
%     runmax_CE(s)=max(u_CE);
%     
%     if runmax_MC(s)>=K
%     [MaxValue_MC, indx_MC]=max(u_MC);
%     timetoachieveK_MC=[timetoachieveK_MC; t(indx_MC)];
%     end
%     
%     if runmax_IS(s)>=K
%     [MaxValue_both, indx_both]=max(u_IS);
%     timetoachieveK_both=[timetoachieveK_both; t(indx_both)];
%     end
%     if runmax_IS_W(s)>=K
%     [MaxValue_W, indx_W]=max(u_IS_W);
%     timetoachieveK_W=[timetoachieveK_W; t(indx_W)];
%     end
%     if runmax_IS_rho0(s)>=K
%     [MaxValue_rho0, indx_rho0]=max(u_IS_rho0);
%     timetoachieveK_rho0=[timetoachieveK_rho0; t(indx_rho0)];
%     end
%     if runmax_CE(s)>=K
%     [MaxValue_CE, indx_CE]=max(u_CE);
%     timetoachieveK_CE=[timetoachieveK_CE; t(indx_CE)];
%     end   
% end
% %% Plot both kernel density for QoI and time to achiev K
% figure
% subplot(1,2,1)
% [f_both,xboth] = ksdensity(runmax_IS, 'function', 'pdf');
% [f_IS_W,x_W] = ksdensity(runmax_IS_W,'function', 'pdf'); 
% [f_IS_rho0,x_rho0] = ksdensity(runmax_IS_rho0,'function', 'pdf'); 
% [f_MC,x_MC] = ksdensity(runmax_MC, 'function', 'pdf'); 
% [f_CE,x_CE] = ksdensity(runmax_CE, 'function', 'pdf'); 
% plot(x_MC, f_MC, 'r-', x_rho0, f_IS_rho0, 'b:', x_W, f_IS_W, 'm--',xboth, f_both, 'k-.', 'Linewidth',4);
% % hold on
% % plot([K K],[y(1) y(2)], '-k', 'LineWidth', 4)
% % hold off
% xlim([-2 2])
% xlabel('u', 'fontsize', 18)
% [~, hobj, ~, ~] =legend('MC', 'IS wrt \rho_0', 'IS wrt W(t)', 'IS wrt both','fontsize',30)
% ylabel('density function of QoI','fontsize', 18)
% pp = get(gca,'XTickLabel');
% set(gca,'XTickLabel',pp,'FontName','Times','fontsize',20)
% hl = findobj(hobj,'type','line');
% set(hl,'LineWidth',2.5);
% set(gca,'FontSize',40)
% 
% subplot(1,2,2)
% histogram(timetoachieveK_rho0,'Normalization', 'pdf','facecolor','b','facealpha',0.8,'edgecolor','b')
% %ylim([0 20])
% xlim([0 1])
% xlabel('t')
% ylabel('density function of time', 'fontsize', 18)
% pp = get(gca,'XTickLabel');
% set(gca,'XTickLabel',pp,'FontName','Times','fontsize',20)
% hl = findobj(hobj,'type','line');
% set(hl,'LineWidth',2.5);
% set(gca,'FontSize',40)
% hold on
% 
% %subplot(1,3,2)
% histogram(timetoachieveK_W,'Normalization', 'pdf','facecolor','m','facealpha',0.8,'edgecolor','m')
% xlim([0 1])
% %ylim([0 20])
% xlabel('t','fontsize', 18)
% pp = get(gca,'XTickLabel');
% set(gca,'XTickLabel',pp,'FontName','Times','fontsize',20)
% hl = findobj(hobj,'type','line');
% set(hl,'LineWidth',2.5);
% set(gca,'FontSize',40)
% 
% %subplot(1,3,3)
% histogram(timetoachieveK_both,'Normalization', 'pdf','facecolor','k','facealpha',0.8,'edgecolor','k')
% xlim([0 1])
% %ylim([0 20])
% %title('$\sigma_0=1, \mathcal{K}=2$','interpreter', 'latex','fontsize', 40)
% xlabel('t','fontsize', 18)
% legend('IS wrt \rho_0', 'IS wrt W(t)', 'IS wrt both')
% pp = get(gca,'XTickLabel');
% set(gca,'XTickLabel',pp,'FontName','Times','fontsize',20)
% hl = findobj(hobj,'type','line');
% set(hl,'LineWidth',2.5);
% set(gca,'FontSize',40)
% sgtitle('$\sigma_0=0.2, \mathcal{K}=0.5$','interpreter', 'latex','fontsize', 40)
% %%
% figure
% [time_both,tboth] = ksdensity(timetoachieveK_both, 'function', 'pdf');
% [time_IS_W,t_W] = ksdensity(timetoachieveK_W,'function', 'pdf'); 
% [time_IS_rho0,t_rho0] = ksdensity(timetoachieveK_rho0,'function', 'pdf'); 
% [time_CE,t_CE] = ksdensity(timetoachieveK_CE, 'function', 'pdf');
% [time_MC,t_MC] = ksdensity(timetoachieveK_MC, 'function', 'pdf');
% plot(t_MC, time_MC, 'r-', t_rho0, time_IS_rho0, 'b:',t_CE, time_CE, 'g:', t_W, time_IS_W, 'm--',tboth, time_both, 'k-.', 'Linewidth',4);
% xlim([0 1])
% xlabel('t', 'fontsize', 18)
% ylabel('density function of time','fontsize', 18)
% pp = get(gca,'XTickLabel');
% set(gca,'XTickLabel',pp,'FontName','Times','fontsize',20)
% hl = findobj(hobj,'type','line');
% set(hl,'LineWidth',2.5);
% set(gca,'FontSize',40)
% sgtitle('$\sigma_0=1, \mathcal{K}=2$','interpreter', 'latex','fontsize', 40)
%%
aaa    = 0.194444444444444;    % drift coefficient
bb    = 0.5;                  % diffusion coefficient

% Exact solution to the KBE IBVP in the constant coefficient case:
R_m = @(t,x) (K-x-aaa*(T-t))./sqrt(2*bb^2*(T-t));
R_p = @(t,x) (K-x+aaa*(T-t))./sqrt(2*bb^2*(T-t));
gamma = @(t,x) 0.5*(erfc(R_m(t,x)) + exp(2*aaa*(K-x)/bb^2).*erfc(R_p(t,x)));

dgamma_dx = @(t,x) 1./sqrt(2*pi*bb^2*(T-t)) ...
  .*( exp(-R_m(t,x).^2) + exp(-R_p(t,x).^2+2*aaa*(K-x)/bb^2) ) ...
  - aaa/bb^2*exp(2*aaa*(K-x)/bb^2).*erfc(R_p(t,x));

% Optimal control obtained as b*d/dx log(gamma)
xi_opt = @(t,x) bb*dgamma_dx(t,x)./gamma(t,x);
%%
ksiStar_Asmp= @(x,t) ((1/sigma)*((K-x)./(T-t))-aa(x)).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function based on asymptotic apprx.
%% 
ksiStar = @(x,t, dt_EM) sum([ksiStar_PDE(x,t)*(T-t>=dt_EM) ksiStar_Asmp(x,t)*(T-t<dt_EM)], 'omitnan');

%% Computing the QoI with and without change of measure wrt W and rho_0
S=10^4; %MC sample size
Srng=[1e4];
for ss=1:length(Srng)
    S=Srng(ss);
%Initialization
h_IS=zeros(1,S); h_IS_rho0=h_IS;h_MC=h_IS;  h_IS_W=h_IS; h_CE=h_IS; timeHit=h_IS;
runmax_IS=zeros(1,S);runmax_IS_W=runmax_IS;runmax_IS_rho0=runmax_IS;runmax_MC=runmax_IS; runmax_CE=runmax_IS;
u_MC=zeros(1,Kstep+1);Y=u_MC; u_IS=u_MC;u_IS_W=u_MC;u_IS_rho0=u_MC; u_CE=u_MC;
u0=mu_0+sigma_0*randn(1,S);
u0_shifted_1=mFit+sigFit*randn(1,S);
u0_shifted_2=mFit_both+sigFit_both*randn(1,S);
u0_CE=mu_tilde+sigma_tilde*randn(1,S);
meanL=0;
meanL_W=0;
dt_const=0.1;
% Kstep=round(T/dt_const);
t0=0;
trng1=[];
trng2=[];
parfor s=1:S
       u_MC=u0(s);
       u_IS=u0_shifted_2(s); 
       u_IS_W=u0(s);
       u_IS_rho0=u0_shifted_1(s);
       u_CE=u0_CE(s);
       L=1;
       Q=1;
       k=1;
       t_n=0;
       dt=dt_const;
       %We integrate until the state hits the threshold or until final time
       trng1=t0;
       trng2=t0;
       %IS wrt both
       Y=0*u0_shifted_2(s);
        while t_n<=T && u_IS(k)<K && dt>0.0000001
              dt=min(dt_const, 0.5*(T-t_n));

            dW1 = sqrt(dt)*randn;
            L=L*exp(-0.5*dt*(ksiStar(u_IS(k),t_n, dt_const))^2-ksiStar(u_IS(k),t_n, dt_const)*dW1);
            Y(k+1)=Y(k)+ksiStar(u_IS(k),t_n, dt_const)*dW1+0.5*(ksiStar(u_IS(k),t_n, dt_const))^2*dt;
            u_IS(k+1)= u_IS(k)+(aa(u_IS(k))+sigma*ksiStar(u_IS(k),t_n, dt_const))*dt+sigma*dW1; %Forward Euler 
            q= exp(-2*max(K-u_IS(k),0)*max(K-u_IS(k+1),0)/(sigma^2*dt));
            if rand<q 
                t_n=t_n+0.5*dt; u_IS(k+1)=K;
            else
                t_n=t_n+dt;
            end
            Q=Q*(q);
            k=k+1;
            trng1=[trng1 t_n];
        end
timeHit(s)=trng1(end);      
% figure(3)
% grid on
% plot(trng1, u_IS', 'r', trng1(end), u_IS(end), 'ko', 'Linewidth',2)
% xlabel('t','Linewidth',2)
% ylabel('X(t)','Linewidth',2)
% hold on
% figure(4)
% plot( trng1, Y, 'b',trng1(end), Y(end), 'ko','Linewidth',2)
% xlabel('t','Linewidth',2)
% ylabel('Y(t)','Linewidth',2)
% hold on
% grid on
% pause()
       %IS wrt W(t)
        dt=dt_const;
        L_W=1;
        Q_W=1;
        k=1;
        t_n=0;
        while t_n<=T && u_IS_W(k)<K && dt>0.0000001
            dt=min(dt_const, 0.5*(T-t_n));
            dW2 = sqrt(dt)*randn;

            L_W=L_W*exp(-0.5*dt*(ksiStar(u_IS_W(k),t_n, dt_const))^2-ksiStar(u_IS_W(k),t_n,dt_const)*dW2);
            u_IS_W(k+1)= u_IS_W(k)+(aa(u_IS_W(k))+sigma*ksiStar(u_IS_W(k),t_n,dt_const))*dt+sigma*dW2; %Forward Euler 
            q_W= exp(-2*max(K-u_IS_W(k),0)*max(K-u_IS_W(k+1),0)/(sigma^2*dt));
            if rand<q_W 
                t_n=t_n+0.5*dt; u_IS_W(k+1)=K;
            else
                t_n=t_n+dt;
            end
            Q_W=Q_W*(q_W);
            k=k+1;
            trng2=[trng2 t_n];
        end
% figure(1)
% plot(trng2, u_IS_W', 'r', 'Linewidth', 2)
% hold on
% pause()
       %MC simulation and IS wrt rho_0 
        Q_MC=1;
        Q_IS_rho0=1;
        Q_CE=1;
        n=1;
        t_n=0;
        while t_n<=T && u_MC(n)<K
            u_MC(n+1)=u_MC(n)+aa(u_MC(n))*dt_const+sigma*sqrt(dt_const)*randn;
            
            q_MC= exp(-2*max(K-u_MC(n),0)*max(K-u_MC(n+1),0)/(sigma^2*dt_const));
            if rand<q_MC 
            u_MC(n+1)=K
            end
            t_n=t_n+dt_const;
            n=n+1;
            Q_MC=Q_MC*(q_MC);
        end
%        
        n=1;
        t_n=0;
        while t_n<=T && u_IS_rho0(n)<K
            u_IS_rho0(n+1)= u_IS_rho0(n)+(aa(u_IS_rho0(n)))*dt_const+sigma*sqrt(dt_const)*randn; %Forward Euler 

            q_IS_rho0=exp(-2*max(K-u_IS_rho0(n),0)*max(K-u_IS_rho0(n+1),0)/(sigma^2*dt_const));
            if rand<q_IS_rho0 
            u_IS_rho0(n+1)=K
            end
            Q_IS_rho0=Q_IS_rho0*(q_IS_rho0);
            t_n=t_n+dt_const;
            n=n+1;
        end
        
        n=1;
        t_n=0;
        while t_n<=T && u_CE(n)<K
            u_CE(n+1)=u_CE(n)+aa(u_CE(n))*dt_const+sigma*sqrt(dt_const)*randn;

            q_CE= exp(-2*max(K-u_CE(n),0)*max(K-u_CE(n+1),0)/(sigma^2*dt_const));
            if rand<q_CE 
            u_CE(n+1)=K;
            end
            t_n=t_n+dt_const;
            n=n+1;
           Q_CE=Q_CE*(q_CE);
        end
% figure(1)
% plot(0:0.01:1, u_MC', 'r', 0:0.01:1, u_IS_rho0', 'g')
% hold on
% pause()

    meanL=meanL+L;
    meanL_W=meanL_W+L_W;
    h_IS(s)=(max(u_IS)>=K)*L;
    h_IS_W(s)=(max(u_IS_W)>=K)*L_W;
    h_IS_rho0(s)=(max(u_IS_rho0)>=K);
    h_MC(s)=(max(u_MC)>=K);
    h_CE(s)=(max(u_CE)>=K);
    
%     h_IS(s)=L*Q;
%     h_IS_W(s)=L_W*Q_W;
%     h_IS_rho0(s)=Q_IS_rho0;
%     h_MC(s)=Q_MC;
%     h_CE(s)=Q_CE;

    runmax_IS(s)=max(u_IS);
    runmax_IS_W(s)=max(u_IS_W);
    runmax_IS_rho0(s)=max(u_IS_rho0);
    runmax_MC(s)=max(u_MC);
    runmax_CE(s)=max(u_CE);
    
end

meanL=meanL/S
meanL_W=meanL_W/S
Weight_CE=(sigma_tilde/sigma_0)*exp(-(u0_CE-mu_0).^2./(2*sigma_0^2)+(u0_CE-mu_tilde).^2./(2*sigma_tilde^2));
Weight_PDE_1=(sigFit/sigma_0)*exp(-(u0_shifted_1-mu_0).^2./(2*sigma_0^2)+(u0_shifted_1-mFit).^2./(2*sigFit^2));
Weight_PDE_2=(sigFit_both/sigma_0)*exp(-(u0_shifted_2-mu_0).^2./(2*sigma_0^2)+(u0_shifted_2-mFit_both).^2./(2*sigFit_both^2));
alpha_hat_IS_both(ss)=mean(h_IS.*Weight_PDE_2)
alpha_hat_IS_W(ss)=mean(h_IS_W)
alpha_hat_IS_rho0(ss)=mean(h_IS_rho0.*Weight_PDE_1)
alpha_hat_MC(ss)=mean(h_MC)
alpha_hat_CE(ss)=mean(h_CE.*Weight_CE)
Vaprx_IS_both=var(h_IS.*Weight_PDE_2)
Vaprx_IS_W=var(h_IS_W)
Vaprx_IS_rho0=var(h_IS_rho0.*Weight_PDE_1)
Vaprx_MC=var(h_MC)
Vaprx_CE=var(h_CE.*Weight_CE)
RelError_MC=1.96*sqrt(Vaprx_MC)/(alpha_hat_MC(ss)*sqrt(S))
RelError_IS_both=1.96*sqrt(Vaprx_IS_both)/(alpha_hat_IS_both(ss)*sqrt(S))
RelError_IS_W=1.96*sqrt(Vaprx_IS_W)/(alpha_hat_IS_W(ss)*sqrt(S))
RelError_IS_rho0=1.96*sqrt(Vaprx_IS_rho0)/(alpha_hat_IS_rho0(ss)*sqrt(S))
RelError_CE=1.96*sqrt(Vaprx_CE)/(alpha_hat_CE(ss)*sqrt(S))
%varRatio_approx=(RelError_MC/RelError_IS)^2
varRatio_both=Vaprx_MC/Vaprx_IS_both
varRatio_W=Vaprx_MC/Vaprx_IS_W
varRatio_rho0=Vaprx_MC/Vaprx_IS_rho0
varRatio_CE=Vaprx_MC/Vaprx_CE

IS_both_CIu(ss)=alpha_hat_IS_both(ss)+1.96*sqrt(Vaprx_IS_both)/sqrt(S);
IS_both_CId(ss)=alpha_hat_IS_both(ss)-1.96*sqrt(Vaprx_IS_both)/sqrt(S);
IS_W_CIu(ss)=alpha_hat_IS_W(ss)+1.96*sqrt(Vaprx_IS_W)/sqrt(S);
IS_W_CId(ss)=alpha_hat_IS_W(ss)-1.96*sqrt(Vaprx_IS_W)/sqrt(S);
IS_rho0_CIu(ss)=alpha_hat_IS_rho0(ss)+1.96*sqrt(Vaprx_IS_rho0)/sqrt(S);
IS_rho0_CId(ss)=alpha_hat_IS_rho0(ss)-1.96*sqrt(Vaprx_IS_rho0)/sqrt(S);
MC_CIu(ss)=alpha_hat_MC(ss)+1.96*sqrt(Vaprx_MC)/sqrt(S);
MC_CId(ss)=alpha_hat_MC(ss)-1.96*sqrt(Vaprx_MC)/sqrt(S);
end
%%
figure
histogram(timeHit,'Normalization', 'pdf')
xlabel('t')
ylabel('density function')

figure
f=ksdensity(timeHit,0:0.01:1,'Function', 'pdf');
plot(0:0.01:1,f,'Linewidth',2)
ylabel('density function')
xlabel('t')
%% Confidence interval
figure(16)
title('95% confidence interval')
semilogx(Srng, alpha_hat_IS_both, 'bo', Srng, IS_both_CIu , 'b--', Srng, IS_both_CId , 'b--', 'Linewidth', 2)
hold on
semilogx(Srng, alpha_hat_IS_W, 'go', Srng, IS_W_CIu , 'g--', Srng, IS_W_CId , 'g--', 'Linewidth', 2)
hold on
semilogx(Srng, alpha_hat_MC, 'ro', Srng, MC_CIu , 'r--', Srng, MC_CId , 'r--','Linewidth', 2)
xlabel('Sample size M')
legend( 'IS wrt both','', '', 'IS wrt W(t)','', '', 'MC')
%%
confplot(Srng,alpha_hat_IS_both,Srng, IS_both_CId,Srng, IS_both_CIu,'Color',[1 0 0],'LineWidth',2);
grid on; box off;
xlabel('time [s]','FontName','Helvetica','FontSize',30);
ylabel('[a.u.]','FontName','Helvetica','FontSize',30);
legend('Estimated value','95% confidence boundaries');
set(gca,'FontName','Helvetica','FontSize',20,'YLim',[-0.2 0.15]);


%%
close all
%Plot KBE solution at different time in 2D
tt=[T-0.05 T-0.5 0];
C = {'r','m', 'g', 'k', 'y'}; 
[Xw,Tw] = ndgrid(s,t);
for i=1:length(tt)
    figure(7)
    tfix=find(t==tt(i));
    semilogy(s, vKBE(:,tfix), 'LineWidth', 2)
    %plot(xW, v(:,tfix), 'LineWidth', 2)
    hold on
end
grid on
legend('t=T-dt', 't=0.9T', 't=0.8T', 't=0.5T', 't=0')
xlabel('x')
ylabel('u(t,x)')
title('KBE solution at different time')

%Plot optimal control at different time in 2D
ksiInterp=ksiStar_PDE(Xw,Tw);
for i=1:length(tt)
    figure(5)
    tfix=find(t==tt(i));
    semilogy(s, ksiInterp(:,tfix), 'LineWidth', 2)
    hold on
end
grid on
legend('t=T-0.001','t=T-0.01', 't=T-0.1', 't=T-0.5', 't=0')
xlabel('x')
ylabel('ksi(t,x)')
hold off
title('Optimal Control at different time: Asympt. estimate')
% 
% %Plot optimal control at different time in 2D
% ksiInterp1=ksiStar_Asmp(Xw,Tw);
% for i=1:length(tt)
%     figure(9)
%     tfix=find(tW==tt(i));
%     semilogy(xW, ksiInterp1(:,tfix), 'LineWidth', 2)
%     hold on
% end
% grid on
% legend('t=T-0.001','t=T-0.01', 't=T-0.1', 't=T-0.5', 't=0')
% xlabel('x')
% ylabel('ksi(t,x)')
% hold off
% title('Optimal Control at different time: Anal.Pde Sol')
% 
% %Plot optimal control at different time in 2D
% ksiInterp2=ksiStar3(Xw,Tw);
% for i=1:length(tt)
%     figure(10)
%     tfix=find(tW==tt(i));
%     semilogy(xW, ksiInterp2(:,tfix), 'LineWidth', 2)
%     hold on
% end
% grid on
% legend('t=T-0.001','t=T-0.01', 't=T-0.1', 't=T-0.5', 't=0')
% xlabel('x')
% ylabel('ksi(t,x)')
% hold off
% title('Optimal Control at different time: Exp. smoothing')
% 
% function Switch_dt(dt_EM, ksiStar_PDE)
% 
% end
%%
figure; hold on
    tpv = 0:0.1:T; 
for i=1:length(tpv)
    t=tpv(i);
    tfix=find(tW==t);
  plot3(t*ones(size(xW)),xW, v(:,tfix),'k-','linewidth',2);
end