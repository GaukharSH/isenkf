function [xv,xi_v,u_tau] = tabulate_IC(a,b,dt,K,xmin,make_plots,Nx)
% tabulate_IC(a,b,dt,K,xmin,make_plots,Nx)
%
% Computes the optimal control for the last time step, at time T-dt, and, 
% by numerical integration, the corresponding value of the KBE solution
% at time T-dt. The solutions are coputed on a discretization of the 
% truncated solution interval, which can later be interpolated and 
% extrapolated.
%
% The optimal control modifies the drift of the Ito SDE approximation in 
% order to minimize the variance of the exit probability.
% Original SDE time stepping:
%   Delta X_n = a(t_n,X_n) Delta t 
%             + b(t_n,X_n) sqrt(Delta t) eps_n,
% with the eps_n independent standard Normal random variables
% Controled SDE time stepping:
%   Delta Y_n = a(t_n,Y_n) Delta t 
%             + b(t_n,Y_n) sqrt(Delta t) ( sqrt(Delta t) Zeta + eps_n )
% Note scaling sqrt(Delta t) Zeta, consistent with Nadhir, Abdul-Lateef,
% et al., arXiv 2207.06926
% This scaling leads to optimal controls Zeta approximately proportional
% to 1/Delta t for x<<K.
%
% Note that the problem is extremely ill-posed due to the rare event 
% feature of the original problem. Numerical approximation of the function
% that is the target of the minimization looses all accuracy when the this
% target is too small (which happens rather quickly). If used in practice,
% most likely the best alternative is to compute the opimal control in a 
% rather small interval below 'K' and then force a smooth transition down
% to 0 outside this interval. (It is tempting to extrapolate the control,
% but this will only lead to numerical issues with the evaluation of the
% likelihood factors in the importance sampling.
%
% INPUT:
%   a,  function handle, drift coefficient _without_ time dependence
%                        i.e., a = @(x) ...
%   b,  function handle, dito diffusion
%                        Assumed: constant sign, bounded away from 0
%   dt, positive real scalar, time step size of the SDE solver
%   K,  real scalar, barrier representing the rate event occurance
%   xmin, real scalar<K, lower boundary of the solution interval
%   xmax, real scalar>K, upper dito
%   make_plots, logical, if 'true' then illustrative plots are produced
%   Nx, positive integer, number of discretization points of x-interval
%
% OUTPUT:
%   xv, real vector, discretization points where the control is computed
%   xiv, real vector, optimal control at time T-dt computed in 'xv'
%   u_tau, real vector, corresponding values of the KBE at time T-dt

  % Affine mapping for change of variable xi <-> mu
  M     = @(xi,x) (K-x-(a(x)-b(x)*xi)*dt)./(b(x)*sqrt(2*dt));
  M_inv = @(mu,x) sqrt(2/dt)*mu - (K-x-a(x)*dt)./(b(x)*dt);

  % Formula for second moment, i.e. target of minimization
  G = @(xi,x) 0.5*exp(xi.^2*dt).*erfc(M(xi,x));

  % For minimization, look for zero of the xi-derivative of G, re-written
  % using the change of variables xi->mu. This function is here called 'g'
  g  = @(mu,x) sqrt(dt)*M_inv(mu,x).*erfc(mu)-1/sqrt(2*pi)*exp(-mu.^2);
  % Newton's method uses derivative of g
  dg = @(mu,x) sqrt(2)*erfc(mu) ...
    + sqrt(2/pi)*(-mu+sqrt(2)*(K-x-a(x)*dt)/(b(x)*sqrt(dt))).*exp(-mu.^2);

  % alternative target to G, more suitable for extreme values
  log_G_mu = @(mu,x) dt*M_inv(mu,x).^2 + log(erfc(mu));

  % Discretize x, coordinate of the 1D SDE solution 
  xv   = linspace(xmin,K,Nx);
  dx   = (K-xmin)/(Nx-1);
  xi_v = zeros(1,Nx);
  Gv   = xi_v;
  Newton_points = false(size(xi_v));
  Search_points = Newton_points;
  Approx_points = Newton_points;
  approx_mu = zeros(size(xv));
  
  xi_0  = sign(b(round(xv(Nx-1))))*1e-14; % Initial guess for optimal control at K-dx

  % Compute mu (and xi) based on an asymptotic approximation of the value
  % function, valid for lager mu
  for n=1:Nx
    x = xv(n);
    r = roots([1,-sqrt(2)*(K-x-a(x)*dt)./(b(x)*sqrt(dt)),0,-1/2/sqrt(pi)]);
    [~,ii] = min(abs(imag(r)));
    approx_mu(n) = r(ii);
  end

  %keyboard

  % Assume monotone mu, and choose to switch to asymptotic approximation
  % around the point where the relative error of approximating erfcx(mu) by
  % 1/sqrt(pi)/mu is 1e-3.
  [~,i_approx] = min(approx_mu(log10((abs(erfcx(approx_mu)-(1./approx_mu/sqrt(pi))))./erfcx(approx_mu))<-3));
  Approx_points(1:i_approx) = true;
  xi_v(1:i_approx) = M_inv(approx_mu(1:i_approx),xv(1:i_approx));

  % Start closest to K, and progressively move further away using iterative
  % methods for finding the optimal control with the previously computed 
  % value as initial guess.
  for n=Nx-1:-1:(i_approx+1)
    x = xv(n);

    % Newton's method
    dmu = inf;
    iter = 0; maxiter = 100; break_Newton = false;
    %mu_v = zeros(maxiter+1,1);
    mu_iter = M(xi_0,x);
    %mu_v(iter+1) = mu_iter;
    while abs(dmu)>1e-12 && iter<maxiter && ~break_Newton
      nom = -g(mu_iter,x);
      denom = dg(mu_iter,x);
      if abs(denom)<1e-10 % Completety arbitrary choice...
        break_Newton = true;
      else
        dmu = nom/denom;
        mu_iter  = mu_iter+dmu;
        iter = iter+1;
        %mu_v(iter+1) = mu_iter;
      end
    end
  
    if abs(dmu)>1e-3 && ~break_Newton
      keyboard
    end

    if break_Newton
      % Switch to a more basic optimization algorithm for unimodal functions.
      gr = 0.5*(1+sqrt(5));
      gr2 = 1/gr;
      gr1 = 1-gr2;
      if mu_iter<0
        mu_max = 0.95*mu_iter; mu_min = 1.05*mu_iter;
      else
        mu_max = 1.05*mu_iter; mu_min = 0.95*mu_iter;
      end
      dmu = mu_max-mu_min;
      G_mu_min = log_G_mu(mu_min,x);
      G_mu_max = log_G_mu(mu_max,x);
      mu1 = mu_min + gr1*dmu;
      mu2 = mu_min + gr2*dmu;
      G_mu1 = log_G_mu(mu1,x);
      G_mu2 = log_G_mu(mu2,x);
      if max(G_mu1,G_mu2)>max(G_mu_min,G_mu_max)
        error('tabulate_IC: Unimodality assumption clearly violated.')
      end
      gr_iter = 0; max_gr_iter = 1e4;
      while abs(dmu/mu_iter)>0.5e-3 && gr_iter<max_gr_iter
        if G_mu2>G_mu1
          mu_max = mu2; 
          dmu = mu_max-mu_min;
          mu2 = mu1;
          G_mu2 = G_mu1;
          mu1 = mu_min + gr1*dmu;
          G_mu1 = log_G_mu(mu1,x);
        else
          mu_min = mu1;
          dmu = mu_max-mu_min;
          mu1 = mu2;
          G_mu1 = G_mu2;
          mu2 = mu_min + gr2*dmu;
          G_mu2 = log_G_mu(mu2,x);
        end
        gr_iter = gr_iter+1;
      end
      mu_iter = mu_min+0.5*dmu;
    end
    if ~break_Newton
      Newton_points(n) = true;
    else
      Search_points(n) = true;
    end
    %mu_v(n)  = mu_iter;
    xi_0    = M_inv(mu_iter,x);
    xi_v(n) = xi_0;
    Gv(n) = G(xi_0,x);
  end

  % Compute u_tau(T-dt,x) according to Nadhir & Gaukhar citing Hartmann et al
  % That is, xi(t,x) = b(t,x) d/dx log(sqrt(u_tau(t,x)))
  integrand = xi_v./b(xv);
  integrand = integrand(end:-1:1);
  exponent  = -2*(cumsum(integrand)-0.5*(integrand(1)+integrand))*dx;
  u_tau     = exp(exponent(end:-1:1));

%   % Extend optimal control to zero above K
%   xv_ext = linspace(K,xmax,5);
%   xv_ext = xv_ext(2:end);
%   xiv_ext = zeros(1,4);
%   u_tau_ext = ones(1,4);
% 
%   xv = [xv,xv_ext];
%   xi_v = [xi_v,xiv_ext];
%   u_tau = [u_tau,u_tau_ext];

  %keyboard

  if make_plots
    figure, plot(xv(i_approx+1:end),xi_v(i_approx+1:end),'k-','linewidth',2)
    hold on, plot(xv(1:i_approx),xi_v(1:i_approx),'r-','linewidth',2)
    xlabel('$x$','interpreter','latex','fontsize',14)
    ylabel('$\xi(T-\Delta t,x)$','interpreter','latex','fontsize',14)
    title(['Optimal control $\xi(T-\Delta t,x)$ for $\Delta t=',num2str(dt),'$'],'interpreter','latex','fontsize',14)
    lh=legend('Numerical solution','Asymptotic approximation');
    set(lh,'fontsize',14)

    figure, plot(xv,u_tau,'k-','linewidth',2)
    xlabel('$x$','interpreter','latex','fontsize',14)
    ylabel('$u_\tau(T-\Delta t,x)$','interpreter','latex','fontsize',14)
    title(['KBE solution at $T-\Delta t$ for $\Delta t=',num2str(dt),'$'],'interpreter','latex','fontsize',14)

    figure, plot(xv,log10(u_tau),'k-','linewidth',2)
    xlabel('$x$','interpreter','latex','fontsize',14)
    ylabel('$\log_{10}{(u_\tau(T-\Delta t,x))}$','interpreter','latex','fontsize',14)
    title(['$\log_{10}$ of KBE solution at $T-\Delta t$ for $\Delta t=',num2str(dt),'$'],'interpreter','latex','fontsize',14)

%     % For visualization of the optimal control, computed above, plot target 
%     % function and square probability as function of x and control, xi.
% 
%     % Square of rare event probability 
%     F2 = @(K,x,ax,bx,dt) (erfc(-R(K,x,ax,bx,dt)/sqrt(2))/2 ).^2;
% 
%     figure; hold on
%     xpv = xmin:0.05:2.95; 
%     xipv = linspace(0,1.05*max(abs(xiv)),1001);
%     for x = xpv
%       zpv = sign(b(x))*xipv*sqrt(dt);
%       tmp = F(zpv,K,x,a(x),b(x),dt);
%       plot3(x*ones(size(zpv)),zpv/sqrt(dt),log(tmp),'k-','linewidth',2);
%       plot3(x*ones(size(zpv)),zpv/sqrt(dt),log(F2(K,x,a(x),b(x),dt))*ones(size(zpv)),'k-','linewidth',2);
%     end
%     l1=plot3(xv(Newton_point),xiv(Newton_point),log(Gv(Newton_point)),'r-','linewidth',2);
%     l2=plot3(xv(Search_point),xiv(Search_point),log(Gv(Search_point)),'b-','linewidth',2);
%     lh=legend([l1,l2],'Found by Newton''s method','Found by Golden Ratio search');
%     set(lh,'location','NorthOutside','fontsize',14)
%     xlabel('$x$','fontsize',16,'interpreter','latex')
%     ylabel('$\xi$','fontsize',16,'interpreter','latex')
%     zlabel('$\log{F(\xi;x)}$','fontsize',16,'interpreter','latex')
%     view(3)
  end
  %keyboard
end