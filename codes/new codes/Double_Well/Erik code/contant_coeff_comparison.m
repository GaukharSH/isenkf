close all, clear all, clc
aa    = 0.194444444444444;    % drift coefficient
bb    = 0.5;                  % diffusion coefficient
T=1;                          % simulation length
K= 0.5;                       % threshold
Kstep=100;
dt = T/Kstep;                 % Timestep in SDE
xmin = K-1.8;                 % dt=0.01
%xmin = K-0.5;                % dt=0.001
%xmin = K-3.5;                % dt=0.1

% aa    = sqrt(7);              % drift coefficient
% bb    = 1.23456;              % diffusion coefficient
% T     = 1;                    % simulation length
% K     = sqrt(2);              % threshold
% Kstep = 100;
% dt    = T/Kstep;              % Timestep in SDE
% xmin  = K-2;

% Exact solution to the KBE IBVP in the constant coefficient case:
R_m = @(t,x) (K-x-aa*(T-t))./sqrt(2*bb^2*(T-t));
R_p = @(t,x) (K-x+aa*(T-t))./sqrt(2*bb^2*(T-t));
gamma = @(t,x) 0.5*(erfc(R_m(t,x)) + exp(2*aa*(K-x)/bb^2).*erfc(R_p(t,x)));

dgamma_dx = @(t,x) 1./sqrt(2*pi*bb^2*(T-t)) ...
  .*( exp(-R_m(t,x).^2) + exp(-R_p(t,x).^2+2*aa*(K-x)/bb^2) ) ...
  - aa/bb^2*exp(2*aa*(K-x)/bb^2).*erfc(R_p(t,x));

% % Verify by numerical differentiation
% x_plot = linspace(xmin,K,2e4+1);
% dxv = 1e-3*2.^(0:-1:-10);
% figure, hold on
% for k=1:length(dxv)
%   dx = dxv(k);
%   plot(x_plot,log2(abs(dgamma_dx(T-dt,x_plot)-(gamma(T-dt,x_plot+dx)-gamma(T-dt,x_plot-dx))/(2*dx))),'-','LineWidth',2)
% end
% xlabel('$x$','FontSize',14,'Interpreter','latex')
% ylabel('$\log_2{Error}$','FontSize',14,'Interpreter','latex')
% grid on

% Optimal control obtained as b*d/dx log(gamma)
xi_opt = @(t,x) bb*dgamma_dx(t,x)./gamma(t,x);

%%
close all
dt_fixed=0.0001;
tv=0:dt_fixed:T;
x=[xmin K-1.5 K-1 K-0.5 K-0.1 K];
x_strings = cell(1,length(x));
figure(1)
for j=1:length(x)
semilogy(tv, xi_opt(tv, x(j)), 'Linewidth',2)
x_strings{j} = ['$x=',num2str(x(j)),'$'];
hold on
end
figure(1)
lh=legend(x_strings);
set(lh,'interpreter','latex','fontsize',18,'location','NorthEast')
xlabel('$t$','interpreter','latex','fontsize',20)
ylabel('$\xi(t,x)$','interpreter','latex','fontsize',20)
title('Optimal control for varying x with fixed $\Delta t = 0.0001$','interpreter','latex','fontsize',20)
%%
close all
dx_fixed = 0.0001;
xv   = xmin:dx_fixed:K;
dtv=[0.1 0.01 0.008 0.005 0.001 0.0001];
dt_strings = cell(1,length(dtv));
figure(2) 
for j=1:length(dtv)
semilogy(xv, dtv(j)*xi_opt(T-dtv(j),xv), 'Linewidth',2)
dt_strings{j} = ['$\Delta t=',num2str(dtv(j)),'$'];
hold on
end
figure(2)
lh=legend(dt_strings);
set(lh,'interpreter','latex','fontsize',18,'location','NorthEast')
xlabel('$x$','interpreter','latex','fontsize',20)
ylabel('$\Delta t \xi(T-\Delta t,x)$','interpreter','latex','fontsize',20)
title('$\Delta t$-scaled optimal control at $T-\Delta t$','interpreter','latex','fontsize',20)
%%
close all
dx_fixed = 0.01;
xv   = xmin:dx_fixed:K;
dtv=[0.1 0.01 0.001 0.0001 0.00001];
dtv=2.^(-1:-1:-9)
dt_strings = cell(1,length(dtv));
figure(2) 
loglog(dtv, xi_opt(T-dtv,0.4),'k*-', dtv, 0.2*dtv.^(-1), 'r--','Linewidth',2)
legend('$\xi(T-\Delta t,0.4)$', '$c (\Delta t)^{-1}$','interpreter','latex','fontsize',18)
%set(lh,'interpreter','latex','fontsize',18,'location','NorthEast')
xlabel('$\Delta t$','interpreter','latex','fontsize',20)
%ylabel('$\xi(T-\Delta t,x)$','interpreter','latex','fontsize',20)
title('Optimal control at $T-\Delta t$ and fixed $x=0.4$','interpreter','latex','fontsize',20)
%%
close all
dx_fixed = 0.01;
xv   = xmin:dx_fixed:K;
dxv=2.^(-1:-1:-9)
dt_strings = cell(1,length(dtv));
figure(2) 
loglog(dxv, xi_opt(0.5,K-dx),'k*-','Linewidth',2)
legend('$\xi(T-\Delta t,0.4)$', '$c (\Delta t)^{-1}$','interpreter','latex','fontsize',18)
%set(lh,'interpreter','latex','fontsize',18,'location','NorthEast')
xlabel('$\Delta t$','interpreter','latex','fontsize',20)
%ylabel('$\xi(T-\Delta t,x)$','interpreter','latex','fontsize',20)
title('Optimal control at $T-\Delta t$ and fixed $x=0.4$','interpreter','latex','fontsize',20)

%%
dx=0.01;
xv   = xmin:dx:K;
tv=linspace(0,1,101);
figure
[Xv,Tv] = ndgrid(xv,tv);
p2=surf(Xv, Tv, gamma(Tv,Xv));
set(p2,'LineStyle','none')
xlabel('x','FontName','Times','fontsize',20)
ylabel('t','FontName','Times','fontsize',20)
zlabel('\xi(x,t)','FontName','Times','fontsize',20)
title('KBE finite diff. solution: $\mathcal{K}=2$, $b=0.5$','interpreter', 'latex', 'FontName','Times','fontsize',20)
[px,py] = gradient(gamma(Tv,Xv));

figure
contour(tv,xv,gamma(Tv,Xv))
hold on
quiver(tv,xv,px,py)
hold off
set(gca, 'XDir','reverse')
%%
gamma_plus=gamma(0.99,K-0.1)+dgamma_dx(0.99,K-0.1).*(0.001)
%%

% Now, compare with the computed optimal control for EM one time step

a=@(x) aa*ones(size(x));      % drift coefficient as a function of x
b=@(x) bb*ones(size(x));      % diffusion coefficient

[xv,xiv,u_tau] = tabulate_IC(a,b,dt,K,xmin,true,2e4+1);

figure, hold on
plot(xv,xi_opt(T-dt,xv),'k-','LineWidth',2)
plot(xv,xiv,'r--','LineWidth',2)
xlabel('$x$','FontSize',18,'Interpreter','latex')
ylabel('$\xi(T-\Delta t,x)$','FontSize',18,'Interpreter','latex')
lh=legend('From exact, continuous time, PDE sol.','Computed for Euler-Maruyama, one step');
set(lh,'Fontsize',18,'Location','southoutside')
title(['Constant coefficient case, with $\Delta t=',num2str(dt),'$'],...
  'FontSize',18,'Interpreter','latex')

figure
plot(xv(1:end-1),abs((xi_opt(T-dt,xv(1:end-1))-xiv(1:end-1))./xiv(1:end-1)),...
  'k-','LineWidth',2)
xlabel('$x$','FontSize',18,'Interpreter','latex')
ylabel('$\frac{|\xi_c(T-\Delta t,x)-\xi_{EM}(T-\Delta t,x)|}{|\xi_{EM}(T-\Delta t,x)|}$','FontSize',18,'Interpreter','latex')
title(['Rel. Err. using continuous $\xi$ Constant coefficient case, with $\Delta t=',num2str(dt),'$'],...
  'FontSize',18,'Interpreter','latex')
%%
M=1;
x0=0.5;
dt=0.01;
dW=sqrt(dt)*randn(1,M);
X=@(x0, t) x0+(a(K)+b(K)*xi_opt(t,x0))*dt+b(K)*dW;
Y=@(x0,t) 2*xi_opt(t,x0).*dW+xi_opt(t,x0).^2.*dt;
figure
plot(xv, X(xv,0), 'Linewidth',2)
hold on
%%
close all
M=100;
x0=-1
Kstep=100;
dt=T/Kstep;
X=zeros(M,Kstep+1);
Xo=X;
Y=X;
Y(:,1)=0;
X(:,1)=x0;
Xo(:,1)=x0;
t=linspace(0,T,Kstep+1);
for n=1:Kstep
    dW=sqrt(dt)*randn(M,1);
    Xo(:,n+1)= Xo(:,n)+a(K)*dt+b(K)*dW;
    X(:,n+1)= X(:,n)+(a(K)+b(K)*xi_opt(t(n),X(:,n)))*dt+b(K)*dW;
    Y(:,n+1)= Y(:,n)+(2*xi_opt(t(n+1),X(:,n+1)).*dW+xi_opt(t(n+1),X(n+1)).^2.*dt);
end
figure(1)
plot( t,X,'r-',t,Xo,'b-','Linewidth', 2)
hold on
figure(2)
plot(t, Y, 'g','Linewidth', 2)
hold on

%%
close all
M=1;
x0=-3
Kstep=100;
dt=T/Kstep;
 X=[];
 Xo=[]; 
 Y=[];
t=linspace(0,T,Kstep+1);
for i=1:M
Y(i,1)=0;
X(i,1)=x0;
Xo(i,1)=x0;
t_n=0;
n=1;
while t_n<=T && X(i,n)<K
    
    if (K-X(i,n))<=1 && (K-X(i,n))>=-1
    (K-X(i,n))
    
    dt=(T-t_n)^1.5
    dW=sqrt(dt).*randn;
    Xo(i,n+1)= Xo(i,n)+a(K)*dt+b(K)*dW
    X(i,n+1)= X(i,n)+((K-X(i,n))./(T-t_n))*dt+b(K).*dW
    Y(i,n+1)= Y(i,n)+(2/b(K)*((K-X(i,n))./(T-t_n)-a(K)).*dW+1/b(K)^2*((K-X(i,n))./(T-t_n)-a(K)).^2.*dt)
    %pause()
    else
     (K-X(i,n))
    dt=(K-X(i,n))^(-0.5)*(T-t_n)
    dW=sqrt(dt).*randn;
    Xo(i,n+1)= Xo(i,n)+a(K)*dt+b(K)*dW
    X(i,n+1)= X(i,n)+((K-X(i,n))./(T-t_n))*dt+b(K).*dW
    Y(i,n+1)= Y(i,n)+(2/b(K)*((K-X(i,n))./(T-t_n)-a(K)).*dW+1/b(K)^2*((K-X(i,n))./(T-t_n)-a(K)).^2.*dt)
   % pause()
    end  
    n=n+1;
    t_n=t_n+dt
%     figure(3)
%     plot( t_n,X(i,n+1),'r-',t_n,Xo(i,n+1),'b-','Linewidth', 2)
%     hold on
%     figure(4)
%     plot(t_n, Y(i,n+1), 'g','Linewidth', 2)
%     hold on
end
end


%% Computing the QoI with and without change of measure wrt W and rho_0
S=10^4; %MC sample size
%Initialization
h_IS=zeros(1,S); h_IS_rho0=h_IS;h_MC=h_IS;  h_IS_W=h_IS; h_CE=h_IS;
runmax_IS=zeros(1,S);runmax_IS_W=runmax_IS;runmax_IS_rho0=runmax_IS;runmax_MC=runmax_IS; runmax_CE=runmax_IS;
u_MC=zeros(1,Kstep+1);u_IS=u_MC;u_IS_W=u_MC;u_IS_rho0=u_MC; u_CE=u_MC;
u0=mu_0+sigma_0*randn(1,S);
u0_shifted_1=mFit+sigFit*randn(1,S);
u0_shifted_2=mFit_both+sigFit_both*randn(1,S);
u0_CE=mu_tilde+sigma_tilde*randn(1,S);
meanL=0;
meanL_W=0;
parfor s=1:S
       u_MC=u0(s);
       u_IS=u0_shifted_2(s);
       u_IS_W=u0(s);
       u_IS_rho0=u0_shifted_1(s);
       u_CE=u0_CE(s);
       L=1;
       k=1;
       t_n=0;
       
       %We integrate until the state hits the threshold or until final time
       
       %IS wrt both
        while t_n<=T && u_IS(k)<K

            dW1 = sqrt(dt)*randn;
            dW2 = sqrt(dt)*randn;

            L=L*exp(-0.5*dt*(ksiStar(u_IS(k),t_n))^2-ksiStar(u_IS(k),t_n)*dW1);
            u_IS(k+1)= u_IS(k)+(aa(u_IS(k))+sigma*ksiStar(u_IS(k),t_n))*dt+sigma*dW1; %Forward Euler 

            k=k+1;
            t_n=t_n+dt;
        end
    
       %IS wrt W(t)
        L_W=1;
        k=1;
        t_n=0;
       
        while t_n<=T && u_IS_W(k)<K
        
        dW2 = sqrt(dt)*randn;
        
        L_W=L_W*exp(-0.5*dt*(ksiStar(u_IS_W(k),t_n))^2-ksiStar(u_IS_W(k),t_n)*dW2);
        u_IS_W(k+1)= u_IS_W(k)+(aa(u_IS_W(k))+sigma*ksiStar(u_IS_W(k),t_n))*dt+sigma*dW2; %Forward Euler 

        k=k+1;
        t_n=t_n+dt;
        end
    
       %MC simulation and IS wrt rho_0 
        for n=1:Kstep
            u_MC(n+1)=u_MC(n)+aa(u_MC(n))*dt+sigma*sqrt(dt)*randn;
            u_IS_rho0(n+1)= u_IS_rho0(n)+(aa(u_IS_rho0(n)))*dt+sigma*sqrt(dt)*randn; %Forward Euler 
            u_CE(n+1)=u_CE(n)+aa(u_CE(n))*dt+sigma*sqrt(dt)*randn;
        end
    meanL=meanL+L;
    meanL_W=meanL_W+L_W;

    h_IS(s)=(max(u_IS)>=K)*L;
    h_IS_W(s)=(max(u_IS_W)>=K)*L_W;
    h_IS_rho0(s)=(max(u_IS_rho0)>=K);
    h_MC(s)=(max(u_MC)>=K);
    h_CE(s)=(max(u_CE)>=K);

    runmax_IS(s)=max(u_IS);
    runmax_IS_W(s)=max(u_IS_W);
    runmax_IS_rho0(s)=max(u_IS_rho0)
    runmax_MC(s)=max(u_MC);
    runmax_CE(s)=max(u_CE);
end