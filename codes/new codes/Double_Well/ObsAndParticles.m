function [q,yobs]=ObsAndParticles(u0,ObsSteps, sigma,tau, H, Gamma, a_pi)

dtRef=0.01;
NRef     = round(tau/dtRef);
%Pre-allocate
yobs= zeros(ObsSteps,1);
v=zeros(ObsSteps,1);
w=zeros(NRef,1);
v(1)=u0;
yobs(1) = H*v(1)+ sqrt(Gamma)*randn();
for n=1:ObsSteps
    w(1)=v(n);
    for k=1:NRef-1
        w(k+1)=w(k)+a_pi(w(k))*dtRef+sigma*sqrt(dtRef)*randn;
    end
    q{n}=w;
    v(n+1)=w(end);
    yobs(n+1)=H*v(n+1) + sqrt(Gamma)*randn;%observation
end
end

% 
% function [v,yobs, mHatRef, cHatRef, cRef]=ObsAndRefSolOU(ObsSteps, sigma, H, Gamma, u0)
%     a_pi=@(x) 4*x.*(x.^2-1).*(abs(x)<=sqrt(2))+sign(x).*(20*abs(x)-16*sqrt(2)).*(abs(x)>sqrt(2));
% 
%     tau=1;
%     LRef     = 12;
%     dtRef    = 2^(-LRef)*tau;
%     NRef     = round(tau/dtRef);
%     M=1000;%number of ensemble members
%     
%     %Pre-allocate
%     yobs= zeros(ObsSteps,1);
%     mHatRef = zeros(ObsSteps,1);
%     cHatRef = zeros(ObsSteps,1);
%     v=zeros(ObsSteps,1);
%     
%     m0=0;%prior initial condition mean
%     c0=1; %prior initial condition variance
%     v(1)=m0+sqrt(c0)*randn; %initial truth
%     mHatRef(1)=u0;%initial mean/estimate
%     cHatRef(1)=c0;%initial covariance
%     u=u0+sqrt(c0)*randn(1,M);%initial ensemble
%     %u=u0*ones(1,M);
%    
%     
% %     u=u0+a_pi(u0)*dtRef+sigma*sqrt(dtRef)*randn(1,M); %initial ensemble
% %     yobs(1)=H*v(1) + sqrt(Gamma)*randn;
% %     K = cRef(1)*H'/(Gamma+H*cRef(1)*H'); %Kalman Gain
% %     mHatRef(1) = (1-K*H)*mRef(1)+K*yobs(1); %initial update mean
% %     cHatRef(1) = (1-K*H)*cRef(1);%initial update covariance
% 
%     for n=1:ObsSteps-1
%         
%         w=v(n);
%         for k=1:NRef
%             w=w+a_pi(w)*dtRef+sigma*sqrt(dtRef)*randn;
%             u=u+a_pi(u).*dtRef+sigma*sqrt(dtRef)*randn(1,M);
%         end
%         v(n+1)=w;
%         yobs(n) = H*v(n+1) + sqrt(Gamma)*randn;%observation
%         %u=Psi(u, Nenkf, M, tau, sigma);
% 
%         mRef = mean(u);%estimator predict
%         cRef = cov(u);%covariance predict
%        
%         K = cRef*H'/(Gamma+H*cRef*H');%Kalman gain
%         yTilde=yobs(n)+sqrt(Gamma)*randn(1,M);%perturbed observation
%         u=(1-K*H)*u+K*yTilde;%ensemble update
%         mHatRef(n) = mean(u);%mean update
%         cHatRef(n) = cov(u);%covariance update 
%     end
% end
% 
% function [yobs, mHatRef, cHatRef, cRef]=ObsAndRefSolOU(ObsSteps, sigma, H, Gamma, u0)
%     a_pi=@(x) 4*x.*(x.^2-1).*(abs(x)<=sqrt(2))+sign(x).*(20*abs(x)-16*sqrt(2)).*(abs(x)>sqrt(2));
%     
%     tau=1;
%     LRef     = 12;
%     dtRef    = 2^(-LRef)*tau;
%     NRef     = round(tau/dtRef);
% 
%     %Pre-allocate
%     yobs= zeros(ObsSteps,1);
%     mRef = zeros(ObsSteps,1);
%     cRef = zeros(ObsSteps,1);
%     mHatRef = zeros(ObsSteps,1);
%     cHatRef = zeros(ObsSteps,1);
%     
%   
%     u=u0+a_pi(u0)*dtRef+sigma*sqrt(dtRef)*randn(); %initial ensemble
%     yobs(1) = H*u+ sqrt(Gamma)*randn(); %initial observation
%     mRef(1) = mean(u);%initail predict mean
%     cRef(1) = cov(u); % initial predict covariance
%     K = cRef(1)*H'/(Gamma+H*cRef(1)*H'); %Kalman Gain
%     mHatRef(1) = (1-K*H)*mRef(1)+K*yobs(1); %initial update mean
%     cHatRef(1) = (1-K*H)*cRef(1);%initial update covariance
%     for n=1:ObsSteps-1
%         for k=1:NRef
%             u=u+a_pi(u)*dtRef+sigma*sqrt(dtRef)*randn();
%         end
%         yobs(n+1) = H*u + sqrt(Gamma)*randn();%observation
% 
%         mRef(n+1) = mean(u);%estimator predict
%         cRef(n+1) = cov(u);%covariance predict
%         K = cRef(n+1)*H'/(Gamma+H*cRef(n+1)*H');%Kalman Gain
%         mHatRef(n+1) = (1-K*H)*mRef(n+1)+K*yobs(n+1);%estimator update
%         cHatRef(n+1) = (1-K*H)*cRef(n+1);%covariance update
%     end
% end
