clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
% Krng=[2 2.5 3 3.5];
% for zzz=1:4
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for Langevin 2D problem to test CE method and PDE method in
%%% performing IS technique: 
%%% X - particle position, V -particle velocity 
%------- dX_t = dV_t dt -----------------------------------------------%
%------- dV_t = -U'(X_t)dt-kappa*V_tdt+(2*kappa*Temp)^1/2 dW_t --------%
%----------------------------------------------------------------------%
%%% in computing QoI: P(max_{0<=t<=T} P1*X_t >K)
% seed=1; rng(seed);

P1   = [0 1];                                               %Projector to track the velocity not the position

%Model parameters
d=2;                                                        %problem dimension 
kappa=2^(-5)*pi^2;                                          %viscosity
Temp=0.1;                                                     %temperature
a    = @(x,v) [v; 0.25*(8*x./(1+2*x.^2).^2 - 2*x)-kappa*v]; %2d Langevin drift
b    = [0; sqrt(2*kappa*Temp)];                             %2d Langevin diffusion
dUdx = @(x) 0.25*(-8*x./(1+2*x.^2).^2 + 2*x);               %U'(x)

%Simulation parameters
T=5;            %Final Time (simulation length)
M=10^2;         %Monte Carlo sample size
N=1000;         %Timestep size in SDE
J=1;            %Brownian motion W_t dimenson
dt_const=T/N;   %time discretization ib SDE
K=1.5           %the threshold

%Initial original density parameters
mu=[-1 -1]';                      %2D mean
corr= 0;                          %correlation
Sigma=[0.05 corr; corr 0.05];     %2D covariance
%%
%Generate samples of Langevin dynamics to use for both CE and L2 regression
u=zeros(d,N+1,M);
Mbar=zeros(1,M);
r=exp(-kappa*dt_const);
sigOU=sqrt(2*kappa*Temp*(1-exp(-2*kappa*dt_const))/(2*kappa));
for m=1:M
    u(:,1,m)=mu+chol(Sigma)'*randn(d,1);
    n=2;
    while n<=N+1 && P1*u(:,n-1,m)<K
        u(2,n,m)=r*u(2,n-1,m)+sigOU*randn;
        u(2,n,m)=u(2,n,m)-dUdx(u(1,n-1,m))*dt_const;
        u(1,n,m)=u(1,n-1,m)+u(2,n,m)*dt_const;
        
        q=exp(-2*max(K-P1*u(:,n-1,m),0)*max(K-P1*u(:,n,m),0)/(sigOU^2*dt_const));
        if rand<q 
            u(2,n,m)=K;
        end
        n=n+1;
    end
    figure(1)
    plot(u(2,:,m))
    hold on
    Mbar(m)=max(P1*u(:,:,m));
    figure(2)
V = @(x) 0.25*(2./(1+2*x.^2) + x.^2);
plot([-3:0.1:3], V([-3:0.1:3])-3, 'k:', u(1,:,m), u(2,:,m),'b-', u(1,1,m), u(2,1,m),'r*','LineWidth', 2)
aa = get(gca,'XTickLabel');
set(gca,'XTickLabel',aa,'FontName','Times','fontsize',18)
xlabel('$u_t$', 'interpreter', 'latex', 'fontsize', 25)
ylabel('$v_t$', 'interpreter', 'latex','fontsize', 25)
title('$\mathcal{T}=0.1$', 'interpreter', 'latex', 'fontsize', 25)
ylim([-3 3])
xlim([-3 3])
hold on
end

%% Plot Langevin dynamics tejectory
figure(2)
V = @(x) 0.25*(2./(1+2*x.^2) + x.^2);
plot([-3:0.1:3], V([-3:0.1:3])-3, 'k:', u(1,:,2), u(2,:,1),'b-', u(1,1,1), u(2,1,1),'r*','LineWidth', 2)
aa = get(gca,'XTickLabel');
set(gca,'XTickLabel',aa,'FontName','Times','fontsize',18)
xlabel('$u_t$', 'interpreter', 'latex', 'fontsize', 25)
ylabel('$v_t$', 'interpreter', 'latex','fontsize', 25)
title('$\mathcal{T}=0.1$', 'interpreter', 'latex', 'fontsize', 25)
ylim([-3 3])
xlim([-3 3])
%%
x=squeeze(u(1,41,:));
y=squeeze(u(2,41,:));
figure
scatterhist(x,y,'Kernel','on','Location','NorthEast',...
    'Direction','out','Color','k','LineStyle',{'-'},...
    'LineWidth',[2],'Marker','o','MarkerSize',[4]);
%% Markovian Projection via L2 regression
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We approximate only the drift, the diffusion is constant

%Define the Polynomial space
w = 1;                                                                   %Polynomial degree
p = poly_space(w,'HC');  %Polynomial space
%p=[0 0; 0 1];
p_dim = size(p, 1);                                                      %Cardinality of polynomial space

t  = linspace(0,T,N+1)';                                                 %Generating t_0, t_1, ..., t_{N-1}
Tt = reshape(repmat(t,1,M)', (N+1)*M,1);                                 %Replicating M times each t_n and saving as a column vector [t_0 t_0 ... t_0, t_1, ..., t_1,..., t_{N-1}, ..., t_{N_1}]'
Xx = reshape(squeeze(pagemtimes(P1,u))', (N+1)*M,1);                     %Projected Samples saved as a column vector [X_0^(1),..., X_0^(M), X_1^(1),..,X_1^(M), ..., X_{N-1}^(1),...,X_{N-1}^(M)]
tX = [Tt Xx];                                                            %Two column vectors of size N*M for [t_n, X_{t_n}]
f  = reshape(squeeze(pagemtimes(P1,a(u(1,:,:),u(2,:,:))))',(N+1)*M,1);   %Given f function, we are going to solve

D     = x2fx(tX, p);                                                     %This function helps to create Psi matrix each column is non-orth. basis function psi_p for different given p
[Q,R] = modified_GS(D);                                                  %Apply modified Gram-Schmidt process to get QR decomposition
a_p   = Q'*f;                                                            %Solve Normal equations based on orthonormalised basis
 
psy     = @(t,s) x2fx([t s], p);                                         %Non-orth. basis function
psy_bar = @(t,s) x2fx([t s], p)/R;                                       %Orth. basis function
a_bar   = @(t,s) psy_bar(t,s)*a_p;                                       %Approximation to the drift function a(x)
b_bar   = @(s)   sqrt(P1*(b*b')*P1')*ones(size(s));                      %Approximation to the diffusion function b(x)
%%
figure
fsurf(a_bar, [0 T -3 K+1])
hold on
plot3(Tt, Xx, f, 'k*')
xlabel('t','fontsize', 18)
ylabel('x','fontsize', 18)
zlabel('$\bar{a}(x,t)$', 'interpreter', 'latex','fontsize', 18)
title(['$\mathcal{K}=$' num2str(K), ', ', '$\sigma=$', num2str(sigma), ', ', 'HC=', num2str(w)],'interpreter', 'latex','fontsize', 22)
%% residual
% figure
% plot3(Tt,Xx, f-a_bar(Tt,Xx), 'r')
% xlabel('t','fontsize', 18)
% ylabel('x','fontsize', 18)
% zlabel('$P_1a(x,t)-\bar{a}(x,t)$', 'interpreter', 'latex','fontsize', 18)
% title('Residual over t')
% view([0 360])
% figure
% plot3(Tt,Xx, f-a_bar(Tt,Xx), 'r')
% xlabel('t','fontsize', 18)
% ylabel('x','fontsize', 18)
% zlabel('$P_1a(x,t)-\bar{a}(x,t)$', 'interpreter', 'latex','fontsize', 18)
% title('Residual over x')
% view([90 360])
%% residual plots
%close all
% figure
% x_val=reshape(Xx, M, N+1);
% t_val=reshape(Tt, M, N+1);
% data_to_fit=reshape(f, M, N+1);
% model_values=reshape(a_bar(Tt,Xx), M,N+1);
% plot(t_val(1,:), data_to_fit, 'r*', t_val(1,:), model_values, 'k-')
% legend('data')
% xlabel('t')
% title('Langevin: initial condition N([0 0], [0.5 0; 0 0.5])')
% figure
% histogram(f-a_bar(Tt,Xx),'Normalization', 'pdf')
% xlabel('residual')
% title('Langevin: initial condition N([0 0], [0.5 0; 0 0.5])')

%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = -3;                                                       %Lower end of discretization interval for x
xU = K;                                                        %Upper end of discretization interval for x
vL = -3;                                                       %Lower end of discretization interval for v
vU = K;                                                        %Upper end of discretization interval for v
Nx = 1000;                                                     %Number of spatial step size for x
dx = (xU-xL)/Nx;                                               %Stepsize for x
Nv = 1000;                                                     %Number of spatial step size for v
dv = (vU-vL)/Nv;                                               %Stepsize for v
Nt = 8*1e2;                                                    %Number of time steps
dtPDE=T/Nt;
xPDE = linspace(xL,xU,Nx+1)';                                  %Linspace for x
vPDE = linspace(vL,vU,Nv+1)';                                  %Linspace for v
sL = P1*[xL;vL];                                               %Lower end of discretization interval for ptojected process s=P_1x
sU = P1*[xU;vU];                                               %Upper end of discretization interval for ptojected process s=P_1x
Ns = P1*[Nx;Nv];                                               %Number of discretization intervals in space s
ds = (sU-sL)/Ns;                                               %Spatial step size in s
sPDE  = linspace(sL,sU,Ns+1)';                                 %Linspace for s

%PDE solution
ileqK  = find(sPDE<=K);                                           %Indexes of sPDE which is less or equal than K
igreK  = find(sPDE>K);                                            %Indexes of sPDE which is greater than K
NsleqK = length(ileqK)-1;                                         %Number of spatial stepsize used for PDE solver

%% Analytical solution for frozen coeff. at the corner
v_anal=@(x,t) 0.5*erfc((K-x-a_bar(T,K).*(T-t))./sqrt(2*b_bar(K).^2.*(T-t)))...
              +0.5*exp(a_bar(T,K).*(K-x)./b_bar(K).^2./2).*erfc((K-x+a_bar(T,K).*(T-t))./sqrt(2*b_bar(K).^2.*(T-t)));
%% Finding a (red) region in the corner to use analytical solution for the pde solver
absdiff=cell(Ns+1, Nt+1);
t=linspace(T,0,Nt+1);
[Xw,Tw] = ndgrid(sPDE,t);
[px, py]=gradient(v_anal(Xw,Tw));
gradNeeded={[ds dtPDE]};
for i = 1:Ns+1
for j = 1:Nt+1
absdiff{i,j}= norm([px(i,j) py(i,j)]-[ds dtPDE]);
end
end
[MinVal, ind]=min(cell2mat(absdiff),[], 'all', 'linear');
[idx{1:ndims(cell2mat(absdiff))}] = ind2sub(size(cell2mat(absdiff)), ind);
RedRegionIndx=[idx{:}];
sPDE(RedRegionIndx(1))
t(RedRegionIndx(2))
K-sPDE(RedRegionIndx(1))
T-t(RedRegionIndx(2))
px(idx{:}) %should be close to dxPde
py(idx{:}) %should be close to dtPde

%%
[t, s, vKBE] = KBEsolverForAllt_AnalSol(sPDE(ileqK),T,a_bar,b_bar,Nt,NsleqK,K,RedRegionIndx);     %PDE solver for the inhomogeneous underlying dynamics with first few time steps Backward Euler and the rest is Crank-Nickolson.
%%
% figure
% [Xw,Tw] = ndgrid(s,t);
% p2=surf(Xw, Tw, vKBE);
% set(p2,'LineStyle','none')
% xlabel('x','FontName','Times','fontsize',20)
% ylabel('t','FontName','Times','fontsize',20)
% zlabel('\gamma(x,t)','FontName','Times','fontsize',20)
% title('KBE finite diff. solution: $\mathcal{K}=2$, $b=0.5$','interpreter', 'latex', 'FontName','Times','fontsize',20)

%Plot PDE solution at time 0
% figure(1)
% plot(s, vKBE(:,1), '-b', sPDE(2:end)', vErik(2:end)', '--k', 'Linewidth',2)
% xlabel('s', 'fontsize', 14)
% ylabel('u(s,0)','fontsize', 14)


%%
%Initial density is set to 2D Gaussian 
rho_Gaussian = @(x, m, sig) exp(-(x-m).^2/(2*sig^2))/(sqrt(2*pi*sig^2)); 
[x, v]   = meshgrid(xPDE, vPDE);
xv = [x(:) v(:)];
rhoJoint_0=(reshape(mvnpdf(xv,mu',Sigma),length(vPDE),length(xPDE)))'; 

%Plot the initial original denisty
figure
h=surf(vPDE, xPDE, rhoJoint_0);
axis equal
set(h,'LineStyle','none')
ylim([-3 3])
xlim([-3 3])
view(2)
% aa = get(gca,'XTickLabel');
% set(gca,'XTickLabel',aa,'FontName','Times','fontsize',35)
% bb = get(gca,'YTickLabel');
% set(gca,'YTickLabel',bb,'FontName','Times','fontsize',35)
% title(['$\rho_{[u_0,v_0]}\sim N([0; 0]; [0.5, 0; 0, 0.5])$'], 'FontName','Times', 'fontsize',40,'interpreter','latex')
% xlabel('$v_0$', 'fontsize',40,'interpreter','latex')
% ylabel('$u_0$','fontsize',40,'interpreter','latex')

%% IS wrt rho0
M=10^5;
%Note that the optimal denisty rhoJointTilde_0(x,v) \approx rho_0(x,v)sqrt(u_PDE(v,0))
% where rho_0(x,v) = rho_0(x|v)rho_0(v) and rho_0(v)*sqrt(u_PDE(v,0)) is fitted to 1D Gaussian

%Marginal denisty in v since we consider the projection P1=[0 1] and solved the KBE with respect to v
rhoMarg_v = rho_Gaussian(s, mu(2), sqrt(Sigma(2,2)));
rhoTemp = [rhoMarg_v.*sqrt(abs(vKBE(:,end)));rho_Gaussian(sPDE(igreK), mu(2), sqrt(Sigma(2,2)))]; %after x>K we have Gaussian
NC = trapezoidal(rhoTemp, ds);  %normalising constant
rhoTemp=rhoTemp./NC;            %optimal IS density

%Fitting the rho_0(v)*sqrt(u_PDE) to Gaussian
v=[s;sPDE(igreK)];
mFit   = trapezoidal(v.*rhoTemp, dv)
sigFit = sqrt(trapezoidal(v.^2.*rhoTemp, dv)-mFit^2)
rhoTildeFit_0 = rho_Gaussian(vPDE, mFit, sigFit);

%Conditional Gaussian rho_0(x|v) 
vSmpl=mFit+sigFit*randn(1,M);
mCond=mu(1)+corr*(vSmpl-mu(2))*sqrt(Sigma(1,1)/Sigma(2,2));
sigCond=sqrt((1-corr^2)*Sigma(1,1));
rhoTildeCond_0= rho_Gaussian(xPDE, mCond(1), sigCond);

%The optimal IS density
rhoJointTilde_0=rhoTildeCond_0*rhoTildeFit_0';
    
figure
h=surf(vPDE,xPDE,rhoJointTilde_0);
axis equal
set(h,'LineStyle','none')
ylim([-3 3])
xlim([-3 3])
view(2)
aa = get(gca,'XTickLabel');
set(gca,'XTickLabel',aa,'FontName','Times','fontsize',35)
bb = get(gca,'YTickLabel');
set(gca,'YTickLabel',bb,'FontName','Times','fontsize',35)
title(['$\tilde{\rho}_{[u_0,v_0]}^{PDE,1} \sim N([0;' num2str(mFit,2) ']; [0.5, 0; 0,'  num2str(sigFit^2,2) '])$'], 'FontName','Times', 'fontsize',40,'interpreter','latex')
xlabel('$v_0$', 'fontsize', 40,'interpreter','latex')
ylabel('$u_0$','fontsize', 40,'interpreter','latex')
%% IS wrt both
%Note that the optimal denisty rhoJointTilde_0(x,v) \approx rho_0(x,v)sqrt(u_PDE(v,0))
% where rho_0(x,v) = rho_0(x|v)rho_0(v) and rho_0(v)*u_PDE(v,0) is fitted to 1D Gaussian

%Marginal denisty in v since we consider the projection P1=[0 1] and solved the KBE with respect to v
rhoTemp_both = [rhoMarg_v.*abs(vKBE(:,end));rho_Gaussian(sPDE(igreK), mu(2), sqrt(Sigma(2,2)))]; %after x>K we have Gaussian
NC_both = trapezoidal(rhoTemp_both, ds); %normalising constant
rhoTemp_both=rhoTemp_both./NC_both;           %optimal IS density

%Fitting the rho_0(v)*u_PDE to Gaussian
v=[s;sPDE(igreK)];
mFit_both   = trapezoidal(v.*rhoTemp_both, dv)
sigFit_both = sqrt(trapezoidal(v.^2.*rhoTemp_both, dv)-mFit_both^2)
rhoTildeFit_0_both = rho_Gaussian(vPDE, mFit_both, sigFit_both);

%Conditional Gaussian rho_0(x|v) 
vSmpl_both=mFit_both+sigFit_both*randn(1,M);
mCond_both=mu(1)+corr*(vSmpl_both-mu(2))*sqrt(Sigma(1,1)/Sigma(2,2));
sigCond_both=sqrt((1-corr^2)*Sigma(1,1));
rhoTildeCond_0_both= rho_Gaussian(xPDE, mCond_both(1), sigCond_both);

%The optimal IS density
rhoJointTilde_0_both=rhoTildeCond_0_both*rhoTildeFit_0_both';

figure
h=surf(vPDE,xPDE,rhoJointTilde_0_both);
axis equal
set(h,'LineStyle','none')
ylim([-3 3])
xlim([-3 3])
view(2)
aa = get(gca,'XTickLabel');
set(gca,'XTickLabel',aa,'FontName','Times','fontsize',35)
bb = get(gca,'YTickLabel');
set(gca,'YTickLabel',bb,'FontName','Times','fontsize',35)
title(['$\tilde{\rho}_{[u_0,v_0]}^{PDE,2} \sim N([0;' num2str(mFit_both,2) ']; [0.5, 0; 0,'  num2str(sigFit_both^2,2) '])$'], 'FontName','Times', 'fontsize',40,'interpreter','latex')
xlabel('$v_0$', 'fontsize', 40,'interpreter','latex')
ylabel('$u_0$','fontsize', 40,'interpreter','latex')
%% MULTILEVEL CROSS-ENTROPY METHOD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  We consider only mean shifting but fixing the covariance
beta=0.01;  %CE method parameter
M=10^5;     %Monte Carlo sample size
N=100;      %Timestep size in SDE
% Multilevel procedure to find the optimal mu_tilde
Weight = ones(1,M);
sMbar=sort(Mbar);
K_ell=sMbar(1, ceil((1-beta)*M));
K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
h=(Mbar>=K_ell);
Sigma_tilde=Sigma;
ell=1;
u0=mu+chol(Sigma)'*randn(d,M);

while K_ell<K
    mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight);
    
    ell=ell+1;
    for m=1:M
        u(:,1,m)=mu_tilde+chol(Sigma_tilde)'*randn(d,1);
        u0(:,m)=u(:,1,m);
        n=2;
        while n<=N+1 && P1*u(:,n-1,m)<K
            u(2,n,m)=r*u(2,n-1,m)+sigOU*randn;
            u(2,n,m)=u(2,n,m)-dUdx(u(1,n-1,m))*dt_const;
            u(1,n,m)=u(1,n-1,m)+u(2,n,m)*dt_const;
            
            q=exp(-2*max(K-P1*u(:,n-1,m),0)*max(K-P1*u(:,n,m),0)/(sigOU^2*dt_const));
            if rand<q
                u(2,n,m)=K;
            end
            n=n+1;
        end
        Mbar(m)=max(P1*u(:,:,m));
    end
    
    sMbar=sort(Mbar);
    K_ell=sMbar(1, ceil((1-beta)*M));
    K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
    h=(Mbar>=K_ell);
    
    Weight=mvnpdf(u0', mu', Sigma)'./mvnpdf(u0',mu_tilde',Sigma_tilde)';
end

if K_ell==K
    mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight)
end

%%
rhoJoint_CE_0=(reshape(mvnpdf(xv,mu_tilde',Sigma_tilde),length(vPDE),length(xPDE)))'; 
figure
h=surf(vPDE,xPDE,rhoJoint_CE_0);
axis equal
set(h,'LineStyle','none')
ylim([-3 3])
xlim([-3 3])
view(2)
aa = get(gca,'XTickLabel');
set(gca,'XTickLabel',aa,'FontName','Times','fontsize',35)
bb = get(gca,'YTickLabel');
set(gca,'YTickLabel',bb,'FontName','Times','fontsize',35)
title(['$\tilde{\rho}_{[u_0,v_0]}^{CE} \sim N([-0.39; 1.8]; [0.5, 0; 0, 0.5])$'], 'FontName','Times', 'fontsize',40,'interpreter','latex')
xlabel('$v_0$', 'fontsize', 40,'interpreter','latex')
ylabel('$u_0$','fontsize', 40,'interpreter','latex')
%% dW CONTROL
%obtained via KBE solution on PDE grids
ksiGridded = zeros(length(s),length(t));
ksiGridded(2:end-1,:) = b(2)*(log(vKBE(3:end,:))-log(vKBE(1:end-2,:)))./(2*ds);
ksiGridded(1,:) = b(2)*(log(vKBE(2,:))-log(vKBE(1,:)))./ds;
ksiGridded(end,:) = b(2)*(log(vKBE(end,:))-log(vKBE(end-1,:)))./ds;

[Xw,Tw] = ndgrid(s,t); %meshgrid for the interpolant
% figure(3)
% p0=surf(Xw, Tw, v);
% set(p0,'LineStyle','none')

cropN=round(0.1*NsleqK); %if we use KBE, I crop 10% of the left boundary and linearly smooth it to avoid instability on the left handside; for HJB no need to do this and nCrop=1
Ff=griddedInterpolant(Xw(cropN:end,:),flip(Tw(cropN:end,:),2), flip(ksiGridded(cropN:end,:),2),'linear','linear');
ksiStar_PDE = @(x,t) Ff(x,t).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function for any x and t

%%
ksiStar_Asmp= @(x,t) ((1/b_bar(x))*((K-x)./(T-t))-a_bar(t,x)).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function based on asymptotic apprx.

%%
ksiStar = @(x,t, dt_EM) sum([ksiStar_PDE(x,t).*(T-t>=10*dt_EM) ksiStar_Asmp(x,t)*(T-t<10*dt_EM)], 'omitnan'); %the factor 4 corresponds for the particular choice of dtPde=0.005/2 where ksigridded(x,t) with t<=0.96 smooth,t>0.96 it has a kink close to the barrier.
%ksiStar = @(x,t, dt_EM) sum([ksiStar_PDE(x,t).*(T-t>=t(RedRegionIndx(2))) ksiStar_Asmp(x,t)*(T-t<t(RedRegionIndx(2)))], 'omitnan'); %the factor 4 corresponds for the particular choice of dtPde=0.005/2 where ksigridded(x,t) with t<=0.96 smooth,t>0.96 it has a kink close to the barrier.
%ksiStar = @(x,t, dt_EM)0;
%% Computing the QoI with parameters obtained by PDE approach
Srng=[1e5];
dt_const=0.01;

for ss=1:length(Srng)
S=Srng(ss);
h=zeros(1,S);h_MC=h;h_rho0=h;h_W=h;h_CE=h;
runmax=h;runmax_MC=h;runmax_rho0=h; runmax_W=h; runmax_CE=h;
u=zeros(d,N,S);
u0_both=[mCond_both+sigCond_both.*randn(1,S); vSmpl_both];
u0_rho0=[mCond+sigCond.*randn(1,S); vSmpl];
u0_MC=mu+chol(Sigma)'*randn(d,S);
u0_W=mu+chol(Sigma)'*randn(d,S);
u0_CE=mu_tilde+chol(Sigma)'*randn(d,S);

t=linspace(0,T,N+1);
meanL=0;
parfor m=1:S
    u=u0_both(:,m);
    u_MC=u0_MC(:,m);
    u_rho0=u0_rho0(:,m);
    u_W=u0_W(:,m);
    u_CE=u0_CE(:,m);
    
    %IS wrt both
    L=1;
    k=1;
    t_k=0;
    dt=dt_const;
    while t_k<=T && P1*u(:,k)<K && dt>0.000001
        dt=min(dt_const, 0.5*(T-t_k));
        W1=randn;
        r=exp(-kappa*dt);
        sigOU=sqrt(2*kappa*Temp*(1-exp(-2*kappa*dt))/(2*kappa)); 
        
        u(2,k+1)=r*u(2,k)+sigOU*W1+b(2)*ksiStar(u(2,k),t_k, dt_const)*dt;
        u(2,k+1)=u(2,k+1)-dUdx(u(1,k))*dt;
        u(1,k+1)=u(1,k)+u(2,k+1)*dt;
        L=L*exp(-0.5*dt*(ksiStar(u(2,k),t_k, dt_const))^2-ksiStar(u(2,k),t_k, dt_const)*sqrt(dt)*W1);
        q=exp(-2*max(K-u(2,k),0)*max(K-u(2,k+1),0)/(sigOU^2*dt));

        if rand<q 
            t_k=t_k+0.5*dt ;
            u(2,k+1)=K;
        else
            t_k=t_k+dt;
        end
        
        k=k+1;
    end
    
    %IS wrt W(t)
        L_W=1;
        k=1;
        t_k=0;
        while t_k<=T && P1*u_W(:,k)<K && dt>0.000001
        dt=min(dt_const, 0.5*(T-t_k));
        W4=randn;
        r=exp(-kappa*dt);
        sigOU=sqrt(2*kappa*Temp*(1-exp(-2*kappa*dt))/(2*kappa));
        
        u_W(2,k+1)=r*u_W(2,k)+sigOU*W4+b(2)*ksiStar(u_W(2,k),t_k, dt_const)*dt;
        u_W(2,k+1)=u_W(2,k+1)-dUdx(u_W(1,k))*dt;
        u_W(1,k+1)=u_W(1,k)+u_W(2,k+1)*dt;
        L_W=L_W*exp(-0.5*dt*(ksiStar(u_W(2,k),t_k, dt_const))^2-ksiStar(u_W(2,k),t_k, dt_const)*sqrt(dt)*W4);
        
        q_W=exp(-2*max(K-u_W(2,k),0)*max(K-u_W(2,k+1),0)/(sigOU^2*dt));

        if rand<q_W 
            t_k=t_k+0.5*dt ;
            u(2,k+1)=K;
        else
            t_k=t_k+dt;
        end
        
        k=k+1;
        end
    %%MC
        n=1;
        t_n=0;
        r=exp(-kappa*dt_const);
        sigOU=sqrt(2*kappa*Temp*(1-exp(-2*kappa*dt_const))/(2*kappa));
        while t_n<=T && P1*u_MC(:,n)<K
            W2=randn;

            u_MC(2,n+1)=r*u_MC(2,n)+sigOU*W2;
            u_MC(2,n+1)=u_MC(2,n+1)-dUdx(u_MC(1,n))*dt_const;
            u_MC(1,n+1)=u_MC(1,n)+u_MC(2,n+1)*dt_const;
                   
            q_MC= exp(-2*max(K-u_MC(2,n),0)*max(K-u_MC(2,n+1),0)/(sigOU^2*dt_const));
            if rand<q_MC 
            u_MC(2,n+1)=K;
            end
            t_n=t_n+dt_const;
            n=n+1;
        end
       
    %IS wrt rho_0
        n=1;
        t_n=0;
        while t_n<=T && P1*u_rho0(:,n)<K
            W3=randn;

            u_rho0(2,n+1)=r*u_rho0(2,n)+sigOU*W3;
            u_rho0(2,n+1)=u_rho0(2,n+1)-dUdx(u_rho0(1,n))*dt_const;
            u_rho0(1,n+1)=u_rho0(1,n)+u_rho0(2,n+1)*dt_const;   
                   
            q_rho0= exp(-2*max(K-u_rho0(2,n),0)*max(K-u_rho0(2,n+1),0)/(sigOU^2*dt_const));
            if rand<q_rho0 
            u_rho0(2,n+1)=K;
            end
            t_n=t_n+dt_const;
            n=n+1;
        end
        
        %CE
        n=1;
        t_n=0;
        while t_n<=T && P1*u_CE(:,n)<K
            W5=randn;

            u_CE(2,n+1)=r*u_CE(2,n)+sigOU*W5;
            u_CE(2,n+1)=u_CE(2,n+1)-dUdx(u_CE(1,n))*dt_const;
            u_CE(1,n+1)=u_CE(1,n)+u_CE(2,n+1)*dt_const; 
                   
            q_CE= exp(-2*max(K-u_CE(2,n),0)*max(K-u_CE(2,n+1),0)/(sigOU^2*dt_const));
            if rand<q_CE
            u_CE(2,n+1)=K;
            end
            t_n=t_n+dt_const;
            n=n+1;
        end
    
    meanL=meanL+L;
    h(m)=(max(P1*u(:,:))>=K)*L
    h_MC(m)=(max(P1*u_MC(:,:))>=K);
    h_rho0(m)=(max(P1*u_rho0(:,:))>=K);
    h_W(m)=(max(P1*u_W(:,:))>=K)*L_W;
    h_CE(m)=(max(P1*u_CE(:,:))>=K);
    
    runmax(m)=max(P1*u);
    runmax_MC(m)=max(P1*u_MC);
    runmax_rho0(m)=max(P1*u_rho0);
    runmax_W(m)=max(P1*u_W);
    runmax_CE(m)=max(P1*u_CE);
end
% fnm = sprintf('Langevin_M1e6_all_K%d.mat',K);
% save(fnm,'-nocompression','-v7.3') 
% end
%%
meanL=meanL/S
Weight_both = rho_Gaussian(P1*u0_both,P1*mu,sqrt(P1*Sigma*P1'))./rho_Gaussian(P1*u0_both, mFit_both, sigFit_both);
Weight_rho0= rho_Gaussian(P1*u0_rho0,P1*mu,sqrt(P1*Sigma*P1'))./rho_Gaussian(P1*u0_rho0, mFit, sigFit);
%Weight_CE= rho_Gaussian(P1*u0_CE,P1*mu,sqrt(P1*Sigma*P1'))./rho_Gaussian(P1*u0_CE, P1*mu_tilde, sqrt(P1*Sigma*P1'));
Weight_CE=mvnpdf(u0_CE', mu', Sigma)'./mvnpdf(u0_CE',mu_tilde',Sigma)';
alpha_hat_IS_both(ss)=mean(h.*Weight_both)
alpha_hat_IS_W(ss)=mean(h_W)
alpha_hat_IS_rho0(ss)=mean(h_rho0.*Weight_rho0)
alpha_hat_MC(ss)=mean(h_MC)
alpha_hat_CE(ss)=mean(h_CE.*Weight_CE)
Vaprx_IS_both=var(h.*Weight_both)
Vaprx_IS_W=var(h_W)
Vaprx_IS_rho0=var(h_rho0.*Weight_rho0)
Vaprx_MC=var(h_MC)
Vaprx_CE=var(h_CE.*Weight_CE)
RelError_MC=1.96*sqrt(Vaprx_MC)/(alpha_hat_MC(ss)*sqrt(S))
RelError_CE=1.96*sqrt(Vaprx_CE)/(alpha_hat_CE(ss)*sqrt(S))
RelError_IS_both=1.96*sqrt(Vaprx_IS_both)/(alpha_hat_IS_both(ss)*sqrt(S))
RelError_IS_W=1.96*sqrt(Vaprx_IS_W)/(alpha_hat_IS_W(ss)*sqrt(S))
RelError_IS_rho0=1.96*sqrt(Vaprx_IS_rho0)/(alpha_hat_IS_rho0(ss)*sqrt(S))
%varRatio_approx=(RelError_MC/RelError_IS)^2
varRatio_both=Vaprx_MC/Vaprx_IS_both
varRatio_W=Vaprx_MC/Vaprx_IS_W
varRatio_rho0=Vaprx_MC/Vaprx_IS_rho0
varRatio_CE=Vaprx_MC/Vaprx_CE

IS_both_CIu(ss)=alpha_hat_IS_both(ss)+1.96*sqrt(Vaprx_IS_both)/sqrt(S)
IS_both_CId(ss)=alpha_hat_IS_both(ss)-1.96*sqrt(Vaprx_IS_both)/sqrt(S)
IS_W_CIu(ss)=alpha_hat_IS_W(ss)+1.96*sqrt(Vaprx_IS_W)/sqrt(S)
IS_W_CId(ss)=alpha_hat_IS_W(ss)-1.96*sqrt(Vaprx_IS_W)/sqrt(S)
IS_rho0_CIu(ss)=alpha_hat_IS_rho0(ss)+1.96*sqrt(Vaprx_IS_rho0)/sqrt(S)
IS_rho0_CId(ss)=alpha_hat_IS_rho0(ss)-1.96*sqrt(Vaprx_IS_rho0)/sqrt(S)
MC_CIu(ss)=alpha_hat_MC(ss)+1.96*sqrt(Vaprx_MC)/sqrt(S)
MC_CId(ss)=alpha_hat_MC(ss)-1.96*sqrt(Vaprx_MC)/sqrt(S)
CE_CIu(ss)=alpha_hat_CE(ss)+1.96*sqrt(Vaprx_CE)/sqrt(S)
CE_CId(ss)=alpha_hat_CE(ss)-1.96*sqrt(Vaprx_CE)/sqrt(S)
end
pause()
%% Confidence interval plots
figure
loglog(Srng, alpha_hat_IS_both, 'bd', Srng, IS_both_CIu , 'b-', Srng, IS_both_CId , 'b-', 'Linewidth', 2)
hold on
loglog(Srng, alpha_hat_IS_W, 'ms', Srng, IS_W_CIu , 'm-', Srng, IS_W_CId , 'm-', 'Linewidth', 2)
hold on
loglog(Srng, alpha_hat_IS_rho0, 'c^', Srng, IS_rho0_CIu , 'c-', Srng, IS_rho0_CId , 'c-', 'Linewidth', 2)
hold on
loglog(Srng, alpha_hat_CE, 'gx', Srng, CE_CIu , 'g-', Srng, CE_CId , 'g-','Linewidth', 2)
hold on
loglog(Srng, alpha_hat_MC, 'ro', Srng, MC_CIu , 'r-', Srng, MC_CId , 'r-','Linewidth', 2)
xlabel('Sample size M')
legend( 'IS wrt both','', '', 'IS wrt W(t)','', '', 'IS wrt \rho_0', '', '', 'CE', '', '', 'MC')
title('95% confidence interval')


%% Bootstrapping
close all
C = {'red','magenta'}; % Cell array of colros.
Cc={'green','blue'};
Ccc = {'cyan', 'yellow'};
Cccc = {'black', 'red'};

B=10^4;
conf=95;
Krng=[2 2.5 3 3.5];                     %the threshold DW

q_both=[h_both_K2; h_both_K25; h_both_K3; h_both_K35];
q_IS_W=[h_IS_W_K2; h_IS_W_K25; h_IS_W_K3; h_IS_W_K35];
q_IS_rho0=[h_IS_rho0_K2; h_IS_rho0_K25; h_IS_rho0_K3; h_IS_rho0_K35];
q_IS_CE=[h_IS_CE_K2; h_IS_CE_K25; h_IS_CE_K3; h_IS_CE_K35];

q_both_2=[h_both_K2_2; h_both_K25_2; h_both_K3_2; h_both_K35_2];
q_IS_W_2=[h_IS_W_K2_2; h_IS_W_K25_2; h_IS_W_K3_2; h_IS_W_K35_2];
q_IS_rho0_2=[h_IS_rho0_K2_2; h_IS_rho0_K25_2; h_IS_rho0_K3_2; h_IS_rho0_K35_2];
q_IS_CE_2=[h_IS_CE_K2_2; h_IS_CE_K25_2; h_IS_CE_K3_2; h_IS_CE_K35_2];

Q_both{1}=q_both;Q_both{2}=q_both_2;
Q_IS_W{1}=q_IS_W;Q_IS_W{2}=q_IS_W_2;
Q_IS_rho0{1}=q_IS_rho0;Q_IS_rho0{2}=q_IS_rho0_2;
Q_IS_CE{1}=q_IS_CE;Q_IS_CE{2}=q_IS_CE_2;

for n=1:2
        for i=1:length(Krng)
        K=Krng(i);
        if isempty(gcp)
           parpool;
        end
        opt = statset('UseParallel',true);

        stats_IS_both = bootstrp(B, @(x) [mean(x) std(x)], Q_both{n}(i,:), 'Options', opt);
        stats_IS_W = bootstrp(B, @(x) [mean(x) std(x)], Q_IS_W{n}(i,:), 'Options', opt);
        stats_IS_rho0 = bootstrp(B, @(x) [mean(x) std(x)], Q_IS_rho0{n}(i,:), 'Options', opt);
        stats_IS_CE = bootstrp(B, @(x) [mean(x) std(x)], Q_IS_CE{n}(i,:), 'Options', opt);

        SmplStd_IS_both(i)=mean(stats_IS_both(:,2));
        SmplStd_IS_W(i) =mean(stats_IS_W(:,2));
        SmplStd_IS_rho0(i) =mean(stats_IS_rho0(:,2));
        SmplStd_IS_CE(i) =mean(stats_IS_CE(:,2));

        CId_IS_both(i) =  prctile(stats_IS_both(:,2), (100-conf)/2);
        CIu_IS_both(i) =  prctile(stats_IS_both(:,2), (100-(100-conf)/2));

        CId_IS_W(i) = prctile(stats_IS_W(:,2), (100-conf)/2);
        CIu_IS_W(i) =  prctile(stats_IS_W(:,2), (100-(100-conf)/2));

        CId_IS_rho0(i) = prctile(stats_IS_rho0(:,2), (100-conf)/2);
        CIu_IS_rho0(i) =  prctile(stats_IS_rho0(:,2), (100-(100-conf)/2));
        
        CId_IS_CE(i) = prctile(stats_IS_CE(:,2), (100-conf)/2);
        CIu_IS_CE(i) =  prctile(stats_IS_CE(:,2), (100-(100-conf)/2));
        end
    figure(1)
    plot_ci(Krng',[SmplStd_IS_both; CId_IS_both; CIu_IS_both],'PatchColor', C{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', C{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', C{n}, 'YScale', 'log');
    grid on
    hold on
    figure(2)
    plot_ci(Krng',[SmplStd_IS_W; CId_IS_W; CIu_IS_W], 'PatchColor', Cc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Cc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Cc{n}, 'YScale', 'log');
    grid on
    hold on
    title('IS with W(t)')
    figure(3)
    plot_ci(Krng',[SmplStd_IS_rho0; CId_IS_rho0; CIu_IS_rho0], 'PatchColor', Ccc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Ccc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Ccc{n}, 'YScale', 'log');
    grid on
    hold on
    title('IS with \rho_0')
    figure(4)
    plot_ci(Krng',[SmplStd_IS_CE; CId_IS_CE; CIu_IS_CE], 'PatchColor', Cccc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Cccc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Cccc{n}, 'YScale', 'log');
    grid on
    hold on
    title('IS with CE')
    
    figure(5)
    plot_ci(Krng',[SmplStd_IS_both; CId_IS_both; CIu_IS_both],'PatchColor', C{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', C{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', C{n}, 'YScale', 'log');
    grid on
    hold on
    plot_ci(Krng',[SmplStd_IS_W; CId_IS_W; CIu_IS_W], 'PatchColor', Cc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Cc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Cc{n}, 'YScale', 'log');
    grid on
    hold on
    plot_ci(Krng',[SmplStd_IS_rho0; CId_IS_rho0; CIu_IS_rho0], 'PatchColor', Ccc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Ccc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Ccc{n}, 'YScale', 'log');
    grid on
    hold on
    plot_ci(Krng',[SmplStd_IS_CE; CId_IS_CE; CIu_IS_CE], 'PatchColor', Cccc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Cccc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Cccc{n}, 'YScale', 'log');
    grid on
    hold on
end
%%
figure(1)
xlabel('$\mathcal{K}$','interpreter', 'latex', 'FontName','Times','fontsize',50)
ylabel('sample std','FontName','Times','fontsize',40)
title(['IS wrt both $\rho_0$ and $W(t)$'], 'FontName','Times', 'fontsize',30,'interpreter','latex')
legend('95% CI bootstrap', 'run 1', '', '', '95% CI bootstrap','run 2', 'FontName','Times', 'fontsize',20)
figure(2)
xlabel('$\mathcal{K}$','interpreter', 'latex', 'FontName','Times','fontsize',50)
ylabel('sample std','FontName','Times','fontsize',40)
title(['IS wrt $W(t)$'], 'FontName','Times', 'fontsize',30,'interpreter','latex')
legend('95% CI bootstrap', 'run 1', '', '', '95% CI bootstrap','run 2', 'FontName','Times', 'fontsize',20)
figure(3)
xlabel('$\mathcal{K}$','interpreter', 'latex', 'FontName','Times','fontsize',50)
ylabel('sample std','FontName','Times','fontsize',40)
title(['IS wrt $\rho_0$'], 'FontName','Times', 'fontsize',30,'interpreter','latex')
legend('95% CI bootstrap', 'run 1', '', '', '95% CI bootstrap','run 2', 'FontName','Times', 'fontsize',20)
figure(4)
xlabel('$\mathcal{K}$','interpreter', 'latex', 'FontName','Times','fontsize',50)
ylabel('sample std','FontName','Times','fontsize',40)
title(['IS with CE'], 'FontName','Times', 'fontsize',30,'interpreter','latex')
legend('95% CI bootstrap', 'run 1', '', '', '95% CI bootstrap','run 2', 'FontName','Times', 'fontsize',20)

%%
[f_both,xboth] = ksdensity(runmax, 'function', 'pdf');
[f_IS_W,x_W] = ksdensity(runmax_W,'function', 'pdf'); 
[f_IS_rho0,x_rho0] = ksdensity(runmax_rho0,'function', 'pdf'); 
[f_MC,x_MC] = ksdensity(runmax_MC, 'function', 'pdf');
[f_CE,x_CE] = ksdensity(runmax_CE, 'function', 'pdf');
figure
plot(x_MC, f_MC, 'r-', x_rho0, f_IS_rho0, 'b:', x_W, f_IS_W, 'c--',xboth, f_both, 'k-.', 'Linewidth',4);
xlabel('u', 'fontsize', 18)
[~, hobj, ~, ~] =legend('MC', 'IS wrt \rho_0', 'IS wrt W(t)', 'IS wrt both','fontsize',25)
ylabel('density function of QoI','fontsize', 18)
title('$\mathcal{K}=2.5$','interpreter', 'latex','fontsize', 20)
pp = get(gca,'XTickLabel');
set(gca,'XTickLabel',pp,'FontName','Times','fontsize',20)
hl = findobj(hobj,'type','line');
set(hl,'LineWidth',2.5);
set(gca,'FontSize',40)

% %% test
% M=10^5;
% %Conditional Gaussian rho_0(x|v) 
% vSmpl=mFit+sigFit*randn(1,M);
% mCond=mu(1)+corr*(vSmpl-mu(2))*sqrt(Sigma(1,1)/Sigma(2,2));
% sigCond=sqrt((1-corr^2)*Sigma(1,1));
% %Conditional Gaussian rho_0(x|v) 
% vSmpl_both=mFit_both+sigFit_both*randn(1,M);
% mCond_both=mu(1)+corr*(vSmpl_both-mu(2))*sqrt(Sigma(1,1)/Sigma(2,2));
% sigCond_both=sqrt((1-corr^2)*Sigma(1,1));
% 
% timetoachieveK_MC=[];
% timetoachieveK_both=[];
% timetoachieveK_W=[];
% timetoachieveK_rho0=[];
% timetoachieveK_CE=[];
% h=zeros(1,M);h_MC=h;h_rho0=h;h_W=h;
% runmax=h;runmax_MC=h;runmax_rho0=h; runmax_W=h;
% u=zeros(d,N,M);
% u0_both=[mCond_both+sigCond_both.*randn(1,M); vSmpl_both];
% u0_rho0=[mCond+sigCond.*randn(1,M); vSmpl];
% u0_MC=mu+chol(Sigma)'*randn(d,M);
% u0_W=mu+chol(Sigma)'*randn(d,M);
% u0_CE=mu_tilde+chol(Sigma)'*randn(d,M);
% r=exp(-kappa*dt);
% sigOU=sqrt(2*kappa*Temp*(1-exp(-2*kappa*dt))/(2*kappa));
% t=linspace(0,T,N+1);
% meanL=0;
% parfor m=1:M
%     u=u0_both(:,m);
%     u_MC=u0_MC(:,m);
%     u_rho0=u0_rho0(:,m);
%     u_W=u0_W(:,m);
%     u_CE=u0_CE(:,m);
%     
%     %IS wrt both
%     L=1;
%     k=1;
%     t_k=0;
%     while k<=N && P1*u(:,k)<K
% 
%         W1=randn;
% 
%         u(2,k+1)=r*u(2,k)+sigOU*W1+b(2)*ksi(u(2,k),t_k)*dt;
%         u(2,k+1)=u(2,k+1)-dUdx(u(1,k))*dt;
%         u(1,k+1)=u(1,k)+u(2,k+1)*dt;
%         L=L*exp(-0.5*dt*(ksi(u(2,k),t_k))^2-ksi(u(2,k),t_k)*sqrt(dt)*W1);
% 
%         k=k+1;
%         t_k=t_k+dt;
%     end
%     
%     %IS wrt W(t)
%         L_W=1;
%         k=1;
%         t_k=0;
%         while k<=N && P1*u_W(:,k)<K
% 
%         W4=randn;
% 
%         u_W(2,k+1)=r*u_W(2,k)+sigOU*W4+b(2)*ksi(u_W(2,k),t_k)*dt;
%         u_W(2,k+1)=u_W(2,k+1)-dUdx(u_W(1,k))*dt;
%         u_W(1,k+1)=u_W(1,k)+u_W(2,k+1)*dt;
%         L_W=L_W*exp(-0.5*dt*(ksi(u_W(2,k),t_k))^2-ksi(u_W(2,k),t_k)*sqrt(dt)*W4);
%         
%         k=k+1;
%         t_k=t_k+dt;
%         end
%     
%     %MC and IS wrt rho_0
%     for n=1:N
%         W2=randn;
%         W3=randn;
%         W5=randn;
%         
%         u_MC(2,n+1)=r*u_MC(2,n)+sigOU*W2;
%         u_MC(2,n+1)=u_MC(2,n+1)-dUdx(u_MC(1,n))*dt;
%         u_MC(1,n+1)=u_MC(1,n)+u_MC(2,n+1)*dt;
%         
%         u_rho0(2,n+1)=r*u_rho0(2,n)+sigOU*W3;
%         u_rho0(2,n+1)=u_rho0(2,n+1)-dUdx(u_rho0(1,n))*dt;
%         u_rho0(1,n+1)=u_rho0(1,n)+u_rho0(2,n+1)*dt;   
%         
%         u_CE(2,n+1)=r*u_CE(2,n)+sigOU*W5;
%         u_CE(2,n+1)=u_CE(2,n+1)-dUdx(u_CE(1,n))*dt;
%         u_CE(1,n+1)=u_CE(1,n)+u_CE(2,n+1)*dt;
%     end
%     
%     meanL=meanL+L;
%     h(m)=(max(P1*u(:,:))>=K)*L;
%     h_MC(m)=(max(P1*u_MC(:,:))>=K);
%     h_rho0(m)=(max(P1*u_rho0(:,:))>=K);
%     h_W(m)=(max(P1*u_W(:,:))>=K)*L_W;
%     h_CE(m)=(max(P1*u_CE(:,:))>=K);
% 
%     runmax(m)=max(P1*u);
%     runmax_MC(m)=max(P1*u_MC);
%     runmax_rho0(m)=max(P1*u_rho0);
%     runmax_W(m)=max(P1*u_W);
%     runmax_CE(m)=max(P1*u_CE);
%     
%     if runmax_MC(m)>=K
%     [MaxValue_MC, indx_MC]=max(P1*u_MC);
%     timetoachieveK_MC=[timetoachieveK_MC; t(indx_MC)];
%     end
%     
%     if runmax(m)>=K
%     [MaxValue_both, indx_both]=max(P1*u);
%     timetoachieveK_both=[timetoachieveK_both; t(indx_both)];
%     end
%     if runmax_W(m)>=K
%     [MaxValue_W, indx_W]=max(P1*u_W);
%     timetoachieveK_W=[timetoachieveK_W; t(indx_W)];
%     end
%     if runmax_rho0(m)>=K
%     [MaxValue_rho0, indx_rho0]=max(P1*u_rho0);
%     timetoachieveK_rho0=[timetoachieveK_rho0; t(indx_rho0)];
%     end
%     if runmax_CE(m)>=K
%     [MaxValue_CE, indx_CE]=max(P1*u_CE);
%     timetoachieveK_CE=[timetoachieveK_CE; t(indx_CE)];
%     end   
% end
% %%
% figure
% subplot(1,2,1)
% [f_both,xboth] = ksdensity(runmax, 'function', 'pdf');
% [f_IS_W,x_W] = ksdensity(runmax_W,'function', 'pdf'); 
% [f_IS_rho0,x_rho0] = ksdensity(runmax_rho0,'function', 'pdf'); 
% [f_MC,x_MC] = ksdensity(runmax_MC, 'function', 'pdf'); 
% [f_CE,x_CE] = ksdensity(runmax_CE, 'function', 'pdf'); 
% plot(x_MC, f_MC, 'r-', x_rho0, f_IS_rho0, 'b:', x_W, f_IS_W, 'm--',xboth, f_both, 'k-.', 'Linewidth',4);
% % hold on
% % plot([K K],[y(1) y(2)], '-k', 'LineWidth', 4)
% % hold off
% xlim([-4 4])
% xlabel('u', 'fontsize', 18)
% [~, hobj, ~, ~] =legend('MC', 'IS wrt \rho_0', 'IS wrt W(t)', 'IS wrt both','fontsize',30)
% ylabel('density function of QoI','fontsize', 18)
% pp = get(gca,'XTickLabel');
% set(gca,'XTickLabel',pp,'FontName','Times','fontsize',20)
% hl = findobj(hobj,'type','line');
% set(hl,'LineWidth',2.5);
% set(gca,'FontSize',40)
% 
% subplot(1,2,2)
% histogram(timetoachieveK_rho0,'Normalization', 'pdf','facecolor','b','facealpha',0.8,'edgecolor','b')
% %ylim([0 20])
% xlim([0 1])
% xlabel('t')
% ylabel('density function of time', 'fontsize', 18)
% pp = get(gca,'XTickLabel');
% set(gca,'XTickLabel',pp,'FontName','Times','fontsize',20)
% hl = findobj(hobj,'type','line');
% set(hl,'LineWidth',2.5);
% set(gca,'FontSize',40)
% hold on
% 
% histogram(timetoachieveK_W,'Normalization', 'pdf','facecolor','m','facealpha',0.8,'edgecolor','m')
% xlim([0 1])
% %ylim([0 20])
% xlabel('t','fontsize', 18)
% pp = get(gca,'XTickLabel');
% set(gca,'XTickLabel',pp,'FontName','Times','fontsize',20)
% hl = findobj(hobj,'type','line');
% set(hl,'LineWidth',2.5);
% set(gca,'FontSize',40)
% 
% histogram(timetoachieveK_both,'Normalization', 'pdf','facecolor','k','facealpha',0.8,'edgecolor','k')
% xlim([0 1])
% %ylim([0 20])
% %title('$\sigma_0=1, \mathcal{K}=2$','interpreter', 'latex','fontsize', 40)
% xlabel('t','fontsize', 18)
% legend('IS wrt \rho_0', 'IS wrt W(t)', 'IS wrt both')
% pp = get(gca,'XTickLabel');
% set(gca,'XTickLabel',pp,'FontName','Times','fontsize',20)
% hl = findobj(hobj,'type','line');
% set(hl,'LineWidth',2.5);
% set(gca,'FontSize',40)
% sgtitle('$\mathcal{K}=2.5$','interpreter', 'latex','fontsize', 40)
