close all 
clear all

a =@(u) -0.5*u;
b =@(u) ones(size(u));

M =30;
N =20;
dt = 1/N;

v = zeros(M,N+1);
v(:,1) = 0.9*randn(M,1)+0.3*rand(M,1)+1

for i =1:N
    v(:,i+1) = v(:,i) + a(v(:,i))*dt + b(v(:,i)).*randn(M,1)*sqrt(dt);
end
fig1 = figure(1)
%subplot_tight(1,1,[0.02 0.01])
t= linspace(0,1,N+1)
[fStart,xiStart] =  ksdensity(v(:,1));
sStart = patch([xiStart fliplr(xiStart)],[zeros(size(xiStart)) fliplr(fStart)],[0.7 0.7 .7]);
alpha(sStart,0.6)
hold on
scatter(v(:,1),t(1)*ones(M,1),250, 'ro','filled')

% txt = '$$\rho_{u_{n} | y_{1:n}} \rightarrow $$';
% text(-1.9,0.12,txt, 'Interpreter','latex','fontsize',17)
% 
% str = '$$ \bullet \,  \hat v_n$$';
% text(-1.8,1.9,str,'Interpreter','latex','fontsize',17,'color','b')

axis([-2 3.5 0 2])


%set(fig1, 'Units', 'Normalized', 'OuterPosition', [0.1, 0.2, 0.9, 0.8]);
%axis tight
saveas(gcf, 'enkf1', 'epsc')

%pause()

plot(v,t,'linewidth',1.1)
[fEnd,xiEnd] = ksdensity(v(:,end))
sEnd = patch([xiEnd fliplr(xiEnd)],[ones(size(xiEnd)) fliplr(fEnd+1)],[0.7 0.7 .7]);
alpha(sStart,0.2)
alpha(sEnd,0.6)
scatter(v(:,end),t(end)*ones(M,1),250, 'ko')
% str = '$$ \rho_{u_{n+1} | y_{1:n}} $$';
% h2 = text(0.2,1.1,str,'Interpreter','latex','fontsize',17)

% str = '$$ \bullet \,  v_{n+1} = \Psi^N(\hat v_n)$$';
% text(-1.8,1.75,str,'Interpreter','latex','fontsize',17,'color','red')
axis([-2 3.5 0 2])

%set(fig1, 'Units', 'Normalized', 'OuterPosition', [0.1, 0.2, 0.9, 0.8]);
%axis tight
saveas(gcf, 'enkf2', 'epsc')

%pause()

y = -0.7;
Gamma = 1;
H =1;
C = cov(v(:,end));
K = C*H/(H*C*H+Gamma);
vHat = (1-K*H)*v(:,end)+ K*y;
% str = '$$ \bullet \, y_{n+1} $$';
% text(-1.8,1.6,str,'Interpreter','latex','fontsize',17,'color','g')
[fHat,xiHat] = ksdensity(vHat);

% str = '$$ \bullet \, \hat v_{n+1} $$';
% text(-1.8,1.45,str,'Interpreter','latex','fontsize',17,'color','k')
% delete(sEnd)
% delete(h2)
sHat = patch([xiHat fliplr(xiHat)],[ones(size(xiHat)) fliplr(fHat+1)],[0.7 0.7 .8]);
alpha(sHat, 0.6)
alpha(sStart,0.8)
scatter(y,t(end),500, 'gp','filled');


vHat = scatter(vHat,t(end)*ones(M,1),250, 'ro','filled');
scatter(y,t(end),600, 'gp','filled');
% str = '$$ \rho_{u_{n+1}|y_{1:n+1}} $$';
% text(0.,max(fHat)/7+1,str,'Interpreter','latex','fontsize',17,'color','k')
%set(fig1, 'Units', 'Normalized', 'OuterPosition', [0.1, 0.2, 0.9, 0.8]);
%axis tight
saveas(gcf, 'enkf3', 'epsc')
