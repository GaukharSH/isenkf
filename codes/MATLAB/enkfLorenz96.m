clear;%close all; set(0,'defaultaxesfontsize',20);format long
%%% EnKF, Lorenz 96
%% setup
T=20; % simulation length
M=10000;
q=1;r=1;p=20;N=r*p;% observe q/r coordinates in N dimensions
%N = 12;
J=100;F=7;        % number of steps and parameter
gamma=1e-1;       % observational noise variance is gammaˆ2
sigma=sqrt(0.5);  % dynamics noise variance is sigmaˆ2
I=eye(N);C0=I;    % prior initial condition covariance
m0=zeros(N,1);    % prior initial condition mean
tau=1;            % obs time step
dt=tau/J;         % time discretization is tau
ObsSteps = round(T/tau)+1;

% observation operator
%H=I;
H=zeros(q*p,N);
for k=1:p
    H(q*(k-1)+1:q*k,r*(k-1)+1:r*k)=[eye(q),zeros(q,r-q)];
end

m=zeros(N,ObsSteps);yobs=m(1:q*p,ObsSteps);c=zeros(N,N,ObsSteps);% pre-allocate
%Generate the Truth and Observations
vTruth=zeros(N,ObsSteps);
vTruth(:,1)=m0+sqrtm(C0)*randn(N,1);% initial truth
yobs(:,1) = H*vTruth(:,1) + gamma*randn(q*p,1);
for n=1:ObsSteps-1
    w=vTruth(:,n);
    for k=1:J
        w=w+f(w,F)*dt + sigma*sqrt(dt)*randn(N,1); %truth  
    end
    vTruth(:,n+1)=w;
    yobs(:,n+1) = H*vTruth(:,n+1) + gamma*randn(q*p,1);%observation
end

v=m0+sqrtm(C0)*randn(N,M);% initial ensemble
m(:,1)=m0;
c(:,:,1)=C0;

%% solution % assimilate!
for i=1:ObsSteps-1
    for j=1:J
        v=v+f(v,F)*dt + sigma*sqrt(dt)*randn(N,M); 
    end 
       mEnKF=sum(v,2)/N;
       Cv=(v-mEnKF)/sqrt(N-1); %centered ensemble
       cEnKF = Cv*Cv';%covariance predict
       K = (cEnKF*H')/(gamma^2*eye(q*p)+H*cEnKF*H');%Kalman gain
       yTilde=yobs(:,i+1)+gamma*randn(q*p,M);%perturbed observation
       v=(I-K*H)*v+K*yTilde;%ensemble update
       m(:,i+1) = mean(v,2);%mean update
       CvHat=(v-m(:,i+1))/sqrt(N-1);%centered ensemble
       c(:,:,i+1) = CvHat*CvHat';%covariance update  
    
end
s=2;
%%
figure;
plot([0:T],vTruth(s,1:ObsSteps), 'LineWidth', 2);hold;plot([0:T], yobs(s,1:ObsSteps),'*r','LineWidth', 2); plot([0:T],m(s,1:ObsSteps),'m','LineWidth', 2);
plot([0:T],m(s,1:ObsSteps)+reshape(sqrt(c(s,s,1:ObsSteps)),1,ObsSteps),'r--','LineWidth', 2);
plot([0:T],m(s,1:ObsSteps)-reshape(sqrt(c(s,s,1:1:ObsSteps)),1,ObsSteps),'r--','LineWidth', 2);
hold;grid;xlabel('t');legend('u_2','y','m_2','m_2 \pm c_2ˆ{1/2}')
title('EnKF, L96, T=20, d=40, F=2');
% figure;plot([0:Jj-1],sum((v(:,1:Jj)-m(:,1:Jj)).^2/N));hold;
% grid;hold;xlabel('t');title('ExKF, L96, MSE, 2/3 observed')

function rhs=f(y,F)
rhs=[y(end,:);y(1:end-1,:)].*([y(2:end,:);y(1,:)] -[y(end-1:end,:);y(1:end-2,:)]) - y + F*y.^0;
end
function A=Df(y,N)
A=-eye(N);
A=A+diag([y(end);y(1:end-2)],1);A(end,1)=y(end-1);
A=A+diag(y(2:end-1),-2);A(1,end-1)=y(end);A(2,end)=y(1);
A=A+diag(([y(3:end);y(1)] - [y(end);y(1:end-2)]),-1);
A(1,end)=y(2)-y(end-1);
end