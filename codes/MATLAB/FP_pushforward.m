function [x,rho] = FP_pushforward(x,rho,tau,dVdx,d2Vdx2,sigma,Nt,Nx)
   
    
 % Spatial discretization
 % x  = linspace(x0,x1,Nx+1)';
  dx = x(2)-x(1);      % spatial step size


  dt = tau/Nt;         % time step
  nstep = Nt;   % number of time steps

  % Time stepping scheme (I-dt/2*A)rho_new = (I+dt/2*A)rho_old, A tridiagonal
  Vprimx = dVdx(x);
  Vbisx  = d2Vdx2(x);
  % Sub-diagonal elements of A
  Am     = sigma^2/dx^2/2 - Vprimx(2:Nx+1)/dx/2;
  Am(Nx) = Am(Nx) + Vprimx(Nx+1)/dx/2+sigma^2/dx^2/2;         % Homo. Neumann through ghostpoint
  % Diagonal elements of A
  Ac     = -sigma^2/dx^2+ Vbisx;
  %Ac     = -sigma^2/dx^2+ zeros(size(Vbisx));
  % Super-diagonal elements of A
  Ap     = sigma^2/dx^2/2 + Vprimx(1:Nx)/dx/2;
  Ap(1)  = Ap(1) - Vprimx(1)/dx/2+sigma^2/dx^2/2;          % Homo. Neumann through ghostpoint
  % Matrix multiplying known solution values, at time t
  
  Aexpl = spdiags(repmat([0,1,0],Nx+1,1)+dt/2*[[Am;0],Ac,[0;Ap]],-1:1,Nx+1,Nx+1);
  %Aexpl = spdiags(repmat([0,1,0],Nx+1,1)+[zeros(size(Vbisx)), Vbisx, zeros(size(Vbisx))]+dt/2*[[Am;0],Ac,[0;Ap]],-1:1,Nx+1,Nx+1);
  % Matrix multiplying unknown solution values, at time t+dt
  Aimpl = spdiags(repmat([0,1,0],Nx+1,1)-dt/2*[[Am;0],Ac,[0;Ap]],-1:1,Nx+1,Nx+1);
  % LU-factorization of tri-diagonal matrix has no fill in
  [L,U] = lu(Aimpl);

  % ----- Time stepping scheme ------------------------------------- %

  % Initializations ------------------------------------------------ %

  % Initial value from the equilibrium distribution
  rho = rho/trapezoidal(rho,dx); % Normalized
  
  % ---- This part is just book keeping for the case when rho is
  %      returned at multiple times
  % ------------------------------------------------------------

  % Update the density through likelihood of observation at time 0
 
  % Loop over discretization time steps ---------------------------- %
  for n=1:nstep
    % Implicit time step, linear cost for tri-diagonal system
    rho = U\(L\(Aexpl*rho));
    
  end
  rho = rho./trapezoidal(rho,dx); % Normalize
  
end

% Integration by the trapezoidal rule (expect super-algebraic
% convergence when the integrand is a density which is very close to
% zero near the integration boundaries and suffiently differentiable,
% due to the Euler-MacLaurin formula)

function I = trapezoidal(fx,dx) % For fx column vecor or matrix
  I = (sum(fx)-0.5*(fx(1,:)+fx(end,:)))*dx;
end

