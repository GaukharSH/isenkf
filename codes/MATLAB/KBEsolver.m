function [x,u] = KBEsolver(x,tau,a,b,Nt,Nx)
    
 % Spatial discretization
  dx = x(2)-x(1);      % spatial step size
  dt = tau/Nt;         % time step
  nstep = Nt;          % number of time steps

  % Time stepping scheme (I+dt/2*A)u_{n} = (I-dt/2*A)u_{n+1}, A tridiagonal
  ax  = a(x);
  bx  = b(x);
  % Sub-diagonal elements of A
  Aim    = ax(3:Nx)/dx/2-bx(3:Nx).^2/dx^2/2;
  Aem    = Aim;
  Aim(end)=-ax(Nx)/dx;
  
  % Diagonal elements of A
  Aic    = bx(2:Nx).^2/dx^2;
  Aic(1) = ax(2)/dx;
  Aec    = Aic;
  Aic(end)=-ax(Nx)/dx;
  
  % Super-diagonal elements of A
  Aip    = -ax(2:Nx-1)/dx/2-bx(2:Nx-1).^2/dx^2/2;
  Aip(1) = -ax(2)/dx;
  Aep    = Aip;
  bc      = -(-ax(Nx)/dx/2-bx(Nx).^2/dx^2/2)*dt/2;
  
  % Matrix multiplying known solution values, at time t
  Aexpl = spdiags(repmat([0,1,0],Nx-1,1)-dt/2*[[Aem;0],Aec,[0;Aep]],-1:1,Nx-1,Nx-1);
  % Matrix multiplying unknown solution values, at time t+dt
  Aimpl = spdiags(repmat([0,1,0],Nx-1,1)+dt/2*[[Aim;0],Aic,[0;Aip]],-1:1,Nx-1,Nx-1);
  % LU-factorization of tri-diagonal matrix has no fill in
  [L,U] = lu(Aimpl);

  % ----- Time stepping scheme ------------------------------------- %

  % Initializations ------------------------------------------------ %

  % Final conditon u(x,T)=0
  u = zeros(1,Nx-1)'; 
  r = [zeros(1,Nx-2)'; bc];
  for n=1:nstep
    u = U\(L\(Aexpl*u+r));  
  end
  %u=[u;1];
end




