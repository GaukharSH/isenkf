clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for checking the formula derived for the cdf of
%%% maximum of drifted Brownian motion approximated by Forward Euler solution.
%%% We try it on GBM model for which we have a closed form of the cdf.
%%% So we consider the dynamics of GBM:
%------- du_t=mu u_tdt+sigma u_t dW_t -----------------------------------%
%------------------------------------------------------------------------%
%%% QoI: P(max_{0<=t<=T} u_t >K)
%Model parameters
Gamma=0.5;
kappa=2^(-5)*pi^2;
Temp=1;
mu=[0 0]';
Sigma=[Gamma 0; 0 Gamma];

T=1;      %Final Time
M=10^5;  %Monte Carlo sample size
N=1000;     %Timestep size
d=2;      %problem dimension
J=1;      %Brownian motion W_t dimenson
dt=T/N;   %time discretization
beta=0.01;%CE method parameter

a    = @(x,v) [v; 0.25*(8*x./(1+2*x.^2).^2 - 2*x)-kappa*v]; %2d Langevin drift
b    = [0; sqrt(2*kappa*Temp)];                             %2d Langevin diffusion
dUdx = @(x) 0.25*(-8*x./(1+2*x.^2).^2 + 2*x);               %U'(x)
P1   = [0 1];                                               %Projector

Krng=[2.5 3 3.5 4];                 %the threshold DW


%Initial density parameters in CE method
%P1=[0 1] parameters:
% mu_tilde_PDEv={[0; 0.926632071368393],[0; 1.035884487689959], [0; 1.153770758812496], [0; 1.280397887661908]};
% sigma_tilde_PDEv={[0.499791107452172 0; 0 0.259309474754189],[0.499791107452172   0;0   0.298236569331185], [0.499791107452171 0; 0 0.330502199760880], [0.499791107452172 0; 0 0.356816469234647]};
% mu_tilde_CEv={[-0.031182900453078; -0.045627870669489], [-0.282381870944081; 1.513470037616855],[-0.406150517384421; 1.841052107029484], [-0.680806205988338; 1.861601864786655]};
% sigma_tilde_CEv=repmat({Sigma}, 4,1);

%after fixing the bugs
%P1=[0 1] parameters:
mu_tilde_PDEv={[0; 0.9192], [0; 1.0758], [0; 1.2305], [0; 1.3859]};
sigma_tilde_PDEv={[0.5 0; 0 0.4085], [0.5   0;0   0.4124], [0.5 0; 0 0.4123], [0.5 0; 0 0.4118]};
mu_tilde_CEv={[-0.2815; 1.5578], [-0.3579; 1.8261],[-0.5331; 2.2694], [-0.5998; 2.5182]};
sigma_tilde_CEv=repmat({Sigma}, 4,1);

%P1=[1 0] parameters:
% mu_tilde_PDEv={[1.771484668340343; 0.005179045372664],[2.215045666641432; 0.005179045372664], [2.800650080239603; 0.005179045372664], [3.589164199060899; 0.005179045372664]};
% sigma_tilde_PDEv={[0.053124943995885 0; 0 0.489615020080776],[0.038211558538249   0;0   0.489615020080776], [0.026350717389011 0; 0 0.489615020080776], [0.018681971155761 0; 0 0.489615020080776]};
% mu_tilde_CEv={[0.042679496057375; -0.039537609884099], [2.430791207677585; 0.613730019987208],[2.853383163050362; 0.799923338163523], [3.292459018880470; 0.947846811434624]};
% sigma_tilde_CEv=repmat({Sigma}, 4,1);
C = {'r','m', 'g', 'k', 'y'}; % Cell array of colros.
Cc={'g','b'};
S=10^5;
conf=95;% in percent
B=10^4;
for n=1:2
    
    for i=1:length(Krng)
        K=Krng(i);
        h_PDE=zeros(1,S);
        h_CE=zeros(1,S);
        u_pde=zeros(d,N,S);
        u_ce=zeros(d,N,S);
        mu_tilde_PDE=mu_tilde_PDEv{i};
        sigma_tilde_PDE = sigma_tilde_PDEv{i};
        mu_tilde_CE=mu_tilde_CEv{i};
        sigma_tilde_CE=sigma_tilde_CEv{i};
        u0_PDE=mu_tilde_PDE+chol(sigma_tilde_PDE)'*randn(d,S);
        u0_CE=mu_tilde_CE+chol(sigma_tilde_CE)'*randn(d,S);
        r=exp(-kappa*dt);
        sigOU=sqrt(2*kappa*Temp*(1-exp(-2*kappa*dt))/(2*kappa));
        parfor s=1:S
            u_pde=u0_PDE(:,s);
            u_ce=u0_CE(:,s);
            for k=1:N
                u_pde(2,k+1)=r*u_pde(2,k)+sigOU*randn;
                u_pde(2,k+1)=u_pde(2,k+1)-dUdx(u_pde(1,k))*dt;
                u_pde(1,k+1)=u_pde(1,k)+u_pde(2,k+1)*dt;
                
                u_ce(2,k+1)=r*u_ce(2,k)+sigOU*randn;
                u_ce(2,k+1)=u_ce(2,k+1)-dUdx(u_ce(1,k))*dt;
                u_ce(1,k+1)=u_ce(1,k)+u_ce(2,k+1)*dt;
            end

            h_PDE(s)=(max(P1*u_pde(:,:))>=K);
            h_CE(s)=(max(P1*u_ce(:,:))>=K);
        end
        Weight_PDE=mvnpdf(u0_PDE', mu', Sigma)'./mvnpdf(u0_PDE',mu_tilde_PDE',sigma_tilde_PDE)';
        Weight_CE=mvnpdf(u0_CE', mu', Sigma)'./mvnpdf(u0_CE',mu_tilde_CE',sigma_tilde_CE)';

        q_PDE=h_PDE.*Weight_PDE;
        q_CE=h_CE.*Weight_CE;
        
        if isempty(gcp)
            parpool;
        end
        opt = statset('UseParallel',true);
             
        stats_PDE = bootstrp(B, @(x) [mean(x) std(x)], q_PDE, 'Options', opt);
        stats_CE = bootstrp(B, @(x) [mean(x) std(x)], q_CE, 'Options', opt);

        SmplStd_PDE(i)=mean(stats_PDE(:,2));
        SmplStd_CE(i)=mean(stats_CE(:,2));
        
        CId_pde(i) = prctile(stats_PDE(:,2), (100-conf)/2);
        CIu_pde(i)=  prctile(stats_PDE(:,2), (100-(100-conf)/2));
        
        CId_ce(i) = prctile(stats_CE(:,2), (100-conf)/2);
        CIu_ce(i)=  prctile(stats_CE(:,2), (100-(100-conf)/2));
    end
    figure(1)
    plot_ci(Krng',[SmplStd_PDE; CId_pde; CIu_pde],'PatchColor', C{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', C{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', C{n}, 'YScale', 'log');
    hold on
    plot_ci(Krng',[SmplStd_CE; CId_ce; CIu_ce], 'PatchColor', Cc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Cc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Cc{n}, 'YScale', 'log');
    grid on
    hold on
end
    xlabel('K')
    ylabel('sample std')
    legend('95% CI of PDE 1st run','PDE mean 1st run','','','95% CI of CE 1st run','CE mean 1st run','','','95% CI of PDE 2nd run', 'PDE mean 2nd run','','', '95% CI of CE 2nd run', 'CE mean2nd run')

