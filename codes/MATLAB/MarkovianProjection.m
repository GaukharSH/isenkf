clear;  close all;

%Model parameters
Gamma=0.5;
kappa=2^(-5)*pi^2;
Temp=1;
mu=[0 0]';
corr=0.0;
Sigma=[Gamma corr*Gamma; corr*Gamma Gamma];

T=1;      %Final Time
M=10^5;   %Monte Carlo sample size
N=1000;   %Timestep size
d=2;      %problem dimension
J=1;      %Brownian motion W_t dimenson
dt=T/N;   %time discretization
K= 3      %the threshold
beta=0.01;%CE method parameter

a    = @(x,v) [v; 0.25*(8*x./(1+2*x.^2).^2 - 2*x)-kappa*v]; %2d Langevin drift
b    = [0; sqrt(2*kappa*Temp)];                             %2d Langevin diffusion
dUdx = @(x) 0.25*(-8*x./(1+2*x.^2).^2 + 2*x);               %U'(x)
P1   = [0 1];                                               %Projector

%Generate samples of Langevin dynamics to use for both CE and L2 regression
u=zeros(d,N,M);
Mbar=zeros(1,M);

r=exp(-kappa*dt);
sigOU=sqrt(2*kappa*Temp*(1-exp(-2*kappa*dt))/(2*kappa));
for m=1:M
    u(:,1,m)=mu+chol(Sigma)'*randn(d,1);
    for n=2:N+1
        u(2,n,m)=r*u(2,n-1,m)+sigOU*randn;
        u(2,n,m)=u(2,n,m)-dUdx(u(1,n-1,m))*dt;
        u(1,n,m)=u(1,n-1,m)+u(2,n,m)*dt;
    end
    Mbar(m)=max(P1*u(:,:,m));
end

%% L2 regression
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We approximate only the drift, the diffusion is constant

%Define the Polynomial space
w = 2                                                        %Polynomial degree
p = poly_space(w,'TD');                                        %Polynomial space
p_dim = size(p, 1);                                            %Cardinality of polynomial space

t  = linspace(0,T,N+1)';                                       %Generating t_0, t_1, ..., t_{N-1}
Tt = reshape(repmat(t,1,M)', (N+1)*M,1);                       %Replicating M times each t_n and saving as a column vector [t_0 t_0 ... t_0, t_1, ..., t_1,..., t_{N-1}, ..., t_{N_1}]'
Xx = reshape(squeeze(pagemtimes(P1,u))', (N+1)*M,1);           %Projected Samples saved as a column vector [X_0^(1),..., X_0^(M), X_1^(1),..,X_1^(M), ..., X_{N-1}^(1),...,X_{N-1}^(M)]
tX = [Tt Xx];                                                  %Two column vectors of size N*M for [t_n, X_{t_n}]
f  = reshape(squeeze(pagemtimes(P1,a(u(1,:,:),u(2,:,:))))',(N+1)*M,1);   %Given f function, we are going to solve

D     = x2fx(tX, p);                                           %This function helps to create Psi matrix each column is non-orth. basis function psi_p for different given p
[Q,R] = modified_GS(D);                                        %Apply modified Gram-Schmidt process to get QR decomposition
a_p   = Q'*f;                                                  %Solve Normal equations based on orthonormalised basis

psy     = @(t,s) x2fx([t s], p);                               %Non-orth. basis function
psy_bar = @(t,s) x2fx([t s], p)/R;                             %Orth. basis function
a_bar   = @(t,s) psy_bar(t,s)*a_p;                             %Approximation to the drift function a(x)
b_bar   = @(s)   sqrt(P1*(b*b')*P1')*ones(size(s));            %Approximation to the diffusion function b(x)
%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = -3;                                                        %Lower end of discretization interval for x
xU = 3;                                                        %Upper end of discretization interval for x
vL = -2;                                                       %Lower end of discretization interval for v
vU = 4;                                                        %Upper end of discretization interval for v
Nx = 1500;                                                     %Number of spatial step size for x
dx = (xU-xL)/Nx;                                               %Stepsize for x
Nv = 1000;                                                     %Number of spatial step size for v
dv = (vU-vL)/Nv;                                               %Stepsize for v
Nt = 1e3;                                                  %Number of time steps
dt = T/Nt;                                                     %Time step size
xPDE = linspace(xL,xU,Nx+1)';                                  %Linspace for x
vPDE = linspace(vL,vU,Nv+1)';                                  %Linspace for v
sL = P1*[xL;vL];                                               %Lower end of discretization interval for ptojected process s=P_1x
sU = P1*[xU;vU];                                               %Upper end of discretization interval for ptojected process s=P_1x
Ns = P1*[Nx;Nv];                                               %Number of discretization intervals in space s
ds = (sU-sL)/Ns;                                               %Spatial step size in s
sPDE  = linspace(sL,sU,Ns+1)';                                 %Linspace for s

%PDE solution
ileqK  = find(sPDE<=K);                                        %Indexes of sPDE which is less or equal than K
igreK  = find(sPDE>K);                                         %Indexes of sPDE which is greater than K
NsleqK = length(ileqK)-1;
[s, uPDE] = KBEsolverL2copy(sPDE(ileqK),T,a_bar,b_bar,Nt,NsleqK);

figure(1)
plot(s, uPDE, 'Linewidth',2)
xlabel('s', 'fontsize', 14)
ylabel('u(s,0)','fontsize', 14)
%%
if P1(1)==1 && P1(2)==0
    
    [x, v]   = meshgrid(s, vPDE);
    [xx, vv] = meshgrid(sPDE(igreK), vPDE);
    xvLeqK = [x(:) v(:)];
    xvGreK = [xx(:) vv(:)];
    
    rhoTilde_x0 = [reshape(mvnpdf(xvLeqK,mu',Sigma),length(vPDE),length(s)).*sqrt(abs(uPDE))', reshape(mvnpdf(xvGreK,mu',Sigma),length(vPDE),length(sPDE(igreK)))];
    NC = trapezoidal2D(rhoTilde_x0, dx, dv);
    rhoTilde_x0=rhoTilde_x0./NC;
    %trapezoidal2D(rhoTilde_x0, dx, dv)
    
    mFit = mean2d(xPDE(2:end),vPDE,rhoTilde_x0')
    SigmaFit = covMatrix2d(xPDE(2:end),vPDE,rhoTilde_x0')
    [xFit, vFit] = meshgrid(xPDE(2:end), vPDE);
    xv = [xFit(:) vFit(:)];
    rhoFit_x0 = reshape(mvnpdf(xv, mFit', SigmaFit), length(vPDE),length(xPDE(2:end)));
    NCfit=trapezoidal2D(rhoFit_x0, dx, dv);
    
    % figure(2)
    % h=surf(s,vPDE,reshape(mvnpdf(xvLeqK,mu',Sigma),length(vPDE),length(s)).*sqrt(abs(uPDE')));
    % set(h,'LineStyle','none')
    % xlabel('x', 'fontsize', 14)
    % ylabel('v','fontsize', 14)
    % zlabel('rho_{X_0}*sqrt(u)','fontsize', 14)
    
    figure(3)
    h=surf(xPDE(2:end),vPDE,rhoTilde_x0);
    set(h,'LineStyle','none')
    xlabel('x', 'fontsize', 14)
    ylabel('v','fontsize', 14)
    zlabel('rhoTilde_{X_0}','fontsize', 14)

    figure(4)
    h=surf(xPDE(2:end),vPDE,rhoFit_x0);
    set(h,'LineStyle','none')
    xlabel('x', 'fontsize', 14)
    ylabel('v','fontsize', 14)
    zlabel('rhoFit_{X_0}','fontsize', 14)
    
elseif P1(1)==0 && P1(2)==1
    [x, v]   = meshgrid(xPDE, s);
    [xx, vv] = meshgrid(xPDE, sPDE(igreK));
    xvLeqK = [x(:) v(:)];
    xvGreK = [xx(:) vv(:)];
    
    rhoTilde_x0 = [reshape(mvnpdf(xvLeqK,mu',Sigma),length(s),length(xPDE)).*sqrt(abs(uPDE)); reshape(mvnpdf(xvGreK,mu',Sigma),length(sPDE(igreK)),length(xPDE))];
    NC = trapezoidal2D(rhoTilde_x0, dx, dv);
    rhoTilde_x0=rhoTilde_x0./NC;
    trapezoidal2D(rhoTilde_x0, dx, dv)
    
    mFit = mean2d(xPDE,vPDE(2:end),rhoTilde_x0')
    SigmaFit = covMatrix2d(xPDE,vPDE(2:end),rhoTilde_x0')
    [xFit, vFit] = meshgrid(xPDE, vPDE(2:end));
    xv = [xFit(:) vFit(:)];
    rhoFit_x0 = reshape(mvnpdf(xv, mFit', SigmaFit), length(vPDE(2:end)),length(xPDE));
    NCfit=trapezoidal2D(rhoFit_x0, dx, dv)
    
    figure(3)
    h=surf(xPDE,vPDE(2:end),rhoTilde_x0);
    set(h,'LineStyle','none')
    xlabel('x', 'fontsize', 14)
    ylabel('v','fontsize', 14)
    zlabel('rhoTilde_{X_0}','fontsize', 14)

    figure(4)
    h=surf(xPDE,vPDE(2:end),rhoFit_x0);
    set(h,'LineStyle','none')
    xlabel('x', 'fontsize', 14)
    ylabel('v','fontsize', 14)
    zlabel('rhoFit_{X_0}','fontsize', 14)
end
%%
% x0 = linspace(1,5,Nx+1)';                                  
% v0 = linspace(-2,4,Nv+1)';
%   [xx, vv] = meshgrid(x0, v0);
%     xv1 = [xx(:) vv(:)];
%     figure(5)
%     rho_0=reshape(mvnpdf(xv1, mu_tilde', Sigma), length(v0),length(x0));
%     h=surf(xx,vv,rho_0);
%     set(h,'LineStyle','none')
%     xlabel('x', 'fontsize', 14)
%     ylabel('v','fontsize', 14)
%     zlabel('rho_{X_0}','fontsize', 14)

%%
%Compute the estimator for P(max P_1 X >= K)
h=zeros(1,M);
u=zeros(d,N,M);
u0=mFit+chol(SigmaFit)'*randn(d,M);
%u0=mvnrnd(mFit, SigmaFit,M)';
r=exp(-kappa*dt);
sigOU=sqrt(2*kappa*Temp*(1-exp(-2*kappa*dt))/(2*kappa));
for m=1:M
    u=u0(:,m);
    for k=1:N
        u(2,k+1)=r*u(2,k)+sigOU*randn;
        u(2,k+1)=u(2,k+1)-dUdx(u(1,k))*dt;
        u(1,k+1)=u(1,k)+u(2,k+1)*dt;
    end
    h(m)=(max(P1*u(:,end))>=K);
end
Weight=mvnpdf(u0', mu', Sigma)'./mvnpdf(u0',mFit',SigmaFit)';
alpha_hat_L2=mean(h.*Weight)
Vaprx_L2=var(h.*Weight)
RelError_L2=1.96/sqrt(alpha_hat_L2*M)
RelErrorIS_L2=1.96*sqrt(Vaprx_L2)/(alpha_hat_L2*sqrt(M))
varRatio_L2=(RelError_L2/RelErrorIS_L2)^2
pause()

%% MULTILEVEL CROSS-ENTROPY METHOD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  We consider only mean shifting but fixing the covariance
Weight = ones(1,M);
sMbar=sort(Mbar);
K_ell=sMbar(1, ceil((1-beta)*M));
K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
h=(Mbar>=K_ell);
Sigma_tilde=Sigma;
ell=1;
u0=mu+chol(Sigma)'*randn(d,M);

while K_ell<K
    mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight);
    
    ell=ell+1;
    for m=1:M
        u(:,1,m)=mu_tilde+chol(Sigma_tilde)'*randn(d,1);
        u0(:,m)=u(:,1,m);
        for n=2:N+1
            u(2,n,m)=r*u(2,n-1,m)+sigOU*randn;
            u(2,n,m)=u(2,n,m)-dUdx(u(1,n-1,m))*dt;
            u(1,n,m)=u(1,n-1,m)+u(2,n,m)*dt;
        end
        Mbar(m)=max(P1*u(:,:,m));
    end
    sMbar=sort(Mbar);
    K_ell=sMbar(1, ceil((1-beta)*M));
    K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
    h=(Mbar>=K_ell);
    
    Weight=mvnpdf(u0', mu', Sigma)'./mvnpdf(u0',mu_tilde',Sigma_tilde)';
end

if K_ell==K
    mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight)
end
%%
%Compute the estimator for P(max P_1 X >= K)
h=zeros(1,M);
%  mu_tilde=mu
%  Sigma_tilde=Sigma
u0=mu_tilde+chol(Sigma_tilde)'*randn(d,M);

for s=1:M
    u=u0(:,s);
    
    for k=1:N
        u(2,k+1)=r*u(2,k)+sigOU*randn;
        u(2,k+1)=u(2,k+1)-dUdx(u(1,k))*dt;
        u(1,k+1)=u(1,k)+u(2,k+1)*dt;
    end
    h(s)=(max(P1*u(:,end))>=K);
end
Weight=mvnpdf(u0', mu', Sigma)'./mvnpdf(u0',mu_tilde',Sigma_tilde)';
alpha_hat_CE=mean(h.*Weight)
Vaprx_CE=var(h.*Weight)
RelError_CE=1.96/sqrt(alpha_hat_CE*M)
RelErrorIS_CE=1.96*sqrt(Vaprx_CE)/(alpha_hat_CE*sqrt(M))
varRatio_CE=(RelError_CE/RelErrorIS_CE)^2


function M=mean2d(x,v,rhoMF)
M=zeros(2,1);
dx=x(2)-x(1);
dv=v(2)-v(1);

% fx=trapz(rhoMF,2);
% NCx=trapz(fx);
% fx=fx./NCx;
% M(1,1)=trapz(x.*fx);
%
% fv=trapz(rhoMF,1);
% fv=fv./NCx;
% M(2,1)=trapz(v'.*fv);

fx=(sum(rhoMF,2)-0.5*(rhoMF(:,1)+rhoMF(:,end)))*dv;%Integration over v
fv=(sum(rhoMF,1)-0.5*(rhoMF(1,:)+rhoMF(end,:)))*dx;%Integration over x
NC1 = (sum(fx)-0.5*(fx(1)+fx(end)))*dx; %Normalising constant
NC2 = (sum(fv)-0.5*(fv(1)+fv(end)))*dv; %Normalising constant
fx=fx./NC1; %rho_x
fv=fv./NC2; %rho_v
f1=x.*fx;
f2=v'.*fv;
M(1)=(sum(f1)-0.5*(f1(1)+f1(end)))*dx; %E[x]
M(2)=(sum(f2)-0.5*(f2(1)+f2(end)))*dv; %E[v]
end

function C=covMatrix2d(x,v,rhoMF)
C=zeros(2,2);
dx=x(2)-x(1);
dv=v(2)-v(1);

% fx=trapz(rhoMF,2);
% NCx=trapz(fx);
% fx=fx./NCx;
% Ex=trapz(x.*fx);
%
% fv=trapz(rhoMF,1);
% NCv=trapz(fv);
% fv=fv./NCv;
% Ev=trapz(v'.*fv);
%
% C(1,1)=trapz(x.^2.*fx)-Ex^2;
% C(2,2)=trapz(v'.^2.*fv)-Ev^2;
% C(1,2)=trapz(v,trapz(x,rhoMF,1),2)-Ex*Ev;
% C(2,1)=C(1,2);


fx=(sum(rhoMF,2)-0.5*(rhoMF(:,1)+rhoMF(:,end)))*dv;%Integration over v
fv=(sum(rhoMF,1)-0.5*(rhoMF(1,:)+rhoMF(end,:)))*dx;%Integration over x
NC = (sum(fx)-0.5*(fx(1)+fx(end)))*dx; %Normalising constant
fx=fx./NC; %rho_x
fv=fv./NC; %rho_v
f1=x.*fx;
f2=v'.*fv;
Ex=(sum(f1)-0.5*(f1(1)+f1(end)))*dx; %E[x]
Ev=(sum(f2)-0.5*(f2(1)+f2(end)))*dv; %E[v]

f1sqr=x.^2.*fx;
f2sqr=v'.^2.*fv;
Ex2=(sum(f1sqr)-0.5*(f1sqr(1)+f1sqr(end)))*dx;
Ev2=(sum(f2sqr)-0.5*(f2sqr(1)+f2sqr(end)))*dv;
Exv=trapezoidal2D(x*v'.*rhoMF, dx,dv);
C(1,1)=Ex2-Ex^2;
C(2,2)=Ev2-Ev^2;
C(1,2)=Exv-Ex*Ev;
C(2,1)=C(1,2);
end

function I = trapezoidal2D(f,dx,dy) % For fx column vecor or matrix
fx = (sum(f,1)-0.5*(f(1,:)+f(end,:)))*dx;
I = (sum(fx)-0.5*(fx(1)+fx(end)))*dy;
end