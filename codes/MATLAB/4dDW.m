V = @(x,y) 3*exp(-x.^2-(y-1/3).^2)-3*exp(-x.^2-(y-5/3).^2)-5*exp(-(x-1).^2-y.^2)-5*exp(-(x+1).^2-y.^2)+0.2*x.^4+0.2*(y-1/3).^4;

x=linspace(-2,2,100);
y=linspace(-1.5,3,100);
[X,Y]=meshgrid(x,y);
surfc(x,y,V(X,Y))