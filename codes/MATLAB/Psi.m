function [u]=Psi(u0, Nenkf, M, tau, sigma, a_pi)
h=tau/Nenkf;%time discretization
u=u0;%initial ensemble
for n=1:Nenkf
    u= u+a_pi(u)*h+sigma*sqrt(h)*randn(1,M); %Euler scheme of OU
end
end

%OU
% function [u]=Psi(u0, Nenkf, M, tau, sigma)
% h=tau/Nenkf;%time discretization
% u=u0;%initial ensemble
% 
% for n=1:Nenkf
%     u= u*(1-h)+sigma*sqrt(h)*randn(1,M); %Euler scheme of OU
% end
% end