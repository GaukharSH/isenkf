clear;  close all;

K=5;
tau=1;
xL = -5; % Lower end of discretization interval
xU = K;  % Upper end of discretization interval
% xL = 0; % Lower end of discretization interval
% xU = exp(K);  % Upper end of discretization interval
%  Nt = 2^11; % Number of time steps between observations
%  Nx = 100*2.^14;% Number of discretization intervals in space
  Nt = 2.5*1e3; % Number of time steps between observations
  Nx = 1e4;% Number of discretization intervals in space
x  = linspace(xL,xU,Nx+1)';% spatial step size
dt = tau/Nt;
dx = (xU-xL)/Nx;

mu=1;
theta=1;
sigma=1;
a=@(x) theta*(mu-x);    
%a=@(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x); %DW
b=@(x) sigma+0*x;  %OU process 
%b=@(x) sigma*x;   %IGBM process
V      = @(x) 0.25*(2./(1+2*x.^2) + x.^2);
plot(x, V(x))
%%
[x,u] = KBEsolver1(x,tau,a,b,Nt,Nx);%PDE Solution
u(end-5:end)

%%
%plot(log(x(2:end)), u, 'Linewidth',3)
plot(x(2:end), u, 'Linewidth',3)
%title('IGBM process: with change of variable y=e^x')
xlabel('x', 'fontsize', 14)
ylabel('u(x,0)','fontsize', 14)
% Inverse Transform Sampling
mu_0 = -1;
sigma_0 = 1;
xRest=linspace(K,10,Nx+1)';
xx = x(2:end);
rho_x0=@(x, mu_0, sigma_0) exp(-(x-mu_0).^2/(2*sigma_0^2))/(sqrt(2*pi*sigma_0^2));
 rhoTilde_x0=[rho_x0(xx, mu_0, sigma_0).*sqrt(u); rho_x0(xRest(2:end), mu_0, sigma_0)];
%rhoTilde_x0=rho_x0(xx, mu_0, sigma_0).*real(sqrt(u));
%%
y=[xx; xRest(2:end)];
plot(y, rho_x0(y, mu_0, sigma_0), '-r',y, 1.2*10^5*rhoTilde_x0, '-b', y, rho_x0(y, 1, 1),'--b','Linewidth',3)
title('DW process, K=5', 'fontsize',22)
xlabel('x','fontsize',18)
legend({'$\rho_{x_0} \sim N(-1,1)$';'$10^5\times \tilde{\rho}_{x_0}$';'$\tilde{\rho}_{x_0}^{fit}\sim N(1, 1)$'},'fontsize',22,'interpreter','latex')
%%
F=cumtrapz(rhoTilde_x0);
F=F./sum(F);
U=rand(size(F));

%Finverse = @(y) interp1(F, [xx; xRest(2:end)], y, 'linear', 'extrap');
Finverse = @(y) interp1(F, xx, y, 'linear', 'extrap');
u0=Finverse(U);

% %%
% x0=zeros(size(xx));
% for i=1:length(xx)-1
%     x0(i)=(U(i)-F(i))*(xx(i+1)-xx(i))/(F(i+1)-F(i))+xx(i);
% end
mu_0 = 1;
sigma_0 = 1;
xx=x(2:end);
rho_x0=exp(-(xx-mu_0).^2/(2*sigma_0^2))/(sqrt(2*pi*sigma_0^2));
rhoTilde_x0=rho_x0.*real(sqrt(u));
F=cumtrapz(rhoTilde_x0);
U=rand(size(xx));

Finverse = @(y) interp1(F, xx, y, 'linear', 'extrap');
u0=Finverse(U);