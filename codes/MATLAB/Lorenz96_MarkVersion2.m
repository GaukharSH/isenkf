clear;  close all;
%Model parameters
d=10;
F=2;
sigma=0.9
Gamma=0.2
mu=zeros(d,1);
I=eye(d);
Sigma=Gamma*I;
corr=0;

T=1;         %Final Time
M=10^4       %Monte Carlo sample size
K=4          %the threshold
beta=0.01    %CE method parameter
N=1000;       %number of timesteps
dt=T/N;      %discretization step in SDE

a    = @(v) [v(end,:,:); v(1:end-1,:,:)].*([v(2:end,:,:); v(1,:,:)]-[v(end-1:end,:,:);v(1:end-2,:,:)])-v+F*v.^0; %Lorenz 96 drift
b    = sqrt(Sigma);   
P1   = I(1,:);  %Projector
Prest = ones(size(P1))-P1;
%Generate samples of Lorenz 96 dynamics to use for both CE and L2 regression
u=zeros(d,N,M);
Mbar=zeros(1,M);

for m=1:M
    u(:,1,m)=mu+chol(Sigma)'*randn(d,1); 
    for n=1:N
        u(:,n+1,m)=u(:,n,m)+lorenz96(u(:,n,m),F)*dt+sigma*sqrt(dt)*randn(d,1);
    end
    Mbar(m)=max(P1*u(:,:,m));
end
%% L2 regression
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We approximate only the drift, the diffusion is constant

%Define the Polynomial space
w = 2;                                                         %Polynomial degree
p = poly_space(w,'TD');                                        %Polynomial space
p_dim = size(p, 1);                                            %Cardinality of polynomial space

t  = linspace(0,T,N+1)';                                       %Generating t_0, t_1, ..., t_{N-1}
Tt = reshape(repmat(t,1,M)', (N+1)*M,1);                       %Replicating M times each t_n and saving as a column vector [t_0 t_0 ... t_0, t_1, ..., t_1,..., t_{N-1}, ..., t_{N_1}]'
Xx = reshape(squeeze(pagemtimes(P1,u))', (N+1)*M,1);           %Projected Samples saved as a column vector [X_0^(1),..., X_0^(M), X_1^(1),..,X_1^(M), ..., X_{N-1}^(1),...,X_{N-1}^(M)]
tX = [Tt Xx];                                                  %Two column vectors of size N*M for [t_n, X_{t_n}]
f  = reshape(squeeze(pagemtimes(P1,a(u)))',(N+1)*M,1);   %Given f function, we are going to solve

D     = x2fx(tX, p);                                           %This function helps to create Psi matrix each column is non-orth. basis function psi_p for different given p
[Q,R] = modified_GS(D);                                        %Apply modified Gram-Schmidt process to get QR decomposition
a_p   = Q'*f;                                                  %Solve Normal equations based on orthonormalised basis

psy     = @(t,s) x2fx([t s], p);                               %Non-orth. basis function
psy_bar = @(t,s) x2fx([t s], p)/R;                             %Orth. basis function
a_bar   = @(t,s) psy_bar(t,s)*a_p;                             %Approximation to the drift function a(x)
b_bar   = @(s)   sqrt(P1*(b*b')*P1')*ones(size(s));            %Approximation to the diffusion function b(x)
%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sL = -3;                                                       %Lower end of discretization interval for ptojected process s=P_1x
sU = 3;                                                        %Upper end of discretization interval for ptojected process s=P_1x
Ns = 1000;                                                     %Number of discretization intervals in space s
ds = (sU-sL)/Ns;                                               %Spatial step size in s
sPDE  = linspace(sL,sU,Ns+1)';                                 %Linspace for s
Npde=2500;
%PDE solution
ileqK  = find(sPDE<=K);                                        %Indexes of sPDE which is less or equal than K
igreK  = find(sPDE>K);                                         %Indexes of sPDE which is greater than K
NsleqK = length(ileqK)-1;
[s, uPDE] = KBEsolverL2copy(sPDE(ileqK),T,a_bar,b_bar,Npde,NsleqK);
figure(1)
plot(s, uPDE, 'Linewidth',2)
xlabel('s', 'fontsize', 14)
ylabel('u(s,0)','fontsize', 14)
%%
rho_Gaussian = @(x, m, sig) exp(-(x-m).^2/(2*sig^2))/(sqrt(2*pi*sig^2)); 
rhoMarg_v1 = rho_Gaussian(s, P1*mu, sqrt(P1*Sigma*P1'));
rhoTemp = [rhoMarg_v1.*sqrt(abs(uPDE));rho_Gaussian(sPDE(igreK), P1*mu, sqrt(P1*Sigma*P1'))];
NC = trapezoidal(rhoTemp, ds); %normalising constant
rhoTemp=rhoTemp./NC;           %optimal IS density
v=[s;sPDE(igreK)];
plot(s,rhoMarg_v1, 'r', v,rhoTemp, 'b', s, sqrt(abs(uPDE)), 'g')
dv = v(2)-v(1);
mFit = trapezoidal(v.*rhoTemp, dv)
sigFit = sqrt(trapezoidal(v.^2.*rhoTemp, dv)-mFit^2)
rhoTildeFit_0 = rho_Gaussian(v, mFit, sigFit);

v1Smpl=mFit+sigFit*randn(1,M);
mCond=mu(2:end)+corr*(v1Smpl(1)-P1*mu)/sqrt(P1*Sigma*P1');
sigCond=(1-corr^2)*Sigma(2:end,2:end);
%     rhoTildeCond_0= rho_Gaussian(xPDE, mCond, sigCond);
%     rhoJointTilde_0=rhoTildeCond_0*rhoTildeFit_0';
%     
%%
%Compute the estimator for P(max P_1 X >= K)
h=zeros(1,M);
u=zeros(d,N,M);
u0=[v1Smpl; mCond+chol(sigCond)'*randn(d-1,M)];
for s=1:M
    u=u0(:,s);  
    for k=1:N
        u(:,k+1)=u(:,k)+lorenz96(u(:,k),F)*dt+sigma*sqrt(dt)*randn(d,1);
    end    
    h(s)=(max(P1*u)>=K);
end
Weight = rho_Gaussian(P1*u0,P1*mu,sqrt(P1*Sigma*P1'))./rho_Gaussian(P1*u0, mFit, sigFit);
alpha_hat_L2=mean(h.*Weight)
Vaprx_L2=var(h.*Weight)
RelError_L2=1.96/sqrt(alpha_hat_L2*M);
RelErrorIS_L2=1.96*sqrt(Vaprx_L2)/(alpha_hat_L2*sqrt(M))
varRatio_L2=(RelError_L2/RelErrorIS_L2)^2
%% MULTILEVEL CROSS-ENTROPY METHOD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  We consider only mean shifting but fixing the covariance
Weight = ones(1,M);
sMbar=sort(Mbar);
K_ell=sMbar(1, ceil((1-beta)*M));
K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
h=(Mbar>=K_ell);
Sigma_tilde=Sigma;
ell=1;
u0=mu+chol(Sigma)'*randn(d,M);

while K_ell<K
    mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight);
    
    ell=ell+1;
    
    for m=1:M
        u(:,1,m)=mu_tilde+chol(Sigma_tilde)'*randn(d,1);
        u0(:,m)=u(:,1,m);
        for n=2:N+1
            u(:,n,m)=u(:,n-1,m)+lorenz96(u(:,n-1,m),F)*dt+sigma*sqrt(dt)*randn(d,1);
        end
        Mbar(m)=max(P1*u(:,:,m));
    end

    sMbar=sort(Mbar);
    K_ell=sMbar(1, ceil((1-beta)*M));
    K_ell=K_ell*(K_ell<K)+K*(K_ell>=K)
    h=(Mbar>=K_ell);
  
    Weight=mvnpdf(u0', mu', Sigma)'./mvnpdf(u0',mu_tilde',Sigma_tilde)';
end

if K_ell==K
    mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight)
end
%%
%Compute the estimator for P(max P_1 X >= K)
h=zeros(1,M);
%mu_tilde=mu;
%Sigma_tilde=Sigma;
u0=mu_tilde+chol(Sigma_tilde)'*randn(d,M);

for s=1:M
    u=u0(:,s);  
    for k=1:N
        u(:,k+1)=u(:,k)+lorenz96(u(:,k),F)*dt+sigma*sqrt(dt)*randn(d,1);
    end    
    h(s)=(max(P1*u)>=K);
end

Weight=mvnpdf(u0', mu', Sigma)'./mvnpdf(u0',mu_tilde',Sigma_tilde)';
alpha_hat_CE=mean(h.*Weight)
Vaprx_CE=var(h.*Weight)
RelError_CE=1.96/sqrt(alpha_hat_CE*M);
RelErrorIS_CE=1.96*sqrt(Vaprx_CE)/(alpha_hat_CE*sqrt(M))
varRatio_CE=(RelError_CE/RelErrorIS_CE)^2


function M=mean2d(x,v,rhoMF)
M=zeros(2,1);
dx=x(2)-x(1);
dv=v(2)-v(1);

% fx=trapz(rhoMF,2);
% NCx=trapz(fx);
% fx=fx./NCx;
% M(1,1)=trapz(x.*fx);
%
% fv=trapz(rhoMF,1);
% fv=fv./NCx;
% M(2,1)=trapz(v'.*fv);

fx=(sum(rhoMF,2)-0.5*(rhoMF(:,1)+rhoMF(:,end)))*dv;%Integration over v
fv=(sum(rhoMF,1)-0.5*(rhoMF(1,:)+rhoMF(end,:)))*dx;%Integration over x
NC1 = (sum(fx)-0.5*(fx(1)+fx(end)))*dx; %Normalising constant
NC2 = (sum(fv)-0.5*(fv(1)+fv(end)))*dv; %Normalising constant
fx=fx./NC1; %rho_x
fv=fv./NC2; %rho_v
f1=x.*fx;
f2=v'.*fv;
M(1)=(sum(f1)-0.5*(f1(1)+f1(end)))*dx; %E[x]
M(2)=(sum(f2)-0.5*(f2(1)+f2(end)))*dv; %E[v]
end

function C=covMatrix2d(x,v,rhoMF)
C=zeros(2,2);
dx=x(2)-x(1);
dv=v(2)-v(1);

% fx=trapz(rhoMF,2);
% NCx=trapz(fx);
% fx=fx./NCx;
% Ex=trapz(x.*fx);
%
% fv=trapz(rhoMF,1);
% NCv=trapz(fv);
% fv=fv./NCv;
% Ev=trapz(v'.*fv);
%
% C(1,1)=trapz(x.^2.*fx)-Ex^2;
% C(2,2)=trapz(v'.^2.*fv)-Ev^2;
% C(1,2)=trapz(v,trapz(x,rhoMF,1),2)-Ex*Ev;
% C(2,1)=C(1,2);


fx=(sum(rhoMF,2)-0.5*(rhoMF(:,1)+rhoMF(:,end)))*dv;%Integration over v
fv=(sum(rhoMF,1)-0.5*(rhoMF(1,:)+rhoMF(end,:)))*dx;%Integration over x
NC = (sum(fx)-0.5*(fx(1)+fx(end)))*dx; %Normalising constant
fx=fx./NC; %rho_x
fv=fv./NC; %rho_v
f1=x.*fx;
f2=v'.*fv;
Ex=(sum(f1)-0.5*(f1(1)+f1(end)))*dx; %E[x]
Ev=(sum(f2)-0.5*(f2(1)+f2(end)))*dv; %E[v]

f1sqr=x.^2.*fx;
f2sqr=v'.^2.*fv;
Ex2=(sum(f1sqr)-0.5*(f1sqr(1)+f1sqr(end)))*dx;
Ev2=(sum(f2sqr)-0.5*(f2sqr(1)+f2sqr(end)))*dv;
Exv=trapezoidal2D(x*v'.*rhoMF, dx,dv);
C(1,1)=Ex2-Ex^2;
C(2,2)=Ev2-Ev^2;
C(1,2)=Exv-Ex*Ev;
C(2,1)=C(1,2);
end

function I = trapezoidal2D(f,dx,dy) % For fx column vecor or matrix
fx = (sum(f,1)-0.5*(f(1,:)+f(end,:)))*dx;
I = (sum(fx)-0.5*(fx(1)+fx(end)))*dy;
end

function I = trapezoidal(fx,dx) % For fx column vector or matrix
  I = (sum(fx)-0.5*(fx(1,:)+fx(end,:)))*dx;
end

function dv = rk4(v_old,h,F)
% v[t+1] = rk4(v[t],step)
 k1 = lorenz96(v_old,F);
 k2 = lorenz96(v_old+1/2.*h.*k1,F);
 k3 = lorenz96(v_old+1/2.*h.*k2,F);
 k4 = lorenz96(v_old+h.*k3,F);
 dv= 1/6*h.*(k1+2*k2+2*k3+k4);
end

function f=lorenz96(v,F)
f=[v(end); v(1:end-1)].*([v(2:end); v(1)]-[v(end-1:end);v(1:end-2)])-v+F*v.^0;
end