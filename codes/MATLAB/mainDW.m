clear;  close all; set(0, 'defaultaxesfontsize', 15); format long
%%% Multilevel Ensemple Kalman Filter with Local Kalman Gains
%%% Double Well model: dX=a_pi(X)dt+sigma*dW
% ----------------------------------------------------------------------- %
% Definition of the dynamics and the observations                         %
% ----------------------------------------------------------------------- %
% Model parameters
seed=1; rng(seed); %choose random  number seed 
tau   = 1;   % time between observations
T     = 4;  % simulation length
theta = 1;   % scales drift in SDE
sigma = 0.5; % constant diffusion coefficient in SDE
Gamma  =0.1; % variance of Gaussian additive measurement noise
H=1;
u0=0.1;
nobs  = round(T/tau)+1; 

% Model: dX=a_pi(X)dt+sigma*dW - Double Well Potential SDE
a_pi   = @(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x);
V      = @(x) 0.25*(2./(1+2*x.^2) + x.^2); 
dVdx   = @(x) -a_pi(x);
d2Vdx2 = @(x) 0.25*((-8+32*x.^2+96*x.^4)./(1+2*x.^2).^4+2);

% Observations for DW
[v, yobs]=ObsAndParticles(u0,nobs, sigma,tau,H, Gamma, a_pi);

% Discretization parameters to get Reference solution for DW
x0 = -5; % Lower end of discretization interval
x1 = 5;  % Upper end of discretization interval
Nt =  2.5e2; % Number of time steps between observations
Nx = 5e4;% Number of discretization intervals in space
x  = linspace(x0,x1,Nx+1)';% spatial step size
dt=tau/Nt;
dx = (x1-x0)/Nx;

%% Reference moments using DMFEnKF filtering method
mHatMF=zeros(1,nobs);
cHatMF=zeros(1,nobs);

% Initial value from the equilibrium distribution
mHatRef=u0;
cHatRef=Gamma;
rhoMF = exp(-(x-mHatRef).^2/(2*cHatRef)); 
rhoMF = rhoMF./trapezoidal(rhoMF,dx);
mHatMF(1)=trapezoidal(x.*rhoMF,dx);
cHatMF(1)=trapezoidal(x.^2.*rhoMF,dx)-trapezoidal(x.*rhoMF,dx)^2;

for n=1:nobs-1
        
    [x,rhoMF] = FP_pushforward(x,rhoMF,tau,dVdx,d2Vdx2,sigma,Nt,Nx);
    
    %rhoMF update
    cMF=trapezoidal(x.^2.*rhoMF,dx)-trapezoidal(x.*rhoMF,dx)^2;
    K=cMF*H'/(H*cMF*H'+Gamma);
    x_hat=abs(1-K*H)\(x-K*yobs(n+1));
    rho_v = abs(1-K*H)\interp1(x,rhoMF,x_hat,'linear',0);
    rho_eta=K\exp(-(K\x).^2/(2*Gamma))/sqrt(2*pi*Gamma);
    rhoMF=conv(rho_v,rho_eta, 'same');
    rhoMF = rhoMF./trapezoidal(rhoMF,dx);
    mHatMF(n+1)=trapezoidal(x.*rhoMF,dx);
    cHatMF(n+1)=trapezoidal(x.^2.*rhoMF,dx)-trapezoidal(x.*rhoMF,dx)^2;
    
end
%%
AvgN=10;%time average number
Eps=2.^(-(1:1:9)); Eps=[Eps(1)  Eps]; %choose tolerance 

%% Pre-allocate space to save time
rmseEnKFm=zeros(1,length(Eps)); 
rmseMLEnKFm=zeros(1,length(Eps));
rmseEnKFc=zeros(1,length(Eps)); 
rmseMLEnKFc=zeros(1,length(Eps));
tEnKF=zeros(1,length(Eps));
tMLEnKF=zeros(1,length(Eps));

for s=1:length(Eps)
    
Tol=Eps(s);%given tolerance

%initialisation
rmseEnKFmTemp=0;
rmseEnKFcTemp=0;
tEnKFTemp=0;
rmseMLEnKFmTemp=0;
rmseMLEnKFcTemp=0;
tMLEnKFTemp=0;

%Parameters for EnKF
M=10*round(Tol.^(-2)); %Ensemble number
Nenkf = ceil(1/Tol); %Timestep number

parfor j=1:AvgN
%% EnKF 
 tic 
 [mEnKFHat, cEnKFHat]=EnKF(nobs, yobs, H, Gamma,u0, Nenkf, M, tau, sigma, a_pi);%Update EnKF mean and covariance
 
 rmseEnKFmTemp = rmseEnKFmTemp+sqrt(mean((mEnKFHat-mHatMF).^2));%Root Mean Squared Error of mean
 rmseEnKFcTemp = rmseEnKFcTemp+sqrt(mean((cEnKFHat-cHatMF).^2));%Root Mean Squared Error of covariance

 tEnKFTemp=tEnKFTemp+toc;%EnKF wall-clock time 
 
end
tEnKF(s)=tEnKFTemp/AvgN;%average EnKF wall-clock time 
rmseEnKFm(s)=rmseEnKFmTemp/AvgN;%EnkF mean RMSE
rmseEnKFc(s)=rmseEnKFcTemp/AvgN;%EnkF cov RMSE
end
%%
%Plot Convergence rates
c1=0.001*rmseEnKFm(1)/tEnKF(1);
c2=0.03*rmseMLEnKFm(1)/tMLEnKF(1);
 figure(1)
 i=2;j=2;
 loglog( tEnKF(i:end), rmseEnKFm(i:end), '-ob', tEnKF(i:end), c1*tEnKF(i:end).^(-1/3),'-b', 'Linewidth', 3)
 legend('EnKF','c_1 s^{-1/3}')
 xlabel('runtime[s]')
 ylabel('RMSE')
 title('mean')
  figure(2)
 loglog( tEnKF(i:end), rmseEnKFc(i:end), '-ob', tEnKF(i:end), c3*tEnKF(i:end).^(-1/3),'-b', 'Linewidth',3)
 legend('EnKF','c_1 s^{-1/3}','MLEnKF','c_2 s^{-1/2}')
 xlabel('runtime[s]')
 ylabel('RMSE')
title('cov')

function I = trapezoidal(fx,dx) % For fx column vecor or matrix
I = (sum(fx)-0.5*(fx(1,:)+fx(end,:)))*dx;
end


