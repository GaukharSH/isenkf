clear;  close all;

%Model parameters
theta=1;
mu=[1 0.5]';
sigma=1;

T=1;   %Final Time
M=1000;%Monte Carlo sample size
N=100; %Timestep size
d=2;   %problem dimension
J=d;   %Brownian motion W_t dimenson

a = @(x) theta*(mu-x);                   %OU drift
%a=@(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x); %DW drift
b = @(x) sigma*ones(d, J);               %OU/DW diffusion

P1= @(x) mean(x, 1);                     %Projector as the mean of components
%P1 = ones(1,d)./d;                      %The same Projector defined as a vector 

%Polynomial space
w=3;                                     %Polynomial degree
p=poly_space(w,'TD');                    %Polynomial space
p_dim=size(p, 1);                        %Cardinality of polynomial space

h=T/N; %time discretization

%Generate samples for finding the coefficients a_p
u=zeros(d,N,M); 
for m=1:M
u(:,1,m)=mu+sigma*randn(d,1);
dW=sqrt(h)*randn(J,1);
for n=2:N
    u(:,n,m)= u(:,n-1,m)+a(u(:,n-1,m))*h+b(u(:,n-1,m))*dW; %Euler scheme
end
end


%%
t= linspace(0,T,N)';                    %Generating t_0, t_1, ..., t_{N-1}
Tt=reshape(repmat(t,1,M)', N*M,1);      %Replicating M times each t_n and saving as a column vector [t_0 t_0 ... t_0, t_1, ..., t_1,..., t_{N-1}, ..., t_{N_1}]'
Xx=reshape(P1(u), N*M,1);               %Projected Samples saved as a column vector [X_0^(1),..., X_0^(M), X_1^(1),..,X_1^(M), ..., X_{N-1}^(1),...,X_{N-1}^(M)]
%XX=reshape(pagemtimes(P1,u), N*M,1);   %This is if we define P1 as a vector

%% Approximation of a_bar
tX = [Tt Xx];                           %Two column vectors of size N*M for [t_n, X_{t_n}]
f=reshape(P1(a(u)),N*M,1);              %Given f function, we are going to solve 

D=x2fx(tX, p);                          %This function helps to create Psi matrix each column is non-orth. basis function psi_p for different given p
[Q,R]=modified_GS(D);                   %Apply modified Gram-Schmidt process to get QR decomposition
%a_p=R\(Q'*f);                          %Solve LS problem via QR
a_p=Q'*f;                               %Solve Normal equations based on orthonormalised basis

 psy = @(t,s) x2fx([t s], p);           %Non-orth. basis function
% h_hat_a = @(t,s) psy(t,s)*a_p;        %Approximation to a_bar based on psi
psy_bar = @(t,s) x2fx([t s], p)/R;      %Orth. basis function
h_hat_a = @(t,s) psy_bar(t,s)*a_p;      %Approximation to a_bar based on psi_bar

%Plot the regression results
figure(1)
s=linspace(0,1,1000);                   %linspace to plot the functions
a_bar=@(s) theta*P1(mu)-s;              %Exact a_bar
plot(s, a_bar(s),'r*', s, h_hat_a(s',s'),'bo')
legend('Exact a bar', 'L2 regression')

%% Approximation of b_bar
%g=reshape(P1(b(u))*P1(b(u))',N*M,1);
g=repmat(P1(b(u))*P1(b(u))',N*M,1);     
%b_p=R\(Q'*g);
b_p=Q'*g;
%h_hat_b= @(t,s) psy(t,s)*b_p;
h_hat_b= @(t,s) psy_bar(t,s)*b_p;

figure(2)
b_bar2=@(x) P1(b(u))*P1(b(u))'*ones(size(x));
plot(s, b_bar2(s),'r*', s, h_hat_b(s',s'),'bo')
legend('Exact squared b bar', 'L2 regression')
ylim([1 3])

