function [mEnKFHat, cEnKFHat]=EnKF(ObsSteps, yobs, H, Gamma,u0, Nenkf, M, tau, sigma, a_pi)
 %Pre-allocate            
 %u= u0*ones(1,M);
 mEnKFHat=zeros(1,ObsSteps);
 cEnKFHat=zeros(1,ObsSteps);
 mEnKFHat(1)=u0;%initial mean/estimate
 cEnKFHat(1)=Gamma;%initial covariance
 u=mEnKFHat(1)+sqrt(cEnKFHat(1))*randn(1,M);%initial ensemble
for i =1:ObsSteps-1
    
    u=Psi(u, Nenkf, M, tau, sigma,a_pi);%ensemble predict
    mEnKF = mean(u);%mean predict
    cEnKF = cov(u);%covariance predict
    
    K = cEnKF*H'/(Gamma+H*cEnKF*H');%Kalman gain
    yTilde=yobs(i+1)+sqrt(Gamma)*randn(1,M);%perturbed observation
    u=(1-K*H)*u+K*yTilde;%ensemble update
    mEnKFHat(i+1) = mean(u);%mean update
    cEnKFHat(i+1) = cov(u);%covariance update    
end

end