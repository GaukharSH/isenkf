clear;  close all;

K=5           % given threshold
T=1;            % final time
xL = -5;        % Lower end of discretization interval
xU = 10;         % Upper end of discretization interval
Nt = 2.5*1e3;   % Number of time steps 
Nx = 1e4;       % Number of discretization intervals in space
dt = T/Nt;      %time step size
dx = (xU-xL)/Nx;% spatial step size
x  = linspace(xL,xU,Nx+1)'; 

%Model parameters
mu = 1;
theta = 1;
sigma = 1;
a = @(x) theta*(mu-x); %OU or IGBM
%a = @(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x); %DW
b = @(x) sigma+0*x;  %OU or DW 
%b = @(x) sigma*x;   %IGBM 

%PDE Solution
ileqK=find(x<=K); 
igreK=find(x>K);
NxleqK=length(ileqK)-1;
[xx, u] = KBEsolverL2(x(ileqK),T,a,b,Nt,NxleqK); 

figure(1)
plot(xx, u, 'Linewidth',3)
xlabel('x', 'fontsize', 14)
ylabel('u(x,0)','fontsize', 14)
%%
%Initial condition parameters
mu_0 = 1;
sigma_0 = 1;

rho_x0 = @(x, m, sig) exp(-(x-m).^2/(2*sig^2))/(sqrt(2*pi*sig^2)); %original initial density
%%
trapezoidal(rho_x0(x, mu_0, sigma_0),dx)
rhoTilde_x0 = [rho_x0(xx, mu_0, sigma_0).*sqrt(u); rho_x0(x(igreK), mu_0, sigma_0)];
NC = trapezoidal(rhoTilde_x0, dx); %normalising constant
rhoTilde_x0=rhoTilde_x0./NC;       %optimal IS density
trapezoidal(rhoTilde_x0, dx)


%%
%fit optimal IS denisty to Gaussian
x=[xx;x(igreK)];
mFit = trapezoidal(x.*rhoTilde_x0, dx)
sigFit = sqrt(trapezoidal(x.^2.*rhoTilde_x0, dx)-mFit^2)
rhoFit_x0 = rho_x0(x, mFit, sigFit);
trapezoidal(rhoFit_x0,dx)
%%

%Plot the denisties
muCE=2.569185136701359; %CE method results for DW
sigmaCE=1;      
rhoCE_x0 = rho_x0(x, muCE, sigmaCE);
trapezoidal(rhoCE_x0, dx);

figure(2)
plot(x, rho_x0(x, mu_0, sigma_0), '-r',x, rhoTilde_x0, '-b',x, rhoFit_x0, '--b', 'Linewidth',3)
title(['OU process K=' num2str(K)],  'fontsize',22)
xlabel('x','fontsize',18)
legend({'$\rho_{x_0} \sim N(1,1)$';'$ \tilde{\rho}_{x_0}^{PDE}$'; ['$\tilde{\rho}_{x_0}^{fit}\sim N$(' num2str(mFit) ',' num2str(sigFit) ')'];['$\tilde{\rho}_{x_0}^{CE}\sim N$(' num2str(muCE) ',' num2str(sigmaCE) ')']},'fontsize',22,'interpreter','latex')

% %% Inverse Transform Sampling
% F=cumtrapz(unique(rhoTilde_x0));
% F=F./sum(F);
% U=rand(size(F));
% 
% Finverse = @(y) interp1(F, x, y, 'linear', 'extrap');
% u0=Finverse(U);



% x0=zeros(size(x));
% for i=1:length(x)-1
%     x0(i)=(U(i)-F(i))*(x(i+1)-x(i))/(F(i+1)-F(i))+x(i);
% end
%
% [vmax, idx]=max(rhoTilde_x0);
% mFit = x(idx);


function I = trapezoidal(fx,dx) % For fx column vector or matrix
  I = (sum(fx)-0.5*(fx(1,:)+fx(end,:)))*dx;
end
