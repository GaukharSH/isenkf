function [Q,R, W] = GS(X)
    [n,p] = size(X);
    Q = zeros(n,p);
    R = zeros(p,p);
    W = zeros(p,p);
    for j = 1:p
        Q(:,j) = X(:,j);
        for i=1:j-1
            R(i,j) = Q(:,i)'*X(:,j);
            Q(:,j) = Q(:,j) - R(i,j)*Q(:,i);
        end
        R(j,j) = norm(Q(:,j));
        W(j,j) = 1/R(j,j);
        W(1:j-1,j)=-R(1:j-1,j)/R(j,j);
        Q(:,j) = Q(:,j)/R(j,j);
     end
end

% D=ones(N*M, p_dim);
% for i=1:p_dim
%     for j=1:2
%         D(:,i)=D(:,i).*tX(:,j).^p(i,j);
%     end
% end
% y=[];
% y=[y; repmat(X,1,N)];
% f=P1(a(y))'; 
% %[Q,R]=qr(D);
% %[Q,R]=modified_GS(D);
% %%
% % I = eye(size(D,2));
% % err_mgs = norm(Q*R-D,inf)/norm(D,inf)
% % qerr_mgs = norm(Q'*Q-I,inf)
% a_p=R\(Q'*f);
% 
% %%
% for n=1:N
%    [Q,R]= modified_GS(D(n*(1:M),:));
%    a_coeff(:,n)=R\(Q'*f(n*(1:M)));
% end