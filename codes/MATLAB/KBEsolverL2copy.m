function [x,u] = KBEsolverL2copy(x,T,a,b,Nt,Nx)
  time  = linspace(T,0,Nt+1);     %Timespace in descending order
  dx = x(2)-x(1);                 %spatial step size
  dt = T/Nt;                      %time step
  
  % Final conditon u(x,T)=0
  u = zeros(1,Nx-1)'; 
  
  %Backward Euler scheme for a few first timesteps
  for n=1:2
      
  % Time stepping scheme (I+dt*A_{n})u_{n} = u_{n+1},
  tt  = repmat(time(n+1),1,length(x))'; %time corresponding to n
  
  % Below we create A_{n} tridiagonal matrix at time n
  ax_n  = a(tt,x);
  bx  = b(x);
  
  % Sub-diagonal elements of A_{n}
  Am_n  = ax_n(3:Nx)/dx/2-bx(3:Nx).^2/dx^2/2;
  
  % Diagonal elements of A_{n}
  Ac_n    = bx(2:Nx).^2/dx^2;
  Ac_n(1) = ax_n(2)/dx;
  
  % Super-diagonal elements of A_{n}
  Ap_n    = -ax_n(2:Nx-1)/dx/2-bx(2:Nx-1).^2/dx^2/2;
  Ap_n(1) = -ax_n(2)/dx;

  
  %Boundary condition at Nx+1
  bc    = (ax_n(Nx)/dx/2+bx(Nx).^2/dx^2/2)*dt;
  
  % Matrix multiplying known solution values, at time t+dt
  Aimpl = spdiags(repmat([0,1,0],Nx-1,1)+dt*[[Am_n;0],Ac_n,[0;Ap_n]],-1:1,Nx-1,Nx-1);
  % ----- Time stepping scheme ------------------------------------- %
  %[L,U] = lu(Aimpl);
  % Initializations ------------------------------------------------ %

    r = [zeros(1,Nx-2)'; bc];
    u = (Aimpl\(u+r));
    %u = U\(L\(u+r));
  end
  
  
  for n=3:Nt
  t   = repmat(time(n),1,length(x))';
  tt  = repmat(time(n+1),1,length(x))';
  
  % Time stepping scheme (I+dt/2*A_n)u_{n} = (I-dt/2*A_{n+1})u_{n+1},
 
  % Below we create A_{n+1} tridiagonal matrix at time n+1
  ax  = a(t,x);
  bx  = b(x);
  
  % Sub-diagonal elements of A_{n+1}
  Am  = ax(3:Nx)/dx/2-bx(3:Nx).^2/dx^2/2;
  
  % Diagonal elements of A_{n+1}
  Ac    = bx(2:Nx).^2/dx^2;
  Ac(1) = ax(2)/dx;
  
  % Super-diagonal elements of A_{n+1}
  Ap    = -ax(2:Nx-1)/dx/2-bx(2:Nx-1).^2/dx^2/2;
  Ap(1) = -ax(2)/dx;
  
  % Below we create A_n tridiagonal matrix
  ax_n  = a(tt,x);
  
  % Sub-diagonal elements of A_n
  Am_n    = ax_n(3:Nx)/dx/2-bx(3:Nx).^2/dx^2/2;
  
  % Diagonal elements of A_n
  Ac_n    = bx(2:Nx).^2/dx^2;
  Ac_n(1) = ax_n(2)/dx;
  
  % Super-diagonal elements of A_n
  Ap_n    = -ax_n(2:Nx-1)/dx/2-bx(2:Nx-1).^2/dx^2/2;
  Ap_n(1) = -ax_n(2)/dx;
  
  %Boundary condition at Nx+1
  bc    = ((ax_n(Nx)+ax(Nx))/dx/2+bx(Nx).^2/dx^2)*dt/2;
  
  % Matrix multiplying known solution values, at time t+dt
  Aexpl = spdiags(repmat([0,1,0],Nx-1,1)-dt/2*[[Am;0],Ac,[0;Ap]],-1:1,Nx-1,Nx-1);
  % Matrix multiplying unknown solution values, at time t
  Aimpl = spdiags(repmat([0,1,0],Nx-1,1)+dt/2*[[Am_n;0],Ac_n,[0;Ap_n]],-1:1,Nx-1,Nx-1);
  % LU-factorization of tri-diagonal matrix has no fill in
  %[L,U] = lu(Aimpl);

  % ----- Time stepping scheme ------------------------------------- %

  % Initializations ------------------------------------------------ %

    r = [zeros(1,Nx-2)'; bc];
    %u = U\(L\(Aexpl*u+r));  
    u = Aimpl\(Aexpl*u+r); 
  end
  x=x(2:end); %x_0 is excluded since u(0,t) is approximated and incorporated to u(x_1,t) 
  u=[u;1];    %u(x_{Nx+1}, t)=1 since we have boundary condition u(K,t)=1

end



% function [x,u] = KBEsolverL2copy(x,tau,a,b,Nt,Nx)
%   time  = linspace(tau,0,Nt+1);
% 
%  % Spatial discretization
%   dx = x(2)-x(1);      % spatial step size
%   dt = tau/Nt;         % time step
%   % Final conditon u(x,T)=0
%   u = zeros(1,Nx-1)'; 
%   
%   for n=1:Nt+1
%   t  = repmat(time(n),1,length(x))';
%   % Time stepping scheme (I+dt/2*A_n)u_{n} = (I-dt/2*A_{n+1})u_{n+1}, A tridiagonal
%   ax  = a(t,x);
%   bx  = b(x);
%   % Sub-diagonal elements of A
%   Am    = ax(3:Nx)/dx/2-bx(3:Nx).^2/dx^2/2;
%   
%   % Diagonal elements of A
%   Ac    = bx(2:Nx).^2/dx^2;
%   Ac(1) = ax(2)/dx;
%   
%   % Super-diagonal elements of A
%   Ap    = -ax(2:Nx-1)/dx/2-bx(2:Nx-1).^2/dx^2/2;
%   Ap(1) = -ax(2)/dx;
%   bc    = (ax(Nx)/dx/2+bx(Nx).^2/dx^2/2)*dt;
%   
%   % Matrix multiplying known solution values, at time t
%   Aexpl = spdiags(repmat([0,1,0],Nx-1,1)-dt/2*[[Am;0],Ac,[0;Ap]],-1:1,Nx-1,Nx-1);
%   % Matrix multiplying unknown solution values, at time t+dt
%   Aimpl = spdiags(repmat([0,1,0],Nx-1,1)+dt/2*[[Am;0],Ac,[0;Ap]],-1:1,Nx-1,Nx-1);
%   % LU-factorization of tri-diagonal matrix has no fill in
%   %[L,U] = lu(Aimpl);
% 
%   % ----- Time stepping scheme ------------------------------------- %
% 
%   % Initializations ------------------------------------------------ %
% 
%     r = [zeros(1,Nx-2)'; bc];
%     %u = U\(L\(Aexpl*u+r));  
%     u = Aimpl\(Aexpl*u+r); 
%   end
%   x=x(2:end);
%   u=[u;1];
% end
