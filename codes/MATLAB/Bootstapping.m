clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for checking the formula derived for the cdf of
%%% maximum of drifted Brownian motion approximated by Forward Euler solution.
%%% We try it on GBM model for which we have a closed form of the cdf.
%%% So we consider the dynamics of GBM:
%------- du_t=mu u_tdt+sigma u_t dW_t -----------------------------------%
%------------------------------------------------------------------------%
%%% QoI: P(max_{0<=t<=T} u_t >K)
%Problem parameters
mu=1;
theta=1;
sigma=1;
%a=@(x)theta*(mu-x);              %the drift parameter
b=@(x) sigma;                     %the diffusion parameter
a=@(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x); %DW
T=1;                              %simulation length
%Krng=[4 4.5 5 5.5];              %the threshold OU
Krng=[2 3 3.5 4];                 %the threshold DW

Kstep=1000;                       %Numerical timestep number
dt = T/Kstep;                     %Timestep

%Initial density parameters in CE method
%mu_0 = 1; %OU
mu_0 = -1; %DW
sigma_0 = 1;
% mu_tilde_CEv=[2.745581440587936 3.809392787771496 4.528265934046741 5.304480593995933];%OU
% sigma_tilde_CEv=1*ones(size(Krng));%OU
mu_tilde_CEv=[0.833269447206876 1.054697897415616 1.928335502214481 2.214791386815672];%DW
sigma_tilde_CEv=1*ones(size(Krng));%DW
% mu_tilde_PDEv=[2.768991980734020 3.109551597073155 3.452486473235639 3.798007791483904];OU old
% sigma_tilde_PDEv=[1.035145572797087 1.102924601023074 1.176807061258182 1.255846227194299];OU old
% mu_tilde_PDEv=[2.701010990012368 3.024390118230905 3.350024397856616 3.677886219446528];%OU after fixing the bug
% sigma_tilde_PDEv=[1.004408380335971 1.062841302029570 1.127113840264130 1.196029541827453];%OU after fixing the bug
mu_tilde_PDEv=[0.141184530482965 0.482750644779824 0.651704570763817 0.822946117195169];%DW after fixing the bug
sigma_tilde_PDEv=[0.866111707328515 0.879542098489083 0.8866300171232420 0.893633608625759];%DW after fixing the bug
C = {'r','m', 'g', 'k', 'y'}; % Cell array of colros.
Cc={'g','b'};
S=10^5;
conf=95;% in percent
B=10^4;
for n=1:2
    
    
    for i=1:length(Krng)
        K=Krng(i);
        h_PDE=zeros(1,S);
        h_CE=zeros(1,S);
        mu_tilde_PDE=mu_tilde_PDEv(i);
        sigma_tilde_PDE = sigma_tilde_PDEv(i);
        mu_tilde_CE=mu_tilde_CEv(i);
        sigma_tilde_CE=sigma_tilde_CEv(i);
        u0_PDE=mu_tilde_PDE+sigma_tilde_PDE*randn(1,S);
        u0_CE=mu_tilde_CE+sigma_tilde_CE*randn(1,S);
        
        parfor s=1:S
            P1_pde=1;
            P1_ce=1;
            u_pde=u0_PDE(s);
            u_ce=u0_CE(s);
            for k=1:Kstep
                t_pde=(b(u_pde))^2*dt;
                t_ce=(b(u_ce))^2*dt;
                dW_pde = sqrt(dt)*randn;
                dW_ce  = sqrt(dt)*randn;
                P1_pde=P1_pde*(1-exp(-2*(K-u_pde)^2/t_pde+2*(b(u_pde)*dW_pde+a(u_pde)*dt)*(K-u_pde)/t_pde))*(K-u_pde>max(0,b(u_pde)*dW_pde+a(u_pde)*dt));
                P1_ce=P1_ce*(1-exp(-2*(K-u_ce)^2/t_ce+2*(b(u_ce)*dW_ce+a(u_ce)*dt)*(K-u_ce)/t_ce))*(K-u_ce>max(0,b(u_ce)*dW_ce+a(u_ce)*dt));
                u_pde= u_pde+a(u_pde)*dt+b(u_pde)*dW_pde; %Forward Euler
                u_ce= u_ce+a(u_ce)*dt+b(u_ce)*dW_ce; %Forward Euler
            end
            h_PDE(s)=1-P1_pde;
            h_CE(s)=1-P1_ce;
        end
        Weight_PDE=(sigma_tilde_PDE/sigma_0)*exp(-(u0_PDE-mu_0).^2./(2*sigma_0^2)+(u0_PDE-mu_tilde_PDE).^2./(2*sigma_tilde_PDE^2));
        Weight_CE=(sigma_tilde_CE/sigma_0)*exp(-(u0_CE-mu_0).^2./(2*sigma_0^2)+(u0_CE-mu_tilde_CE).^2./(2*sigma_tilde_CE^2));
        
        q_PDE=h_PDE.*Weight_PDE;
        q_CE=h_CE.*Weight_CE;
        
        if isempty(gcp)
            parpool;
        end
        opt = statset('UseParallel',true);
             
        stats_PDE = bootstrp(B, @(x) [mean(x) std(x)], q_PDE, 'Options', opt);
        stats_CE = bootstrp(B, @(x) [mean(x) std(x)], q_CE, 'Options', opt);

        SmplStd_PDE(i)=mean(stats_PDE(:,2));
        SmplStd_CE(i)=mean(stats_CE(:,2));
        
        CId_pde(i) = prctile(stats_PDE(:,2), (100-conf)/2);
        CIu_pde(i)=  prctile(stats_PDE(:,2), (100-(100-conf)/2));
        
        CId_ce(i) = prctile(stats_CE(:,2), (100-conf)/2);
        CIu_ce(i)=  prctile(stats_CE(:,2), (100-(100-conf)/2));
    end
    figure(1)
    plot_ci(Krng',[SmplStd_PDE; CId_pde; CIu_pde],'PatchColor', C{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', C{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', C{n}, 'YScale', 'log');
    hold on
    plot_ci(Krng',[SmplStd_CE; CId_ce; CIu_ce], 'PatchColor', Cc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Cc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Cc{n}, 'YScale', 'log');
    grid on
    hold on
end
    xlabel('K')
    ylabel('sample std')
    legend('95% CI of PDE 1st run','PDE mean 1st run','','','95% CI of CE 1st run','CE mean 1st run','','','95% CI of PDE 2nd run', 'PDE mean 2nd run','','', '95% CI of CE 2nd run', 'CE mean2nd run')

