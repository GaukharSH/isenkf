clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for checking the formula derived for the cdf of
%%% maximum of drifted Brownian motion approximated by Forward Euler solution.
%%% We try it on GBM model for which we have a closed form of the cdf. 
%%% So we consider the dynamics of GBM:
%------- du_t=mu u_tdt+sigma u_t dW_t -----------------------------------%
%------------------------------------------------------------------------%
%parpool(4)
%%% Define Model parameters
seed=1; rng(seed); %choose random  number seed
mu=0;              %the drift parameter
sigma=1;         %the diffusion parameter
T=1;               %simulation length 
K=20;              %the threshold
M=10^5;            %number of i.i.d copies 
%%% QoI: P(max_{0<=t<=T} u_t >K)
%%% Reference solution for GBM:
 
Pref=zeros(1,M);
u0= 5+sqrt(0.1)*randn(1,M); %initial u_0
for i=1:M
    Pref(i)=1-normcdf((-(mu-sigma^2/2)*T+log(K/u0(i)))/(sigma*sqrt(T)))+...
    (u0(i)/K)^(1-2*mu/sigma^2)*normcdf((-(mu-sigma^2/2)*T-log(K/u0(i)))/(sigma*sqrt(T)));
end
Pexact=mean(Pref);
%vpa(Pexact);
%% maximum of Brownian Bridge
Kn=2.^(2:2:10);
Paprx=zeros(1, length(Kn));
u_0=5+sqrt(0.1)*randn(1,M); %sampled by rho_0 ~ N(5,0.1)
c=exp(-(K-u_0).^2./(4*u_0.^2*(exp(T)-2)));
uTilde_0=5*c+sqrt(c*0.1).*randn(1,M);
for j=1:length(Kn)
Kstep=Kn(j);
h = T/Kstep;

P=0;
for m=1:M
    P1=1;
<<<<<<< HEAD
    u=u0(m);  
    for k=1:Kstep
=======
    u=uTilde_0(i);  
    for k=1:Kstep
        
>>>>>>> 35083c36f642dd9eb6fd5da0aa1ed8e41cc26734
        b=sigma*u;
        a=mu*u;
        t=b^2*h;
        dW = sqrt(h)*randn;
  
        if K-u>max(0,b*dW+a*h)
            P1=P1*(1-exp(-2*(K-u)^2/t+2*(b*dW+a*h)*(K-u)/t));
        else
            P1=0;
        end 
        u= u*(1+mu*h+sigma*dW); %Forward Euler for GBM 
    end
        
    P=P+(1-P1)/c(i);
end
Paprx(j)=P/M;
%vpa(Paprx)
end
%poolobj = gcp('nocreate');
%delete(poolobj);
%%
dt=T./Kn;
for j=1:length(Kn)
   MSE(j)= mean((Paprx(j)-Pexact)^2);
   Error(j)=abs(Paprx(j)-Pexact);
end
figure(1)
loglog(dt,Paprx,'r-*', dt, Pexact, 'k-o')













%%
% This was when we didn't consider the Brownian bridge
% %% here if I compute for the fixed timestep and compare the approximation with ref value
% M = 100;
% Kstep = 100;
% h = T/Kstep;
% %u=u0+sqrt(Gamma)*randn(1,M); %initial ensemble
% u = u0*ones(1,M); %fixed initial value
% F = normcdf((K-u-mu*u*h)./(sqrt(h)*sigma*u))-exp(2*mu*u.*(K-u)./(sigma*u).^2).*normcdf(-(K-u+mu*u*h)./(sigma*u*sqrt(h)));
% 
% for k=1:Kstep-1
%     u= (1+mu*h)*u+sigma*u*sqrt(h).*randn(1,M); %Forward Euler for GBM
%     F= F.*normcdf((K-u-mu*u*h)./(sqrt(h)*sigma*u))-exp(2*mu*u.*(K-u)./(sigma*u).^2).*normcdf(-(K-u+mu*u*h)./(sigma*u*sqrt(h)));
% end
%     P1=1-sum(F)/M; 
%     MSE1=mean((P1-Pref)^2); %MSE error
% %% here I compute for a sequence of timesteps and check the convergence of the error
% M = 1000;
% Kstep = 2.^(2:1:12);
% dt = T./Kstep;
% P=zeros(1,length(Kstep));
% MSE=zeros(1,length(Kstep));
% for j=1:length(Kstep)
%     h= dt(j);
%     u = u0*ones(1,M); %initial ensemble
%     F = normcdf((K-u-mu*u*h)./(sqrt(h)*sigma*u))-exp(2*mu*u.*(K-u)./(sigma*u).^2).*normcdf(-(K-u+mu*u*h)./(sigma*u*sqrt(h)));
%     for k=1:Kstep(j)-1
%     u= (1+mu*h)*u+sigma*u*sqrt(h).*randn(1,M);
%     F= F.*normcdf((K-u-mu*u*h)./(sqrt(h)*sigma*u))-exp(2*mu*u.*(K-u)./(sigma*u).^2).*normcdf(-(K-u+mu*u*h)./(sigma*u*sqrt(h)));
%     end
%     P(j)=1-sum(F)/M; 
%     MSE(j)=mean((P(j)-Pref)^2);
%     RelError(j)=abs(P(j)-Pref)/Pref;
% end
% loglog(dt,MSE, 'r-*')
% loglog(dt, P, 'r-*', dt, Pref, 'b-o')    
