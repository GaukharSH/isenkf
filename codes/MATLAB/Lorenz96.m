clear; %close all;
d=10;
F=8;
T=0.5;
h=0.01;
%initialize
sigma=0.5
mu=zeros(d,1);
I=eye(d);
Sigma=I;
t=(0:h:T)';
v=zeros(d,length(t));
b=sigma;
v(:,1)=mu+chol(Sigma)'*randn(d,1); %choose initial condition

% [tt,u]=ode45(@(t,v) lorenz96(v,F), [0 T], v(:,1));
% 
% figure(1)
% plot(tt, u(:,1), 'b', 'LineWidth', 2)
% xlabel('t')
% ylabel('v_1')
%%
% error=sqrt(sum((v-u).^2,2));
% figure(2)
% semilogy(t, error, 'k','LineWidth', 2)
% xlabel('t')
% ylabel('error')
M=100;dt=h;
for m=1:M
    v(:,1)=mu+chol(Sigma)'*randn(d,1); %choose initial condition
    %v(:,1)=mu;
for n=1:length(t)-1 %for each time
    v(:,n+1)=v(:,n)+lorenz96(v(:,n),F)*h+b.*sqrt(h).*randn(d,1); %solved via RK4
end

figure(2)
plot(t, v(1,:), 'LineWidth', 2)
xlabel('t', 'fontsize', 14)
ylabel('v_1', 'fontsize', 14)
hold on
end
% title('Noisy Lorenz 96')
% hold off
function dv = rk4(v_old,h,F)
% v[t+1] = rk4(v[t],step)
 k1 = lorenz96(v_old,F);
 k2 = lorenz96(v_old+1/2.*h.*k1,F);
 k3 = lorenz96(v_old+1/2.*h.*k2,F);
 k4 = lorenz96(v_old+h.*k3,F);
 dv= 1/6*h*(k1+2*k2+2*k3+k4);
end

function f=lorenz96(v,F)
f=[v(end); v(1:end-1)].*([v(2:end); v(1)]-[v(end-1:end);v(1:end-2)])-v+F*v.^0;
end

function f=lorenz63(y,r,q,s)
f(1,1)=r*(y(2)-y(1));
f(2,1)=-r*y(1)-y(2)-y(1)*y(3);
f(3,1)=y(1)*y(2)-s*y(3)-s*(r+q);
end