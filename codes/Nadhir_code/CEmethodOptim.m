clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for checking the formula derived for the cdf of
%%% maximum of drifted Brownian motion approximated by Forward Euler solution.
%%% We try it on GBM model for which we have a closed form of the cdf.
%%% So we consider the dynamics of GBM:
%------- du_t=mu u_tdt+sigma u_t dW_t -----------------------------------%
%------------------------------------------------------------------------%
%%% QoI: P(max_{0<=t<=T} u_t >K)
% load('DWK4u02ndwell.mat')
% u0=u0;
%Problem parameters
mu=1;
theta=1;
sigma=1;
%a=@(x)theta*(mu-x);              %the drift parameter
b=@(x) sigma;                     %the diffusion parameter
a=@(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x); %DW
T=1;                              %simulation length
K=4;                            %the threshold
M=10^5;                          %number of i.i.d copies
beta=0.01;                        %CE method parameter
Kstep=1000;                       %Numerical timestep number
dt = T/Kstep;                     %Timestep

%Initial density parameters in CE method
mu_0  = -1;
sigma_0 = 1;
mu_tilde = -1;
sigma_tilde = 1;

u0=zeros(1,M);
u=zeros(1,Kstep);
Mbar=zeros(1,M);
h=zeros(1,M);
Weight=ones(1,M);

%Defining first K_1
u0=mu_tilde+sigma_tilde*randn(1,M);
for m=1:M
    u(1)=u0(m);
    for k=1:Kstep
        dW = sqrt(dt)*randn;
        u(k+1)= u(k)+a(u(k))*dt+b(u(k))*dW; %Forward Euler
    end
    Mbar(m)=max(u);
end
sMbar=sort(Mbar);
K_ell=sMbar(1, ceil((1-beta)*M));
K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
h=Mbar>=K_ell;

%%
ell=1;
while K_ell<K
    mu_tilde=sum(h.*Weight.*u0)/sum(h.*Weight);
    %sigma_tilde=sqrt(sum(h.*Weight.*(u0-mu_tilde).^2)/sum(h.*Weight));
    
    ell=ell+1;
    u0=mu_tilde+sigma_tilde*randn(1,M);
    for m=1:M
        u(1)=u0(m);
        for k=1:Kstep
            dW = sqrt(dt)*randn;
            u(k+1)= u(k)+a(u(k))*dt+b(u(k))*dW; %Forward Euler
        end
        Mbar(m)=max(u);
    end
    sMbar=sort(Mbar);
    K_ell=sMbar(1, ceil((1-beta)*M));
    K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
    h=Mbar>=K_ell;
    Weight=(sigma_tilde/sigma_0)*exp(-(u0-mu_0).^2./(2*sigma_0^2)+(u0-mu_tilde).^2./(2*sigma_tilde^2));
end

if K_ell==K
    mu_tilde=sum(h.*Weight.*u0)/sum(h.*Weight)
    %sigma_tilde=sqrt(sum(h.*Weight.*(u0-mu_tilde).^2)/sum(h.*Weight));
end

%%
%%
N=1;
alpha_hat_Temp=0;
Vaprx_Temp=0;
for n=1:N
S=5*10^5;
h=zeros(1,S);
u0=mu_tilde+sigma_tilde*randn(1,S);
parfor s=1:S
    P1=1;
    u=u0(s);
    for k=1:Kstep
        t=(b(u))^2*dt;
        dW = sqrt(dt)*randn;
        P1=P1*(1-exp(-2*(K-u)^2/t+2*(b(u)*dW+a(u)*dt)*(K-u)/t))*(K-u>max(0,b(u)*dW+a(u)*dt));
        u= u+a(u)*dt+b(u)*dW; %Forward Euler
    end
    h(s)=1-P1;
end
Weight=(sigma_tilde/sigma_0)*exp(-(u0-mu_0).^2./(2*sigma_0^2)+(u0-mu_tilde).^2./(2*sigma_tilde^2));
alpha_hat_Temp=alpha_hat_Temp+mean(h.*Weight);
Vaprx_Temp=Vaprx_Temp+var(h.*Weight);
end
alpha_hat=alpha_hat_Temp/N
Vaprx=Vaprx_Temp/N
RelError=1.96/sqrt(alpha_hat*S);
RelErrorIS=1.96*sqrt(Vaprx)/(alpha_hat*sqrt(S))
varRatio=(RelError/RelErrorIS)^2


