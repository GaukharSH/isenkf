clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for checking the formula derived for the cdf of
%%% maximum of drifted Brownian motion approximated by Forward Euler solution.
%%% We try it on GBM model for which we have a closed form of the cdf.
%%% So we consider the dynamics of GBM:
%------- du_t=mu u_tdt+sigma u_t dW_t -----------------------------------%
%------------------------------------------------------------------------%
%%% QoI: P(max_{0<=t<=T} u_t >K)
%Problem parameters
mu=1;
theta=1;
sigma=1;
a=@(x)theta*(mu-x) ;              %the drift parameter
b=@(x) sigma;                     %the diffusion parameter
T=1;                              %simulation length
K=6;                              %the threshold
M=10^3;                           %number of i.i.d copies
beta=0.01;                        %CE method parameter
Kstep=1000;                       %Numerical timestep number
dt = T/Kstep;                     %Timestep

%Initial density parameters in CE method
mu_0 = 1;
sigma_0 = 1;
mu_tilde(1)=mu_0;
sigma_tilde(1)=sigma_0;

u0=zeros(1,M);
u=zeros(1,Kstep);
Mbar=zeros(1,M);
h=zeros(1,M);
Weight=ones(1,M);

%Defining first K_1
for m=1:M
    u0(m)=mu_0+sigma_0*randn;
    u(1)=u0(m);
    for k=1:Kstep-1
        dW = sqrt(dt)*randn;
        u(k+1)= u(k)+a(u(k))*dt+b(u(k))*dW; %Forward Euler
    end
    Mbar(m)=max(u);
end
sMbar=sort(Mbar);
ell=1;
K_ell(ell)=sMbar(1, ceil((1-beta)*M));
K_ell(ell)=K_ell(ell)*(K_ell(ell)<K)+K*(K_ell(ell)>=K);

h=(Mbar>=K_ell(ell));
%%
while K_ell(ell)<K
    mu_tilde(ell)=sum(h.*Weight.*u0)/sum(h.*Weight);
    sigma_tilde(ell)=sqrt(sum(h.*Weight.*(u0-mu_tilde(ell)).^2)/sum(h.*Weight));
    
    ell=ell+1;
    for m=1:M
        u0(m)=mu_tilde(ell-1)+sigma_tilde(ell-1)*randn;
        u(1)=u0(m);
        for k=1:Kstep-1
            dW = sqrt(dt)*randn;
            u(k+1)= u(k)+a(u(k))*dt+b(u(k))*dW; %Forward Euler
        end
        Mbar(m)=max(u);
    end
    sMbar=sort(Mbar);
    K_ell(ell)=sMbar(1, ceil((1-beta)*M));
    K_ell(ell)=K_ell(ell)*(K_ell(ell)<K)+K*(K_ell(ell)>=K);
    
    
    h=(Mbar>=K_ell(ell));
    Weight=(sigma_tilde(ell-1)/sigma_0)*exp(-(u0-mu_0).^2./(2*sigma_0^2)+(u0-mu_tilde(ell-1)).^2./(2*sigma_tilde(ell-1)^2));
    
end

if K_ell(ell)==K
    mu_tilde(ell)=sum(h.*Weight.*u0)/sum(h.*Weight);
    sigma_tilde(ell)=sqrt(sum(h.*Weight.*(u0-mu_tilde(ell)).^2)/sum(h.*Weight));   
end

S=10^5;
h=zeros(1,S);
u0=mu_tilde(end)+sigma_tilde(end)*randn(1,S);

for s=1:S
    P1=1;
    u(1)=u0(s);
    for k=1:Kstep
        t=(b(u(k)))^2*dt;
        dW = sqrt(dt)*randn;
        P1=P1*(1-exp(-2*(K-u(k))^2/t+2*(b(u(k))*dW+a(u(k))*dt)*(K-u(k))/t))*(K-u(k)>max(0,b(u(k))*dW+a(u(k))*dt));
        u(k+1)= u(k)+a(u(k))*dt+b(u(k))*dW; %Forward Euler
    end
    h(s)=1-P1;
end
%%
Weight=(sigma_tilde(end)/sigma_0)*exp(-(u0-mu_0).^2./(2*sigma_0^2)+(u0-mu_tilde(end)).^2./(2*sigma_tilde(end)^2));
alpha_hat=mean(h.*Weight);
Vaprx=var(h.*Weight);
RelError=1.96/sqrt(alpha_hat*S);
RelErrorIS=1.96*sqrt(Vaprx)/(alpha_hat*sqrt(S));
varRatio=(RelError/RelErrorIS)^2;


