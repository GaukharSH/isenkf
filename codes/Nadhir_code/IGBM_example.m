clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for checking the formula derived for the cdf of
%%% maximum of drifted Brownian motion approximated by Forward Euler solution.
%%% We try it on GBM model for which we have a closed form of the cdf. 
%%% So we consider the dynamics of GBM:
%------- du_t=mu u_tdt+sigma u_t dW_t -----------------------------------%
%------------------------------------------------------------------------%
lam=0.9;
mu=1;
sigma=1;
a=@(x)lam*(mu-x) ;              %the drift parameter
b=@(x) sigma*x;         %the diffusion parameter
T=1;               %simulation length 
             %the threshold
M=10^5;            %number of i.i.d copies 
%%% QoI: P(max_{0<=t<=T} u_t >K)
%%% Reference solution for GBM:
 
%u0=zeros(1,M);
%
%Pref=zeros(1,M);
%for i=1:M
 %   u0(i)= 5+sqrt(1)*randn; %initial u_0
  %  Pref(i)=1-normcdf((-(mu-sigma^2/2)*T+log(K/u0(i)))/(sigma*sqrt(T)))+...
   %(u0(i)/K)^(1-2*mu/sigma^2)*normcdf((-(mu-sigma^2/2)*T-log(K/u0(i)))/(sigma*sqrt(T)));
%end
%Pexact=mean(Pref)
%vpa(Pexact);
Krange=[15];
Paprx=zeros(1, length(Krange));
Vaprx=zeros(1, length(Krange));
RelError=zeros(1, length(Krange));
RelErrorIS=zeros(1, length(Krange));
theta=lam;
sigma1=sqrt(2*theta*mu^2*sigma^2/(2*lam-sigma^2));
for k=1:length(Krange)
    K=Krange(k);
%% maximum of Brownian Bridge
Kstep=1000;
h = T/Kstep;
mu_0=1;
sigma_0=1;
c=(1-exp(-theta*T))/(T*theta);
sigmaT2=sigma1^2*(1-exp(-2*theta*T))/(2*theta);
K_tilde=K-mu*(1-c);
sigma_tilde=sqrt(1/(1/sigma_0^2+c^2/(2*sigmaT2)))
mu_tilde=sigma_tilde^2*(mu_0/sigma_0^2+K_tilde*c/(2*sigmaT2))
%mu_tilde=2
%sigma_tilde=0.7
likelihood=ones(1,M);
u0=zeros(1,M);
P=zeros(1,M);
parfor m=1:M
    P1=1;
    u0(m)=mu_tilde+sigma_tilde*randn;
    u=u0(m);  
    for k=1:Kstep
        %b=sigma*u;
        %a=mu*u;
        t=(b(u))^2*h;
        dW = sqrt(h)*randn;
        P1=P1*(1-exp(-2*(K-u)^2/t+2*(b(u)*dW+a(u)*h)*(K-u)/t))*(K-u>max(0,b(u)*dW+a(u)*h));
        u= u+a(u)*h+b(u)*dW; %Forward Euler 
    end
    likelihood(m)=(sigma_tilde/sigma_0)*exp(-0.5*(u0(m))^2*(1/sigma_0^2-1/sigma_tilde^2))...
        *exp(u0(m)*(mu_0/sigma_0^2-mu_tilde/sigma_tilde^2)-mu_0^2/(2*sigma_0^2)+mu_tilde^2/(2*sigma_tilde^2));  
    P(m)=(1-P1)*likelihood(m);
end
Paprx(k)=mean(P)
%mean(likelihood)
Vaprx(k)=var(P)
%RelError(k)=1.96/sqrt(Paprx(k)*M);
%RelErrorIS(k)=1.96*sqrt(Vaprx(k))/(Paprx(k)*sqrt(M));
%vpa(Paprx)
end
%%
%tol=0.05;
% alpha=[0.837727183110725 0.060642593084884 5.014783842005144e-05 2.269332705695519e-09];
% alphaIS=[0.838577524435623 0.059984815423977 7.272397739973920e-05 2.269332705695519e-09];
% V=[0.134509625045141 0.056308077478754 5.000017081585402e-05  0];
% Vis=[0.189092173311094 0.018953543627808 4.936224508681833e-07 2.033046746732150e-14];
%alpha=[0.837608509876553   0.631749887628447   0.378754338920240   0.173688580135777   0.059690665020322   0.016161629087112   0.003447387617799 0.000708758643666   0.000096151464643   0.000010488134324 0.000000582384769   0.000000041472309   0.000000001464048];
%alphaIS=[0.837608441118303   0.629931568026846   0.377187910206016   0.174517108914902   0.060205024018825   0.015851718561760   0.003367161956683 0.000536139236811   0.000071143989491   0.000007601061932   0.000000582384769   0.000000041472309   0.000000001464048];
%V=[0.134600608099028   0.230424649603558   0.233086951895623   0.142028738355292   0.055485045348714   0.015699258057095   0.003386856319488 0.000692870793498   0.000092822323489   0.000010023817749 0.000000000207422   0.000000000002732   0.000000000000013];
%Vis=[0.186786517968959   0.185778014578311   0.152390941119797   0.073043269609482   0.018039641325688   0.002711846162487   0.000248230032449 0.000012532381983   0.000000451792915   0.000000011672569   0.000000000207422   0.000000000002732   0.000000000000013];
%M=((1.96/tol)^2)./alphaIS;
%Mis=((1.96/tol)^2.*Vis)./alphaIS.^2;
%semilogy(Krange, M, 'r-', Krange, Mis, 'b-', 'Linewidth', 2)
%xlabel('K')
%ylabel('Work')
%legend('Crude MC', 'with IS')


