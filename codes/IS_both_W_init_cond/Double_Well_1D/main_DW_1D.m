clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for 1D problem to performe IS technique with respect
%%% to both rho_0 and W
%%% 
%------- dX_t= a(X_t)dt+b(X_t) dW_t -----------------------------------%
%----------------------------------------------------------------------%
%%% in computing QoI: P(max_{0<=t<=T} X_t >K)

%Model parameters
sigma=0.5;
% a=@(t,x) 1-x+0*t;                             %OU drift
% aa=@(x) 1-x;                                  %OU drift
a=@(t,x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x)+0*t;   %DW drift
aa=@(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x);        %DW drift
b=@(x) sigma+0*x;                               %constant diffusion

%Simulation parameters
T=1;                                            %simulation time length
K=1                                            %the threshold
Kstep=10;                                      %numerical timestep number in SDE
dt = T/Kstep;                                   %timestep in SDE

%Original initial density parameters
mu_0  =  -1;    %mean
sigma_0 = 1;   %std
%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = -3;                    % Lower end of discretization interval
xU = K;                       % Upper end of discretization interval
dxPde = 0.005;                % Space step in PDE    
dtPde = dxPde^2;                 % Time step in PDE
Nx = round((xU-xL)/dxPde);    % Number of space steps in PDE
Nt = round(T/dtPde);          % Number of time steps in PDE
x  = linspace(xL,xU,Nx+1)';   % Space grid points

[~,~,vErik] = tabulate_IC(aa,b,dt,K,xL,xU,false,Nx+1); %KBE solution at T-dt obtained via numerical optimization (using Erik's code)
%Factor in exp.smoothing depends on EM timestep dt, based on numerical
%tests using Erik's solution, we observe the following settings
%dt=0.1, smf=15; dt=0.01, smf=40; dt=0.001, smf=100
smf=15;

%KBE solution for all time
[tW, xW, v] = KBEsolverForAllt(x,T,a,b,Nt,Nx,K,smf); %PDE solver via CN backward scheme using exponential smoothing of Ind.Fun in a final condition

%% rho_0 CONTROL
rho_x0 = @(x, m, sig) exp(-(x-m).^2/(2*sig^2))/(sqrt(2*pi*sig^2)); %1D Gaussian density
rhoTilde_x0 = [rho_x0(xW, mu_0, sigma_0).*v(:,end); rho_x0(x(igreK), mu_0, sigma_0)];
NC = trapezoidal(rhoTilde_x0, dx); %normalising constant
rhoTilde_x0=rhoTilde_x0./NC;       %optimal IS density

%fit optimal IS denisty to Gaussian
x=[xW;x(igreK)];
mFit = trapezoidal(x.*rhoTilde_x0, dx)
sigFit = sqrt(trapezoidal(x.^2.*rhoTilde_x0, dx)-mFit^2)
rhoFit_x0 = rho_x0(x, mFit, sigFit);

%Plot the densities
figure(2)
plot(x, rho_x0(x, mu_0, sigma_0), '-r',x, rhoTilde_x0, '-b',x, rhoFit_x0, '--b', 'Linewidth',3)
hold on
y = ylim; % current y-axis limits
plot([K K],[y(1) y(2)], '-k', 'LineWidth', 2)
hold off
title(['DW process'],  'fontsize',22)
xlabel('x','fontsize',18)
legend({'$\rho_{x_0} \sim N(-1,1)$';'$ \tilde{\rho}_{x_0}^{PDE}$'; ['$\tilde{\rho}_{x_0}^{fit}\sim N$(' num2str(mFit,3) ', ' num2str(sigFit,2) ')']; ['K=' num2str(K)]},'fontsize',22,'interpreter','latex')

%% dW CONTROL
dxW=xW(2)-xW(1);
%v=[2*v(1,:)-v(2,:); v];
ksiGridded = zeros(length(xW),length(tW));
ksiGridded(2:end-1,:) = sigma*(log(v(3:end,:))-log(v(1:end-2,:)))./(2*dxW);
ksiGridded(1,:) = sigma*(log(v(2,:))-log(v(1,:)))./dxW;
ksiGridded(end,:) = sigma*(log(v(end,:))-log(v(end-1,:)))./dxW;
% 
% ksiGridded = zeros(length(xW),length(tW));
% ksiGridded(2:end-1,:) = sigma*(v(3:end,:)-v(1:end-2,:))./(2*dxW*v(2:end-1,:));
% ksiGridded(1,:) = sigma*(v(2,:)-v(1,:))./(dxW*v(1,:));
% ksiGridded(end,:) = sigma*(v(end,:)-v(end-1,:))./(dxW*v(end,:));

[Xw,Tw] = ndgrid(xW,tW);
F=griddedInterpolant(Xw(1:end,1:end),flip(Tw(1:end,1:end),2), flip(ksiGridded(1:end,1:end),2),'linear', 'linear');
%F=griddedInterpolant(Xw(100:end,10:end),flip(Tw(100:end,10:end),2), flip(ksiGridded(100:end,10:end),2),'linear', 'linear');
ksi = @(x,t) F(x,t).*(x<K)+zeros(size(t)).*(x>=K); 
figure(4)
p0=surf(Xw, Tw, v);
set(p0,'LineStyle','none')
figure(5)
p1=surf(Xw(:,1:end), Tw(:,1:end), ksiGridded(:,1:end));
%p1=surf(Xw(1:end,:), Tw(1:end,:), F(Xw(1:end,:),Tw(1:end,:)));
set(p1,'LineStyle','none')
figure(6)
xF  = linspace(xL-K,K+2,Nx+1)'; 
[XwF,TwF] = ndgrid(xF,tW);
p2=surf(XwF, TwF, ksi(XwF,TwF));
set(p2,'LineStyle','none')
%%
 %[Xw,Tw] = meshgrid(xx(2:end),tW);
 %ksi =@(x,t) interp2(Xw,Tw,ksiGridded',x,t,'linear', 0);
%  xq=x(ileqK);
%  ksi = @(x) interp1(xq(2:end),ksiGridded(:,end),x,'linear', 0);
%  figure(3)
%  plot(x,ksi(x),'-b')
%%
ksi = @(x,t) 0 

%% Computing the QoI with obtained mFit and sigFit parameters from PDE approach
S=10^5;
h=zeros(1,S);
hOrig=zeros(1,S);
u=zeros(1,Kstep+1);
uOrig=zeros(1,Kstep+1);
%mFit=mu_0 %Crude MC setting to compare
%sigFit = sigma_0
u0_Orig=mu_0+sigma_0*randn(1,S);
u0=mFit+sigFit*randn(1,S);
%fprintf(['1st run'])
t=linspace(0,T,Kstep+1);
meanL=0;
parfor s=1:S
    u=u0(s);
    uOrig=u0_Orig(s);
    %u=mFit;
    %uOrig=mFit;
    L=1;
    for k=1:Kstep
        dW = sqrt(dt)*randn;
        uOrig(k+1)=uOrig(k)+aa(uOrig(k))*dt+sigma*dW;
        u(k+1)= u(k)+(aa(u(k))+sigma*ksi(u(k),t(k)))*dt+sigma*dW; %Forward Euler
        L=L*exp(-0.5*dt*(ksi(u(k),t(k)))^2-ksi(u(k),t(k))*dW);
    end
    meanL=meanL+L;
    h(s)=(max(u)>=K)*L;
    hOrig(s)=(max(uOrig)>=K);
end
meanL=meanL/S
Weight_PDE=(sigFit/sigma_0)*exp(-(u0-mu_0).^2./(2*sigma_0^2)+(u0-mFit).^2./(2*sigFit^2));
alpha_hat_PDE=mean(h.*Weight_PDE)
alpha_hat_PDE_orig=mean(hOrig)
Vaprx_PDE=var(h.*Weight_PDE)
Vaprx_MC=var(hOrig)
RelError_PDE=1.96*sqrt(1-alpha_hat_PDE)/sqrt(alpha_hat_PDE*S);
RelErrorIS_PDE=1.96*sqrt(Vaprx_PDE)/(alpha_hat_PDE*sqrt(S))
varRatio_PDE=(RelError_PDE/RelErrorIS_PDE)^2
varRatio_PDE1=Vaprx_MC/Vaprx_PDE

% %% Computing the QoI with obtained mFit and sigFit parameters from PDE approach
% S=10^5;
% 
% h=zeros(1,S);
% hOrig=zeros(1,S);
% u=zeros(1,Kstep+1);
% mFit=mu_0; %Crude MC setting to compare
% sigFit = sigma_0;
% u0=mFit+sigFit*randn(1,S);
% %fprintf(['1st run'])
% t=linspace(0,T,Kstep+1);
% meanL=0;
% parfor s=1:S
%        u=u0(s);
%        uOrig=u0(s);
% %   u=mFit;
% %   uOrig=mFit;
%    % L=1;
%    optcontTemp=zeros(1,Kstep);
%    dWTemp=zeros(1,Kstep);
%     for k=1:Kstep
%         dW = sqrt(dt)*randn;
%         %u(k+1)= u(k)+(a(t(k),u(k))+b(u(k))*ksi(u(k),t(k)))*dt+b(u(k))*dW;
%         uOrig(k+1)=uOrig(k)+aa(uOrig(k))*dt+sigma*dW;
%         optcontTemp(k)=ksi(u(k),t(k))
% %        optcontTemp(k)=interp1(xW,ksi1(:,k),u(k),'linear', 'extrap').*(u(k)<K)+0.*(u(k)>=K);
% %         exponent=-0.5*dt*(ksi(u(k),t(k)))^2-ksi(u(k),t(k))*dW
%         dWTemp(k) = dW;
%         u(k+1)= u(k)+(aa(u(k))+sigma*optcontTemp(k))*dt+sigma*dW;
%         %u(k+1)= u(k)+(aa(u(k))+sigma*ksi(u(k),t(k)))*dt+sigma*dW; %Forward Euler
%         %L=L*exp(-0.5*dt*(ksi(u(k),t(k)))^2-ksi(u(k),t(k))*dW);
%         if u(k)<-5
%          u(k)
%        pause()
%         end
%     end
%     L=exp(-0.5*dt*norm(optcontTemp)^2-dot(optcontTemp, dWTemp));
%     meanL=meanL+L;
%     h(s)=(max(u)>=K)*L;
%     hOrig(s)=(max(uOrig)>=K);
% end
% meanL=meanL/S
% %Weight_PDE=(sigFit/sigma_0)*exp(-(u0-mu_0).^2./(2*sigma_0^2)+(u0-mFit).^2./(2*sigFit^2));
% alpha_hat_PDE=mean(h)
% alpha_hat_PDE_orig=mean(hOrig)
% Vaprx_PDE=var(h)
% Vaprx_PDE_orig=var(hOrig)
% RelError_PDE=1.96*sqrt(1-alpha_hat_PDE)/sqrt(alpha_hat_PDE*S);
% RelErrorIS_PDE=1.96*sqrt(Vaprx_PDE)/(alpha_hat_PDE*sqrt(S))
% varRatio_PDE=(RelError_PDE/RelErrorIS_PDE)^2
% varRatio_PDE1=Vaprx_PDE_orig/Vaprx_PDE

% %%
% tt=[1.0 0.9 0.8 0.5 0];
% C = {'r','m', 'g', 'k', 'y'}; 
% for i=1:length(tt)
%     figure(10)
%     tfix=find(tW==tt(i));
%     loglog(xW, v(:,tfix), 'LineWidth', 2)
%     hold on
% end
% grid on
% legend('t=T', 't=0.9T', 't=0.8T', 't=0.5T', 't=0')
% xlabel('x')
% ylabel('u(t,x)')
% title('KBE solution at different time')
% %%
% ksiInterp=ksi(Xw,Tw);
% for i=1:length(tt)
%     figure(13)
%     tfix=find(tW==tt(i));
%     semilogy(xW, ksiInterp(:,tfix), 'LineWidth', 2)
%     hold on
% end
% grid on
% legend('t=T','t=0.9T', 't=0.8T', 't=0.5T', 't=0')
% xlabel('x')
% ylabel('ksi(t,x)')
% title('Optimal control at different time')
%%
% for s=1:S
% 
%     plot(t,ksi(u0(s),t))
%     hold on
% end
% smfac=3;
% indicatfun = @(x) (x>=K);
% smoothfun=@(x)0.5*(tanh(smfac*(x-K))+1);
% figure(1)
% plot(x, smoothfun(x), 'b--', x, indicatfun(x), 'r:', xW, v(:,1),'b-', 'Linewidth',2)
% legend('Smoothed Indicator', 'Indicator function', 'PDE sol at final T')