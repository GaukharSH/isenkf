%clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for 1D problem to test PDE method in
%%% performing IS technique with respect to dW
%%% 
%-----x-- dX_t= a(X_t)dt+b(X_t) dW_t -----------------------------------%
%----------------------------------------------------------------------%
%%% in computing QoI: P(max_{0<=t<=T} X_t >K)

%Model parameters
sigma=0.5;
% a=@(t,x) 1-x+0*t;                             %OU drift
% aa=@(x) 1-x;                                  %OU drift
a=@(t,x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x)+0*t;   %DW drift time dependent
aa=@(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x);        %DW drift homogeneous
b=@(x) sigma+0*x;                               %constant diffusion

%Simulation parameters
T=1;                                            %simulation time length
K=1.5                                           %the threshold
Kstep=100;                                      %numerical timestep number in SDE
dt = T/Kstep;                                   %timestep in SDE

%Original initial density parameters
mu_0  =  -1;    %mean
sigma_0 = 1;    %std

%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = -3;                      % Lower end of discretization interval
xU = K;                       % Upper end of discretization interval
dxPde = 0.005;                % Space step in PDE    
dtPde = dxPde^2;              % Time step in PDE
Nx = round((xU-xL)/dxPde);    % Number of space steps in PDE
Nt = round(T/dtPde);          % Number of time steps in PDE
x  = linspace(xL,xU,Nx+1)';   % Space grid points

[~,~,vErik] = tabulate_IC(aa,b,dt,K,xL,xU,false,Nx+1); %KBE solution at T-dt obtained via numerical optimization (using Erik's code)
%Factor in exp.smoothing depends on EM timestep dt, based on numerical
%tests using Erik's solution, we observe the following settings
%dt=0.1, smf=15; dt=0.01, smf=40; dt=0.001, smf=100
smf=40; % exp. smoothing factor for the final condition 

%% KBE solution for all time
[tW, xW, v] = KBEsolverForAllt(x,T,a,b,Nt,Nx,K,smf); %PDE solver via CN backward scheme using exponential smoothing of Ind.Fun in a final condition
%% HJB solution for all time
[tW, xW, u] = HJBsolverForAllt_CN(x,T,a,b,Nt,Nx,K,smf);
%% Comparison of solutions
% Depending on the given Euler-Maruyama timestep, we can choose appropriate
% exp.smoothing factor in the final condition which best fits Erik's
% solution at T-dt in the region closer to K

%Plot Erik's solution at T-dt and exp.smoothing of ind.fun for comparison
figure(1)
subplot(1,2,1)
plot(x(2:end),vErik(2:end)', 'k--', xW, v(:,1),'b-', 'Linewidth',3)
xlabel('x')
ylabel('u(x,T-dt_{EM})')
legend('ErikSol', ['Exp.Smooth c=', num2str(smf)])
subplot(1,2,2)
semilogy(x(2:end),vErik(2:end)', 'k--', xW, v(:,1),'b-', 'Linewidth',3)
xlabel('x')
ylabel('u(x,T-dt_{EM})')
legend('ErikSol', ['Exp.Smooth c=', num2str(smf)])

%Plot the KBE solution at time 0 and corresponding equivalent solution
%obtained via HJB at time 0 for comparison
figure(2)
semilogy(xW, v(:,end), 'r-',xW, exp(-u(:,end)), 'b-', 'Linewidth',3)
xlabel('x', 'fontsize', 14)
ylabel('v(x,0)','fontsize', 14)
legend('KBE', 'HJB')
%% Optimal control

%obtained via KBE solution on PDE grids
ksiGridded = zeros(length(xW),length(tW));
ksiGridded(2:end-1,:) = sigma*(log(v(3:end,:))-log(v(1:end-2,:)))./(2*dxPde);
ksiGridded(1,:) = sigma*(log(v(2,:))-log(v(1,:)))./dxPde;
ksiGridded(end,:) = sigma*(log(v(end,:))-log(v(end-1,:)))./dxPde;

%obtained via HJB solution on PDE grids
ksiGriddedHJB = zeros(length(xW),length(tW));
ksiGriddedHJB(2:end-1,:) = -sigma*(u(3:end,:)-u(1:end-2,:))./(2*dxPde);
ksiGriddedHJB(1,:) = -sigma*(u(2,:)-u(1,:))./dxPde;
ksiGriddedHJB(end,:) = -sigma*(u(end,:)-u(end-1,:))./dxPde;

[Xw,Tw] = ndgrid(xW,tW); %meshgrid for the interpolant
%Optimal control using KBE
cropN=0.1*Nx; %if we use KBE, I crop 10% of the left baoundary and linearly smooth it to avoid instability on the left handside; for HJB no need to do this and nCrop=1
F=griddedInterpolant(Xw(cropN:end,:),flip(Tw(cropN:end,:),2), flip(ksiGridded(cropN:end,:),2),'linear','linear');
ksi = @(x,t) F(x,t).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function for any x and t

% %Optimal control using HJB
% F=griddedInterpolant(Xw(:,:),flip(Tw(:,:),2), flip(ksiGriddedHJB(:,:),2),'linear','linear');
% ksi = @(x,t) F(x,t).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function for any x and t

% %Plot the KBE solution on surface
% figure(3)
% p0=surf(Xw, Tw, v);
% set(p0,'LineStyle','none')
% 
% %Plot the associated solution via HJB on surface
% figure(4)
% p0=surf(Xw, Tw, exp(-u));
% set(p0,'LineStyle','none')
% 
% %Plot the optimal control on the PDE meshgrid
% figure(5)
% p1=surf(Xw, Tw, ksiGridded);
% set(p1,'LineStyle','none')
% 
% %Plot the extrapolated optimal control
% figure(6)
% xF  = linspace(xL-2,K+2,Nx+1)'; 
% [XwF,TwF] = ndgrid(xF,tW);
% p2=surf(XwF, TwF, ksi(XwF,TwF));
% set(p2,'LineStyle','none')

%Plot KBE solution at different time in 2D
tt=[T-dtPde 0.9 0.8 0.5 0];
C = {'r','m', 'g', 'k', 'y'}; 
for i=1:length(tt)
    figure(7)
    tfix=find(tW==tt(i));
    %semilogy(xW, exp(-u(:,tfix)), 'LineWidth', 2)
    plot(xW, v(:,tfix), 'LineWidth', 2)
    hold on
end
grid on
legend('t=T-dt', 't=0.9T', 't=0.8T', 't=0.5T', 't=0')
xlabel('x')
ylabel('u(t,x)')
title('KBE solution at different time')

%Plot optimal control at different time in 2D
ksiInterp=ksi(Xw,Tw);
for i=1:length(tt)
    figure(8)
    tfix=find(tW==tt(i));
    plot(xW, ksiInterp(:,tfix), 'LineWidth', 2)
    hold on
end
grid on
legend('t=T-dt','t=0.9T', 't=0.8T', 't=0.5T', 't=0')
xlabel('x')
ylabel('ksi(t,x)')
hold off
title('Optimal Control at different time')

%% Computing the QoI with and without change of measure wrt W
S=10^5; %MC sample size

%Initialization
h_IS=zeros(1,S);
h_MC=zeros(1,S);
u_MC=zeros(1,Kstep+1);
U_IS=u_MC;
u0=mu_0+sigma_0*randn(1,S);
t=linspace(0,T,Kstep+1);
meanL=0;

parfor s=1:S
       u_IS=u0(s);
       u_MC=u0(s);
       L=1;
    for k=1:Kstep
        dW = sqrt(dt)*randn;
        u_MC(k+1)=u_MC(k)+aa(u_MC(k))*dt+sigma*dW;
        u_IS(k+1)= u_IS(k)+(aa(u_IS(k))+sigma*ksi(u_IS(k),t(k)))*dt+sigma*dW; %Forward Euler
        L=L*exp(-0.5*dt*(ksi(u_IS(k),t(k)))^2-ksi(u_IS(k),t(k))*dW);
    end
    meanL=meanL+L;
    h_IS(s)=(max(u_IS)>=K)*L;
    h_MC(s)=(max(u_MC)>=K);
end
meanL=meanL/S
alpha_hat_IS_W=mean(h_IS)
alpha_hat_MC=mean(h_MC)
Vaprx_IS_W=var(h_IS)
Vaprx_MC=var(h_MC)
RelError_MC=1.96*sqrt(1-alpha_hat_IS_W)/sqrt(alpha_hat_IS_W*S)
RelError_IS=1.96*sqrt(Vaprx_IS_W)/(alpha_hat_IS_W*sqrt(S))
varRatio_approx=(RelError_MC/RelError_IS)^2
varRatio_exact=Vaprx_MC/Vaprx_IS_W

