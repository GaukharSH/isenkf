clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for 1D problem to test CE method and PDE method in
%%% performing IS technique 
%%% 
%------- dX_t= a(X_t)dt+b(X_t) dW_t -----------------------------------%
%----------------------------------------------------------------------%
%%% in computing QoI: P(max_{0<=t<=T} X_t >K)
%seed=1; rng(seed);

%Problem parameters
sigma=0.5;
%a=@(x) 1-x;                                   %OU drift
aa  = @(t,x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x)+0*t;  %DW drift
a=@(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x);        %DW drift  0.25*(8*x./(1+2*x.^2).^2 - 2*x);
b=@(x) sigma+0*x;                              %DW diffusion
T=1;                                           %simulation length
K=0                                            %the threshold
M=10^5                                         %number of i.i.d copies
Kstep=100;                                     %Numerical timestep number in SDE
dt = T/Kstep;                                  %Timestep in SDE

%Original density parameters
mu_0  = -1;     %mean
sigma_0 = 0.2;  %std

%% MULTILEVEL CROSS-ENTROPY METHOD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We consider only mean shifting but fixing the covariance
beta=0.01;                              %CE method parameter
mu_tilde = mu_0;
sigma_tilde = sigma_0;

% Multilevel procedure to find the optimal mu_tilde
u=zeros(1,Kstep);
Mbar=zeros(1,M);
Weight=ones(1,M);

%Defining first K_1
u0=mu_tilde+sigma_tilde*randn(1,M);
for m=1:M
    u(1)=u0(m);
    for k=1:Kstep
        dW = sqrt(dt)*randn;
        u(k+1)= u(k)+a(u(k))*dt+b(u(k))*dW; %Forward Euler
    end
    Mbar(m)=max(u);
end

%% Plotting the trajectory with potential
close all
V = @(x) 0.25*(2./(1+2*x.^2) + x.^2); 
u0=-0.7;
ObsSteps=100;
N=100;
[dwTraj,yobs]=ObsAndParticles(u0(1),ObsSteps,sigma,1, 1, 0.1, a);%(u0,ObsSteps, sigma,tau, H, Gamma, a_pi)
tt=linspace(0,ObsSteps,ObsSteps*100);
plot(30*V(-2:0.1:2)-10.5,[-2:0.1:2], 'k:','LineWidth', 3)
hold on
for i=1:ObsSteps
    plot(tt((i-1)*100+1:1:i*100), dwTraj{i},'-b','LineWidth', 1.5)
    hold on
    plot(tt(i*100), yobs(i+1), '*r', 'LineWidth', 2)
    hold on
end
hold off
xlabel('t')
legend('DW potential','SDE trajectory', 'Observations', 'LineWidth', 1.5, 'FontSize', 20)
set(gca,'FontSize',20)
%%
sMbar=sort(Mbar);
K_ell=sMbar(1, ceil((1-beta)*M));
K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
h=Mbar>=K_ell;
ell=1;
while K_ell<K
    mu_tilde=sum(h.*Weight.*u0)/sum(h.*Weight);
    %sigma_tilde=sqrt(sum(h.*Weight.*(u0-mu_tilde).^2)/sum(h.*Weight));
    
    ell=ell+1;
    u0=mu_tilde+sigma_tilde*randn(1,M);
    for m=1:M
        u(1)=u0(m);
        for k=1:Kstep
            dW = sqrt(dt)*randn;
            u(k+1)= u(k)+a(u(k))*dt+b(u(k))*dW; %Forward Euler
        end
        Mbar(m)=max(u);
    end
    sMbar=sort(Mbar);
    K_ell=sMbar(1, ceil((1-beta)*M));
    K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
    h=Mbar>=K_ell;
    Weight=(sigma_tilde/sigma_0)*exp(-(u0-mu_0).^2./(2*sigma_0^2)+(u0-mu_tilde).^2./(2*sigma_tilde^2));
end

if K_ell==K
    mu_tilde=sum(h.*Weight.*u0)/sum(h.*Weight);
    %sigma_tilde=sqrt(sum(h.*Weight.*(u0-mu_tilde).^2)/sum(h.*Weight));
end

%% CE: Computing the QoI with obtained mu_tilde parameter and fixed sigma_tilde
S=10^6;
h=zeros(1,S);
%mu_tilde=mu_0; %Crude MC setting to compare
u0=mu_tilde+sigma_tilde*randn(1,S);
fprintf(['CE results:'])
mu_tilde
sigma_tilde
for s=1:S
    u(1)=u0(s);
    for k=1:Kstep
        dW = sqrt(dt)*randn;
        u(k+1)= u(k)+a(u(k))*dt+b(u(k))*dW; %Forward Euler
    end
    h(s)=(max(u)>K);
end
%Continuous Forward Euler approach
% parfor s=1:S
%     P1=1;
%     u=u0(s);
%     for k=1:Kstep
%         t=(b(u))^2*dt;
%         dW = sqrt(dt)*randn;
%         P1=P1*(1-exp(-2*(K-u)^2/t+2*(b(u)*dW+a(u)*dt)*(K-u)/t))*(K-u>max(0,b(u)*dW+a(u)*dt));
%         u= u+a(u)*dt+b(u)*dW; %Forward Euler
%     end
%     h(s)=1-P1;
% end
Weight_CE=(sigma_tilde/sigma_0)*exp(-(u0-mu_0).^2./(2*sigma_0^2)+(u0-mu_tilde).^2./(2*sigma_tilde^2));
alpha_hat_CE=mean(h.*Weight_CE)
Vaprx_CE=var(h.*Weight_CE)
RelError_CE=1.96/sqrt(alpha_hat_CE*S);
RelErrorIS_CE=1.96*sqrt(Vaprx_CE)/(alpha_hat_CE*sqrt(S))
varRatio_CE=(RelError_CE/RelErrorIS_CE)^2


%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = -3;         % Lower end of discretization interval
xU = 7;         % Upper end of discretization interval
Nt = 4*1e4;    % Number of time steps 
Nx = 2e3;        % Number of discretization intervals in space
dx = (xU-xL)/Nx; % spatial step size
x  = linspace(xL,xU,Nx+1)'; 

%PDE Solution
ileqK=find(x<=K); 
igreK=find(x>K);
NxleqK=length(ileqK)-1;
[xx, u] = BKEsolver(x(ileqK),T,a,b,Nt,NxleqK); 

%Plot the PDE solution at time 0
figure(1)
plot(xx, u, 'Linewidth',3)
xlabel('x', 'fontsize', 14)
ylabel('u(x,0)','fontsize', 14)

rho_x0 = @(x, m, sig) exp(-(x-m).^2/(2*sig^2))/(sqrt(2*pi*sig^2)); %1D Gaussian density
rhoTilde_x0 = [rho_x0(xx, mu_0, sigma_0).*sqrt(u); rho_x0(x(igreK), mu_0, sigma_0)];
NC = trapezoidal(rhoTilde_x0, dx); %normalising constant
rhoTilde_x0=rhoTilde_x0./NC;       %optimal IS density

%fit optimal IS denisty to Gaussian
x=[xx;x(igreK)];
mFit = trapezoidal(x.*rhoTilde_x0, dx);
sigFit = sqrt(trapezoidal(x.^2.*rhoTilde_x0, dx)-mFit^2);
rhoFit_x0 = rho_x0(x, mFit, sigFit);

rhoCE_x0 = rho_x0(x, mu_tilde, sigma_tilde);

%% Plot the densities 1 with pde sol
V = @(x) 0.25*(2./(1+2*x.^2) + x.^2); 
figure(2)
plot(x, rho_x0(x, mu_0, sigma_0), '-r',x, rhoTilde_x0, '-.b',x, rhoFit_x0, '--b', x, rhoCE_x0, '--g', 'Linewidth',3)
hold on
plot([-4:0.01:4], V([-4:0.01:4])-0.37,':', 'color', [.5 .5 .5],'LineWidth', 3)
hold on
y = ylim; % current y-axis limits
plot([K K],[y(1) y(2)], '-k', 'LineWidth', 3)
hold off
ylim([0 1])
xlim([-4.5 5.5])
%title(['DW process'],  'fontsize',22)
xlabel('x','fontsize',18)
legend({'Original: $\rho_{u_0} \sim N(-1,0.2)$';'PDE-based: $\tilde{\rho}_{u_0}$'; ['Gaussian fit: $\tilde{\rho}_{u_0}\sim N$(' num2str(mFit,2) ', ' num2str(sigFit,1) ')'];['CE: $\tilde{\rho}_{u_0}\sim N$(' num2str(mu_tilde,1) ', ' num2str(sigma_tilde,2) ')']; ['$\mathcal{K}$=' num2str(K)]},'fontsize',20,'interpreter','latex')
%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = -5;                         % Lower end of discretization interval
xU = K;                          % Upper end of discretization interval
dxPde = 0.005;                   % Space step in PDE    
dtPde = dxPde^2;                 % Time step in PDE
Nx = round((xU-xL)/dxPde);       % Number of space steps in PDE
Nt = round(T/dtPde);             % Number of time steps in PDE
x  = linspace(xL,xU,Nx+1)';      % Space grid points

%[~,~,vErik] = tabulate_IC(aa,b,dt,K,xL,xU,false,Nx+1); %KBE solution at T-dt obtained via numerical optimization (using Erik's code)
%Factor in exp.smoothing depends on EM timestep dt, based on numerical
%tests using Erik's solution, we observe the following settings
%dt=0.1, smf=15; dt=0.01, smf=40; dt=0.001, smf=100
smf=40;

%KBE solution for all time
[tW, xW, v] = KBEsolverForAllt(x,T,aa,b,Nt,Nx,K,smf); %PDE solver via CN backward scheme using exponential smoothing of Ind.Fun in a final condition


%% rho_0 CONTROL
rho_x0 = @(x, m, sig) exp(-(x-m).^2/(2*sig^2))/(sqrt(2*pi*sig^2)); %1D Gaussian density
rhoTilde_x0 = [rho_x0(xW, mu_0, sigma_0).*v(:,end); rho_x0((K:dxPde:K+500*dxPde)', mu_0, sigma_0)];
NC = trapezoidal(rhoTilde_x0, dxPde); %normalising constant
rhoTilde_x0=rhoTilde_x0./NC;          %optimal IS density

%fit optimal IS denisty to Gaussian
x=[xW;(K:dxPde:K+500*dxPde)'];
mFit = trapezoidal(x.*rhoTilde_x0, dxPde)
sigFit = sqrt(trapezoidal(x.^2.*rhoTilde_x0, dxPde)-mFit^2)
rhoFit_x0 = rho_x0(x, mFit, sigFit);
rhoCE_x0 = rho_x0(x, mu_tilde, sigma_tilde);

%% Plot the densities 2
V = @(x) 0.25*(2./(1+2*x.^2) + x.^2); 
figure(3)
plot(x, rho_x0(x, mu_0, sigma_0), '-r', x, rhoFit_x0, '-.b', x, rhoCE_x0, '--g', 'Linewidth',5)
hold on
plot([-4:0.01:4], V([-4:0.01:4])-0.37,':', 'color', [.5 .5 .5],'LineWidth', 5)
hold on
y = ylim; % current y-axis limits
plot([K K],[y(1) y(2)], '-k', 'LineWidth', 4)
hold off
ylim([0 2.1])
xlim([-4.5 5.5])
aa = get(gca,'XTickLabel');
set(gca,'XTickLabel',aa,'FontName','Times','fontsize',20)
title(['$\mathbf{\sigma_0=}$',num2str(sigma_0,2)], 'FontName','Times', 'fontsize',40,'interpreter','latex')
xlabel('x','fontsize',20)
%legend({'$\rho_{u_0}$'; ['$\tilde{\rho}_{u_0}^{PDE}$'];['$\tilde{\rho}_{u_0}^{CE}$']; ['DW potential']; ['$\mathcal{K}$=' num2str(K)]},'fontsize',30,'FontName','Times','interpreter','latex')
[~, hobj, ~, ~] =legend({'$\rho_{u_0} \sim N(-1,0.2)$'; ['$\tilde{\rho}_{u_0}^{PDE}\sim N$(' num2str(mFit,1) ', ' num2str(sigFit,3) ')'];['$\tilde{\rho}_{u_0}^{CE}\sim N$(' num2str(mu_tilde,1) ', ' num2str(sigma_tilde,2) ')']; ['DW potential']; ['$\mathcal{K}$=' num2str(K)]},'fontsize',35,'FontName','Times','interpreter','latex');
hl = findobj(hobj,'type','line');
set(hl,'LineWidth',2.5);
set(gca,'FontSize',40)
%% PDE: Computing the QoI with obtained mFit and sigFit parameters from PDE approach
S=10^6;
h=zeros(1,S);
%  mFit=mu_0; %Crude MC setting to compare
%  sigFit = sigma_0;
u=zeros(1,Kstep+1);
u0=mFit+sigFit*randn(1,S);
fprintf(['PDE results:'])
mFit
sigFit
for s=1:S
    u(1)=u0(s);
    for k=1:Kstep
        dW = sqrt(dt)*randn;
        u(k+1)= u(k)+a(u(k))*dt+b(u(k))*dW; %Forward Euler
    end
    h(s)=(max(u)>K);
end
Weight_PDE=(sigFit/sigma_0)*exp(-(u0-mu_0).^2./(2*sigma_0^2)+(u0-mFit).^2./(2*sigFit^2));
alpha_hat_PDE=mean(h.*Weight_PDE)
Vaprx_PDE=var(h.*Weight_PDE)
RelError_PDE=1.96/sqrt(alpha_hat_PDE*S);
RelErrorIS_PDE=1.96*sqrt(Vaprx_PDE)/(alpha_hat_PDE*sqrt(S))
varRatio_PDE=(RelError_PDE/RelErrorIS_PDE)^2
