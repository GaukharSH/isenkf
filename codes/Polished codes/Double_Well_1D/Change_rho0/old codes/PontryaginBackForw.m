%Ponttryagin for DW
b=@(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x); %DW drift  
dxb=@(x) (2-16*x.^2)./(1+2*x.^2).^2-1/2; 
smf=40;
sigma=0.5;
T=1;
N=100;
dt=T/N;
X=zeros(1,N+1);
X(1)=0.1;
lambda=zeros(1,N+1);
lambda(N+1)=smf;
tol=1;
while tol>1e-6
    Xold=X;
    %forward iteration
    for n=1:N
        X(n+1)=X(n)+dt*(-b(X(n))+sigma^2*lambda(n+1));
    end
    
    %backward iteration
    for n=1:N
        k=N+2-n;
        lambda(k-1)=lambda(k)+dt*(dxb(X(k-1))*lambda(k));
    end
    tol=norm(X-Xold);
end
control=sigma*lambda';
plot(0:dt:1, control, 'LineWidth', 4)