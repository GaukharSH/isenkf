%clc; clear, close all
%Ponttryagin for L63
d=3;            %problem dimension
sigma=10;      %constant diffusion parameter
r=10;
q=8/3;
s=28;

a = @(u) [r*(u(2,:,:)-u(1,:,:)); s*u(1,:,:)-u(2,:,:)-u(1,:,:).*u(3,:,:); u(1,:,:).*u(2,:,:)-q*u(3,:,:)]; %Lorenz 63 drift
b = [sigma;sigma;sigma]; 
I=eye(d);
K=27;
%K= 19.278;
smf=5;
T=1;
N=1000;
dt=T/N;
%mu_0=[0 0 1]';
sigma_0=0.0025*I;
P1=I(1,:);

M = 10^0;
t=linspace(0,T,N+1);
X0range=[5 5 25]';
%X0range=[-2.5, -2,-1.5, -1, -0.5, 0, 0.5, 1, 1.5, 2, 2.4, 2.5];
%X0range=linspace(-3,2.5,100);
%X0range=[5 5 25; 6 5 25; 7 5 25; 8 5 25; 9 5 25; 10 5 25; 11 5 25]';
%X0range=[11.65 12.8 30.2; 11.7 12.8 30.2; 11.75 12.8 30.2; 11.8 12.8 30.2; 11.85 12.8 30.2; 11.9 12.8 30.2; 11.95 12.8 30.2]';
%X0range=[5 6 25; 6 7 26; 7 8 27; 8 9 28; 9 10 28.5; 10 11 39; 11 12 30]';
%X0range=[11.65 12.8 30.2; 11.7 12.8 30.2; 11.75 12.8 30.2; 11.8 12.8 30.2; 11.85 12.8 30.2; 11.9 12.8 30.2; 11.95 12.8 30.2]';

delta=0.01; %damping factor
ksi0=zeros(1,length(X0range));
ksi =zeros(N+1,length(X0range));
for j=1:size(X0range,2)
    tol=1;
   X=zeros(3,N+1);
   X(:,1)=X0range(:,j);
   lambda=zeros(3,N+1);
   lambda(:,N+1)=-smf;
   
for n=1:N
    X(:,n+1)=X(:,n)+dt*(a(X(:,n))-1/2*sigma^2*lambda(:,n+1));
end
  tau_K_indx=find(P1*X>=K,1,'first');
  if isempty(tau_K_indx)==1
      NtauOld=N+1;
      lambdaOld=zeros(3,NtauOld);
      lambdaOld(1,NtauOld)=-smf;
  elseif (t(tau_K_indx)==T && P1*X(:,tau_K_indx)<K)
      NtauOld=tau_K_indx;
      lambdaOld=zeros(3,NtauOld);
      lambdaOld(1,NtauOld)=-smf;
  elseif (t(tau_K_indx)<T && P1*X(:,tau_K_indx)>=K)||(t(tau_K_indx)==T && P1*X(:,tau_K_indx)>=K)
      NtauOld=tau_K_indx;
      lambdaOld=zeros(3,NtauOld);
      %lambdaOld(NtauOld)=0;
      lambdaOld(1,NtauOld)=-smf;
  end

while tol>1e-4
    Xold=X;
    %forward iteration
    for n=1:NtauOld-1
        X(:,n+1)=X(:,n)+dt*(a(X(:,n))-1/2*sigma^2*lambdaOld(:,n+1));
    end

      tau_K_indx=find(P1*X>=K,1,'first');
      if isempty(tau_K_indx)==1
          Ntau=N+1;
          lambda=zeros(3,Ntau);
          lambda(1,Ntau)=-smf;
      elseif (t(tau_K_indx)==T && P1*X(:,tau_K_indx)<K)
          Ntau=tau_K_indx;
          lambda=zeros(3,Ntau);
          lambda(1,Ntau)=-smf;
      elseif (t(tau_K_indx)<T && P1*X(:,tau_K_indx)>=K)||(t(tau_K_indx)==T && P1*X(:,tau_K_indx)>=K)
          Ntau=tau_K_indx;
          lambda=zeros(3,Ntau);
          %lambda(:,Ntau)=0;
          lambda(1,Ntau)=-smf;
      end
  
%     %backward iteration
%     for n=1:Ntau-1
%         k=Ntau+1-n;
%         lambda(1,k-1)=lambda(k)+dt*(r*lambda(k));
%         lambda(1,k-1)=lambda(k)+dt*(lambda(k));
%         lambda(1,k-1)=lambda(k)+dt*(q*lambda(k));
%     end
     Ntaumin=min(Ntau,NtauOld)
    
    lambda=zeros(3,Ntau);
    lambda(1,Ntau)=-smf;
    lambda(1,1:Ntau-1)=-smf*exp(-10*(t(Ntau)-t(1:Ntau-1)));
%     lambda(2,1:Ntau-1)=-smf*exp(-(t(Ntau)-t(1:Ntau-1)));
%     lambda(3,1:Ntau-1)=-smf*exp(-8/3*(t(Ntau)-t(1:Ntau-1)));
    lambda(2,1:Ntau-1)=0;
    lambda(3,1:Ntau-1)=0;
%     %Update with damping factor
%     for n=1:NtauOld-1
%     X(:,n+1)=X(:,n+1)-delta*dt*(a(X(:,n))-sigma^2*lambdaOld(:,n+1)/2);
%     end
%     

    tol=norm(X(1,1:Ntaumin)-Xold(1,1:Ntaumin))
    lambdaOld=lambda;
    NtauOld=Ntau;
%     figure
%     plot(t(1:Ntaumin), X(1,1:Ntaumin),'r',t(1:Ntaumin), Xold(1,1:Ntaumin), 'b')
%     figure
%     plot(t(1:Ntaumin), lambda(1,1:Ntaumin)', 'Linewidth', 2)
    %pause()
end

%         figure(1) 
%     plot(t(1:Ntaumin), X(1:Ntaumin))
%     figure(2)
%     plot(t(1:Ntaumin), lambda(1:Ntaumin)')
% pause()
control=zeros(3,N+1);
control(:,1:Ntau)=-sigma*lambda/2;
% figure
% plot(0:dt:T, control, 'LineWidth', 4)
% pause()
figure
plot(t, control(1,:), 'LineWidth', 2)
figure
plot(t(1:Ntaumin), X(1,1:Ntaumin),'b','LineWidth', 3)
%pause()
ksi(:,j)=control(1,:);
taurng(j)=t(Ntaumin);
valuefun(j) = trapezoidal(sigma^2*lambda(1,:).^2/4, dt)-smf*(P1*X(:,Ntau)-K)*(t(Ntau)<T)-smf*(P1*X(:,N+1)-K)*(t(Ntau)==T);
end
plot(X0range(1,:), valuefun, 'g-', 'Linewidth', 2)
title('Value function at t=0 with \sigma=1')
xlabel('x','fontsize', 18)
ylabel('v_1(x,0)','fontsize', 18)
legend('Pontryagin')
%% Using control based on time only
ksiStar = @(t) interp1(0:dt:T, 2*control(1,:),t);

M=10^4; %MC sample size

%Initialization
h_IS=zeros(1,M);
h_MC=zeros(1,M);
u_MC=zeros(d,N+1);
u_IS=u_MC;
%u0=X0v;
%u0=mu_0*ones(1,M);%+chol(Sigma)'*randn(d,S);
u0=mu_0+chol(sigma_0)'*randn(d,M);
%u0_shifted=[vSmpl; mCond+chol(SigmaCond)'*randn(d-1,M)];
u0_shifted=u0;
t=linspace(0,T,N+1);
meanL=0;
parfor m=1:M
       u_MC=u0(:,m);
       u_IS=u0_shifted(:,m);
%        u_MC=mu_0;
%        u_IS=mu_0;
      L=1;
      k=1;
    while k<=N && P1*u_IS(:,k)<K
  
    dW = sqrt(dt).*randn(d,1);
    %Lorenz63 
    %u_IS(:,k+1)= u_IS(:,k)+rk4(u_IS(:,k),dt,r,q,s)+P1'.*b.*ksi(P1*u_IS(:,k),t(k))*dt+b.*dW; %Runge-Kutta
    %u_IS(:,k+1)= u_IS(:,k)+(lorenz63(u_IS(:,k),r,q,s)+P1'.*b.*ksi(P1*u_IS(:,k),t(k)))*dt+b.*dW; %Forward Euler 
    u_IS(:,k+1)= u_IS(:,k)+(lorenz63(u_IS(:,k),r,q,s)+P1'.*b.*ksiStar(t(k)))*dt+b.*dW; %Forward Euler 

    L=L*exp(-0.5*dt*ksiStar(t(k))^2-ksiStar(t(k))*P1*dW);
    
    k=k+1;
    end

    %Monte Carlo simulation
    for n=1:N
         dW = sqrt(dt).*randn(d,1);
         u_MC(:,n+1)= u_MC(:,n)+lorenz63(u_MC(:,n),r,q,s)*dt+b.*dW;
    end

    meanL=meanL+L;
    h_IS(m)=(max(P1*u_IS)>=K)*L;
    h_MC(m)=(max(P1*u_MC)>=K);
end
meanL=meanL/M
%Weight = rho_x0(P1*u0_shifted,P1*mu,sqrt(P1*Sigma*P1'))./rho_x0(P1*u0_shifted, mFit, sigFit);
%Weight = rho_x0(P1*u0_shifted,P1*mu,sqrt(P1*Sigma*P1'))./rho_x0(P1*u0_shifted, mFit, sqrt(P1*Sigma*P1'));
Weight = ones(1,M);
alpha_hat_IS_W=mean(h_IS.*Weight)
%alpha_hat_IS_W1=mean(h_IS1.*exp(expDist-mean(expDist)))*exp(mean(expDist))
alpha_hat_MC=mean(h_MC)
Vaprx_IS_W=var(h_IS.*Weight)
%Vaprx_IS_W1=var(h_IS1.*exp(expDist-mean(expDist)))*exp(mean(expDist))^2
Vaprx_MC=var(h_MC)
RelError_MC=1.96*sqrt(1-alpha_hat_IS_W)/sqrt(alpha_hat_IS_W*M)
RelError_IS=1.96*sqrt(Vaprx_IS_W)/(alpha_hat_IS_W*sqrt(M))
varRatio_approx=(RelError_MC/RelError_IS)^2
varRatio_exact=Vaprx_MC/Vaprx_IS_W

function I = trapezoidal(fx,dx) % For fx column vecor or matrix
I = (sum(fx)-0.5*(fx(1)+fx(end)))*dx;
end