% Solving Hamiltonian system with Newton’s method
% for T=1, delta=dt and gamma=1
N=100; dt=1/N;
J=sparse(2*N,2*N); rhs=sparse(2*N,1);
X=sparse(N+1,1); L=sparse(N+1,1);
X(1)= 0.01; % initial data
L(N+1)=40;
tol=1;
sigma=0.5;
b=@(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x); %DW drift  
dxb=@(x) (2-16*x.^2)./(1+2*x.^2).^2-1/2; 

while tol>1e-6
% Assemble Newton system row-wise
for n=1:N
rhs(2*n-1)=L(n)-L(n+1)-dt*(dxb(X(n)).*L(n+1));
rhs(2*n)=X(n+1)-X(n)+dt*(-b(X(n))+sigma^2*L(n+1));
end

J(1,1:2)=[1 -1]; J(2*N,2*N-1:2*N)=[-1 1];
for n=1:N-1
J(2*n,2*n-1:2*n+1)=[-1 sigma^2 1];
J(2*n+1,2*n:2*n+2)=[1 -dt -1];
end
J(2,1)=0; J(2*N-1,2*N)=0;
% Solve and update
dXL=J\rhs;
L(1)=L(1)-dXL(1); X(N+1)=X(N+1)-dXL(2*N);
for n=2:N
X(n)=X(n)-dXL(2*n-1); L(n)=L(n)-dXL(2*n-2);
end
tol = norm(rhs) % Error
end