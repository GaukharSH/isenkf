clc; clear, close all
%Ponttryagin for DW
b  = @(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x); %DW drift  
dxb= @(x) (-2+16*x.^2)./(1+2*x.^2).^2+1/2; 
smf= 60;
sigma=1;
T = 1;
N = 100;
dt= T/N;
M = 10^0;
t=linspace(0,T,N+1);
K=2.5;
%X0range=[-2.5, -2,-1.5, -1, -0.5, 0, 0.5, 1,1.5, 2, 2.4 2.5];
%X0range=linspace(-3,2.5,100);
X0range=[0];
delta=0.001; %damping factor
ksi0=zeros(1,length(X0range));
for j=1:length(X0range)
X0v=X0range(j); %mu_0+sigma_0*randn(1,M)
ksi=zeros(N+1,M);
for i=1:M
    tol=1;
   X=zeros(1,N+1);
   X(1)=X0v(i);
   lambda=zeros(1,N+1);
   lambda(1:N+1)=-smf; %Initial guess for adjoint variable lambda
for n=1:N
    X(n+1)=X(n)+dt*(b(X(n))-sigma^2*lambda(n+1)/2);
end
  tau_K_indx=find(X>=K,1,'first');
  if isempty(tau_K_indx)==1
      NtauOld=N+1;
      lambdaOld=zeros(1,NtauOld);
      lambdaOld(NtauOld)=-smf;
  elseif (t(tau_K_indx)==T && X(tau_K_indx)<K)
      NtauOld=tau_K_indx;
      lambdaOld=zeros(1,NtauOld);
      lambdaOld(NtauOld)=-smf;
  elseif (t(tau_K_indx)<T && X(tau_K_indx)>=K)||(t(tau_K_indx)==T && X(tau_K_indx)>=K)
      NtauOld=tau_K_indx;
      lambdaOld=zeros(1,NtauOld);
      %lambdaOld(NtauOld)=-smf;
      lambdaOld(NtauOld)=-smf+4*b(X(tau_K_indx))/sigma^2;
  end

while tol>1e-4
    Xold=X;
    %forward iteration
    for n=1:NtauOld-1
        X(n+1)=X(n)+dt*(b(X(n))-sigma^2*lambdaOld(n+1)/2);
    end

      tau_K_indx=find(X>=K,1,'first');
      if isempty(tau_K_indx)==1
          Ntau=N+1;
          lambda=zeros(1,Ntau);
          lambda(Ntau)=-smf;
      elseif (t(tau_K_indx)==T && X(tau_K_indx)<K)
          Ntau=tau_K_indx;
          lambda=zeros(1,Ntau);
          lambda(Ntau)=-smf;
      elseif (t(tau_K_indx)<T && X(tau_K_indx)>=K)||(t(tau_K_indx)==T && X(tau_K_indx)>=K)
          Ntau=tau_K_indx;
          lambda=zeros(1,Ntau);
          %lambda(Ntau)=-smf;
          lambda(Ntau)=-smf+4*b(X(tau_K_indx))/sigma^2;
      end
  
    %backward iteration
    for n=1:Ntau-1
        k=Ntau+1-n;
        lambda(k-1)=lambda(k)+dt*(-dxb(X(k-1))*lambda(k));
    end
    Ntaumin=min(Ntau,NtauOld);
    
%     %Update with damping factor
%     for n=1:NtauOld-1
%     X(n+1)=X(n+1)-delta*dt*(b(X(n))-sigma^2*lambdaOld(n+1)/4);
%     end
%     
%     for n=1:Ntau-1
%         k=Ntau+1-n;
%         lambda(k-1)=lambda(k-1)-delta*dt*(dxb(X(k-1))*lambda(k));
%     end 
        figure(1) 
    plot(t(1:Ntaumin), X(1:Ntaumin))
    figure(2)
    plot(t(1:Ntaumin), lambda(1:Ntaumin)')
pause()

    tol=norm(X(1:Ntaumin)-Xold(1:Ntaumin))
pause()
    lambdaOld=lambda;
    NtauOld=Ntau;
end

%         figure(1) 
%     plot(t(1:Ntaumin), X(1:Ntaumin))
%     figure(2)
%     plot(t(1:Ntaumin), lambda(1:Ntaumin)')
% pause()
control=zeros(1,N+1);
control(1:Ntau)=-sigma*lambda/2;
% figure
% plot(0:dt:T, control, 'LineWidth', 4)
% pause()
ksi(:,i)=control;
end
ksi0(j)=control(1);
figure
plot(0:dt:T, control', 'LineWidth', 4)
figure
plot(0:dt:t(Ntaumin), X(1:Ntaumin), 'b-', 'LineWidth', 4)
taurng(j)=t(Ntaumin);
pause()
valuefun(j) = trapezoidal(sigma^2*lambda.^2/4, dt)-smf*(X(Ntau)-K)*(t(Ntau)<T)-smf*(X(N+1)-K)*(t(Ntau)==1);
end
%%
%Model parameters
% a=@(t,x) 1-x+0*t;                               %OU drift
% aa=@(x) 1-x;                                    %OU drift
a  = @(t,x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x)+0*t;  %DW drift
aa = @(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x);        %DW drift
b  = @(x) sigma+0*x;                              %constant diffusion

ksiStar = @(t) interp1(0:dt:t(Ntaumin), control(1:Ntaumin),t, 'nearest', 'nearest');
%% Computing the QoI with and without change of measure wrt W and rho_0
S=10^5; %MC sample size
%Initialization
h_IS=zeros(1,S);
h_MC=zeros(1,S);
Ldist=zeros(1,S);
u_MC=zeros(1,Ntau+1);
u_IS=u_MC;
u0=mu_0*ones(1,S);%+sigma_0*randn(1,S);
%u0_shifted=mFit+sigFit*randn(1,S);
%t=linspace(0,T,Kstep+1);
meanL=0;
dtMC=0.01;

parfor s=1:S
       u_MC=u0(s);
       %u_IS=u0_shifted(s);
       u_IS=u0(s);
       L=1;
       
       %We integrate until the state hits the threshold or until final time
    for k=1:N
        
        %Adaptive timestep
        %dt=min(0.5, 1/ksi(ksiStar(u_IS(k),t_n)))
        %if ksiStar(u_IS(k),t_n)==0
        %     dt=0.01;
        % else
        %     dt=1/ksiStar(u_IS(k),t_n);
        % end
        
        dW = sqrt(dt)*randn;

        L=L*exp(-0.5*dt*(ksiStar(t(k)))^2-ksiStar(t(k))*dW);
        u_IS(k+1)= u_IS(k)+(aa(u_IS(k))+sigma*ksiStar(t(k)))*dt+sigma*dW; %Forward Euler 
    end
   % MC counterpart simulation    
        for n=1:Ntau
            dW=sqrt(dtMC)*randn;
            u_MC(n+1)=u_MC(n)+aa(u_MC(n))*dtMC+sigma*dW;
        end
    Ldist(s)=L;
    meanL=meanL+L;
    h_IS(s)=(max(u_IS)>=K)*L;
    h_MC(s)=(max(u_MC)>=K);
    
end
%semilogy(Ldist)
meanL=meanL/S
%Weight_PDE=(sigFit/sigma_0)*exp(-(u0_shifted-mu_0).^2./(2*sigma_0^2)+(u0_shifted-mFit).^2./(2*sigFit^2));
%alpha_hat_IS_W=mean(h_IS.*Weight_PDE)
alpha_hat_IS_W=mean(h_IS)
alpha_hat_MC=mean(h_MC)
%Vaprx_IS_W=var(h_IS.*Weight_PDE)
Vaprx_IS_W=var(h_IS)
Vaprx_MC=var(h_MC)
RelError_MC=1.96*sqrt(1-alpha_hat_IS_W)/sqrt(alpha_hat_IS_W*S)
RelError_IS=1.96*sqrt(Vaprx_IS_W)/(alpha_hat_IS_W*sqrt(S))
varRatio_approx=(RelError_MC/RelError_IS)^2
varRatio_exact=Vaprx_MC/Vaprx_IS_W
ciMCleft=alpha_hat_MC-1.96*sqrt(Vaprx_MC./S)
ciMCright=alpha_hat_MC+1.96*sqrt(Vaprx_MC./S)
ciISleft=alpha_hat_IS_W-1.96*sqrt(Vaprx_IS_W./S)
ciISright=alpha_hat_IS_W+1.96*sqrt(Vaprx_IS_W./S)

function I = trapezoidal(fx,dx) % For fx column vecor or matrix
I = (sum(fx)-0.5*(fx(1)+fx(end)))*dx;
end
