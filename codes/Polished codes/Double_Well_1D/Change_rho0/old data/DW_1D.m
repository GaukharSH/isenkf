clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for 1D problem to test CE method and PDE method in
%%% performing IS technique 
%%% 
%------- dX_t= a(X_t)dt+b(X_t) dW_t -----------------------------------%
%----------------------------------------------------------------------%
%%% QoI: P(max_{0<=t<=T} X_t >K)

%Problem parameters
sigma=1;
a=@(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x); %DW drift
b=@(x) sigma;                           %DW diffusion
T=1;                                    %simulation length
K=4;                                    %the threshold
M=10^5;                                 %number of i.i.d copies
Kstep=1000;                             %Numerical timestep number in SDE
dt = T/Kstep;                           %Timestep in SDE

%% MULTILEVEL CROSS-ENTROPY METHOD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We consider only mean shifting but fixing the covariance
beta=0.01;                              %CE method parameter

%Initial density parameters in CE method
mu_0  = -1;
sigma_0 = 1;
mu_tilde = -1;
sigma_tilde = 1;

u=zeros(1,Kstep);
Mbar=zeros(1,M);
Weight=ones(1,M);

%Defining first K_1
u0=mu_tilde+sigma_tilde*randn(1,M);
for m=1:M
    u(1)=u0(m);
    for k=1:Kstep
        dW = sqrt(dt)*randn;
        u(k+1)= u(k)+a(u(k))*dt+b(u(k))*dW; %Forward Euler
    end
    Mbar(m)=max(u);
end

% Multilevel procedure to find the optimal mu_tilde
sMbar=sort(Mbar);
K_ell=sMbar(1, ceil((1-beta)*M));
K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
h=Mbar>=K_ell;
ell=1;
while K_ell<K
    mu_tilde=sum(h.*Weight.*u0)/sum(h.*Weight);
    %sigma_tilde=sqrt(sum(h.*Weight.*(u0-mu_tilde).^2)/sum(h.*Weight));
    
    ell=ell+1;
    u0=mu_tilde+sigma_tilde*randn(1,M);
    for m=1:M
        u(1)=u0(m);
        for k=1:Kstep
            dW = sqrt(dt)*randn;
            u(k+1)= u(k)+a(u(k))*dt+b(u(k))*dW; %Forward Euler
        end
        Mbar(m)=max(u);
    end
    sMbar=sort(Mbar);
    K_ell=sMbar(1, ceil((1-beta)*M));
    K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
    h=Mbar>=K_ell;
    Weight=(sigma_tilde/sigma_0)*exp(-(u0-mu_0).^2./(2*sigma_0^2)+(u0-mu_tilde).^2./(2*sigma_tilde^2));
end

if K_ell==K
    mu_tilde=sum(h.*Weight.*u0)/sum(h.*Weight)
    %sigma_tilde=sqrt(sum(h.*Weight.*(u0-mu_tilde).^2)/sum(h.*Weight));
end

% Computing the QoI with obtained mu_tilde parameter and fixed sigma_tilde
S=5*10^5;
h=zeros(1,S);
u0=mu_tilde+sigma_tilde*randn(1,S);
for s=1:S
    u(1)=u0(s);
    for k=1:Kstep
        dW = sqrt(dt)*randn;
        u(k+1)= u(k)+a(u(k))*dt+b(u(k))*dW; %Forward Euler
    end
    h(s)=max(u);
end
%Continuous Forward Euler approach
% parfor s=1:S
%     P1=1;
%     u=u0(s);
%     for k=1:Kstep
%         t=(b(u))^2*dt;
%         dW = sqrt(dt)*randn;
%         P1=P1*(1-exp(-2*(K-u)^2/t+2*(b(u)*dW+a(u)*dt)*(K-u)/t))*(K-u>max(0,b(u)*dW+a(u)*dt));
%         u= u+a(u)*dt+b(u)*dW; %Forward Euler
%     end
%     h(s)=1-P1;
% end
Weight=(sigma_tilde/sigma_0)*exp(-(u0-mu_0).^2./(2*sigma_0^2)+(u0-mu_tilde).^2./(2*sigma_tilde^2));
alpha_hat=mean(h.*Weight);
Vaprx=var(h.*Weight);
RelError=1.96/sqrt(alpha_hat*S);
RelErrorIS=1.96*sqrt(Vaprx)/(alpha_hat*sqrt(S));
varRatio=(RelError/RelErrorIS)^2;


