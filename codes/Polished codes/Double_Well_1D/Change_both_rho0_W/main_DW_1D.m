clear; close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for 1D problem to performe IS technique with respect
%%% to both rho_0 and W
%%% 
%------- dX_t= a(X_t)dt+b(X_t)dW_t -----------------------------------%
%----------------------------------------------------------------------%
%%% in computing QoI: P(max_{0<=t<=T} X_t >K)
%seed=1; rng(seed);

%Model parameters
sigma=0.5;
% a=@(t,x) 1-x+0*t;                               %OU drift
% aa=@(x) 1-x;                                    %OU drift
a  = @(t,x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x)+0*t;  %DW drift
aa = @(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x);        %DW drift
b  = @(x) sigma+0*x;                              %constant diffusion

%Simulation parameters
T=1;                                              %simulation time length
K=1                                               %the threshold
Kstep=100;                                        %numerical timestep number in SDE
dt = T/Kstep;                                     %timestep in SDE

%Original initial density parameters
mu_0    =  -1;    %mean
sigma_0 = 0.5   %std
%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = -3;                         % Lower end of discretization interval
xU = K;                          % Upper end of discretization interval
dxPde = 0.005;                   % Space step in PDE    
dtPde = dxPde^2;                 % Time step in PDE
Nx = round((xU-xL)/dxPde);       % Number of space steps in PDE
Nt = round(T/dtPde);             % Number of time steps in PDE
x  = linspace(xL,xU,Nx+1)';      % Space grid points

[~,~,vErik] = tabulate_IC(aa,b,dt,K,xL,xU,false,Nx+1); %KBE solution at T-dt obtained via numerical optimization (using Erik's code)
%Factor in exp.smoothing depends on EM timestep dt, based on numerical
%tests using Erik's solution, we observe the following settings
%dt=0.1, smf=15; dt=0.01, smf=40; dt=0.001, smf=100
smf=40;

%KBE solution for all time
[tW, xW, v] = KBEsolverForAllt(x,T,a,b,Nt,Nx,K,smf); %PDE solver via CN backward scheme using exponential smoothing of Ind.Fun in a final condition

%HJB solution for all time
[tW, xW, u] = HJBsolverForAllt_CN(x,T,a,b,Nt,Nx,K,smf);
% %v=exp(-u);

%[tW, xW, uPont] = HJBsolverForAllt_CN_forPontryagin(x,T,a,b,Nt,Nx,K,smf);
%%
figure
plot(X0range, interp1(xW, u(:,end), X0range), 'b', X0range, interp1(xW, uPont(:,end), X0range), 'r', X0range, valuefun, 'g', 'Linewidth', 2)
xlabel('x','fontsize', 18)
ylabel('v(x,0)','fontsize', 18)
legend('stoch. HJB','determ. HJB','Pontryagin')
title('Value function at t=0 with \sigma=0.05')
%% rho_0 CONTROL
rho_x0 = @(x, m, sig) exp(-(x-m).^2/(2*sig^2))/(sqrt(2*pi*sig^2)); %1D Gaussian density
rhoTilde_x0 = [rho_x0(xW, mu_0, sigma_0).*v(:,end); rho_x0((K:dxPde:K+500*dxPde)', mu_0, sigma_0)];
NC = trapezoidal(rhoTilde_x0, dxPde); %normalising constant
rhoTilde_x0=rhoTilde_x0./NC;          %optimal IS density

%fit optimal IS denisty to Gaussian
x=[xW;(K:dxPde:K+500*dxPde)'];
mFit = trapezoidal(x.*rhoTilde_x0, dxPde)
sigFit = sqrt(trapezoidal(x.^2.*rhoTilde_x0, dxPde)-mFit^2)
rhoFit_x0 = rho_x0(x, mFit, sigFit);

%Plot the densities
figure(1)
plot(x, rho_x0(x, mu_0, sigma_0), '-r',x, rhoTilde_x0, '-b',x, rhoFit_x0, '--b', 'Linewidth',3)
hold on
y = ylim; % current y-axis limits
plot([K K],[y(1) y(2)], '-k', 'LineWidth', 2)
hold off
title('DW process',  'fontsize',22)
xlabel('x','fontsize',18)
legend({['$\rho_{x_0} \sim N($' num2str(mu_0), ', ',num2str(sigma_0) ')'];'$ \tilde{\rho}_{x_0}^{PDE}$'; ['$\tilde{\rho}_{x_0}^{fit}\sim N$(' num2str(mFit,3) ', ' num2str(sigFit,2) ')']; ['K=' num2str(K)]},'fontsize',22,'interpreter','latex')

%% dW CONTROL

%obtained via HJB solution on PDE grids
% ksiGriddedHJB = zeros(length(xW),length(tW));
% ksiGriddedHJB(2:end-1,:) = -sigma*(u(3:end,:)-u(1:end-2,:))./(2*dxPde)/2;
% ksiGriddedHJB(1,:) = -sigma*(u(2,:)-u(1,:))./dxPde/2;
% ksiGriddedHJB(end,:) = -sigma*(u(end,:)-u(end-1,:))./dxPde/2;

% %obtained via KBE solution on PDE grids
ksiGridded = zeros(length(xW),length(tW));
ksiGridded(2:end-1,:) = sigma*(log(v(3:end,:))-log(v(1:end-2,:)))./(2*dxPde);
ksiGridded(1,:) = sigma*(log(v(2,:))-log(v(1,:)))./dxPde;
ksiGridded(end,:) = sigma*(log(v(end,:))-log(v(end-1,:)))./dxPde;

% ksiGriddedHJB_Pont = zeros(length(xW),length(tW));
% ksiGriddedHJB_Pont(2:end-1,:) = -sigma*(uPont(3:end,:)-uPont(1:end-2,:))./(2*dxPde)/2;
% ksiGriddedHJB_Pont(1,:) = -sigma*(uPont(2,:)-uPont(1,:))./dxPde/2;
% ksiGriddedHJB_Pont(end,:) = -sigma*(uPont(end,:)-uPont(end-1,:))./dxPde/2;
% figure(10)
% for j=1:4000:Nt
% plot(xW, ksiGridded(:,j), 'LineWidth', 2)
% hold on
% end
% hold off
% xlabel('x')
% ylabel('ksi')
% figure(11)
% for j=1:4000:Nt
% plot(xW, ksiGriddedHJB(:,j), 'LineWidth', 2)
% hold on
% end
% hold off
% xlabel('x')
% ylabel('ksi')
% figure
% plot(tW, ksiGriddedHJB(800,:),'r-', tW,ksiGriddedHJB_Pont(800,:), 'b-', t, control', 'g-', 'Linewidth', 2)
% xlabel('t','fontsize', 18)
% ylabel('\xi(1,t)','fontsize', 18)
% title('Optimal control at x=1 with \sigma=1')
% legend('stoch. HJB','determ. HJB','Pontryagin')
%%
[Xw,Tw] = ndgrid(xW,tW); %meshgrid for the interpolant
%Optimal control using KBE solution
cropN=0.9*Nx; %if we use KBE, I crop 10% of the left baoundary and linearly smooth it to avoid instability on the left handside; for HJB no need to do this and nCrop=1
F=griddedInterpolant(Xw(cropN:end,:),flip(Tw(cropN:end,:),2), flip(ksiGridded(cropN:end,:),2),'linear','nearest');

%Optimal control using HJB solution
% F = griddedInterpolant(Xw,flip(Tw,2), flip(ksiGriddedHJB,2),'linear','linear');
 ksiStar = @(x,t) F(x,t).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function for any x and t
% L=0;
% U=10
% ksi = @(ksiStar) ksiStar.*(ksiStar<=U && ksiStar>=L)+L.*(ksiStar<L)+U.*(ksiStar>U);
% ksiStar = @(x,t) 0;
% ksi = @(ksiStar) 0;
% ksi = @(x,t)0;
%%Plot the extrapolated optimal control
% figure(6)
% xF  = linspace(xL,K,Nx+1)'; 
% [XwF,TwF] = ndgrid(xF,tW);
% p2=surf(XwF, TwF, ksiStar(XwF,TwF));
%  set(p2,'LineStyle','none')
% %%
% figure(7)
% xF  = linspace(xL-2,K+2,Nx+1)'; 
% [XwF,TwF] = ndgrid(xF,tW);
% dW = sqrt(dt)*randn;
% Likl=exp(-0.5*dt*(ksi(XwF,TwF)).^2-ksi(XwF,TwF)*dW);
% p2=surf(XwF, TwF, exp(-0.5*dt*(ksi(XwF,TwF)).^2-ksi(XwF,TwF)*dW));
% set(p2,'LineStyle','none')
%% Computing the QoI with and without change of measure wrt W and rho_0
S=10^5; %MC sample size
%Initialization
h_IS=zeros(1,S);
h_MC=zeros(1,S);
Ldist=zeros(1,S);
u_MC=zeros(1,Kstep+1);
u_IS=u_MC;
u0=mu_0+sigma_0*randn(1,S);
u0_shifted=mFit+sigFit*randn(1,S);
%t=linspace(0,T,Kstep+1);
meanL=0;
dtMC=0.01;
%u0=X0v*ones(1,S);
parfor s=1:S
       u_MC=u0(s);
       u_IS=u0_shifted(s);
       %u_IS=u0(s);
       L=1;
       k=1;
       t_n=0;
       
       %We integrate until the state hits the threshold or until final time
    while t_n<=T && u_IS(k)<K
        
        %Adaptive timestep
        %dt=min(0.5, 1/ksi(ksiStar(u_IS(k),t_n)))
        %if ksiStar(u_IS(k),t_n)==0
        %     dt=0.01;
        % else
        %     dt=1/ksiStar(u_IS(k),t_n);
        % end
        
        dW = sqrt(dt)*randn;

        L=L*exp(-0.5*dt*(ksiStar(u_IS(k),t_n))^2-ksiStar(u_IS(k),t_n)*dW);
        u_IS(k+1)= u_IS(k)+(aa(u_IS(k))+sigma*ksiStar(u_IS(k),t_n))*dt+sigma*dW; %Forward Euler 
        %u_IS(k+1)= u_IS(k)+(aa(u_IS(k)))*dt+sigma*dW; %Forward Euler 

        k=k+1;
        t_n=t_n+dt;
    end
%     figure(4)
%     hold on
%     plot(0:dt:t_n, u_IS, 'b:', 'Linewidth',2)
%     hold on
%     xlabel('t','fontsize', 18)
%     ylabel('X(t)','fontsize', 18)
%     legend('Pontryagin','Stoch.HJB')
%     pause()
   % MC counterpart simulation    
        n=1;
        while n<=Kstep && u_MC(n)<K
            dW=sqrt(dtMC)*randn;
            u_MC(n+1)=u_MC(n)+aa(u_MC(n))*dtMC+sigma*dW;
            n=n+1;
        end
    Ldist(s)=L;
    meanL=meanL+L;
    h_IS(s)=(max(u_IS)>=K)*L;
    h_MC(s)=(max(u_MC)>=K);
    
    runmax_IS(s)=(max(u_IS)>=K);
end

%semilogy(Ldist)
meanL=meanL/S
Weight_PDE=(sigFit/sigma_0)*exp(-(u0_shifted-mu_0).^2./(2*sigma_0^2)+(u0_shifted-mFit).^2./(2*sigFit^2));
alpha_hat_IS_W=mean(h_IS.*Weight_PDE)
%alpha_hat_IS_W=mean(h_IS)
alpha_hat_MC=mean(h_MC)
Vaprx_IS_W=var(h_IS.*Weight_PDE)
%Vaprx_IS_W=var(h_IS)
Vaprx_MC=var(h_MC)
RelError_MC=1.96*sqrt(Vaprx_MC)/(alpha_hat_MC*sqrt(S))
%RelError_MC=1.96*sqrt(1-alpha_hat_IS_W)/sqrt(alpha_hat_IS_W*S)
RelError_IS=1.96*sqrt(Vaprx_IS_W)/(alpha_hat_IS_W*sqrt(S))
varRatio_approx=(RelError_MC/RelError_IS)^2
varRatio_exact=Vaprx_MC/Vaprx_IS_W
ciMCleft=alpha_hat_MC-1.96*sqrt(Vaprx_MC./S)
ciMCright=alpha_hat_MC+1.96*sqrt(Vaprx_MC./S)
ciISleft=alpha_hat_IS_W-1.96*sqrt(Vaprx_IS_W./S)
ciISright=alpha_hat_IS_W+1.96*sqrt(Vaprx_IS_W./S)

%%
conf=95;% in percent
B=10^4;
if isempty(gcp)
    parpool;
end
opt = statset('UseParallel',true);

q_PDE       = h_IS;
stats_PDE   = bootstrp(B, @(x) [mean(x) std(x)], q_PDE, 'Options', opt);
SmplStd_PDE = mean(stats_PDE(:,1))
CId_pde     = prctile(stats_PDE(:,1), (100-conf)/2)
CIu_pde     =  prctile(stats_PDE(:,1), (100-(100-conf)/2))
%% For plotting KDE density of running maximum
%% Computing the QoI with and without change of measure wrt W and rho_0
S=10^6; %MC sample size
%Initialization
h_IS=zeros(1,S);
h_MC=zeros(1,S);
Ldist=zeros(1,S);
u_MC=zeros(1,Kstep+1);
u_IS=u_MC;
u_IS_W=u_MC;
u_IS_rho0=u_MC;
u0=mu_0+sigma_0*randn(1,S);
u0_shifted=mFit+sigFit*randn(1,S);
%t=linspace(0,T,Kstep+1);
meanL=0;
dtMC=0.01;
%u0=X0v*ones(1,S);
parfor s=1:S
       u_MC=u0(s);
       u_IS=u0_shifted(s);
       u_IS_W=u0(s);
       u_IS_rho0=u0_shifted(s);
       %u_IS=u0(s);
       L=1;
       k=1;
       t_n=0;
       
       %We integrate until the state hits the threshold or until final time
    while t_n<=T && u_IS(k)<K
        
        %Adaptive timestep
        %dt=min(0.5, 1/ksi(ksiStar(u_IS(k),t_n)))
        %if ksiStar(u_IS(k),t_n)==0
        %     dt=0.01;
        % else
        %     dt=1/ksiStar(u_IS(k),t_n);
        % end
        
        dW = sqrt(dt)*randn;

        L=L*exp(-0.5*dt*(ksiStar(u_IS(k),t_n))^2-ksiStar(u_IS(k),t_n)*dW);
        u_IS(k+1)= u_IS(k)+(aa(u_IS(k))+sigma*ksiStar(u_IS(k),t_n))*dt+sigma*dW; %Forward Euler 
        u_IS_W(k+1)= u_IS_W(k)+(aa(u_IS_W(k))+sigma*ksiStar(u_IS_W(k),t_n))*dt+sigma*dW; %Forward Euler 
        u_IS_rho0(k+1)= u_IS_rho0(k)+(aa(u_IS_rho0(k)))*dt+sigma*dW; %Forward Euler 

        k=k+1;
        t_n=t_n+dt;
    end
%     figure(4)
%     hold on
%     plot(0:dt:t_n, u_IS, 'b:', 'Linewidth',2)
%     hold on
%     xlabel('t','fontsize', 18)
%     ylabel('X(t)','fontsize', 18)
%     legend('Pontryagin','Stoch.HJB')
%     pause()
   % MC counterpart simulation    
        n=1;
        while n<=Kstep && u_MC(n)<K
            dW=sqrt(dtMC)*randn;
            u_MC(n+1)=u_MC(n)+aa(u_MC(n))*dtMC+sigma*dW;
            n=n+1;
        end
    Ldist(s)=L;
    meanL=meanL+L;
    h_IS(s)=(max(u_IS)>=K)*L;
    h_IS_W(s)=(max(u_IS_W)>=K)*L;
    h_IS_rho0(s)=(max(u_IS_rho0)>=K);
    h_MC(s)=(max(u_MC)>=K);
    
    runmax_IS(s)=max(u_IS);
    runmax_IS_W(s)=max(u_IS_W);
    runmax_IS_rho0(s)=max(u_IS_rho0)
    runmax_MC(s)=max(u_MC);

end

%semilogy(Ldist)
meanL=meanL/S
Weight_PDE=(sigFit/sigma_0)*exp(-(u0_shifted-mu_0).^2./(2*sigma_0^2)+(u0_shifted-mFit).^2./(2*sigFit^2));
alpha_hat_IS_W=mean(h_IS.*Weight_PDE)
%alpha_hat_IS_W=mean(h_IS)
alpha_hat_MC=mean(h_MC)
Vaprx_IS_W=var(h_IS.*Weight_PDE)
%Vaprx_IS_W=var(h_IS)
Vaprx_MC=var(h_MC)

RelError_MC=1.96*sqrt(Vaprx_MC)/(alpha_hat_MC*sqrt(S))
%RelError_MC=1.96*sqrt(1-alpha_hat_IS_W)/sqrt(alpha_hat_IS_W*S)
RelError_IS=1.96*sqrt(Vaprx_IS_W)/(alpha_hat_IS_W*sqrt(S))
varRatio_approx=(RelError_MC/RelError_IS)^2
varRatio_exact=Vaprx_MC/Vaprx_IS_W
ciMCleft=alpha_hat_MC-1.96*sqrt(Vaprx_MC./S);
ciMCright=alpha_hat_MC+1.96*sqrt(Vaprx_MC./S);
ciISleft=alpha_hat_IS_W-1.96*sqrt(Vaprx_IS_W./S);
ciISright=alpha_hat_IS_W+1.96*sqrt(Vaprx_IS_W./S);
%%
[f_both,xboth] = ksdensity(runmax_IS, 'function', 'pdf');
[f_IS_W,x_W] = ksdensity(runmax_IS_W,'function', 'pdf'); 
[f_IS_rho0,x_rho0] = ksdensity(runmax_IS_rho0,'function', 'pdf'); 
[f_MC,x_MC] = ksdensity(runmax_MC, 'function', 'pdf'); 
figure
plot(x_MC, f_MC, 'r-', x_rho0, f_IS_rho0, 'b:', x_W, f_IS_W, 'c--',xboth, f_both, 'k-.', 'Linewidth',4);
% hold on
% plot([K K],[y(1) y(2)], '-k', 'LineWidth', 4)
% hold off
xlabel('u', 'fontsize', 18)
[~, hobj, ~, ~] =legend('MC', 'IS wrt \rho_0', 'IS wrt W(t)', 'IS wrt both','fontsize',25)
ylabel('density function of QoI','fontsize', 18)
title('$\sigma_0=1, \mathcal{K}=2$','interpreter', 'latex','fontsize', 20)
pp = get(gca,'XTickLabel');
set(gca,'XTickLabel',pp,'FontName','Times','fontsize',20)
hl = findobj(hobj,'type','line');
set(hl,'LineWidth',2.5);
set(gca,'FontSize',40)
