clear;%close all; set(0,'defaultaxesfontsize',20);format long
%%% EnKF, Lorenz 96
%% setup

%Model parameters
sigma=0.5;
% a=@(t,x) 1-x+0*t;                               %OU drift
% aa=@(x) 1-x;                                    %OU drift
a  = @(t,x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x)+0*t;  %DW drift
aa = @(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x);        %DW drift
b  = @(x) sigma+0*x;                              %constant diffusion

%Simulation parameters
T=10;                                            %simulation time length
K=2.5                                           %the threshold

%Original initial density parameters
mu_0  =  -1;    %mean
sigma_0 = 1;   %std

M=100000;
q=1;r=1;p=1;N=r*p;% observe q/r coordinates in N dimensions
%N = 12;
J=100;            % number of steps and parameter
gamma=1e-1;       % observational noise variance is gammaˆ2
I=eye(N);C0=I;    % prior initial condition covariance
m0=zeros(N,1);    % prior initial condition mean
tau=1;            % obs time step
dt=tau/J;         % time discretization is tau
ObsSteps = round(T/tau)+1;

% observation operator
%H=I;
H=zeros(q*p,N);
for k=1:p
    H(q*(k-1)+1:q*k,r*(k-1)+1:r*k)=[eye(q),zeros(q,r-q)];
end

m=zeros(N,ObsSteps);yobs=m(1:q*p,ObsSteps);c=zeros(N,N,ObsSteps);% pre-allocate
%Generate the Truth and Observations
vTruth=zeros(N,ObsSteps);
vTruth(:,1)=mu_0+sigma_0*randn(N,1);% initial truth
yobs(:,1) = H*vTruth(:,1) + gamma*randn(q*p,1);
for n=1:ObsSteps-1
    w=vTruth(:,n);
    for k=1:J
        w=w+aa(w)*dt + sigma*sqrt(dt)*randn(N,1); %truth  
    end
    vTruth(:,n+1)=w;
    yobs(:,n+1) = H*vTruth(:,n+1) + gamma*randn(q*p,1);%observation
end

%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = -3;                         % Lower end of discretization interval
xU = K;                          % Upper end of discretization interval
dxPde = 0.005;                   % Space step in PDE    
dtPde = dxPde^2;                 % Time step in PDE
Nx = round((xU-xL)/dxPde);       % Number of space steps in PDE
Nt = round(tau/dtPde);             % Number of time steps in PDE
x  = linspace(xL,xU,Nx+1)';      % Space grid points

%[~,~,vErik] = tabulate_IC(aa,b,dt,K,xL,xU,false,Nx+1); %KBE solution at T-dt obtained via numerical optimization (using Erik's code)
%Factor in exp.smoothing depends on EM timestep dt, based on numerical
%tests using Erik's solution, we observe the following settings
%dt=0.1, smf=15; dt=0.01, smf=40; dt=0.001, smf=100
smf=40;

%KBE solution for all time
[tW, xW, v] = KBEsolverForAllt(x,T,a,b,Nt,Nx,K,smf); %PDE solver via CN backward scheme using exponential smoothing of Ind.Fun in a final condition

%HJB solution for all time
[tW, xW, u] = HJBsolverForAllt_CN(x,T,a,b,Nt,Nx,K,smf);
%v=exp(-u);

%% rho_0 CONTROL
rho_x0 = @(x, m, sig) exp(-(x-m).^2/(2*sig^2))/(sqrt(2*pi*sig^2)); %1D Gaussian density
rhoTilde_x0 = [rho_x0(xW, mu_0, sigma_0).*v(:,end); rho_x0((K:dxPde:K+100*dxPde)', mu_0, sigma_0)];
NC = trapezoidal(rhoTilde_x0, dxPde); %normalising constant
rhoTilde_x0=rhoTilde_x0./NC;          %optimal IS density

%fit optimal IS denisty to Gaussian
x=[xW;(K:dxPde:K+100*dxPde)'];
mFit = trapezoidal(x.*rhoTilde_x0, dxPde)
sigFit = sqrt(trapezoidal(x.^2.*rhoTilde_x0, dxPde)-mFit^2)
rhoFit_x0 = rho_x0(x, mFit, sigFit);

%Plot the densities
figure(1)
plot(x, rho_x0(x, mu_0, sigma_0), '-r',x, rhoTilde_x0, '-b',x, rhoFit_x0, '--b', 'Linewidth',3)
hold on
y = ylim; % current y-axis limits
plot([K K],[y(1) y(2)], '-k', 'LineWidth', 2)
hold off
title('DW process',  'fontsize',22)
xlabel('x','fontsize',18)
legend({['$\rho_{x_0} \sim N($' num2str(mu_0), ', ',num2str(sigma_0) ')'];'$ \tilde{\rho}_{x_0}^{PDE}$'; ['$\tilde{\rho}_{x_0}^{fit}\sim N$(' num2str(mFit,3) ', ' num2str(sigFit,2) ')']; ['K=' num2str(K)]},'fontsize',22,'interpreter','latex')

%% dW CONTROL

%obtained via HJB solution on PDE grids
ksiGriddedHJB = zeros(length(xW),length(tW));
ksiGriddedHJB(2:end-1,:) = -sigma*(u(3:end,:)-u(1:end-2,:))./(2*dxPde);
ksiGriddedHJB(1,:) = -sigma*(u(2,:)-u(1,:))./dxPde;
ksiGriddedHJB(end,:) = -sigma*(u(end,:)-u(end-1,:))./dxPde;

%obtained via KBE solution on PDE grids
ksiGridded = zeros(length(xW),length(tW));
ksiGridded(2:end-1,:) = sigma*(log(v(3:end,:))-log(v(1:end-2,:)))./(2*dxPde);
ksiGridded(1,:) = sigma*(log(v(2,:))-log(v(1,:)))./dxPde;
ksiGridded(end,:) = sigma*(log(v(end,:))-log(v(end-1,:)))./dxPde;

figure(10)
for j=1:4000:Nt
plot(xW, ksiGridded(:,j), 'LineWidth', 2)
hold on
end
hold off
xlabel('x')
ylabel('ksi')

figure(11)
for j=1:4000:Nt
plot(xW, ksiGriddedHJB(:,j), 'LineWidth', 2)
hold on
end
hold off
xlabel('x')
ylabel('ksi')

[Xw,Tw] = ndgrid(xW,tW); %meshgrid for the interpolant
%Optimal control using KBE solution
cropN=0.1*Nx; %if we use KBE, I crop 10% of the left baoundary and linearly smooth it to avoid instability on the left handside; for HJB no need to do this and nCrop=1
F=griddedInterpolant(Xw(cropN:end,:),flip(Tw(cropN:end,:),2), flip(ksiGridded(cropN:end,:),2),'linear','nearest');

% %Optimal control using HJB solution
% F = griddedInterpolant(Xw,flip(Tw,2), flip(ksiGriddedHJB,2),'linear','linear');
 ksiStar = @(x,t) F(x,t).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function for any x and t

%% solution % assimilate!
v0=mu_0+sigma_0*randn(N,M);% initial ensemble
m(:,1)=mu_0;
c(:,:,1)=sigma_0;
v_shifted=mFit+sigFit*randn(1,M);
m_shifted(:,1)=mFit;
c_shifted(:,:,1)=sigFit;
tv=[0:dt:T];
v=zeros(J,M);
Kv=zeros(1,ObsSteps);
Kv(1)=K;
S=zeros(1,ObsSteps);
S(1)=mFit;

for i=1:ObsSteps-1
    v(1,:)=v0;
    for j=1:J
        v(j+1,:)=v(j,:)+aa(v(j,:))*dt + sigma*sqrt(dt)*randn(N,M);
    end 
       %alpha=mean((max(v)>K))
    
       mEnKF=mean(v(end,:));
       cEnKF = cov(v(end,:));%covariance predict

       Kg = (cEnKF*H')/(gamma^2*eye(q*p)+H*cEnKF*H');%Kalman gain
       yTilde=yobs(:,i+1)+gamma*randn(q*p,M);%perturbed observation
       v(end,:)=(I-Kg*H)*v(end,:)+Kg*yTilde;%ensemble update
       m(:,i+1) = mean(v(end,:));%mean update
       c(:,:,i+1) = cov(v(end,:));%covariance update  
       v0=v(end,:);
       
        %IS 
        % PDE method
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        xL = -3;                         % Lower end of discretization interval
        xU = K;                          % Upper end of discretization interval
        dxPde = 0.005;                   % Space step in PDE    
        dtPde = dxPde^2;                 % Time step in PDE
        Nx = round((xU-xL)/dxPde);       % Number of space steps in PDE
        Nt = round(tau/dtPde);           % Number of time steps in PDE
        x  = linspace(xL,xU,Nx+1)';      % Space grid points
        smf=40;
        T1=i-1;
        T2=i;
        %KBE solution for all time
        [tW, xW, vPDE] = KBEsolverForAllt2(x,T1,T2,a,b,Nt,Nx,K,smf); %PDE solver via CN backward scheme using exponential smoothing of Ind.Fun in a final condition

        rhoTilde_x0 = [rho_x0(xW, m(1,i+1), c(1,1,i+1)).*vPDE(:,end); rho_x0((K:dxPde:K+100*dxPde)', m(1,i+1), c(1,1,i+1))];
        NC = trapezoidal(rhoTilde_x0, dxPde); %normalising constant
        rhoTilde_x0=rhoTilde_x0./NC;          %optimal IS density

        %fit optimal IS denisty to Gaussian
        x=[xW;(K:dxPde:K+100*dxPde)'];
        mFit = trapezoidal(x.*rhoTilde_x0, dxPde)
        sigFit = sqrt(trapezoidal(x.^2.*rhoTilde_x0, dxPde)-mFit^2)
        
        %obtained via KBE solution on PDE grids
        ksiGridded = zeros(length(xW),length(tW));
        ksiGridded(2:end-1,:) = sigma*(log(vPDE(3:end,:))-log(vPDE(1:end-2,:)))./(2*dxPde);
        ksiGridded(1,:) = sigma*(log(vPDE(2,:))-log(vPDE(1,:)))./dxPde;
        ksiGridded(end,:) = sigma*(log(vPDE(end,:))-log(vPDE(end-1,:)))./dxPde;
        
        [Xw,Tw] = ndgrid(xW,tW); %meshgrid for the interpolant
        %Optimal control using KBE solution
        cropN=0.1*Nx; %if we use KBE, I crop 10% of the left baoundary and linearly smooth it to avoid instability on the left handside; for HJB no need to do this and nCrop=1
        F=griddedInterpolant(Xw(cropN:end,:),flip(Tw(cropN:end,:),2), flip(ksiGridded(cropN:end,:),2),'linear','nearest');
        ksiStar = @(x,t) F(x,t).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function for any x and t

        %Auxililary ensemble for rare events
        mu_0=m(1,i+1);
        sig_0=c(1,1,i+1);
        v_shifted=mFit+sigFit*randn(1,M);
        sum_v=0;
        parfor k=1:M
            L=1;
            for j=1:J
                t_n=tv(j);
                dW = sqrt(dt)*randn(N,1);
                L=L*exp(-0.5*dt*(ksiStar(v_shifted(k),t_n))^2-ksiStar(v_shifted(k),t_n)*dW);
                v_shifted(k)=v_shifted(k)+(aa(v_shifted(k))+sigma*ksiStar(v_shifted(k),t_n))*dt + sigma*dW;
            end
            Weight_PDE=(sigFit/sig_0)*exp(-(v_shifted(k)-mu_0).^2./(2*sig_0^2)+(v_shifted(k)-mFit).^2./(2*sigFit^2));
            sum_v=sum_v+v_shifted(k)*L*Weight_PDE;
        end 
       S(i+1)=sum_v/M;
       m_shifted(:,i+1) = mean(v_shifted);%mean update
       c_shifted(:,:,i+1) = cov(v_shifted);%covariance update  
       
       K=round(max(max(v)),1);
       Kv(i+1)=K;
end

s=1;
%%
figure;
plot([0:T],vTruth(s,1:ObsSteps), 'r--', 'LineWidth', 3);hold; plot([0:T],m(s,1:ObsSteps),'m-o','LineWidth', 3);
plot([0:T],m(s,1:ObsSteps)+1.96*reshape(sqrt(c(s,s,1:ObsSteps)),1,ObsSteps),'m:','LineWidth', 3);
plot([0:T],m(s,1:ObsSteps)-1.96*reshape(sqrt(c(s,s,1:1:ObsSteps)),1,ObsSteps),'m:','LineWidth', 3);
plot([0:T],m_shifted(s,1:ObsSteps),'b-o','LineWidth', 3);
plot([0:T],m_shifted(s,1:ObsSteps)+1.96*reshape(sqrt(c_shifted(s,s,1:ObsSteps)),1,ObsSteps),'b:','LineWidth', 3);
plot([0:T],m_shifted(s,1:ObsSteps)-1.96*reshape(sqrt(c_shifted(s,s,1:1:ObsSteps)),1,ObsSteps),'b:','LineWidth', 3);
plot([0:T], yobs(s,1:ObsSteps),'*r','LineWidth', 4);
plot([0:T],Kv,'k--','LineWidth', 2);
hold;grid;xlabel('observation times');legend('u_n','m_n','95% CI','', 'm_n shifted', '95% CI','','y_n','Threshold', 'Fontsize', 18)
%title('EnKF, L96, T=20, d=40, F=2');
% figure;plot([0:Jj-1],sum((v(:,1:Jj)-m(:,1:Jj)).^2/N));hold;
% grid;hold;xlabel('t');title('ExKF, L96, MSE, 2/3 observed')
