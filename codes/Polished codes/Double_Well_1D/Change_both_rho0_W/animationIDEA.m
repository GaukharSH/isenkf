clear; close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for 1D problem to performe IS technique with respect
%%% to both rho_0 and W
%%% 
%------- dX_t= a(X_t)dt+b(X_t)dW_t -----------------------------------%
%----------------------------------------------------------------------%
%%% in computing QoI: P(max_{0<=t<=T} X_t >K)
%seed=1; rng(seed);
%Model parameters
sigma=0.5;
% a=@(t,x) 1-x+0*t;                               %OU drift
% aa=@(x) 1-x;                                    %OU drift
a  = @(t,x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x)+0*t;  %DW drift
aa = @(x) 0.25*(8*x./(1+2*x.^2).^2 - 2*x);        %DW drift
b  = @(x) sigma+0*x;                              %constant diffusion

%Simulation parameters
T=1;                                            %simulation time length
K=1.5                                             %the threshold
Kstep=100;                                      %numerical timestep number in SDE
dt = T/Kstep;                                   %timestep in SDE

%Original initial density parameters
mu_0  =  -1;    %mean
sigma_0 = 0.5   %std
%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = -3;                         % Lower end of discretization interval
xU = K;                          % Upper end of discretization interval
dxPde = 0.005;                   % Space step in PDE    
dtPde = dxPde^2;                 % Time step in PDE
Nx = round((xU-xL)/dxPde);       % Number of space steps in PDE
Nt = round(T/dtPde);             % Number of time steps in PDE
x  = linspace(xL,xU,Nx+1)';      % Space grid points

%[~,~,vErik] = tabulate_IC(aa,b,dt,K,xL,xU,false,Nx+1); %KBE solution at T-dt obtained via numerical optimization (using Erik's code)
%Factor in exp.smoothing depends on EM timestep dt, based on numerical
%tests using Erik's solution, we observe the following settings
%dt=0.1, smf=15; dt=0.01, smf=40; dt=0.001, smf=100
smf=40;

%KBE solution for all time
[tW, xW, v] = KBEsolverForAllt(x,T,a,b,Nt,Nx,K,smf); %PDE solver via CN backward scheme using exponential smoothing of Ind.Fun in a final condition

%HJB solution for all time
[tW, xW, u] = HJBsolverForAllt_CN(x,T,a,b,Nt,Nx,K,smf);
%v=exp(-u);
%% rho_0 CONTROL
rho_x0 = @(x, m, sig) exp(-(x-m).^2/(2*sig^2))/(sqrt(2*pi*sig^2)); %1D Gaussian density
rhoTilde_x0 = [rho_x0(xW, mu_0, sigma_0).*v(:,end); rho_x0((K:dxPde:K+100*dxPde)', mu_0, sigma_0)];
NC = trapezoidal(rhoTilde_x0, dxPde); %normalising constant
rhoTilde_x0=rhoTilde_x0./NC;          %optimal IS density

%fit optimal IS denisty to Gaussian
x=[xW;(K:dxPde:K+100*dxPde)'];
mFit = trapezoidal(x.*rhoTilde_x0, dxPde)
sigFit = sqrt(trapezoidal(x.^2.*rhoTilde_x0, dxPde)-mFit^2)
rhoFit_x0 = rho_x0(x, mFit, sigFit);

%Plot the densities
figure(1)
plot(x, rho_x0(x, mu_0, sigma_0), '-r',x, rhoTilde_x0, '-b',x, rhoFit_x0, '--b', 'Linewidth',3)
hold on
y = ylim; % current y-axis limits
plot([K K],[y(1) y(2)], '-k', 'LineWidth', 2)
hold off
title('DW process',  'fontsize',22)
xlabel('x','fontsize',18)
legend({['$\rho_{x_0} \sim N($' num2str(mu_0), ', ',num2str(sigma_0) ')'];'$ \tilde{\rho}_{x_0}^{PDE}$'; ['$\tilde{\rho}_{x_0}^{fit}\sim N$(' num2str(mFit,3) ', ' num2str(sigFit,2) ')']; ['K=' num2str(K)]},'fontsize',22,'interpreter','latex')

%% dW CONTROL

%obtained via HJB solution on PDE grids
ksiGriddedHJB = zeros(length(xW),length(tW));
ksiGriddedHJB(2:end-1,:) = -sigma*(u(3:end,:)-u(1:end-2,:))./(2*dxPde);
ksiGriddedHJB(1,:) = -sigma*(u(2,:)-u(1,:))./dxPde;
ksiGriddedHJB(end,:) = -sigma*(u(end,:)-u(end-1,:))./dxPde;

% %obtained via KBE solution on PDE grids
% ksiGridded = zeros(length(xW),length(tW));
% ksiGridded(2:end-1,:) = sigma*(log(v(3:end,:))-log(v(1:end-2,:)))./(2*dxPde);
% ksiGridded(1,:) = sigma*(log(v(2,:))-log(v(1,:)))./dxPde;
% ksiGridded(end,:) = sigma*(log(v(end,:))-log(v(end-1,:)))./dxPde;

% figure(10)
% for j=1:4000:Nt
% plot(xW, ksiGridded(:,j), 'LineWidth', 2)
% hold on
% end
% hold off
% xlabel('x')
% ylabel('ksi')
figure(11)
for j=1:4000:Nt
plot(xW, ksiGriddedHJB(:,j), 'LineWidth', 2)
hold on
end
hold off
xlabel('x')
ylabel('ksi')
%%

[Xw,Tw] = ndgrid(xW,tW); %meshgrid for the interpolant
% cropN=0.9*Nx; %if we use KBE, I crop 10% of the left baoundary and linearly smooth it to avoid instability on the left handside; for HJB no need to do this and nCrop=1
% F=griddedInterpolant(Xw(cropN:end,:),flip(Tw(cropN:end,:),2), flip(ksiGridded(cropN:end,:),2),'linear','nearest');
F = griddedInterpolant(Xw,flip(Tw,2), flip(ksiGriddedHJB,2),'linear','linear');
ksiStar = @(x,t) F(x,t).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function for any x and t
% L=0;
% U=10
% ksi = @(ksiStar) ksiStar.*(ksiStar<=U && ksiStar>=L)+L.*(ksiStar<L)+U.*(ksiStar>U);
%ksiStar = @(x,t) 0;
% ksi = @(ksiStar) 0;
%ksi = @(x,t)0;
% % %Plot the extrapolated optimal control
% figure(6)
% xF  = linspace(xL-5,K+2,Nx+1)'; 
% [XwF,TwF] = ndgrid(xF,tW);
% p2=surf(XwF, TwF, sigma^2./ksiStar(XwF,TwF));
% set(p2,'LineStyle','none')
% %%
% figure(7)
% xF  = linspace(xL-2,K+2,Nx+1)'; 
% [XwF,TwF] = ndgrid(xF,tW);
% dW = sqrt(dt)*randn;
% Likl=exp(-0.5*dt*(ksi(XwF,TwF)).^2-ksi(XwF,TwF)*dW);
% p2=surf(XwF, TwF, exp(-0.5*dt*(ksi(XwF,TwF)).^2-ksi(XwF,TwF)*dW));
% set(p2,'LineStyle','none')
%% Computing the QoI with and without change of measure wrt W and rho_0
close all
S=20; %MC sample size
%Initialization
h_IS=zeros(1,S);
h_MC=zeros(1,S);
Ldist=zeros(1,S);
u_MC=zeros(1,Kstep+1);
u_IS_rho0=u_MC;
u_IS_dW=u_MC;
u_IS_both=u_MC;
u0=mu_0+sigma_0*randn(1,S);
u0_shifted=mFit+sigFit*randn(1,S);
%t=linspace(0,T,Kstep+1);
meanL=0;
dtMC=0.01;
t=[0:0.01:1];
for s=1:S
       u_MC=u0(s);
       u_IS_rho0=u0_shifted(s);
       u_IS_dW=u0(s);
       u_IS_both=u0_shifted(s);
       L=1;

       t_n=0;
    for k=1:Kstep

        dW = sqrt(dt)*randn;
        u_MC(k+1)= u_MC(k)+aa(u_MC(k))*dt+sigma*dW; %Forward Euler
        u_IS_rho0(k+1)= u_IS_rho0(k)+aa(u_IS_rho0(k))*dt+sigma*dW; %Forward Euler
        %L=L*exp(-0.5*dt*(ksiStar(u_IS(k),t_n))^2-ksiStar(u_IS(k),t_n)*dW);
        u_IS_dW(k+1)= u_IS_dW(k)+(aa(u_IS_dW(k))+sigma*ksiStar(u_IS_dW(k),t_n))*dt+sigma*dW; %Forward Euler 
        u_IS_both(k+1)= u_IS_both(k)+(aa(u_IS_both(k))+sigma*ksiStar(u_IS_both(k),t_n))*dt+sigma*dW; %Forward Euler 

        t_n=t_n+dt;
    end
    figure(1)
    plot(t,u_MC,'r-', t, u_IS_rho0, 'b-', t, u_IS_dW, 'c-', t, u_IS_both, 'm-', 'LineWidth', 3)
    xlabel('t','fontsize', 24)
    hold on
    yline(K,'k-', 'LineWidth', 3);
    legend('MC', 'IS with $\rho_0$', 'IS with $dW$', 'IS with both','K=1', 'interpreter', 'latex', 'FontSize', 26)
    figure(2)
    plot(t,u_MC,'r-', t, u_IS_both, 'b:', 'LineWidth', 3)
    hold on
    yline(K,'k--', 'LineWidth', 6);
    legend('Main', 'Auxiliary', 'Threshold', 'interpreter', 'latex', 'FontSize', 26)
    xlabel('t','fontsize', 24)
end
xgca=get(gca,'XTickLabel');
ygca=get(gca,'YTickLabel');
set(gca,'XTickLabel',xgca,'FontName','Times','fontsize',20)
set(gca,'YTickLabel',ygca,'FontName','Times','fontsize',20)