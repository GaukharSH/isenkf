clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for Langevin 2D problem to test CE method and PDE method in
%%% performing IS technique: 
%%% X - particle position, V -particle velocity 
%------- dX_t = dV_t dt -----------------------------------------------%
%------- dV_t = -U'(X_t)dt-kappa*V_tdt+(2*kappa*Temp)^1/2 dW_t --------%
%----------------------------------------------------------------------%
%%% in computing QoI: P(max_{0<=t<=T} P1*X_t >K)
P1   = [0 1];                                               %Projector to track the velocity not the position

%Model parameters
d=2;                                                        %problem dimension 
kappa=2^(-5)*pi^2;                                          %viscosity
Temp=1;                                                     %temperature
a    = @(x,v) [v; 0.25*(8*x./(1+2*x.^2).^2 - 2*x)-kappa*v]; %2d Langevin drift
b    = [0;sqrt(2*kappa*Temp)];                              %2d Langevin diffusion
dUdx = @(x) 0.25*(-8*x./(1+2*x.^2).^2 + 2*x);               %U'(x)

%Simulation parameters
T=1;        %Final Time (simulation length)
M=10^5;     %Monte Carlo sample size
N=100;     %Timestep size in SDE
J=1;        %Brownian motion W_t dimenson
dt=T/N;     %time discretization ib SDE
K=3         %the threshold

%Initial original density parameters
mu=[0 0]';                          %2D mean
corr= 0;                            %correlation
Sigma=[0.5 corr*0.5; corr*0.5 0.5]; %2D covariance

%Generate samples of Langevin dynamics to use for both CE and L2 regression
u=zeros(d,N,M);
Mbar=zeros(1,M);
r=exp(-kappa*dt);
sigOU=sqrt(2*kappa*Temp*(1-exp(-2*kappa*dt))/(2*kappa));
for m=1:M
    u(:,1,m)=mu+chol(Sigma)'*randn(d,1);
    %u(:,1,m)=[-1; 1];
    for n=2:N+1
        u(2,n,m)=r*u(2,n-1,m)+sigOU*randn;
        u(2,n,m)=u(2,n,m)-dUdx(u(1,n-1,m))*dt;
        u(1,n,m)=u(1,n-1,m)+u(2,n,m)*dt;
    end
    Mbar(m)=max(P1*u(:,:,m));
end

%% Plot Langevin dynamics tejectory
% V = @(x) 0.25*(2./(1+2*x.^2) + x.^2);
% plot([-3:0.1:3], V([-3:0.1:3])-3, 'k:', u(1,:,1), u(2,:,1),'b-', u(1,1,1), u(2,1,1),'r*','LineWidth', 2)
% aa = get(gca,'XTickLabel');
% set(gca,'XTickLabel',aa,'FontName','Times','fontsize',18)
% xlabel('$u_t$', 'interpreter', 'latex', 'fontsize', 25)
% ylabel('$v_t$', 'interpreter', 'latex','fontsize', 25)
% title('$\mathcal{T}=0.1$', 'interpreter', 'latex', 'fontsize', 25)
% ylim([-3 3])
% xlim([-3 3])
%% Markovian Projection via L2 regression
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We approximate only the drift, the diffusion is constant

%Define the Polynomial space
w = 2;                                                                   %Polynomial degree
p = poly_space(w,'TD');                                                  %Polynomial space
p_dim = size(p, 1);                                                      %Cardinality of polynomial space

t  = linspace(0,T,N+1)';                                                 %Generating t_0, t_1, ..., t_{N-1}
Tt = reshape(repmat(t,1,M)', (N+1)*M,1);                                 %Replicating M times each t_n and saving as a column vector [t_0 t_0 ... t_0, t_1, ..., t_1,..., t_{N-1}, ..., t_{N_1}]'
Xx = reshape(squeeze(pagemtimes(P1,u))', (N+1)*M,1);                     %Projected Samples saved as a column vector [X_0^(1),..., X_0^(M), X_1^(1),..,X_1^(M), ..., X_{N-1}^(1),...,X_{N-1}^(M)]
tX = [Tt Xx];                                                            %Two column vectors of size N*M for [t_n, X_{t_n}]
f  = reshape(squeeze(pagemtimes(P1,a(u(1,:,:),u(2,:,:))))',(N+1)*M,1);   %Given f function, we are going to solve

D     = x2fx(tX, p);                                                     %This function helps to create Psi matrix each column is non-orth. basis function psi_p for different given p
[Q,R] = modified_GS(D);                                                  %Apply modified Gram-Schmidt process to get QR decomposition
a_p   = Q'*f;                                                            %Solve Normal equations based on orthonormalised basis
 
psy     = @(t,s) x2fx([t s], p);                                         %Non-orth. basis function
psy_bar = @(t,s) x2fx([t s], p)/R;                                       %Orth. basis function
a_bar   = @(t,s) psy_bar(t,s)*a_p;                                       %Approximation to the drift function a(x)
b_bar   = @(s)   sqrt(P1*(b*b')*P1')*ones(size(s));                      %Approximation to the diffusion function b(x)
%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = -3;                                                       %Lower end of discretization interval for x
xU = 3;                                                        %Upper end of discretization interval for x
vL = -3;                                                       %Lower end of discretization interval for v
vU = 3;                                                        %Upper end of discretization interval for v
Nx = 1000;                                                      %Number of spatial step size for x
dx = (xU-xL)/Nx;                                               %Stepsize for x
Nv = 1000;                                                    %Number of spatial step size for v
dv = (vU-vL)/Nv;                                               %Stepsize for v
Nt = 2.5*1e3;                                                  %Number of time steps
xPDE = linspace(xL,xU,Nx+1)';                                  %Linspace for x
vPDE = linspace(vL,vU,Nv+1)';                                  %Linspace for v
sL = P1*[xL;vL];                                               %Lower end of discretization interval for ptojected process s=P_1x
sU = P1*[xU;vU];                                               %Upper end of discretization interval for ptojected process s=P_1x
Ns = P1*[Nx;Nv];                                               %Number of discretization intervals in space s
ds = (sU-sL)/Ns;                                               %Spatial step size in s
sPDE  = linspace(sL,sU,Ns+1)';                                 %Linspace for s

%PDE solution
ileqK  = find(sPDE<=K);                                           %Indexes of sPDE which is less or equal than K
igreK  = find(sPDE>K);                                            %Indexes of sPDE which is greater than K
NsleqK = length(ileqK)-1;                                         %Number of spatial stepsize used for PDE solver
[s, uPDE] = KBEsolverL2(sPDE(ileqK),T,a_bar,b_bar,Nt,NsleqK);     %PDE solver for the inhomogeneous underlying dynamics with first few time steps Backward Euler and the rest is Crank-Nickolson.

%Plot PDE solution at time 0
figure(1)
plot(s, uPDE, 'Linewidth',2)
xlabel('s', 'fontsize', 14)
ylabel('u(s,0)','fontsize', 14)

%Initial density is set to 2D Gaussian 
rho_Gaussian = @(x, m, sig) exp(-(x-m).^2/(2*sig^2))/(sqrt(2*pi*sig^2)); 
[x, v]   = meshgrid(xPDE, vPDE);
xv = [x(:) v(:)];
rhoJoint_0=(reshape(mvnpdf(xv,mu',Sigma),length(vPDE),length(xPDE)))'; 

%Plot the initial original denisty
figure(2)
h=surf(vPDE, xPDE, rhoJoint_0);
axis equal
set(h,'LineStyle','none')
ylim([-3 3])
xlim([-3 3])
view(2)
aa = get(gca,'XTickLabel');
set(gca,'XTickLabel',aa,'FontName','Times','fontsize',35)
bb = get(gca,'YTickLabel');
set(gca,'YTickLabel',bb,'FontName','Times','fontsize',35)
title(['$\rho_{[u_0,v_0]}\sim N([0; 0]; [0.5, 0; 0, 0.5])$'], 'FontName','Times', 'fontsize',40,'interpreter','latex')
xlabel('$v_0$', 'fontsize',40,'interpreter','latex')
ylabel('$u_0$','fontsize',40,'interpreter','latex')
 % xlabel('x','fontsize',40)

%Note that the optimal denisty rhoJointTilde_0(x,v) \approx rho_0(x,v)sqrt(u_PDE(v,0))
% where rho_0(x,v) = rho_0(x|v)rho_0(v) and rho_0(v)*sqrt(u_PDE(v,0)) is fitted to 1D Gaussian

%Marginal denisty in v since we consider the projection P1=[0 1] and solved the KBE with respect to v
rhoMarg_v = rho_Gaussian(s, mu(2), sqrt(Sigma(2,2)));
rhoTemp = [rhoMarg_v.*sqrt(abs(uPDE));rho_Gaussian(sPDE(igreK), mu(2), sqrt(Sigma(2,2)))]; %after x>K we have Gaussian
NC = trapezoidal(rhoTemp, ds); %normalising constant
rhoTemp=rhoTemp./NC;           %optimal IS density

%Fitting the rho_0(v)*sqrt(u_PDE) to Gaussian
v=[s;sPDE(igreK)];
mFit   = trapezoidal(v.*rhoTemp, dv)
sigFit = sqrt(trapezoidal(v.^2.*rhoTemp, dv)-mFit^2)
rhoTildeFit_0 = rho_Gaussian(vPDE, mFit, sigFit);

%Conditional Gaussian rho_0(x|v) 
vSmpl=mFit+sigFit*randn(1,M);
mCond=mu(1)+corr*(vSmpl-mu(2))*sqrt(Sigma(1,1)/Sigma(2,2));
sigCond=sqrt((1-corr^2)*Sigma(1,1));
rhoTildeCond_0= rho_Gaussian(xPDE, mCond(1), sigCond);

%The optimal IS density
rhoJointTilde_0=rhoTildeCond_0*rhoTildeFit_0';
    
figure(3)
h=surf(vPDE,xPDE,rhoJointTilde_0);
axis equal
set(h,'LineStyle','none')
ylim([-3 3])
xlim([-3 3])
view(2)
aa = get(gca,'XTickLabel');
set(gca,'XTickLabel',aa,'FontName','Times','fontsize',35)
bb = get(gca,'YTickLabel');
set(gca,'YTickLabel',bb,'FontName','Times','fontsize',35)
title(['$\tilde{\rho}_{[u_0,v_0]}^{PDE} \sim N([0;' num2str(mFit,2) ']; [0.5, 0; 0,'  num2str(sigFit^2,2) '])$'], 'FontName','Times', 'fontsize',40,'interpreter','latex')
xlabel('$v_0$', 'fontsize', 40,'interpreter','latex')
ylabel('$u_0$','fontsize', 40,'interpreter','latex')

%% Computing the QoI with parameters obtained by PDE approach

h=zeros(1,M);
u=zeros(d,N,M);
u0=[mCond+sigCond.*randn(1,M); vSmpl];
r=exp(-kappa*dt);
sigOU=sqrt(2*kappa*Temp*(1-exp(-2*kappa*dt))/(2*kappa));
for m=1:M
    u=u0(:,m);
    for k=1:N
        u(2,k+1)=r*u(2,k)+sigOU*randn;
        u(2,k+1)=u(2,k+1)-dUdx(u(1,k))*dt;
        u(1,k+1)=u(1,k)+u(2,k+1)*dt;
    end
    h(m)=(max(P1*u(:,:))>=K);
end
Weight = rho_Gaussian(P1*u0,P1*mu,sqrt(P1*Sigma*P1'))./rho_Gaussian(P1*u0, mFit, sigFit);
alpha_hat_L2=mean(h.*Weight)
Vaprx_L2=var(h.*Weight)
RelError_L2=1.96/sqrt(alpha_hat_L2*M);
RelErrorIS_L2=1.96*sqrt(Vaprx_L2)/(alpha_hat_L2*sqrt(M))
varRatio_L2=(RelError_L2/RelErrorIS_L2)^2

%% MULTILEVEL CROSS-ENTROPY METHOD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  We consider only mean shifting but fixing the covariance
beta=0.01;  %CE method parameter
M=10^5;     %Monte Carlo sample size
N=100;     %Timestep size in SDE
% Multilevel procedure to find the optimal mu_tilde
Weight = ones(1,M);
sMbar=sort(Mbar);
K_ell=sMbar(1, ceil((1-beta)*M));
K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
h=(Mbar>=K_ell);
Sigma_tilde=Sigma;
ell=1;
u0=mu+chol(Sigma)'*randn(d,M);

while K_ell<K
    mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight);
    
    ell=ell+1;
    for m=1:M
        u(:,1,m)=mu_tilde+chol(Sigma_tilde)'*randn(d,1);
        u0(:,m)=u(:,1,m);
        for n=2:N+1
            u(2,n,m)=r*u(2,n-1,m)+sigOU*randn;
            u(2,n,m)=u(2,n,m)-dUdx(u(1,n-1,m))*dt;
            u(1,n,m)=u(1,n-1,m)+u(2,n,m)*dt;
        end
        Mbar(m)=max(P1*u(:,:,m));
    end
    sMbar=sort(Mbar);
    K_ell=sMbar(1, ceil((1-beta)*M));
    K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
    h=(Mbar>=K_ell);
    
    Weight=mvnpdf(u0', mu', Sigma)'./mvnpdf(u0',mu_tilde',Sigma_tilde)';
end

if K_ell==K
    mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight)
end

%%
rhoJoint_CE_0=(reshape(mvnpdf(xv,mu_tilde',Sigma_tilde),length(vPDE),length(xPDE)))'; 

figure(4)
h=surf(vPDE,xPDE,rhoJoint_CE_0);
axis equal
set(h,'LineStyle','none')
ylim([-3 3])
xlim([-3 3])
view(2)
aa = get(gca,'XTickLabel');
set(gca,'XTickLabel',aa,'FontName','Times','fontsize',35)
bb = get(gca,'YTickLabel');
set(gca,'YTickLabel',bb,'FontName','Times','fontsize',35)
title(['$\tilde{\rho}_{[u_0,v_0]}^{CE}\sim N([', num2str(mu_tilde(1),2), ';', num2str(mu_tilde(2),2), ']; [0.5, 0; 0,0.5])$'], 'FontName','Times', 'fontsize',40,'interpreter','latex')
xlabel('$v_0$', 'fontsize', 40,'interpreter','latex')
ylabel('$u_0$','fontsize', 40,'interpreter','latex')
%% Computing the QoI with parameters obtained by multilevel CE approach
mu_tilde=mu       %Crude MC setting to compare
M=10^5;     %Monte Carlo sample size
N=100;     %Timestep size in SDE
h=zeros(1,M);
u0=mu_tilde+chol(Sigma_tilde)'*randn(d,M);

for m=1:M
    u=u0(:,m); 
    for k=1:N
        u(2,k+1)=r*u(2,k)+sigOU*randn;
        u(2,k+1)=u(2,k+1)-dUdx(u(1,k))*dt;
        u(1,k+1)=u(1,k)+u(2,k+1)*dt;
    end
    h(m)=(max(P1*u(:,:))>=K);
end
Weight=mvnpdf(u0', mu', Sigma)'./mvnpdf(u0',mu_tilde',Sigma_tilde)';
alpha_hat_CE=mean(h.*Weight)
Vaprx_CE=var(h.*Weight)
RelError_CE=1.96/sqrt(alpha_hat_CE*M);
RelErrorIS_CE=1.96*sqrt(Vaprx_CE)/(alpha_hat_CE*sqrt(M))
varRatio_CE=(RelError_CE/RelErrorIS_CE)^2
