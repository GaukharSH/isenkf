clear; close all;
d=40;
F=8;
% d=3;         %problem dimension
% r=10;
% s=28;
% q=8/3;
% r=1;
% q=2;
% s=8/3;
T=0.01;
h=0.0001;
%initialize
sigma=0.5;
mu=zeros(d,1);
I=eye(d);
Sigma=0.005*I;
t=(0:h:T)';
u=zeros(d,length(t));
% F1=2;
% F2=6;
% F3=8;
% sigma1=0;
% sigma2=0;
% sigma3=0;
% v1=zeros(d,length(t));
% v2=zeros(d,length(t));
% v3=zeros(d,length(t));
P1   = I(1,:); 
% a = @(u) [r*(u(2,:,:)-u(1,:,:)); s*u(1,:,:)-u(2,:,:)-u(1,:,:).*u(3,:,:); u(1,:,:).*u(2,:,:)-q*u(3,:,:)]; %Lorenz 63 drift
% b=[sigma; sigma; sigma];
%b = [sigma; 0.5*ones(d-1,1)];
%v(:,1)=mu+chol(Sigma)'*randn(d,1); %choose initial condition
% [tt,u]=ode45(@(t,v) lorenz96(v,F), [0 T], v(:,1));
% 
% figure(1)
% plot(tt, u(:,1), 'b', 'LineWidth', 2)
% xlabel('t')
% ylabel('v_1')
%%
% error=sqrt(sum((v-u).^2,2));
% figure(2)
% semilogy(t, error, 'k','LineWidth', 2)
% xlabel('t')
% ylabel('error')
M=100;dt=h;
for m=1:M
     %v(:,1)=mu;
     v(:,1)=mu+chol(Sigma)'*randn(d,1); %choose initial condition
%      v1(:,1)=v(:,1);
%      v2(:,1)=v(:,1);
%      v3(:,1)=v(:,1);
 %   u(:,1)=[0;1;0]+chol(Sigma)'*randn(d,1);
for n=1:length(t)-1 %for each time
    v(:,n+1)=v(:,n)+lorenz96(v(:,n),F)*h+sigma*sqrt(h)*randn; %solved via RK4
%     v(:,n+1,m)=v(:,n)+rk4(v(:,n),h,F)+b.*sqrt(h).*randn(d,1);
%     v1(:,n+1)=v1(:,n)+lorenz96(v1(:,n),F1)*h+ sigma1*sqrt(h)*randn; %solved via RK4
%     v2(:,n+1)=v2(:,n)+lorenz96(v2(:,n),F2)*h+sigma2*sqrt(h)*randn; %solved via RK4
%     v3(:,n+1)=v3(:,n)+lorenz96(v3(:,n),F3)*h+sigma3*sqrt(h)*randn;
%       u(1,k+1)=u(1,k)+r*(u(2,k)-u(1,k))*dt;%+sigma*sqrt(dt)*randn;
%       u(2,k+1)=u(2,k)+(q*u(1,k)-u(2,k)-u(1,k)*u(3,k))*dt;
%       u(3,k+1)=u(3,k)+(u(1,k)*u(2,k)-s*u(3,k));
  %    u(:,n+1)=u(:,n)+lorenz63(u(:,n),r,q,s)*dt+b.*sqrt(dt).*randn(3,1);
      %u(:,n+1)=u(:,n)+rk4(u(:,n),dt,r,q,s)+b.*sqrt(dt).*randn(3,1);
end

% figure(5)
% plot(t, u(1,:)-40, 'r', t, u(2,:), 'b', t,u(3,:)+15, 'k', 'LineWidth', 2)
% xlabel('t', 'fontsize', 14)
% ylabel('u', 'fontsize', 14)
% hold on
% legend('u_1', 'u_2', 'u_3')
% title('\sigma=0, \rho=160')
figure(6)
plot(t, v(1,:),'LineWidth', 2)
hold on
end
%A=a(u);
% title('Noisy Lorenz 63')
% hold off
%
figure(7)
contourf(v)
colormap(jet)
title('$F=8,\sigma_1=7, \sigma_k=0.5, k=2,...,d$', 'interpreter', 'latex', 'fontsize',24)
xlabel('t', 'fontsize', 22)
ylabel('states', 'fontsize',28)
xticklabels({'5','10','15','20'})

% figure(6)
% subplot(1,3,1)
% contourf(v1)
% colormap(jet)
% title('$F=2,\sigma=0$', 'interpreter', 'latex', 'fontsize',24)
% xlabel('t', 'fontsize', 22)
% ylabel('states', 'fontsize',28)
% xticklabels({'5','10','15','20'})
% subplot(1,3,2)
% contourf(v2)
% colormap(jet)
% title('$F=4,\sigma=0$', 'interpreter', 'latex', 'fontsize',24)
% xlabel('t', 'fontsize', 22)
% xticklabels({'5','10','15','20'})
%ylabel('states', 'fontsize',22)
% subplot(1,3,3)
% contourf(v3)
% colormap(jet)
% title('$F=8,\sigma=0$', 'interpreter', 'latex', 'fontsize',24)
% xlabel('t', 'fontsize', 22)
% xticklabels({'5','10','15','20'})
%ylabel('states', 'fontsize',22)
% function dv = rk4(v_old,h,F)
% % v[t+1] = rk4(v[t],step)
%  k1 = lorenz96(v_old,F);
%  k2 = lorenz96(v_old+1/2.*h.*k1,F);
%  k3 = lorenz96(v_old+1/2.*h.*k2,F);
%  k4 = lorenz96(v_old+h.*k3,F);
%  dv= 1/6*h*(k1+2*k2+2*k3+k4);
% end

function dv = rk4(v_old,h,r,q,s)
% v[t+1] = rk4(v[t],step)
 k1 = lorenz63(v_old,r,q,s);
 k2 = lorenz63(v_old+1/2.*h.*k1,r,q,s);
 k3 = lorenz63(v_old+1/2.*h.*k2,r,q,s);
 k4 = lorenz63(v_old+h.*k3,r,q,s);
 dv= 1/6*h*(k1+2*k2+2*k3+k4);
end

function f=lorenz96(v,F)
f=[v(end); v(1:end-1)].*([v(2:end); v(1)]-[v(end-1:end);v(1:end-2)])-v+F*v.^0;
%f=-v+F;
end

function f=lorenz63(y,r,q,s)
f(1,1)=r*(y(2)-y(1));
f(2,1)=s*y(1)-y(2)-y(1)*y(3);
f(3,1)=y(1)*y(2)-q*y(3);
end
% function f=lorenz63(y,r,q,s)
% f(1,1)=r*(y(2)-y(1));
% f(2,1)=-r*y(1)-y(2)-y(1)*y(3);
% f(3,1)=y(1)*y(2)-s*y(3)-s*(r+q);
% end