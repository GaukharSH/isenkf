clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for BOOTSTRAPPING of Langevin 2D problem to check 
%%% the robustness of CE method and PDE method
%%% X - particle position, V -particle velocity 
%------- dX_t = dV_t dt -----------------------------------------------%
%------- dV_t = -U'(X_t)dt-kappa*V_tdt+(2*kappa*Temp)^1/2 dW_t --------%
%----------------------------------------------------------------------%
%%% computing QoI: P(max_{0<=t<=T} P1*X_t >K)
%Model parameters
d=6;        %problem dimension
F=2;         %forcing term
sigma=0.4;   %constant diffusion parameter
I=eye(d);

beta=0.01;%CE method parameter

a = @(v) [v(end,:,:); v(1:end-1,:,:)].*([v(2:end,:,:); v(1,:,:)]-[v(end-1:end,:,:);v(1:end-2,:,:)])-v+F*v.^0; %Lorenz 96 drift
b = sigma*I;   
%Define the Projector which component to track
P1   = I(1,:); 

%Simulation parameters
T=1;         %final time (simulation length)
M=10^4;       %Monte Carlo sample size
K=3.3;        %the threshold
N=1000;      %number of timesteps in SDE
dt=T/N;      %discretization step in SDE

%Initial original density parameters 
corr=0;
mu=zeros(d,1);
A=randn(d);
%Sigma=0.5*I;
Sigma=cov(A);

Krng=[3 3.3 3.5 3.7];                 %the threshold DW


%Initial density parameters in CE method
%P1=[0 1] parameters:
% mu_tilde_PDEv={[0; 0.926632071368393],[0; 1.035884487689959], [0; 1.153770758812496], [0; 1.280397887661908]};
% sigma_tilde_PDEv={[0.499791107452172 0; 0 0.259309474754189],[0.499791107452172   0;0   0.298236569331185], [0.499791107452171 0; 0 0.330502199760880], [0.499791107452172 0; 0 0.356816469234647]};
% mu_tilde_CEv={[-0.031182900453078; -0.045627870669489], [-0.282381870944081; 1.513470037616855],[-0.406150517384421; 1.841052107029484], [-0.680806205988338; 1.861601864786655]};
% sigma_tilde_CEv=repmat({Sigma}, 4,1);

%after fixing the bugs
%P1=[0 1] parameters:
mu_tilde_PDEv={[0; 0.9192], [0; 1.0758], [0; 1.2305], [0; 1.3859]};
sigma_tilde_PDEv={[0.5 0; 0 0.4085], [0.5   0;0   0.4124], [0.5 0; 0 0.4123], [0.5 0; 0 0.4118]};
mu_tilde_CEv={[-0.2815; 1.5578], [-0.3579; 1.8261],[-0.5331; 2.2694], [-0.5998; 2.5182]};
sigma_tilde_CEv=repmat({Sigma}, 4,1);

%P1=[1 0] parameters:
% mu_tilde_PDEv={[1.771484668340343; 0.005179045372664],[2.215045666641432; 0.005179045372664], [2.800650080239603; 0.005179045372664], [3.589164199060899; 0.005179045372664]};
% sigma_tilde_PDEv={[0.053124943995885 0; 0 0.489615020080776],[0.038211558538249   0;0   0.489615020080776], [0.026350717389011 0; 0 0.489615020080776], [0.018681971155761 0; 0 0.489615020080776]};
% mu_tilde_CEv={[0.042679496057375; -0.039537609884099], [2.430791207677585; 0.613730019987208],[2.853383163050362; 0.799923338163523], [3.292459018880470; 0.947846811434624]};
% sigma_tilde_CEv=repmat({Sigma}, 4,1);
C = {'r','m', 'g', 'k', 'y'}; % Cell array of colros.
Cc={'g','b'};
S=10^5;
conf=95;% in percent
B=10^4;
for n=1:2
    
    for i=1:length(Krng)
        K=Krng(i);
        h_PDE=zeros(1,S);
        h_CE=zeros(1,S);
        u_pde=zeros(d,N,S);
        u_ce=zeros(d,N,S);
        mu_tilde_PDE=mu_tilde_PDEv{i};
        sigma_tilde_PDE = sigma_tilde_PDEv{i};
        mu_tilde_CE=mu_tilde_CEv{i};
        sigma_tilde_CE=sigma_tilde_CEv{i};
        u0_PDE=mu_tilde_PDE+chol(sigma_tilde_PDE)'*randn(d,S);
        u0_CE=mu_tilde_CE+chol(sigma_tilde_CE)'*randn(d,S);
        
        for s=1:S
            u_pde=u0_PDE(:,s);
            u_ce=u0_CE(:,s);
            for k=1:N
                u_pde(:,k+1)=u_pde(:,k)+lorenz96(u_pde(:,k),F)*dt+sigma*sqrt(dt)*randn(d,1);
                u_ce(:,k+1)=u_ce(:,k)+lorenz96(u_ce(:,k),F)*dt+sigma*sqrt(dt)*randn(d,1);

            end

            h_PDE(s)=(max(P1*u_pde(:,:))>=K);
            h_CE(s)=(max(P1*u_ce(:,:))>=K);
        end
        Weight_PDE=mvnpdf(u0_PDE', mu', Sigma)'./mvnpdf(u0_PDE',mu_tilde_PDE',sigma_tilde_PDE)';
        Weight_CE=mvnpdf(u0_CE', mu', Sigma)'./mvnpdf(u0_CE',mu_tilde_CE',sigma_tilde_CE)';

        q_PDE=h_PDE.*Weight_PDE;
        q_CE=h_CE.*Weight_CE;
        
        if isempty(gcp)
            parpool;
        end
        opt = statset('UseParallel',true);
             
        stats_PDE = bootstrp(B, @(x) [mean(x) std(x)], q_PDE, 'Options', opt);
        stats_CE = bootstrp(B, @(x) [mean(x) std(x)], q_CE, 'Options', opt);

        SmplStd_PDE(i)=mean(stats_PDE(:,2));
        SmplStd_CE(i)=mean(stats_CE(:,2));
        
        CId_pde(i) = prctile(stats_PDE(:,2), (100-conf)/2);
        CIu_pde(i)=  prctile(stats_PDE(:,2), (100-(100-conf)/2));
        
        CId_ce(i) = prctile(stats_CE(:,2), (100-conf)/2);
        CIu_ce(i)=  prctile(stats_CE(:,2), (100-(100-conf)/2));
    end
    figure(1)
    plot_ci(Krng',[SmplStd_PDE; CId_pde; CIu_pde],'PatchColor', C{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', C{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', C{n}, 'YScale', 'log');
    hold on
    plot_ci(Krng',[SmplStd_CE; CId_ce; CIu_ce], 'PatchColor', Cc{n}, 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', Cc{n},'LineWidth', 0.5, 'LineStyle','--', 'LineColor', Cc{n}, 'YScale', 'log');
    grid on
    hold on
end
    xlabel('K')
    ylabel('sample std')
    legend('95% CI of PDE 1st run','PDE mean 1st run','','','95% CI of CE 1st run','CE mean 1st run','','','95% CI of PDE 2nd run', 'PDE mean 2nd run','','', '95% CI of CE 2nd run', 'CE mean2nd run')
%% Lorenz 96

Krng=[3 3.1 3.3 3.5]; 

SmplStd_PDE1=[0.042800339164230  0.005634791796339  9.844515892464174e-04 5.927482113839285e-04];
CId_pde1=[0.009390313749591   0.003645519687356    6.201777839343482e-04 2.083230779608770e-04];
CIu_pde1=[0.078575848221704  0.003833641263296+0.003645519687356  0.001364068809485 9.507325061749775e-04];

SmplStd_PDE2=[0.006907618181482  0.006937442169867     0.001284001943342 3.163970732743842e-04];
CId_pde2=[0.005336371612166   0.004371439227941    8.497989068676041e-04 1.796325897937227e-04];
CIu_pde2=[0.008578810544938   0.009967651693408  0.001712167038253 4.481512879935792e-04];

SmplStd_CE1=[0.004963662740646  0.003351659966565   6.569617034235530e-04 1.599335142972116e-04];
CId_ce1=[0.003537969075385  0.001724062651360     4.872177965592547e-04 1.143618716773757e-04];
CIu_ce1=[0.006654550062573  0.005557703914656     8.388368799761272e-04 2.105278144056329e-04];

SmplStd_CE2=[0.004477142211861   0.002305754967843  7.745010544209989e-04 2.719430079177214e-04];
CId_ce2=[0.003578510092258  0.001914672205150  5.080706396546531e-04 1.777325807538349e-04];
CIu_ce2=[0.005420743590901   0.002731101792173   0.001117109255665 3.683355554236508e-04];

figure(1)
plot_ci(Krng,[SmplStd_PDE1; CId_pde1; CIu_pde1],'PatchColor', 'r', 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', 'r','LineWidth', 0.5, 'LineStyle','--', 'LineColor', 'r', 'YScale', 'log');
hold on
plot_ci(Krng,[SmplStd_PDE2; CId_pde2; CIu_pde2],'PatchColor', 'm', 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', 'm','LineWidth', 0.5, 'LineStyle','--', 'LineColor', 'm', 'YScale', 'log');
hold on
plot_ci(Krng,[SmplStd_CE1; CId_ce1; CIu_ce1], 'PatchColor', 'g', 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', 'g','LineWidth', 0.5, 'LineStyle','--', 'LineColor', 'g', 'YScale', 'log');
hold on
plot_ci(Krng,[SmplStd_CE2; CId_ce2; CIu_ce2], 'PatchColor', 'b', 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', 'b','LineWidth', 0.5, 'LineStyle','--', 'LineColor', 'b', 'YScale', 'log');
grid on
hold off
xlabel('K')
ylabel('sample std')
legend('95% CI of PDE 1st run','PDE mean 1st run','','','95% CI of PDE 2nd run','PDE mean 2nd run','','','95% CI of CE 1st run', 'CE mean 1st run','','', '95% CI of CE 2nd run', 'CE mean2nd run')

%% Lorenz 63

Krng=[2 2.3 2.7 3.2]; 

SmplStd_PDE1=[0.011243580731495  0.002522856378647  2.859647582761669e-04 1.438959837742187e-05];
CId_pde1=[0.010014220131909   0.002331087259012    2.776094758013967e-04 1.406154980447664e-05];
CIu_pde1=[0.012693615863425  0.002747437024934  2.956602627846356e-04 1.473506403125143e-05];

SmplStd_PDE2=[0.016320356199760  0.002679737013136     3.598971650432271e-04 1.437226747344083e-05];
CId_pde2=[0.010354214861303   0.002316556125230    2.887671209597313e-04 1.403169926773089e-05];
CIu_pde2=[0.024747006141627   0.003107142479709  4.671253650592805e-04 1.473416662652978e-05];

SmplStd_CE1=[0.014624869593353  0.002838681993880   2.680643604422563e-04 9.204362878060869e-06];
CId_ce1=[0.009228379159896  0.001865627591750     1.891640323805823e-04 8.803428814270015e-06];
CIu_ce1=[0.006654550062573  0.004008824840418     3.933126301742914e-04 9.667721232829389e-06];

SmplStd_CE2=[0.009618004334809   0.003085155095070  2.063977715914781e-04 3.337589559350476e-05];
CId_ce2=[0.008241484521659  0.001897267305117  1.917932559131006e-04 3.107832965548481e-05];
CIu_ce2=[0.011356884068317   0.004522047480078   2.238214142202924e-04 3.667752009208649e-05];

figure(1)
plot_ci(Krng,[SmplStd_PDE1; CId_pde1; CIu_pde1],'PatchColor', 'r', 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', 'r','LineWidth', 0.5, 'LineStyle','--', 'LineColor', 'r', 'YScale', 'log');
hold on
plot_ci(Krng,[SmplStd_PDE2; CId_pde2; CIu_pde2],'PatchColor', 'm', 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', 'm','LineWidth', 0.5, 'LineStyle','--', 'LineColor', 'm', 'YScale', 'log');
hold on
plot_ci(Krng,[SmplStd_CE1; CId_ce1; CIu_ce1], 'PatchColor', 'g', 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', 'g','LineWidth', 0.5, 'LineStyle','--', 'LineColor', 'g', 'YScale', 'log');
hold on
plot_ci(Krng,[SmplStd_CE2; CId_ce2; CIu_ce2], 'PatchColor', 'b', 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', 'b','LineWidth', 0.5, 'LineStyle','--', 'LineColor', 'b', 'YScale', 'log');
grid on
hold off
xlabel('K')
ylabel('sample std')
legend('95% CI of PDE 1st run','PDE mean 1st run','','','95% CI of PDE 2nd run','PDE mean 2nd run','','','95% CI of CE 1st run', 'CE mean 1st run','','', '95% CI of CE 2nd run', 'CE mean2nd run')

%% Lorenz 96 with F=0, d=10

Krng=[2 2.5 3 3.3]; 

SmplStd_PDE1=[0.005042260066046  4.512227810090733e-04  1.829080030859747e-05 2.819016214676679e-06];
CId_pde1=[0.004762700160994   3.606291433332894e-04    1.756251247750838e-05 2.286543019258897e-06];
CIu_pde1=[0.005404647159633  5.989932819282463e-04  1.944240777444581e-05 3.730690910491042e-06];

SmplStd_PDE2=[0.007255527170182  3.894750412482309e-04     1.888216055796271e-05 2.385431777850017e-06];
CId_pde2=[0.004583453556648   3.660718018253646e-04    1.812422942003526e-05 2.293348454744075e-06];
CIu_pde2=[0.011316837239281   4.224595303067822e-04  1.981266211271130e-05 2.509966675846678e-06];

SmplStd_CE1=[0.006393849266093  6.349164142810872e-04   3.160498719044022e-05 4.407714302858240e-06];
CId_ce1=[0.006001323782260  5.567243087003138e-04     3.068191415310166e-05 4.327083485413533e-06];
CIu_ce1=[0.006949152700644  7.639845021350979e-04     3.264215238532471e-05 4.492847187495941e-06];

SmplStd_CE2=[0.006621228249104   5.841261041605407e-04  3.196376154677186e-05 4.520421987132114e-06];
CId_ce2=[0.006145424237502  5.553296314908559e-04  3.087327332004539e-05 4.369187973274650e-06];
CIu_ce2=[0.007332424040373   6.189327832291534e-04   3.322331454510789e-05 4.728962636479804e-06];

figure(1)
plot_ci(Krng,[SmplStd_PDE1; CId_pde1; CIu_pde1],'PatchColor', 'r', 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', 'r','LineWidth', 0.5, 'LineStyle','--', 'LineColor', 'r', 'YScale', 'log');
hold on
plot_ci(Krng,[SmplStd_PDE2; CId_pde2; CIu_pde2],'PatchColor', 'm', 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', 'm','LineWidth', 0.5, 'LineStyle','--', 'LineColor', 'm', 'YScale', 'log');
hold on
plot_ci(Krng,[SmplStd_CE1; CId_ce1; CIu_ce1], 'PatchColor', 'g', 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', 'g','LineWidth', 0.5, 'LineStyle','--', 'LineColor', 'g', 'YScale', 'log');
hold on
plot_ci(Krng,[SmplStd_CE2; CId_ce2; CIu_ce2], 'PatchColor', 'b', 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', 'b','LineWidth', 0.5, 'LineStyle','--', 'LineColor', 'b', 'YScale', 'log');
grid on
hold off
xlabel('K')
ylabel('sample std')
legend('95% CI of PDE 1st run','PDE mean 1st run','','','95% CI of PDE 2nd run','PDE mean 2nd run','','','95% CI of CE 1st run', 'CE mean 1st run','','', '95% CI of CE 2nd run', 'CE mean2nd run')

%% Lorenz 96 with F=1, d=40

Krng=[3.2 3.8 4.2 4.5]; 

SmplStd_PDE1=[0.006904015563592  4.323640391570437e-04  5.920444075281821e-05 1.588743149507219e-05];
CId_pde1=[0.005050824521283   3.674176042677206e-04    5.608987474415475e-05 1.378644218833932e-05];
CIu_pde1=[0.008872377799279  5.122512116536733e-04  6.265465836865243e-05 1.896552909667927e-05];

SmplStd_PDE2=[0.007752767141561  7.971694907485676e-04     6.511705976414411e-05 2.071266178299799e-05];
CId_pde2=[0.005065723471498   3.783718686691836e-04    5.757134134972524e-05 1.543395648591388e-05];
CIu_pde2=[0.010616152620691   0.001375185617248  7.392208626705380e-05 2.634477398208434e-05];

SmplStd_CE1=[0.008225804654820  0.001023305784096   2.153034105206110e-04 1.573695736300979e-05];
CId_ce1=[0.004383563339306  2.925372950245613e-04     5.746056927851731e-05 1.354140969814346e-05];
CIu_ce1=[0.013219944946764  0.001913911698505     3.901884550295935e-04 1.865104924666832e-05];

SmplStd_CE2=[0.013331425827583   4.012066742130965e-04  1.021467550200029e-04 1.705695824251108e-05];
CId_ce2=[0.005737281845784  3.212627031885790e-04  5.977158200172440e-05 1.412768546776191e-05];
CIu_ce2=[0.020176947136214   4.927554310919075e-04   1.489879782780887e-04 2.048586800010817e-05];

figure(1)
plot_ci(Krng,[SmplStd_PDE1; CId_pde1; CIu_pde1],'PatchColor', 'r', 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', 'r','LineWidth', 0.5, 'LineStyle','--', 'LineColor', 'r', 'YScale', 'log');
hold on
plot_ci(Krng,[SmplStd_PDE2; CId_pde2; CIu_pde2],'PatchColor', 'm', 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', 'm','LineWidth', 0.5, 'LineStyle','--', 'LineColor', 'm', 'YScale', 'log');
hold on
plot_ci(Krng,[SmplStd_CE1; CId_ce1; CIu_ce1], 'PatchColor', 'g', 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', 'g','LineWidth', 0.5, 'LineStyle','--', 'LineColor', 'g', 'YScale', 'log');
hold on
plot_ci(Krng,[SmplStd_CE2; CId_ce2; CIu_ce2], 'PatchColor', 'b', 'PatchAlpha', 0.1, 'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', 'b','LineWidth', 0.5, 'LineStyle','--', 'LineColor', 'b', 'YScale', 'log');
grid on
hold off
xlabel('K')
ylabel('sample std')
legend('95% CI of PDE 1st run','PDE mean 1st run','','','95% CI of PDE 2nd run','PDE mean 2nd run','','','95% CI of CE 1st run', 'CE mean 1st run','','', '95% CI of CE 2nd run', 'CE mean2nd run')