function [x]=nonuniform(N,xc,alpha, z, xmax, xmin)
%% non uniform grids
%N=100;
%xc=0.001;
%z=linspace(0,1,N);
%xmax=1;
%xmin=0;
%alpha=0.5;
beta=alpha*(xmax-xmin);
delta=asinh((xmin-xc)/beta);
gamma=asinh((xmax-xc)/beta)-delta;
x=xc+beta*sinh(gamma*z+delta);