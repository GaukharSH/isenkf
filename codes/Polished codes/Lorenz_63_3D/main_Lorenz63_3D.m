clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for Lorenz 96 problem to test CE method and PDE method
%%% in performing IS technique: 
%-- dv1 = r(v2-v1)+sigma*dW ---------------%
%-- dv2 = sv1-v2-v1v3   ---------------%
%-- dv3 = v1v2-qv3      ---------------%
%-------------------------------------------------------------------------%
%%% in computing QoI: P(max_{0<=t<=T} P1*v(t) >K)

%Model parameters
d=3;         %problem dimension

%This setting is from paper by Zuev et.al, also chaotic regime
%sigma=3;    %constant diffusion parameter
% r=3;
% q=26;
% s=1;

% This setting gives chaotic regime with added noice
sigma=0.1;
r=10;
q=28;
s=8/3;

%In this parameter seeting, IS works
% sigma=0.5;
% r=1;
% q=2;
% s=8/3;

I=eye(d);
a = @(u) [r*(u(2,:,:)-u(1,:,:)); q*u(1,:,:)-u(2,:,:)-u(1,:,:).*u(3,:,:); u(1,:,:).*u(2,:,:)-s*u(3,:,:)]; %Lorenz 63 drift
b = [sigma; 0; 0];   

%Simulation parameters
T=1;         %final time (simulation length)
M=10^5       %Monte Carlo sample size
K=12        %the threshold
N=1000;       %number of timesteps in SDE
dt=T/N;      %discretization step in SDE

%Initial original density parameters 
mu=zeros(d,1);
Sigma=0.5*I;
% rho=0.3;
% Sigma = 0.1*(1-rho)*I+0.1*rho*ones(d,d);
%Define the Projector which component to track
P1   = I(1,:); 

%Generate samples of Lorenz 96 dynamics to use for both CE and L2 regression
u=zeros(d,N,M);
Mbar=zeros(1,M);
for m=1:M
    %u(:,1,m)=mu+chol(Sigma)'*randn(d,1); 
    u(:,1,m)=[5 5 25]+0.5*[1 1 1];
    for n=1:N
        u(1,n+1,m)=u(1,n,m)+r*(u(2,n,m)-u(1,n,m))*dt+sigma*sqrt(dt)*randn;
        u(2,n+1,m)=u(2,n,m)+(q*u(1,n,m)-u(2,n,m)-u(1,n,m)*u(3,n,m))*dt;
        u(3,n+1,m)=u(3,n,m)+(u(1,n,m)*u(2,n,m)-s*u(3,n,m))*dt;
    %u(:,n+1,m)=u(:,n,m)+lorenz63(u(:,n,m),r,q,s)*dt+b.*sqrt(dt).*randn(3,1);
    end
    Mbar(m)=max(P1*u(:,:,m));
end
%% L2 regression
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We approximate only the drift, the diffusion is constant

%Define the Polynomial space
w = 2;                                                         %Polynomial degree
p = poly_space(w,'TD');                                        %Polynomial space
p_dim = size(p, 1);                                            %Cardinality of polynomial space

t  = linspace(0,T,N+1)';                                       %Generating t_0, t_1, ..., t_{N-1}
Tt = reshape(repmat(t,1,M)', (N+1)*M,1);                       %Replicating M times each t_n and saving as a column vector [t_0 t_0 ... t_0, t_1, ..., t_1,..., t_{N-1}, ..., t_{N_1}]'
Xx = reshape(squeeze(pagemtimes(P1,u))', (N+1)*M,1);           %Projected Samples saved as a column vector [X_0^(1),..., X_0^(M), X_1^(1),..,X_1^(M), ..., X_{N-1}^(1),...,X_{N-1}^(M)]
tX = [Tt Xx];                                                  %Two column vectors of size N*M for [t_n, X_{t_n}]
f  = reshape(squeeze(pagemtimes(P1,a(u)))',(N+1)*M,1);         %Given f function, we are going to solve

D     = x2fx(tX, p);                                           %This function helps to create Psi matrix each column is non-orth. basis function psi_p for different given p
[Q,R] = modified_GS(D);                                        %Apply modified Gram-Schmidt process to get QR decomposition
a_p   = Q'*f;                                                  %Solve Normal equations based on orthonormalised basis

psy     = @(t,s) x2fx([t s], p);                               %Non-orth. basis function
psy_bar = @(t,s) x2fx([t s], p)/R;                             %Orth. basis function
a_bar   = @(t,s) psy_bar(t,s)*a_p;                             %Approximation to the drift function a(x)
b_bar   = @(s)   sqrt(P1*(b*b')*P1')*ones(size(s));            %Approximation to the diffusion function b(x)

%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = -3;                         % Lower end of discretization interval
xU = K;                          % Upper end of discretization interval
dxPde = 0.005;                   % Space step in PDE    
dtPde = dxPde^2;                 % Time step in PDE
Nx = round((xU-xL)/dxPde);       % Number of space steps in PDE
Nt = round(T/dtPde);             % Number of time steps in PDE
x  = linspace(xL,xU,Nx+1)';      % Space grid points

%[~,~,vErik] = tabulate_IC(a,b,dt,K,xL,xU,false,Nx+1); %KBE solution at T-dt obtained via numerical optimization (using Erik's code)
%Factor in exp.smoothing depends on EM timestep dt, based on numerical
%tests using Erik's solution, we observe the following settings
%dt=0.1, smf=15; dt=0.01, smf=40; dt=0.001, smf=100
smf=40;

%KBE solution for all time
[tW, xW, v] = KBEsolverForAllt(x,T,a_bar,b_bar,Nt,Nx,K,smf); %PDE solver via CN backward scheme using exponential smoothing of Ind.Fun in a final condition

%HJB solution for all time
%[tW, xW, u] = HJBsolverForAllt_CN(x,T,a_bar,b_bar,Nt,Nx,K,smf);
%v=exp(-u);
%Plot PDE solution at time 0
figure(1)
plot(xW, v(:,end), '-b', 'Linewidth',2)
xlabel('s', 'fontsize', 14)
ylabel('u(s,0)','fontsize', 14)

%% rho_0 CONTROL
rho_x0 = @(x, m, sig) exp(-(x-m).^2/(2*sig^2))/(sqrt(2*pi*sig^2)); %1D Gaussian density
rhoTilde_x0 = [rho_x0(xW, P1*mu, sqrt(P1*Sigma*P1')).*v(:,end); rho_x0((K:dxPde:K+100*dxPde)', P1*mu, sqrt(P1*Sigma*P1'))];
NC = trapezoidal(rhoTilde_x0, dxPde); %normalising constant
rhoTilde_x0=rhoTilde_x0./NC;          %optimal IS density

%fit optimal IS denisty to Gaussian
x=[xW;(K:dxPde:K+100*dxPde)'];
mFit = trapezoidal(x.*rhoTilde_x0, dxPde)
sigFit = sqrt(trapezoidal(x.^2.*rhoTilde_x0, dxPde)-mFit^2)
rhoFit_x0 = rho_x0(x, mFit, sigFit);

%Plot the densities
figure(1)
plot(x, rho_x0(x, P1*mu, sqrt(P1*Sigma*P1')), '-r',x, rhoTilde_x0, '-b',x, rhoFit_x0, '--b', 'Linewidth',3)
hold on
y = ylim; % current y-axis limits
plot([K K],[y(1) y(2)], '-k', 'LineWidth', 2)
hold off
title('DW process',  'fontsize',22)
xlabel('x','fontsize',18)
legend({['$\rho_{x_0} \sim N($' num2str(P1*mu), ', ',num2str(sqrt(P1*Sigma*P1')) ')'];'$ \tilde{\rho}_{x_0}^{PDE}$'; ['$\tilde{\rho}_{x_0}^{fit}\sim N$(' num2str(mFit,3) ', ' num2str(sigFit,2) ')']; ['K=' num2str(K)]},'fontsize',22,'interpreter','latex')

%%
for n=1:2
M=10^5;
%Conditional Gaussian rho_0(v_2,...,v_d|v_1) 
v1Smpl=mFit+sigFit*randn(1,M);
% mCond=mu(2:end)+corr*(v1Smpl-P1*mu)/sqrt(P1*Sigma*P1');
% sigCond=(1-corr^2)*Sigma(2:end,2:end);
mCond = mu(2:end)+ Sigma(2:d,1)/Sigma(1,1)*(v1Smpl-mu(1));
sigCond = Sigma(2:d, 2:d) - Sigma(2:d,1)/Sigma(1,1)*Sigma(1,2:d);
%% Computing the QoI with parameters obtained by PDE approach
h=zeros(1,M);
u=zeros(d,N);
u0=[v1Smpl; mCond+chol(sigCond)'*randn(d-1,M)];

parfor m=1:M
    u=u0(:,m);   
    for k=1:N
          u(:,k+1)=u(:,k)+lorenz63(u(:,k),r,q,s)*dt+b.*sqrt(dt).*randn(3,1);
    end
    h(m)=(max(P1*u)>=K);
end
Weight = rho_x0(P1*u0,P1*mu,sqrt(P1*Sigma*P1'))./rho_x0(P1*u0, mFit, sigFit);
alpha_hat_L2=mean(h.*Weight)
Vaprx_L2=var(h.*Weight)
RelError_L2=1.96/sqrt(alpha_hat_L2*M)
RelErrorIS_L2=1.96*sqrt(Vaprx_L2)/(alpha_hat_L2*sqrt(M))
varRatio_L2=(RelError_L2/RelErrorIS_L2)^2

%% Bootstrapping for PDE
if isempty(gcp)
    parpool;
end
opt = statset('UseParallel',true);
B=10^4;
conf=95;% in percent
stats_PDE = bootstrp(B, @(x) [mean(x) std(x)], h.*Weight, 'Options', opt);
SmplStd_PDE=mean(stats_PDE(:,2))
CId_pde = prctile(stats_PDE(:,2), (100-conf)/2)
CIu_pde = prctile(stats_PDE(:,2), (100-(100-conf)/2))
confIntlength=CIu_pde-CId_pde
end
%pause()
%% MULTILEVEL CROSS-ENTROPY METHOD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  We consider only mean shifting but fixing the covariance
beta=0.01;     %CE method parameter
M=10^5;
% Multilevel procedure to find the optimal mu_tilde
Weight = ones(1,M);
sMbar=sort(Mbar);
K_ell=sMbar(1, ceil((1-beta)*M));
K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
h=(Mbar>=K_ell);
Sigma_tilde=Sigma;
ell=1;
u0=mu+chol(Sigma)'*randn(d,M);

while K_ell<K
    mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight);
    
    ell=ell+1;
    
    for m=1:M
        u(:,1,m)=mu_tilde+chol(Sigma_tilde)'*randn(d,1);
        u0(:,m)=u(:,1,m);
        for n=2:N+1
%            u(1,n+1,m)=u(1,n,m)+r*(u(2,n,m)-u(1,n,m))*dt+sigma*sqrt(dt)*randn;
%            u(2,n+1,m)=u(2,n,m)+(q*u(1,n,m)-u(2,n,m)-u(1,n,m)*u(3,n,m))*dt;
%            u(3,n+1,m)=u(3,n,m)+(u(1,n,m)*u(2,n,m)-s*u(3,n,m)); 
             u(:,n,m)=u(:,n-1,m)+lorenz63(u(:,n-1,m),r,q,s)*dt+b.*sqrt(dt).*randn(3,1);
        end
        Mbar(m)=max(P1*u(:,:,m));
    end

    sMbar=sort(Mbar);
    K_ell=sMbar(1, ceil((1-beta)*M));
    K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
    h=(Mbar>=K_ell);
  
    Weight=mvnpdf(u0', mu', Sigma)'./mvnpdf(u0',mu_tilde',Sigma_tilde)';
end

if K_ell==K
    mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight)
end
% Computing the QoI with parameters obtained by multilevel CE approach
%mu_tilde=mu       %Crude MC setting to compare
for n=1:1
M=10^5;
h=zeros(1,M);
u0=mu_tilde+chol(Sigma_tilde)'*randn(d,M);

parfor m=1:M
    u=u0(:,m);  
    for k=1:N
%         u(1,k+1)=u(1,k)+r*(u(2,k)-u(1,k))*dt+sigma*sqrt(dt)*randn;
%         u(2,k+1)=u(2,k)+(q*u(1,k)-u(2,k)-u(1,k)*u(3,k))*dt;
%         u(3,k+1)=u(3,k)+(u(1,k)*u(2,k)-s*u(3,k));  
        u(:,k+1)=u(:,k)+lorenz63(u(:,k),r,q,s)*dt+b.*sqrt(dt).*randn(3,1);
    end    
    h(m)=(max(P1*u)>=K);
end
Weight=mvnpdf(u0', mu', Sigma)'./mvnpdf(u0',mu_tilde',Sigma_tilde)';
alpha_hat_CE=mean(h.*Weight)
Vaprx_CE=var(h.*Weight)
RelError_CE=1.96/sqrt(alpha_hat_CE*M)
RelErrorIS_CE=1.96*sqrt(Vaprx_CE)/(alpha_hat_CE*sqrt(M))
varRatio_CE=(RelError_CE/RelErrorIS_CE)^2

%% Bootstrapping for CE
if isempty(gcp)
    parpool;
end
opt = statset('UseParallel',true);
B=10^4;
conf=95;% in percent
stats_CE = bootstrp(B, @(x) [mean(x) std(x)], h.*Weight, 'Options', opt);
SmplStd_CE=mean(stats_CE(:,2))
CId_ce = prctile(stats_CE(:,2), (100-conf)/2)
CIu_ce=  prctile(stats_CE(:,2), (100-(100-conf)/2))
confIntlength=CIu_ce-CId_ce
end