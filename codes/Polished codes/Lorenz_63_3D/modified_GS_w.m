function [Q,R] =  modified_GS_w(D,w)
    [n,p] = size(D);
    Q = zeros(n,p);
    R = zeros(p,p);
    for k = 1:p
        Q(:,k) = D(:,k);
        for i = 1:k-1
            R(i,k) = Q(:,i)'*Q(:,k);
            Q(:,k) = Q(:,k) - R(i,k)*Q(:,i);
        end
        R(k,k) = norm(w.*Q(:,k))';
        Q(:,k) = Q(:,k)/R(k,k);
    end
end