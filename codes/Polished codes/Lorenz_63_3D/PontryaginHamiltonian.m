%clc; clear; close all
%Ponttryagin for Lorenz 63
%Model parameters 
d=3;            %problem dimension
sigma=3;      %constant diffusion parameter
r=10;
q=8/3;
s=28;

a = @(u) [r*(u(2,:,:)-u(1,:,:)); s*u(1,:,:)-u(2,:,:)-u(1,:,:).*u(3,:,:); u(1,:,:).*u(2,:,:)-q*u(3,:,:)]; %Lorenz 63 drift
b = [sigma;sigma;sigma]; 
I=eye(d);
K=16;
smf=15;
T=1;
N=1000
dt=T/N;
mu_0=[5 5 25]';
sigma_0=0.0025*I;
M=10^0;
X0v=mu_0+chol(sigma_0)'*randn(d,M);
%X0v=mu_0;
%X0v=linspace(0,K,M);
P1=I(1,:);
t=linspace(0,T,N+1);
ksi=zeros(N+1,M);
traj=zeros(N+1,M);
for i=1:M
    tol=1;
   X=zeros(3,N+1);
   X(:,1)=X0v(:,i);
   lambda=zeros(3,N+1);
   lambda(:,N+1)=-smf;
for n=1:N
    X(:,n+1)=X(:,n)+dt*(a(X(:,n))-sigma^2*lambda(:,n+1));
end
  tau_K_indx=find(P1*X>=K);
  if isempty(tau_K_indx)==1
      NtauOld=N;
  else
     NtauOld=min(tau_K_indx(1),N);
  end
 % NtauOld
 %pause()
    lambdaOld=zeros(3,NtauOld);
    lambdaOld(:,NtauOld)=-smf;
    lambdaOld(1,1:NtauOld-1)=-smf*exp(-10*(t(NtauOld)-t(1:NtauOld-1)));
    lambdaOld(2,1:NtauOld-1)=-smf*exp(-(t(NtauOld)-t(1:NtauOld-1)));
    lambdaOld(3,1:NtauOld-1)=-smf*exp(-8/3*(t(NtauOld)-t(1:NtauOld-1)));
while tol>1e-6
    Xold=X;
    %forward iteration
    for n=1:NtauOld-1
        X(:,n+1)=X(:,n)+dt*(a(X(:,n))-sigma^2*lambdaOld(:,n+1));
    end
    tau_K_indx=find(P1*X>=K);
      if isempty(tau_K_indx)==1
         Ntau=N;
      else
         Ntau=min(tau_K_indx(1),N);
      end
    %  Ntau
    lambda=zeros(3,Ntau);
    lambda(:,Ntau)=-smf;
    lambda(1,1:Ntau-1)=-exp(-10*(t(Ntau)-t(1:Ntau-1)));
    lambda(2,1:Ntau-1)=-smf*exp(-(t(Ntau)-t(1:Ntau-1)));
    lambda(3,1:Ntau-1)=-smf*exp(-8/3*(t(Ntau)-t(1:Ntau-1)));

    Ntaumin=min(Ntau,NtauOld);
    tol=norm(X(:,1:Ntaumin)-Xold(:,1:Ntaumin));
    lambdaOld=lambda;
    NtauOld=Ntau;
end
control=zeros(3,N+1);
control(:,1:Ntau)=-sigma*lambda;
% figure
% plot(t, control(1,1:end-1), 'LineWidth', 4)
% pause()
ksi(:,i)=control(1,:);
traj(:,i)=X(1,:);
end

figure(1)
plot(0:dt:t(Ntaumin), X(1,1:Ntaumin), 'r-', 'LineWidth', 4)
figure(2)
plot(0:dt:t(Ntaumin), control(1,1:Ntaumin)','r-', 'LineWidth', 4)

%%
figure(10)
hold on
subplot(1,4,1)
plot(0:dt:T, mean(traj,2),'r', 'LineWidth', 2)
xlabel('t')
ylabel('mean')
subplot(1,4,2)
plot(0:dt:T, std(traj,1,2),'r','LineWidth', 2)
xlabel('t')
ylabel('std')
subplot(1,4,3)
plot(0:dt:T, skewness(traj,1,2),'r','LineWidth', 2)
xlabel('t')
ylabel('skewness')
subplot(1,4,4)
plot(0:dt:T, kurtosis(traj,1,2),'r','LineWidth', 2)
xlabel('t')
ylabel('kurtosis')
%%
for k=1:M
    for l=1:length(trajStoch{1,k})
    trajL63(l,k)=trajStoch{1,k}(l);
    end
end
figure(10)
subplot(1,4,1)
hold on
plot(0:dt:T, mean(trajL63,2),'b', 'LineWidth', 2)
xlabel('t')
ylabel('mean')
subplot(1,4,2)
hold on
plot(0:dt:T, std(trajL63,1,2),'b','LineWidth', 2)
xlabel('t')
ylabel('std')
subplot(1,4,3)
hold on
plot(0:dt:T, skewness(trajL63,1,2),'b','LineWidth', 2)
xlabel('t')
ylabel('skewness')
subplot(1,4,4)
hold on
plot(0:dt:T, kurtosis(trajL63,1,2),'b','LineWidth', 2)
xlabel('t')
ylabel('kurtosis')

%%
for k=1:78752
    for l=1:length(trajStochM{1,k})
    trajL63M(l,k)=trajStochM{1,k}(l);
    end
end
%%
figure(10)
subplot(1,4,1)
hold on
plot(0:dt:T, mean(trajL63M,2),'g', 'LineWidth', 2)
xlabel('t')
ylabel('mean')
subplot(1,4,2)
hold on
plot(0:dt:T, std(trajL63M,1,2),'g','LineWidth', 2)
xlabel('t')
ylabel('std')
subplot(1,4,3)
hold on
plot(0:dt:T, skewness(trajL63M,1,2),'g','LineWidth', 2)
xlabel('t')
ylabel('skewness')
subplot(1,4,4)
hold on
plot(0:dt:T, kurtosis(trajL63M,1,2),'g','LineWidth', 2)
xlabel('t')
ylabel('kurtosis')
%% Using control based on time only
ksiStar = @(t) interp1(0:dt:T, control(1,:),t);

M=10^4; %MC sample size

%Initialization
h_IS=zeros(1,M);
h_MC=zeros(1,M);
u_MC=zeros(d,N+1);
u_IS=u_MC;
%u0=X0v;
%u0=mu*ones(1,M);%+chol(Sigma)'*randn(d,S);
%u0=mu_0+chol(sigma_0)'*randn(d,M);
%u0_shifted=[vSmpl; mCond+chol(SigmaCond)'*randn(d-1,M)];
%u0_shifted=u0;
t=linspace(0,T,N+1);
meanL=0;
parfor m=1:M
%        u_MC=u0(:,m);
%        u_IS=u0_shifted(:,m);
       u_MC=mu_0;
       u_IS=mu_0;
      L=1;
      k=1;
    while k<=N && P1*u_IS(:,k)<K
  
    dW = sqrt(dt).*randn(d,1);
    %Lorenz63 
    %u_IS(:,k+1)= u_IS(:,k)+rk4(u_IS(:,k),dt,r,q,s)+P1'.*b.*ksi(P1*u_IS(:,k),t(k))*dt+b.*dW; %Runge-Kutta
    %u_IS(:,k+1)= u_IS(:,k)+(lorenz63(u_IS(:,k),r,q,s)+P1'.*b.*ksi(P1*u_IS(:,k),t(k)))*dt+b.*dW; %Forward Euler 
    u_IS(:,k+1)= u_IS(:,k)+(lorenz63(u_IS(:,k),r,q,s)+P1'.*b.*ksiStar(t(k)))*dt+b.*dW; %Forward Euler 

    L=L*exp(-0.5*dt*ksiStar(t(k))^2-ksiStar(t(k))*P1*dW);
    
    k=k+1;
    end

    %Monte Carlo simulation
    for n=1:N
         dW = sqrt(dt).*randn(d,1);
         u_MC(:,n+1)= u_MC(:,n)+lorenz63(u_MC(:,n),r,q,s)*dt+b.*dW;
    end

    meanL=meanL+L;
    h_IS(m)=(max(P1*u_IS)>=K)*L;
    h_MC(m)=(max(P1*u_MC)>=K);
end
meanL=meanL/M
%Weight = rho_x0(P1*u0_shifted,P1*mu,sqrt(P1*Sigma*P1'))./rho_x0(P1*u0_shifted, mFit, sigFit);
%Weight = rho_x0(P1*u0_shifted,P1*mu,sqrt(P1*Sigma*P1'))./rho_x0(P1*u0_shifted, mFit, sqrt(P1*Sigma*P1'));
Weight = ones(1,M);
alpha_hat_IS_W=mean(h_IS.*Weight)
%alpha_hat_IS_W1=mean(h_IS1.*exp(expDist-mean(expDist)))*exp(mean(expDist))
alpha_hat_MC=mean(h_MC)
Vaprx_IS_W=var(h_IS.*Weight)
%Vaprx_IS_W1=var(h_IS1.*exp(expDist-mean(expDist)))*exp(mean(expDist))^2
Vaprx_MC=var(h_MC)
RelError_MC=1.96*sqrt(1-alpha_hat_IS_W)/sqrt(alpha_hat_IS_W*M)
RelError_IS=1.96*sqrt(Vaprx_IS_W)/(alpha_hat_IS_W*sqrt(M))
varRatio_approx=(RelError_MC/RelError_IS)^2
varRatio_exact=Vaprx_MC/Vaprx_IS_W

%% Using optimal control based on time and state
[Xw, Tw]=ndgrid(X0v,t);
Ff=griddedInterpolant(Xw,Tw, ksi','linear','linear');
ksiStar = @(x,t) Ff(x,t).*(x<K)+zeros(size(t)).*(x>=K);

M=10^4; %MC sample size

%Initialization
h_IS=zeros(1,M);
h_IS1=zeros(1,M);
h_MC=zeros(1,M);
u_MC=zeros(d,N+1);
u_IS=u_MC;
%u0=mu*ones(1,M);%+chol(Sigma)'*randn(d,S);
u0=mu_0+chol(sigma_0)'*randn(d,M);
%u0_shifted=[vSmpl; mCond+chol(SigmaCond)'*randn(d-1,M)];
u0_shifted=u0;
t=linspace(0,T,N+1);
meanL=0;
parfor m=1:M
       u_MC=u0(:,m);
       u_IS=u0_shifted(:,m);
       %u_IS=mu_tilde+chol(Sigma_tilde)'*randn(d,1);
      L=1;
      k=1;
%    optcontTemp=zeros(1,N);
%     dWTemp=zeros(1,N);
%     drift=[];
%     drift_IS=[];
%      u_o=u_IS;
% Leach=[];
% Leach(1)=1;
    while k<=N && P1*u_IS(:,k)<K
        %Adaptive timestep
%        dt=min(0.01, 1/ksiX(ksi(P1*u_IS(:,k),t(k))))
%         if ksi(P1*u_IS(:,k),t(k))==0
%             dt=0.01;
%         else
%             dt=1/ksi(P1*u_IS(:,k),t(k));
%         end
%         
    %for k=1:N
    dW = sqrt(dt).*randn(d,1);
    %dW(1) = sign(P1*dW)*dW(1);
    % optcontTemp(k)=ksi(P1*u_IS(:,k),t(k));
    % dWTemp(k) = P1*dW;
    %Lorenz63 
    %u_IS(:,k+1)= u_IS(:,k)+rk4(u_IS(:,k),dt,r,q,s)+P1'.*b.*ksi(P1*u_IS(:,k),t(k))*dt+b.*dW; %Runge-Kutta
    %u_IS(:,k+1)= u_IS(:,k)+(lorenz63(u_IS(:,k),r,q,s)+P1'.*b.*ksi(P1*u_IS(:,k),t(k)))*dt+b.*dW; %Forward Euler 
    u_IS(:,k+1)= u_IS(:,k)+(lorenz63(u_IS(:,k),r,q,s)+P1'.*b.*ksiStar(P1*u_IS(:,k),t(k)))*dt+b.*dW; %Forward Euler 

    %     if ksi(P1*u_IS(:,k),t(k))>10
%         u_IS(:,k+1)
%         k
%         t(k)
%         ksi(P1*u_IS(:,k),t(k))
%         pause()
%     end
%      drift(k+1)   = P1*lorenz63(u_IS(:,k),r,q,s);
%      drift_IS(k+1)= P1*lorenz63(u_IS(:,k),r,q,s)+P1*b.*ksi(P1*u_IS(:,k),t(k));  
% %     u_o(:,k+1)= u_o(:,k)+(lorenz63(u_o(:,k),r,q,s))*dt+b.*dW; %Forward Euler 
%     if ksi(P1*u_IS(:,k),t(k))>=5
%         ksi(P1*u_IS(:,k),t(k))=5;
%     end
    L=L*exp(-0.5*dt*ksiStar(P1*u_IS(:,k),t(k))^2-ksiStar(P1*u_IS(:,k),t(k))*P1*dW);
    
    %Leach(k+1)=L;
    k=k+1;
    end
%      if exponent>=0 
%         L=0;
%      end
%      if exponent<=-5
%         L=0;
%      end
    
%     Ldist(m)=L;
    
%     figure
%     plot(t(1:k), u_o(1,:),'r',t(1:k), u_IS(1,:), 'b', 'LineWidth',3)
%     title('u')
%     legend('without IS', 'with IS')
% 
%     figure
%     plot(t(1:k), drift, 'r', t(1:k), drift_IS, 'b', 'LineWidth',3)
%     legend('without IS', 'with IS')
%     title('drift')
%     pause()
     %Monte Carlo simulation
    for n=1:N
         dW = sqrt(dt).*randn(d,1);
         u_MC(:,n+1)= u_MC(:,n)+lorenz63(u_MC(:,n),r,q,s)*dt+b.*dW;
         %u_MC(:,n+1)= u_MC(:,n)+rk4(u_MC(:,n),dt,r,q,s)+b.*dW;
    end

    meanL=meanL+L;
    h_IS(m)=(max(P1*u_IS)>=K)*L;
    h_IS1(m)=(max(P1*u_IS)>=K);
    h_MC(m)=(max(P1*u_MC)>=K);
end
meanL=meanL/M
%Weight = rho_x0(P1*u0_shifted,P1*mu,sqrt(P1*Sigma*P1'))./rho_x0(P1*u0_shifted, mFit, sigFit);
%Weight = rho_x0(P1*u0_shifted,P1*mu,sqrt(P1*Sigma*P1'))./rho_x0(P1*u0_shifted, mFit, sqrt(P1*Sigma*P1'));
Weight = ones(1,M);
alpha_hat_IS_W=mean(h_IS.*Weight)
alpha_hat_MC=mean(h_MC)
Vaprx_IS_W=var(h_IS.*Weight)
Vaprx_MC=var(h_MC)
RelError_MC=1.96*sqrt(1-alpha_hat_IS_W)/sqrt(alpha_hat_IS_W*M)
RelError_IS=1.96*sqrt(Vaprx_IS_W)/(alpha_hat_IS_W*sqrt(M))
varRatio_approx=(RelError_MC/RelError_IS)^2
varRatio_exact=Vaprx_MC/Vaprx_IS_W

%% Using the same set for initial condition and optimal control
M=10^4; %MC sample size

%Initialization
h_IS=zeros(1,M);
h_IS1=zeros(1,M);
h_MC=zeros(1,M);
u_MC=zeros(d,N+1);
u_IS=u_MC;
u0=X0v;
%u0_shifted=[vSmpl; mCond+chol(SigmaCond)'*randn(d-1,M)];
u0_shifted=u0;
t=linspace(0,T,N+1);
meanL=0;
parfor m=1:M
       u_MC=u0(:,m);
       u_IS=u0_shifted(:,m);
       %u_IS=mu_tilde+chol(Sigma_tilde)'*randn(d,1);
      L=1;
      k=1;
%    optcontTemp=zeros(1,N);
%     dWTemp=zeros(1,N);
%     drift=[];
%     drift_IS=[];
%      u_o=u_IS;
% Leach=[];
% Leach(1)=1;
    while k<=N && P1*u_IS(:,k)<K
        %Adaptive timestep
%        dt=min(0.01, 1/ksiX(ksi(P1*u_IS(:,k),t(k))))
%         if ksi(P1*u_IS(:,k),t(k))==0
%             dt=0.01;
%         else
%             dt=1/ksi(P1*u_IS(:,k),t(k));
%         end
%         
    %for k=1:N
    dW = sqrt(dt).*randn(d,1);
    %dW(1) = sign(P1*dW)*dW(1);
    % optcontTemp(k)=ksi(P1*u_IS(:,k),t(k));
    % dWTemp(k) = P1*dW;
    %Lorenz63 
    %u_IS(:,k+1)= u_IS(:,k)+rk4(u_IS(:,k),dt,r,q,s)+P1'.*b.*ksi(P1*u_IS(:,k),t(k))*dt+b.*dW; %Runge-Kutta
    %u_IS(:,k+1)= u_IS(:,k)+(lorenz63(u_IS(:,k),r,q,s)+P1'.*b.*ksi(P1*u_IS(:,k),t(k)))*dt+b.*dW; %Forward Euler 
    u_IS(:,k+1)= u_IS(:,k)+(lorenz63(u_IS(:,k),r,q,s)+P1'.*b.*ksi(k,m))*dt+b.*dW; %Forward Euler 

    %     if ksi(P1*u_IS(:,k),t(k))>10
%         u_IS(:,k+1)
%         k
%         t(k)
%         ksi(P1*u_IS(:,k),t(k))
%         pause()
%     end
%      drift(k+1)   = P1*lorenz63(u_IS(:,k),r,q,s);
%      drift_IS(k+1)= P1*lorenz63(u_IS(:,k),r,q,s)+P1*b.*ksi(P1*u_IS(:,k),t(k));  
% %     u_o(:,k+1)= u_o(:,k)+(lorenz63(u_o(:,k),r,q,s))*dt+b.*dW; %Forward Euler 
%     if ksi(P1*u_IS(:,k),t(k))>=5
%         ksi(P1*u_IS(:,k),t(k))=5;
%     end
    L=L*exp(-0.5*dt*ksi(k,m)^2-ksi(k,m)*P1*dW);
    
    %Leach(k+1)=L;
    k=k+1;
    end
%      if exponent>=0 
%         L=0;
%      end
%      if exponent<=-5
%         L=0;
%      end
    
%     Ldist(m)=L;
    
%     figure
%     plot(t(1:k), u_o(1,:),'r',t(1:k), u_IS(1,:), 'b', 'LineWidth',3)
%     title('u')
%     legend('without IS', 'with IS')
% 
%     figure
%     plot(t(1:k), drift, 'r', t(1:k), drift_IS, 'b', 'LineWidth',3)
%     legend('without IS', 'with IS')
%     title('drift')
%     pause()
     %Monte Carlo simulation
    for n=1:N
         dW = sqrt(dt).*randn(d,1);
         u_MC(:,n+1)= u_MC(:,n)+lorenz63(u_MC(:,n),r,q,s)*dt+b.*dW;
         %u_MC(:,n+1)= u_MC(:,n)+rk4(u_MC(:,n),dt,r,q,s)+b.*dW;
    end

    meanL=meanL+L;
    h_IS(m)=(max(P1*u_IS)>=K)*L;
    h_IS1(m)=(max(P1*u_IS)>=K);
    h_MC(m)=(max(P1*u_MC)>=K);
end
meanL=meanL/M
%Weight = rho_x0(P1*u0_shifted,P1*mu,sqrt(P1*Sigma*P1'))./rho_x0(P1*u0_shifted, mFit, sigFit);
%Weight = rho_x0(P1*u0_shifted,P1*mu,sqrt(P1*Sigma*P1'))./rho_x0(P1*u0_shifted, mFit, sqrt(P1*Sigma*P1'));
Weight = ones(1,M);
alpha_hat_IS_W=mean(h_IS.*Weight)
alpha_hat_MC=mean(h_MC)
Vaprx_IS_W=var(h_IS.*Weight)
Vaprx_MC=var(h_MC)
RelError_MC=1.96*sqrt(1-alpha_hat_IS_W)/sqrt(alpha_hat_IS_W*M)
RelError_IS=1.96*sqrt(Vaprx_IS_W)/(alpha_hat_IS_W*sqrt(M))
varRatio_approx=(RelError_MC/RelError_IS)^2
varRatio_exact=Vaprx_MC/Vaprx_IS_W

function f=lorenz63(y,r,q,s)
f(1,1)=r*(y(2)-y(1));
f(2,1)=s*y(1)-y(2)-y(1)*y(3);
f(3,1)=y(1)*y(2)-q*y(3);
end

function dv = rk4(v_old,h,r,q,s)
 k1 = lorenz63(v_old,r,q,s);
 k2 = lorenz63(v_old+1/2.*h.*k1,r,q,s);
 k3 = lorenz63(v_old+1/2.*h.*k2,r,q,s);
 k4 = lorenz63(v_old+h.*k3,r,q,s);
 dv= 1/6*h*(k1+2*k2+2*k3+k4);
end