function [p, p_dim]=poly_space(w, space_type)

switch space_type
    case 'TD'
         p=[];
         for i=1:w+1
             j=0;
             while i-1+j <=w
                 p=[p; [i-1,j]];
                 j=j+1;
             end 
         end
    case 'HC'
        p=[];
        for i=1:w+1
            j=0;
            while i*(j+1)<=w+1
                p=[p; [i-1,j]];
                j=j+1;
            end
        end
       
    case 'TP'
        p=[];
        for i=1:w+1
            j=0;
            while max(i-1,j)<=w
                p=[p; [i-1,j]];
                j=j+1;
            end
        end
       
end