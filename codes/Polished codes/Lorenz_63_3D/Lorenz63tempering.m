clear;   close all; set(0, 'defaultaxesfontsize', 15); format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This is the code for Lorenz 63 problem to test PDE method
%%% in performing IS technique: 
%-- dv1 = r(v2-v1)+sigma*dW ---------------%
%-- dv2 = sv1-v2-v1v3   ---------------%
%-- dv3 = v1v2-qv3      ---------------%
%-------------------------------------------------------------------------%
%%% in computing QoI: P(max_{0<=t<=T} P1*v(t) >K)

%Model parameters for Lorenz 63
d=3;            %problem dimension
sigma=10;     %constant diffusion parameter
r=10;
q=8/3;
s=28;
% r=3;
% q=1;
% s=26.5;
a = @(u) [r*(u(2,:,:)-u(1,:,:)); s*u(1,:,:)-u(2,:,:)-u(1,:,:).*u(3,:,:); u(1,:,:).*u(2,:,:)-q*u(3,:,:)]; %Lorenz 63 drift
b = [sigma; sigma; sigma]; 
I=eye(d);

%Simulation parameters
T=0.05;         %final time (simulation length)
M=10^4;      %Monte Carlo sample size
N=100;       %number of timesteps in SDE
dt=T/N;      %discretization step in SDE
% Initial original density parameters 
mu=[5 5 25]';
%mu=[0 0 1]';
Sigma=0.25*I;

%Define the Projector which component to track
P1   = I(1,:); 
%P1 = (1/3)*diag(I)';
%% Generate samples of Lorenz 63 dynamics to use for L2 regression
u=zeros(d,N+1,M);
Mbar=zeros(1,M);
for m=1:M
    u(:,1,m)=mu+chol(Sigma)'*randn(d,1);
    for n=1:N       
        %Lorenz63
        u(:,n+1,m)=u(:,n,m)+lorenz63(u(:,n,m),r,q,s)*dt+b.*sqrt(dt).*randn(3,1);%EM time-stepping
    end  
    Mbar(m)=max(P1*u(:,:,m));
end
%K=floor(max(Mbar)) %corresponds to 10^(-4) probability
%K=11.93;
%K=10.48% s=28
%K = 11.95 %sigma=0.05
%K=12.6; %sigma=05
%K = 13.4 %sigma=1
K=5; %sigma=10 
alpha_hat=mean(Mbar>=K)
var_hat=var(Mbar>=K)
%%
x1=squeeze(u(1,:,:));
figure
plot(0:dt:T,mean(x1,2))
x2=squeeze(u(2,:,:));
figure
plot(0:dt:T,mean(x2,2))
x3=squeeze(u(3,:,:));
figure
plot(0:dt:T,mean(x3,2))
figure
subplot(1,4,1)
plot(0:dt:T, mean(x1,2), 'LineWidth', 2)
subplot(1,4,2)
plot(0:dt:T, std(x1,1,2),'LineWidth', 2)
subplot(1,4,3)
plot(0:dt:T, skewness(x1,1,2),'LineWidth', 2)
subplot(1,4,4)
plot(0:dt:T, kurtosis(x1,1,2),'LineWidth', 2)
% %% CE
% beta=0.01;                              %CE method parameter
% sMbar=sort(Mbar);
% K_ell=sMbar(1, ceil((1-beta)*M));
% K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
% Weight = ones(1,M);
% h=Mbar>=K_ell;
% ell=1;
% Sigma_tilde=Sigma;
% u0=reshape(u(:,1,:),d,M);
% 
% while K_ell<K
%     mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight);
%     
%     ell=ell+1;
%     for m=1:M
%         u(:,1,m)=mu_tilde+chol(Sigma)'*randn(d,1);
%         u0(:,m)=u(:,1,m);
%         for n=1:N       
%             %Lorenz63
%             u(:,n+1,m)=u(:,n,m)+lorenz63(u(:,n,m),r,q,s)*dt+b.*sqrt(dt).*randn(3,1);%EM time-stepping
%         end  
%         Mbar(m)=max(P1*u(:,:,m));
%     end
%     sMbar=sort(Mbar);
%     K_ell=sMbar(1, ceil((1-beta)*M));
%     K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
%     h=Mbar>=K_ell;
%     Weight=mvnpdf(u0', mu', Sigma)'./mvnpdf(u0',mu_tilde',Sigma_tilde)';
% end
% 
% if K_ell==K
%     mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight)
% end
%% L2 regression
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We approximate only the drift, the diffusion is constant

%Define the Polynomial space
w = 2;                                                         %Polynomial degree
p = poly_space(w,'HC');                                        %Polynomial space
p_dim = size(p,1);                                             %Cardinality of polynomial space

t  = linspace(0,T,N+1)';                                       %Generating t_0, t_1, ..., t_{N-1}
Tt = reshape(repmat(t,1,M)', (N+1)*M,1);                       %Replicating M times each t_n and saving as a column vector [t_0 t_0 ... t_0, t_1, ..., t_1,..., t_{N-1}, ..., t_{N_1}]'
Xx = reshape(squeeze(pagemtimes(P1,u))', (N+1)*M,1);           %Projected Samples saved as a column vector [X_0^(1),..., X_0^(M), X_1^(1),..,X_1^(M), ..., X_{N-1}^(1),...,X_{N-1}^(M)]
tX = [Tt Xx];                                                  %Two column vectors of size N*M for [t_n, X_{t_n}]
f  = reshape(squeeze(pagemtimes(P1,a(u)))',(N+1)*M,1);         %Given f function, we are going to solve

D     = x2fx(tX, p);                                           %This function helps to create Psi matrix each column is non-orth. basis function psi_p for different given p
[Q,R] = modified_GS(D);                                        %Apply modified Gram-Schmidt process to get QR decomposition
a_p   = Q'*f;                                                  %Solve Normal equations based on orthonormalised basis

psy     = @(t,s) x2fx([t s], p);                               %Non-orth. basis function
psy_bar = @(t,s) x2fx([t s], p)/R;                             %Orth. basis function
a_bar   = @(t,s) psy_bar(t,s)*a_p;                             %Approximation to the drift function a(x)
b_bar   = @(s)   sqrt(P1*(b*b')*P1')*ones(size(s));            %Approximation to the diffusion function b(x)
%close all
%%
figure(1)
fsurf(a_bar, [0 T 0 K])
hold on
plot3(Tt, Xx, f, 'k*')
hold off
xlabel('t', 'fontsize', 18)
ylabel('x','fontsize', 18)
zlabel('$\bar{a}(x,t)$', 'interpreter', 'latex','fontsize', 18)
title(['$\mathcal{K}=$' num2str(K), ', ', '$\hat{\alpha}=$', num2str(alpha_hat), ', ', 'HC=4'],'interpreter', 'latex','fontsize', 22)
% figure
% plot3(Tt, Xx, f, 'k*')
% hold on
% xplot=linspace(-2,K+2,51);
% aplot=zeros(length(t), length(xplot));
% [tt,xx]=ndgrid(t,xplot);
% for i=1:length(t)
%     for j=1:length(xplot)
%         aplot(i,j)=a_bar(t(i),xplot(j));
%     end
% end
% surf(tt, xx, aplot)
% grid on
% xlabel('t', 'fontsize', 18)
% ylabel('x','fontsize', 18)
% zlabel('$\bar{a}(x,t)$', 'interpreter', 'latex','fontsize', 18)
% title(['$\mathcal{K}=$' num2str(K), ', ', '$\hat{\alpha}=$', num2str(alpha_hat), ', ', 'HC=4'],'interpreter', 'latex','fontsize', 22)
% hold off
%%
figure
plot3(Xx,Tt, f, 'k*')
hold on
xplot=linspace(-2,K+2,51);
aplot=zeros(length(t), length(xplot));
[tt,xx]=ndgrid(t,xplot);
for i=1:length(t)
    for j=1:length(xplot)
        aplot(i,j)=a_bar(t(i),xplot(j));
    end
end
surf(xx,tt, aplot)
grid on
xlabel('t', 'fontsize', 18)
ylabel('x','fontsize', 18)
zlabel('$\bar{a}(x,t)$', 'interpreter', 'latex','fontsize', 18)
title(['$\mathcal{K}=$' num2str(K), ', ', '$\hat{\alpha}=$', num2str(alpha_hat), ', ', 'HC=4'],'interpreter', 'latex','fontsize', 22)
hold off
%%
rho_x0 = @(x, m, sig) exp(-(x-m).^2/(2*sig^2))/(sqrt(2*pi*sig^2)); %1D Gaussian density
Kell=K; %initial threshold defined closer to the mean

while Kell<24.2
    % PDE method
    xL = 0;                 % Lower end of discretization interval
    xU = Kell;                    % Upper end of discretization interval
    dxPde = 0.001;                 % Space step in PDE    
    dtPde = dxPde;              % Time step in PDE
    Nx = round((xU-xL)/dxPde);    % Number of space steps in PDE
    Nt = round(T/dtPde);          % Number of time steps in PDE
    x  = linspace(xL,xU,Nx+1)';   % Space grid points
    smf=10;                       % smoothing factor
    [tW, xW, v] = KBEsolverForAllt(x,T,a_bar,b_bar,Nt,Nx,Kell,smf);%KBE solution for all time
    % rho_0 CONTROL
    rhoTilde_x0 = [rho_x0(xW, P1*mu, P1*Sigma*P1').*v(:,end); rho_x0((Kell:dxPde:Kell+100*dxPde)', P1*mu, P1*Sigma*P1')];
    NC = trapezoidal(rhoTilde_x0, dxPde); %normalising constant
    rhoTilde_x0=rhoTilde_x0./NC;          %optimal IS density

    %fit optimal IS denisty to Gaussian
    x=[xW;(Kell:dxPde:Kell+100*dxPde)'];
    mFit = trapezoidal(x.*rhoTilde_x0, dxPde)
    sigFit = sqrt(trapezoidal(x.^2.*rhoTilde_x0, dxPde)-mFit^2)
    %rhoFit_x0 = rho_x0(x, mFit, sigFit);
    vSmpl=mFit+sigFit*randn(1,M);
    mCond=mu(2:3)+Sigma(2:3,1)*(vSmpl-mu(1))/Sigma(1,1);
    SigmaCond=Sigma(2:3,2:3)-Sigma(2:3,1)/Sigma(1,1)*Sigma(1,2:3);
    % dW control
    %obtained via KBE solution on PDE grids
    ksiGridded = zeros(length(xW),length(tW));
    ksiGridded(2:end-1,:) = sigma*(log(v(3:end,:))-log(v(1:end-2,:)))./(2*dxPde);
    ksiGridded(1,:) = sigma*(log(v(2,:))-log(v(1,:)))./dxPde;
    ksiGridded(end,:) = sigma*(log(v(end,:))-log(v(end-1,:)))./dxPde;
    
    %Plot the KBE solution
    figure
    [Xw,Tw] = ndgrid(xW,tW); %meshgrid for the interpolant
    p0=surf(Xw, Tw, v);
    set(p0,'LineStyle','none')
    xlabel('x','fontsize', 18)
    ylabel('t','fontsize', 18)
    zlabel('u(x,t)','fontsize', 22)
    title(['$\mathcal{K}_\ell=$', num2str(Kell)], 'interpreter', 'latex')
    grid on

    %Plot the optimal control on the PDE meshgrid
    figure
    p1=surf(Xw, Tw, ksiGridded);
    set(p1,'LineStyle','none')
    xlabel('x','fontsize', 18)
    ylabel('t','fontsize', 18)
    title(['$\mathcal{K}_\ell=$', num2str(Kell)],'interpreter', 'latex')
    zlabel('optimal control')
    grid on

    cropN=round(0.1*Nx)+1; %if we use KBE, I crop 10% of the left baoundary and linearly smooth it to avoid instability on the left handside; for HJB no need to do this and nCrop=1
    cropT=round(0*Nt)+1;
    Ff=griddedInterpolant(Xw(cropN:end,cropT:end),flip(Tw(cropN:end,cropT:end),2), flip(real(ksiGridded(cropN:end,cropT:end)),2),'nearest','nearest');
    ksi = @(x,t) Ff(x,t).*(x<Kell)+zeros(size(t)).*(x>=Kell); %optimal control function for any x and t
    %ksi = @(x,t) Ff(x,t); %optimal control function for any x and t
    %ksi = @(x,t) 0;
    
    h_IS=zeros(1,M);
    u_IS=zeros(d,N+1,M);
    Mbar=zeros(1,M);
    mu_tilde=mu;
    Sigma_tilde=Sigma;
    mu_tilde(1)=mFit;
    %Sigma_tilde(1,1)=sigFit^2;
    Lkl_dW=ones(N+1,M);
    
    for m=1:M
        u_IS(:,1,m)=mu_tilde+chol(Sigma_tilde)'*randn(d,1);
        L=1;
        %k=1;
        %while k<=N && P1*u_IS(:,k,m)<Kell
        Lkl_dW(1,m)=L;
        for k=1:N   
        dW = sqrt(dt).*randn(d,1);
        %Lorenz63 
        u_IS(:,k+1,m)= u_IS(:,k,m)+(lorenz63(u_IS(:,k,m),r,q,s)+P1'.*b.*ksi(P1*u_IS(:,k,m),t(k)))*dt+b.*dW; %Forward Euler 
        L=L*exp(-0.5*dt*(ksi(P1*u_IS(:,k,m),t(k)))^2-ksi(P1*u_IS(:,k,m),t(k))*P1*dW);
        Lkl_dW(k+1,m)=exp(-0.5*dt*(ksi(P1*u_IS(:,k,m),t(k)))^2-ksi(P1*u_IS(:,k,m),t(k))*P1*dW);
        %k=k+1;
        end
        Mbar(m)=max(P1*u_IS(:,:,m));
        h_IS(m)=(max(P1*u_IS(:,:,m))>=Kell)*L;
    end
        Lkl_rho0 = rho_x0(P1*squeeze(u_IS(:,1,:)),P1*mu,sqrt(P1*Sigma*P1'))./rho_x0(P1*squeeze(u_IS(:,1,:)), mFit, sqrt(P1*Sigma*P1'));
        %Lkl_rho0 = ones(1,M);
        sMbar=sort(Mbar);
%       Kell=sMbar(1, ceil((1-beta)*M));
%       Kell=Kell*(Kell<K)+K*(Kell>=K)
        alpha_hat_IS=mean(h_IS.*Lkl_rho0)
        var_hat_IS=var(h_IS.*Lkl_rho0)
    %Define the Polynomial space
    w = 2;                                                         %Polynomial degree
    p = poly_space(w,'HC');                                        %Polynomial space
    p_dim = size(p,1);    
    
    t  = linspace(0,T,N+1)';                                       %Generating t_0, t_1, ..., t_{N-1}
    Tt = reshape(repmat(t,1,M)', (N+1)*M,1);                       %Replicating M times each t_n and saving as a column vector [t_0 t_0 ... t_0, t_1, ..., t_1,..., t_{N-1}, ..., t_{N_1}]'
    Xx = reshape(squeeze(pagemtimes(P1,u_IS))', (N+1)*M,1);        %Projected Samples saved as a column vector [X_0^(1),..., X_0^(M), X_1^(1),..,X_1^(M), ..., X_{N-1}^(1),...,X_{N-1}^(M)]
    tX = [Tt Xx];                                                  %Two column vectors of size N*M for [t_n, X_{t_n}]
    f  = reshape(squeeze(pagemtimes(P1,a(u_IS)))',(N+1)*M,1);      %Given f function, we are going to solve

    D     = x2fx(tX, p);                                           %This function helps to create Psi matrix each column is non-orth. basis function psi_p for different given p
    Lkl_dW(1,:)= Lkl_dW(1,:).*Lkl_rho0;
    weight= reshape(Lkl_dW', (N+1)*M,1);
    [Q,R] = modified_GS_w(D, weight);                              %Apply modified Gram-Schmidt process to get QR decomposition
    a_p   = Q'*f;                                                  %Solve Normal equations based on orthonormalised basis
    
    psy     = @(t,s) x2fx([t s], p);                               %Non-orth. basis function
    psy_bar = @(t,s) x2fx([t s], p)/R;                             %Orth. basis function
    a_bar   = @(t,s) psy_bar(t,s)*a_p;                             %Approximation to the drift function a(x)
    b_bar   = @(s)   sqrt(P1*(b*b')*P1')*ones(size(s));            %Approximation to the diffusion function b(x)
    
    figure
    plot3(Tt, Xx, f, 'k*')
    hold on
    xplot=linspace(-2,15.5,51);
    aplot=zeros(length(t), length(xplot));
    [tt,xx]=ndgrid(t,xplot);
    for i=1:length(t)
        for j=1:length(xplot)
            aplot(i,j)=a_bar(t(i),xplot(j));
        end
    end
    surf(tt,xx,aplot)
    xlabel('t', 'fontsize', 18)
    ylabel('x','fontsize', 18)
    zlabel('$\bar{a}(x,t)$', 'interpreter', 'latex','fontsize', 18)
    title(['$\mathcal{K}=$' num2str(Kell), ', ', '$\hat{\alpha}=$', num2str(alpha_hat_IS), ', ', 'HC=4'],'interpreter', 'latex','fontsize', 22)
    grid on
    hold off
    
%     figure(102)
%     fsurf(a_bar, [0 T mu(1)-8 Kell+8])
%     hold on
%     plot3(Tt, Xx, f, 'r*')
%     title('Kell=', num2str(Kell))
%     hold off
    pause()
    Kell=Kell+1
    mu=mu_tilde
    Sigma=Sigma_tilde
end

%% 
pause()

%% Plot for a and a_bar
% t=[0:dt:T];
% for i=1:N+1
%     aL2(i)=a_bar(t(i),P1*mean(u(:,i,:),3));
% end
% plot(t, aL2, 'r', t, P1*mean(a(u),3),'b', 'LineWidth', 3)
% xlabel('t')
% ylabel('u_1')
% legend('$\bar{a}(t,mean(u_1))$', '$mean(a(u_1))$', 'interpreter', 'latex','fontsize', 18)
% title('T=1')
%% PDE method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xL = mu(1)-1;                       % Lower end of discretization interval
xU = K;                       % Upper end of discretization interval
dxPde = 0.01;                 % Space step in PDE    
dtPde = dxPde^2;              % Time step in PDE
%dtPde = dxPde/2;             % Time step in PDE
Nx = round((xU-xL)/dxPde);    % Number of space steps in PDE
Nt = round(T/dtPde);          % Number of time steps in PDE
x  = linspace(xL,xU,Nx+1)';   % Space grid points

a_barTmdt=@(x) a_bar(T-dt, x);

[~,~,vErik] = tabulate_IC(a_barTmdt,b_bar,dt,K,xL,xU,false,Nx+1); %KBE solution at T-dt obtained via numerical optimization (using Erik's code)

%%
smf=5; %smoothing factor
%KBE solution for all time
[tW, xW, v] = KBEsolverForAllt(x,T,a_bar,b_bar,Nt,Nx,K,smf); %PDE solver via CN backward scheme using exponential smoothing of Ind.Fun in a final condition
% [tW, xW, u] = HJBsolverForAllt_CN(x,T,a_bar,b_bar,Nt,Nx,K,smf);
% [tW, xW, uPont] = HJBsolverForAllt_CN_forPontryagin(x,T,a_bar,b_bar,Nt,Nx,K,smf);
%%
figure
plot(X0range(1,:), interp1(xW, u(:,end), X0range(1,:)), 'b', X0range(1,:), valuefun, 'g', 'Linewidth', 2)
xlabel('x','fontsize', 18)
ylabel('v(x,0)','fontsize', 18)
legend('stoch. KBE','Pontryagin')
title('Value function at t=0 with \sigma=0.05')
%%
z=linspace(0,1,Nx+1);
alpha=0.05;
xc=K-dxPde;
beta=alpha*(xU-xL);
delta=asinh((xL-xc)/beta);
gamma=asinh((xU-xc)/beta)-delta;
xnonuniform=xc+beta*sinh(gamma*z+delta)';

[tW, xW, v] = KBEsolverForAllt(xnonuniform,T,a_bar,b_bar,Nt,Nx,K,smf); %PDE solver via CN backward scheme using exponential smoothing of Ind.Fun in a final condition

%%
figure
plot(xW, v(:,1), '-b', xW, vErik(2:end)', '--k', 'Linewidth',2) % xW, exp(-u(:,1)), '-r',
xlabel('s', 'fontsize', 14)
ylabel('u(s,0)','fontsize', 14)

[Xw,Tw] = ndgrid(xW,tW);
figure
p0=surf(Xw, Tw, v);
set(p0,'LineStyle','none')
grid on
xlabel('x','fontsize', 18)
ylabel('t','fontsize', 18)
zlabel('u(x,t)','fontsize', 20)
title('KBE solution: $K=11.95, \sigma=0.05$', 'fontsize', 22, 'interpreter', 'latex')
% figure
% p0=surf(Xw, Tw, exp(-u));
% set(p0,'LineStyle','none')
% xlabel('x','fontsize', 18)
% ylabel('t','fontsize', 18)
% zlabel('u(x,t)','fontsize', 20)
% title('HJB solution: $K=13.5, \sigma=0.5$', 'fontsize', 22, 'interpreter', 'latex')

%pause() 

%% dW CONTROL
% gradlog1d=zeros(length(xW),length(tW));
% gradlog1d(2:end-1,:) = (log(v(3:end,:))-log(v(1:end-2,:)))./(2*dxPde);
% gradlog1d(1,:) = (log(v(2,:))-log(v(1,:)))./dxPde;
% gradlog1d(end,:) = (log(v(end,:))-log(v(end-1,:)))./dxPde;

% %obtained via KBE solution on PDE grids
% ksiGridded = zeros(length(xW),length(tW));
% ksiGridded(2:end-1,:) = sigma*(log(v(3:end,:))-log(v(1:end-2,:)))./(2*dxPde);
% ksiGridded(1,:) = sigma*(log(v(2,:))-log(v(1,:)))./dxPde;
% ksiGridded(end,:) = sigma*(log(v(end,:))-log(v(end-1,:)))./dxPde;

%obtained via KBE solution on PDE grids
ksiGridded = zeros(length(xW),length(tW));
ksiGridded(2:end-1,:) = sigma*(log(v(3:end,:)./v(1:end-2,:)))./(2*dxPde)/2;
ksiGridded(1,:) = sigma*(log(v(2,:)./v(1,:)))./dxPde/2;
ksiGridded(end,:) = sigma*(log(v(end,:)./v(end-1,:)))./dxPde/2;

% %obtained via HJB solution on PDE grids
% ksiGriddedHJB = zeros(length(xW),length(tW));
% ksiGriddedHJB(2:end-1,:) = -sigma*(u(3:end,:)-u(1:end-2,:))./(2*dxPde);
% ksiGriddedHJB(1,:) = -sigma*(u(2,:)-u(1,:))./dxPde;
% ksiGriddedHJB(end,:) = -sigma*(u(end,:)-u(end-1,:))./dxPde;

[Xw,Tw] = ndgrid(xW,tW); %meshgrid for the interpolant

cropN=round(0.1*Nx)+1; %if we use KBE, I crop 10% of the left baoundary and linearly smooth it to avoid instability on the left handside; for HJB no need to do this and nCrop=1
cropT=round(0*Nt)+1;
Ff=griddedInterpolant(Xw(cropN:end,cropT:end),flip(Tw(cropN:end,cropT:end),2), flip(real(ksiGridded(cropN:end,cropT:end)),2),'linear','nearest');
%ksi = @(x,t) Ff(x,t).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function for any x and t
ksi = @(x,t) Ff(x,t).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function for any x and t
%ksiX  = @(ksi) ksi;
ksiX  = @(ksi) ksi.*(ksi>=0)+0.*(ksi<0);
%Optimal control using HJB solution
% Ff = griddedInterpolant(Xw,flip(Tw,2), flip(ksiGriddedHJB,2),'nearest','nearest');
% ksi = @(x,t) Ff(x,t).*(x<K)+zeros(size(t)).*(x>=K); %optimal control function for any x and t
%%
% % %Plot the optimal control on the PDE meshgrid
figure
p1=surf(Xw, Tw, ksiGridded);
set(p1,'LineStyle','none')
% 
% % %Plot the extrapolated optimal control
% figure
% xF  = linspace(xL-5,K+3,Nx+1)'; 
% [XwF,TwF] = ndgrid(xF,tW);
% p2=surf(XwF, TwF, ksi(XwF,TwF));
% set(p2,'LineStyle','none')
%%
figure
plot(t, control(1,:)', 'g-',tW, ksiGridded(100,:),'r-',tW, ksiGridded(600,:),'b-',tW, ksiGridded(1100,:),'m-', tW, ksiGridded(1600,:),'c-','Linewidth', 2)
xlabel('t','fontsize', 18)
ylabel('\xi_1(x,t)','fontsize', 18)
title('Optimal control with \sigma=10')
legend('Pontryagin','KBE: x=5','KBE: x=10','KBE: x=15','KBE: x=20')
%%
%Plot KBE solution at different time in 2D
tt=[T-dtPde 0.9 0.8 0.5 0];
C = {'r','m', 'g', 'k', 'y'}; 
for i=1:length(tt)
    figure(7)
    tfix=find(tW==tt(i));
    %semilogy(xW, exp(-u(:,tfix)), 'LineWidth', 2)
    plot(xW, v(:,tfix), 'LineWidth', 2)
    hold on
end
grid on
legend('t=T-dt', 't=0.9T', 't=0.8T', 't=0.5T', 't=0')
xlabel('x')
ylabel('u(t,x)')
title('KBE solution at different time, \sigma=1, s_f=5')

%Plot optimal control at different time in 2D
ksiInterp=ksi(Xw,Tw);
for i=1:length(tt)
    figure(8)
    tfix=find(tW==tt(i));
    plot(xW, ksiInterp(:,tfix), 'LineWidth', 2)
    hold on
end
grid on
legend('t=T-dt','t=0.9T', 't=0.8T', 't=0.5T', 't=0')
xlabel('x')
ylabel('ksi(t,x)')
hold off
title('Optimal Control at different time')
%% Computing the QoI with and without change of measure wrt W 
M=10^4; %MC sample size

%Initialization
h_IS=zeros(1,M);
h_IS1=zeros(1,M);
h_MC=zeros(1,M);
u_MC=zeros(d,N+1);
u_IS=u_MC;
%u0=mu*ones(1,M);%+chol(Sigma)'*randn(d,S);
u0=mu+chol(Sigma)'*randn(d,M);
%u0_shifted=[vSmpl; mCond+chol(SigmaCond)'*randn(d-1,M)];
u0_shifted=u0;
t=linspace(0,T,N+1);
meanL=0;
trajStoch=cell(1,M);
parfor m=1:M
       u_MC=u0(:,m);
       u_IS=u0_shifted(:,m);
       %u_IS=mu_tilde+chol(Sigma_tilde)'*randn(d,1);
      L=1;
      k=1;
%    optcontTemp=zeros(1,N);
%     dWTemp=zeros(1,N);
%     drift=[];
%     drift_IS=[];
%      u_o=u_IS;
% Leach=[];
% Leach(1)=1;
   % exponent=0;
    while k<=N && P1*u_IS(:,k)<K
        %Adaptive timestep
%        dt=min(0.01, 1/ksiX(ksi(P1*u_IS(:,k),t(k))))
%         if ksi(P1*u_IS(:,k),t(k))==0
%             dt=0.01;
%         else
%             dt=1/ksi(P1*u_IS(:,k),t(k));
%         end
%         
    %for k=1:N
    dW = sqrt(dt).*randn(d,1);
    %dW(1) = sign(P1*dW)*dW(1);
    % optcontTemp(k)=ksi(P1*u_IS(:,k),t(k));
    % dWTemp(k) = P1*dW;
    %Lorenz63 
    %u_IS(:,k+1)= u_IS(:,k)+rk4(u_IS(:,k),dt,r,q,s)+P1'.*b.*ksi(P1*u_IS(:,k),t(k))*dt+b.*dW; %Runge-Kutta
    %u_IS(:,k+1)= u_IS(:,k)+(lorenz63(u_IS(:,k),r,q,s)+P1'.*b.*ksi(P1*u_IS(:,k),t(k)))*dt+b.*dW; %Forward Euler 
    u_IS(:,k+1)= u_IS(:,k)+(lorenz63(u_IS(:,k),r,q,s)+P1'.*b.*ksiX(ksi(P1*u_IS(:,k),t(k))))*dt+b.*dW; %Forward Euler 

    %     if ksi(P1*u_IS(:,k),t(k))>10
%         u_IS(:,k+1)
%         k
%         t(k)
%         ksi(P1*u_IS(:,k),t(k))
%         pause()
%     end
%      drift(k+1)   = P1*lorenz63(u_IS(:,k),r,q,s);
%      drift_IS(k+1)= P1*lorenz63(u_IS(:,k),r,q,s)+P1*b.*ksi(P1*u_IS(:,k),t(k));  
% %     u_o(:,k+1)= u_o(:,k)+(lorenz63(u_o(:,k),r,q,s))*dt+b.*dW; %Forward Euler 
%     if ksi(P1*u_IS(:,k),t(k))>=5
%         ksi(P1*u_IS(:,k),t(k))=5;
%     end
    %exponent=exponent+(-0.5*dt*(ksiX(ksi(P1*u_IS(:,k),t(k))))^2-ksiX(ksi(P1*u_IS(:,k),t(k)))*P1*dW);
    L=L*exp(-0.5*dt*(ksiX(ksi(P1*u_IS(:,k),t(k))))^2-ksiX(ksi(P1*u_IS(:,k),t(k)))*P1*dW);
    
    %Leach(k+1)=L;
    k=k+1;
    end
    %trajStochM{m}=u_IS(1,:);
%     figure(2)
%     hold on
%     plot(0:dt:t(k), u_IS(1,:), 'r:','Linewidth', 2)
%     hold on
%     pause()
%     k
%     pause()
%      if exponent>=0 
%         L=0;
%      end
%      if exponent<=-5
%         L=0;
%      end
%      expDist(m)=exponent;
    
%     Ldist(m)=L;
    
%     figure
%     plot(t(1:k), u_o(1,:),'r',t(1:k), u_IS(1,:), 'b', 'LineWidth',3)
%     title('u')
%     legend('without IS', 'with IS')
% 
%     figure
%     plot(t(1:k), drift, 'r', t(1:k), drift_IS, 'b', 'LineWidth',3)
%     legend('without IS', 'with IS')
%     title('drift')
%     pause()
     %Monte Carlo simulation
    for n=1:N
         dW = sqrt(dt).*randn(d,1);
         u_MC(:,n+1)= u_MC(:,n)+lorenz63(u_MC(:,n),r,q,s)*dt+b.*dW;
         %u_MC(:,n+1)= u_MC(:,n)+rk4(u_MC(:,n),dt,r,q,s)+b.*dW;
    end

    meanL=meanL+L;
    h_IS(m)=(max(P1*u_IS)>=K)*L;
    h_IS1(m)=(max(P1*u_IS)>=K);
    h_MC(m)=(max(P1*u_MC)>=K);
end
meanL=meanL/M
%Weight = rho_x0(P1*u0_shifted,P1*mu,sqrt(P1*Sigma*P1'))./rho_x0(P1*u0_shifted, mFit, sigFit);
%Weight = rho_x0(P1*u0_shifted,P1*mu,sqrt(P1*Sigma*P1'))./rho_x0(P1*u0_shifted, mFit, sqrt(P1*Sigma*P1'));
Weight = ones(1,M);
alpha_hat_IS_W=mean(h_IS.*Weight)
%alpha_hat_IS_W1=mean(h_IS1.*exp(expDist-mean(expDist)))*exp(mean(expDist))
alpha_hat_MC=mean(h_MC)
Vaprx_IS_W=var(h_IS.*Weight)
%Vaprx_IS_W1=var(h_IS1.*exp(expDist-mean(expDist)))*exp(mean(expDist))^2
Vaprx_MC=var(h_MC)
RelError_MC=1.96*sqrt(1-alpha_hat_IS_W)/sqrt(alpha_hat_IS_W*M)
RelError_IS=1.96*sqrt(Vaprx_IS_W)/(alpha_hat_IS_W*sqrt(M))
varRatio_approx=(RelError_MC/RelError_IS)^2
varRatio_exact=Vaprx_MC/Vaprx_IS_W

%% Plot for a and a_bar
% t=[0:dt:T];
% for i=1:N+1
%     aL2(i)=a_bar(t(i),P1*mean(u(:,i,:),3));
% end
% plot(t, aL2, 'r', t, P1*mean(a(u),3),'b', 'LineWidth', 3)
% xlabel('t')
% ylabel('u_1')
% legend('$\bar{a}(t,mean(u_1))$', '$mean(a(u_1))$', 'interpreter', 'latex','fontsize', 18)
% title('T=1')
function dv = rk4(v_old,h,r,q,s)
% v[t+1] = rk4(v[t],step)
 k1 = lorenz63(v_old,r,q,s);
 k2 = lorenz63(v_old+1/2.*h.*k1,r,q,s);
 k3 = lorenz63(v_old+1/2.*h.*k2,r,q,s);
 k4 = lorenz63(v_old+h.*k3,r,q,s);
 dv= 1/6*h*(k1+2*k2+2*k3+k4);
end