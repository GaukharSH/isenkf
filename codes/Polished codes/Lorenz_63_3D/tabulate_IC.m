function [xv,zetav,u_tau] = tabulate_IC(a,b,dt,K,xmin,xmax,make_plots,Nxn)
% tabulate_IC(a,b,dt,K,xmin,xmax,make_plots)
%
% Computes the optimal control for the last time step, at time T-dt, and, 
% by numerical integration, the corresponding value of the KBE solution
% at time T-dt. The solutions are coputed on a discretization of the 
% truncated solution interval, which can later be interpolated and 
% extrapolated.
%
% The optimal control modifies the drift of the Ito SDE approximation in 
% order to minimize the variance of the exit probability.
% Original SDE time stepping:
%   Delta X_n = a(t_n,X_n) Delta t 
%             + b(t_n,X_n) sqrt(Delta t) eps_n,
% with the eps_n independent standard Normal random variables
% Controled SDE time stepping:
%   Delta Y_n = a(t_n,Y_n) Delta t 
%             + b(t_n,Y_n) sqrt(Delta t) ( sqrt(Delta t) Zeta + eps_n )
% Note scaling sqrt(Delta t) Zeta, consistent with Nadhir, Abdul-Lateef,
% et al., arXiv 2207.06926
% This scaling leads to optimal controls Zeta approximately proportional
% to 1/Delta t for x<<K.
%
% Note that the problem is extremely ill-posed due to the rare event 
% feature of the original problem. Numerical approximation of the function
% that is the target of the minimization looses all accuracy when the this
% target is too small (which happens rather quickly). If used in practice,
% most likely the best alternative is to compute the opimal control in a 
% rather small interval below 'K' and then force a smooth transition down
% to 0 outside this interval. (It is tempting to extrapolate the control,
% but this will only lead to numerical issues with the evaluation of the
% likelihood factors in the importance sampling.
%
% INPUT:
%   a,  function handle, drift coefficient _without_ time dependence
%                        i.e., a = @(x) ...
%   b,  function handle, dito diffusion
%                        Assumed: constant sign, bounded away from 0
%   dt, positive real scalar, time step size of the SDE solver
%   K,  real scalar, barrier representing the rate event occurance
%   xmin, real scalar<K, lower boundary of the solution interval
%   xmax, real scalar>K, upper dito
%   make_plots, logical, if 'true' then illustrative plots are produced
%
% OUTPUT:
%   xv, real vector, discretization points where the control is computed
%   zetav, real vector, optimal control at time T-dt computed in 'xv'
%   u_tau, real vector, corresponding values of the KBE at time T-dt


  R = @(K,x,alpha,beta,dt) ...
    -sign(beta).*((K-x)./(sqrt(dt)*beta)-sqrt(dt)*alpha./beta);
  % Target of minimization
  F = @(z,K,x,ax,bx,dt) ...
    exp(0.5*(z.^2+2*abs(z).*R(K,x,ax,bx,dt)-R(K,x,ax,bx,dt).^2)) ...
    .*erfcx((abs(z)-R(K,x,ax,bx,dt))/sqrt(2))/2;

  % Its derivative, target of zero-search
  f = @(z,K,x,ax,bx,dt) z.*(erf((R(K,x,ax,bx,dt)-abs(z))/sqrt(2))+1) - ...
    sign(z).*exp(-(R(K,x,ax,bx,dt)-abs(z)).^2/2)/sqrt(2*pi);
  % and its second derivative, for Newton's method
  dfdz = @(z,K,x,ax,bx,dt) 2*(erf((R(K,x,ax,bx,dt)-abs(z))/sqrt(2))+1) - ...
    (R(K,x,ax,bx,dt)-abs(z)).*exp(-(R(K,x,ax,bx,dt)-abs(z)).^2/2)/sqrt(2*pi);


  % Discretize x, coordinate of the 1D SDE solution 
  Nx = Nxn;
  xv = linspace(xmin,K,Nx);
  dx = (K-xmin)/(Nx-1);
  zetav = zeros(1,Nx);
  Fv = zetav;
  Newton_points = false(size(zetav));
  Search_points = Newton_points;
  
  z  = sign(b(round(xv(Nx-1))))*1e-14; % Initial guess for optimal control at K-dx

  % Start closest to K, and progressively move further away using iterative
  % methods for finding the optimal control with the previously computed 
  % value as initial guess.
  for n=Nx-1:-1:1
    x = xv(n);
    ax = a(x); bx = b(x);

    % Newton's method
    dz = inf;
    iter = 0; maxiter = 100; break_Newton = false;
    zv = zeros(maxiter+1,1);
    zv(iter+1) = z;
    while abs(dz)>1e-8 && iter<maxiter && ~break_Newton
      nom = -f(z,K,x,ax,bx,dt);
      denom = dfdz(z,K,x,ax,bx,dt);
      if abs(denom)<1e-10 % Completety arbitrary choice...
        break_Newton = true;
      else
        dz = nom/denom;
        z  = z+dz;
        iter = iter+1;
        zv(iter+1) = z;
      end
    end
  
    if abs(dz)>1e-3 && ~break_Newton
      keyboard
    end

    if break_Newton
      % Evaluating F, using MATLAB's "erfcx", is more numerically stable
      % than evaluating f/dfdx. Switch to a more basic optimization 
      % algorithm for unimodal functions.
      gr = 0.5*(1+sqrt(5));
      gr2 = 1/gr;
      gr1 = 1-gr2;
      if z<0
        z_max = 0.95*z; z_min = 1.05*z;
      else
        z_max = 1.05*z; z_min = 0.95*z;
      end
      dz = z_max-z_min;
      F_z_min = F(z_min,K,x,ax,bx,dt);
      F_z_max = F(z_max,K,x,ax,bx,dt);
      z1 = z_min + gr1*dz;
      z2 = z_min + gr2*dz;
      F_z1 = F(z1,K,x,ax,bx,dt);
      F_z2 = F(z2,K,x,ax,bx,dt);
      if max(F_z1,F_z2)>max(F_z_min,F_z_max)
        error('tabulate_IC: Unimodality assumption clearly violated.')
      end
      gr_iter = 0; max_gr_iter = 1e4;
      while dz>0.5e-6 && gr_iter<max_gr_iter
        if F_z2>F_z1
          z_max = z2; 
          dz = z_max-z_min;
          z2 = z1;
          F_z2 = F_z1;
          z1 = z_min + gr1*dz;
          F_z1 = F(z1,K,x,ax,bx,dt);
        else
          z_min = z1;
          dz = z_max-z_min;
          z1 = z2;
          F_z1 = F_z2;
          z2 = z_min + gr2*dz;
          F_z2 = F(z2,K,x,ax,bx,dt);
        end
        gr_iter = gr_iter+1;
      end
      z = z_min+0.5*dz;
    end
    if ~break_Newton
      Newton_point(n) = true;
    else
      Search_point(n) = true;
    end
    zetav(n) = z/sqrt(dt);
    Fv(n) = F(z,K,x,ax,bx,dt);
  end

  % Compute u_tau(T-dt,x) according to Nadhir & Gaukhar citing Hartmann et al
  % That is, zeta(t,x) = b(t,x) d/dx log(sqrt(u_tau(t,x)))
  integrand = zetav./b(xv);
  integrand = integrand(end:-1:1);
  exponent  = -2*(cumsum(integrand)-0.5*(integrand(1)+integrand))*dx;
  u_tau     = exp(exponent(end:-1:1));

  % Extend optimal control to zero above K
  xv_ext = linspace(K,xmax,5);
  xv_ext = xv_ext(2:end);
  zetav_ext = zeros(1,4);
  u_tau_ext = ones(1,4);

  xv = [xv];
  zetav = [zetav];
  u_tau = [u_tau];

  if make_plots
    figure, plot(xv,zetav,'k-','linewidth',2)
    xlabel('$x$','interpreter','latex','fontsize',14)
    ylabel('$\zeta(T-\Delta t,x)$','interpreter','latex','fontsize',14)
    title(['Optimal control $\zeta(T-\Delta t,x)$ for $\Delta t=',num2str(dt),'$'],'interpreter','latex','fontsize',14)

    figure, plot(xv,u_tau,'k-','linewidth',2)
    xlabel('$x$','interpreter','latex','fontsize',14)
    ylabel('$u_\tau(T-\Delta t,x)$','interpreter','latex','fontsize',14)
    title(['KBE solution at $T-\Delta t$ for $\Delta t=',num2str(dt),'$'],'interpreter','latex','fontsize',14)

    figure, plot(xv,log10(u_tau),'k-','linewidth',2)
    xlabel('$x$','interpreter','latex','fontsize',14)
    ylabel('$\log_{10}{(u_\tau(T-\Delta t,x))}$','interpreter','latex','fontsize',14)
    title(['$\log_{10}$ of KBE solution at $T-\Delta t$ for $\Delta t=',num2str(dt),'$'],'interpreter','latex','fontsize',14)

    % For visualization of the optimal control, computed above, plot target 
    % function and square probability as function of x and control, zeta.

    % Square of rare event probability 
    F2 = @(K,x,ax,bx,dt) (erfc(-R(K,x,ax,bx,dt)/sqrt(2))/2 ).^2;

    figure; hold on
    xpv = xmin:0.05:2.95; 
    zetapv = linspace(0,1.05*max(abs(zetav)),1001);
    for x = xpv
      zpv = sign(b(x))*zetapv*sqrt(dt);
      tmp = F(zpv,K,x,a(x),b(x),dt);
      plot3(x*ones(size(zpv)),zpv/sqrt(dt),log(tmp),'k-','linewidth',2);
      plot3(x*ones(size(zpv)),zpv/sqrt(dt),log(F2(K,x,a(x),b(x),dt))*ones(size(zpv)),'k-','linewidth',2);
    end
    l1=plot3(xv(Newton_point),zetav(Newton_point),log(Fv(Newton_point)),'r-','linewidth',2);
    l2=plot3(xv(Search_point),zetav(Search_point),log(Fv(Search_point)),'b-','linewidth',2);
    lh=legend([l1,l2],'Found by Newton''s method','Found by Golden Ratio search');
    set(lh,'location','NorthOutside','fontsize',14)
    xlabel('$x$','fontsize',16,'interpreter','latex')
    ylabel('$\zeta$','fontsize',16,'interpreter','latex')
    zlabel('$\log{F(\zeta;x)}$','fontsize',16,'interpreter','latex')
    view(3)
  end
end