clear; %close all;
% d=40;
% F=0;
d=3;         %problem dimension
r=10;
q=8/3;
s=28;
% r=3;
% q=1;
% s=26.5;
% r=1;
% q=2;
% s=8/3;
T=1.5;
h=0.001;
%initialize
sigma=10;
%mu=zeros(d,1);
mu=[0 0 1]';
I=eye(d);
Sigma=0.0025*I;
t=(0:h:T)';
v=zeros(d,length(t));
b=[sigma; sigma;sigma];
c=[0.5; 0.5; 0.5];
v(:,1)=mu+chol(Sigma)'*randn(d,1); %choose initial condition
% [tt,u]=ode45(@(t,v) lorenz96(v,F), [0 T], v(:,1));
% 
% figure(1)
% plot(tt, u(:,1), 'b', 'LineWidth', 2)
% xlabel('t')
% ylabel('v_1')
%%
% error=sqrt(sum((v-u).^2,2));
% figure(2)
% semilogy(t, error, 'k','LineWidth', 2)
% xlabel('t')
% ylabel('error')
M=1;dt=h;
for m=1:M
    %v(:,1)=mu;
     u(:,1)=mu+chol(Sigma)'*randn(d,1); %choose initial condition
     u1(:,1)=mu+chol(Sigma)'*randn(d,1);
     %u1(:,1)=u(:,1); %choose initial conditio
     %u(:,1)=[1;1;1];
for k=1:length(t)-1 %for each time
%    v(:,n+1)=v(:,n)+lorenz96(v(:,n),F)*h+sigma*sqrt(h)*randn(d,1); %solved via RK4
%       u(1,k+1)=u(1,k)+r*(u(2,k)-u(1,k))*dt;%+sigma*sqrt(dt)*randn;
%       u(2,k+1)=u(2,k)+(q*u(1,k)-u(2,k)-u(1,k)*u(3,k))*dt;
%       u(3,k+1)=u(3,k)+(u(1,k)*u(2,k)-s*u(3,k));
      u(:,k+1)=u(:,k)+lorenz63(u(:,k),r,q,s)*dt+b.*sqrt(dt).*randn(3,1);
      u1(:,k+1)=u1(:,k)+lorenz63(u(:,k),r,q,s)*dt+c.*sqrt(dt).*randn(3,1);
end

figure(5)
plot(t, u(1,:),'r',t, u1(1,:),'b', 'LineWidth', 2)
xlabel('t', 'fontsize', 14)
ylabel('v_1', 'fontsize', 14)
hold on
end
title('Noisy Lorenz 63')
hold off
function dv = rk4(v_old,h,F)
% v[t+1] = rk4(v[t],step)
 k1 = lorenz96(v_old,F);
 k2 = lorenz96(v_old+1/2.*h.*k1,F);
 k3 = lorenz96(v_old+1/2.*h.*k2,F);
 k4 = lorenz96(v_old+h.*k3,F);
 dv= 1/6*h*(k1+2*k2+2*k3+k4);
end

function f=lorenz96(v,F)
f=[v(end); v(1:end-1)].*([v(2:end); v(1)]-[v(end-1:end);v(1:end-2)])-v+F*v.^0;
end

% function f=lorenz63(y,r,q,s)
% f(1,1)=r*(y(2)-y(1));
% f(2,1)=-r*y(1)-y(2)-y(1)*y(3);
% f(3,1)=y(1)*y(2)-s*y(3)-s*(r+q);
% end