function [u,x,t]=Tprob( xgridpoints,tgridpoints,taumax,spot,a,b,sg)
%%%%%Forward Kolmogorov PDE solver

%build the mesh grid
x_L=0; x_U=1.0;
x=linspace(x_L,x_U,xgridpoints);
t=linspace(0,taumax, tgridpoints);

% set up the pde solver
m=0;
sol=pdepe(m, @pdefun, @pdex1ic, @pdex1bcfun, x,t, [], taumax, spot, a,b,sg);

%Extract the first solution component as u.
u=sol(:,:,1);

%Defining the PDE
function [c,f,s]=pdefun(x,t,u,dudx,taumax, spot, a, b, sg)
c=1;
f=0.5*sg*sg*x.*x.*dudx+sg^2.*x.*u;
s=-a*(b-x).*dudx+a*u-x.*u;



%Defining the initial condition
function u0=pdex1ic(x,t,u,dudx,taumax, spot, a,b, sg)
u0=1000*max(1-abs(x-0.01)/0.001,0);



%Defining the Boundary conditions
function [pa, qa,pb,qb]=pdex1bcfun(xa,ua,xb,ub,x, t,u, dudx, taumax, spot, a,b,sg)
pa=ua;
qa=0;
pb=ub;
qb=0;


