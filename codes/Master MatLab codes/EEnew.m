close all; clear all; clc;
%%%%%Exponent expansion

%Paramater settings
a=0.1;
b=0.02;
sg=0.5;
y0=0.01;


x0=log(y0);
%Time horizon
T=1.0;

y=[0.0001:0.001:1.0];

x=log(y);
  
xmin=log(0.0001);
xmax=log(1.0);

dx=@(x) x-x0;
%mu_x, Veff_x, W0, W1, W2, W3

norm=1./sqrt(2*pi*sg^2*T);
pp=@(x) exp((-(x-x0).^2)./(2.*T.*sg^2));
pp0=@(x) exp(-x)-exp(-x0);
pp1=@(x) exp(x)-exp(x0);
pp2=@(x) exp(-2*x)-exp(-2*x0);
pp3=@(x) exp(-3*x)-exp(-3*x0);
pp4=@(x) exp(-4*x)-exp(-4*x0);
pp5=@(x) exp(2*x)-exp(2*x0);
Veff=@(x) (a*b/sg)^2.*exp(-2*x)/2.-a*b.*exp(-x).*(a+sg^2)./sg^2+(a+sg^2/2)^2/(2*sg^2)+exp(x);


%W0 coeff
c01=a*b/sg^2;
c02=(a+sg^2/2)/sg^2;
W0=@(x) c01.*pp0(x)+c02.*dx(x);

AD_x0= @(x)norm.*pp(x).*exp(-W0(x));
%plot(AD_x0);
SP0=integral(AD_x0, xmin, xmax);

%W1 coeff
c11=-0.25*(a*b/sg)^2;
c12=a*b*(a+sg^2)/sg^2;
c13=1;
c14=0.5*(a/sg+sg/2)^2;
W1=@(x) (c11.*pp2(x)+c12.*pp0(x)+c13.*pp1(x)+c14.*dx(x))./dx(x);

%W1p=0.5*(a*b/sg)^2*exp(-2*x0)-(a*b/sg^2)*exp(-x0)*(a+sg^2)+(a+sg^2/2)^2/(2*sg^2)+exp(x0);
AD_x1=  @(x)norm.*pp(x).*exp(-W0(x)).*exp(-W1(x).*T);
%plot(AD_x1(1:1000));
SP1=integral(AD_x1, xmin, xmax);

%W2 coeff
W2=@(x) sg^2*(Veff(x)+Veff(x0)-2*W1(x))./(2*dx(x).^2);

%W2p=(sg^2/12)*(exp(x0)+2*(a*b/sg)^2*exp(-2*x0)-(a*b/sg^2)*(a+sg^2)*exp(-x0));
AD_x2= @(x) norm.*pp(x).*exp(-W0(x)-W1(x).*T-W2(x).*T^2);
SP2=integral(AD_x2, xmin, xmax);
%spadx2=(1.0-0.00001)*trapz(AD_x2)/10000;
%plot(AD_x2(1:10000));
%title('AD prices by EE(2) at T=0.1')
%xlabel('y');
%ylabel('AD_y2');
%W3 coeff
c30=b*(a/sg)^4+2*a^3*b/sg^2-(a*b/sg)^2+1.25*a^2*b+0.25*a*b*sg^2;
c31=(a/sg)^2+a+0.25*sg^2;
c32=-0.75*(a/sg)^4*b^2-1.25*a^3*(b/sg)^2-0.5625*(a*b)^2;
c33=((a/sg)^4*b^3)/3+((a*b)^3/sg^2)/3;
c34=-0.0625*(a*b/sg)^4;
c35=0.5;
c36=0.25*(a/sg)^4+0.5*a^3/sg^2-2*(a/sg)^2*b+0.375*a^2-2*a*b+0.125*a*sg^2+0.015625*sg^4;

A=@(x) c30.*pp0(x)+c31.*pp1(x)+c32.*pp2(x)+c33.*pp3(x)+c34.*pp4(x)+c35.*pp5(x)+c36.*dx(x);
C=@(x) (-(a*b/sg)^2).*pp2(x)+(a+sg^2)*(a*b/sg^2).*pp0(x)+pp1(x);

%z=[ 0.99, 0.999, 0.9999, 0.99999, 0.999999, 1.000001, 1.00001, 1.0001, 1.001, 1.01]
%z1=[0.9991, 0.9992, 0.9993, 1.0009, 1.0008, 1.0007]
%h=x0.*z1;
W3=@(x)(-sg^2./(2.*dx(x).^4)).*(dx(x).*A(x)-dx(x).^2.*W1(x).^2)-(3*sg^2./dx(x).^2).*W2(x)+C(x).*(sg^4./(4.*dx(x).^3));

AD_x3= @(x) norm.*pp(x).*exp(-W0(x)-W1(x).*T-W2(x).*T^2-W3(x).*T^3);

%W3p=@(x) -sg^2/24*(exp(x)-a^2*b^2*exp(-2*x)/sg^2+a*b*(a+sg^2)*exp(-x)/sg^2).^2+sg^4/240*(exp(x)+8*a^2*b^2*exp(-2*x)/sg^2-a*b*(a+sg^2)*exp(-x)/sg^2);
%SP3=sum(AD_x3(4:2000));
%SP31=integral(AD_x3, xmin, @(x)-x);
SP3=integral(AD_x3, xmin, xmax);
%SP3=SP31+SP32;

%plot(AD_x3(13:1000))
%title('AD_x density 3rd order approximation ')
%xlabel('x');
%ylabel('AD_x3');
%AD_x11=norm.*pp(x).*exp(-W0(x)).*exp(-W1(x).*T)./y;
%plot(AD_x11);

%x=log(y);
