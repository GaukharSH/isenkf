close all; clear all; clc
%%Numerical solution of the Forward Kolmogorov PDE%% 
%%%%%%%%%%%%for AD prices%%%%%%%%%%%%%%
%%du/dt=-X*u-d(a(b-X)u)/dX+0.5*sigma*d2(sigma*X^2*u)/dX2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%assign the time horizon
taumax=1.0;

%%model settings
spot=0.01; a=0.1; b=0.02; sg=0.5;

%%pde settings
xgridpoints=10000; %assign the number of xgrid points
tgridpoints=100; %assign the number of tgrid points

%solve the PDE
[u,x,t]=pdeFK( xgridpoints, tgridpoints,taumax, spot, a,b,sg);

sp=trapz(u(100,:))/10000;
%plot the solution
%h=figure('Color', [1 1 1]);
%surf(x, t, u)

%title('Numerical solution of the FKE for AD prices at T=0.25')
%xlabel('y');
%ylabel('AD_y');
%print(h, '-djpeg', 'figNumsolBKE.jpg')

