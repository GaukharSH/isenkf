close all; clear all; clc
%%Numerical solution of the Forward Kolmogorov PDE%% 
%%%%%%%%%%%%for Transition Probabilities%%%%%%%%%%%%%%
%%du/dt=-a(b-X)du/dX+0.5*sigma*sigma*X^2*d2u/dX2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%assign the time horizon
taumax=10.0;
%%model settings
spot=0.01; a=0.1; b=0.02; sg=0.5;

%%pde settings
xgridpoints=1000; %assign the number of xgrid points
tgridpoints=100; %assign the number of tgrid points

%solve the PDE
[u,x,t]=Tprob( xgridpoints, tgridpoints,taumax, spot, a,b,sg);

%plot the solution
h=figure('Color', [1 1 1]);
surf(x, t, u)
plot(u(100,1:100));


