close all; clear all; clc
%%Numerical solution of the Backward Kolmogorov PDE%% 
%%%%%%%%%%%%for survival probabilities%%%%%%%%%%%%%%
%%du/dt-X*u+a(b-X)du/dX+0.5*sigma*sigma*X^2*d2u/dX2=0
%%u(T,T;X)=1 the final condition
%%u(t,T;0)=1 and u(t,T;+inf)=0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Model parameters for Lorenz 63
d=3;           %problem dimension
sigma=0.5;     %constant diffusion parameter
r=10;
q=8/3;
s=28;
a = @(u) [r*(u(2,:,:)-u(1,:,:)); s*u(1,:,:)-u(2,:,:)-u(1,:,:).*u(3,:,:); u(1,:,:).*u(2,:,:)-q*u(3,:,:)]; %Lorenz 63 drift
b = [sigma; sigma; sigma]; 
I=eye(d);

%Simulation parameters
T=1;         %final time (simulation length)
M=10^4;      %Monte Carlo sample size
N=100;       %number of timesteps in SDE
dt=T/N;      %discretization step in SDE
% Initial original density parameters 
mu=[5 5 25]';
Sigma=0.5*I;

%Define the Projector which component to track
P1   = I(1,:); 
%% Generate samples of Lorenz 63 dynamics to use for L2 regression
u=zeros(d,N+1,M);
Mbar=zeros(1,M);
for m=1:M
    u(:,1,m)=mu+chol(Sigma)'*randn(d,1);
    for n=1:N       
        %Lorenz63
        u(:,n+1,m)=u(:,n,m)+lorenz63(u(:,n,m),r,q,s)*dt+b.*sqrt(dt).*randn(3,1);%EM time-stepping
    end  
    Mbar(m)=max(P1*u(:,:,m));
end
%K=floor(max(Mbar)) %corresponds to 10^(-4) probability
K = 13.5
alpha_hat=mean(Mbar>=K)
var_hat=var(Mbar>=K)
% %% CE
% beta=0.01;                              %CE method parameter
% sMbar=sort(Mbar);
% K_ell=sMbar(1, ceil((1-beta)*M));
% K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
% Weight = ones(1,M);
% h=Mbar>=K_ell;
% ell=1;
% Sigma_tilde=Sigma;
% u0=reshape(u(:,1,:),d,M);
% 
% while K_ell<K
%     mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight);
%     
%     ell=ell+1;
%     for m=1:M
%         u(:,1,m)=mu_tilde+chol(Sigma)'*randn(d,1);
%         u0(:,m)=u(:,1,m);
%         for n=1:N       
%             %Lorenz63
%             u(:,n+1,m)=u(:,n,m)+lorenz63(u(:,n,m),r,q,s)*dt+b.*sqrt(dt).*randn(3,1);%EM time-stepping
%         end  
%         Mbar(m)=max(P1*u(:,:,m));
%     end
%     sMbar=sort(Mbar);
%     K_ell=sMbar(1, ceil((1-beta)*M));
%     K_ell=K_ell*(K_ell<K)+K*(K_ell>=K);
%     h=Mbar>=K_ell;
%     Weight=mvnpdf(u0', mu', Sigma)'./mvnpdf(u0',mu_tilde',Sigma_tilde)';
% end
% 
% if K_ell==K
%     mu_tilde=sum(h.*Weight.*u0,2)/sum(h.*Weight)
% end
%% L2 regression
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We approximate only the drift, the diffusion is constant

%Define the Polynomial space
w = 4;                                                         %Polynomial degree
p = poly_space(w,'HC');                                        %Polynomial space
p_dim = size(p,1);                                             %Cardinality of polynomial space

t  = linspace(0,T,N+1)';                                       %Generating t_0, t_1, ..., t_{N-1}
Tt = reshape(repmat(t,1,M)', (N+1)*M,1);                       %Replicating M times each t_n and saving as a column vector [t_0 t_0 ... t_0, t_1, ..., t_1,..., t_{N-1}, ..., t_{N_1}]'
Xx = reshape(squeeze(pagemtimes(P1,u))', (N+1)*M,1);           %Projected Samples saved as a column vector [X_0^(1),..., X_0^(M), X_1^(1),..,X_1^(M), ..., X_{N-1}^(1),...,X_{N-1}^(M)]
tX = [Tt Xx];                                                  %Two column vectors of size N*M for [t_n, X_{t_n}]
f  = reshape(squeeze(pagemtimes(P1,a(u)))',(N+1)*M,1);         %Given f function, we are going to solve

D     = x2fx(tX, p);                                           %This function helps to create Psi matrix each column is non-orth. basis function psi_p for different given p
[Q,R] = modified_GS(D);                                        %Apply modified Gram-Schmidt process to get QR decomposition
a_p   = Q'*f;                                                  %Solve Normal equations based on orthonormalised basis

psy     = @(t,s) x2fx([t s], p);                               %Non-orth. basis function
psy_bar = @(t,s) x2fx([t s], p)/R;                             %Orth. basis function
a_bar   = @(t,s) psy_bar(t,s)*a_p;                             %Approximation to the drift function a(x)
b_bar   = @(s)   sqrt(P1*(b*b')*P1')*ones(size(s));            %Approximation to the diffusion function b(x)
%close all
% figure(1)
% fsurf(a_bar, [0 T 0 K])
% hold on
% plot3(Tt, Xx, f, 'k*')
% hold off

figure
plot3(Tt, Xx, f, 'k*')
hold on
xplot=linspace(-2,K+2,51);
aplot=zeros(length(t), length(xplot));
[tt,xx]=ndgrid(t,xplot);
for i=1:length(t)
    for j=1:length(xplot)
        aplot(i,j)=a_bar(t(i),xplot(j));
    end
end
surf(tt, xx, aplot)
grid on
xlabel('t', 'fontsize', 18)
ylabel('x','fontsize', 18)
zlabel('$\bar{a}(x,t)$', 'interpreter', 'latex','fontsize', 18)
title(['$\mathcal{K}=$' num2str(K), ', ', '$\hat{\alpha}=$', num2str(alpha_hat), ', ', 'HC=4'],'interpreter', 'latex','fontsize', 22)
hold off
%%
%assign the time horizon
T=1.0;

%%model settings
spot=0.01; a=0.1; b=0.02; sg=0.5; sigma=0.5;

%%pde settings
xL = 3;                       % Lower end of discretization interval
xU = K;                       % Upper end of discretization interval
dxPde = 0.01;                 % Space step in PDE    
dtPde = 0.01;%dxPde^2;              % Time step in PDE
xgridpoints = round((xU-xL)/dxPde);    % Number of space steps in PDE
tgridpoints = round(T/dtPde);          % Number of time steps in PDE
x  = linspace(xL,xU,xgridpoints);      % Space grid points
t  = linspace(0,T,tgridpoints);
%solve the PDE
[u,x,t]=pdeBK(T, x, t, sigma, a_bar);


h=figure('Color', [1 1 1]);
surf(x, t, u)
%plot(x,u(50,:));
grid on,
%magnify

title('A solution of the BKE for surv.prob')
xlabel('Distance Y');
ylabel('Time \tau');



function [u,x,t]=pdeBK(T,x,t,sg, a_bar)
%%% Backward Kolmogorov PDE solver
%build the mesh grid
% set up the pde solver
m=0;
sol=pdepe(m, @pdefun,@pdex1ic, @pdex1bcfun, x,t,[],sg, a_bar);

%Extract the first solution component as Q.
u=sol(:,:,1);
end
%Defining the PDE

function [c,f,s]=pdefun(x,t,u,dudx,sg, a_bar)
c=1;
f=-0.5*sg*sg*dudx;
%s=-a_bar(x,t).*dudx;
s=-0.1*(0.2-x).*dudx;
end


%Defining the final condition
function u0=pdex1ic(x,t, u, dudx, sg)
u0=exp(10*(x-13.5));
end

%Defining the Boundary conditions
function [pa, qa,pb,qb]=pdex1bcfun(xa,ua,xb,ub,x,t, u, dudx,sg)
pa=ua;
qa=0;
pb=ub-1.0;
qb=0;
end



