close all; clear all; clc;
%%%%%Exponent expansion

%Paramater settings
a=0.1;
b=0.02;
sg=0.5;
y0=0.01;


x0=log(y0);
%Time horizon
T=1.0;

y=[0.00001:0.0001:1.0];

x=log(y);

dx=@(x) x-x0;
%mu_x, Veff_x, W0, W1, W2, W3

norm=1./sqrt(2*pi*sg^2*T);
pp=@(x) exp((-(x-x0).^2)./(2.*T.*sg^2));
pp0=@(x) exp(-x)-exp(-x0);
pp1=@(x) exp(x)-exp(x0);
pp2=@(x) exp(-2*x)-exp(-2*x0);
pp3=@(x) exp(-3*x)-exp(-3*x0);
pp4=@(x) exp(-4*x)-exp(-4*x0);
pp5=@(x) exp(2*x)-exp(2*x0);
Veff=@(x) (a*b/sg)^2.*exp(-2*x)/2.-a*b.*exp(-x).*(a+sg^2)./sg^2+(a+sg^2/2)^2/(2*sg^2);


%W0 coeff
c01=a*b/sg^2;
c02=(a+sg^2/2)/sg^2;
W0=@(x) c01.*pp0(x)+c02.*dx(x);

Tprob_x0= norm.*pp(x).*exp(-W0(x))./y;
plot(Tprob_x0);


%W1 coeff
c11=-0.25*(a*b/sg)^2;
c12=a*b*(a+sg^2)/sg^2;
c14=0.5*(a/sg+sg/2)^2;
W1=@(x) (c11.*pp2(x)+c12.*pp0(x)+c14.*dx(x))./dx(x);

%W1p=0.5*(a*b/sg)^2*exp(-2*x0)-(a*b/sg^2)*exp(-x0)*(a+sg^2)+(a+sg^2/2)^2/(2*sg^2)+exp(x0);
Tprob_x1= norm.*pp(x).*exp(-W0(x)).*exp(-W1(x).*T)./y;
%plot(Tprob_x1(1:1000));
%SP1=integral(Tprob_x1, xmin, xmax);

%W2 coeff
W2=@(x) sg^2*(Veff(x)+Veff(x0)-2*W1(x))./(2*dx(x).^2);

%W2p=(sg^2/12)*(exp(x0)+2*(a*b/sg)^2*exp(-2*x0)-(a*b/sg^2)*(a+sg^2)*exp(-x0));
Tprob_x2= norm.*pp(x).*exp(-W0(x)-W1(x).*T-W2(x).*T^2)./y;
%plot(Tprob_x2(1:1000));
title('Transition density by EE(2) at T=1.0')
xlabel('y');
ylabel('Tprob_y2');

%W3 coeff
v1=(a*b/sg)^2/2;
v2=-a*b*(a+sg^2)/sg^2;
v3=(a+sg^2/2)^2/(2*sg^2);
c30=-2*v2*v3;
c32=-0.5*(2*v1*v3+v2^2);
c33=-2*v1*v2/3;
c34=-0.0625*(a*b/sg)^4;
c36=v3^2;

A=@(x) c30.*pp0(x)+c32.*pp2(x)+c33.*pp3(x)+c34.*pp4(x)+c36.*dx(x);
C=@(x) (-(a*b/sg)^2).*pp2(x)+(a+sg^2)*(a*b/sg^2).*pp0(x);

W3=@(x)(-sg^2./(2.*dx(x).^4)).*(dx(x).*A(x)-dx(x).^2.*W1(x).^2)-(3*sg^2./dx(x).^2).*W2(x)+C(x).*(sg^4./(4.*dx(x).^3));

Tprob_x3= norm.*pp(x).*exp(-W0(x)-W1(x).*T-W2(x).*T^2-W3(x).*T^3)./y;


%plot(Tprob_x3(1:1000))
%title('Transition density 3rd order approximation ')
%xlabel('x');
%ylabel('Tprob_x3');

