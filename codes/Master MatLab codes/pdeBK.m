function [u,x,t]=pdeBK(T,x,t,sg, a_bar)
%%% Backward Kolmogorov PDE solver
%build the mesh grid
% set up the pde solver
m=0;
sol=pdepe(m, @pdefun,@pdex1ic, @pdex1bcfun, x,t, [], spot,a,b,sg);

%Extract the first solution component as Q.
u=sol(:,:,1);

%Defining the PDE
function [c,f,s]=pdefun(x,t,u,dudx,sg, a_bar)
c=1;
f=-0.5*sg*sg*dudx;
s=-a_bar(x,t).*dudx;



%Defining the final condition
function u0=pdex1ic(x,t, u, dudx, sg)
u0=0.0;

%Defining the Boundary conditions
function [pa, qa,pb,qb]=pdex1bcfun(xa,ua,xb,ub,x,t, u, dudx,sg)
pa=ua;
qa=0;
pb=ub-1.0;
qb=0;


