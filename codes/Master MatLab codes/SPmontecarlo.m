%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%Monte Carlo simulation for survival probabilities%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;close all
%Model: dr = a * (b - r ) * dt + sg *r* dW
%Assign Inputs
r0=0.01; a=0.1; b=0.02; sg=0.5; 
nstep=1000; T=1.0; nsimul=100000; 
dt=T/nstep;
avr=0;
avr2=0;
if dt<(1/sqrt(nsimul))

for j=1:nsimul
    %Initialize the vector
    r=zeros(nstep,1); r(1)=r0;
    W=randn(nstep,1);
    %Start iteration
   for i=1:nstep-1 
    r(i+1)=r(i)+a*(b-r(i))*dt+sg*r(i)*W(i)*sqrt(dt); 
   end
  
   I=T*trapz(r)/(nstep);
   avr=avr+exp(-I);
   avr2=avr+exp(-I)*exp(-I);
    
end
%Plot(r);
avr=avr/nsimul;
avr2=avr2/nsimul;
error=sqrt((avr2-avr*avr)/nsimul);
end